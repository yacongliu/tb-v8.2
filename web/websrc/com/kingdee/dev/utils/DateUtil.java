package com.kingdee.dev.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @Copyright 天津金蝶软件有限公司
 * @Title DateUtil
 * @Description 日期工具类
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019年8月27日
 */
public class DateUtil {

    private static ThreadLocal<SimpleDateFormat> threadDateTime = new ThreadLocal<SimpleDateFormat>();

    private static ThreadLocal<SimpleDateFormat> threadDate = new ThreadLocal<SimpleDateFormat>();

    private static ThreadLocal<Calendar> threadCalender = new ThreadLocal<Calendar>();

    /**
     * 日期时间类型格式
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 日期类型格式
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 时间类型的格式
     */
    public static final String TIME_FORMAT = "HH:mm:ss";

    private static SimpleDateFormat DateTimeInstance() {
        SimpleDateFormat sdf = threadDateTime.get();
        if (sdf == null) {
            sdf = new SimpleDateFormat(DATETIME_FORMAT);
            threadDateTime.set(sdf);
        }
        return sdf;
    }

    private static SimpleDateFormat DateInstance() {
        SimpleDateFormat sdf = threadDate.get();
        if (sdf == null) {
            sdf = new SimpleDateFormat(DATE_FORMAT);
            threadDate.set(sdf);
        }
        return sdf;
    }

    private static Calendar CalenderInstance() {
        Calendar calender = threadCalender.get();
        if (calender == null) {
            calender = Calendar.getInstance();
            threadCalender.set(calender);
        }
        return calender;
    }

    /**
     * 
     * @Title currentDateTime
     * @Description 获取当前日期时间
     * @Author yacong_liu
     * @return 返回当前时间的字符串值
     */
    public static String currentDateTime() {
        return DateTimeInstance().format(new Date());
    }

    /**
     * 
     * @Title dateTime
     * @Description 日期格式化
     * @Author yacong_liu
     * @param date
     * @return 年月日时分秒格式字符串
     */
    public static String dateTime(Date date) {
        return DateTimeInstance().format(date);
    }

    /**
     * 
     * @Title dateTime
     * @Description 字符串解析为时间类型
     * @Author yacong_liu
     * @param datestr
     * @return 日期
     * @throws ParseException
     */
    public static Date dateTime(String datestr) throws ParseException {
        return DateTimeInstance().parse(datestr);
    }

    /**
     * 
     * @Title currentDate
     * @Description 获取当前的日期
     * @Author yacong_liu
     * @return 年月日字符串日期
     */
    public static String currentDate() {
        return DateInstance().format(new Date());
    }

    /**
     * 
     * @Title date
     * @Description 日期格式化
     * @Author yacong_liu
     * @param date 日期
     * @return 年月日类型字符串日期
     */
    public static String date(Date date) {
        return DateInstance().format(date);
    }

    /**
     * 
     * @Title date
     * @Description 字符串解析为时间类型
     * @Author yacong_liu
     * @param dateStr 字符串日期
     * @return 年月日类型日期
     * @throws ParseException
     */
    public static Date date(String dateStr) throws ParseException {
        return DateInstance().parse(dateStr);
    }

    /**
     * 
     * @Title currentTimestamp
     * @Description 当前时间戳
     * @Author yacong_liu
     * @return 时间戳
     */
    public static Long currentTimestamp() {
        return System.currentTimeMillis();
    }

    /**
     * 
     * @Title getCurrentDate
     * @Description 系统当前日期
     * @Author yacong_liu
     * @return 日期
     */
    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    public static int getYear(Date date) {
        CalenderInstance().setTime(date);
        return CalenderInstance().get(Calendar.YEAR);
    }

    public static int getCurrentYear() {
        CalenderInstance().setTime(getCurrentDate());
        return CalenderInstance().get(Calendar.YEAR);
    }

    public static int getMonth(Date date) {
        CalenderInstance().setTime(date);
        return CalenderInstance().get(Calendar.MONTH) + 1;
    }

    public static int getCurrentMonth() {
        CalenderInstance().setTime(getCurrentDate());
        return CalenderInstance().get(Calendar.MONTH) + 1;
    }

    public static int getDay(Date date) {
        CalenderInstance().setTime(date);
        return CalenderInstance().get(Calendar.DAY_OF_MONTH);
    }

    public static int getCurrentDay() {
        CalenderInstance().setTime(getCurrentDate());
        return CalenderInstance().get(Calendar.DAY_OF_MONTH);
    }

}
