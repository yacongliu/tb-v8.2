package com.kingdee.dev.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @Copyright 天津金蝶软件有限公司
 * @Title StringUtil
 * @Description 字符串工具类
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019年8月27日
 */
public class StringUtil {

    /**
     * 
     * @Title toString
     * @Description 进行toString操作，如果传入的是null，返回指定的默认值
     * @Author yacong_liu
     * @param obj
     * @param defaultValue 默认值 （对象为空时 返回该值）
     * @return String
     */
    public static String toString(Object obj, String defaultValue) {
        return obj == null ? defaultValue : obj.toString();
    }

    /**
     * 
     * @Title toString
     * @Description 进行toString操作，如果传入的是null，返回null
     * @Author yacong_liu
     * @param obj
     * @return String
     */
    public static String toString(Object obj) {
        return toString(obj, null);
    }

    /**
     * 
     * @Title encoder
     * @Description 以UTF-8类型对字符串编码
     * @Author yacong_liu
     * @param str 传入字符串
     * @return String 编码字符串
     * @throws UnsupportedEncodingException
     */
    public static String encoder(String str) throws UnsupportedEncodingException {
        return encoder(str, "UTF-8");
    }

    /**
     * 
     * @Title encoder
     * @Description 对字符串编码
     * @Author yacong_liu
     * @param str 传入字符串
     * @param codeType 编码类型
     * @return 编码字符串
     * @throws UnsupportedEncodingException
     */
    public static String encoder(String str, String codeType) throws UnsupportedEncodingException {

        return str == null ? null : URLEncoder.encode(str, codeType);

    }

    /**
     * 
     * @Title decoder
     * @Description 以 UTF-8类型对字符串解码
     * @Author yacong_liu
     * @param str 传入字符串
     * @return 解码后字符串
     * @throws UnsupportedEncodingException
     */
    public static String decoder(String str) throws UnsupportedEncodingException {
        return decoder(str, "UTF-8");
    }

    /**
     * 
     * @Title decoder
     * @Description 对字符串解码
     * @Author yacong_liu
     * @param str 传入字符串
     * @param codeType 解码类型
     * @return 解码后字符串
     * @throws UnsupportedEncodingException
     */
    public static String decoder(String str, String codeType) throws UnsupportedEncodingException {

        return str == null ? null : URLDecoder.decode(str, codeType);

    }

    /**
     * 
     * @Title isEmpty
     * @Description 字符串是否为空
     * @Author yacong_liu
     * @param str 传入字符串
     * @return 是否为空
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    /**
     * 
     * @Title isNotEmpty
     * @Description 字符串是否不为空
     * @Author yacong_liu
     * @param str 传入字符串
     * @return 是否不为空
     */
    public static boolean isNotEmpty(String str) {
        return str != null && str.trim().length() > 0;
    }

    /**
     * 
     * @Title replaceBlank
     * @Description 去除字符串中的空格、回车、换行符、制表符
     * @Author yacong_liu
     * @param str 传入字符串
     * @return 处理后的字符串
     */
    public static String replaceBlank(String str) {
        if (isNotEmpty(str)) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            str = m.replaceAll("");
        }
        return str;
    }

    /**
     * 
     * @Title split
     * @Description 分割字符串
     * @Author yacong_liu
     * @param toSplit 待分割字符串
     * @param delimiter 分隔符
     * @return 分割后字符串数组 否则return null
     */
    public static String[] split(String toSplit, String delimiter) {
        if (isNotEmpty(toSplit)) {
            int offset = toSplit.indexOf(delimiter);
            if (offset < 0) {
                return null;
            } else {
                String beforeDelimiter = toSplit.substring(0, offset);
                String afterDelimiter = toSplit.substring(offset + delimiter.length());
                return new String[] { beforeDelimiter, afterDelimiter };
            }
        } else {
            return null;
        }
    }
}
