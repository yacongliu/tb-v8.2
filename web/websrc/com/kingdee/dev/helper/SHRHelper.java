package com.kingdee.dev.helper;

import java.sql.SQLException;
import java.util.HashSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.IMetaDataLoader;
import com.kingdee.bos.metadata.MetaDataLoaderFactory;
import com.kingdee.bos.metadata.entity.EntityObjectInfo;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.dev.utils.StringUtil;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.IPositionMember;
import com.kingdee.eas.basedata.org.PositionMemberCollection;
import com.kingdee.eas.basedata.org.PositionMemberFactory;
import com.kingdee.eas.basedata.person.IPerson;
import com.kingdee.eas.basedata.person.PersonFactory;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.ICoreBase;
import com.kingdee.eas.hr.emp.IPersonPosition;
import com.kingdee.eas.hr.emp.PersonPositionCollection;
import com.kingdee.eas.hr.emp.PersonPositionFactory;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;

/**
 * 
 * @Copyright 天津金蝶软件有限公司
 * @Title SHRHelper
 * @Description S-HR 职员数据工具类
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019年8月27日
 */
public class SHRHelper {
    private static Logger logger = Logger.getLogger(SHRHelper.class);

    /**
     * @description 判断当前登录用户是否具有相关的权限
     * @title isHasPermissions
     * @param personId 当前用户id
     * @param permissinName 权限项名称
     * @return 是否具有权限 true 是 ，false 否
     * @author 相建彬
     * @throws ShrWebBizException
     * @date 2020年1月7日
     */
    public static boolean isHasPermissions(Context ctx, String personId, String permissinName)
            throws ShrWebBizException {
        String userId = getUserIdForPersonId(ctx, personId);
        String sql = "SELECT uop.FID FROM T_PM_UserOrgPerm uop left join T_PM_PermItem per on per.FID= uop.FPERMITEMID where per.FName='"
                + permissinName + "'and uop.FOWNER='" + userId + "'";
        System.out.println("判断当前登录用户是否具有相关的权限的sql：" + sql);
        IRowSet rowSet;
        try {
            rowSet = DbUtil.executeQuery(ctx, sql.toString());
            if (rowSet.size() <= 0) {
                logger.error("该用户没有对应权限项！");
                return false;
            }
        } catch (BOSException e) {
            logger.error("********SHRHelper.isHasPermissions error!", e);
        }

        return true;

    }

    /**
     * @description 根据职员id获取用户id
     * @title getUserIdForPersonId
     * @param ctx 上下文
     * @return userId 用户id
     * @throws ShrWebBizException
     * @author 相建彬
     * @date 2020年1月7日
     */
    public static String getUserIdForPersonId(Context ctx, String personId) throws ShrWebBizException {
        UserInfo userInfo = (UserInfo) ctx.get("UserInfo");
        StringBuffer sql = new StringBuffer();
        sql.append(
                " select pmuser.fid from t_pm_user pmuser inner join t_bd_person person on pmuser.fpersonid=person.fid ");
        sql.append(" where pmuser.fnumber='" + userInfo.getNumber() + "'");
        sql.append(" and pmuser.fpersonid ='" + personId + "'");
        String userId = null;
        IRowSet rowSet;
        try {
            rowSet = DbUtil.executeQuery(ctx, sql.toString());
            if (rowSet.size() <= 0) {
                throw new ShrWebBizException("该用户没有对应人员，无法操作系统业务！");
            }
            while (rowSet.next()) {
                userId = rowSet.getString("fid");
            }
        } catch (BOSException e) {
            logger.error("********SHRHelper.getUserIdForPersonId error!", e);
            logger.error("********sql：" + sql.toString());
        } catch (SQLException e) {
            logger.error("********SHRHelper.getUserIdForPersonId error!", e);
            logger.error("********sql：" + sql.toString());
        }
        return userId;
    }

    /**
     * 
     * @Title getAdminOrg
     * @Description 获取职员的行政组织 --远程Web端 (如果有岗 则获取岗位对应行政组织，否则获取其挂靠组织)
     * @Author yacong_liu
     * @param personId 职员内码ID
     * @return 行政组织信息
     * @throws Exception
     */
    public static AdminOrgUnitInfo getAdminOrgUnit(String personId) {
        return getAdminOrgUnit(null, personId);
    }

    /**
     * 
     * @Title getAdminOrg
     * @Description 获取职员的行政组织 --本地服务端 (如果有岗 则获取岗位对应行政组织，否则获取其挂靠组织)
     * @Author yacong_liu
     * @param ctx 上下文
     * @param personId 职员内码ID
     * @return 行政组织信息
     * @throws Exception
     */
    public static AdminOrgUnitInfo getAdminOrgUnit(Context ctx, String personId) {
        EntityViewInfo evi = new EntityViewInfo();
        FilterInfo fi = new FilterInfo();
        fi.getFilterItems().add(new FilterItemInfo("person.id", personId));
        fi.getFilterItems().add(new FilterItemInfo("isPrimary", new Integer(1)));
        SelectorItemCollection sic = new SelectorItemCollection();
        sic.add(new SelectorItemInfo("id"));
        sic.add(new SelectorItemInfo("position.id"));
        sic.add(new SelectorItemInfo("position.name"));
        sic.add(new SelectorItemInfo("position.adminOrgUnit.id"));
        sic.add(new SelectorItemInfo("position.adminOrgUnit.name"));
        evi.setFilter(fi);
        evi.setSelector(sic);

        PositionMemberCollection pColl = null;
        try {
            pColl = iPositionMember(ctx).getPositionMemberCollection(evi);
        } catch (BOSException e) {
            logger.error("********SHRHelper.getAdminOrg_getPositionMemberCollection is error!", e);
        }
        if (pColl != null && pColl.size() > 0) {
            return pColl.get(0).getPosition().getAdminOrgUnit();
        } else {
            EntityViewInfo view = new EntityViewInfo();
            FilterInfo filter = new FilterInfo();
            filter.getFilterItems().add(new FilterItemInfo("person.id", personId));
            SelectorItemCollection selector = new SelectorItemCollection();
            selector.add(new SelectorItemInfo("id"));
            selector.add(new SelectorItemInfo("gkAdmin.id"));
            selector.add(new SelectorItemInfo("gkAdmin.name"));
            view.setFilter(filter);
            view.setSelector(selector);

            PersonPositionCollection psColl = null;
            try {
                psColl = iPersonPosition(ctx).getPersonPositionCollection(view);
            } catch (BOSException e) {
                logger.error("********SHRHelper.getAdminOrg_getPersonPositionCollection is error!", e);
            }
            return (psColl != null && psColl.size() > 0) ? psColl.get(0).getGkAdmin() : null;
        }
    }

    /**
     * 
     * @Title getPersonDetailInfo
     * @Description 获取员工信息（内码，姓名，编码，员工类型，人员类型） --远程Web端
     * @Author yacong_liu
     * @param personId 员工内码
     * @return 员工信息
     * @throws EASBizException
     * @throws BOSException
     */
    public static PersonInfo getLocalPersonDetailInfo(String personId) {
        return getPersonDetailInfo(null, personId);
    }

    /**
     * 
     * @Title getPersonDetailInfo
     * @Description 获取员工信息（内码，姓名，编码，员工类型，人员类型） --本地服务端
     * @Author yacong_liu
     * @param ctx 上下文
     * @param personId 员工内码
     * @return 员工信息
     * @throws EASBizException
     * @throws BOSException
     */
    public static PersonInfo getPersonDetailInfo(Context ctx, String personId) {
        SelectorItemCollection sic = new SelectorItemCollection();
        sic.add(new SelectorItemInfo("employeeType.id"));
        sic.add(new SelectorItemInfo("employeeType.name"));
        sic.add(new SelectorItemInfo("employeeType.number"));
        sic.add(new SelectorItemInfo("employeeClassify.id"));
        sic.add(new SelectorItemInfo("id"));
        sic.add(new SelectorItemInfo("name"));
        sic.add(new SelectorItemInfo("number"));

        try {
            return iPerson(ctx).getPersonInfo(new ObjectUuidPK(personId), sic);
        } catch (EASBizException e) {
            logger.error("********SHRHelper.getPersonDetailInfo_getPersonInfo is error!", e);
        } catch (BOSException e) {
            logger.error("********SHRHelper.getPersonDetailInfo_getPersonInfo is error!", e);
        }

        return null;
    }

    /**
     * 
     * @Title checkStateCanDelete
     * @Description 校验单据是否可删除
     * @Author yacong_liu
     * @param bizInterface 单据接口实例
     * @param bizObjId 单据内码 （多个内码用,间隔）
     * @throws SHRWebException 只能删除未提交状态的单据，请重新选择!
     */
    public static void checkStateCanDelete(ICoreBase bizInterface, String bizObjId) throws SHRWebException {

        if (!checkBillCanDelete(bizInterface, bizObjId)) {
            throw new SHRWebException("只能删除未提交状态的单据，请重新选择！");
        }
    }

    /**
     * 
     * @Title checkBillCanDelete
     * @Description 校验单据是否可删除
     * @Author yacong_liu
     * @param bizInterface 单据接口实例
     * @param bizObjId 单据内码 （多个内码用,间隔）
     * @return true 可删除 false 不可删除
     */
    public static boolean checkBillCanDelete(ICoreBase bizInterface, String bizObjId) {

        if (StringUtil.isNotEmpty(bizObjId)) {
            String[] ids = StringUtil.split(bizObjId, ",");
            HashSet<String> set = new HashSet<String>(ids.length);
            for (int i = 0; i < ids.length; i++) {
                set.add(ids[i]);
            }

            FilterInfo filter = new FilterInfo();
            filter.getFilterItems().add(new FilterItemInfo("id", set, CompareType.INCLUDE));
            filter.getFilterItems().add(new FilterItemInfo("billState", new Integer(3)));
            filter.getFilterItems().add(new FilterItemInfo("billState", new Integer(4)));
            filter.getFilterItems().add(new FilterItemInfo("billState", new Integer(1)));
            filter.getFilterItems().add(new FilterItemInfo("billState", new Integer(2)));
            filter.setMaskString("#0 and (#1 or #2 or #3 or #4)");

            try {
                return !bizInterface.exists(filter);
            } catch (EASBizException e) {
                logger.error("********SHRHelper.checkBillCanDelete_exists is error!", e);
            } catch (BOSException e) {
                logger.error("********SHRHelper.checkBillCanDelete_exists is error!", e);
            }
        }

        return false;
    }

    /**
     * 
     * @Title getCurrPersonInfoNew
     * @Description 获取当前用户信息 (该用户需有关联职员账户)
     * @Author yacong_liu
     * @param ctx 上下文
     * @return 用户信息 如果该用户没有关联的职员账户会抛出异常提示：该用户没有对应人员，无法操作系统业务！
     * @throws ShrWebBizException 该用户没有对应人员，无法操作系统业务！
     */
    public static PersonInfo getCurrPersonInfoNew(Context ctx) throws ShrWebBizException {
        UserInfo userInfo = (UserInfo) ctx.get("UserInfo");
        StringBuffer sql = new StringBuffer();
        sql.append(
                " select pmuser.fid from t_pm_user pmuser inner join t_bd_person person on pmuser.fpersonid=person.fid ");
        sql.append(" where pmuser.fnumber='" + userInfo.getNumber() + "'");
        sql.append(" and pmuser.fpersonid is not null");

        IRowSet rowSet;
        try {
            rowSet = DbUtil.executeQuery(ctx, sql.toString());
            if (rowSet.size() <= 0) {
                throw new ShrWebBizException("该用户没有对应人员，无法操作系统业务！");
            }
        } catch (BOSException e) {
            logger.error("********SHRHelper.getCurrPersonInfoNew_executeQueryis error!", e);
            logger.error("********sql：" + sql.toString());
        }

        return getCurrPersonInfo(ctx);

    }

    /**
     * 
     * @Title getCurrPersonInfo
     * @Description 获取当前登录用户信息
     * @Author yacong_liu
     * @param ctx 上下文
     * @return 当前登录用户信息
     */
    public static PersonInfo getCurrPersonInfo(Context ctx) {
        return ContextUtil.getCurrentUserInfo(ctx).getPerson();
    }

    /**
     * 
     * @Title getCurrPersonInfo
     * @Description 获取当前登录用户信息
     * @Author yacong_liu
     * @return 当前登录用户信息
     */
    public static PersonInfo getCurrPersonInfo() {
        Context ctx = SHRContext.getInstance().getContext();
        return getCurrPersonInfo(ctx);
    }

    /**
     * 
     * @Title getTableNameByBillId
     * @Description 根据单据内码id获取表名称
     * @Author yacong_liu
     * @param billId 单据内码id
     * @return 表名 (billId 为空时 return null)
     */
    public static String getTableNameByBillId(String billId) {
        if (StringUtils.isNotEmpty(billId)) {
            EntityObjectInfo entity = getEntityObjectInfo(billId);
            return entity.getTable().getName();
        }
        return null;
    }

    /**
     * 
     * @Title getEntityObjectInfo
     * @Description 根据单据内码id获取实体对象EntityObjectInfo
     * @Author yacong_liu
     * @param billId 单据内码
     * @return EntityObjectInfo (billId为空时return null)
     */
    public static EntityObjectInfo getEntityObjectInfo(String billId) {
        if (StringUtil.isNotEmpty(billId)) {
            IMetaDataLoader metadataloader = MetaDataLoaderFactory.getRemoteMetaDataLoader();
            return metadataloader.getEntity(getBosType(billId));
        }
        return null;
    }

    /**
     * 
     * @Title getBosType
     * @Description 根据单据内码id获取实体的BosType
     * @Author yacong_liu
     * @param billId 单据内码id
     * @return BosType (billId 为空时 return null)
     */
    public static BOSObjectType getBosType(String billId) {
        if (StringUtil.isNotEmpty(billId)) {
            BOSUuid uuid = BOSUuid.read(billId);
            return uuid.getType();
        }
        return null;
    }

    /**
     * 
     * @Title iPerson
     * @Description 员工—个人信息实例
     * @Author yacong_liu
     * @param ctx 上下文
     * @return IPerson
     * @throws BOSException
     */
    private static IPerson iPerson(Context ctx) throws BOSException {
        return ctx == null ? PersonFactory.getRemoteInstance() : PersonFactory.getLocalInstance(ctx);
    }

    /**
     * 
     * @Title IPositionMember
     * @Description 职员-任职情况实例
     * @Author yacong_liu
     * @param ctx 上下文
     * @return IPositionMember
     * @throws BOSException
     */
    private static IPositionMember iPositionMember(Context ctx) throws BOSException {
        return ctx == null ? PositionMemberFactory.getRemoteInstance()
                : PositionMemberFactory.getLocalInstance(ctx);
    }

    /**
     * 
     * @Title iPersonPosition
     * @Description 员工-职业信息实例
     * @Author yacong_liu
     * @param ctx 上下文
     * @return IPersonPosition
     * @throws BOSException
     */
    private static IPersonPosition iPersonPosition(Context ctx) throws BOSException {
        return ctx == null ? PersonPositionFactory.getRemoteInstance()
                : PersonPositionFactory.getLocalInstance(ctx);
    }

}
