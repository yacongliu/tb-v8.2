package com.kingdee.eas.hrcus.web.handler.worklist;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.workflow.WfException;
import com.kingdee.bos.workflow.service.EnactmentServiceProxy;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hrcus.worklist.IRedign;
import com.kingdee.eas.hrcus.worklist.RedignFactory;
import com.kingdee.eas.hrcus.worklist.RedignInfo;
import com.kingdee.shr.base.syssetting.app.filter.HRFilterUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.StringUtils;

/**
 * @Copyright 版权所有： 天津金蝶软件有限公司 <br>
 *            Title: RedignEditHandler <br>
 *            Description: 离职工作清单
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-1-2 & 下午03:40:52
 * @since V1.0
 */
public class RedignEditHandler extends EditHandler {

    private Context ctx;

    public RedignEditHandler(Context ctx) {
        this.ctx = ctx;
    }

    public RedignEditHandler() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    @SuppressWarnings("unchecked")
    public void canSubmitEffectAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        Map map = new HashMap();
        map.put("state", "success");
        JSONUtils.writeJson(response, map);

    }

    public void submitEffectAction(HttpServletRequest req, HttpServletResponse res, ModelMap modelMap)
            throws SHRWebException {

        CoreBaseInfo model = (CoreBaseInfo) req.getAttribute("dynamic_model");
        try {
            IRedign iRedign = RedignFactory.getRemoteInstance();
            IObjectPK objectPK = iRedign.submitEffect(model);
            model.setId(BOSUuid.read(objectPK.toString()));
        } catch (EASBizException e) {
            throw new SHRWebException(e.getMessage());
        } catch (Exception e) {
            throw new ShrWebBizException(e.getMessage());
        }

        writeSuccessData(model.getId().toString());
    }

    protected void afterCreateNewModel(HttpServletRequest request, HttpServletResponse response,
            CoreBaseInfo coreBaseInfo) throws SHRWebException {
        // TODO 初始化页面
        super.afterCreateNewModel(request, response, coreBaseInfo);
    }

    protected void beforeSave(HttpServletRequest request, HttpServletResponse response, CoreBaseInfo model)
            throws SHRWebException {
        super.beforeSave(request, response, model);

        RedignInfo bill = (RedignInfo) model;
        bill.setBillState(HRBillStateEnum.SAVED);
    }

    protected void beforeSubmit(HttpServletRequest request, HttpServletResponse response, CoreBaseInfo model)
            throws SHRWebException {
        super.beforeSubmit(request, response, model);

        RedignInfo bill = (RedignInfo) model;
        String userID = HRFilterUtils.getCurrentUserId(ctx);
        String functionName = "com.kingdee.eas.hrcus.worklist.app.RedignFunction";
        String operationName = "actionSubmit";
        try {
            String temp = EnactmentServiceProxy.getEnacementService(ctx).findSubmitProcDef(userID, bill,
                    functionName, operationName);
            if ((temp == null) || (temp.trim().equals("")))
                throw new ShrWebBizException("没有可用的离职工作清单工作流");
        } catch (WfException e) {
            throw new SHRWebException(e.getMessage());
        } catch (BOSException e) {
            throw new SHRWebException(e.getMessage());
        }

        bill.setBillState(HRBillStateEnum.SUBMITED);
        String operateStatus = request.getParameter("operateState");
        if ((StringUtils.isEmpty(operateStatus)) || (!("ADDNEW".equalsIgnoreCase(operateStatus)))) {
            return;
        }
        bill.setExtendedProperty("isAddNew", "isAddNew");
    }

}
