package com.kingdee.shr.custom.osf.atsleave;

import java.sql.SQLException;
import java.util.Map;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.bsf.service.app.IHRMsfService;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.human.AssessmentIndexsFactory;
import com.kingdee.eas.custom.human.AssessmentIndexsInfo;
import com.kingdee.eas.fi.gl.util.StringUitls;
import com.kingdee.eas.hr.perf.KindEnum;
import com.kingdee.eas.hr.perf.PerformTargetItemFactory;
import com.kingdee.eas.hr.perf.PerformTargetItemInfo;
import com.kingdee.eas.hr.perf.PerformTargetTypeInfo;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;

/**
 * 
 * Title: itemService Description:新增网页和客户端考试指标库信息OSF
 * 
 * @author 宋佩骏 Email:tjsongpeijun@kingdee.com
 * @date 2019年8月23日
 */
public class itemService implements IHRMsfService {

	static String EvaluationCriterion = null;// 评价标准
	static Boolean Status = null;// 是否启用
	static String number = null;// 指标编号
	static String name = null;// 指标名称
	static String typeid = null;// 考核标准
	static String adminOrgUnit = null;// 所属组织id
	@SuppressWarnings("rawtypes")
	@Override
	public Object process(Context ctx, Map param) throws EASBizException, BOSException {
		/**
		 * 新增网页和客户端考试指标库信息
		 */
		EvaluationCriterion = (String)param.get("EvaluationCriterion");
	    Status = (Boolean)param.get("isStatus");
	    number = (String)param.get("Number");
	    name = (String)param.get("Name");
	    typeid = (String)param.get("TypeID");
		adminOrgUnit = (String) param.get("adminid");
		try {
			//保存客户端考核指标库信息
			savePerformTargetItemInfo(ctx);
			//保存网页端考核指标库信息
			saveAssessmentIndexsInfo(ctx);
		} catch (ShrWebBizException e) {
			throw new BOSException("新增网页和客户端考试指标库信息失败" + e);
		} catch (SQLException e) {
			throw new BOSException("新增网页和客户端考试指标库信息失败" + e);
		}

		return null;
	}

	/**
	 * <p>
	 * Title: savePerformTargetItemInfo
	 * </p>
	 * <p>
	 * Description: 保存客户端考核指标库信息
	 * </p>
	 * @param ctx
	 * @throws EASBizException
	 * @throws BOSException
	 * @throws ShrWebBizException
	 */
	public static void savePerformTargetItemInfo(Context ctx)
			throws EASBizException, BOSException, ShrWebBizException, SQLException {

		PerformTargetItemInfo performTargetItemInfo = new PerformTargetItemInfo();
		performTargetItemInfo.setStandard(EvaluationCriterion);// 评价标准
		performTargetItemInfo.setEnable(Status);// 是否启用
		performTargetItemInfo.setContent(name);// 考核名称
		performTargetItemInfo.setNumber(number);// 考核编码
		performTargetItemInfo.setKind(KindEnum.QUALITATIVE);//定性
		PerformTargetTypeInfo targetTypeInfo = new PerformTargetTypeInfo();
		targetTypeInfo.setId(BOSUuid.read(typeid));
		if (targetTypeInfo == null || targetTypeInfo.size() == 0) {
			throw new ShrWebBizException("考核标准不能为空!");
		}
		performTargetItemInfo.setType(targetTypeInfo);
		StringBuffer selectsql = new StringBuffer();
		//通过能否查询到id判断是新增还是修改
		selectsql.append("select fid id from T_PF_PERFITEM where fnumber = '").append(number).append("'");
		IRowSet row = DbUtil.executeQuery(ctx, selectsql.toString());
		String count = null;
		while (row.next()) {
			count = row.getString("id");
		}
		if (StringUitls.stringIsNull(count)) {
			PerformTargetItemFactory.getRemoteInstance().save(performTargetItemInfo);
		} else {
			performTargetItemInfo.setNumber(null);// 考核名称
			PerformTargetItemFactory.getRemoteInstance().update(new ObjectUuidPK(count), performTargetItemInfo);
		}
	}

	/**
	 * <p>
	 * Title: saveAssessmentIndexsInfo
	 * </p>
	 * <p>
	 * Description:保存网页端考核指标库信息
	 * </p>
	 * @param ctx
	 * @throws EASBizException
	 * @throws BOSException
	 * @throws ShrWebBizException
	 */
	public static void saveAssessmentIndexsInfo(Context ctx)
			throws EASBizException, BOSException, ShrWebBizException, SQLException {
		
		AssessmentIndexsInfo assessmentIndexsInfo = new AssessmentIndexsInfo();
		assessmentIndexsInfo.setNumber(number);// 考核编码
		assessmentIndexsInfo.setName(name);// 考核名称
		assessmentIndexsInfo.setEvaluationCriterion(EvaluationCriterion);// 评价标准
		assessmentIndexsInfo.setStatus(Status);// 是否启用
		AdminOrgUnitInfo adminOrgUnitInfo = new AdminOrgUnitInfo();
		adminOrgUnitInfo.setId(BOSUuid.read(adminOrgUnit));
		if (adminOrgUnitInfo == null || adminOrgUnitInfo.size() == 0) {
			throw new ShrWebBizException("所属组织不能为空!");
		}
		assessmentIndexsInfo.setSubordinate(adminOrgUnitInfo);
		PerformTargetTypeInfo targetTypeInfo = new PerformTargetTypeInfo();
		targetTypeInfo.setId(BOSUuid.read(typeid));
		if (targetTypeInfo == null || targetTypeInfo.size() == 0) {
			throw new ShrWebBizException("考核标准不能为空!");
		}
		assessmentIndexsInfo.setTargetType(targetTypeInfo);
		//通过能否查询到id判断是新增还是修改
		StringBuffer selectsql = new StringBuffer();
		selectsql.append("select fid id from CT_MAN_AssessmentIndexs where fnumber = '").append(number).append("'");
		IRowSet row = DbUtil.executeQuery(ctx, selectsql.toString());
		String count = null;
		while (row.next()) {
			count = row.getString("id");
		}
		if (StringUitls.stringIsNull(count)) {
			AssessmentIndexsFactory.getRemoteInstance().save(assessmentIndexsInfo);
		} else {
			AssessmentIndexsFactory.getRemoteInstance().update(new ObjectUuidPK(count), assessmentIndexsInfo);
		}
	}

}
