package com.kingdee.shr.custom.osf.atsleave;

import java.sql.SQLException;
import java.util.Map;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;

import com.kingdee.bos.bsf.service.app.IHRMsfService;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.fi.gl.util.StringUitls;
import com.kingdee.eas.util.app.DbUtil;

/**
 * 
 * Title: updateItemService
 * Description: 同步修改客户端和网页端考核指标库信息osf
 * @author 宋佩骏 Email:tjsongpeijun@kingdee.com
 * @date 2019年8月24日
 */
public class updateItemService implements IHRMsfService{
	static String EvaluationCriterion = null;//评价标准
	static Boolean Status =null;//是否启用
	static String number = null;//指标编号
	@Override
	public Object process(Context ctx, @SuppressWarnings("rawtypes") Map param) throws EASBizException, BOSException {
		/**
		 * 同步修改客户端和网页端考核指标库信息
		 */
		EvaluationCriterion = (String) param.get("EvaluationCriterion");//评价标准
		Status =(Boolean) param.get("isStatus");//是否启用
		number = (String) param.get("Number");//指标编号
		if(StringUitls.stringIsNull(number)) {
			throw new BOSException("编号不能为空" );
		}
		try {
			//修改网页端考核指标库
			updateAssessmentIndexs(ctx);
			//修改客户端考核指标库
			updatePerformTargetItemInfo(ctx);
		} catch (SQLException e) {
			throw new BOSException("维护网页和客户端考试指标库信息失败" + e);
		}
		
		return null;
		
	}
	/**
	 * 
	 * <p>Title: updateAssessmentIndexs</p>
	 * <p>Description:修改网页端考核指标库信息 </p>
	 * @param ctx
	 * @throws EASBizException
	 * @throws BOSException
	 * @throws SQLException 
	 */
	public static void updateAssessmentIndexs(Context ctx) throws EASBizException, BOSException, SQLException {
		int a = 0;
		if(Status) {
			a = 1;
		}
		StringBuffer sql = new StringBuffer();
		sql.append("update  CT_MAN_AssessmentIndexs set CFEvaluationCriterion = '").append(EvaluationCriterion).append("',CFStatus = ").append(a).append(" where fnumber = '").append(number).append("'");
		DbUtil.execute(ctx,sql.toString());
	}
	/**
	 * 
	 * <p>Title: updatePerformTargetItemInfo</p>
	 * <p>Description: 修改客户端考核指标库信息</p>
	 * @param ctx
	 * @throws EASBizException
	 * @throws BOSException
	 */
	public static void updatePerformTargetItemInfo(Context ctx) throws EASBizException, BOSException {
		int a = 0;
		if(Status) {
			a = 1;
		}
		StringBuffer sql = new StringBuffer();
		sql.append("update T_PF_PERFITEM set fenable = ").append(a).append(",fstandard2 = '").append(EvaluationCriterion).append("' where fnumber = '").append(number).append("'");
		DbUtil.execute(ctx,sql.toString());
	}

}
