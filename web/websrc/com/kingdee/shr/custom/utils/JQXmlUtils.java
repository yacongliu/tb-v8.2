package com.kingdee.shr.custom.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.kingdee.bos.BOSException;
import com.kingdee.shr.compensation.util.common.StringUtil;

/**
 * 
 * Title: JQXmlUtils
 * Description:调用久其接口的XML相关操作的工具类
 * 
 * @author 仝飞 Email:tjtongfei@kingdee.com
 * @date 2019年9月10日
 */
public class JQXmlUtils {
	public static final Logger log = Logger.getLogger(JQXmlUtils.class);

	// 久其接口默认访问地址
	public static final String JiuQiURL = "http://60.28.230.140:9799/dna_ws/ImpBillWebService"; 
	public static final String JiuQiClass = "ImpBillWebServerSoapBinding";
	public static final String JiuQiMethod = "parseStrWithAuthResponse";
	// 久其接口默认用户密码
	public static final String userName = "jq_hkw";
	public static final String userPWD = "5CBFEC3C943B0DC19E8288474B330438";

	
	/**
	 * <p>
	 * Title: parseSOAPResult
	 * </p>
	 * <p>
	 * Description: 将久其薪酬、社保、年金、公积金接口返回的xml解析成结果映射的方法
	 * </p>
	 * @param soapResultXML
	 * @param callChain
	 * @return HashMap<String,String>类型的结果映射，键值对有
	 * ----------主要值----------
	 * isSuccess：执行成功为"true"，执行失败为"false"||
	 * msg：执行结果的主要说明||
	 * xml：久其接口的返回报文
	 * ----------备用值----------
	 * dataID：单据标识||
	 * billCode：久其系统中的单据编号||
	 * rtnCode：久其系统执行结果代码||
	 * rtnMsg：久其系统执行结果说明
	 * @throws DocumentException 
	 * @throws MalformedURLException 
	 */
	public static Map<String,String> parseSOAPResult(String soapResultXML, StringBuffer callChain) throws MalformedURLException, DocumentException {
		if(callChain==null)callChain=new StringBuffer();
		callChain.append("将久其薪酬接口返回的xml解析成结果映射的方法：");
		Map<String,String> resultMap = new HashMap<String, String>();
		resultMap.put("isSuccess", "false");
		resultMap.put("msg","久其soap接口返回内容解析失败");
		resultMap.put("xml","久其soap接口返回内容解析失败");
		
		//先从外层soap报文中解析出结果对象xml内容
		String operResultXML="";
        SAXReader reader = new SAXReader();
        InputStream in = new   ByteArrayInputStream(soapResultXML.getBytes());
        Document document = reader.read(in);
        // 通过document对象获取根节点bookstore
        Element root = document.getRootElement();
        // 通过element对象的elementIterator方法获取迭代器
        Iterator it = root.elementIterator();

	/*
 	失败情况
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
				<ns2:parseStrWithAuthResponse xmlns:ns2="http://jiuqi.com.cn">
					<return>
		&lt;?xml version='1.0' encoding='UTF-8'?&gt;&lt;returninfo&gt;&lt;dataID&gt;100001&lt;/dataID&gt;&lt;billCode&gt;&lt;/billCode&gt;&lt;rtnCode&gt;40&lt;/rtnCode&gt;&lt;rtnMsg&gt;操作结果为：合法性错误标签&quot;FO_PAYPLAN\STAFF&quot;。详细信息是：STAFF[ZY-005428]未找到对应的基础数据&lt;/rtnMsg&gt;&lt;/returninfo&gt;
					</return>
				</ns2:parseStrWithAuthResponse>
			</soap:Body>
		</soap:Envelope>
		
	成功情况		
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
				<ns2:parseStrWithAuthResponse xmlns:ns2="http://jiuqi.com.cn">
					<return>
		&lt;?xml version='1.0' encoding='UTF-8'?&gt;&lt;returninfo&gt;&lt;dataID&gt;100002&lt;/dataID&gt;&lt;billCode&gt;ZGXC201909TBZY0018&lt;/billCode&gt;&lt;rtnCode&gt;10&lt;/rtnCode&gt;&lt;rtnMsg&gt;操作结果为：单据已经执行完成&lt;/rtnMsg&gt;&lt;/returninfo&gt;
					</return>
				</ns2:parseStrWithAuthResponse>
			</soap:Body>
		</soap:Envelope>
		
		*/
        if(it.hasNext()) {
        	String rootData = root.getData().toString();
        	Element soapEnv = (Element) it.next();
        	Iterator itEnv = soapEnv.elementIterator();
        	if(itEnv.hasNext()) {
        		String envData = soapEnv.getData().toString();
        		Element soapBody = (Element) itEnv.next();
        		Iterator itBody = soapBody.elementIterator();
        		if(itBody.hasNext()) {
        			String bodyData = soapBody.getData().toString();
        			Element ns2 = (Element) itBody.next();
        			//这时才得到内层结果xml文字
        			operResultXML = ns2.getData().toString();
        		}
        	}
        }
		resultMap.put("xml",operResultXML);
        
        //再从内容结果xml内容中解析出操作结果数据
        if(StringUtil.isNullOrEmpty(operResultXML)) {
        	return resultMap;
        }
        reader = null;
        reader = new SAXReader();
        in = null;
        in = new ByteArrayInputStream(operResultXML.getBytes());
        document = null;
        document = reader.read(in);
        root = null;
        root = document.getRootElement();
        /*
	成功情况：
        <?xml version="1.0" encoding="utf-8"?>
		<returninfo>
		  <dataID>100003</dataID>
		  <billCode>ZGXC201909TBZY0019</billCode>
		  <rtnCode>10</rtnCode>
		  <rtnMsg>操作结果为：单据已经执行完成</rtnMsg>
		</returninfo>
	失败情况：
		<?xml version="1.0" encoding="utf-8"?>
		<returninfo>
		  <dataID>100002</dataID>
		  <billCode/>
		  <rtnCode>60</rtnCode>
		  <rtnMsg>操作结果为：单据重复发送错误。详细信息是：存入数据库时，存在相同报文</rtnMsg>
		</returninfo>
        */
        it = null;
        it = root.elementIterator();
        if(it.hasNext()) {
        	Element dataID = root.element("dataID");
        	String dataIDStr = toStringIfGetDataNotNull(dataID, callChain);
        	Element jqBillCode = root.element("billCode");
        	String jqBillCodeStr = toStringIfGetDataNotNull(jqBillCode, callChain);
			Element rtnCode = root.element("rtnCode");
        	String rtnCodeStr = toStringIfGetDataNotNull(rtnCode, callChain);
			Element rtnMsg = root.element("rtnMsg");
        	String rtnMsgStr = toStringIfGetDataNotNull(rtnMsg, callChain);
        	
        	resultMap.put("dataID",dataIDStr.trim());
        	resultMap.put("billCode",jqBillCodeStr.trim());
        	resultMap.put("rtnCode",rtnCodeStr.trim());
        	resultMap.put("rtnMsg",rtnMsgStr.trim());
        }else {
        	resultMap.put("msg","久其接口xml返回值解析失败：未找到元素");
        }
        String rtnCode = resultMap.get("rtnCode");
        if(!StringUtil.isNullOrEmpty(rtnCode)&&"10".equals(rtnCode)) {
        	resultMap.put("isSuccess", "true");
        }
        String rtnMsg = resultMap.get("rtnMsg");
        if(!StringUtil.isNullOrEmpty(rtnMsg)) {
        	resultMap.put("msg",resultMap.get("rtnMsg"));
        }
		return resultMap;
	}
	
	/**
	 * <p>
	 * Title: toStringIfGetDataNotNull
	 * </p>
	 * <p>
	 * Description: 在确保不会出现空指针异常的情况下提取xml元素elm的文字内容返回若null则“”的私有静态方法
	 * </p>
	 * @param elm
	 * @param callChain
	 * @return
	 */
	private static String toStringIfGetDataNotNull(Element elm, StringBuffer callChain) {
		if(callChain==null) callChain = new StringBuffer();
		callChain.append("在确保不会出现空指针异常的情况下提取xml元素elm的文字内容返回若null则“”的私有静态方法：");
		if(elm==null) {
			String toLog = callChain.append("xml元素elm为null，无法读取文字内容").toString();
			log.error(toLog);
			System.out.println(toLog);
			return "";
		}
		
		Object dataObj = elm.getData();
		if(dataObj==null) {
			String toLog = callChain.append("xml元素elm的data内容为null，无法读取文字内容").toString();
			log.error(toLog);
			System.out.println(toLog);
			return "";
		}
		String dataStr = dataObj.toString();
		return dataStr;
	}
}
