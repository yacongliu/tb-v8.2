package com.kingdee.shr.custom.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.enterprisedt.util.debug.Logger;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.dao.query.SQLExecutor;
import com.kingdee.eas.basedata.hraux.DegreeFactory;
import com.kingdee.eas.basedata.hraux.DegreeInfo;
import com.kingdee.eas.basedata.hraux.DiplomaFactory;
import com.kingdee.eas.basedata.hraux.DiplomaInfo;
import com.kingdee.eas.basedata.hraux.IDegree;
import com.kingdee.eas.basedata.hraux.IDiploma;
import com.kingdee.eas.basedata.hraux.IPoliticalFace;
import com.kingdee.eas.basedata.hraux.PoliticalFaceFactory;
import com.kingdee.eas.basedata.hraux.PoliticalFaceInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.EduTypeFactory;
import com.kingdee.eas.hr.base.EduTypeInfo;
import com.kingdee.eas.hr.base.IEduType;
import com.kingdee.eas.hr.base.IRelation;
import com.kingdee.eas.hr.base.ITechnicalPost;
import com.kingdee.eas.hr.base.RelationFactory;
import com.kingdee.eas.hr.base.RelationInfo;
import com.kingdee.eas.hr.base.TechnicalPostFactory;
import com.kingdee.eas.hr.base.TechnicalPostInfo;
import com.kingdee.eas.hr.emp.IPersonDegree;
import com.kingdee.eas.hr.emp.IPersonFamily;
import com.kingdee.eas.hr.emp.IPersonRewardPunish;
import com.kingdee.eas.hr.emp.IPersonTechnicalPost;
import com.kingdee.eas.hr.emp.PersonDegreeCollection;
import com.kingdee.eas.hr.emp.PersonDegreeFactory;
import com.kingdee.eas.hr.emp.PersonDegreeInfo;
import com.kingdee.eas.hr.emp.PersonFamilyCollection;
import com.kingdee.eas.hr.emp.PersonFamilyFactory;
import com.kingdee.eas.hr.emp.PersonFamilyInfo;
import com.kingdee.eas.hr.emp.PersonRewardPunishCollection;
import com.kingdee.eas.hr.emp.PersonRewardPunishFactory;
import com.kingdee.eas.hr.emp.PersonRewardPunishInfo;
import com.kingdee.eas.hr.emp.PersonTechnicalPostFactory;
import com.kingdee.eas.hr.emp.PersonTechnicalPostInfo;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.StringUtils;

/**
 * @copyright 天津金蝶软件有限公司 <br>
 *            Title: PersonDataUtils <br>
 *            Description: 员工数据工具类
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-3-22 & 下午07:05:24
 * @since V1.0
 */
public class PersonDataUtils {

	/**
	 * 20190923：仝飞：为了在不调试天保正式服务环境的情况下分析天保正式服务环境与测试服务环境的差异
	 * 只能把调试信息输出到日志中
	 */
	private static Logger log = Logger.getLogger(PersonDataUtils.class);
	
	/**
	 * 
	 * <p>
	 * Title: getRewardPunish
	 * </p>
	 * <p>
	 * Description: 获取员工奖惩信息（本地）
	 * </p>
	 * 
	 * @param personId
	 * @return List<Map<String, Object>> Map key:occurDate 奖惩时间 key:content 奖惩内容
	 * @throws BOSException
	 */
	public static List<Map<String, Object>> getRewardPunish(String personId)
			throws BOSException {
		return getRewardPunish(null, personId);
	}

	/**
	 * 
	 * <p>
	 * Title: getPersonFamily
	 * </p>
	 * <p>
	 * Description: 获取员工社会关系 (本地)
	 * </p>
	 * 
	 * @param personId
	 * @return List<Map<String, Object>> Map key：name 姓名 birthday 出生日期 workUnit
	 *         工作单位 relation 员工关系 politicalFace 政治面貌
	 * @throws BOSException
	 */
	public static List<Map<String, Object>> getPersonFamily(String personId)
			throws BOSException {
		return getPersonFamily(null, personId);
	}

	/**
	 * 
	 * <p>
	 * Title: getPersonDegreeByPersonId
	 * </p>
	 * <p>
	 * Description: 根据员工内码获取员工教育经历 (本地)
	 * </p>
	 * 
	 * @param personId
	 * @return Map[] index = 0 : 在职教育经历 index = 1 : 全日制教育经历
	 * @throws BOSException
	 */
	@SuppressWarnings("unchecked")
	public static Map[] getPersonDegreeByPersonId(String personId)
			throws BOSException {
		return getPersonDegreeByPersonId(null, personId);
	}

	/**
	 * 
	 * <p>
	 * Title: getPersonFamily
	 * </p>
	 * <p>
	 * Description: 获取员工社会关系 (服务端)
	 * </p>
	 * 
	 * @param ctx
	 * @param personId
	 * @return List<Map<String, Object>> Map key：name 姓名 birthday 出生日期 workUnit
	 *         工作单位 relation 员工关系 politicalFace 政治面貌
	 * @throws BOSException
	 */
	public static List<Map<String, Object>> getPersonFamily(Context ctx,
			String personId) throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}

		IPersonFamily instance;
		if (ctx == null) {
			instance = PersonFamilyFactory.getRemoteInstance();
		} else {
			instance = PersonFamilyFactory.getLocalInstance(ctx);
		}

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(6);
		/*
		 * @update 20190429 yacong_liu 经与PM 王琨确定 社会关系需要排序 排序规则为社会关系编码
		 */
		String oql = " where person.id = '" + personId + "' order by relation.number";
		PersonFamilyCollection coll = instance.getPersonFamilyCollection(oql);
		if (coll != null && coll.size() > 0) {
			for (int i = 0; i < coll.size(); i++) {
				PersonFamilyInfo info = coll.get(i);
				Map<String, Object> map = new HashMap<String, Object>(5);
				// 姓名
				String name = info.getName();
				// 出生日期
				Date birthday = info.getBirthday();
				// 工作单位
				String workUnit = info.getWorkUnit();
				// 关系
				String relation = "";
				// 政治面貌
				String politicalFace = "";
				if (info.getRelation() != null) {
					relation = getRelationNameById(ctx, info.getRelation()
							.getId().toString());
				}
				if (info.getPoliticalFace() != null) {
					politicalFace = getPoliticalFaceNameById(ctx, info
							.getPoliticalFace().getId().toString());
				}

				map.put("name", name);
				map.put("birthday", birthday);
				map.put("workUnit", workUnit);
				map.put("relation", relation);
				map.put("politicalFace", politicalFace);

				list.add(map);
			}
		}

		return list;

	}

	/**
	 * 
	 * <p>
	 * Title: getTechnicalPost
	 * </p>
	 * <p>
	 * Description: 根据员工内码获取员工职称信息 (本地)
	 * </p>
	 * 
	 * @param personId
	 * @return 职称
	 * @throws BOSException
	 */
	public static String getTechnicalPostByPersonId(String personId)
			throws BOSException {
		return getTechnicalPostByPersonId(null, personId);
	}

	/**
	 * 
	 * <p>
	 * Title: getNDKH
	 * </p>
	 * <p>
	 * Description: 获取员工年度考核信息
	 * </p>
	 * 
	 * @param personId
	 * @return List<Map<String, String>> Map key: year 考核年度 key: res 考核结果
	 * @throws BOSException
	 */
	public static List<Map<String, String>> getNDKH(String personId)
			throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}

		String sql = " select cfkhjg as res, cfnd as year from ct_mp_ndkh where fpersonid = '"
				+ personId + "'";

		SQLExecutor sec = new SQLExecutor(sql);
		IRowSet rowSet = sec.executeSQL();

		List<Map<String, String>> list = new ArrayList<Map<String, String>>(8);
		try {
			while (rowSet.next()) {
				Map<String, String> map = new HashMap<String, String>(2);
				// 考核年度
				String year = rowSet.getString("year");
				// 考核结果
				String res = rowSet.getString("res");

				map.put("year", year);
				map.put("res", res);

				list.add(map);

			}
		} catch (SQLException e) {
			System.out.println("********获取员工年度考核信息Error! SQL: " + sql
					+ " Exception：" + e.toString());
		}

		return list;

	}

	/**
	 * 
	 * <p>
	 * Title: getRewardPunish
	 * </p>
	 * <p>
	 * Description: 获取员工奖惩信息（服务端）
	 * </p>
	 * 
	 * @param ctx
	 * @param personId
	 * @return List<Map<String, Object>> Map key:occurDate 奖惩时间 key:content 奖惩内容
	 * @throws BOSException
	 */
	public static List<Map<String, Object>> getRewardPunish(Context ctx,
			String personId) throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}
		IPersonRewardPunish iPersonRewardPunish;
		if (ctx == null) {
			iPersonRewardPunish = PersonRewardPunishFactory.getRemoteInstance();
		} else {
			iPersonRewardPunish = PersonRewardPunishFactory
					.getLocalInstance(ctx);
		}

		String oql = " where person.id = '" + personId + "'";
		PersonRewardPunishCollection coll = iPersonRewardPunish
				.getPersonRewardPunishCollection(oql);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(7);
		if (coll != null && coll.size() > 0) {
			for (int i = 0; i < coll.size(); i++) {
				PersonRewardPunishInfo info = coll.get(i);
				Map<String, Object> map = new HashMap<String, Object>(2);
				// 奖惩时间
				Date occurDate = info.getOccurDate();
				// 奖惩内容
				String content = info.getContent();

				map.put("occurDate", occurDate);
				map.put("content", content);

				list.add(map);
			}

		}

		return list;

	}

	/**
	 * 
	 * <p>
	 * Title: getNowAndFatureJobByPersonId
	 * </p>
	 * <p>
	 * Description: 获取员工的现任职务和拟任职务
	 * </p>
	 * 
	 * @param personId
	 * @return Map<String, String> key：fatureJob 拟任职务 key:nowJob 现任职务
	 * @throws BOSException
	 */
	public static Map<String, String> getNowAndFatureJobByPersonId(
			String personId) throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}

		Map<String, String> map = new HashMap<String, String>(2);
		// 任职简历
		LinkedList<Map<String, Object>> vitae = getVitaeByPersonId(personId);
		/*
		 * @desc 经与PM确认，任职简历结束时间最近的一条为拟任职务，倒数第二条为现任职务
		 */
		String fatureJob = ""; // 拟任职务
		String nowJob = ""; // 现任职务
		if (vitae != null && vitae.size() >= 2) {

			Map<String, Object> fatureMap = vitae.get(vitae.size() - 1);
			Map<String, Object> nowMap = vitae.get(vitae.size() - 2);

			fatureJob = (String) fatureMap.get("vita");
			nowJob = (String) nowMap.get("vita");

		} else if (vitae.size() == 1) {
			Map<String, Object> nowMap = vitae.get(0);
			nowJob = (String) nowMap.get("vita");

		}

		map.put("fatureJob", fatureJob);
		map.put("nowJob", nowJob);

		return map;

	}

	/**
	 * 
	 * <p>
	 * Title: getNowJobByPersonId
	 * </p>
	 * <p>
	 * Description: 获取员工的现任职务（任职简历）
	 * </p>
	 * 
	 * @param personId
	 * @return 现任职务
	 * @throws BOSException
	 */
	public static String getNowJobByPersonId(String personId)
			throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return "";
		}

		// 任职简历
		LinkedList<Map<String, Object>> vitae = getVitaeByPersonId(personId);
		if (vitae != null && vitae.size() > 0) {
			Map<String, Object> map = vitae.getLast();
			// 现任职务
			return (String) map.get("vita");
		}

		return "";

	}

	/**
	 * 
	 * <p>
	 * Title: getVitaeByPersonId
	 * </p>
	 * <p>
	 * Description: 根据员工内码获取其任职简历
	 * </p>
	 * 
	 * @param personId
	 * @return LinkedList<Map<String, Object>> Map key:begintime key:endtime
	 *         key:vita 简历
	 * @throws BOSException
	 */
	public static LinkedList<Map<String, Object>> getVitaeByPersonId(
			String personId) throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}
		String sql = getVitaSQLByPersonId(personId);

		SQLExecutor sec = new SQLExecutor(sql);

		IRowSet rowSet = sec.executeSQL();
		LinkedList<Map<String, Object>> list = new LinkedList<Map<String, Object>>();
		try {
			while (rowSet.next()) {
				Map<String, Object> map = new HashMap<String, Object>(3);
				Date begintime = rowSet.getDate("begintime");
				Date endtime = rowSet.getDate("endtime");
				String vita = rowSet.getString("vita");

				map.put("begintime", begintime);
				map.put("endtime", endtime);
				map.put("vita", vita);

				list.add(map);
			}

			return list;

		} catch (SQLException e) {
			System.out
					.println("***********根据员工内码获取其任职简历_getVitaeByPersonId() error! "
							+ " SQL：" + sql);
			System.out.println(" Exception：" + e.toString());
		}

		return null;

	}

	/**
	 * 
	 * <p>
	 * Title: getPersonBaseInfoById
	 * </p>
	 * <p>
	 * Description: 根据员工内码获取其基本信息
	 * </p>
	 * 
	 * @param personId
	 * @return Map<String, Object>
	 * @throws BOSException
	 */
	public static Map<String, Object> getPersonBaseInfoById(String personId)
			throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}

		String sql = getSearchBaseInfoSQL(personId);

		SQLExecutor sec = new SQLExecutor(sql);

		IRowSet rowSet = sec.executeSQL();
		try {
			while (rowSet.next()) {
				Map<String, Object> map = new HashMap<String, Object>(10);
				String name = rowSet.getString("name");// 姓名
				int gender = rowSet.getInt("gender"); // 性别 1：男 2：女
				Date birthday = rowSet.getDate("birthday"); // 出生日期
				String folk = rowSet.getString("folk"); // 民族
				String nativeplace = rowSet.getString("nativeplace"); // 籍贯
				String homeplace = rowSet.getString("homeplace"); // 出生地
				Date rdsj = rowSet.getDate("rdsj"); // 入党时间
				String health = rowSet.getString("health"); // 健康状况
				Date jobstartdate = rowSet.getDate("jobstartdate"); // 参加工作时间

				map.put("name", name);
				//20190924：仝飞：用于正式环境最高学历改进测试
				log.error("员工姓名："+name);
				System.out.println("员工姓名："+name);
				map.put("gender", gender);
				map.put("birthday", birthday);
				map.put("folk", folk);
				map.put("nativeplace", nativeplace);
				map.put("homeplace", homeplace);
				map.put("rdsj", rdsj);
				map.put("health", health);
				map.put("jobstartdate", jobstartdate);

				return map;

			}

		} catch (SQLException e) {
			System.out
					.println("***********根据员工内码获取其基本信息_getPersonBaseInfoById() error! "
							+ " SQL：" + sql);
			System.out.println(" Exception：" + e.toString());
		}

		return null;

	}

	/**
	 * 
	 * <p>
	 * Title: getTechnicalPost
	 * </p>
	 * <p>
	 * Description: 根据员工内码获取员工职称信息 (服务端)
	 * </p>
	 * 
	 * @param ctx
	 * @param personId
	 * @return 职称
	 * @throws BOSException
	 */
	public static String getTechnicalPostByPersonId(Context ctx, String personId)
			throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return "";
		}

		IPersonTechnicalPost iPersonTP;
		ITechnicalPost iTechnicalPost;
		if (ctx == null) {
			iPersonTP = PersonTechnicalPostFactory.getRemoteInstance();
			iTechnicalPost = TechnicalPostFactory.getRemoteInstance();
		} else {
			iPersonTP = PersonTechnicalPostFactory.getLocalInstance(ctx);
			iTechnicalPost = TechnicalPostFactory.getLocalInstance(ctx);
		}

		String oql = " where person.id = '" + personId
				+ "' and isHighTechnical = 1";
		String technicalPostId = null;

		try {
			boolean exists = iPersonTP.exists(oql);
			if (!exists) {
				return "";
			}

			PersonTechnicalPostInfo personTechnicalPostInfo = iPersonTP
					.getPersonTechnicalPostInfo(oql);
			if (personTechnicalPostInfo != null
					&& personTechnicalPostInfo.getTechnicalPost().getId() != null) {
				technicalPostId = personTechnicalPostInfo.getTechnicalPost()
						.getId().toString();
			}

			if (StringUtils.isEmpty(technicalPostId)) {
				return "";
			}

			TechnicalPostInfo technicalPostInfo = iTechnicalPost
					.getTechnicalPostInfo(new ObjectUuidPK(technicalPostId));

			String technicalPostName = technicalPostInfo.getName();

			return technicalPostName;

		} catch (EASBizException e) {
			System.out.println("**********获取职称失败！  员工id: " + personId
					+ " Exception: " + e.toString());
		}

		return "";

	}

	/**
	 * 
	 * <p>
	 * Title: getPersonDegreeByPersonId
	 * </p>
	 * <p>
	 * Description: 根据员工内码获取员工教育经历 (服务端)
	 * </p>
	 * 
	 * @param ctx
	 * @param personId
	 * @return Map[] index = 0 : 在职教育经历 index = 1 : 全日制教育经历
	 * @throws BOSException
	 */
	@SuppressWarnings("unchecked")
	public static Map[] getPersonDegreeByPersonId(Context ctx, String personId)
			throws BOSException {

		if (StringUtils.isEmpty(personId)) {
			return null;
		}

		Map<String, Object>[] personDegrees = new Map[2];

		String zzId = getEduTypeIdByName(ctx, "在职");
		String qrzId = getEduTypeIdByName(ctx, "全日制");

		if (!StringUtils.isEmpty(zzId)) {
			// 在职教育经历
			Map<String, Object> map = getPersonDegreeMapByPersonIdAndEduTypeId(
					ctx, personId, zzId);
			personDegrees[0] = map;
		}

		if (!StringUtils.isEmpty(qrzId)) {
			// 全日制教育经历
			Map<String, Object> map = getPersonDegreeMapByPersonIdAndEduTypeId(
					ctx, personId, qrzId);
			personDegrees[1] = map;
		}

		return personDegrees;

	}

	/**
	 * 
	 * <p>
	 * Title: getVitaSQLByPersonId
	 * </p>
	 * <p>
	 * Description: 获取员工简历查询SQL
	 * </p>
	 * 
	 * @param personId
	 * @return String
	 */
	private static String getVitaSQLByPersonId(String personId) {

		if (StringUtils.isEmpty(personId)) {
			return "";
		}
		String sql = "select t.cfkssj as begintime,t.cfjssj as endtime,t.cfjljl as vita from CT_MP_GBJL t "
				.concat(" where t.fpersonid = '").concat(personId).concat(
						"' order by t.cfjssj");

		return sql;

	}

	private static String getSearchBaseInfoSQL(String personId) {

		StringBuffer sb = new StringBuffer();

		if (StringUtils.isEmpty(personId)) {
			return "";
		}

		sb.append("select  person.fname_l2 as name,");
		sb.append(" person.fgender as gender,  ");
		sb.append(" person.fbirthday as birthday,  ");
		sb.append(" folk.fname_l2 as folk, ");
		sb.append(" person.FNATIVEPLACE_L2 as nativeplace, ");
		sb.append(" person.FHOMEPLACE as homeplace, ");
		sb.append(" person.cfrdsj AS rdsj, ");
		sb.append(" health.fname_l2 AS health,  ");
		sb.append(" pp.fjobstartdate as jobstartdate  ");

		sb.append(" from t_bd_person person  ");
		sb.append(" LEFT JOIN T_BD_HRFolk folk");
		sb.append("   ON folk.fid = person.ffolkid  ");
		sb.append(" LEFT JOIN T_BD_HRHealth health  ");
		sb.append("   ON health.fid = person.fhealthid  ");
		sb.append(" LEFT JOIN T_HR_PersonPosition pp ");
		sb.append("   ON pp.fpersonid = person.fid ");

		sb.append(" where person.fid = '").append(personId).append("'");

		return sb.toString();

	}

	private static Map<String, Object> getPersonDegreeMapByPersonIdAndEduTypeId(
			Context ctx, String personId, String eduTypeId) throws BOSException {

		if (StringUtils.isEmpty(personId) || StringUtils.isEmpty(eduTypeId)) {
			return null;
		}

		String oql = "select * where person.id = '" + personId
				+ "' and eduType.id = '" + eduTypeId + "'";

		PersonDegreeCollection coll = getPersonDegreeInfoByOQL(ctx, oql);

		if (coll != null) {
			return getMapFromInfo(ctx, coll);
		}

		return null;

	}

	private static PersonDegreeCollection getPersonDegreeInfoByOQL(Context ctx,
			String oql) {

		if (StringUtils.isEmpty(oql)) {
			return null;
		}
		IPersonDegree iPersonDegree = null;
		try {
			iPersonDegree = getPersonDegreeInstance(ctx);
		} catch (BOSException e) {
			e.printStackTrace();
		}

		try {
			return iPersonDegree.getPersonDegreeCollection(oql);
		} catch (BOSException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 
	 * <p>
	 * Title: getPersonDegreeInstance
	 * </p>
	 * <p>
	 * Description: 员工教育经历实例
	 * </p>
	 * 
	 * @param ctx
	 * @return IPersonDegree
	 * @throws BOSException
	 */
	private static IPersonDegree getPersonDegreeInstance(Context ctx)
			throws BOSException {
		IPersonDegree iPersonDegree;
		if (ctx == null) {
			iPersonDegree = PersonDegreeFactory.getRemoteInstance();
		} else {
			iPersonDegree = PersonDegreeFactory.getLocalInstance(ctx);
		}

		return iPersonDegree;
	}

	private static Map<String, Object> getMapFromInfo(Context ctx,
			PersonDegreeCollection coll) throws BOSException {

		/**
		 * @update yacong_liu 经PM确认，此次逻辑进行调整。学历取最高学历。学位有多个 中间用、分割
		 */

		Map<String, Object> map = new HashMap<String, Object>(4);
		String diploma = ""; // 最高学历
		String[] schoolAndSpecialty = new String[coll.size()]; // 毕业院校专业
		String[] degree = new String[coll.size()]; // 学位
		
		String backUpDiploma = ""; // 备用学历：20190924：仝飞：由于正式环境的最高学历判断有问题，所以需使用备用学历在最高学历为空时代替最高学历
		if (coll != null && coll.size() > 0) {
			// 20190924：仝飞：对于单一学历，不对是否最高学历进行判断
			boolean isSingleEducation = coll.size()==1;
			log.error("是否单学历："+isSingleEducation);
			System.out.println("是否单学历："+isSingleEducation);
			for (int i = 0; i < coll.size(); i++) {
				PersonDegreeInfo personDegreeInfo = coll.get(i);
				// 获取最高学历
				// 20190921：经PM琨姐、君磊、仝飞讨论确认，全日制最高学历取决于isHighestBefore，在职最高学位取决于isHighWorkDip
				// 20190924：正式环境无法取到最高学位标志，怀疑正式环境的全日制最高学历并不仅取决于isHighestBefore
				boolean isHighestEduLevel = isSingleEducation?true:(personDegreeInfo.isIsHighestBefore()
						||personDegreeInfo.isIsHighest()
						||("5tQAAAC3+CrPw822".equals( getEduCFHigheastValueByPersonDegreeId(ctx,personDegreeInfo.getId().toString()))));
				boolean isHighestVacLevel = isSingleEducation?true:personDegreeInfo.isIsHighWorkDip();
				boolean hasDiploma = personDegreeInfo.getDiploma() != null;
				log.error("是否最高全日制学历："+isHighestEduLevel);
				System.out.println("是否最高全日制学历_："+isHighestEduLevel);
				log.error("是否最高在职学历："+isHighestVacLevel);
				System.out.println("是否最高在职学历_："+isHighestVacLevel);
				log.error("是否有学历数据："+hasDiploma);
				System.out.println("是否有学历数据_："+hasDiploma);
				
				// 20190924：仝飞：由于正式环境的最高学历判断有问题，所以需使用备用学历在最高学历为空时代替最高学历
				if(hasDiploma){
					backUpDiploma = getDiplomaNameById(ctx, personDegreeInfo
							.getDiploma().getId().toString());
				}
				
				if ((isHighestEduLevel || isHighestVacLevel)//是全日制最高学历或在职最高学历
						&& hasDiploma) {//且学历字段非空
					diploma = backUpDiploma;
				}

				// 专业
				String specialty = personDegreeInfo.getSpecialty();
				// 毕业学校
				String graduateSchool = personDegreeInfo.getGraduateSchool();

				//20190601仝飞：specialty有为空的可能性，会报500，解决方案为加非空验证
				//20190922：仝飞：此处只取最高学历对应的毕业院校和专业
				boolean hasSchoolAndSpeciality = specialty!=null&&!"".equals(specialty);
				log.error("是否有专业数据："+hasSchoolAndSpeciality);
				System.out.println("是否有专业数据："+hasSchoolAndSpeciality);
				
				if((isHighestEduLevel || isHighestVacLevel)//是全日制最高学历或在职最高学历
						&&hasSchoolAndSpeciality){//专业非空
					schoolAndSpecialty[i] = graduateSchool+specialty;
				}else if((isHighestEduLevel || isHighestVacLevel) ) {//是全日制最高学历或在职最高学历
					schoolAndSpecialty[i] = graduateSchool;
				}

				// 20190922：仝飞：此处只取最高学历的学位
				boolean hasDegree = personDegreeInfo.getDegree() != null; 
				if ((isHighestEduLevel || isHighestVacLevel)//是全日制最高学历或在职最高学历
						&&hasDegree) {//且学位字段非空
					degree[i] = getDegreeNameById(ctx, personDegreeInfo
							.getDegree().getId().toString());
				}
			}
		}

		map.put("schoolAndSpecialty", schoolAndSpecialty);
		map.put("degree", degree);
		// 20190924：仝飞：由于正式环境的最高学历判断有问题，所以需使用备用学历在最高学历为空时代替最高学历
		if(diploma==null||"".equals(diploma)){
			diploma = backUpDiploma;
		}
		map.put("diploma", diploma);
		
		log.error("院校专业："+schoolAndSpecialty.toString());
		System.out.println("院校专业_："+schoolAndSpecialty.toString());
		log.error("学位："+degree.toString());
		System.out.println("学位_："+degree.toString());
		log.error("学历："+diploma.toString());
		System.out.println("学历_："+diploma.toString());

		return map;

	}

	/**
	 * 
	 * <p>
	 * Title: getEdyTypeIdByName
	 * </p>
	 * <p>
	 * Description: 根据教育类型名称获取其对应的内码
	 * </p>
	 * 
	 * @param typeName
	 * @return
	 * @throws BOSException
	 */
	private static String getEduTypeIdByName(Context ctx, String typeName)
			throws BOSException {

		if (StringUtils.isEmpty(typeName)) {
			return "";
		}

		typeName = typeName.trim();
		IEduType iEduType;
		if (ctx == null) {
			iEduType = EduTypeFactory.getRemoteInstance();
		} else {
			iEduType = EduTypeFactory.getLocalInstance(ctx);
		}

		try {
			boolean exists = iEduType
					.exists(" where name = '" + typeName + "'");
			if (exists) {
				String oql = "select id where name = '" + typeName + "'";
				EduTypeInfo info = iEduType.getEduTypeInfo(oql);

				return info.getId().toString();
			}
		} catch (EASBizException e) {
			e.printStackTrace();
		}

		return "";

	}

	/**
	 * 
	 * <p>
	 * Title: getEdyTypeIdByName
	 * </p>
	 * <p>
	 * Time：20190924
	 * Description: 根据学位内码获取其 是否最高全日制属性 的疑似值，查无数据则返回null
	 * </p>
	 * @param ctx
	 * @param id
	 * @return
	 * @throws BOSException
	 */
	private static String getEduCFHigheastValueByPersonDegreeId(Context ctx, String id)
	throws BOSException {

	if (StringUtils.isEmpty(id)) {
		return "";
	}
	
	id = id.trim();
	IPersonDegree iPersonDegree;
	if (ctx == null) {
		iPersonDegree = PersonDegreeFactory.getRemoteInstance();
	} else {
		iPersonDegree = PersonDegreeFactory.getLocalInstance(ctx);
	}
	
	try {
		boolean exists = iPersonDegree
				.exists(" where id = '" + id + "'");
		if (exists) {
			String oql = "select CFHIGHEASTBEFOREID where id = '" + id + "'";
			PersonDegreeInfo info = iPersonDegree.getPersonDegreeInfo(oql);
			Object infoObj = info.get("CFHIGHEASTBEFOREID");
			if(infoObj==null){
				System.out.println("没有查询到是否最高学位的疑似结果");
				return null;
			}
			String value = infoObj.toString();
			//20190924：仝飞：用于正式环境手动调试的输出代码
			System.out.println("当前学位的是否为最高学位的疑似值为："+value+"(期望值为5tQAAAC3+CrPw822)");
			return value;
		}
	} catch (EASBizException e) {
		e.printStackTrace();
	}
	
	return "";
	
	}
	
	/**
	 * 
	 * <p>
	 * Title: getDegreeNameById
	 * </p>
	 * <p>
	 * Description: 根据学位内码获取其名称
	 * </p>
	 * 
	 * @param ctx
	 * @param id
	 * @return 学位
	 * @throws BOSException
	 */
	private static String getDegreeNameById(Context ctx, String id)
			throws BOSException {

		if (StringUtils.isEmpty(id)) {
			return "";
		}

		IDegree iDegree;
		if (ctx == null) {
			iDegree = DegreeFactory.getRemoteInstance();
		} else {
			iDegree = DegreeFactory.getLocalInstance(ctx);
		}

		try {
			DegreeInfo info = iDegree.getDegreeInfo(new ObjectUuidPK(id));
			return info.getName();
		} catch (EASBizException e) {
			System.out
					.println("***********根据学位内码获取其名称_getDegreeNameById() ERROR! ID: "
							+ id);
		}

		return "";

	}

	/**
	 * 
	 * <p>
	 * Title: getRelationNameById
	 * </p>
	 * <p>
	 * Description: 根据员工社会关系内码获取其名称
	 * </p>
	 * 
	 * @param ctx
	 * @param id
	 * @return
	 * @throws BOSException
	 */
	private static String getRelationNameById(Context ctx, String id)
			throws BOSException {

		if (StringUtils.isEmpty(id)) {
			return "";
		}

		IRelation instance;
		if (ctx == null) {
			instance = RelationFactory.getRemoteInstance();
		} else {
			instance = RelationFactory.getLocalInstance(ctx);
		}

		try {
			RelationInfo info = instance.getRelationInfo(new ObjectUuidPK(id));
			return info.getName();
		} catch (EASBizException e) {
			System.out
					.println("***********根据员工社会关系内码获取其名称_getRelationNameById() ERROR! ID: "
							+ id);
		}

		return "";

	}

	/**
	 * 
	 * <p>
	 * Title: getPoliticalFaceNameById
	 * </p>
	 * <p>
	 * Description: 根据员工社会关系政治面貌内码获取名称
	 * </p>
	 * 
	 * @param ctx
	 * @param id
	 * @return
	 * @throws BOSException
	 */
	private static String getPoliticalFaceNameById(Context ctx, String id)
			throws BOSException {

		if (StringUtils.isEmpty(id)) {
			return "";
		}

		IPoliticalFace instance;
		if (ctx == null) {
			instance = PoliticalFaceFactory.getRemoteInstance();
		} else {
			instance = PoliticalFaceFactory.getLocalInstance(ctx);
		}

		try {
			PoliticalFaceInfo info = instance
					.getPoliticalFaceInfo(new ObjectUuidPK(id));
			return info.getName();
		} catch (EASBizException e) {
			System.out
					.println("***********根据员工社会关系政治面貌内码获取名称_getPoliticalFaceNameById() ERROR! ID: "
							+ id);
		}

		return "";

	}

	/**
	 * 
	 * <p>
	 * Title: getDiplomaNameById
	 * </p>
	 * <p>
	 * Description: 根据学历内码获取其名称
	 * </p>
	 * 
	 * @param ctx
	 * @param id
	 * @return 学历
	 * @throws BOSException
	 */
	private static String getDiplomaNameById(Context ctx, String id)
			throws BOSException {

		if (StringUtils.isEmpty(id)) {
			return "";
		}

		IDiploma iDiploma;
		if (ctx == null) {
			iDiploma = DiplomaFactory.getRemoteInstance();
		} else {
			iDiploma = DiplomaFactory.getLocalInstance(ctx);
		}

		try {

			DiplomaInfo info = iDiploma.getDiplomaInfo(new ObjectUuidPK(id));
			return info.getName();
		} catch (EASBizException e) {
			System.out
					.println("***********根据学历内码获取其名称_getDiplomaNameById() ERROR! ID: "
							+ id);
		}

		return "";

	}

}
