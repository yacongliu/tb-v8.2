package com.kingdee.shr.custom.utils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.*;
 
public class SoapUtil {
	
	/*
	 * 远程访问SOAP协议接口
	 * 
	 * @param url： 服务接口地址"http://192.168.0.120:8222/HelloWorld?wsdl"
	 * @param isClass：接口类名
	 * @param isMethod： 接口方法名
	 * @param sendSoapString： soap协议xml格式访问接口
	 *    
	 * @return  soap协议xml格式
	 * 
	 * @备注：有四种请求头格式1、SOAP 1.1； 2、SOAP 1.2 ； 3、HTTP GET； 4、HTTP POST
	 * 参考---》http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?op=getWeatherbyCityName
	 */
	public static String getWebServiceAndSoap(String url,String isClass,String isMethod,StringBuffer sendSoapString) throws IOException {
		String soap = sendSoapString.toString();
		if (soap == null) {
			return null;
		}
		URL soapUrl = new URL(url);
		URLConnection conn = soapUrl.openConnection();
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Length",
				Integer.toString(soap.length()));
		conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		// 调用的接口方法是
		conn.setRequestProperty(isClass,isMethod);
		OutputStream os = conn.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "utf-8");
		osw.write(soap);
		osw.flush();
		osw.close();
		// 获取webserivce返回的流
		InputStream is = conn.getInputStream();
		if (is!=null) {
			byte[] bytes = new byte[0];
			bytes = new byte[is.available()];
			is.read(bytes);
			String str = new String(bytes);
			return str;
		}else {
			return null;
		}
	}
}
