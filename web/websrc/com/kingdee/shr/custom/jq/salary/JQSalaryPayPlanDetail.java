package com.kingdee.shr.custom.jq.salary;

/**
 * 
 * Title: JQSalaryPayPlanDetail
 * <p>
 * Description: 久其接口薪酬付款单的付款计划明细分录实体
 * 
 * @author 仝飞
 * @date 2019-9-10 & 下午02:20:31
 * @since V1.0
 */
public class JQSalaryPayPlanDetail {
/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	private String tb04_bm;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	private String tb05_payPlan_dExternalSysId;
	
	/**
	 * [工资]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb06_gz;
	
	/**
	 * [津贴和补贴]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb07_jtbt;
	
	/**
	 * [福利费]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb08_flf;
	
	/**
	 * [交通补贴]，久其接口的类型是数值型类型最大长度是14
	 * 车补
	 */
	private String tb09_cb;
	
	/**
	 * [养老保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb10_yangLBX_dw;
	
	/**
	 * [医疗保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb11_yiLBX_dw;

	/**
	 * [失业保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb12_shiYBX_dw;
	
	/**
	 * [工伤保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb13_gsbx_dw;
	
	/**
	 * [生育保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb14_shengYBX_dw;
	
	/**
	 * [养老保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb15_yangLBX_gr;
	
	/**
	 * [医疗保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb16_yiLBX_gr;
	
	/**
	 * [失业保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb17_shiYBX_gr;
	
	/**
	 * [公积金-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb18_gjj_dw;
	
	/**
	 * [公积金-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb19_gjj_gr;
	
	/**
	 * [补充公积金]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb20_gjj_bc;
	
	/**
	 * [企业年金-企业缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb21_qynj_dw;
	
	/**
	 * [企业年金-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb22_qynj_gr;
	
	/**
	 * [个税扣款]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb23_gs;
	
	/**
	 * [工会会员费]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb24_ghhyf;
	
	/**
	 * [实发工资额]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb25_paymoney;
	
	
	
	
	///////////////////////////////////////////////////////////////////
	
	
	
	
	/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门名称
	 */
	public String getTb04_bm() {
		return tb04_bm;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门名称
	 */
	public void setTb04_bm(String tb04_bm) {
		this.tb04_bm = tb04_bm;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public String getTb05_payPlan_dExternalSysId() {
		return tb05_payPlan_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public void setTb05_payPlan_dExternalSysId(String tb05_payPlan_dExternalSysId) {
		this.tb05_payPlan_dExternalSysId = tb05_payPlan_dExternalSysId;
	}

	/**
	 * [工资]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb06_gz() {
		return tb06_gz;
	}

	/**
	 * [工资]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb06_gz(String tb06_gz) {
		this.tb06_gz = tb06_gz;
	}

	/**
	 * [津贴和补贴]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb07_jtbt() {
		return tb07_jtbt;
	}

	/**
	 * [津贴和补贴]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb07_jtbt(String tb07_jtbt) {
		this.tb07_jtbt = tb07_jtbt;
	}

	/**
	 * [福利费]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb08_flf() {
		return tb08_flf;
	}

	/**
	 * [福利费]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb08_flf(String tb08_flf) {
		this.tb08_flf = tb08_flf;
	}

	/**
	 * [交通补贴]，久其接口的类型是数值型类型最大长度是14
	 * 车补
	 */
	public String getTb09_cb() {
		return tb09_cb;
	}

	/**
	 * [交通补贴]，久其接口的类型是数值型类型最大长度是14
	 * 车补
	 */
	public void setTb09_cb(String tb09_cb) {
		this.tb09_cb = tb09_cb;
	}

	/**
	 * [养老保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb10_yangLBX_dw() {
		return tb10_yangLBX_dw;
	}

	/**
	 * [养老保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb10_yangLBX_dw(String tb10_yangLBX_dw) {
		this.tb10_yangLBX_dw = tb10_yangLBX_dw;
	}

	/**
	 * [医疗保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb11_yiLBX_dw() {
		return tb11_yiLBX_dw;
	}

	/**
	 * [医疗保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb11_yiLBX_dw(String tb11_yiLBX_dw) {
		this.tb11_yiLBX_dw = tb11_yiLBX_dw;
	}

	/**
	 * [失业保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb12_shiYBX_dw() {
		return tb12_shiYBX_dw;
	}

	/**
	 * [失业保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb12_shiYBX_dw(String tb12_shiYBX_dw) {
		this.tb12_shiYBX_dw = tb12_shiYBX_dw;
	}

	/**
	 * [工伤保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb13_gsbx_dw() {
		return tb13_gsbx_dw;
	}

	/**
	 * [工伤保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb13_gsbx_dw(String tb13_gsbx_dw) {
		this.tb13_gsbx_dw = tb13_gsbx_dw;
	}

	/**
	 * [生育保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb14_shengYBX_dw() {
		return tb14_shengYBX_dw;
	}

	/**
	 * [生育保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb14_shengYBX_dw(String tb14_shengYBX_dw) {
		this.tb14_shengYBX_dw = tb14_shengYBX_dw;
	}

	/**
	 * [养老保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb15_yangLBX_gr() {
		return tb15_yangLBX_gr;
	}

	/**
	 * [养老保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb15_yangLBX_gr(String tb15_yangLBX_gr) {
		this.tb15_yangLBX_gr = tb15_yangLBX_gr;
	}

	/**
	 * [医疗保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb16_yiLBX_gr() {
		return tb16_yiLBX_gr;
	}

	/**
	 * [医疗保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb16_yiLBX_gr(String tb16_yiLBX_gr) {
		this.tb16_yiLBX_gr = tb16_yiLBX_gr;
	}

	/**
	 * [失业保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb17_shiYBX_gr() {
		return tb17_shiYBX_gr;
	}

	/**
	 * [失业保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb17_shiYBX_gr(String tb17_shiYBX_gr) {
		this.tb17_shiYBX_gr = tb17_shiYBX_gr;
	}

	/**
	 * [公积金-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb18_gjj_dw() {
		return tb18_gjj_dw;
	}

	/**
	 * [公积金-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb18_gjj_dw(String tb18_gjj_dw) {
		this.tb18_gjj_dw = tb18_gjj_dw;
	}

	/**
	 * [公积金-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb19_gjj_gr() {
		return tb19_gjj_gr;
	}

	/**
	 * [公积金-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb19_gjj_gr(String tb19_gjj_gr) {
		this.tb19_gjj_gr = tb19_gjj_gr;
	}

	/**
	 * [补充公积金]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb20_gjj_bc() {
		return tb20_gjj_bc;
	}

	/**
	 * [补充公积金]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb20_gjj_bc(String tb20_gjj_bc) {
		this.tb20_gjj_bc = tb20_gjj_bc;
	}

	/**
	 * [企业年金-企业缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb21_qynj_dw() {
		return tb21_qynj_dw;
	}

	/**
	 * [企业年金-企业缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb21_qynj_dw(String tb21_qynj_dw) {
		this.tb21_qynj_dw = tb21_qynj_dw;
	}

	/**
	 * [企业年金-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb22_qynj_gr() {
		return tb22_qynj_gr;
	}

	/**
	 * [企业年金-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb22_qynj_gr(String tb22_qynj_gr) {
		this.tb22_qynj_gr = tb22_qynj_gr;
	}

	/**
	 * [个税扣款]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb23_gs() {
		return tb23_gs;
	}

	/**
	 * [个税扣款]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb23_gs(String tb23_gs) {
		this.tb23_gs = tb23_gs;
	}

	/**
	 * [工会会员费]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb24_ghhyf() {
		return tb24_ghhyf;
	}

	/**
	 * [工会会员费]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb24_ghhyf(String tb24_ghhyf) {
		this.tb24_ghhyf = tb24_ghhyf;
	}

	/**
	 * [实发工资额]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb25_paymoney() {
		return tb25_paymoney;
	}

	/**
	 * [实发工资额]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb25_paymoney(String tb25_paymoney) {
		this.tb25_paymoney = tb25_paymoney;
	}
}
