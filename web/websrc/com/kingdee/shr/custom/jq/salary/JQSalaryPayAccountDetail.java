package com.kingdee.shr.custom.jq.salary;

/**
 * 
 * Title: JQSalaryPayAccountDetail
 * <p>
 * Description: 久其接口薪酬付款单的付款账户分录实体
 * 
 * @author 仝飞
 * @date 2019-9-10 & 下午02:20:31
 * @since V1.0
 */
public class JQSalaryPayAccountDetail {

	/*******************  以下为FO_PAYACCOUNT  ********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	private String tb26_bank;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	private String tb27_payAccount_dExternalSysId;
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	private String tb28_supplier;
	
	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	private String tb29_bankNumber;
	
	
	
//////////////////////////////////////////////////////////////////////	
	


	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public String getTb26_bank() {
		return tb26_bank;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public void setTb26_bank(String tb26_bank) {
		this.tb26_bank = tb26_bank;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public String getTb27_payAccount_dExternalSysId() {
		return tb27_payAccount_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public void setTb27_payAccount_dExternalSysId(String tb27_payAccount_dExternalSysId) {
		this.tb27_payAccount_dExternalSysId = tb27_payAccount_dExternalSysId;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public String getTb28_supplier() {
		return tb28_supplier;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public void setTb28_supplier(String tb28_supplier) {
		this.tb28_supplier = tb28_supplier;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public String getTb29_bankNumber() {
		return tb29_bankNumber;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public void setTb29_bankNumber(String tb29_bankNumber) {
		this.tb29_bankNumber = tb29_bankNumber;
	}

}
