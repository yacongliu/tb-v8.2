package com.kingdee.shr.custom.jq.housingFund;

/**
 * 
 * Title: JQHousingFundPayAccountDetail
 * <p>
 * Description: 久其接口公积金付款单付款账户分录实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQHousingFundPayAccountDetail {

	/*******************  以下为FO_PAYACCOUNT  ********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	private String tb08_bank;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	private String tb09_dExternalSysId;
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	private String tb10_supplier;
	
	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	private String tb11_bankNumber;
	
	
	
	//////////////////////////////////////////////////////////////////////	
	


	/*******************  以下为FO_PAYACCOUNT  ********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public String getTb08_bank() {
		return tb08_bank;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public void setTb08_bank(String tb08_bank) {
		this.tb08_bank = tb08_bank;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public String getTb09_dExternalSysId() {
		return tb09_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public void setTb09_dExternalSysId(String tb09_dExternalSysId) {
		this.tb09_dExternalSysId = tb09_dExternalSysId;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public String getTb10_supplier() {
		return tb10_supplier;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public void setTb10_supplier(String tb10_supplier) {
		this.tb10_supplier = tb10_supplier;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public String getTb11_bankNumber() {
		return tb11_bankNumber;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public void setTb11_bankNumber(String tb11_bankNumber) {
		this.tb11_bankNumber = tb11_bankNumber;
	}


}
