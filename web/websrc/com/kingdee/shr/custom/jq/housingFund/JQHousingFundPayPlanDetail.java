package com.kingdee.shr.custom.jq.housingFund;

/**
 * 
 * Title: JQHousingFundPayPlanDetail
 * <p>
 * Description: 久其接口公积金付款单付款计划分录实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQHousingFundPayPlanDetail {

	/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放类型]字段
	 * 编码	名称
	 * 01	发放员工薪酬
	 * 02	发放劳务派遣人员薪酬
	 */
	private String tb02_fflx;
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	private String tb03_bm;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	private String tb04_dExternalSysId;
	
	/**
	 * [基本公积金-单位缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	private String tb05_gjj_dw;
	
	/**
	 * [基本公积金-个人缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	private String tb06_gjj_gr;
	
	/**
	 * [补充公积金]，填写的类型是数值型类型最大长度是14
	 */
	private String tb07_gjj_bc;
	
	
	

	///////////////////////////////////////////////////////////////////
	
	
	
	
	/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放类型]字段
	 * 编码	名称
	 * 01	发放员工薪酬
	 * 02	发放劳务派遣人员薪酬
	 */
	public String getTb02_fflx() {
		return tb02_fflx;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放类型]字段
	 * 编码	名称
	 * 01	发放员工薪酬
	 * 02	发放劳务派遣人员薪酬
	 */
	public void setTb02_fflx(String tb02_fflx) {
		this.tb02_fflx = tb02_fflx;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	public String getTb03_bm() {
		return tb03_bm;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	public void setTb03_bm(String tb03_bm) {
		this.tb03_bm = tb03_bm;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public String getTb04_dExternalSysId() {
		return tb04_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public void setTb04_dExternalSysId(String tb04_dExternalSysId) {
		this.tb04_dExternalSysId = tb04_dExternalSysId;
	}

	/**
	 * [基本公积金-单位缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public String getTb05_gjj_dw() {
		return tb05_gjj_dw;
	}

	/**
	 * [基本公积金-单位缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public void setTb05_gjj_dw(String tb05_gjj_dw) {
		this.tb05_gjj_dw = tb05_gjj_dw;
	}

	/**
	 * [基本公积金-个人缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public String getTb06_gjj_gr() {
		return tb06_gjj_gr;
	}

	/**
	 * [基本公积金-个人缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public void setTb06_gjj_gr(String tb06_gjj_gr) {
		this.tb06_gjj_gr = tb06_gjj_gr;
	}

	/**
	 * [补充公积金]，填写的类型是数值型类型最大长度是14
	 */
	public String getTb07_gjj_bc() {
		return tb07_gjj_bc;
	}

	/**
	 * [补充公积金]，填写的类型是数值型类型最大长度是14
	 */
	public void setTb07_gjj_bc(String tb07_gjj_bc) {
		this.tb07_gjj_bc = tb07_gjj_bc;
	}
	
	
	
}
