package com.kingdee.shr.custom.jq.serviceTest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import com.kingdee.bos.Context;
import com.kingdee.bos.bsf.service.app.IHRMsfService;
import com.kingdee.shr.compensation.util.common.StringUtil;
import com.kingdee.shr.custom.jq.annuity.AnnuityDataTransferService;
import com.kingdee.shr.custom.utils.JQXmlUtils;
import com.kingdee.shr.custom.utils.SoapUtil;

/**
 * 
 * Title: TestDataTransferService
 * Description:调用SOAP工具类对接久其接口通讯进行测试
 * 
 * @author 仝飞 Email:tjtongfei@kingdee.com
 * @date 2019年10月11日
 */
public class TestDataTransferService implements IHRMsfService {

	Logger log = Logger.getLogger(AnnuityDataTransferService.class);
	
	/**
	 *调用SOAP工具类对接久其接口通讯进行测试的OSF执行方法
	 * @param HashMap<String,Object>类型的参数映射，键值对有
	 * url：久其webService的服务地址，默认为"http://60.28.230.140:9799/dna_ws/ImpBillWebService"，测试环境可以不填，转正式环境后必填
	 * userName：久其webService的服务登陆用户，默认为jq_hkw，测试环境可以不填，转正式环境后必填
	 * userPWD：久其webService的服务登陆密码，默认为"5CBFEC3C943B0DC19E8288474B330438"，测试环境可以不填，转正式环境后必填
	 * @return HashMap
	 * ----------主要值----------
	 * send：发送的报文
	 * isSuccess：执行成功为"true"，执行失败为"false"||
	 * msg：执行结果的主要说明||
	 * xml：久其接口通讯成功时的返回报文
	 */
	@Override
	public Object process(Context ctx, Map param) {
		StringBuffer callChain = new StringBuffer("调用SOAP工具类对接久其接口通讯进行测试的OSF执行方法：");
		//关键参数NPE处理
		if(param==null) {
			param = new HashMap<String,Object>();
		}
		// 久其接口实际访问地址
		Object JiuQiURL_obj = param.get("url");
		String JiuQiURL_ = JiuQiURL_obj==null?JQXmlUtils.JiuQiURL:JiuQiURL_obj.toString();
		Object userName_obj = param.get("userName");
		String userName_ = userName_obj==null?JQXmlUtils.userName:userName_obj.toString();
		Object userPWD_obj = param.get("userPWD");
		String userPWD_ = userPWD_obj==null?JQXmlUtils.userPWD:userPWD_obj.toString();
		StringBuffer sendSoapString = new StringBuffer();
		String dataid = "100001";
		sendSoapString.append(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:jiuq=\"http://jiuqi.com.cn\">")
				.append("   <soapenv:Header/>")
				.append("   <soapenv:Body>")
				.append("      <jiuq:parseStrWithAuth>")
				.append("         <xmlStr><![CDATA[")
				.append("<sscinterface systemname=\"skd\" billdefine=\"FO_QY_WYZJSK\" unitid=\"500000TBTZFGGY\" userid=\"fggyxm\" operatertype=\"add\" dataid=\"").append(dataid).append("\">")
				.append("<bill>")
				.append("</bill>")
				.append("</sscinterface>")
				.append("]]></xmlStr>")
				.append("        <userName>").append(userName_).append("</userName>")
				.append("         <userPWD>").append(userPWD_).append("</userPWD>")
				.append("      </jiuq:parseStrWithAuth>")
				.append("   </soapenv:Body>")
				.append("</soapenv:Envelope>")
				;
		Map<String, String> retMap = new HashMap<String, String>();
		retMap.put("send", sendSoapString.toString());
		try {
			
			String xml = SoapUtil.getWebServiceAndSoap(JiuQiURL_, JQXmlUtils.JiuQiClass, JQXmlUtils.JiuQiMethod, sendSoapString);

			retMap = JQXmlUtils.parseSOAPResult(xml, callChain);
			
		} catch (IOException e) {
			String toLog=callChain.append("调用soap通讯方法时出现IO异常：").append(e.getMessage())
					.append("\r\n发送报文为：").append(sendSoapString)
					.toString();
			log.error(toLog);
			retMap.put("isSuccess","false");
			retMap.put("msg",toLog);
			retMap.put("send", sendSoapString.toString());
			return retMap;
		} catch (DocumentException e) {
			String toLog=callChain.append("解析soap通讯方法返回值时出现XML异常：").append(e.getMessage())
					.append("\r\n发送报文为：").append(sendSoapString)
					.toString();
			log.error(toLog);
			retMap.put("isSuccess","false");
			retMap.put("msg",toLog);
			retMap.put("send", sendSoapString.toString());
			return retMap;
		}
		
		String retCode = retMap.get("rtnCode");
		if(StringUtil.isNullOrEmpty(retCode)) {
			retMap.put("isSuccess", "false");
			retMap.put("msg", "服务器没有返回符合约定的响应信息");
			retMap.put("send", sendSoapString.toString());
			return retMap;
		}
		
		retMap.put("isSuccess", "true");
		retMap.put("msg", "服务器响应成功");
		retMap.put("send", sendSoapString.toString());
		return retMap;
	}
}
