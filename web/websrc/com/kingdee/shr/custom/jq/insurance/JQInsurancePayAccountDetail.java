package com.kingdee.shr.custom.jq.insurance;

/**
 * 
 * Title: JQInsurancePayAccountDetail
 * <p>
 * Description: 久其接口社保付款单的付款账户分录实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQInsurancePayAccountDetail {

	/*******************  以下为FO_PAYACCOUNT  ********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	private String tb13_bank;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	private String tb14_dExternalSysId;
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	private String tb15_supplier;
	
	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	private String tb16_bankNumber;
	
	
	
//////////////////////////////////////////////////////////////////////	
	


	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public String getTb13_bank() {
		return tb13_bank;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public void setTb13_bank(String tb13_bank) {
		this.tb13_bank = tb13_bank;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public String getTb14_dExternalSysId() {
		return tb14_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public void setTb14_dExternalSysId(String tb14_dExternalSysId) {
		this.tb14_dExternalSysId = tb14_dExternalSysId;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public String getTb15_supplier() {
		return tb15_supplier;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public void setTb15_supplier(String tb15_supplier) {
		this.tb15_supplier = tb15_supplier;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public String getTb16_bankNumber() {
		return tb16_bankNumber;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public void setTb16_bankNumber(String tb16_bankNumber) {
		this.tb16_bankNumber = tb16_bankNumber;
	}


}
