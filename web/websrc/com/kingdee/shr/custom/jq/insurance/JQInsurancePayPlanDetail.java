package com.kingdee.shr.custom.jq.insurance;

/**
 * Title: JQInsurancePayPlanDetail
 * <p>
 * Description: 久其接口社保付款单的付款计划明细分录实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQInsurancePayPlanDetail {

	/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放类型]字段
	 * 编码	名称
	 * 01	发放员工薪酬
	 * 02	发放劳务派遣人员薪酬
	 */
	private String tb02_fflx;
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	private String tb03_bm;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	private String tb04_dExternalSysId;
	
	/**
	 * [养老保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb05_yangLBX_dw;
	
	/**
	 * [医疗保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb06_yiLBX_dw;
	
	/**
	 * [失业保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb07_shiYBX_dw;
	
	/**
	 * [工伤保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb08_gsbx_dw;
	
	/**
	 * [生育保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb09_shengYBX_dw;
	
	/**
	 * [养老保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb10_yangLBX_gr;
	
	/**
	 * [医疗保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb11_yiLBX_gr;

	/**
	 * [失业保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	private String tb12_shiYBX_gr;
	
	
	
	
	///////////////////////////////////////////////////////////////////
	
	
	
	
	/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放类型]字段
	 * 编码	名称
	 * 01	发放员工薪酬
	 * 02	发放劳务派遣人员薪酬
	 */
	public String getTb02_fflx() {
		return tb02_fflx;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放类型]字段
	 * 编码	名称
	 * 01	发放员工薪酬
	 * 02	发放劳务派遣人员薪酬
	 */
	public void setTb02_fflx(String tb02_fflx) {
		this.tb02_fflx = tb02_fflx;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	public String getTb03_bm() {
		return tb03_bm;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [部门]字段
	 * shr中的部门代码
	 */
	public void setTb03_bm(String tb03_bm) {
		this.tb03_bm = tb03_bm;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public String getTb04_dExternalSysId() {
		return tb04_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public void setTb04_dExternalSysId(String tb04_dExternalSysId) {
		this.tb04_dExternalSysId = tb04_dExternalSysId;
	}

	/**
	 * [养老保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb05_yangLBX_dw() {
		return tb05_yangLBX_dw;
	}

	/**
	 * [养老保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb05_yangLBX_dw(String tb05_yangLBX_dw) {
		this.tb05_yangLBX_dw = tb05_yangLBX_dw;
	}

	/**
	 * [医疗保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb06_yiLBX_dw() {
		return tb06_yiLBX_dw;
	}

	/**
	 * [医疗保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb06_yiLBX_dw(String tb06_yiLBX_dw) {
		this.tb06_yiLBX_dw = tb06_yiLBX_dw;
	}

	/**
	 * [失业保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb07_shiYBX_dw() {
		return tb07_shiYBX_dw;
	}

	/**
	 * [失业保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb07_shiYBX_dw(String tb07_shiYBX_dw) {
		this.tb07_shiYBX_dw = tb07_shiYBX_dw;
	}

	/**
	 * [工伤保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb08_gsbx_dw() {
		return tb08_gsbx_dw;
	}

	/**
	 * [工伤保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb08_gsbx_dw(String tb08_gsbx_dw) {
		this.tb08_gsbx_dw = tb08_gsbx_dw;
	}

	/**
	 * [生育保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb09_shengYBX_dw() {
		return tb09_shengYBX_dw;
	}

	/**
	 * [生育保险-单位缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb09_shengYBX_dw(String tb09_shengYBX_dw) {
		this.tb09_shengYBX_dw = tb09_shengYBX_dw;
	}

	/**
	 * [养老保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb10_yangLBX_gr() {
		return tb10_yangLBX_gr;
	}

	/**
	 * [养老保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb10_yangLBX_gr(String tb10_yangLBX_gr) {
		this.tb10_yangLBX_gr = tb10_yangLBX_gr;
	}

	/**
	 * [医疗保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb11_yiLBX_gr() {
		return tb11_yiLBX_gr;
	}

	/**
	 * [医疗保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb11_yiLBX_gr(String tb11_yiLBX_gr) {
		this.tb11_yiLBX_gr = tb11_yiLBX_gr;
	}

	/**
	 * [失业保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public String getTb12_shiYBX_gr() {
		return tb12_shiYBX_gr;
	}

	/**
	 * [失业保险-个人缴纳部分]，久其接口的类型是数值型类型最大长度是14
	 */
	public void setTb12_shiYBX_gr(String tb12_shiYBX_gr) {
		this.tb12_shiYBX_gr = tb12_shiYBX_gr;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
