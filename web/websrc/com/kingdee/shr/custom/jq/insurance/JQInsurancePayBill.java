package com.kingdee.shr.custom.jq.insurance;

import java.util.List;

/**
 * 
 * Title: JQInsurancePayBill
 * <p>
 * Description: 久其接口社保付款单实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQInsurancePayBill {
	/********************  以下是sscinterface接口系统数据   *********************/
	
	/**
	 * [所属组织编码]，所属SHR组织的编码
	 */
	private String tb0a_unitId;
	
	/**
	 * [当前登陆用户编码]，SHR系统中用户编码
	 */
	private String tb0b_userId;
	
	/**
	 * [操作类型]，新增(ADD)、修改(UPDATE)、删除(DELETE)
	 */
	private String tb0c_operatertype;
	
	/**
	 * [是否带有电子文件]，参数值true代表带有，false代表不存在
	 */
	private String tb0d_hasimage;
	
	/**
	 * [关联附件的全路径]
	 */
	private String tb0e_filepath;

	/**
	 * [主键id]，薪酬付款单在Shr中对应的唯一标识
	 */
	private String tb0f_dataId;

	
	/********************  以下是FO_PAYPLAN单据头数据   *********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放月份]字段
	 * 编码	名称
	 * 01	1月  、 02	2月  、 03	3月  、 04	4月  、 05	5月  、 06	6月  、 07	7月  、 08	8月  、 09	9月  、 10	10月
	 */
	private String tb01_ffyf;
	

	
	/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	private List<JQInsurancePayPlanDetail> fo01_payPlan;


	/*******************  以下为付款账户分录FO_PAYACCOUNT  ********************/
	private List<JQInsurancePayAccountDetail> fo02_payAccount;
	

	

	//////////////////////////////////////////////////////////////////////////////////
	
	
	

	/********************  以下是付款单sscinterface接口系统数据   *********************/
	
	/**
	 * [所属组织编码]，所属SHR组织的编码
	 * @return
	 */
	public String getTb0a_unitId() {
		return tb0a_unitId;
	}

	/**
	 * [所属组织编码]，所属SHR组织的编码
	 * @param tb0a_unitId
	 */
	public void setTb0a_unitId(String tb0a_unitId) {
		this.tb0a_unitId = tb0a_unitId;
	}
	
	/**
	 * [当前登陆用户编码]，SHR系统中用户编码
	 * @return
	 */
	public String getTb0b_userId() {
		return tb0b_userId;
	}

	/**
	 * [当前登陆用户编码]，SHR系统中用户编码
	 * @param tb0b_userId
	 */
	public void setTb0b_userId(String tb0b_userId) {
		this.tb0b_userId = tb0b_userId;
	}

	/**
	 * [操作类型]，新增(ADD)、修改(UPDATE)、删除(DELETE)
	 * @return
	 */
	public String getTb0c_operatertype() {
		return tb0c_operatertype;
	}

	/**
	 * [操作类型]，新增(ADD)、修改(UPDATE)、删除(DELETE)
	 * @param tb0c_operatertype
	 */
	public void setTb0c_operatertype(String tb0c_operatertype) {
		this.tb0c_operatertype = tb0c_operatertype;
	}

	/**
	 * [是否带有电子文件]，参数值true代表带有，false代表不存在
	 * @return
	 */
	public String getTb0d_hasimage() {
		return tb0d_hasimage;
	}

	/**
	 * [是否带有电子文件]，参数值true代表带有，false代表不存在
	 * @param tb0d_hasimage
	 */
	public void setTb0d_hasimage(String tb0d_hasimage) {
		this.tb0d_hasimage = tb0d_hasimage;
	}

	/**
	 * [关联附件的全路径]
	 * @return
	 */
	public String getTb0e_filepath() {
		return tb0e_filepath;
	}

	/**
	 * [关联附件的全路径]
	 * @param tb0e_filepath
	 */
	public void setTb0e_filepath(String tb0e_filepath) {
		this.tb0e_filepath = tb0e_filepath;
	}

	/**
	 * [主键id]，当前薪酬付款单在Shr中对应的唯一标识
	 */
	public String getTb0f_dataId() {
		return tb0f_dataId;
	}

	/**
	 * [主键id]，当前薪酬付款单在Shr中对应的唯一标识
	 * @param tb0f_dataId
	 */
	public void setTb0f_dataId(String tb0f_dataId) {
		this.tb0f_dataId = tb0f_dataId;
	}

	
	/********************  以下是付款单FO_PAYPLAN单据头数据   *********************/
	
	/**	
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放月份]字段
	 * 编码	名称
	 * 01	1月  、 02	2月  、 03	3月  、 04	4月  、 05	5月  、 06	6月  、 07	7月  、 08	8月  、 09	9月  、 10	10月
	 * @return
	 */
	public String getTb01_ffyf() {
		return tb01_ffyf;
	}

	/**	
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [发放月份]字段
	 * 编码	名称
	 * 01	1月  、 02	2月  、 03	3月  、 04	4月  、 05	5月  、 06	6月  、 07	7月  、 08	8月  、 09	9月  、 10	10月
	 * @param tb01_ffyf
	 */
	public void setTb01_ffyf(String tb01_ffyf) {
		this.tb01_ffyf = tb01_ffyf;
	}
	
	
	

	/**
	 * 付款计划分录
	 * @return
	 */
	public List<JQInsurancePayPlanDetail> getFo01_payPlan() {
		return fo01_payPlan;
	}

	/**
	 * 付款计划分录
	 * @param fo01_payPlan
	 */
	public void setFo01_payPlan(List<JQInsurancePayPlanDetail> fo01_payPlan) {
		this.fo01_payPlan = fo01_payPlan;
	}

	/**
	 * 付款账户分录
	 * @return
	 */
	public List<JQInsurancePayAccountDetail> getFo02_payAccount() {
		return fo02_payAccount;
	}

	/**
	 * 付款账户分录
	 * @param fo02_payAccount
	 */
	public void setFo02_payAccount(List<JQInsurancePayAccountDetail> fo02_payAccount) {
		this.fo02_payAccount = fo02_payAccount;
	}
	
	


}
