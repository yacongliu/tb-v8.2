package com.kingdee.shr.custom.jq.annuity;

/**
 * 
 * Title: JQAnnuityPayPlanDetail
 * <p>
 * Description: 久其接口年金付款单的付款计划明细分录实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQAnnuityPayPlanDetail {

/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	/**
	 * [外部明细标识]，填写的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	private String tb02_dExternalSysId;
	
	/**
	 * [代码]，填写的类型是字符类型，将映射到单据上的[部门]字段
	 * shr中的部门名称
	 */
	private String tb03_bm;
	
	/**
	 * [企业年金-单位缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	private String tb04_qynj_dw;
	
	/**
	 * [企业年金-个人缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	private String tb05_qynj_gr;


/******************  以下是付款计划分录FO_PAYPLAN_ITEM单据体数据   ******************/
	
	/**
	 * [外部明细标识]，填写的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public String getTb02_dExternalSysId() {
		return tb02_dExternalSysId;
	}

	/**
	 * [外部明细标识]，填写的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，一般是分录ID、EntryId
	 */
	public void setTb02_dExternalSysId(String tb02_dExternalSysId) {
		this.tb02_dExternalSysId = tb02_dExternalSysId;
	}

	/**
	 * [代码]，填写的类型是字符类型，将映射到单据上的[部门]字段
	 * shr中的部门名称
	 */
	public String getTb03_bm() {
		return tb03_bm;
	}

	/**
	 * [代码]，填写的类型是字符类型，将映射到单据上的[部门]字段
	 * shr中的部门名称
	 */
	public void setTb03_bm(String tb03_bm) {
		this.tb03_bm = tb03_bm;
	}

	/**
	 * [企业年金-单位缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public String getTb04_qynj_dw() {
		return tb04_qynj_dw;
	}

	/**
	 * [企业年金-单位缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public void setTb04_qynj_dw(String tb04_qynj_dw) {
		this.tb04_qynj_dw = tb04_qynj_dw;
	}

	/**
	 * [企业年金-个人缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public String getTb05_qynj_gr() {
		return tb05_qynj_gr;
	}

	/**
	 * [企业年金-个人缴纳部分]，填写的类型是数值型类型最大长度是14
	 */
	public void setTb05_qynj_gr(String tb05_qynj_gr) {
		this.tb05_qynj_gr = tb05_qynj_gr;
	} 
	
	
	
	
	
	
	
	
}
