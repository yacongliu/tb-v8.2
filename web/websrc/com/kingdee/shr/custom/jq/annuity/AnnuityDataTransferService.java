package com.kingdee.shr.custom.jq.annuity;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.bsf.service.app.IHRMsfService;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.shr.compensation.util.common.StringUtil;
import com.kingdee.shr.custom.jq.salary.JQSalaryPayAccountDetail;
import com.kingdee.shr.custom.jq.salary.JQSalaryPayPlanDetail;
import com.kingdee.shr.custom.utils.JQXmlUtils;
import com.kingdee.shr.custom.utils.SoapUtil;
import com.kingdee.shr.dataMapHelper.EntityHelper;

/**
 * 
 * Title: AnnuityDataTransferService
 * Description:调用SOAP工具类对接久其接口收发薪酬付款单单据数据OSF
 * 
 * @author 仝飞 Email:tjtongfei@kingdee.com
 * @date 2019年10月10日
 */
public class AnnuityDataTransferService implements IHRMsfService {
	Logger log = Logger.getLogger(AnnuityDataTransferService.class);

	/**
	 *调用SOAP工具类对接久其接口收发年金付款单单据数据的OSF执行方法
	 * @param HashMap<String,Object>类型的参数映射，键值对有
	 * bill：JQSalaryPayBill类型的薪酬付款单单据实体
	 * url：久其webService的服务地址，默认为"http://60.28.230.140:9799/dna_ws/ImpBillWebService"，测试环境可以不填，转正式环境后必填
	 * userName：久其webService的服务登陆用户，默认为jq_hkw，测试环境可以不填，转正式环境后必填
	 * userPWD：久其webService的服务登陆密码，默认为"5CBFEC3C943B0DC19E8288474B330438"，测试环境可以不填，转正式环境后必填
	 * @return HashMap<String,String>类型的结果映射，键值对有
	 * ----------主要值----------
	 * send：发送的报文
	 * isSuccess：执行成功为"true"，执行失败为"false"||
	 * msg：执行结果的主要说明||
	 * xml：久其接口的返回报文
	 * ----------备用值----------
	 * dataID：单据标识||
	 * billCode：久其系统中的单据编号||
	 * rtnCode：久其系统执行结果代码||
	 * rtnMsg：久其系统执行结果说明
	 */
	@Override
	public Object process(Context ctx, Map param) throws EASBizException, BOSException {
		StringBuffer callChain = new StringBuffer("调用SOAP工具类对接久其接口收发年金付款单单据数据的OSF执行方法：");
		//关键参数NPE处理
		if(param==null) {
			String toLog=callChain.append("关键参数 param 为 null")
					.toString();
			log.error(toLog);
			throw new BOSException(toLog);
		}
		
		// 久其薪酬付款单实体
		Object billObj = param.get("bill");
		if(billObj==null) {
			String toLog=callChain.append("关键参数 param 集合中的bill值 为 null")
					.toString();
			log.error(toLog);
			throw new BOSException(toLog);
		}
		JQAnnuityPayBill bill;
		try {
			bill = (JQAnnuityPayBill)param.get("bill");
		} catch (Exception e1) {
			String toLog=callChain.append("关键参数 param 集合中的bill值 转化成 年金实体类JQAnnuityPayBill失败")
					.toString();
			log.error(toLog);
			throw new BOSException(toLog);
		}
		
		// 久其接口实际访问地址
		Object JiuQiURL_obj = param.get("url");
		String JiuQiURL_ = JiuQiURL_obj==null?JQXmlUtils.JiuQiURL:JiuQiURL_obj.toString();
		Object userName_obj = param.get("userName");
		String userName_ = userName_obj==null?JQXmlUtils.userName:userName_obj.toString();
		Object userPWD_obj = param.get("userPWD");
		String userPWD_ = userPWD_obj==null?JQXmlUtils.userPWD:userPWD_obj.toString();
		
		StringBuffer soapForm = getSOAPFormByBillObj(bill, userName_, userPWD_, callChain);
		String resultStr;
		Map<String,String> resultMap = new HashMap<String,String>();
		resultMap.put("send", soapForm.toString());//2019-10-10:仝飞：确保出现异常后仍能查到发送的报文
		
		try {
			resultStr = SoapUtil.getWebServiceAndSoap(JiuQiURL_, JQXmlUtils.JiuQiClass, JQXmlUtils.JiuQiMethod, soapForm);
			resultMap = JQXmlUtils.parseSOAPResult(resultStr, callChain);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			String toLog=callChain.append("调用soap通讯方法时出现IO异常：").append(e.getMessage())
					.append("\r\n发送报文为：").append(soapForm)
					.toString();
			log.error(toLog);
			resultMap.put("isSuccess","false");
			resultMap.put("msg",toLog);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			String toLog=callChain.append("解析soap通讯方法返回值时出现XML异常：").append(e.getMessage())
					.append("\r\n发送报文为：").append(soapForm)
					.toString();
			log.error(toLog);
			resultMap.put("isSuccess","false");
			resultMap.put("msg",toLog);
		}
		resultMap.put("send", soapForm.toString());//2019-09-25:相建彬：增加发送报文的数据映射
		return resultMap;
	}


	/**
	 * <p>
	 * Title: getSOAPFormByBillObj
	 * </p>
	 * <p>
	 * Description: 将年金付款单实体对象转化为soap报文的方法
	 * </p>
	 * @param bill
	 * @param userName
	 * @param userPWD法链的中文描述
	 * @param callChain 调用链描述，调用本方法的方法名或方
	 * @return
	 * @throws BOSException
	 */
	public StringBuffer getSOAPFormByBillObj(JQAnnuityPayBill bill, String userName, String userPWD,
			StringBuffer callChain) throws BOSException {
		
		if(callChain==null)callChain=new StringBuffer();
		callChain.append("将年金付款单实体对象转化为soap报文的方法：");
		
		if(bill==null) {
			throw new BOSException(callChain.append("参数jqPayBill为空").toString());
		}
		
		if(StringUtil.isNullOrEmpty(userName)) {
			userName = JQXmlUtils.userName;
		}
		
		if(StringUtil.isNullOrEmpty(userPWD)) {
			userPWD = JQXmlUtils.userPWD;
		}
		
		EntityHelper eh = EntityHelper.getEntityHelper();
		//将JQSalaryPayBill对象中所有字符串属性值为null的都改为""
		eh.transferStringFieldsFromNullToEmpty(bill, callChain.toString());
		
		StringBuffer sendSoapString = new StringBuffer();
		
		//薪酬单据头
		StringBuffer billHeadString = new StringBuffer();
		billHeadString.append(
			"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:jiuq=\"http://jiuqi.com.cn\">")
			.append("   <soapenv:Header/>")
			.append("   <soapenv:Body>")
			.append("      <jiuq:parseStrWithAuth>")
			.append("         <xmlStr><![CDATA[")
			.append("<sscinterface systemname=\"kingdee\" billdefine=\"FO_TY_NJFK\" ")
			.append("	unitid=\"").append(bill.getTb0a_unitId()).append("\" ")
			.append("	userid=\"").append(bill.getTb0b_userId()).append("\" ")
			.append("	operatertype=\"").append(bill.getTb0c_operatertype()).append("\" hasimage=\"\" filepath=\"\" " )
			.append("	dataid=\"").append(bill.getTb0f_dataId()).append("\">")
			.append("<bill>")
			.append("	<FO_PAYPLAN>")
			.append("		<TB_FFYF>").append(bill.getTb01_ffyf()).append("</TB_FFYF>")//发放月份
			.append("		<EXTERNALREJECTFLAG>false</EXTERNALREJECTFLAG>")//久其固定值
			.append("	</FO_PAYPLAN>")
			.append("	<FO_PAYPLAN_ITEM>")
			;
		sendSoapString.append(billHeadString);
		
		
		
		//薪酬付款计划明细分录
		StringBuffer payPlanDetailString = new StringBuffer();
		List<JQAnnuityPayPlanDetail> lPlan = bill.getFo01_payPlan();
		if(lPlan==null) {
			throw new BOSException(callChain.append("薪酬明细分录集合fo_payPlan为null").toString());
		}
		int n = lPlan.size();
		for(int i =0 ; i<n ; i++) {
			JQAnnuityPayPlanDetail plan = lPlan.get(i);
			
			//确保JQSalaryPayPlanDetail对象中所有的String属性没有null值，null转化为""
			eh.transferStringFieldsFromNullToEmpty(plan, callChain.toString());
			
			payPlanDetailString
				.append("	<data>")
				.append("		<DEXTERNALSYSID>").append(plan.getTb02_dExternalSysId()).append("</DEXTERNALSYSID>")
				.append("		<TB_BM>").append(plan.getTb03_bm()).append("</TB_BM>")
				.append("		<TB_QYNJ_DW>").append(plan.getTb04_qynj_dw()).append("</TB_QYNJ_DW>")
				.append("		<TB_QYNJ_GR>").append(plan.getTb05_qynj_gr()).append("</TB_QYNJ_GR>")
				.append("	</data>")
			;
			
		}
		sendSoapString
			.append(payPlanDetailString)
			.append("	</FO_PAYPLAN_ITEM>")
			.append("	<FO_PAYACCOUNT>")
			;
		
		//薪酬账户明细分录
		StringBuffer payAccountDetailString = new StringBuffer();
		List<JQAnnuityPayAccountDetail> lAcct = bill.getFo02_payAccount();
		if(lAcct==null) {
			throw new BOSException(callChain.append("年金账户分录集合fo_payAccount为null").toString());
		}
		n = lAcct.size();
		for(int i=0; i<n; i++) {
			JQAnnuityPayAccountDetail acct = lAcct.get(i);
			//确保JQSalaryPayAccountDetail对象的所有String类型属性不为null，为null的转化为“”
			eh.transferStringFieldsFromNullToEmpty(acct, callChain.toString());
			
			payAccountDetailString
				.append("	<data>")
				.append("	<BANK>").append(acct.getTb06_bank()).append("</BANK>")//收款银行
				.append("	<DEXTERNALSYSID>").append(acct.getTb07_dExternalSysId()).append("</DEXTERNALSYSID>")//分录标识
				.append("	<SUPPLIER>").append(acct.getTb08_supplier()).append("</SUPPLIER>")//供应商
				.append("	<BANKNUMBER>").append(acct.getTb09_bankNumber()).append("</BANKNUMBER>")//银行卡号
				.append("	</data>")
				;
		}
		sendSoapString
			.append(payAccountDetailString)
			.append("	</FO_PAYACCOUNT>")
			.append("</bill>")
			.append("</sscinterface>")
			.append("]]></xmlStr>")
			.append("        <userName>").append(userName).append("</userName>")
			.append("         <userPWD>").append(userPWD).append("</userPWD>")
			.append("      </jiuq:parseStrWithAuth>")
			.append("   </soapenv:Body>")
			.append("</soapenv:Envelope>")
			.append("")
			;
		
		return sendSoapString;
	}
}
