package com.kingdee.shr.custom.jq.annuity;

/**
 * 
 * Title: JQAnnuityPayAccountDetail
 * <p>
 * Description: 久其接口年金付款单的付款账户分录实体
 * 
 * @author 仝飞
 * @date 2019-10-07 & 下午02:20:31
 * @since V1.0
 */
public class JQAnnuityPayAccountDetail {
	/*******************  以下为FO_PAYACCOUNT  ********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	private String tb06_bank;
	
	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	private String tb07_dExternalSysId;
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	private String tb08_supplier;
	
	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	private String tb09_bankNumber;

	
	/*******************  以下为FO_PAYACCOUNT  ********************/
	
	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public String getTb06_bank() {
		return tb06_bank;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [开户行]字段
	 * SHR中的银行编码
	 */
	public void setTb06_bank(String tb06_bank) {
		this.tb06_bank = tb06_bank;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public String getTb07_dExternalSysId() {
		return tb07_dExternalSysId;
	}

	/**
	 * [外部明细标识]，久其接口的类型是字符型类型最大长度是100
	 * 分录的自定义分录标识，可以是分录ID、EntryId
	 */
	public void setTb07_dExternalSysId(String tb07_dExternalSysId) {
		this.tb07_dExternalSysId = tb07_dExternalSysId;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public String getTb08_supplier() {
		return tb08_supplier;
	}

	/**
	 * [代码]，久其接口的类型是字符类型，将映射到单据上的
	 * [供应商]字段
	 * SHR中的供应商编码
	 */
	public void setTb08_supplier(String tb08_supplier) {
		this.tb08_supplier = tb08_supplier;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public String getTb09_bankNumber() {
		return tb09_bankNumber;
	}

	/**
	 * [银行卡号]，久其接口的类型是字符型类型最大长度是100
	 */
	public void setTb09_bankNumber(String tb09_bankNumber) {
		this.tb09_bankNumber = tb09_bankNumber;
	}
	
	
	
	
	
	
	
}
