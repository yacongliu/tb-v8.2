package com.kingdee.shr.custom.handler.personchange;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.basedata.org.PositionInfo;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.HRBizDefineInfo;
import com.kingdee.eas.hr.emp.PersonChangeInfo;
import com.kingdee.eas.hr.emp.web.handler.PersonChangeEditHandler;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;

/**
 * 
 * @description 员工信息维护-企业任职经历-员工变动 Handler扩展类
 * @title PersonChangeEditHandlerEx
 * @copyright 天津金蝶软件有限公司
 * @author Jambin Email:tjxiangjianbin@kingdee.com
 * @date 2020年3月6日
 */
public class PersonChangeEditHandlerEx extends PersonChangeEditHandler {
    Context ctx = SHRContext.getInstance().getContext();

    /**
     * 
     * @description 附件反写：员工变动保存以后，触发交换附件与业务对象关系表 业务对象内码 （需求：员工变动上传的附件在企业任职经历多行表中显示）
     * @title afterSave
     * @param request
     * @param response
     * @param model
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年3月6日
     */
    @Override
    protected void afterSave(HttpServletRequest request, HttpServletResponse response, CoreBaseInfo model)
            throws SHRWebException {

        super.afterSave(request, response, model);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        PersonChangeInfo changeInfo = (PersonChangeInfo) model;
        System.out.println("==============员工变动：" + changeInfo.toString());
        
        String oldId = request.getParameter("billId"); // 附件关联内码
        Date date = changeInfo.getEFFDT();// 生效日期
        PersonInfo person = changeInfo.getPerson(); // 人员
        HRBizDefineInfo actionDef = changeInfo.getActionDef();// 变动操作: 调用 兼职 等
        PositionInfo newPosition = changeInfo.getNewPosition();// 目标职位
        // 校验
        String personId = null;
        String effdt = null;
        String actionDefId = null;
        String newPositionId = null;

        if (person != null) {
            personId = person.getId().toString();
        }
        if (date != null) {
            effdt = format.format(changeInfo.getEFFDT());
        }
        if (actionDef != null) {
            actionDefId = actionDef.getId().toString();
        }
        if (newPosition != null) {
            newPositionId = newPosition.getId().toString();
        }

        // 根据人员，生效日期，目标职位等查询企业任职经历单据内码
        try {
            String newId = getPersonChangeId(personId, actionDefId, newPositionId, effdt);
            // 调用附件反写工具类，进行反写
            ReviseAttachmentUtil.reviseAttachment(newId, oldId);
        } catch (BOSException e) {
            System.out.println("根据人员，生效日期，目标职位等查询企业任职经历单据内码异常" + e);
            return;
        } catch (EASBizException e) {
            System.out.println("调用附件反写工具类，进行反写时发生异常" + e);
            return;
        }
  
    }

    /**
     * @description 根据人员，生效日期，目标职位等查询企业任职经历单据内码
     * @title getPersonChangeId
     * @param personId 人员id
     * @param actionDefId 变动操作id
     * @param newPositionId 目标职位id
     * @param effdt 开始日期
     * @return id 企业任职经历内码
     * @throws BOSException
     * @author Jambin
     * @date 2020年3月6日
     */
    private String getPersonChangeId(String personId, String actionDefId, String newPositionId, String effdt)
            throws BOSException {
        if (StringUtils.isBlank(personId) || StringUtils.isBlank(actionDefId)
                || StringUtils.isBlank(newPositionId) || StringUtils.isBlank(effdt)) {
            System.out.println("根据人员，生效日期，目标职位等查询企业任职经历单据内码，查询参数为空！");
            return null;
        }
        String id = null;
        String sql = "/*dialect*/ select fid from T_HR_EmpOrgRelation where fpersonid='" + personId
                + "' and factionid='" + actionDefId + "' and fpositionid='" + newPositionId
                + "' and feffdt= to_date('" + effdt + "','yyyy-mm-dd hh24:mi:ss')";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                id = rowSet.getString("fid");
            }
        } catch (BOSException e) {
            throw new BOSException("根据人员，生效日期，目标职位等查询企业任职经历单据内码异常", e);
        } catch (SQLException e) {
            throw new BOSException("根据人员，生效日期，目标职位等查询企业任职经历单据内码异常", e);
        }

        return id;

    }

}
