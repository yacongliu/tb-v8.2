package com.kingdee.shr.custom.handler.personchange;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.base.attachment.BoAttchAssoCollection;
import com.kingdee.eas.base.attachment.BoAttchAssoFactory;
import com.kingdee.eas.base.attachment.BoAttchAssoInfo;
import com.kingdee.eas.base.attachment.IBoAttchAsso;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.util.StringUtils;

/**
 * 
 * @description 附件反写工具类
 * @title ReviseAttachmentUtil
 * @copyright 天津金蝶软件有限公司
 * @author Jambin Email:tjxiangjianbin@kingdee.com
 * @date 2020年3月6日
 */
public class ReviseAttachmentUtil {

    /**
     * 
     * @description 附件反写
     * @title reviseAttachment
     * @param newBoId 新关联单据内码
     * @param oldBoId 旧关联单据内码
     * @author Jambin
     * @throws BOSException
     * @throws EASBizException
     * @throws ShrWebBizException
     * @date 2020年3月6日
     */
    public static void reviseAttachment(String newBoId, String oldBoId)
            throws BOSException, EASBizException, ShrWebBizException {

        if (StringUtils.isEmpty(newBoId) || StringUtils.isEmpty(oldBoId)) {
            System.out.println("******ReviseAttachmentUtil_reviseAttachment 入参参数为空！");
            throw new ShrWebBizException("附件反写失败！");
        }

        System.out.println(
                "******ReviseAttachmentUtil_reviseAttachment 入参：newBoId：" + newBoId + " oldBoId: " + oldBoId);

        IBoAttchAsso iBoAttchAsso = BoAttchAssoFactory.getRemoteInstance();
        BoAttchAssoCollection collection = iBoAttchAsso
                .getBoAttchAssoCollection(" where boID = '" + oldBoId + "'");

        if (collection != null && collection.size() > 0) {
            for (int i = 0; i < collection.size(); i++) {
                BoAttchAssoInfo boAttchAssoInfo = collection.get(i);
                String uuid = boAttchAssoInfo.getId().toString();
                boAttchAssoInfo.setBoID(newBoId);

                iBoAttchAsso.update(new ObjectUuidPK(uuid), boAttchAssoInfo);

                System.out.println("******附件反写业务对象内码已完成******");
            }
        }

    }

}
