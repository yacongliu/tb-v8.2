package com.kingdee.shr.custom.handler.atsexception;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.IMetaDataLoader;
import com.kingdee.bos.metadata.MetaDataLoaderFactory;
import com.kingdee.bos.metadata.entity.EntityObjectInfo;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: AtsExceptiontionBillListHandler <br>
 *            Description: 考勤异常单-Web端
 * @author 杨旭东 Email:tjyangxudong@kingdee.com
 * @date 2019-8-1
 */
public class AtsExceptiontionBillListHandler extends ListHandler {

    private static Logger logger = Logger
            .getLogger(com.kingdee.shr.custom.handler.atsexception.AtsExceptiontionBillListHandler.class);

    private Context ctx;

    public AtsExceptiontionBillListHandler(Context ctx) {
        this.ctx = ctx;
    }

    public AtsExceptiontionBillListHandler() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * 
     * <p>
     * Title: isApprovedAction
     * </p>
     * <p>
     * Description: 判断单据是否审批通过状态
     * </p>
     *
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws ShrWebBizException
     */
    public void isApproveAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException {
        String billId = request.getParameter("billId");
        String tableName = getTableNameByBillId(billId);
        String billState = getBillState(tableName, billId);
        if ("3".equals(billState)) {
            // 该单据已审批通过
            modelMap.put("msg", true);

        } else {
            modelMap.put("msg", false);
        }

        try {
            JSONUtils.SUCCESS(modelMap);
        } catch (SHRWebException e) {
            e.printStackTrace();
            throw new ShrWebBizException("反审批，获取单据状态失败！" + e);
        }

    }

    /**
     * Title: getBillStateAction *
     * <p>
     * Description:判断是否可以删除
     * <p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws ShrWebBizException
     */
    public void isDeleteAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException {
        String billId = request.getParameter("billId");
        System.out.println("单据ID" + billId);
        String tableName = getTableNameByBillId(billId);
        String billState = getBillState(tableName, billId);
        System.out.println("单据状态为：" + billState);
        if ("0".equals(billState) || "".equals(billState)) {
            modelMap.put("msg", "SUCCESS");

        } else {

            modelMap.put("msg", "Erorr");
        }
        try {
            JSONUtils.SUCCESS(modelMap);
        } catch (SHRWebException e) {
            e.printStackTrace();
            throw new ShrWebBizException("删除失败，获取单据状态失败！" + e);
        }
    }

    /**
     * 
     * <p>
     * Title: againstApproveAction
     * </p>
     * <p>
     * Description:反审批
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws ShrWebBizException
     */
    public void againstApproveAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws ShrWebBizException {
        String billId = request.getParameter("billId");
        String tableName = getTableNameByBillId(billId);
        // 反审批单据
        String msg = againstBillState(tableName, billId);
        modelMap.put("flag", msg);
        try {
            JSONUtils.writeJson(response, modelMap);
        } catch (SHRWebException e) {
            e.printStackTrace();
            throw new ShrWebBizException("反审批失败！" + e);
        }
    }

    /**
     * 
     * <p>
     * Title: getBillState
     * </p>
     * <p>
     * Description: 获取单据状态
     * </p>
     * 
     * @param tableName 表名
     * @param fid 编号
     * @return
     * @throws ShrWebBizException
     */
    private String getBillState(String tableName, String fid) throws ShrWebBizException {
        if (StringUtils.isEmpty(tableName) || StringUtils.isEmpty(fid)) {
            throw new ShrWebBizException(" 获取单据状态失败，无法进行反审核！");
        }
        String billState = "";
        String sql = " select fbillstate from " + tableName + " where fid = '" + fid + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            if (rowSet.next()) {
                billState = rowSet.getString("fbillstate");
            }
        } catch (BOSException e) {
            e.printStackTrace();
            throw new ShrWebBizException("获取单据状态成功！", e);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ShrWebBizException("获取单据状态失败！", e);
        }

        return billState;

    }

    /**
     * 
     * <p>
     * Title: againstBillState
     * </p>
     * <p>
     * Description:设置单据状态为 0 -> 反审批单据
     * </p>
     * 
     * @param tableName
     * @param fid
     * @return msg 1: 设置单据状态 未提交 成功
     * @throws ShrWebBizException
     */
    private String againstBillState(String tableName, String fid) throws ShrWebBizException {
        if (StringUtils.isEmpty(tableName) || StringUtils.isEmpty(fid)) {
            throw new ShrWebBizException(" 反审批失败！");
        }
        String msg = "";
        String sql = " update " + tableName + " set fbillstate = '0'  where fid = '" + fid + "'";
        try {
            DbUtil.execute(ctx, sql);
            msg = "1";
        } catch (BOSException e) {
            e.printStackTrace();
            logger.error("设置单据状态 为 未提交 失败！" + e);
        }

        return msg;

    }

    private String getTableNameByBillId(String billId) {
        if (StringUtils.isNotEmpty(billId)) {
            BOSUuid uuid = BOSUuid.read(billId);
            BOSObjectType bosType = uuid.getType();
            IMetaDataLoader metadataloader = MetaDataLoaderFactory.getRemoteMetaDataLoader();
            EntityObjectInfo entity = metadataloader.getEntity(bosType);
            String tableName = entity.getTable().getName();

            return tableName;
        }
        return null;
    }
}
