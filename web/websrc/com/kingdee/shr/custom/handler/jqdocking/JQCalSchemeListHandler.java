package com.kingdee.shr.custom.handler.jqdocking;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.shr.base.syssetting.web.dynamic.model.FilterItemInfo;
import com.kingdee.shr.base.syssetting.web.dynamic.model.QuerySolutionInfo;
import com.kingdee.shr.compensation.web.handler.CalSchemeListHandler;

/**
 * @description 久其生成费用分配单列表Handler
 * @title JQCalSchemeListHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月17日
 */
public class JQCalSchemeListHandler extends CalSchemeListHandler {
    /**
     * @description 自定义查询方案
     * @title afterGetDefaultSolutionInfo
     * @param request
     * @param querySolutionInfo 查询条件
     * @return querySolutionInfo 查询条件
     * @see com.kingdee.shr.base.syssetting.web.handler.ListHandler#afterGetDefaultSolutionInfo(javax.servlet.http.HttpServletRequest,
     *      com.kingdee.shr.base.syssetting.web.dynamic.model.QuerySolutionInfo)
     */
    public QuerySolutionInfo afterGetDefaultSolutionInfo(HttpServletRequest request,
            QuerySolutionInfo querySolutionInfo) {
        querySolutionInfo = super.afterGetDefaultSolutionInfo(request, querySolutionInfo);
        if (null == querySolutionInfo) {
            querySolutionInfo = new QuerySolutionInfo();
        }
        Calendar calendar = Calendar.getInstance();
        // 当前年
        String currnetYear = String.valueOf(calendar.get(Calendar.YEAR));
        // 当前月
        String currnetMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        // 查询方案名称
        String defaultQueryName = "统计年为" + currnetYear + "年 并且 统计月为" + currnetMonth + "月";
        if (null != querySolutionInfo.getName()) {
            querySolutionInfo.setName(querySolutionInfo.getName() + " and " + defaultQueryName);
        } else {
            querySolutionInfo.setName(defaultQueryName);
        }
        com.kingdee.shr.base.syssetting.web.dynamic.model.FilterItemInfo filterYear = new com.kingdee.shr.base.syssetting.web.dynamic.model.FilterItemInfo();
        filterYear.setFieldName("periodYear");
        filterYear.setCompareType(CompareType.EQUALS.toString());
        filterYear.setValue(currnetYear);
        com.kingdee.shr.base.syssetting.web.dynamic.model.FilterItemInfo filterMonth = new com.kingdee.shr.base.syssetting.web.dynamic.model.FilterItemInfo();
        filterMonth.setFieldName("periodMonth");
        filterMonth.setCompareType(CompareType.EQUALS.toString());
        filterMonth.setValue(currnetMonth);
        if (null != querySolutionInfo.getItems()) {
            querySolutionInfo.getItems().add(filterYear);
            querySolutionInfo.getItems().add(filterMonth);
        } else {
            List<com.kingdee.shr.base.syssetting.web.dynamic.model.FilterItemInfo> items = new ArrayList<FilterItemInfo>();
            items.add(filterYear);
            items.add(filterMonth);
            querySolutionInfo.setItems(items);
        }
        querySolutionInfo.setDefaultSolution(true);
        querySolutionInfo.setId("defaultSolution");

        return querySolutionInfo;

    }

}
