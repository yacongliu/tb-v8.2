package com.kingdee.shr.custom.handler.jqdocking.vo;

/**
 * 
 * @description 薪酬项目Vo
 * @title CmpItemVo
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月20日
 */
public class CmpItemVo {
    private String id;

    private String name;

    private Integer fieldSn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFieldSn() {
        return fieldSn;
    }

    public void setFieldSn(Integer fieldSn) {
        this.fieldSn = fieldSn;
    }

    @Override
    public String toString() {
        return "CmpItemVo [id=" + id + ", name=" + name + ", fieldSn=" + fieldSn + "]";
    }

}
