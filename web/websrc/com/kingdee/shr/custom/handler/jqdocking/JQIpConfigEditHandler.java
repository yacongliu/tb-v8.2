package com.kingdee.shr.custom.handler.jqdocking;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.Context;
import com.kingdee.eas.custom.jqdocking.JQIpConfigInfo;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.custom.jq.serviceTest.TestDataTransferService;

/**
 * @description 久其接口配置表单Handler
 * @title JQIpConfigEditHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年10月8日
 */
public class JQIpConfigEditHandler extends EditHandler {
    Context ctx = SHRContext.getInstance().getContext();

    // 付款单工具类
    SlipUtil slipUtil = SlipUtil.getInstance();

    /**
     * @description 链接测试
     * @title testAction
     * @author 相建彬
     * @throws SHRWebException
     */
    @SuppressWarnings("unchecked")
    public void testAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        // 获取久其接口配置信息
        JQIpConfigInfo ipConfigInfo = slipUtil.getJqIpConfigInfo();
        System.out.println("久其接口配置信息URL：" + ipConfigInfo.getJiuQiURL() + ",用户：" + ipConfigInfo.getUser()
                + ",密码：" + ipConfigInfo.getPassword());
        // 调用测试接口service
        TestDataTransferService service = new TestDataTransferService();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("userName", ipConfigInfo.getUser());
        param.put("userPWD", ipConfigInfo.getPassword());
        param.put("url", ipConfigInfo.getJiuQiURL());
        Object object = service.process(null, param);
        HashMap<String, String> m = (HashMap<String, String>) object;
        // 判断链接是否通讯
        if ("false".equals(m.get("isSuccess"))) {
            throw new ShrWebBizException("SHR:接口校验未通过，请检查修正后再处理");
        }
        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }
}
