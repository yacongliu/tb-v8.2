package com.kingdee.shr.custom.handler.jqdocking;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * 
 * @description 久其接口配置列表Handler
 * @title JQIpConfigListHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年10月8日
 */
public class JQIpConfigListHandler extends ListHandler {
    Context ctx = SHRContext.getInstance().getContext();

    /**
     * @description 查询列表中数据的数量
     * @title getListNumber
     * @return number 数量
     * @author 相建彬
     * @throws SHRWebException
     */
    public void getListNumberAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        int number = 0;
        String sql = "SELECT FID FROM CT_JQ_JQIpConfig";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            number = rowSet.size();
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询久其接口配置数量时出错", e);
        }
        
        Map<String, String> map = new HashMap<String, String>(2);
        map.put("state", "success");
        map.put("number", String.valueOf(number));
        JSONUtils.writeJson(response, map);
    }
}
