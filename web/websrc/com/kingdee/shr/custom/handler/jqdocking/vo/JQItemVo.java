package com.kingdee.shr.custom.handler.jqdocking.vo;

/**
 * 
 * @description 久其项目Vo
 * @title JQItemVo 
 * @copyright 天津金蝶软件有限公司 
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月20日
 */

import java.util.Set;

public class JQItemVo {
    private String id;

    private String name;

    private Set<CmpItemVo> cmpItemVos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CmpItemVo> getCmpItemVos() {
        return cmpItemVos;
    }

    public void setCmpItemVos(Set<CmpItemVo> cmpItemVos) {
        this.cmpItemVos = cmpItemVos;
    }

    @Override
    public String toString() {
        return "JQItemVo [id=" + id + ", name=" + name + ", cmpItemVos=" + cmpItemVos + "]";
    }

}
