package com.kingdee.shr.custom.handler.jqdocking;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.SocialSecuritySlipFactory;
import com.kingdee.eas.custom.jqdocking.SocialSecuritySlipInfo;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * @description 社保付款单——列表handler
 * @title SocialSecuritySlipListHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月25日
 */
public class SocialSecuritySlipListHandler extends ListHandler {
    Context ctx = SHRContext.getInstance().getContext();

    /**
     * @description 获取单据状态
     * @title getStateAction
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws SHRWebException
     */
    public void getStateAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        String billId = request.getParameter("billId");
        if (StringUtils.isBlank(billId)) {
            throw new ShrWebBizException("SHR:数据未知");
        }
        SocialSecuritySlipInfo securitySlipInfo = getSecuritySlipInfo(billId);
        String state = securitySlipInfo.getState().getValue();
        if ("1".equals(state)) {
            modelMap.put("msg", "error");
        } else {
            modelMap.put("msg", "success");
        }
        JSONUtils.SUCCESS(modelMap);
    }

    /**
     * @description 根据单据编号获取社保付款单信息
     * @title getSecuritySlipInfo
     * @param billId 单据编号
     * @return securitySlipInfo 社保付款单信息
     * @author 相建彬
     * @throws SHRWebException
     */
    private SocialSecuritySlipInfo getSecuritySlipInfo(String billId) throws SHRWebException {
        if (StringUtils.isBlank(billId)) {
            throw new ShrWebBizException("SHR:找不到相关的社保付款单信息");
        }
        SocialSecuritySlipInfo securitySlipInfo = null;
        try {
            securitySlipInfo = SocialSecuritySlipFactory.getRemoteInstance()
                    .getSocialSecuritySlipInfo(new ObjectUuidPK(billId));
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:查询社保付款单信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询社保付款单信息时出错", e);
        }

        return securitySlipInfo;
    }
}
