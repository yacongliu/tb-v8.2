package com.kingdee.shr.custom.handler.jqdocking;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.base.permission.UserFactory;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.assistant.AccountBankFactory;
import com.kingdee.eas.basedata.assistant.AccountBankInfo;
import com.kingdee.eas.basedata.assistant.BankFactory;
import com.kingdee.eas.basedata.assistant.BankInfo;
import com.kingdee.eas.basedata.master.cssp.SupplierFactory;
import com.kingdee.eas.basedata.master.cssp.SupplierInfo;
import com.kingdee.eas.basedata.org.AdminOrgUnitFactory;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.JQIpConfigCollection;
import com.kingdee.eas.custom.jqdocking.JQIpConfigFactory;
import com.kingdee.eas.custom.jqdocking.JQIpConfigInfo;
import com.kingdee.eas.custom.jqdocking.JQitemControlEntryCollection;
import com.kingdee.eas.custom.jqdocking.JQitemControlEntryFactory;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.jdbc.rowset.IRowSetMetaData;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.custom.handler.jqdocking.vo.CmpItemVo;
import com.kingdee.shr.custom.handler.jqdocking.vo.JQItemVo;

/**
 * @description 久其付款单工具类
 * @title SlipUtil
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月23日
 */
public class SlipUtil {
    private static SlipUtil instance = new SlipUtil();

    Context ctx = SHRContext.getInstance().getContext();

    private SlipUtil() {
    }

    public static SlipUtil getInstance() {
        return instance;
    }

    /**
     * 
     * @description 获取薪酬核算信息
     * @title getCmpCalTableinfo
     * @param map 计算规则的相关信息，jqMap 薪酬项目对照的相关信息
     * @return cmpTableList 查询集合
     * @author 相建彬
     * @throws SHRWebException
     */
    public List<Map<String, String>> getCmpCalTableinfo(Map<String, String> map, Map<String, JQItemVo> jqMap,
            String billType, Context ctx) throws SHRWebException {
        StringBuffer sqlBuffer = assemblySql(map, jqMap, billType);
        System.out.println("********************查询薪酬核算数据的sql为：" + sqlBuffer + "********************");
        // 新建存放查询的薪酬核算数据的集合
        List<Map<String, String>> cmpTableList = new ArrayList<Map<String, String>>();
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sqlBuffer.toString());
            IRowSetMetaData metaData = rowSet.getRowSetMetaData();
            while (rowSet.next()) {
                Map<String, String> cmpTableMap = new HashMap<String, String>();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    if ("FCOSTBEARORGID".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("costBearOrgid", rowSet.getString("FCostbearOrgid"));
                    }
                    if ("工资".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("wage", rowSet.getBigDecimal("工资").toString());
                    }
                    if ("津贴和补贴".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("subsidy", rowSet.getBigDecimal("津贴和补贴").toString());
                    }
                    if ("福利费".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("welfareFee", rowSet.getBigDecimal("福利费").toString());
                    }
                    if ("交通补贴".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("transportSubsidy", rowSet.getBigDecimal("交通补贴").toString());
                    }
                    if ("工会会员费".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unionMemberFee", rowSet.getBigDecimal("工会会员费").toString());
                    }
                    if ("单位养老保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitPension", rowSet.getBigDecimal("单位养老保险").toString());
                    }
                    if ("单位医疗保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitMedical", rowSet.getBigDecimal("单位医疗保险").toString());
                    }
                    if ("单位失业保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitUnemployment", rowSet.getBigDecimal("单位失业保险").toString());
                    }
                    if ("单位工伤保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitInjury", rowSet.getBigDecimal("单位工伤保险").toString());
                    }
                    if ("单位生育保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitMaternity", rowSet.getBigDecimal("单位生育保险").toString());
                    }
                    if ("个人养老保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("perPension", rowSet.getBigDecimal("个人养老保险").toString());
                    }
                    if ("个人医疗保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("perMedical", rowSet.getBigDecimal("个人医疗保险").toString());
                    }
                    if ("个人失业保险".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("perUnemployment", rowSet.getBigDecimal("个人失业保险").toString());
                    }
                    if ("单位公积金".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitProvidentFund", rowSet.getBigDecimal("单位公积金").toString());
                    }
                    if ("个人公积金".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("perProvidentFund", rowSet.getBigDecimal("个人公积金").toString());
                    }
                    if ("补充公积金".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("addProvidentFund", rowSet.getBigDecimal("补充公积金").toString());
                    }
                    if ("单位企业年金".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("unitAnnuity", rowSet.getBigDecimal("单位企业年金").toString());
                    }
                    if ("个人企业年金".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("perAnnuity", rowSet.getBigDecimal("个人企业年金").toString());
                    }
                    if ("个所税扣款".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("personalIncomeTax", rowSet.getBigDecimal("个所税扣款").toString());
                    }
                    if ("实发工资额".equals(metaData.getColumnName(i))) {
                        cmpTableMap.put("realWage", rowSet.getBigDecimal("实发工资额").toString());
                    }
                }
                cmpTableList.add(cmpTableMap);
            }
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询薪酬核算信息时出现错误，请检查薪酬项目对照关系是否正确或联系管理员！", e);
        } catch (SQLException e) {
            throw new SHRWebException("SHR:查询薪酬核算信息时出现错误，请检查薪酬项目对照关系是否正确或联系管理员！", e);
        }

        return cmpTableList;
    }

    /**
     * @description 获取计算规则信息
     * @title getSchemeInfo
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws SHRWebException
     */
    public Map<String, String> getSchemeInfo(String billId, Context ctx) throws SHRWebException {
        Map<String, String> map = new HashMap<String, String>();
        String sql = "SELECT FID,FAdminOrgUnitID,FCalTime,FPERIODYEAR,FPERIODMONTH,FCALSTATE,CFISJQ,CFSUPPLIERID,CFBANKID,CFBANKACCOUNTID,CFBANKACCOUNTSTR FROM T_HR_SCalScheme WHERE FID ='"
                + billId + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                // id
                map.put("id", rowSet.getString("FID"));
                // 组织
                map.put("orgId", rowSet.getString("FAdminOrgUnitID"));
                // 统计年
                map.put("releaseYear", rowSet.getString("FPERIODYEAR"));
                // 统计月
                map.put("releaseMonth", rowSet.getString("FPERIODMONTH"));
                // 核算次数
                map.put("calTime", String.valueOf(rowSet.getInt("FCalTime")));
                // 状态
                map.put("calState", rowSet.getString("FCALSTATE"));
                // 供应商
                map.put("supplier", rowSet.getString("CFSUPPLIERID"));
                // 银行
                map.put("bank", rowSet.getString("CFBANKID"));
                // 银行账户
                map.put("bankAccount", rowSet.getString("CFBANKACCOUNTID"));
                // 银行账户字符串
                map.put("bankAccountStr", rowSet.getString("CFBANKACCOUNTSTR"));
            }
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询计算规则时出错", e);
        } catch (SQLException e) {
            throw new SHRWebException("SHR:查询计算规则时出错", e);
        }
        return map;
    }

    /**
     * 
     * @description 根据计算规则的组织获取薪酬对照表id
     * @title getControlID
     * @param orgId 组织id
     * @return controlID 项目对照表id
     * @throws SHRWebException
     * @author 相建彬
     */
    public String getControlID(String orgId, Context ctx) throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(orgId)) {
            throw new ShrWebBizException("SHR:找不到相关的薪酬对照信息，请先添加该公司的薪酬项目对照关系哦！");
        }
        String sql = "SELECT FId FROM CT_JQ_JQitemControl WHERE FAdminOrgUnitID='" + orgId
                + "' AND CFState=1";
        String controlID = null;
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                controlID = rowSet.getString("FId");
            }
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询薪酬对照表id时出错", e);
        } catch (SQLException e) {
            throw new SHRWebException("SHR:查询薪酬对照表id时出错", e);
        }

        return controlID;
    }

    /**
     * @description 根据对照表id获取对应分录薪酬项目对照信息
     * @title getJQitemControlEntryCollection
     * @return jqitemControlEntryCollection 项目对照集合
     * @author 相建彬
     * @throws SHRWebException
     */
    public JQitemControlEntryCollection getJQitemControlEntryCollection(String controlID)
            throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(controlID)) {
            throw new ShrWebBizException("SHR:找不到相关的薪酬对照信息，请先添加该公司的薪酬项目对照关系哦！");
        }
        JQitemControlEntryCollection jqitemControlEntryCollection = null;
        String oql = "select id,JQItem.id,JQItem.name,CMPItem.id,CMPItem.name,CMPItem.fieldSn where bill='"
                + controlID + "'";
        try {
            jqitemControlEntryCollection = JQitemControlEntryFactory.getRemoteInstance()
                    .getJQitemControlEntryCollection(oql);

        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询薪酬项目对照信息时出错", e);
        }
        return jqitemControlEntryCollection;
    }

    /**
     * 
     * @description 组装久其项目实体
     * @title assembleJQItemVo
     * @param jqitemControlEntryCollection 薪酬对照集合
     * @return jqItemVo
     * @author 相建彬
     */
    public Map<String, JQItemVo> assembleJQItemVo(JQitemControlEntryCollection jqCollection) {
        // 新建存放薪酬项目对照关系的Map集合
        Map<String, JQItemVo> jqitemMap = new HashMap<String, JQItemVo>();
        // 遍历查询到的分录集合
        for (int i = 0; i < jqCollection.size(); i++) {
            // 新建存放薪酬项目的Set集合
            Set<CmpItemVo> cmpItemVos = new HashSet<CmpItemVo>();
            CmpItemVo cmpItemVo = new CmpItemVo();
            JQItemVo jqItemVo = new JQItemVo();
            /* =======组装薪酬项目数据======= */
            cmpItemVo.setId(jqCollection.get(i).getCMPItem().getId().toString());
            cmpItemVo.setName(jqCollection.get(i).getCMPItem().getName());
            cmpItemVo.setFieldSn(jqCollection.get(i).getCMPItem().getFieldSn());
            // 判断是否已有对应的久其项目，有则直接向他的Set集合添加数据
            if (jqitemMap.containsKey(jqCollection.get(i).getJQItem().getId().toString())) {
                jqitemMap.get(jqCollection.get(i).getJQItem().getId().toString()).getCmpItemVos()
                        .add(cmpItemVo);
            } else {
                // 没有对应的久其项目则新建一条久其项目对应数据
                cmpItemVos.add(cmpItemVo);
                jqItemVo.setCmpItemVos(cmpItemVos);
                jqItemVo.setId(jqCollection.get(i).getJQItem().getId().toString());
                jqItemVo.setName(jqCollection.get(i).getJQItem().getName());

                jqitemMap.put(jqCollection.get(i).getJQItem().getId().toString(), jqItemVo);
            }
        }

        return jqitemMap;
    }

    /**
     * @description 获取唯一编码
     * @title getRandomId
     * @return stringBuffer 唯一编码
     * @author 相建彬
     */
    public String getRandomId() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmm");

        int number = (int) (1 + Math.random() * (100 - 1 + 1));
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("JQ").append(sdf.format(new Date())).append(number);

        return stringBuffer.toString();
    }

    /**
     * @description 获取久其接口配置信息
     * @title getJqIpConfigInfo
     * @return ipConfigInfo 配置信息
     * @author 相建彬
     * @throws SHRWebException
     */
    public JQIpConfigInfo getJqIpConfigInfo() throws SHRWebException {
        JQIpConfigInfo ipConfigInfo = null;
        try {
            JQIpConfigCollection ipConfigCollection = JQIpConfigFactory.getRemoteInstance()
                    .getJQIpConfigCollection();
            for (int i = 0; i < ipConfigCollection.size(); i++) {
                ipConfigInfo = ipConfigCollection.get(0);
            }
        } catch (BOSException e) {
            throw new SHRWebException("SHR:获取久其接口配置信息时出现错误", e);
        }
        return ipConfigInfo;
    }

    /**
     * 
     * @description 获取组织信息
     * @title getOrgUnitInfo
     * @param orgName
     * @return adminOrgUnitInfo 组织信息
     * @throws SHRWebException
     * @author 相建彬
     */
    public AdminOrgUnitInfo getOrgUnitInfo(String id) throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(id)) {
            throw new ShrWebBizException("SHR:找不到相关的组织信息");
        }
        AdminOrgUnitInfo adminOrgUnitInfo = null;
        try {
            adminOrgUnitInfo = AdminOrgUnitFactory.getRemoteInstance()
                    .getAdminOrgUnitInfo(new ObjectUuidPK(id));
        } catch (BOSException e) {
            throw new SHRWebException("SHR:获取组织信息时出现错误", e);
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:获取组织信息时出现错误", e);
        }

        return adminOrgUnitInfo;
    }

    /**
     * @description 获取人员信息
     * @title getUserInfo
     * @param id 人员id
     * @return userInfo 人员信息
     * @author 相建彬
     * @throws SHRWebException
     */
    public UserInfo getUserInfo(String id) throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(id)) {
            throw new ShrWebBizException("SHR:找不到相关的人员信息");
        }
        UserInfo userInfo = null;
        try {
            userInfo = UserFactory.getRemoteInstance().getUserInfo("where id='" + id + "'");
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:获取人员信息时出现错误", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:获取人员信息时出现错误", e);
        }
        return userInfo;
    }

    /**
     * @description 根据用户id获取用户number
     * @title getUserNumber
     * @param id 用户id
     * @return UserInfo 用户
     * @author Jambin
     * @throws SHRWebException
     * @date 2020年5月19日
     */
    /*
     * public UserInfo getUserNumber(String id) throws SHRWebException { if
     * (StringUtils.isBlank(id)) { throw new ShrWebBizException("SHR:找不到相关的人员信息"); } UserInfo
     * userInfo = null; try { userInfo = UserFactory.getRemoteInstance().getUserInfo("where id='" +
     * id + "'"); } catch (EASBizException e) { throw new SHRWebException("SHR:获取人员信息时出现错误", e); }
     * catch (BOSException e) { throw new SHRWebException("SHR:获取人员信息时出现错误", e); } return userInfo;
     * 
     * }
     */

    /**
     * 
     * @description 获取银行信息
     * @title getBankinfo
     * @param id 唯一标识
     * @return bankInfo 银行信息
     * @author 相建彬
     * @throws SHRWebException
     */
    public BankInfo getBankinfo(String id) throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(id)) {
            throw new ShrWebBizException("SHR:找不到相关的银行信息");
        }
        BankInfo bankInfo = null;
        try {
            bankInfo = BankFactory.getRemoteInstance().getBankInfo(new ObjectUuidPK(id));
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:查询银行信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询银行信息时出错", e);
        }
        return bankInfo;
    }

    /**
     * 
     * @description 获取供应商信息
     * @title getSupplierinfo
     * @param id 唯一标识
     * @return SupplierInfo 供应商信息
     * @author 相建彬
     * @throws SHRWebException
     */
    public SupplierInfo getSupplierinfo(String id) throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(id)) {
            throw new ShrWebBizException("SHR:找不到相关的供应商信息");
        }
        SupplierInfo supplierInfo = null;
        try {
            supplierInfo = SupplierFactory.getRemoteInstance().getSupplierInfo(new ObjectUuidPK(id));
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:查询供应商信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询供应商信息时出错", e);
        }
        return supplierInfo;
    }

    /**
     * 
     * @description 获取银行账户信息
     * @title getAccountBankinfo
     * @param id 唯一标识
     * @return AccountBankInfo 银行账户信息
     * @author 相建彬
     * @throws SHRWebException
     */
    public AccountBankInfo getAccountBankinfo(String id) throws ShrWebBizException, SHRWebException {
        if (StringUtils.isBlank(id)) {
            throw new ShrWebBizException("SHR:找不到相关的银行账户信息");
        }
        AccountBankInfo accountBankInfo = null;
        try {
            accountBankInfo = AccountBankFactory.getRemoteInstance().getAccountBankInfo(new ObjectUuidPK(id));
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:查询银行账户信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询银行账户信息时出错", e);
        }
        return accountBankInfo;
    }

    /**
     * @description 组装动态sql
     * @title assemblySql
     * @param map
     * @param jqMap
     * @return sqlBuffer sql语句
     * @author 相建彬
     * @throws SHRWebException
     */
    protected StringBuffer assemblySql(Map<String, String> map, Map<String, JQItemVo> jqMap, String billType)
            throws SHRWebException {
        // 获取是否传付款单的sn
        String sn = getCmpItemFieldSN(ctx);
        String snsql = "' and st.s" + sn + "='是' ";
        // 需要查询的列的动态sql
        StringBuffer selectSql = new StringBuffer();
        // 遍历薪酬对照关系，拼接sql
        for (JQItemVo value : jqMap.values()) {
            StringBuffer sqlBuffer = new StringBuffer(",sum(");
            // 该久其项目对应薪酬项目Set集合
            Iterator<CmpItemVo> iterator = value.getCmpItemVos().iterator();
            while (iterator.hasNext()) {
                CmpItemVo cmpItemVo = iterator.next();
                StringBuffer decode = new StringBuffer("decode(s").append(cmpItemVo.getFieldSn())
                        .append(",null,0,s").append(cmpItemVo.getFieldSn()).append(")+");

                sqlBuffer.append(decode);
            }
            sqlBuffer.append(")").append(value.getName()).deleteCharAt(sqlBuffer.lastIndexOf("+"));
            selectSql.append(sqlBuffer);
        }

        StringBuffer sqlBuffer = new StringBuffer();
        // 只针对薪酬付款单进行是否传付款单过滤
        if ("payslip".equals(billType)) {
            sqlBuffer.append("/*dialect*/ SELECT co.FCostbearOrgid ").append(selectSql.toString()).append(
                    " from T_HR_SCmpCalTable st left join (select * from(select T_HR_SCostBearOrg.*, row_number() over(partition by fpersonid order by feffactdate desc) rn from T_HR_SCostBearOrg)where rn=1 ) co on co.fpersonid=st.fpersonid WHERE st.FCalState='")
                    .append(map.get("calState")).append("' and st.FCalTime='").append(map.get("calTime"))
                    .append("' and st.fperiodyear='").append(map.get("releaseYear"))
                    .append("' and st.fperiodmonth='").append(map.get("releaseMonth"))
                    .append("' and st.fcalschemeid='").append(map.get("id")).append(snsql)
                    .append(" Group by co.FCostbearOrgid");
        } else {
            sqlBuffer.append("/*dialect*/ SELECT co.FCostbearOrgid ").append(selectSql.toString()).append(
                    " from T_HR_SCmpCalTable st left join (select * from(select T_HR_SCostBearOrg.*, row_number() over(partition by fpersonid order by feffactdate desc) rn from T_HR_SCostBearOrg)where rn=1 ) co on co.fpersonid=st.fpersonid WHERE st.FCalState='")
                    .append(map.get("calState")).append("' and st.FCalTime='").append(map.get("calTime"))
                    .append("' and st.fperiodyear='").append(map.get("releaseYear"))
                    .append("' and st.fperiodmonth='").append(map.get("releaseMonth"))
                    .append("' and st.fcalschemeid='").append(map.get("id"))
                    .append("' Group by co.FCostbearOrgid");
        }

        return sqlBuffer;
    }

    /**
     * @description 根据编码获取薪酬项目sn
     * @title getCmpItemFieldSN
     * @return fieldSn 薪酬项目sn
     * @author Jambin
     * @throws SHRWebException
     * @date 2020年2月10日
     */

    private String getCmpItemFieldSN(Context ctx) throws SHRWebException {
        String sql = "SELECT FNAME_L2,FIELDSN FROM T_HR_SCmpItem where fnumber ='SFCFKD'";
        String sn = null;
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                sn = rowSet.getString("FIELDSN");
            }
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询薪酬项目sn时出错", e);
        } catch (SQLException e) {
            throw new SHRWebException("SHR:查询薪酬项目sn时出错", e);
        }
        return sn;

    }
}
