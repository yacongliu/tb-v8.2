package com.kingdee.shr.custom.handler.jqdocking;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.custom.jqdocking.JQItemCollection;
import com.kingdee.eas.custom.jqdocking.JQItemFactory;
import com.kingdee.eas.custom.jqdocking.JQitemControlEntryCollection;
import com.kingdee.eas.custom.jqdocking.JQitemControlFactory;
import com.kingdee.eas.custom.jqdocking.JQitemControlInfo;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.compensation.CmpItemCollection;
import com.kingdee.shr.compensation.CmpItemFactory;

/**
 * @description 久其薪酬项目对照表单Handler
 * @title JQItemControlEditHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月17日
 */
public class JQItemControlEditHandler extends EditHandler {
    /**
     * @description 复制功能
     * @title copyAccountAction
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     * @author 相建彬
     */
    public String copyAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        try {
            Context ctx = SHRContext.getInstance().getContext();
            String billId = request.getParameter("billId");
            SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
            String random = sdf.format(new Date());
            JQitemControlInfo itemControlInfo = JQitemControlFactory.getRemoteInstance()
                    .getJQitemControlInfo(new ObjectUuidPK(billId));
            itemControlInfo.setId(null);
            itemControlInfo.setName(itemControlInfo.getName() + "_复制" + random);
            itemControlInfo.setNumber(itemControlInfo.getNumber() + "_copy" + random);

            JQitemControlEntryCollection controlEntryCollection = itemControlInfo.getEntrys();

            if (controlEntryCollection != null) {
                for (int i = 0; i < controlEntryCollection.size(); i++) {
                    controlEntryCollection.get(i).setId(null);

                    CmpItemCollection cmpItemCollection = getCMPItemInfo(
                            controlEntryCollection.get(i).getCMPItem().getId().toString());
                    JQItemCollection jqItemCollection = getJQItemInfo(
                            controlEntryCollection.get(i).getJQItem().getId().toString());

                    if (cmpItemCollection != null) {
                        for (int j = 0; j < cmpItemCollection.size(); j++) {
                            cmpItemCollection.get(j).setId(null);
                        }
                    }
                    if (jqItemCollection != null) {
                        for (int j = 0; j < jqItemCollection.size(); j++) {
                            jqItemCollection.get(j).setId(null);
                        }
                    }
                }
            }

            IObjectPK newId = JQitemControlFactory.getLocalInstance(ctx).save(itemControlInfo);
            JSONUtils.SUCCESS(newId.toString());
            return null;
        } catch (Exception e) {
            throw new SHRWebException(e.getMessage(), e);
        }
    }

    /**
     * 
     * @description 获取薪酬项目
     * @title getCMPItemInfo
     * @param id 薪酬项目id
     * @return cmpItemCollection 薪酬项目集合
     * @throws SHRWebException
     * @author 相建彬
     */
    private CmpItemCollection getCMPItemInfo(String id) throws SHRWebException {
        CmpItemCollection cmpItemCollection = null;
        try {
            cmpItemCollection = CmpItemFactory.getRemoteInstance()
                    .getCmpItemCollection("where id ='" + id + "'");
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询薪酬项目时出错", e);
        }
        return cmpItemCollection;
    }

    /**
     * @description 获取久其项目信息
     * @title getJQItemInfo
     * @param id 久其项目id
     * @return JQItemCollection 久其项目集合
     * @author 相建彬
     * @throws SHRWebException
     */
    private JQItemCollection getJQItemInfo(String id) throws SHRWebException {
        JQItemCollection jqItemCollection = null;
        try {
            jqItemCollection = JQItemFactory.getRemoteInstance()
                    .getJQItemCollection("where id ='" + id + "'");
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询久其项目时出错", e);
        }
        return jqItemCollection;
    }
}
