package com.kingdee.shr.custom.handler.jqdocking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.DataLimitExceedException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.dao.ormapping.SQLAccessException;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryCollection;
import com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryInfo;
import com.kingdee.eas.custom.jqdocking.AnnuitySlipFactory;
import com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo;
import com.kingdee.eas.custom.jqdocking.JQIpConfigInfo;
import com.kingdee.eas.custom.jqdocking.app.JQSlipState;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.shr.base.syssetting.app.filter.HRFilterUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.costbudget.util.SHRBillUtil;
import com.kingdee.shr.custom.jq.annuity.AnnuityDataTransferService;
import com.kingdee.shr.custom.jq.annuity.JQAnnuityPayAccountDetail;
import com.kingdee.shr.custom.jq.annuity.JQAnnuityPayBill;
import com.kingdee.shr.custom.jq.annuity.JQAnnuityPayPlanDetail;
import com.kingdee.util.DateTimeUtils;

/**
 * @description 年金付款单-表单
 * @title AnnuitySlipEditHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月25日
 */
public class AnnuitySlipEditHandler extends EditHandler {
    Context ctx = SHRContext.getInstance().getContext();

    // 付款单工具类
    SlipUtil slipUtil = SlipUtil.getInstance();

    /**
     * @description 页面初始赋值
     * @title afterCreateNewModel
     * @param request
     * @param response
     * @param coreBaseInfo
     * @throws SHRWebException
     */
    @Override
    protected void afterCreateNewModel(HttpServletRequest request, HttpServletResponse response,
            CoreBaseInfo coreBaseInfo) throws ShrWebBizException, SHRWebException {
        super.afterCreateNewModel(request, response, coreBaseInfo);
        // 获取计算规则id
        String billId = getBillId(request);
        System.out.println("==========计算规则billid为" + billId + "==========");
        // 根据id获取计算规则相关信息的map
        Map<String, String> map = slipUtil.getSchemeInfo(billId, ctx);
        /* ====================表头数据==================== */
        AnnuitySlipInfo annuitySlipInfo = (AnnuitySlipInfo) coreBaseInfo;
        // 单据编号
        // annuitySlipInfo.setNumber(slipUtil.getRandomId());
        // 申请日期
        annuitySlipInfo.setApplyDate(DateTimeUtils.truncateDate(new java.util.Date()));
        // 经办人
        annuitySlipInfo.setApplier(SHRBillUtil.getCurrPersonInfo());
        // 发放月份
        annuitySlipInfo.setReleaseMonth(map.get("releaseMonth"));
        // 银行
        annuitySlipInfo.setBank(slipUtil.getBankinfo(map.get("bank")));
        // 供应商
        annuitySlipInfo.setSupplier(slipUtil.getSupplierinfo(map.get("supplier")));
        // 银行账户
        // annuitySlipInfo.setBankAccount(slipUtil.getAccountBankinfo(map.get("bankAccount")));
        annuitySlipInfo.setBankAccountStr(map.get("bankAccountStr"));
        // 状态（未同步）
        annuitySlipInfo.setState(JQSlipState.NOTSYN);
        // 所属组织
        annuitySlipInfo.setAdminOrg(slipUtil.getOrgUnitInfo(map.get("orgId")));

        /* ====================分录数据==================== */
        // 分录集合
        AnnuitySlipEntryCollection entrys = annuitySlipInfo.getEntrys();
        // 获取薪酬核算表的数据List集合
        List<Map<String, String>> cmpTableList = slipUtil.getCmpCalTableinfo(map,
                slipUtil.assembleJQItemVo(slipUtil
                        .getJQitemControlEntryCollection(slipUtil.getControlID(map.get("orgId"), ctx))),
                "", ctx);
        // 遍历薪酬核算表的数据List集合
        Iterator<Map<String, String>> iterator = cmpTableList.iterator();
        while (iterator.hasNext()) {
            // 分录实体
            AnnuitySlipEntryInfo annuitySlipEntryInfo = new AnnuitySlipEntryInfo();
            // 薪酬核算表的数据map集合
            Map<String, String> cmpTableMap = iterator.next();
            /*-------组装数据-------*/
            // 组织
            if (StringUtils.isNotBlank(cmpTableMap.get("costBearOrgid"))) {
                annuitySlipEntryInfo.setAdminOrg(slipUtil.getOrgUnitInfo(cmpTableMap.get("costBearOrgid")));
            }
            // 单位企业年金
            if (StringUtils.isNotBlank(cmpTableMap.get("unitAnnuity"))) {
                annuitySlipEntryInfo.setUnitAnnuity(new BigDecimal(cmpTableMap.get("unitAnnuity")));
            }
            // 个人企业年金
            if (StringUtils.isNotBlank(cmpTableMap.get("perAnnuity"))) {
                annuitySlipEntryInfo.setPerAnnuity(new BigDecimal(cmpTableMap.get("perAnnuity")));
            }

            entrys.add(annuitySlipEntryInfo);
        }
    }

    /**
     * @description 调用久其接口同步
     * @title synchronizeAction
     * @author 相建彬
     * @throws SHRWebException
     */
    @SuppressWarnings("unchecked")
    public void synchronizeAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        // 获取单据id
        String billId = save(request, response, modelMap);
        System.out.println("********************要与久其同步的单据id为：" + billId + "********************");
        // 获取单据信息
        AnnuitySlipInfo annuitySlipInfo = getAnnuitySlipInfo(billId);
        // 获取单据状态
        String state = annuitySlipInfo.getState().getValue();
        String operationType = null;
        // 判断操作类型
        if ("0".equals(state)) {
            operationType = "add";
        }
        if ("1".equals(state) || "2".equals(state)) {
            operationType = "update";
        }

        JQAnnuityPayBill payBill = new JQAnnuityPayBill();
        // 组装数据调用接口
        assemblyJQinfo(annuitySlipInfo, payBill, billId, operationType);
        AnnuityDataTransferService service = new AnnuityDataTransferService();
        // 新建久其接口需要的参数的map集合
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("bill", payBill);
        // 获取久其接口配置信息
        JQIpConfigInfo ipConfigInfo = slipUtil.getJqIpConfigInfo();
        System.out.println("久其接口配置信息URL：" + ipConfigInfo.getJiuQiURL() + ",用户：" + ipConfigInfo.getUser()
                + ",密码：" + ipConfigInfo.getPassword());
        param.put("userName", ipConfigInfo.getUser());
        param.put("userPWD", ipConfigInfo.getPassword());
        param.put("url", ipConfigInfo.getJiuQiURL());
        System.out.println(payBill.toString());
        try {
            // 调用接口
            Map<String, String> msgMap = (Map<String, String>) service.process(null, param);
            String ustate = JQSlipState.NOTSYN_VALUE;
            String send = null;
            String accept = null;
            // 发送的报文
            System.out.println("发送报文：" + msgMap.get("send"));
            if (StringUtils.isNotBlank(msgMap.get("send"))) {
                send = msgMap.get("send").replaceAll(" ", "").substring(0, 500);
            }
            // 接受的报文
            if (StringUtils.isNotBlank(msgMap.get("xml"))) {
                accept = msgMap.get("xml").replaceAll(" ", "").substring(36);
            }
            // 调用成功则更改单据状态为已同步
            if ("true".equals(msgMap.get("isSuccess"))) {
                ustate = JQSlipState.SYSCED_VALUE;
            }
            // 执行更改操作
            updateAnnuitySlipInfo(billId, ustate, send, accept);
            if ("false".equals(msgMap.get("isSuccess"))) {
                if (msgMap.get("msg").indexOf("IO异常") != -1 || msgMap.get("msg").indexOf("XML异常") != -1) {
                    throw new ShrWebBizException("久其：同步至久其接口时出错，请联系管理员");
                } else {
                    throw new ShrWebBizException("久其" + msgMap.get("msg"));
                }
            }
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:调用久其接口时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:调用久其接口时出错", e);
        }
        writeSuccessData(billId);
    }

    /**
     * @description 同步久其前先保存
     * @title saveAction
     * @param request
     * @return model.getId().toString() 生成的单据id
     * @throws SHRWebException
     * @author 相建彬
     */
    public String save(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        CoreBaseInfo model = null;
        try {
            model = (CoreBaseInfo) request.getAttribute("dynamic_model");

            beforeSave(request, response, model);

            IObjectPK objectPK = runSaveData(request, response, model);
            model.setId(BOSUuid.read(objectPK.toString()));

            afterSave(request, response, model);
        } catch (SQLAccessException e) {
            if ((e.getCause() instanceof DataLimitExceedException)) {
                throw new ShrWebBizException("字段长度超过系统设定的最大长度，无法保存成功", e);
            }
            throw new SHRWebException(e);
        } catch (Exception exception) {
            throw new SHRWebException(exception);
        }
        return model.getId().toString();
    }

    /**
     * @description 组装需要传递久其的数据
     * @title assemblyJQinfo
     * @author 相建彬
     * @throws SHRWebException
     */
    private void assemblyJQinfo(AnnuitySlipInfo annuitySlipInfo, JQAnnuityPayBill payBill, String dataID,
            String operationType) throws SHRWebException {
        List<JQAnnuityPayPlanDetail> planList = new ArrayList<JQAnnuityPayPlanDetail>();
        List<JQAnnuityPayAccountDetail> acctList = new ArrayList<JQAnnuityPayAccountDetail>();
        /* ==================单据头信息================== */
        payBill.setTb0a_unitId(
                slipUtil.getOrgUnitInfo(annuitySlipInfo.getAdminOrg().getId().toString()).getNumber());
        // payBill.setTb0b_userId(slipUtil.getUserInfo(annuitySlipInfo.getApplier().getId().toString()).getNumber());
        payBill.setTb0b_userId(slipUtil.getUserInfo(HRFilterUtils.getCurrentUserId(ctx)).getNumber());
        payBill.setTb0c_operatertype(operationType);
        payBill.setTb0f_dataId(dataID);
        String releaseMonth = null;
        if (annuitySlipInfo.getReleaseMonth().length() == 2) {
            releaseMonth = annuitySlipInfo.getReleaseMonth();
        } else {
            releaseMonth = "0" + annuitySlipInfo.getReleaseMonth();
        }
        payBill.setTb01_ffyf(releaseMonth);
        // payBill.setTb03_fflx(annuitySlipInfo.getReleaseType().getValue());
        /* ==================供应商、银行、银行账户================== */
        JQAnnuityPayAccountDetail payAcct = new JQAnnuityPayAccountDetail();
        payAcct.setTb06_bank(slipUtil.getBankinfo(annuitySlipInfo.getBank().getId().toString()).getNumber());
        payAcct.setTb07_dExternalSysId(dataID);
        payAcct.setTb08_supplier(
                slipUtil.getSupplierinfo(annuitySlipInfo.getSupplier().getId().toString()).getNumber());
        payAcct.setTb09_bankNumber(annuitySlipInfo.getBankAccountStr());
        acctList.add(payAcct);
        /* ==================单据体分录信息================== */
        AnnuitySlipEntryCollection entrys = annuitySlipInfo.getEntrys();
        for (int i = 0; i < entrys.size(); i++) {
            JQAnnuityPayPlanDetail payPlan = new JQAnnuityPayPlanDetail();
            payPlan.setTb02_dExternalSysId(entrys.get(i).getId().toString());
            payPlan.setTb03_bm(
                    slipUtil.getOrgUnitInfo(entrys.get(i).getAdminOrg().getId().toString()).getNumber());

            if (entrys.get(i).getUnitAnnuity() != null) {
                payPlan.setTb04_qynj_dw(entrys.get(i).getUnitAnnuity().toString());
            }
            if (entrys.get(i).getPerAnnuity() != null) {
                payPlan.setTb05_qynj_gr(entrys.get(i).getPerAnnuity().toString());
            }

            planList.add(payPlan);
        }

        payBill.setFo01_payPlan(planList);
        payBill.setFo02_payAccount(acctList);
    }

    /**
     * @description 同步后更改单据状态，增加发送和接受的报文
     * @title updateAnnuitySlipInfo
     * @param id 单据id
     * @param state 状态
     * @param send 发送报文
     * @param accept 接受报文
     * @author 相建彬
     * @throws SHRWebException
     */
    private void updateAnnuitySlipInfo(String id, String state, String send, String accept)
            throws SHRWebException {
        String sql = "update CT_JQ_AnnuitySlip SET CFSTATE='" + state + "',CFSendContent='" + send
                + "',CFAcceptContent='" + accept + "' where fid ='" + id + "'";
        try {
            DbUtil.execute(ctx, sql);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:更改年金付款单信息时出错", e);
        }
    }

    /**
     * @description 根据单据编号获取年金付款单信息
     * @title getAnnuitySlipInfo
     * @param billId 单据id
     * @return annuitySlipInfo 年金付款单信息
     * @author 相建彬
     * @throws SHRWebException
     */
    private AnnuitySlipInfo getAnnuitySlipInfo(String billId) throws SHRWebException {
        if (StringUtils.isBlank(billId)) {
            throw new ShrWebBizException("SHR:找不到相关的年金付款单信息");
        }
        AnnuitySlipInfo annuitySlipInfo = null;
        try {
            annuitySlipInfo = AnnuitySlipFactory.getRemoteInstance()
                    .getAnnuitySlipInfo(new ObjectUuidPK(billId));
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:查询年金付款单信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询年金付款单信息时出错", e);
        }

        return annuitySlipInfo;
    }
}
