package com.kingdee.shr.custom.handler.jqdocking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.DataLimitExceedException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.dao.ormapping.SQLAccessException;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.JQIpConfigInfo;
import com.kingdee.eas.custom.jqdocking.PaySlipEntryCollection;
import com.kingdee.eas.custom.jqdocking.PaySlipEntryInfo;
import com.kingdee.eas.custom.jqdocking.PaySlipFactory;
import com.kingdee.eas.custom.jqdocking.PaySlipInfo;
import com.kingdee.eas.custom.jqdocking.app.JQSlipState;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.shr.base.syssetting.app.filter.HRFilterUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.costbudget.util.SHRBillUtil;
import com.kingdee.shr.custom.jq.salary.JQSalaryPayAccountDetail;
import com.kingdee.shr.custom.jq.salary.JQSalaryPayBill;
import com.kingdee.shr.custom.jq.salary.JQSalaryPayPlanDetail;
import com.kingdee.shr.custom.jq.salary.SalaryDataTransferService;
import com.kingdee.util.DateTimeUtils;

/**
 * @description 薪酬付款单-表单handler
 * @title PaySlipEditHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月17日
 */
public class PaySlipEditHandler extends EditHandler {
    Context ctx = SHRContext.getInstance().getContext();

    // 付款单工具类
    SlipUtil slipUtil = SlipUtil.getInstance();

    /**
     * @description 页面初始赋值
     * @title afterCreateNewModel
     * @param request
     * @param response
     * @param coreBaseInfo
     * @throws SHRWebException
     */
    @Override
    protected void afterCreateNewModel(HttpServletRequest request, HttpServletResponse response,
            CoreBaseInfo coreBaseInfo) throws ShrWebBizException, SHRWebException {
        super.afterCreateNewModel(request, response, coreBaseInfo);
        // 获取计算规则id
        String billId = getBillId(request);
        System.out.println("==========计算规则billid为" + billId + "==========");
        // 根据id获取计算规则相关信息的map
        Map<String, String> map = slipUtil.getSchemeInfo(billId, ctx);
        /* ====================表头数据==================== */
        PaySlipInfo paySlipInfo = (PaySlipInfo) coreBaseInfo;
        // 单据编号
        // paySlipInfo.setNumber(slipUtil.getRandomId());
        // 申请日期
        paySlipInfo.setApplyDate(DateTimeUtils.truncateDate(new java.util.Date()));
        // 经办人
        paySlipInfo.setApplier(SHRBillUtil.getCurrPersonInfo());
        // 发放月份
        paySlipInfo.setReleaseMonth(map.get("releaseMonth"));
        // 银行
        paySlipInfo.setBank(slipUtil.getBankinfo(map.get("bank")));
        // 供应商
        paySlipInfo.setSupplier(slipUtil.getSupplierinfo(map.get("supplier")));
        // 银行账户
        // paySlipInfo.setBankAccount(slipUtil.getAccountBankinfo(map.get("bankAccount")));
        paySlipInfo.setBankAccountStr(map.get("bankAccountStr"));
        // 状态（未同步）
        paySlipInfo.setState(JQSlipState.NOTSYN);
        // 所属组织
        paySlipInfo.setAdminOrg(slipUtil.getOrgUnitInfo(map.get("orgId")));

        /* ====================分录数据==================== */
        // 分录集合
        PaySlipEntryCollection entrys = paySlipInfo.getEntrys();
        // 获取薪酬核算表的数据List集合
        List<Map<String, String>> cmpTableList = slipUtil.getCmpCalTableinfo(map,
                slipUtil.assembleJQItemVo(slipUtil
                        .getJQitemControlEntryCollection(slipUtil.getControlID(map.get("orgId"), ctx))),
                "payslip", ctx);
        // 遍历薪酬核算表的数据List集合
        Iterator<Map<String, String>> iterator = cmpTableList.iterator();
        while (iterator.hasNext()) {
            // 分录实体
            PaySlipEntryInfo paySlipEntryInfo = new PaySlipEntryInfo();
            // 薪酬核算表的数据map集合
            Map<String, String> cmpTableMap = iterator.next();
            /*-------组装数据-------*/
            // 组织
            if (StringUtils.isNotBlank(cmpTableMap.get("costBearOrgid"))) {
                paySlipEntryInfo.setAdminOrg(slipUtil.getOrgUnitInfo(cmpTableMap.get("costBearOrgid")));
            }
            // 工资
            if (StringUtils.isNotBlank(cmpTableMap.get("wage"))) {
                paySlipEntryInfo.setWage(new BigDecimal(cmpTableMap.get("wage")));
            }
            // 津贴和补贴
            if (StringUtils.isNotBlank(cmpTableMap.get("subsidy"))) {
                paySlipEntryInfo.setSubsidy(new BigDecimal(cmpTableMap.get("subsidy")));
            }
            // 福利费
            if (StringUtils.isNotBlank(cmpTableMap.get("welfareFee"))) {
                paySlipEntryInfo.setWelfareFee(new BigDecimal(cmpTableMap.get("welfareFee")));
            }
            // 交通补贴
            if (StringUtils.isNotBlank(cmpTableMap.get("transportSubsidy"))) {
                paySlipEntryInfo.setTransportSubsidy(new BigDecimal(cmpTableMap.get("transportSubsidy")));
            }
            // 工会会员费
            if (StringUtils.isNotBlank(cmpTableMap.get("unionMemberFee"))) {
                paySlipEntryInfo.setUnionMemberFee(new BigDecimal(cmpTableMap.get("unionMemberFee")));
            }
            // 个人养老保险
            if (StringUtils.isNotBlank(cmpTableMap.get("perPension"))) {
                paySlipEntryInfo.setPerPension(new BigDecimal(cmpTableMap.get("perPension")));
            }
            // 个人医疗保险
            if (StringUtils.isNotBlank(cmpTableMap.get("perMedical"))) {
                paySlipEntryInfo.setPerMedical(new BigDecimal(cmpTableMap.get("perMedical")));
            }
            // 个人失业保险
            if (StringUtils.isNotBlank(cmpTableMap.get("perUnemployment"))) {
                paySlipEntryInfo.setPerUnemployment(new BigDecimal(cmpTableMap.get("perUnemployment")));
            }
            // 单位养老保险
            if (StringUtils.isNotBlank(cmpTableMap.get("unitPension"))) {
                paySlipEntryInfo.setUnitPension(new BigDecimal(cmpTableMap.get("unitPension")));
            }
            // 单位医疗保险
            if (StringUtils.isNotBlank(cmpTableMap.get("unitMedical"))) {
                paySlipEntryInfo.setUnitMedical(new BigDecimal(cmpTableMap.get("unitMedical")));
            }
            // 单位失业保险
            if (StringUtils.isNotBlank(cmpTableMap.get("unitUnemployment"))) {
                paySlipEntryInfo.setUnitUnemployment(new BigDecimal(cmpTableMap.get("unitUnemployment")));
            }
            // 单位工伤保险
            if (StringUtils.isNotBlank(cmpTableMap.get("unitInjury"))) {
                paySlipEntryInfo.setUnitInjury(new BigDecimal(cmpTableMap.get("unitInjury")));
            }
            // 单位生育保险
            if (StringUtils.isNotBlank(cmpTableMap.get("unitMaternity"))) {
                paySlipEntryInfo.setUnitMaternity(new BigDecimal(cmpTableMap.get("unitMaternity")));
            }
            // 个人公积金
            if (StringUtils.isNotBlank(cmpTableMap.get("perProvidentFund"))) {
                paySlipEntryInfo.setPerProvidentFund(new BigDecimal(cmpTableMap.get("perProvidentFund")));
            }
            // 单位公积金
            if (StringUtils.isNotBlank(cmpTableMap.get("unitProvidentFund"))) {
                paySlipEntryInfo.setUnitProvidentFund(new BigDecimal(cmpTableMap.get("unitProvidentFund")));
            }
            // 单位补充公积金
            if (StringUtils.isNotBlank(cmpTableMap.get("addProvidentFund"))) {
                paySlipEntryInfo.setAddProvidentFund(new BigDecimal(cmpTableMap.get("addProvidentFund")));
            }
            // 单位企业年金
            if (StringUtils.isNotBlank(cmpTableMap.get("unitAnnuity"))) {
                paySlipEntryInfo.setUnitAnnuity(new BigDecimal(cmpTableMap.get("unitAnnuity")));
            }
            // 个人企业年金
            if (StringUtils.isNotBlank(cmpTableMap.get("perAnnuity"))) {
                paySlipEntryInfo.setPerAnnuity(new BigDecimal(cmpTableMap.get("perAnnuity")));
            }
            // 个人所得税
            if (StringUtils.isNotBlank(cmpTableMap.get("personalIncomeTax"))) {
                paySlipEntryInfo.setPersonalIncomeTax(new BigDecimal(cmpTableMap.get("personalIncomeTax")));
            }
            // 实发工资
            if (StringUtils.isNotBlank(cmpTableMap.get("realWage"))) {
                paySlipEntryInfo.setRealWage(new BigDecimal(cmpTableMap.get("realWage")));
            }

            entrys.add(paySlipEntryInfo);
        }
    }

    /**
     * @description 调用久其接口同步
     * @title synchronizeAction
     * @author 相建彬
     * @throws SHRWebException
     */
    @SuppressWarnings("unchecked")
    public void synchronizeAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        // 获取单据id
        String billId = save(request, response, modelMap);
        System.out.println("********************要与久其同步的单据id为：" + billId + "********************");
        // 获取单据信息
        PaySlipInfo paySlipInfo = getPaySlipInfo(billId);
        // 获取单据状态
        String state = paySlipInfo.getState().getValue();
        String operationType = null;
        // 判断操作类型
        if ("0".equals(state)) {
            operationType = "add";
        }
        if ("1".equals(state) || "2".equals(state)) {
            operationType = "update";
        }
        JQSalaryPayBill payBill = new JQSalaryPayBill();
        // 组装数据调用接口
        assemblyJQinfo(paySlipInfo, payBill, billId, operationType);
        SalaryDataTransferService service = new SalaryDataTransferService();
        // 新建久其接口需要的参数的map集合
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("bill", payBill);
        // 获取久其接口配置信息
        JQIpConfigInfo ipConfigInfo = slipUtil.getJqIpConfigInfo();
        System.out.println("久其接口配置信息URL：" + ipConfigInfo.getJiuQiURL() + ",用户：" + ipConfigInfo.getUser()
                + ",密码：" + ipConfigInfo.getPassword());
        param.put("userName", ipConfigInfo.getUser());
        param.put("userPWD", ipConfigInfo.getPassword());
        param.put("url", ipConfigInfo.getJiuQiURL());
        try {
            // 调用接口
            Map<String, String> msgMap = (Map<String, String>) service.process(null, param);
            String ustate = JQSlipState.NOTSYN_VALUE;
            String send = null;
            String accept = null;
            // 发送的报文
            System.out.println("发送报文：" + msgMap.get("send"));
            if (StringUtils.isNotBlank(msgMap.get("send"))) {
                send = msgMap.get("send").replaceAll(" ", "").substring(0, 500);
            }
            // 接受的报文
            if (StringUtils.isNotBlank(msgMap.get("xml"))) {
                accept = msgMap.get("xml").replaceAll(" ", "").substring(36);
            }
            // 调用成功则更改状态
            if ("true".equals(msgMap.get("isSuccess"))) {
                ustate = JQSlipState.SYSCED_VALUE;
            }
            // 执行更改操作
            updatePaySlipInfo(billId, ustate, send, accept);
            if ("false".equals(msgMap.get("isSuccess"))) {
                if (msgMap.get("msg").indexOf("IO异常") != -1 || msgMap.get("msg").indexOf("XML异常") != -1) {
                    throw new ShrWebBizException("久其:同步至久其接口时出错，请联系管理员");
                } else {
                    throw new ShrWebBizException("久其:" + msgMap.get("msg"));
                }
            }
        } catch (EASBizException e) {
            throw new SHRWebException("调用久其接口时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("调用久其接口时出错", e);
        }
        writeSuccessData(billId);
    }

    /**
     * @description 同步久其前先保存
     * @title saveAction
     * @param request
     * @return model.getId().toString() 生成的单据id
     * @throws SHRWebException
     * @author 相建彬
     */
    public String save(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        CoreBaseInfo model = null;
        try {
            model = (CoreBaseInfo) request.getAttribute("dynamic_model");

            beforeSave(request, response, model);

            IObjectPK objectPK = runSaveData(request, response, model);
            model.setId(BOSUuid.read(objectPK.toString()));

            afterSave(request, response, model);

        } catch (SQLAccessException e) {
            if ((e.getCause() instanceof DataLimitExceedException)) {
                throw new ShrWebBizException("字段长度超过系统设定的最大长度，无法保存成功", e);
            }
            throw new SHRWebException(e);
        } catch (Exception exception) {
            throw new SHRWebException(exception);
        }
        return model.getId().toString();
    }

    /**
     * @description 组装需要传递久其的数据
     * @title assemblyJQinfo
     * @author 相建彬
     * @throws SHRWebException
     */
    private void assemblyJQinfo(PaySlipInfo paySlipInfo, JQSalaryPayBill payBill, String dataID,
            String operationType) throws SHRWebException {
        List<JQSalaryPayPlanDetail> planList = new ArrayList<JQSalaryPayPlanDetail>();
        List<JQSalaryPayAccountDetail> acctList = new ArrayList<JQSalaryPayAccountDetail>();
        /* ==================单据头信息================== */
        payBill.setTb0a_unitId(
                slipUtil.getOrgUnitInfo(paySlipInfo.getAdminOrg().getId().toString()).getNumber());
        // payBill.setTb0b_userId(slipUtil.getUserInfo(paySlipInfo.getApplier().getId().toString()).getNumber());
        payBill.setTb0b_userId(slipUtil.getUserInfo(HRFilterUtils.getCurrentUserId(ctx)).getNumber());
        payBill.setTb0c_operatertype(operationType);
        payBill.setTb0f_dataId(dataID);
        String releaseMonth = null;
        if (paySlipInfo.getReleaseMonth().length() == 2) {
            releaseMonth = paySlipInfo.getReleaseMonth();
        } else {
            releaseMonth = "0" + paySlipInfo.getReleaseMonth();
        }
        payBill.setTb02_ffyf(releaseMonth);
        payBill.setTb03_fflx(paySlipInfo.getReleaseType().getValue());
        /* ==================供应商、银行、银行账户================== */
        JQSalaryPayAccountDetail payAcct = new JQSalaryPayAccountDetail();
        payAcct.setTb27_payAccount_dExternalSysId(dataID);
        payAcct.setTb26_bank(slipUtil.getBankinfo(paySlipInfo.getBank().getId().toString()).getNumber());
        payAcct.setTb28_supplier(
                slipUtil.getSupplierinfo(paySlipInfo.getSupplier().getId().toString()).getNumber());
        payAcct.setTb29_bankNumber(paySlipInfo.getBankAccountStr());
        acctList.add(payAcct);
        /* ==================单据体信息================== */
        PaySlipEntryCollection entrys = paySlipInfo.getEntrys();
        for (int i = 0; i < entrys.size(); i++) {
            JQSalaryPayPlanDetail payPlan = new JQSalaryPayPlanDetail();
            payPlan.setTb05_payPlan_dExternalSysId(entrys.get(i).getId().toString());
            payPlan.setTb04_bm(
                    slipUtil.getOrgUnitInfo(entrys.get(i).getAdminOrg().getId().toString()).getNumber());
            if (entrys.get(i).getWage() != null) {
                payPlan.setTb06_gz(entrys.get(i).getWage().toString());
            }
            if (entrys.get(i).getSubsidy() != null) {
                payPlan.setTb07_jtbt(entrys.get(i).getSubsidy().toString());
            }
            if (entrys.get(i).getWelfareFee() != null) {
                payPlan.setTb08_flf(entrys.get(i).getWelfareFee().toString());
            }
            if (entrys.get(i).getTransportSubsidy() != null) {
                payPlan.setTb09_cb(entrys.get(i).getTransportSubsidy().toString());
            }
            if (entrys.get(i).getUnitPension() != null) {
                payPlan.setTb10_yangLBX_dw(entrys.get(i).getUnitPension().toString());
            }
            if (entrys.get(i).getUnitMedical() != null) {
                payPlan.setTb11_yiLBX_dw(entrys.get(i).getUnitMedical().toString());
            }
            if (entrys.get(i).getUnitUnemployment() != null) {
                payPlan.setTb12_shiYBX_dw(entrys.get(i).getUnitUnemployment().toString());
            }
            if (entrys.get(i).getUnitInjury() != null) {
                payPlan.setTb13_gsbx_dw(entrys.get(i).getUnitInjury().toString());
            }
            if (entrys.get(i).getUnitMaternity() != null) {
                payPlan.setTb14_shengYBX_dw(entrys.get(i).getUnitMaternity().toString());
            }
            if (entrys.get(i).getPerPension() != null) {
                payPlan.setTb15_yangLBX_gr(entrys.get(i).getPerPension().toString());
            }
            if (entrys.get(i).getPerMedical() != null) {
                payPlan.setTb16_yiLBX_gr(entrys.get(i).getPerMedical().toString());
            }
            if (entrys.get(i).getPerUnemployment() != null) {
                payPlan.setTb17_shiYBX_gr(entrys.get(i).getPerUnemployment().toString());
            }
            if (entrys.get(i).getUnitProvidentFund() != null) {
                payPlan.setTb18_gjj_dw(entrys.get(i).getUnitProvidentFund().toString());
            }
            if (entrys.get(i).getPerProvidentFund() != null) {
                payPlan.setTb19_gjj_gr(entrys.get(i).getPerProvidentFund().toString());
            }
            if (entrys.get(i).getAddProvidentFund() != null) {
                payPlan.setTb20_gjj_bc(entrys.get(i).getAddProvidentFund().toString());
            }
            if (entrys.get(i).getUnitAnnuity() != null) {
                payPlan.setTb21_qynj_dw(entrys.get(i).getUnitAnnuity().toString());
            }
            if (entrys.get(i).getPerAnnuity() != null) {
                payPlan.setTb22_qynj_gr(entrys.get(i).getPerAnnuity().toString());
            }
            if (entrys.get(i).getPersonalIncomeTax() != null) {
                payPlan.setTb23_gs(entrys.get(i).getPersonalIncomeTax().toString());
            }
            if (entrys.get(i).getUnionMemberFee() != null) {
                payPlan.setTb24_ghhyf(entrys.get(i).getUnionMemberFee().toString());
            }
            if (entrys.get(i).getRealWage() != null) {
                payPlan.setTb25_paymoney(entrys.get(i).getRealWage().toString());
            }

            planList.add(payPlan);
        }
        payBill.setFo01_payPlan(planList);
        payBill.setFo02_payAccount(acctList);
    }

    /**
     * @description 同步后更改单据状态，增加发送和接受的报文
     * @title updatePaySlipInfo
     * @param id 单据id
     * @param state 状态
     * @param send 发送报文
     * @param accept 接受报文
     * @author 相建彬
     * @throws SHRWebException
     */
    private void updatePaySlipInfo(String id, String state, String send, String accept)
            throws SHRWebException {
        String sql = "update CT_JQ_PaySlip SET CFSTATE='" + state + "',CFSendContent='" + send
                + "',CFAcceptContent='" + accept + "' where fid ='" + id + "'";
        try {
            DbUtil.execute(ctx, sql);
        } catch (BOSException e) {
            throw new SHRWebException("更改薪酬付款单信息时出错", e);
        }
    }

    /**
     * @description 根据单据编号获取薪酬付款单信息
     * @title getPaySlipInfo
     * @param billId 单据id
     * @return paySlipInfo 薪酬付款单信息
     * @author 相建彬
     * @throws SHRWebException
     */
    private PaySlipInfo getPaySlipInfo(String billId) throws SHRWebException {
        if (StringUtils.isBlank(billId)) {
            throw new ShrWebBizException("找不到相关的薪酬付款单信息");
        }
        PaySlipInfo paySlipInfo = null;
        try {
            paySlipInfo = PaySlipFactory.getRemoteInstance().getPaySlipInfo(new ObjectUuidPK(billId));
        } catch (EASBizException e) {
            throw new SHRWebException("查询薪酬付款单信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("查询薪酬付款单信息时出错", e);
        }

        return paySlipInfo;
    }
}
