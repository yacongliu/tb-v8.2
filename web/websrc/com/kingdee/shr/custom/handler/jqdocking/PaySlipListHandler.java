package com.kingdee.shr.custom.handler.jqdocking;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.PaySlipFactory;
import com.kingdee.eas.custom.jqdocking.PaySlipInfo;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * @description 薪酬付款单列表
 * @title PaySlipListHandler
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年9月24日
 */
public class PaySlipListHandler extends ListHandler {
    Context ctx = SHRContext.getInstance().getContext();

    /**
     * @description 获取单据状态
     * @title getStateAction
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws SHRWebException
     */
    public void getStateAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        String billId = request.getParameter("billId");
        if (StringUtils.isBlank(billId)) {
            throw new ShrWebBizException("SHR:数据未知");
        }
        PaySlipInfo paySlipInfo = getPaySlipInfo(billId);
        String state = paySlipInfo.getState().getValue();
        if ("1".equals(state)) {
            modelMap.put("msg", "error");
        } else {
            modelMap.put("msg", "success");
        }
        JSONUtils.SUCCESS(modelMap);
    }

    /**
     * @description 根据单据编号获取薪酬付款单信息
     * @title getPaySlipInfo
     * @param billId 单据编号
     * @return paySlipInfo 薪酬付款单信息
     * @author 相建彬
     * @throws SHRWebException
     */
    private PaySlipInfo getPaySlipInfo(String billId) throws SHRWebException {
        if (StringUtils.isBlank(billId)) {
            throw new ShrWebBizException("SHR:找不到相关的薪酬付款单信息");
        }
        PaySlipInfo paySlipInfo = null;
        try {
            paySlipInfo = PaySlipFactory.getRemoteInstance().getPaySlipInfo(new ObjectUuidPK(billId));
        } catch (EASBizException e) {
            throw new SHRWebException("SHR:查询薪酬付款单信息时出错", e);
        } catch (BOSException e) {
            throw new SHRWebException("SHR:查询薪酬付款单信息时出错", e);
        }

        return paySlipInfo;
    }
}
