package com.kingdee.shr.custom.handler.examinscore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.handler.F7Handler;

/**
 * @description 部门绩效考核负责人f7Handler
 * @title UserF7Handler
 * @copyright 天津金蝶软件有限公司
 * @author Jambin Email:tjxiangjianbin@kingdee.com
 * @date 2020年1月21日
 */
public class UserF7Handler extends F7Handler {
    Context ctx = SHRContext.getInstance().getContext();

    /**
     * @description 根据角色过滤
     * @title getDefaultFilter
     * @param request
     * @param response
     * @return
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年1月21日
     */
    @Override
    public FilterInfo getDefaultFilter(HttpServletRequest request, HttpServletResponse response)
            throws SHRWebException {
        FilterInfo hrFilter = new FilterInfo();
        hrFilter.getFilterItems().add(new FilterItemInfo("id", getResponseSql(), CompareType.INNER));
        return hrFilter;
    }

    /**
     * 
     * @description 获取过滤数据的sql --TB-RLZY-016 本部绩效部门负责人角色编码
     * @title getResponseSql
     * @return sql 过滤sql
     * @author Jambin
     * @date 2020年1月21日
     */
    private String getResponseSql() {
        String sql = "select usr.fid from T_PM_User usr LEFT  JOIN T_PM_UserRoleOrg uro on uro.fuserid=usr.fid  LEFT  JOIN T_PM_Role role on role.fid=uro.froleid where role.fnumber='TB-RLZY-016'";

        return sql;
    }
}
