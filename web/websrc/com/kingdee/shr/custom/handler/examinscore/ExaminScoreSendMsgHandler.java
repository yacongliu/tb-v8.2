package com.kingdee.shr.custom.handler.examinscore;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.Context;
import com.kingdee.bos.service.message.Message;
import com.kingdee.bos.service.message.agent.MessageFactory;
import com.kingdee.bos.service.message.agent.SenderAgent;
import com.kingdee.eas.base.message.MsgBizType;
import com.kingdee.eas.base.message.MsgPriority;
import com.kingdee.eas.base.message.MsgSourceStatus;
import com.kingdee.eas.base.message.MsgType;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.web.handler.CoreHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * @description 考核评分ListHandler 继承类
 * @title ExaminScoreListHandlerEx
 * @copyright 天津金蝶软件有限公司
 * @author Jambin Email:tjxiangjianbin@kingdee.com
 * @date 2020年1月21日
 */
public class ExaminScoreSendMsgHandler extends CoreHandler {

    private Context ctx;

    public ExaminScoreSendMsgHandler(Context ctx) {
        this.ctx = ctx;
    }

    public ExaminScoreSendMsgHandler() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * @description 发送消息
     * @title sendMsgAction
     * @param request
     * @param response
     * @param modelMap
     * @author Jambin
     * @date 2020年1月21日
     */
    public void sendMsgAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        try {
            // 获取将要发送消息的人员id
            String userIds = request.getParameter("userid");
            // 获取消息内容
            String content = request.getParameter("content");
            System.out.println("将要给下列用户发送消息：" + userIds + ",消息内容为：" + content);
            // 获取当前登录人信息
            UserInfo userInfo = ContextUtil.getCurrentUserInfo(this.ctx);
            String userName = userInfo.getName();

            Locale locale = this.ctx.getLocale();
            SenderAgent senderAgent = SenderAgent.getSenderAgent();
            // 组装消息内容
            Message message = MessageFactory.newMessage("kingdee.workflow");// 生成一个消息对象
            message.setLocaleStringHeader("title", "绩效考核开始提醒", locale);// 设置消息标题
            message.setLocaleStringHeader("body", content, locale);// 设置消息体内容，根据具体业务自己设定
            message.setLocaleStringHeader("sender", userName, locale);// 设置发送人，属于文本，不是ID

            message.setStringHeader("receiver", userIds); // 设置接收者，后面那参数是用户ID，多个ID可用分号";"分割
            message.setStringHeader("databaseCenter", this.ctx.getAIS());// 得到数据中心
            message.setStringHeader("solution", this.ctx.getSolution());// 设置解决方案

            message.setIntHeader("type", MsgType.NOTICE_VALUE);// 设置消息类型为通知
            message.setIntHeader("bizType", MsgBizType.FORWARN_VALUE);// 业务类型设置为工作流
            message.setIntHeader("sourceStatus", MsgSourceStatus.EMPTY_VALUE);// 设置任务状态，此处是通知消息，所以设置空
            message.setIntHeader("priority", MsgPriority.HIGH_VALUE);// 设置消息优先级，自己根据需要设定相应的级别

            senderAgent.sendMessage(message);
            JSONUtils.SUCCESS("消息发送成功！");
        } catch (Exception e) {
            JSONUtils.ERROR("消息发送失败！");
        }
    }
}
