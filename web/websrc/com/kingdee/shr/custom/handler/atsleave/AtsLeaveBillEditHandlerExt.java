package com.kingdee.shr.custom.handler.atsleave;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.hr.emp.IPersonContactMethod;
import com.kingdee.eas.hr.emp.PersonContactMethodCollection;
import com.kingdee.eas.hr.emp.PersonContactMethodFactory;
import com.kingdee.eas.hr.emp.PersonContactMethodInfo;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.ats.web.handler.AtsLeaveBillEditHandler;
import com.kingdee.shr.ats.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.StringUtils;

/**
 * 
 * Title: AtsLeaveBillEditHandlerExt
 * <p>
 * Description: 请假单Handler
 * 
 * @author saisai_cheng Email:854296216@qq.com
 * @date 2019-7-27 & 上午11:28:10
 * @since V1.0
 */
public class AtsLeaveBillEditHandlerExt extends AtsLeaveBillEditHandler {

    private static Logger logger = Logger.getLogger("com.kingdee.shr.ats.service.AtsLeaveBillEditHandlerExt");

    private Context ctx;

    private IPersonContactMethod instance;

    public AtsLeaveBillEditHandlerExt(Context ctx) {
        this.ctx = ctx;
    }

    public AtsLeaveBillEditHandlerExt() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * （非 Javadoc）
     * <p>
     * Title: editAction
     * </p>
     * <p>
     * Description: 根据请假单人员获取其对应的紧急联系人和电话
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws SHRWebException
     * @see com.kingdee.shr.ats.web.handler.AtsLeaveBillEditHandler#editAction(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse, org.springframework.ui.ModelMap)
     */
    public void getEmergencyContactInfoAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        // service 业务逻辑 1获取人员内码id, 2.根据人员内码获取其对应的紧急联系人信息
        String personId = request.getParameter("personId");
        if (StringUtils.isEmpty(personId)) {
            logger.error("请假单人员为空！");
        }
        Map<String, String> data = new HashMap<String, String>(2);
        String linkName = "";// 紧急联系人姓名
        String linkTelNum = "";// 紧急联系人电话

        try {
            getIPersonContactMethod();
            PersonContactMethodCollection collection = this.instance
                    .getPersonContactMethodCollection(" where person.id = '" + personId + "'");
            if ((collection != null) && (collection.size() > 0)) {
                PersonContactMethodInfo info = collection.get(0);
                linkName = info.getLinkName();
                linkTelNum = info.getLinkTelNum();

                data.put("linkName", linkName);
                data.put("linkTelNum", linkTelNum);

                System.out.println(" 紧急联系人信息:" + " 联系人姓名" + linkName + " 联系人电话" + linkTelNum);
            }
            writeSuccessData(data);
        } catch (BOSException e) {
            logger.error(e.getMessage(), e);
            throw new SHRWebException("获取紧急联系人信息失败...");
        }
    }

    private void getIPersonContactMethod() throws BOSException {
        getIPersonContactMethod(null);
    }

    /**
     * 
     * <p>
     * Title: getIPersonContactMethod
     * </p>
     * <p>
     * Description: 获取紧急联系人信息接口实例
     * </p>
     * 
     * @param ctx
     * @throws BOSException
     */
    private void getIPersonContactMethod(Context ctx) throws BOSException {
        if (ctx != null) {
            this.instance = (this.instance == null ? PersonContactMethodFactory.getLocalInstance(ctx)
                    : this.instance);
        } else {
            this.instance = (this.instance == null ? PersonContactMethodFactory.getRemoteInstance()
                    : this.instance);
        }
    }

    /**
     * 
     * <p>
     * Title: passValueAction
     * </p>
     * <p>
     * Description:请假助手弹框，弹出假期制度
     * </p>
     * 
     * @param request
     * @param response
     * @param model
     * @throws SHRWebException
     */
    public void getHolidaySystemAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap model) throws SHRWebException {

        UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(this.ctx);
        BOSUuid userId = currentUserInfo.getPerson().getId();
        PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());

        String personorgId = personPositionInfo.getPersonDep().getId().toString();
        System.out.println("获取部门 ID:  " + personorgId);
        StringBuffer sql = new StringBuffer();
        String allLongNum = null;
        String currentLongNum = null;
        sql.append("select flongnumber as currentLongNum from T_ORG_BaseUnit where fid =")
                .append("'" + personorgId + "'");
        System.out.println("获取当前登录人的部门长编码sql :  " + sql);
        IRowSet rowSets = null;
        StringBuffer sqls = new StringBuffer();

        sqls.append(
                "select FLONGNUMBER as allLongNum  from T_ORG_BaseUnit where fid in(SELECT cfsszzid FROM CT_MP_Jqzd)");
        System.out.println("获取所有假期制度的部门长编码sql :  " + sqls);
        Map<String, String> map = new HashMap<String, String>();
        IRowSet rowSetss = null;

        try {
            rowSetss = DbUtil.executeQuery(ctx, sqls.toString());
            while (rowSetss.next()) {
                allLongNum = rowSetss.getString("allLongNum");
                System.out.println("假期制度中长编码的循环结果集：" + allLongNum);
                if (!"00".equals(allLongNum)) {
                    rowSets = DbUtil.executeQuery(ctx, sql.toString());
                    while (rowSets.next()) {
                        currentLongNum = rowSets.getString("currentLongNum");
                        System.out.println("当前登录人所在组织长编码：" + currentLongNum);
                        if (currentLongNum.contains(allLongNum)) {
                            StringBuffer sqll = new StringBuffer();
                            String holidaySystem = null;
                            IRowSet rowSet = null;
                            sqll.append(
                                    "select fnumber,fname_l2,cfsszzid,cfjqzd as jqzd from CT_MP_Jqzd where cfsszzid =")
                                    .append("(select fid from T_ORG_BaseUnit where flongNumber='" + allLongNum
                                            + "')");
                            System.out.println("获取当前登录人弹出的假期制度sql :  " + sqll);
                            try {
                                rowSet = DbUtil.executeQuery(ctx, sqll.toString());
                                while (rowSet.next()) {
                                    holidaySystem = rowSet.getString("jqzd");
                                    if (holidaySystem != null && !"".equals(holidaySystem)) {
                                        map.put("holidaySystem", holidaySystem);
                                        System.out.println("============假期制度holidaySystem:" + holidaySystem);
                                    }
                                }
                            } catch (BOSException e) {
                                logger.error(e.getMessage(), e);
                                throw new SHRWebException("查询公司部门失败，请联系管理员...", e);

                            } catch (SQLException e) {
                                logger.error(e.getMessage(), e);
                                throw new SHRWebException("查询公司部门失败，请联系管理员...", e);
                            }
                        }
                    }
                }

            }
        } catch (BOSException e) {
            logger.error(e.getMessage(), e);
            throw new SHRWebException("请假助手有误，请联系管理员...", e);

        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw new SHRWebException("请假助手有误，请联系管理员...", e);
        }
        JSONUtils.SUCCESS(map);
    }
}
