package com.kingdee.shr.custom.handler.atsleave;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.ats.web.handler.AtsLeaveBillEditHandler;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;

public class AtsLeaveBillEditHandlerEx extends AtsLeaveBillEditHandler {
	private static Logger logger = Logger.getLogger("com.kingdee.shr.ats.web.handler.custom.AtsLeaveBillEditHandlerEx");

    private Context ctx;

    public AtsLeaveBillEditHandlerEx(Context ctx) {
        this.ctx = ctx;
    }

    public AtsLeaveBillEditHandlerEx() {
        this.ctx = SHRContext.getInstance().getContext();
    }
    /**
     * 检查是否符合年休假规则
     * <p>Title: checkAnnualLeaveAction</p>
     * <p>Description: </p>
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     */
    public void checkAnnualLeaveAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
    	/**
    	 * 规则
			（1）累计工作满1年不满10年的员工，请病假累计2个月以上的；
			（2）累计工作满10年不满20年的员工，请病假累计3个月以上的；
			（3）累计工作满20年以上的员工，请病假累计4个月以上的。
			注：病假有可能分成带薪病假和病假，会影响取病假累计值，
    	 */
    	Map<String, Boolean> res = new HashMap<String, Boolean>();
    	//假期类型
    	String vacationType = request.getParameter("policy");
    	//人员id
    	String personId = request.getParameter("personId");
    	boolean checkResult = true;
    	Double workingYears = 0.0;
    	Double DiseaseYears = 0.0;
    	if(vacationType.equals("年假")){
    		StringBuffer DiseaseYearssql = new StringBuffer();
    		//获取病假天数
    		DiseaseYearssql.append("select sum(FLEAVELENGTH) DiseaseYears from T_HR_ATS_LeaveBillEntry where FPOLICYID='VTmjUM/7Sf6/IA+e5Ct+8l/BCRA=' and DateName(year,getDate()) = datename(year,FREALBEGINTIME)  and FPERSONID ='").append(personId).append("'");
    		System.out.println("获取病假的查询语句"+DiseaseYearssql.toString());
    		//获取年假天数
    		StringBuffer workingYearssql = new StringBuffer();
    		workingYearssql.append("select fnewworktime workingYears from T_HR_Personposition where FPERSONID ='").append(personId).append("'");
    		System.out.println("获取工龄的查询语句"+workingYearssql.toString());
    		try {
				IRowSet rowSet = DbUtil.executeQuery(ctx, DiseaseYearssql.toString());
				while(rowSet.next()){
					DiseaseYears = rowSet.getDouble("DiseaseYears");
					System.out.println("获取病假的值==================="+DiseaseYears);
				}
				rowSet = DbUtil.executeQuery(ctx, workingYearssql.toString());
				while(rowSet.next()){
					workingYears = rowSet.getDouble("workingYears");
					System.out.println("获取工龄的值==================="+workingYears);
				}
				if((workingYears <10 && DiseaseYears>2*30)||((10<=workingYears && workingYears < 20) && DiseaseYears > 3*30)||(workingYears > 20 && DiseaseYears > 4*30)){
					checkResult = false;
				}
			} catch (BOSException e) {
				throw new SHRWebException(e);
			} catch (SQLException e) {
				throw new SHRWebException(e);
			}
    	}
    	System.out.println("获取最后的结果==================="+checkResult);
    	res.put("checkResult", checkResult);
    	writeSuccessData(res);
    }
}
