package com.kingdee.shr.custom.handler.complaint;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.workflow.WfException;
import com.kingdee.bos.workflow.service.EnactmentServiceProxy;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.complaint.ComplaintItemFactory;
import com.kingdee.eas.custom.complaint.ComplaintItemInfo;
import com.kingdee.eas.custom.complaint.IComplaintItem;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.shr.base.syssetting.app.filter.HRFilterUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.DateTimeUtils;
import com.kingdee.util.StringUtils;

/**
 * 
 * Title: ComplaintEditHandler
 * Description: 投诉单handler
 * @author saisai_cheng Email:854296216@qq.com
 * @date 2019-9-9
 */
public class ComplaintEditHandler extends EditHandler {
	private static Logger logger = Logger
			.getLogger("com.kingdee.shr.custom.handler.complaint.ComplaintEditHandler");

	private Context ctx;

	public ComplaintEditHandler(Context ctx) {
		this.ctx = ctx;
	}

	public ComplaintEditHandler() {
		this.ctx = SHRContext.getInstance().getContext();
	}
	
	public void canSubmitEffectAction(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap)
			throws SHRWebException {
		Map<String, String> map = new HashMap<String, String>();
		map.put("state", "success");
		JSONUtils.writeJson(response, map);
	}
	/**
	 * 
	 * <p>Title: submitEffectAction</p>
	 * <p>Description: 提交生效</p>
	 * @param req
	 * @param res
	 * @param modelMap
	 * @throws SHRWebException
	 */
	public void submitEffectAction(HttpServletRequest req,
			HttpServletResponse res, ModelMap modelMap) throws SHRWebException {

		CoreBaseInfo model = (CoreBaseInfo) req.getAttribute("dynamic_model");
		try {
			IComplaintItem iComplaintItem = ComplaintItemFactory
					.getRemoteInstance();
			IObjectPK objectPK = iComplaintItem.submitEffect(model);
			model.setId(BOSUuid.read(objectPK.toString()));
		} catch (EASBizException e) {
			throw new SHRWebException(e.getMessage());
		} catch (Exception e) {
			throw new ShrWebBizException(e.getMessage());
		}

		System.out.println("投诉单id：" + model.getId().toString());

		writeSuccessData(model.getId().toString());
	}
	/**
	 * （非 Javadoc）
	 * <p>Title: afterCreateNewModel</p>
	 * <p>Description: 自动带出日期</p>
	 * @param request
	 * @param response
	 * @param coreBaseInfo
	 * @throws SHRWebException
	 * @see com.kingdee.shr.base.syssetting.web.handler.EditHandler#afterCreateNewModel(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.kingdee.eas.framework.CoreBaseInfo)
	 */
	protected void afterCreateNewModel(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo coreBaseInfo)
			throws SHRWebException {
		super.afterCreateNewModel(request, response, coreBaseInfo);

		Date nowDate = DateTimeUtils.truncateDate(new Date());
		UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(this.ctx);
		ComplaintItemInfo info = (ComplaintItemInfo) coreBaseInfo;
		info.setCreator(currentUserInfo);
		info.setBizDate(nowDate);
	}

	@Override
	protected void beforeSave(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		super.beforeSave(request, response, model);
		ComplaintItemInfo bill = (ComplaintItemInfo) model;
		bill.setBillState(HRBillStateEnum.SAVED);
	}

	@Override
	protected void beforeSubmit(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		super.beforeSubmit(request, response, model);

		ComplaintItemInfo bill = (ComplaintItemInfo) model;

		String userID = HRFilterUtils.getCurrentUserId(ctx);
		String functionName = "com.kingdee.eas.custom.complaint.app.ComplaintItemEditUIFunction";
		String operationName = "actionSubmit";
		try {
			String temp = EnactmentServiceProxy.getEnacementService(ctx)
					.findSubmitProcDef(userID, bill, functionName,
							operationName);
			/*if ((temp == null) || (temp.trim().equals("")))
				throw new ShrWebBizException("没有可用的投诉单工作流");*/
		} catch (WfException e) {
			throw new SHRWebException(e.getMessage());
		} catch (BOSException e) {
			throw new SHRWebException(e.getMessage());
		}

		bill.setBillState(HRBillStateEnum.SUBMITED);
		String operateStatus = request.getParameter("operateState");
		if ((StringUtils.isEmpty(operateStatus))
				|| (!("ADDNEW".equalsIgnoreCase(operateStatus)))) {
			return;
		}
		bill.setExtendedProperty("isAddNew", "isAddNew");
	}

}
