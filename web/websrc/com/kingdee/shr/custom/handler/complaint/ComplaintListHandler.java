package com.kingdee.shr.custom.handler.complaint;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.complaint.ComplaintItemFactory;
import com.kingdee.eas.custom.complaint.IComplaintItem;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.UuidException;

/**
 * 
 * Title: ComplaintListHandler
 * Description:  投诉单handler
 * @author saisai_cheng Email:854296216@qq.com
 * @date 2019-9-9
 */
public class ComplaintListHandler extends ListHandler {
	private static Logger logger = Logger
			.getLogger("com.kingdee.shr.custom.handler.complaint.ComplaintListHandler");

	private Context ctx;

	public ComplaintListHandler(Context ctx) {
		this.ctx = ctx;
	}

	public ComplaintListHandler() {
		this.ctx = SHRContext.getInstance().getContext();
	}
	
	private IComplaintItem iComplaintItem;

	/**
	 * 
	 * <p>
	 * Title: againstApproveAction
	 * </p>
	 * <p>
	 * Description: 反审批
	 * </p>
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 * @throws SHRWebException
	 */
	public void againstApproveAction(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap)
			throws SHRWebException {
		String billId = request.getParameter("billId");
		try {
			getComplaintItemInterface().setEditState(BOSUuid.read(billId));
		} catch (BOSException e1) {
			logger.error(e1.getMessage(), e1);
			throw new SHRWebException("反审批失败!", e1);
		} catch (EASBizException e) {
			logger.error(e.getMessage(), e);
			throw new SHRWebException("反审批失败!", e);
		} catch (UuidException e) {
			logger.error(e.getMessage(), e);
			throw new SHRWebException("反审批失败!", e);
		}

		modelMap.put("flag", "1");
		JSONUtils.writeJson(response, modelMap);
	}

	/**
	 * 
	 * <p>
	 * Title: isApproveAction
	 * </p>
	 * <p>
	 * Description: 判断是否审批通过
	 * </p>
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @throws SHRWebException
	 */
	public void isApproveAction(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap)
			throws SHRWebException {
		String billId = request.getParameter("billId");

		String billState = null;
		String sql = " select fbillstate from CT_COM_ComplaintItem where fid = '"
				+ billId + "'";
		System.out.println("==============单据状态sql===========："+sql);
		IRowSet rowSet;
		try {
			rowSet = DbUtil.executeQuery(ctx, sql);
			while (rowSet.next()) {
				billState = rowSet.getString("fbillstate");
				
				System.out.println("==============单据状态===========："+billState);
			}
		} catch (BOSException e) {
			logger.error(e.getMessage(), e);
			throw new SHRWebException("获取当前单据状态失败!", e);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new SHRWebException("获取当前单据状态失败!", e);
		}
		if ("3".equals(billState)) {
			// 该单据已审批通过
			System.out.println("该单据已审批通过");
			modelMap.put("msg", true);
		} else {
			System.out.println("该单据未审批通过");
			modelMap.put("msg", false);
		}
		JSONUtils.SUCCESS(modelMap);
	}	
		/**
		 * 
		 * <p>Title: getBillStateAction</p>
		 * <p>Description: 判断单据状态 只有当单据状态为0时才能删除该单据</p>
		 * @param request
		 * @param response
		 * @param modelMap
		 * @throws SHRWebException
		 */
		public void isDeleteBillAction(HttpServletRequest request,
				HttpServletResponse response, ModelMap modelMap)
				throws SHRWebException {
			String billId = request.getParameter("billId");

			String billState = null;
			String sql = " select fbillstate from CT_COM_ComplaintItem where fid = '"
					+ billId + "'";

			IRowSet rowSet;
			try {
				rowSet = DbUtil.executeQuery(ctx, sql);
				while (rowSet.next()) {
					billState = rowSet.getString("fbillstate");
				}
			} catch (BOSException e) {
				logger.error(e.getMessage(), e);
				throw new SHRWebException("获取当前单据状态失败!", e);
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
				throw new SHRWebException("获取当前单据状态失败!", e);
			}
					
			if ("0".equals(billState) || "".equals(billState)) {
				modelMap.put("messages", "success");
			} else {
				// 单据非保存状态 不能删除
				modelMap.put("messages", "error");
			}	
		
		JSONUtils.SUCCESS(modelMap);

	}
	/**
	 * 
	 * <p>
	 * Title: getComplaintItemInterface
	 * </p>
	 * <p>
	 * Description: 获取投诉单远程调用接口实例
	 * </p>
	 * 
	 * @throws BOSException
	 */
	private IComplaintItem getComplaintItemInterface() throws BOSException {
		return iComplaintItem == null ? ComplaintItemFactory.getRemoteInstance()
				: iComplaintItem;
	}
}
