package com.kingdee.shr.custom.handler.dutycost;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.IMetaDataLoader;
import com.kingdee.bos.metadata.MetaDataLoaderFactory;
import com.kingdee.bos.metadata.entity.EntityObjectInfo;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * 
 * @Copyright: 版权所有 天津金蝶有限公司 <br/>
 *             Title: DutyCostListHandler Description: 值班费用列表
 * @author 马伟楠 Email:tjmaweinan@kingdee.com
 * @date 2019-8-15
 */
public class DutyCostListHandler extends ListHandler {
    private static Logger logger = Logger
            .getLogger(com.kingdee.shr.custom.handler.dutycost.DutyCostListHandler.class);

    private Context ctx;

    public DutyCostListHandler(Context ctx) {
        this.ctx = ctx;
    }

    public DutyCostListHandler() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * 
     * <p>
     * Title: againstApproveAction
     * </p>
     * <p>
     * Description: 单据是否可反审批
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws ShrWebBizException
     * @author 马伟楠
     */
    public void isApproveAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException {
        String billId = request.getParameter("billId");
        String tableName = getTableNameByBillId(billId);
        String billState = getBillState(tableName, billId);
        if ("3".equals(billState)) {
            modelMap.put("msg", Boolean.valueOf(true));
        } else {
            modelMap.put("msg", Boolean.valueOf(false));
        }
        try {
            JSONUtils.SUCCESS(modelMap);
        } catch (SHRWebException e) {
            e.printStackTrace();
            throw new ShrWebBizException("反审批，获取单据状态失败！" + e);
        }
    }

    /**
     * 
     * <p>
     * Title: isDeleteStateAction
     * </p>
     * <p>
     * Description: 判断单据是否可删除
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws ShrWebBizException
     * @author 马伟楠
     */
    public void isDeleteStateAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws ShrWebBizException {
        String billId = request.getParameter("billId");
        System.out.println("单据ID" + billId);
        String tableName = getTableNameByBillId(billId);
        String billState = getBillState(tableName, billId);
        System.out.println("单据状态为：" + billState);
        if (("0".equals(billState)) || ("".equals(billState))) {
            modelMap.put("msg", "SUCCESS");
        } else {
            modelMap.put("msg", "Erorr");
        }
        try {
            JSONUtils.SUCCESS(modelMap);
        } catch (SHRWebException e) {
            e.printStackTrace();
            throw new ShrWebBizException("删除失败，获取单据状态失败！" + e);
        }
    }

    /**
     * 
     * <p>
     * Title: againstApproveAction
     * </p>
     * <p>
     * Description: 反审批单据
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws ShrWebBizException
     * @author 马伟楠
     */
    public void againstApproveAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws ShrWebBizException {
        String billId = request.getParameter("billId");
        String tableName = getTableNameByBillId(billId);

        String msg = againstBillState(tableName, billId);
        modelMap.put("flag", msg);
        try {
            JSONUtils.writeJson(response, modelMap);
        } catch (SHRWebException e) {
            e.printStackTrace();
            throw new ShrWebBizException("反审批失败！" + e);
        }
    }

    /**
     * 
     * <p>
     * Title: getBillState
     * </p>
     * <p>
     * Description: 获取单据状态
     * </p>
     * 
     * @param tableName 表名
     * @param fid 单据内码
     * @return String
     * @throws ShrWebBizException
     * @author 马伟楠
     */
    private String getBillState(String tableName, String fid) throws ShrWebBizException {
        if ((StringUtils.isEmpty(tableName)) || (StringUtils.isEmpty(fid))) {
            throw new ShrWebBizException(" 获取单据状态失败，无法进行反审核！");
        }
        String billState = "";
        String sql = " select fbillstate from " + tableName + " where fid = '" + fid + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(this.ctx, sql);
            if (rowSet.next()) {
                billState = rowSet.getString("fbillstate");
            }
        } catch (BOSException e) {
            e.printStackTrace();
            throw new ShrWebBizException("获取单据状态成功！", e);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ShrWebBizException("获取单据状态失败！", e);
        }
        return billState;
    }

    /**
     * 
     * <p>
     * Title: againstBillState
     * </p>
     * <p>
     * Description: 反审批单据-更改单据状态为未提交
     * </p>
     * 
     * @param tableName 表名
     * @param fid 单据内码
     * @return
     * @throws ShrWebBizException
     * @author 马伟楠
     */
    private String againstBillState(String tableName, String fid) throws ShrWebBizException {
        if ((StringUtils.isEmpty(tableName)) || (StringUtils.isEmpty(fid))) {
            throw new ShrWebBizException(" 反审批失败！");
        }
        String msg = "";
        String sql = " update " + tableName + " set fbillstate = '0'  where fid = '" + fid + "'";
        try {
            DbUtil.execute(this.ctx, sql);
            msg = "1";
        } catch (BOSException e) {
            e.printStackTrace();
            logger.error("设置单据状态 为 未提交 失败！" + e);
        }
        return msg;
    }

    /**
     * 
     * <p>
     * Title: getTableNameByBillId
     * </p>
     * <p>
     * Description: 获取表名
     * </p>
     * 
     * @param billId 单据内码
     * @return String 单据对应的数据表名
     * @author 马伟楠
     */
    private String getTableNameByBillId(String billId) {
        if (StringUtils.isNotEmpty(billId)) {
            BOSUuid uuid = BOSUuid.read(billId);
            BOSObjectType bosType = uuid.getType();
            IMetaDataLoader metadataloader = MetaDataLoaderFactory.getRemoteMetaDataLoader();
            EntityObjectInfo entity = metadataloader.getEntity(bosType);
            String tableName = entity.getTable().getName();

            return tableName;
        }
        return null;
    }
}
