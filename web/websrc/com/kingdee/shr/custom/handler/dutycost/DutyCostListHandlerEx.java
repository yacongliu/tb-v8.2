package com.kingdee.shr.custom.handler.dutycost;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.dev.helper.SHRHelper;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.affair.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;

/**
 * @description 值班费用列表扩展
 * @title DutyCostListHandlerEx
 * @copyright 天津金蝶软件有限公司
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2020年1月7日
 */
public class DutyCostListHandlerEx extends DutyCostListHandler {
    Context ctx = SHRContext.getInstance().getContext();

    /**
     * @description 判断当前登录人是否是bp，若是则显示其管理部门的数据，若不是则只显示当前组织的数据
     * @title getDefaultFilter
     * @param request
     * @param response
     * @return
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年3月9日
     */
    @Override
    protected FilterInfo getDefaultFilter(HttpServletRequest request, HttpServletResponse response)
            throws SHRWebException {
        // 获取当前登录人id
        String currentUserId = SHRHelper.getCurrPersonInfoNew(ctx).getId().toString();
        String currentOrgId = SHRBillUtil.getAdminOrgUnit(currentUserId).getPersonDep().getId().toString();
        System.out.println("当前登录人：" + currentUserId + ",当前组织：" + currentOrgId);
        FilterInfo filterInfo = new FilterInfo();
        // 判断当前登录人是否是BP
        boolean isBP = isBP(currentUserId);
        // 当前登录人不是bp
        if (isBP == false) {
            String sql = getFilterList(false, currentOrgId, null);
            filterInfo.getFilterItems().add(new FilterItemInfo("id", sql, CompareType.INNER));
        }
        // 当前登录人是bp
        if (isBP == true) {
            // 根据职员表id获取用户表id
            String userId = SHRHelper.getUserIdForPersonId(ctx, currentUserId);
            // 根据用户id获取组织范围
            List<String> orgRangeList = getOrgRangeList(userId);
            String sql = getFilterList(true, currentOrgId, orgRangeList);
            filterInfo.getFilterItems().add(new FilterItemInfo("id", sql, CompareType.INNER));
        }
        return filterInfo;
    }

    /**
     * @description
     * @title getFilterList
     * @param currentOrgId
     * @param dataList
     * @return
     * @throws ShrWebBizException
     * @author Jambin
     * @date 2020年1月13日
     */
    private String getFilterList(Boolean type, String currentOrgId, List orgRangeList)
            throws ShrWebBizException {
        // 根据type拼接不同的sql
        StringBuffer sql = new StringBuffer();
        if (type == false) {
            // 查询过滤后的数据
//            sql.append(
//                    "SELECT dut.fid FROM CT_DUT_DutyCost dut left join T_PM_User u on dut.fcreatorid=u.fid left join T_BD_Person per on per.Fid=u.FPERSONID  where per.FGKADMINID ='")
//                    .append(currentOrgId).append("'");
            sql.append(
                    "SELECT dut.* FROM CT_DUT_DutyCost dut left join T_PM_User u on dut.fcreatorid=u.fid left join T_ORG_PositionMember psm on psm.FPERSONID =u.FPERSONID left join T_ORG_Position pos on pos.fid=psm.fpositionid where pos.FADMINORGUNITID ='")
                    .append(currentOrgId).append("'");
            

        } else {
            String orgids = StringUtils.join(orgRangeList.toArray(), "','");
            sql.append(
                    "SELECT dut.* FROM CT_DUT_DutyCost dut left join T_PM_User u on dut.fcreatorid=u.fid left join T_ORG_PositionMember psm on psm.FPERSONID =u.FPERSONID left join T_ORG_Position pos on pos.fid=psm.fpositionid where pos.FADMINORGUNITID in ('")
                    .append(orgids).append("')");

        }
        System.out.println("查询过滤数据的sql：" + sql);
        return sql.toString();
    }

    /**
     * @description 判断当前登录人是否是bp
     * @title isBP
     * @param personId 当前登录人id
     * @return true是,false否
     * @author Jambin
     * @throws SHRWebException
     * @date 2020年1月13日
     */
    private boolean isBP(String personId) throws SHRWebException {
        String sql = "SELECT person.fname_l2,person.fnumber,position.fname_l2,position.fnumber FROM T_ORG_POSITIONMEMBER pm left join T_ORG_POSITION position on pm.FPOSITIONID  = position.FID left join T_BD_PERSON person on pm.FPERSONID = person.FID where position.FNUMBER in ('ZW-004255','ZW-004060')  and person.FID ='"
                + personId + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            if (rowSet.size() > 0) {
                return true;
            }
        } catch (BOSException e) {
            throw new SHRWebException("执行【判断当前登录人是否是bp】sql语句时发生异常！");
        }

        return false;
    }

    /**
     * @description 根据用户id获取行政组织范围
     * @title getOrgRangeList
     * @param userId 用户id
     * @return orgRangeList 组织集合
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年1月13日
     */
    private List<String> getOrgRangeList(String userId) throws SHRWebException {
        String sql = "select FORGID from T_PM_OrgRange where FTYPE ='20' and FUSERID  ='" + userId + "'";
        System.out.println("根据用户id获取行政组织范围sql:" + sql);
        List<String> orgRangeList = new ArrayList<String>();
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                orgRangeList.add(rowSet.getString("FORGID"));
            }
        } catch (BOSException e) {
            throw new SHRWebException("执行【查询bp的组织范围】sql语句时发生异常！");
        } catch (SQLException e) {
            throw new SHRWebException("执行【查询bp的组织范围】sql语句时发生异常！");
        }
        return orgRangeList;
    }
}
