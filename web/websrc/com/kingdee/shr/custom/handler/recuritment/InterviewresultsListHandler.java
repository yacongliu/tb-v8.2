package com.kingdee.shr.custom.handler.recuritment;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.basedata.org.AdminOrgUnitCollection;
import com.kingdee.eas.basedata.org.AdminOrgUnitFactory;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.PositionCollection;
import com.kingdee.eas.basedata.org.PositionFactory;
import com.kingdee.eas.basedata.org.PositionInfo;
import com.kingdee.eas.basedata.person.PersonCollection;
import com.kingdee.eas.basedata.person.PersonFactory;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.recruitment.InterviewresultsCollection;
import com.kingdee.eas.custom.recruitment.InterviewresultsEntryCollection;
import com.kingdee.eas.custom.recruitment.InterviewresultsEntryInfo;
import com.kingdee.eas.custom.recruitment.InterviewresultsFactory;
import com.kingdee.eas.custom.recruitment.InterviewresultsInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.hr.train.TrainRecordIsPassedEnum;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.base.syssetting.web.util.UserUtil;
import com.kingdee.shr.custom.handler.interviewutils.InterviewresUtils;
import com.kingdee.shr.custom.handler.recuritment.vo.InterviewPersonVo;
import com.kingdee.shr.custom.handler.recuritment.vo.InterviewerVo;
import com.kingdee.shr.recuritment.InterviewLinkCollection;
import com.kingdee.shr.recuritment.InterviewLinkFactory;
import com.kingdee.shr.recuritment.InterviewProcessCollection;
import com.kingdee.shr.recuritment.InterviewProcessFactory;
import com.kingdee.shr.recuritment.InterviewProcessInfo;
import com.kingdee.shr.recuritment.InterviewResultStatusEnum;
/**
 * 
 * @copyright 天津金蝶软件有限公司 <br>
 *            Title: InterviewresultsListHandler <br>
 *            Description:面试结果列表Handler
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年8月22日
 */
public class InterviewresultsListHandler extends ListHandler {
    private static Logger logger = Logger.getLogger(InterviewresultsListHandler.class);

    private Context ctx;

    public InterviewresultsListHandler(Context ctx) {
        this.ctx = ctx;
    }

    public InterviewresultsListHandler() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * 
     * <p>
     * Title: averageScoreAction
     * </p>
     * <p>
     * Description:写入平均分和通过率
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws SHRWebException
     */
    public void averageScoreAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws ShrWebBizException, SHRWebException {
        String ids = request.getParameter("billId");
        if (StringUtils.isBlank(ids)) { // 判空
            throw new ShrWebBizException("计算平均分和通过率的数据未知");
        }
        String[] billIds = ids.split(",");

        for (int i = 0; i < billIds.length; i++) {
            int avescore = getAvescore(billIds[i]);
            String passRate = getPassRate(billIds[i]);
            try {
                // 将平均分和通过率写入单据
                String sql = "UPDATE CT_REC_Interviewresults SET CFRatingSocre = " + avescore
                        + ", CFPassRate = '" + passRate + "'" + " WHERE FID = '" + billIds[i] + "'";
                DbUtil.execute(ctx, sql);
            } catch (BOSException e) {
                logger.error("计算平均分和通过率时出现错误", e);
                throw new SHRWebException("计算平均分和通过率时出现错误");
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }

    /**
     * 
     * <p>
     * Title: rankingAction
     * </p>
     * <p>
     * Description:排名
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     * @author 相建彬
     */
    public void rankingAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException, SHRWebException {
        // 分组排序语句
        String ranksql = "SELECT fid , dense_rank() over (partition by CFApplyOrgID,CFApplyJobID,CFInterLink order by CFRatingSocre desc ) as cfranking from CT_REC_Interviewresults";
        String ids = request.getParameter("billId");
        if (StringUtils.isBlank(ids)) { // 判空
            throw new ShrWebBizException("要排名的数据未知");
        }
        String[] billIds = ids.split(",");

        for (int i = 0; i < billIds.length; i++) {
            try {
                // 写入排名
                String upsql = "/*dialect*/ UPDATE CT_REC_Interviewresults SET cfranking = (select cfranking from("
                        + ranksql + ") where fid='" + billIds[i] + "') where fid='" + billIds[i] + "'";
                DbUtil.execute(ctx, upsql);
            } catch (BOSException e) {
                logger.error("排名时出现错误", e);
                throw new SHRWebException("排名时出现错误");
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }

    /**
     * 
     * @title finalRankingAction
     * @description 计算最终排名
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws SHRWebException
     */
    public void finalRankingAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        String ids = request.getParameter("billId");
        String lastLink = request.getParameter("lastLink");
        if (StringUtils.isBlank(ids)) { // 判空
            throw new ShrWebBizException("要排名的数据未知");
        }
        String[] billIds = ids.split(",");
        // 分组排序语句
        String ranksql = "SELECT fid ,dense_rank() over (partition by CFApplyOrgID,CFApplyJobID,FInterviewPlanID order by CFFinalRating desc ) as CFFinalRanking from CT_REC_Interviewresults where CFInterLink ='"
                + lastLink + "'";
        for (int i = 0; i < billIds.length; i++) {
            try {
                // 写入排名
                String upsql = "/*dialect*/ UPDATE CT_REC_Interviewresults SET CFFinalRanking = (select CFFinalRanking from("
                        + ranksql + ") where fid='" + billIds[i] + "') where fid='" + billIds[i] + "'";
                DbUtil.execute(ctx, upsql);
            } catch (BOSException e) {
                logger.error("最终排名时出现错误", e);
                throw new SHRWebException("最终排名时出现错误");
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }

    /**
     * @title averageFinalScoreAction
     * @description 计算总平均分
     * @author 相建彬
     * @throws ShrWebBizException
     */
    public void averageFinalScoreAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws ShrWebBizException, SHRWebException {
        String ids = request.getParameter("billId");
        if (StringUtils.isBlank(ids)) { // 判空
            throw new ShrWebBizException("要操作的数据未知");
        }
        String[] billIds = ids.split(",");

        for (int i = 0; i < billIds.length; i++) {
            try {
                int finalAveScore = getFinalAveScore(billIds[i]);
                // 将最终评分写入单据
                String sql = "UPDATE CT_REC_Interviewresults SET CFFinalRating = " + finalAveScore
                        + " WHERE FID = '" + billIds[i] + "'";
                DbUtil.execute(ctx, sql);
            } catch (BOSException e) {
                logger.error("计算最终平均分时出现错误", e);
                throw new SHRWebException("计算最终平均分时出现错误");
            } catch (SHRWebException e) {
                throw new SHRWebException("计算最终平均分时出现错误");
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }

    /**
     * 
     * <p>
     * Title: operationAction
     * </p>
     * <p>
     * Description:面试结果操作
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws ShrWebBizException
     * @throws SHRWebException
     */
    public void operationAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException, SHRWebException {
        // 获取id字符串
        String ids = request.getParameter("billId");
        // 获取操作类型
        String operation = request.getParameter("operation");
        // 待定
        if ("undetermined".equals(operation)) {
            String statusEnum = InterviewResultStatusEnum.WAIT_VALUE;
            Boolean flag = handle(ids, statusEnum);
            if (flag == false) {
                throw new ShrWebBizException("进行待定操作失败！");
            }
        }
        // 通过
        if ("pass".equals(operation)) {
            String statusEnum = InterviewResultStatusEnum.PASS_VALUE;
            Boolean flag = handle(ids, statusEnum);
            if (flag == false) {
                throw new ShrWebBizException("进行通过操作失败！");
            }
        }
        // 不通过
        if ("nopass".equals(operation)) {
            String statusEnum = InterviewResultStatusEnum.REFUSE_VALUE;
            Boolean flag = handle(ids, statusEnum);
            if (flag == false) {
                throw new ShrWebBizException("进行不通过操作失败！");
            }
        }
        // 放弃面试
        if ("giveupinterview".equals(operation)) {
            String statusEnum = InterviewResultStatusEnum.QUIT_VALUE;
            Boolean flag = handle(ids, statusEnum);
            if (flag == false) {
                throw new ShrWebBizException("进行放弃面试操作失败！");
            }
        }
        // 终止面试
        if ("endinterview".equals(operation)) {
            String statusEnum = InterviewResultStatusEnum.END_VALUE;
            Boolean flag = handle(ids, statusEnum);
            if (flag == false) {
                throw new ShrWebBizException("进行终止面试操作失败！");
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }

    /**
     * 
     * @Title handle
     * @Description 面试结果的操作（待定、通过、不通过、放弃、终止面试）
     * @param ids 数据id
     * @throws ShrWebBizException
     * @throws SHRWebException
     * @Author 相建彬
     */
    private Boolean handle(String ids, String isParss) throws ShrWebBizException, SHRWebException {
        // 判空
        if (StringUtils.isBlank(ids)) {
            throw new ShrWebBizException("要操作的数据未知");
        }
        String[] billIds = ids.split(",");
        InterviewresUtils ivutils = new InterviewresUtils();
        String personName = null;// 姓名
        String personNumber = null;// 编码
        String interviewSegment = null;// 环节
        String Score = null;// 评分
        // String description = null;// 评价
        int ranking = 0;// 排名
        boolean flag = false;

        for (int i = 0; i < billIds.length; i++) {
            String sql = "SELECT CFInterviewPersonName,CFInterviewPersonNumber,CFInterLink,CFRatingSocre,CFRanking,FDescription,FID FROM CT_REC_Interviewresults WHERE FID = '"
                    + billIds[i] + "'";
            try {
                IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
                while (rowSet.next()) {
                    personName = rowSet.getString("CFInterviewPersonName");
                    personNumber = rowSet.getString("CFInterviewPersonNumber");
                    interviewSegment = rowSet.getString("CFInterLink");
                    Score = String.valueOf(rowSet.getInt("CFRatingSocre"));
                    // description = rowSet.getString("FDescription");
                    ranking = rowSet.getInt("CFRanking");
                }
                // 操作前判断是否已经进行计算
                if (ranking == 0) {
                    throw new ShrWebBizException("操作前,请先对该候选人：" + personName + "进行计算和排名！");
                }
                // 执行操作
                flag = ivutils.synInterviewresults(ctx, personName, personNumber, interviewSegment, Score,
                        isParss);
                if (!flag) {
                    throw new ShrWebBizException("操作失败");
                }
            } catch (EASBizException e) {
                logger.error("进行操作时出现错误", e);
                return false;
            } catch (BOSException e) {
                logger.error("进行操作时出现错误", e);
                return false;
            } catch (SQLException e) {
                logger.error("查询数据时出现错误", e);
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * <p>
     * Title: approvalAction
     * </p>
     * <p>
     * Description: 录用报批
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @author 相建彬
     * @throws SHRWebException
     */
    public void approvalAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException, SHRWebException {
        String personName = null;// 姓名
        String personNumber = null;// 编码
        String interviewSegment = null;// 环节
        String ids = request.getParameter("billId");
        if (StringUtils.isBlank(ids)) { // 判空
            throw new ShrWebBizException("要录用报批的数据未知");
        }
        String[] billIds = ids.split(",");
        InterviewresUtils ivutils = new InterviewresUtils();

        for (int i = 0; i < billIds.length; i++) {
            String sql = "SELECT CFInterviewPersonName,CFInterviewPersonNumber,CFInterLink,CFRatingSocre,FID FROM CT_REC_Interviewresults WHERE FID = '"
                    + billIds[i] + "'";
            try {
                IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
                while (rowSet.next()) {
                    personName = rowSet.getString("CFInterviewPersonName");
                    personNumber = rowSet.getString("CFInterviewPersonNumber");
                    interviewSegment = rowSet.getString("CFInterLink");
                }

                ivutils.interviewPass(ctx, personName, personNumber, interviewSegment);
            } catch (BOSException e) {
                logger.error("进行录用报批时出现错误", e);
                throw new SHRWebException("进行录用报批时出现错误", e);
            } catch (SQLException e) {
                logger.error("进行录用报批时出现错误", e);
                throw new SHRWebException("进行录用报批时出现错误", e);
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }

    /**
     * 
     * <p>
     * Title: getBodyDataAction
     * </p>
     * <p>
     * Description:根据面试方案获取面试环节
     * </p>
     * 
     * @param request
     * @param response
     * @param model
     * @author 相建彬
     * @throws SHRWebException
     */
    public void getBodyDataAction(HttpServletRequest request, HttpServletResponse response, ModelMap model)
            throws SHRWebException {
        String schemeId = request.getParameter("schemeId");
        InterviewLinkCollection interviewLinkCollection = null;
        try {
            interviewLinkCollection = InterviewLinkFactory.getRemoteInstance()
                    .getInterviewLinkCollection("where ipid ='" + schemeId + "'");
        } catch (BOSException e) {
            throw new SHRWebException("获取面试环节时出现错误", e);
        }

        Map<String, InterviewLinkCollection> res = new HashMap<String, InterviewLinkCollection>();
        res.put("data", interviewLinkCollection);
        JSONUtils.writeJson(response, res);
    }

    /**
     * 
     * <p>
     * Title: importFileAction
     * </p>
     * <p>
     * Description:文件导入
     * </p>
     * 
     * @param file
     * @param request
     * @param response
     * @author 相建彬
     * @throws SHRWebException
     * @throws Exception
     */
    public void importFileAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws ShrWebBizException, SHRWebException {
        String contentType = request.getContentType();
        Map<String, String> map = new HashMap<String, String>(2);
        StringBuffer msg = new StringBuffer("其中过滤掉的重复数据的编码为");
        if ((contentType != null) && (contentType.indexOf("multipart/form-data") != -1)) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
            for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
                // 获取文件
                MultipartFile file = (MultipartFile) entity.getValue();
                // 获取文件名
                String fileName = file.getOriginalFilename();
                // 获取文件类型
                String fileType = fileName.substring(fileName.lastIndexOf("."), fileName.length());

                System.out.println("==============" + fileName + "================");
                File dir = new File(UserUtil.getUserTempDirAbsolutePath(request.getSession()));
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                // 校验模板
                if (!".xlsx".equals(fileType)) {
                    map.put("state", "4");
                    JSONUtils.writeJson(response, map);
                    return;
                }
                try {
                    // 读取文件数据存进list集合
                    List<String[]> list = POIUtils.readExcel(file);
                    // 校验模板
                    if (!"面试信息".equals(list.get(0)[0].substring(0, 4))) {
                        map.put("state", "1");
                        JSONUtils.writeJson(response, map);
                        return;
                    }
                    // 新建存放表格数据的Map集合
                    Map<String, InterviewPersonVo> interviewresultsInfosMap = new HashMap<String, InterviewPersonVo>();
                    CoreBaseCollection collection = new CoreBaseCollection();

                    // 遍历表格数据list
                    for (int i = 0; i < list.size(); i++) {
                        // 第3行开始读取表数据
                        if (i > 1) {
                            // 校验数据
                            boolean flag = checkExcel(list, i);
                            if (!flag) {
                                map.put("state", "2");
                                JSONUtils.writeJson(response, map);
                                return;
                            }
                            // 组装数据
                            assemblyData(list, interviewresultsInfosMap, i);
                        }
                    }

                    // 遍历组装的数据
                    for (Map.Entry<String, InterviewPersonVo> entry : interviewresultsInfosMap.entrySet()) {
                        assemblyInfoData(entry, collection, msg);
                    }
                    // 执行保存
                    InterviewresultsFactory.getRemoteInstance().addnew(collection);

                } catch (IOException e) {
                    logger.error("解析数据表出现错误", e);
                    map.put("state", "3");
                    JSONUtils.writeJson(response, map);
                    return;
                } catch (EASBizException e) {
                    logger.error("保存时出现错误", e);
                    map.put("state", "3");
                    JSONUtils.writeJson(response, map);
                    return;
                } catch (BOSException e) {
                    logger.error("保存时出现错误", e);
                    map.put("state", "3");
                    JSONUtils.writeJson(response, map);
                    return;
                }
            }
            map.put("msg", msg.toString());
            map.put("state", "success");
            JSONUtils.writeJson(response, map);
        }
    }

    /**
     * 
     * @Title assemblyInfoData
     * @Description 将excel表的数居存入实体集合
     * @param entry map集合
     * @throws SHRWebException
     * @Author 相建彬
     */
    private void assemblyInfoData(Map.Entry<String, InterviewPersonVo> entry, CoreBaseCollection collection,
            StringBuffer msg) throws ShrWebBizException, SHRWebException {
        InterviewPersonVo vo = entry.getValue();
        Set<InterviewerVo> interviewers = vo.getInterviewers();
        InterviewresultsInfo info = new InterviewresultsInfo();
        InterviewresultsEntryCollection entrys = info.getEntrys();

        // 校验重复数据
        Boolean flag = isexist(getInterviewPlanInfo(vo.getPlanName()).getId().toString(), vo.getLink(),
                vo.getNumber(), getOrgUnitInfo(vo.getAdminOrg()).getId().toString(),
                getPositionInfo(vo.getPositionName(), vo.getPositionNum()).getId().toString());

        if (flag == false) {
            try {
                // 面试人编码
                info.setInterviewPersonNumber(vo.getNumber());
                // 面试人姓名
                info.setInterviewPersonName(vo.getName());
                // 应聘部门
                info.setApplyOrg(getOrgUnitInfo(vo.getAdminOrg()));
                // 应聘职位
                info.setApplyJob(getPositionInfo(vo.getPositionName(), vo.getPositionNum()));
                // 面试环节
                info.setInterLink(vo.getLink());
                // 面试方案
                info.setInterviewPlan(getInterviewPlanInfo(vo.getPlanName()));
                // 创建年份
                info.setCreateYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

                // 分录-面试官信息
                Iterator<InterviewerVo> iterator = interviewers.iterator();
                while (iterator.hasNext()) {
                    InterviewerVo interviewerVo = iterator.next();
                    InterviewresultsEntryInfo entryinfo = new InterviewresultsEntryInfo();
                    // 是否通过
                    if (interviewerVo.isThrough()) {
                        entryinfo.setIsThough(TrainRecordIsPassedEnum.PASSED);
                    } else {
                        entryinfo.setIsThough(TrainRecordIsPassedEnum.NOT_PASSED);
                    }
                    // 评分
                    if (StringUtils.isNotBlank(interviewerVo.getScore())) {
                        entryinfo.setInterviewScore(Integer.parseInt(interviewerVo.getScore()));
                    } else {
                        entryinfo.setInterviewScore(0);
                    }
                    // 面试官信息
                    entryinfo.setPerson(getPersonInfo(interviewerVo.getName(), interviewerVo.getNumber()));
                    // 评价
                    entryinfo.setDescription(interviewerVo.getDescription());
                    // 添加分录信息
                    entrys.add(entryinfo);
                }
                // 添加进集合
                collection.add(info);
            } catch (NumberFormatException e) {
                logger.error("导入时出现错误", e);
                throw new SHRWebException("导入时出现错误");
            } catch (SHRWebException e) {
                logger.error("导入时出现错误", e);
                throw new SHRWebException("导入查询时出现错误");
            }
        } else {
            // 添加重复数据编码
            msg.append(vo.getNumber()).append("、");
        }

    }

    /**
     * 
     * @Title assemblyData
     * @Description 组装数据
     * @param list 读取的数据集合
     * @param interviewresultsInfosMap
     * @param i 当前行数
     * @Author 相建彬
     */
    private void assemblyData(List<String[]> list, Map<String, InterviewPersonVo> interviewresultsInfosMap,
            int i) {
        // 面试人信息
        InterviewPersonVo interviewPersonVo = new InterviewPersonVo();
        // 分录面试官信息
        InterviewerVo interviewerVo = new InterviewerVo();
        // 新建存放分录面试官信息的Set集合
        Set<InterviewerVo> interviewers = new HashSet<InterviewerVo>();
        // Map集合的key值
        String key = list.get(i)[0] + list.get(i)[1] + list.get(i)[2] + list.get(i)[4] + list.get(i)[5]
                + list.get(i)[6];

        // 面试官姓名
        interviewerVo.setName(list.get(i)[7]);
        // 面试官编码
        interviewerVo.setNumber(list.get(i)[8]);
        // 是否通过
        if ("通过".equals(list.get(i)[9])) {
            interviewerVo.setThrough(true);
        } else {
            interviewerVo.setThrough(false);
        }
        // 评分
        interviewerVo.setScore(list.get(i)[10]);
        // 评价
        interviewerVo.setDescription(list.get(i)[11]);

        // 判断map集合中是否已经包含该条数据
        if (interviewresultsInfosMap.containsKey(key)) {
            InterviewPersonVo info = interviewresultsInfosMap.get(key);
            info.getInterviewers().add(interviewerVo);
        } else {
            interviewers.add(interviewerVo);
            // =====将分录信息set集合添加进面试人信息中======
            interviewPersonVo.setInterviewers(interviewers);
            // 面试人编码
            interviewPersonVo.setNumber(list.get(i)[0]);
            // 面试人员姓名
            interviewPersonVo.setName(list.get(i)[1]);
            // 招聘部门
            interviewPersonVo.setAdminOrg(list.get(i)[2]);
            // 招聘职位名称
            interviewPersonVo.setPositionName(list.get(i)[3]);
            // 招聘职位编码
            interviewPersonVo.setPositionNum(list.get(i)[4]);
            // 面试环节
            interviewPersonVo.setLink(list.get(i)[5]);
            // 面试方案
            interviewPersonVo.setPlanName(list.get(i)[6]);
            // 将一条完整的面试人信息添加进map集合中
            interviewresultsInfosMap.put(key, interviewPersonVo);
        }
    }

    /**
     * 
     * @Title checkExcel
     * @Description 校验excel数据
     * @param list 读取的数据集合
     * @param i 当前行数
     * @throws ShrWebBizException
     * @Author 相建彬
     */
    private boolean checkExcel(List<String[]> list, int i) {
        // 数据校验
        if (StringUtils.isBlank(list.get(i)[0]) || StringUtils.isBlank(list.get(i)[1])
                || StringUtils.isBlank(list.get(i)[2]) || StringUtils.isBlank(list.get(i)[3])
                || StringUtils.isBlank(list.get(i)[4]) || StringUtils.isBlank(list.get(i)[5])
                || StringUtils.isBlank(list.get(i)[6])) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @Title isexist
     * @Description 校验导入数据是否存在
     * @return boolean true 存在 false 不存在
     * @throws SHRWebException
     * @Author 相建彬
     */
    private boolean isexist(String FInterviewPlanID, String CFInterLink, String CFInterviewPersonNumber,
            String CFApplyOrgID, String CFApplyJobID) throws SHRWebException {
        String sql = "SELECT FID FROM CT_REC_Interviewresults WHERE FInterviewPlanID='" + FInterviewPlanID
                + "' AND CFInterLink='" + CFInterLink + "' AND CFInterviewPersonNumber='"
                + CFInterviewPersonNumber + "' AND CFApplyOrgID='" + CFApplyOrgID + "' AND CFApplyJobID='"
                + CFApplyJobID + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            if (rowSet.size() == 0) {
                return false;
            }
        } catch (BOSException e) {
            throw new SHRWebException("校验重复数据时出现错误", e);
        }

        return true;
    }

    /**
     * 
     * @Title getPersonInfo
     * @Description 获取人员信息
     * @param personName 人员姓名
     * @param personNumber 人员编码
     * @return personInfo 人员
     * @throws SHRWebException
     * @Author 相建彬
     */
    private PersonInfo getPersonInfo(String personName, String personNumber) throws SHRWebException {
        PersonInfo personInfo = null;
        try {
            PersonCollection personCollection = PersonFactory.getRemoteInstance().getPersonCollection(
                    "where name = '" + personName + "' and number ='" + personNumber + "'");
            if (personCollection.size() > 0) {
                personInfo = personCollection.get(0);
            }
        } catch (BOSException e) {
            throw new SHRWebException("获取人员信息时出现错误", e);
        }

        return personInfo;
    }

    /**
     * 
     * <p>
     * Title: getOrgUnitInfo
     * </p>
     * <p>
     * Description:获取组织
     * </p>
     * 
     * @param orgName 组织长名称
     * @return adminOrgUnitInfo 组织信息
     * @author 相建彬
     * @throws SHRWebException
     */
    private AdminOrgUnitInfo getOrgUnitInfo(String orgName) throws SHRWebException {
        AdminOrgUnitInfo adminOrgUnitInfo = null;
        try {
            AdminOrgUnitCollection adminOrgUnitCollection = AdminOrgUnitFactory.getLocalInstance(this.ctx)
                    .getAdminOrgUnitCollection(" where DisplayName='" + orgName + "'");
            if (adminOrgUnitCollection.size() > 0) {
                adminOrgUnitInfo = adminOrgUnitCollection.get(0);
            }
        } catch (BOSException e) {
            throw new SHRWebException("获取组织信息时出现错误", e);
        }

        return adminOrgUnitInfo;
    }

    /**
     * 
     * <p>
     * Title: getPositionInfo
     * </p>
     * <p>
     * Description: 获取职位
     * </p>
     * 
     * @param posName 职位名称
     * @param posNumber 职位编码
     * @return positionInfo 职位信息
     * @author 相建彬
     * @throws SHRWebException
     */
    private PositionInfo getPositionInfo(String posName, String posNumber) throws SHRWebException {
        PositionInfo positionInfo = null;
        try {
            PositionCollection positionCollection = PositionFactory.getRemoteInstance()
                    .getPositionCollection("where name = '" + posName + "' and number ='" + posNumber + "'");
            if (positionCollection.size() > 0) {
                positionInfo = positionCollection.get(0);
            }
        } catch (BOSException e) {
            throw new SHRWebException("获取职位信息时出现错误", e);
        }

        return positionInfo;
    }

    /**
     * 
     * @Title getInterviewPlanInfo
     * @Description 获取面试方案
     * @param planName 方案名称
     * @return interviewPlanInfo 面试方案信息
     * @throws SHRWebException
     * @Author 相建彬
     */
    private InterviewProcessInfo getInterviewPlanInfo(String planName) throws SHRWebException {
        InterviewProcessInfo inProcessInfo = null;
        InterviewProcessCollection interviewProcessCollection;
        try {
            interviewProcessCollection = InterviewProcessFactory.getRemoteInstance()
                    .getInterviewProcessCollection("where name = '" + planName + "'");
            if (interviewProcessCollection.size() > 0) {
                inProcessInfo = interviewProcessCollection.get(0);
            }
        } catch (BOSException e) {
            throw new SHRWebException("获取面试方案信息时出现错误", e);
        }

        return inProcessInfo;
    }

    /**
     * 
     * @title getAvescore
     * @description 计算平均分
     * @param billid 数据id
     * @return avescore 平均分
     * @throws SHRWebException
     * @author 相建彬
     */
    private Integer getAvescore(String billid) throws SHRWebException {
        int sumscore = 0;// 总分
        int avescore = 0;// 平均分
        String sql = "SELECT FBillID,CFInterviewScore,FPersonID,CFIsThough FROM CT_REC_InterviewresultsEntry WHERE FBillID = '"
                + billid + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                sumscore += rowSet.getInt("CFInterviewScore");
            }
            if (rowSet.size() > 0) {
                // 计算平均分
                avescore = sumscore / rowSet.size();
            }
        } catch (BOSException e) {
            throw new SHRWebException("计算平均分时出现错误", e);
        } catch (SQLException e) {
            throw new SHRWebException("计算平均分时出现错误", e);
        }

        return avescore;
    }

    /**
     * 
     * <p>
     * Title: getPassRate
     * </p>
     * <p>
     * Description:计算通过率
     * </p>
     * 
     * @param billid 数据id
     * @return passRate 通过率
     * @author 相建彬
     * @throws SHRWebException
     */
    private String getPassRate(String billid) throws SHRWebException {
        // 创建一个数值格式化对象
        NumberFormat numberFormat = NumberFormat.getInstance();
        // 设置精确到小数点后2位
        numberFormat.setMaximumFractionDigits(2);
        // 通过率
        String passRate = null;
        // 通过数
        int countpass = 0;
        // 总数
        int count = 0;

        String sql = "SELECT FBillID,CFInterviewScore,FPersonID,CFIsThough FROM CT_REC_InterviewresultsEntry WHERE FBillID = '"
                + billid + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            count = rowSet.size();
            // 当通过时countpass加一
            while (rowSet.next()) {
                if ("10".equals(rowSet.getString("CFIsThough"))) {
                    countpass++;
                }
            }
            if (count > 0) {
                // 计算通过率
                passRate = numberFormat.format((float) countpass / (float) count * 100) + "%";
            }
        } catch (BOSException e) {
            throw new SHRWebException("计算通过率时出现错误", e);
        } catch (SQLException e) {
            throw new SHRWebException("计算通过率时出现错误", e);
        }

        return passRate;
    }

    /**
     * 
     * @description 计算最终评分
     * @title getFinalAveScore
     * @param billid 数据id
     * @return finalAveScore 最终评分
     * @throws SHRWebException
     * @author 相建彬
     */
    private int getFinalAveScore(String billid) throws SHRWebException {
        double sumscore = 0;// 总平均分
        double finalAveScore = 0;// 最终平均分
        double weights = 0;// 权重
        int rating = 0;// 评分
        Map<String, String> map = getInerviewNumber(billid);
        String interviewPersonNumber = map.get("interviewPersonNumber");
        String interviewPlanID = map.get("interviewPlanID");
        // 取得面试者的面试结果
        InterviewresultsCollection interviewresultsCollection = getInterviewResult(interviewPersonNumber);
        // 取得面试环节的结果集
        InterviewLinkCollection interviewLinkCollection = getInterviewLink(interviewPlanID);
        // 遍历面试结果
        for (int j = 0; j < interviewresultsCollection.size(); j++) {
            // 遍历面试环节
            for (int i = 0; i < interviewLinkCollection.size(); i++) {
                if (interviewresultsCollection.get(j).getInterLink()
                        .equals(interviewLinkCollection.get(i).getInterviewStageName())) {
                    try {
                        // 将当前环节权重转换为小数
                        weights = (Double) NumberFormat.getPercentInstance()
                                .parse(interviewLinkCollection.get(i).getInterviewStageDescription());
                        // 当前环节评分
                        rating = interviewresultsCollection.get(j).getRatingSocre();
                        // 总评分
                        sumscore += (weights * rating);
                    } catch (ParseException e) {
                        throw new ShrWebBizException("百分数转换为小数时报错");
                    }

                }
            }
        }
        if (interviewresultsCollection.size() > 0) {
            finalAveScore = sumscore / interviewresultsCollection.size();
        }

        return (int) finalAveScore;

    }

    /**
     * @description 获取面试者编码和面试方案id
     * @title getInerviewNumber
     * @param billid 数据id
     * @return interviewPersonNumber 面试者编码
     * @throws SHRWebException
     * @author 相建彬
     */
    private Map<String, String> getInerviewNumber(String billid) throws SHRWebException {
        String interviewPersonNumber = null;
        String interviewPlanID = null;
        Map<String, String> map = new HashMap<String, String>(2);
        // 根据id获取面试者编码
        String sql = "SELECT CFInterviewPersonNumber,FInterviewPlanID,fid FROM CT_REC_Interviewresults WHERE Fid = '"
                + billid + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                interviewPersonNumber = rowSet.getString("CFInterviewPersonNumber");
                interviewPlanID = rowSet.getString("FInterviewPlanID");
                map.put("interviewPersonNumber", interviewPersonNumber);
                map.put("interviewPlanID", interviewPlanID);
            }
        } catch (BOSException e) {
            throw new SHRWebException("查询面试者编码出错", e);
        } catch (SQLException e) {
            throw new SHRWebException("查询面试者编码出错", e);
        }
        return map;
    }

    /**
     * 
     * @description 根据面试者编码获取所有面试结果
     * @title getInterviewResult
     * @return interviewresultsCollection 面试结果集
     * @author 相建彬
     * @throws SHRWebException
     */
    private InterviewresultsCollection getInterviewResult(String interviewPersonNumber)
            throws SHRWebException {
        InterviewresultsCollection interviewresultsCollection = null;
        try {
            interviewresultsCollection = InterviewresultsFactory.getRemoteInstance()
                    .getInterviewresultsCollection(
                            "where InterviewPersonNumber = '" + interviewPersonNumber + "'");
        } catch (BOSException e) {
            throw new SHRWebException("获取面试者的面试结果时出现错误", e);
        }
        return interviewresultsCollection;
    }

    /**
     * 
     * @description 根据面试方案id获取面试环节信息
     * @title getInterviewLink
     * @param interviewPlanID 面试方案id
     * @return interviewLinkCollection 面试环节结果集
     * @throws SHRWebException
     * @author 相建彬
     */
    private InterviewLinkCollection getInterviewLink(String interviewPlanID) throws SHRWebException {
        InterviewLinkCollection interviewLinkCollection = null;
        try {
            interviewLinkCollection = InterviewLinkFactory.getRemoteInstance()
                    .getInterviewLinkCollection("where ipid ='" + interviewPlanID + "'");
        } catch (BOSException e) {
            throw new SHRWebException("获取面试环节信息时出现错误", e);
        }
        return interviewLinkCollection;
    }

}