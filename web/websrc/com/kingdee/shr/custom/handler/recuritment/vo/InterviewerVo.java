package com.kingdee.shr.custom.handler.recuritment.vo;

/**
 * 
 * @Copyright 天津金蝶软件有限公司
 * @Title InterviewPersonVo
 * @Description 面试官传输对象
 * @Author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @Date 2019年8月28日
 */
public class InterviewerVo {
    /*
     * 面试官名称
     */
    private String name;

    /*
     * 面试官编码
     */
    private String number;

    /*
     * 是否通过
     */
    private boolean through;

    /*
     * 评分
     */
    private String score;

    /*
     * 评价
     */
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isThrough() {
        return through;
    }

    public void setThrough(boolean through) {
        this.through = through;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "InterviewerVo [name=" + name + ", number=" + number + ", through=" + through + ", score="
                + score + ", description=" + description + "]";
    }

}
