package com.kingdee.shr.custom.handler.recuritment.vo;

import java.util.Set;

/**
 * 
 * @Copyright 天津金蝶软件有限公司
 * @Title InterviewPersonVo
 * @Description 面试者传输对象
 * @Author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @Date 2019年8月28日
 */
public class InterviewPersonVo {
    /*
     * 面试者名称
     */
    private String name;

    /*
     * 面试者编码
     */
    private String number;

    /*
     * 部门
     */
    private String adminOrg;

    /*
     * 职位名称
     */
    private String positionName;

    /*
     * 职位编码
     */
    private String positionNum;

    /*
     * 环节
     */
    private String link;

    /*
     * 面试官信息
     */
    private Set<InterviewerVo> interviewers;

    /*
     * 面试方案名称
     */
    private String planName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAdminOrg() {
        return adminOrg;
    }

    public void setAdminOrg(String adminOrg) {
        this.adminOrg = adminOrg;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNum() {
        return positionNum;
    }

    public void setPositionNum(String positionNum) {
        this.positionNum = positionNum;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Set<InterviewerVo> getInterviewers() {
        return interviewers;
    }

    public void setInterviewers(Set<InterviewerVo> interviewers) {
        this.interviewers = interviewers;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    @Override
    public String toString() {
        return "InterviewPersonVo [name=" + name + ", number=" + number + ", adminOrg=" + adminOrg
                + ", positionName=" + positionName + ", positionNum=" + positionNum + ", link=" + link
                + ", interviewers=" + interviewers + ", planName=" + planName + "]";
    }

}
