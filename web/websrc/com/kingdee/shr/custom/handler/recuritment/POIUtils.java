package com.kingdee.shr.custom.handler.recuritment;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class POIUtils {

    /**
     * 导出Excel
     */

    /*
     * 参数说明： tableName：表名 String 类型 tableHead：表头内容 Map<String, String>类型 tableBody：表格数据
     * List<Map<String, String>>类型 可使用org.apache.commons.beanutils.BeanUtils中的
     * BeanUtils.describe(Obj)方法将Javabean转化为Map类型 也可以使用JavaBean2Map.JavaBean2Map(Object
     * bean)（利用反射自己写的工具类）
     */
    // 示例：
    /*
     * //表名 String tableName="学生信息统计表"; //表头内容 Map<String, String> tableHead=new HashMap<>();
     * tableHead.put("id", "编号"); tableHead.put("name", "姓名"); tableHead.put("sex", "性别");
     * tableHead.put("date", "日期");
     *
     * //表数据(Test为Java对象) Test test=new Test(1, "lisis","男", new Date()) //将Javabean转化为Map类型
     * Map<String, String> map=BeanUtils.describe(test); //或者使用Map<String, String>
     * map=JavaBean2Map.JavaBean2Map(test);将Javabean转化为Map类型 List<Map<String, String>> tableBody=new
     * ArrayList<>(); tableBody.add(map); //调用导出工具类 POIUtils.Excel(response, tableName, tableHead,
     * tableBody);
     */
    public static void downLoadExcel(HttpServletResponse response, String tableName,
            Map<String, String> tableHead, List<Map<String, String>> tableBody) throws Exception {
        // 创建XSSFWorkbook对象(excel的工作簿对象)
        XSSFWorkbook wb = new XSSFWorkbook();
        // 建立新的sheet对象（excel的表单）
        XSSFSheet sheet = wb.createSheet("数据表");
        // cell样式
        XSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);// 水平居中
        style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
        // 创建第1行(标题行)（Excel默认下标从0开始）
        XSSFRow row1 = sheet.createRow(0);
        // 创建第1行的第1列（Excel默认下标从0开始）
        XSSFCell cell1 = row1.createCell(0);
        // 设置第1行标题行行高
        sheet.getRow(0).setHeight((short) (2 * 256));
        // 合并单元格CellRangeAddress构造参数依次表示（起始行，截至行，起始列， 截至列 )
        // 合并第一行的列
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, tableHead.size() - 1));
        // 设置单元格内容
        sheet.getRow(0).getCell(0).setCellValue(tableName);
        // 字体居中
        sheet.getRow(0).getCell(0).setCellStyle(style);

        // 创建第二行(表头)
        XSSFRow row2 = sheet.createRow(1);
        // 设置局部变量定义表的列号
        int index = 0;
        // 遍历表头（map.entrySet（）是将map里的每一个键值对取出来封装成一个Entry对象在存到一个Set里面）
        for (Map.Entry<String, String> head : tableHead.entrySet()) {
            // 创建第2行的第index列（Excel默认下标从0开始）
            XSSFCell cell = row2.createCell(index);
            // 设置单元格内容
            cell.setCellValue(head.getValue());
            // 字体居中
            cell.setCellStyle(style);
            index++;
        }

        // 遍历表数据
        for (int i = 0; i < tableBody.size(); i++) {
            index = 0;
            // 创建第2+i行(表数据行)
            XSSFRow row = sheet.createRow(2 + i);
            // 根据表头提供的键遍历对应的数据列的值
            for (Map.Entry<String, String> head : tableHead.entrySet()) {
                // 创建第2+i行的index列(表数据的列)
                XSSFCell cell = row.createCell(index);
                // tableBody.get(i).get(head.getKey()获取的是表头的提供的键head.getKey()所对应Map的值
                if (tableBody.get(i).get(head.getKey()) == null) {
                    cell.setCellValue(String.valueOf(""));
                } else {
                    cell.setCellValue(String.valueOf(tableBody.get(i).get(head.getKey())));
                }

                cell.setCellStyle(style);
                index++;
            }
        }

        /*
         * cellNum:表示要冻结的列数； rowNum:表示要冻结的行数； firstCellNum:表示被固定列右边第一列的列号； firstRollNum
         * :表示被固定行下边第一列的行号;
         */
        // 冻结表头和标题行（冻结前两行）
        sheet.createFreezePane(0, 2, 0, 2);
        // 根据第一行标题自动适应宽度
        sheet.autoSizeColumn(0, true);
        // 如果内容宽度大于默认宽度，设置为内容的宽度。
        for (int i = 0; i < tableHead.size(); i++) {
            int lg = sheet.getColumnWidth(i) / 256;
            for (int j = 1; j < tableBody.size(); j++) {
                String l = new String(sheet.getRow(j).getCell(i).getStringCellValue().getBytes("GB2312"),
                        "ISO-8859-1");
                int lg2 = l.length();
                if (lg < lg2) {
                    lg = lg2;
                }
            }
            if (lg > 99) {
                lg = 99;
            }
            sheet.setColumnWidth(i, (lg + 1) * 256);
        }

        // 输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(tableName.getBytes("gbk"), "iso8859-1") + ".xlsx");
        // response.setCharacterEncoding("utf-8");
        wb.write(output);
        output.close();

    }

    /**
     * excel导入
     */
    private final static String xls = "xls";

    private final static String xlsx = "xlsx";

    /**
     * 读入excel文件，解析后返回 return List<String[]>
     */
    public static List<String[]> readExcel(MultipartFile file) throws IOException {
        // 检查文件
        checkFile(file);
        // 获得Workbook工作薄对象
        Workbook workbook = getWorkBook(file);
        // 创建返回对象，把每行中的值作为一个数组，所有行作为一个集合返回
        List<String[]> list = new ArrayList<String[]>();
        if (workbook != null) {
            for (int sheetNum = 0; sheetNum < 1; sheetNum++) {
                // 获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if (sheet == null) {
                    continue;
                }
                // 获得当前sheet的开始行
                int firstRowNum = sheet.getFirstRowNum();
                // 获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                // 循环除了第一行的所有行
                for (int rowNum = firstRowNum; rowNum <= lastRowNum; rowNum++) {
                    // 获得当前行
                    Row row = sheet.getRow(rowNum);
                    if (row == null) {
                        continue;
                    }
                    // 获得当前行的开始列
                    int firstCellNum = row.getFirstCellNum();
                    // 获得当前行的列数
                    int lastCellNum = row.getLastCellNum();
                    String[] cells = new String[lastCellNum];
                    // 循环当前行
                    for (int cellNum = firstCellNum; cellNum < lastCellNum; cellNum++) {
                       // System.out.println(cellNum + "      " + lastCellNum);
                        Cell cell = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        cells[cellNum] = getCellValue(cell);
                    }
                    list.add(cells);
                }
            }
            workbook.close();
        }
        return list;
    }

    public static void checkFile(MultipartFile file) throws IOException {
        // 判断文件是否存在
        if (null == file) {
            throw new FileNotFoundException("文件不存在！");
        }
        // 获得文件名
        String fileName = file.getOriginalFilename();
        // 判断文件是否是excel文件
        if (!fileName.endsWith(xls) && !fileName.endsWith(xlsx)) {
            throw new IOException(fileName + "不是excel文件");
        }
    }

    public static Workbook getWorkBook(MultipartFile file) {
        // 获得文件名
        String fileName = file.getOriginalFilename();
        // 创建Workbook工作薄对象，表示整个excel
        Workbook workbook = null;
        try {
            // 获取excel文件的io流
            InputStream is = file.getInputStream();
            // 根据文件后缀名不同(xls和xlsx)获得不同的Workbook实现类对象
            if (fileName.endsWith(xls)) {
                // 2003
                workbook = new HSSFWorkbook(is);
            } else if (fileName.endsWith(xlsx)) {
                // 2007 及2007以上
                workbook = new XSSFWorkbook(is);
            }
        } catch (IOException e) {
        }
        return workbook;
    }

    /**
     * 处理单元格格式
     *
     * @param cell
     * @return
     */
    @SuppressWarnings("deprecation")
    public static String getCellValue(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellType()) {
        // 数字
        case Cell.CELL_TYPE_NUMERIC:

            // 日期格式的处理
            if (DateUtil.isCellDateFormatted(cell)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                return sdf.format(DateUtil.getJavaDate(cell.getNumericCellValue())).toString();
            } else {
                cell.setCellType(Cell.CELL_TYPE_STRING);// 把数字当成String来读，避免出现1读成1.0的情况
            }

            return String.valueOf(cell.getStringCellValue());

        // 字符串
        case Cell.CELL_TYPE_STRING:
            return cell.getStringCellValue();

        // 公式
        case Cell.CELL_TYPE_FORMULA:
            return cell.getCellFormula();

        // 空白
        case Cell.CELL_TYPE_BLANK:
            return "";

        // 布尔取值
        case Cell.CELL_TYPE_BOOLEAN:
            return cell.getBooleanCellValue() + "";

        // 错误类型
        case Cell.CELL_TYPE_ERROR:
            return cell.getErrorCellValue() + "";
        }

        return "";
    }

}
