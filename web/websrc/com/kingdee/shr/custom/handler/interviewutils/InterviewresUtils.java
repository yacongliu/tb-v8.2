package com.kingdee.shr.custom.handler.interviewutils;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.bi.model.DB.DBUtil;
import com.kingdee.bos.framework.ejb.EJBFactory;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.recuritment.InterviewInviteStatusEnum;
import com.kingdee.shr.recuritment.service.interviewResult.NewInterviewResultService;
import com.kingdee.util.StringUtils;

/**
 * 
 * @copyright 天津金蝶软件有限公司 <br>
 *            Title: InterviewresUtils<br>
 *            Description:面试结果工具类
 * @author 贺军磊 Email:1617340211@qq.com
 * @date 2019-9-3
 */
public class InterviewresUtils {

    /**
     * 
     * <p>
     * Title: process
     * </p>
     * <p>
     * Description:点击通过时同步数据至
     * </p>
     * 
     * @param ctx
     * @param personName面试候选人姓名
     * @param personNumber 面试候选人编码
     * @param interviewSegment 面试环节
     * @param isPass 是否通过
     * @param Score 分数
     * @return true为成功 false为失败
     * @throws EASBizException
     * @throws BOSException
     * @throws ShrWebBizException
     */
    public boolean synInterviewresults(Context ctx, String personName, String personNumber,
            String interviewSegment, String Score, String isParss)
            throws EASBizException, BOSException, ShrWebBizException {
        if (StringUtils.isEmpty(personName) || StringUtils.isEmpty(personNumber)
                || StringUtils.isEmpty(personName) || StringUtils.isEmpty(Score)
                || StringUtils.isEmpty(isParss)) {
            return false;
        }
        if (!StringUtils.isEmpty(getPlanId(ctx, personName, personNumber, interviewSegment))) {
            return updatePlanResult(ctx, getPlanId(ctx, personName, personNumber, interviewSegment), isParss,
                    Score);
        }

        return false;
    }

    /**
     * 
     * <p>
     * Title: interviewPass
     * </p>
     * <p>
     * Description:录用报批
     * </p>
     * 
     * @param ctx 上下文
     * @param personName 候选人名称
     * @param personNumber 候选人编码
     * @param interviewSegment 面试环节
     * @throws ShrWebBizException
     */
    public void interviewPass(Context ctx, String personName, String personNumber, String interviewSegment)
            throws ShrWebBizException {
        if (StringUtils.isEmpty(personName) || StringUtils.isEmpty(personNumber)
                || StringUtils.isEmpty(interviewSegment)) {
            throw new ShrWebBizException("录用报批失败候选人与候选人编码或面试环节不可为空！");
        }
        interviewCheck(ctx, personName, personNumber, interviewSegment);
        passInterview(ctx, personName, personNumber, interviewSegment);
    }

    /**
     * 
     * <p>
     * Title: interviewCheck
     * </p>
     * <p>
     * Description: 录用报批校验 -卫模式
     * </p>
     * 
     * @param ctx 上下文
     * @param personName 候选人名称
     * @param personNumber 候选人编码
     * @param interviewSegment 面试环节
     * @throws ShrWebBizException
     * @author 贺军磊
     */
    private void interviewCheck(Context ctx, String personName, String personNumber, String interviewSegment)
            throws ShrWebBizException {
        if (isBlackList(ctx, personName, personNumber)) {
            throw new ShrWebBizException("该候选人：" + personName + "在黑名单中，不能进行该操作！");
        }

        if (!isFinalLink(ctx, personName, personNumber, interviewSegment)) {
            throw new ShrWebBizException("该候选人：" + personName + "不是面试最终环节，不能进行该操作！");
        }

        if (!isAllPass(ctx, personName, personNumber)) {
            throw new ShrWebBizException("该候选人：" + personName + "面试环节没有全部通过，不能进行该操作！");
        }
    }

    /**
     * <p>
     * Title: isFinalLink
     * </p>
     * <p>
     * Description: 判断是否最终面试环节
     * </p>
     * 
     * @param ctx
     * @param personName 候选人
     * @param personNumber 候选人编码
     * @param interviewSegment 面试环节
     * @return
     * @throws ShrWebBizException
     */
    private boolean isFinalLink(Context ctx, String personName, String personNumber, String interviewSegment)
            throws ShrWebBizException {

        String planId = getPlanId(ctx, personName, personNumber, interviewSegment);
        if (StringUtils.isEmpty(planId)) {
            return false;
        }
        String sql = "select flastlink FROM T_REC_InterviewPlan where fid='" + planId + "'";
        System.out.println("判断是否最终环节SQL" + sql);
        String link;
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                link = rowSet.getString("flastlink");
                System.out.println("查询是否是最终环节" + link);
                if ("1".equals(link)) {
                    return true;
                }
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("判断是否最终环节失败！", e);
        } catch (SQLException e) {
            throw new ShrWebBizException("判断是否最终环节失败！", e);
        }

        return false;
    }

    /**
     * 
     * <p>
     * Title: blacklist
     * </p>
     * <p>
     * Description: 判断该候选人是否存在黑名单中
     * </p>
     * 
     * @param ctx
     * @param personName 候选人名称
     * @param personNumber 候选人编码
     * @return 在黑名单中为true不在为false
     * @throws ShrWebBizException
     */

    private boolean isBlackList(Context ctx, String personName, String personNumber)
            throws ShrWebBizException {
        String interviewReusltId = null;
        boolean isInBlack = false;

        StringBuffer sb = new StringBuffer("SELECT fid FROM T_REC_InterviewResult where fresumebaserecid=(")
                .append("SELECT fid FROM T_REC_ResumeBaseRec where fname='" + personName + "' and fnumber='"
                        + personNumber + "')");
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sb.toString());
            while (rowSet.next()) {
                interviewReusltId = rowSet.getString("fid");
            }
            if (!StringUtils.isEmpty(interviewReusltId)) {
                isInBlack = NewInterviewResultService.getInstance()
                        .checkCandidateIsInBlackList(interviewReusltId);
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("判断候选人为" + personName + " 是否在黑名单中失败！", e);
        } catch (SQLException e) {
            throw new ShrWebBizException("判断候选人为" + personName + " 是否在黑名单中失败！", e);
        }
        return isInBlack;

    }

    /**
     * 
     * <p>
     * Title: isAllPass
     * </p>
     * <p>
     * Description: 判断当前候选人所有环节是否通过
     * </p>
     * 
     * @param ctx
     * @param personName 候选人名称
     * @param personNumber 候选人编码
     * @return true为全部通过 false为有环节未通过
     * @throws ShrWebBizException
     */
    private boolean isAllPass(Context ctx, String personName, String personNumber) throws ShrWebBizException {
        StringBuffer sb = new StringBuffer();
        String status;
        sb.append("SELECT finvitestatus FROM  T_REC_InterviewPlan where ")
                .append(" finterviewid=(SELECT fid FROM T_REC_InterviewResult where fresumebaserecid= ")
                .append(" (SELECT fid FROM T_REC_ResumeBaseRec where fname='" + personName + "' and fnumber='"
                        + personNumber + "')) ");
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sb.toString());
            while (rowSet.next()) {
                status = rowSet.getString("finvitestatus");
                if (!"6".equals(status)) {
                    return false;
                }
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("判断候选人为" + personName + " 全部环节是否通过失败！", e);
        } catch (SQLException e) {
            throw new ShrWebBizException("判断候选人为" + personName + " 全部环节是否通过失败！", e);
        }
        return true;
    }

    /**
     * 
     * <p>
     * Title: passInterview
     * </p>
     * <p>
     * Description:通过强制反设调用标准产的产品的录用报批功能
     * </p>
     * 
     * @param ctx 上下文
     * @param personName 候选人名称
     * @param personNumber 候选人编码
     * @param interviewSegment 面试环节
     * @throws ShrWebBizException
     */
    private void passInterview(Context ctx, String personName, String personNumber, String interviewSegment)
            throws ShrWebBizException {

        try {
            Class<?> classBook = Class.forName("com.kingdee.shr.recuritment.app.InterviewPlanControllerBean");
            Method methodBook = classBook.getDeclaredMethod("passInterview", Context.class, String.class);
            methodBook.setAccessible(true);
            Object objectBook = classBook.newInstance();
            methodBook.invoke(objectBook, ctx, getPlanId(ctx, personName, personNumber, interviewSegment));
        } catch (Exception e) {
            throw new ShrWebBizException("录用报批失败,请查看该人员是否已经进行录用报批或联系管理员！", e);
        }
    }

    /**
     * <p>
     * Title: updatePlanResult
     * </p>
     * <p>
     * Description: 同步面试安排环节结果数据
     * </p>
     * 
     * @param ctx
     * @param planId 面试安排内码
     * @param isParss 操作的类型
     * @param score 分数
     * @return
     * @throws ShrWebBizException
     */

    private boolean updatePlanResult(Context ctx, String planId, String isParss, String score)
            throws ShrWebBizException {
        Connection conn = null;

        String sql = "update T_REC_InterviewPlanResult set fresult='" + isParss + "',finterviewscore='"
                + score + "',fhrwriteresult=1 where finterviewplanid='" + planId + "'";
        String planIsParss = getStatusEnum(isParss);
        if (StringUtils.isEmpty(planIsParss)) {
            return false;
        }
        String planSql = "UPDATE T_REC_InterviewPlan SET  finvitestatus=" + planIsParss + "  WHERE  fid='"
                + planId + "'";
        try {
            conn = EJBFactory.getConnection(ctx);
            int number = DBUtil.executeUpdate(planSql, conn);
            int sqlnumber = DBUtil.executeUpdate(sql, conn);
            if (number > 0 && sqlnumber > 0) {
                return true;
            }

        } catch (SQLException e) {
            throw new ShrWebBizException("获取数据库连接信息失败！", e);
        }
        return false;
    }

    /**
     * 
     * <p>
     * Title: getStatusEnum
     * </p>
     * <p>
     * Description: 获取邀约状态枚举值
     * </p>
     * 
     * @param isParss 面试环节状态枚举值
     * @return 邀约状态枚举值
     */
    private String getStatusEnum(String isParss) {
        String enumValue = null;
        if ("2".equals(isParss)) {
            enumValue = InterviewInviteStatusEnum.WAIT_VALUE;
        }
        if ("3".equals(isParss)) {
            enumValue = InterviewInviteStatusEnum.PASS_VALUE;
        }
        if ("4".equals(isParss)) {
            enumValue = InterviewInviteStatusEnum.REFUSE_VALUE;
        }
        if ("5".equals(isParss)) {
            enumValue = InterviewInviteStatusEnum.QUIT_VALUE;
        }
        if ("6".equals(isParss)) {
            enumValue = InterviewInviteStatusEnum.END_VALUE;
        }
        return enumValue;
    }

    /**
     * 
     * <p>
     * Title: getPlanId
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param ctx
     * @param personName 候选人名称
     * @param personNumber 候选人编码
     * @param interviewSegment 面试环节
     * @return 面试安排内码
     * @throws ShrWebBizException
     */
    private String getPlanId(Context ctx, String personName, String personNumber, String interviewSegment)
            throws ShrWebBizException {
        String planId = null;
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT FID FROM  T_REC_InterviewPlan where finterviewstagename='" + interviewSegment + "'")
                .append(" and finterviewid=(SELECT fid FROM T_REC_InterviewResult where fresumebaserecid= ")
                .append(" (SELECT fid FROM T_REC_ResumeBaseRec where fname='" + personName + "' and fnumber='"
                        + personNumber + "')) ");
        System.out.println("*****************获取PLANID为" + sb.toString() + "***************");
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sb.toString());
            while (rowSet.next()) {
                planId = rowSet.getString("FID");
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("获取面试结果PlanId！失败", e);
        } catch (SQLException e) {
            throw new ShrWebBizException("获取面试结果PlanId！失败", e);
        }
        return planId;
    }
}
