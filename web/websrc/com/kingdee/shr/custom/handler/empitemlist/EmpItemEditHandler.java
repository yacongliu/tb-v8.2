package com.kingdee.shr.custom.handler.empitemlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.workflow.WfException;
import com.kingdee.bos.workflow.service.EnactmentServiceProxy;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.empitemlist.EmpItemListFactory;
import com.kingdee.eas.custom.empitemlist.EmpItemListInfo;
import com.kingdee.eas.custom.empitemlist.IEmpItemList;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.affair.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.MSFServiceFacadeFactory;
import com.kingdee.shr.base.syssetting.app.filter.HRFilterUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.DateTimeUtils;
import com.kingdee.util.StringUtils;

/**
 * 
 * Title: EmpItemEditHandler
 * Description: 部门考核指标清单新增handler
 * @author saisai_cheng Email:854296216@qq.com
 * @date 2019-8-23
 */
public class EmpItemEditHandler extends EditHandler {
	private static Logger logger = Logger
			.getLogger("com.kingdee.shr.custom.handler.empitemlist.EmpItemEditHandler");

	private Context ctx;

	public EmpItemEditHandler(Context ctx) {
		this.ctx = ctx;
	}

	public EmpItemEditHandler() {
		this.ctx = SHRContext.getInstance().getContext();
	}
	
	/**
	 * （非 Javadoc）
	 * <p>Title: afterCreateNewModel</p>
	 * <p>Description: 自动带出当前登录人、日期、部门</p>
	 * @param request
	 * @param response
	 * @param coreBaseInfo
	 * @throws SHRWebException
	 * @see com.kingdee.shr.base.syssetting.web.handler.EditHandler#afterCreateNewModel(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.kingdee.eas.framework.CoreBaseInfo)
	 */
	protected void afterCreateNewModel(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo coreBaseInfo)
			throws SHRWebException {
		super.afterCreateNewModel(request, response, coreBaseInfo);

		Date nowDate = DateTimeUtils.truncateDate(new Date());
		UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(this.ctx);
		EmpItemListInfo info = (EmpItemListInfo) coreBaseInfo;
		BOSUuid userId = currentUserInfo.getPerson().getId();
		PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());
		info.setCreator(currentUserInfo);
		info.setBizDate(nowDate);
		info.setAdminOrg(personPositionInfo.getPersonDep());

	}
	@Override
	protected void beforeSave(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		super.beforeSave(request, response, model);
		EmpItemListInfo bill = (EmpItemListInfo) model;
		bill.setBillState(HRBillStateEnum.SAVED);
	}
	
	public void canSubmitEffectAction(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap)
			throws SHRWebException {
		Map<String, String> map = new HashMap<String, String>();
		map.put("state", "success");
		JSONUtils.writeJson(response, map);
	}
	/**
	 * 
	 * <p>Title: submitEffectAction</p>
	 * <p>Description: 提交生效</p>
	 * @param req
	 * @param res
	 * @param modelMap
	 * @throws SHRWebException
	 */
	public void submitEffectAction(HttpServletRequest req,
			HttpServletResponse res, ModelMap modelMap) throws SHRWebException {

		CoreBaseInfo model = (CoreBaseInfo) req.getAttribute("dynamic_model");
		
		try {
			IEmpItemList iEmpItemList = EmpItemListFactory
					.getRemoteInstance();
			IObjectPK objectPK = iEmpItemList.submitEffect(model);
			
			model.setId(BOSUuid.read(objectPK.toString()));
			IRowSet rs = null;
			StringBuffer sb = new StringBuffer();
			sb.append("select CFExamProject,CFItemNum,FItemlistType,FDescription from CT_EMP_EmpItemListEntry where fbillid = '")
			.append(objectPK.toString()+"'");
			//List<PerformTargetItemInfo> list = new ArrayList<PerformTargetItemInfo>();
			rs = DbUtil.executeQuery(ctx, sb.toString());
			String name = null;
			String number = null;
			String typeid = null;
			String EvaluationCriterion = null;
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			while (rs.next()){
				Map<String, Object> param = new HashMap<String, Object>();
				name = rs.getString("CFExamProject");
				number = rs.getString("CFItemNum");
				typeid = rs.getString("FItemlistType");
				EvaluationCriterion = rs.getString("FDescription");
				Boolean Status =true;
				UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(this.ctx);
				BOSUuid userId = currentUserInfo.getPerson().getId();
				PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());
				String adminOrgUnit = personPositionInfo.getPersonDep().getId().toString();
				String IndexDescribe = null;

				param.put("Name", name);
				param.put("Number", number);
				param.put("TypeID", typeid);
				param.put("EvaluationCriterion", EvaluationCriterion);
				param.put("isStatus", Status);
				param.put("adminid", adminOrgUnit);
				param.put("IndexDescribe", IndexDescribe);
				list.add(param);
			}
			for(Map<String, Object> m :list){
				MSFServiceFacadeFactory.getLocalInstance(ctx)
			       .processService("itemService", m);
			}
			
		} catch (EASBizException e) {
			logger.error(e.getMessage(), e);
			throw new ShrWebBizException(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ShrWebBizException(e.getMessage());
		}

		System.out.println("部门考核指标清单id：" + model.getId().toString());

		writeSuccessData(model.getId().toString());
		
		
		
	}
	
	
	
	/**
	 * Title: beforeSubmit
	 * Description: 提交工作流
	 */
	@Override
	protected void beforeSubmit(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		super.beforeSubmit(request, response, model);

		EmpItemListInfo bill = (EmpItemListInfo) model;

		String userID = HRFilterUtils.getCurrentUserId(ctx);
		String functionName = "com.kingdee.eas.custom.deptseasonworklist.app.EmpItemListEditUIFunction";
		String operationName = "actionSubmit";
		try {
			String temp = EnactmentServiceProxy.getEnacementService(ctx)
					.findSubmitProcDef(userID, bill, functionName,
							operationName);
			if ((temp == null) || (temp.trim().equals("")))
				throw new ShrWebBizException("没有可用的部门考核指标清单工作流");
		} catch (WfException e) {
			throw new SHRWebException(e.getMessage());
		} catch (BOSException e) {
			throw new SHRWebException(e.getMessage());
		}

		bill.setBillState(HRBillStateEnum.SUBMITED);
		String operateStatus = request.getParameter("operateState");
		if ((StringUtils.isEmpty(operateStatus))
				|| (!("ADDNEW".equalsIgnoreCase(operateStatus)))) {
			return;
		}
		bill.setExtendedProperty("isAddNew", "isAddNew");
	}
}
