package com.kingdee.shr.custom.handler.empitemlist;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.workflow.WfException;
import com.kingdee.bos.workflow.service.EnactmentServiceProxy;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.empitemlist.EmpSafeguardListFactory;
import com.kingdee.eas.custom.empitemlist.EmpSafeguardListInfo;
import com.kingdee.eas.custom.empitemlist.IEmpSafeguardList;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.affair.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.MSFServiceFacadeFactory;
import com.kingdee.shr.base.syssetting.app.filter.HRFilterUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.DateTimeUtils;
import com.kingdee.util.StringUtils;

/**
 * 
 * Title: EmpSafeguardEditHandler 
 * Description: 部门考核指标清单维护handler
 * 
 * @author saisai_cheng Email:854296216@qq.com
 * @date 2019-8-23
 */
public class EmpSafeguardEditHandler extends EditHandler {

	static String EvaluationCriterion = null;// 评价标准
	// static String IndexDescribe = null;//指标描述
	static Boolean Status = null;// 是否启用
	static String number = null;// 指标编号
	static String id = null;

	private static Logger logger = Logger
			.getLogger("com.kingdee.shr.custom.handler.empitemlist.EmpSafeguardEditHandler");

	private Context ctx;

	public EmpSafeguardEditHandler(Context ctx) {
		this.ctx = ctx;
	}

	public EmpSafeguardEditHandler() {
		this.ctx = SHRContext.getInstance().getContext();
	}

	/**
	 * （非 Javadoc）
	 * <p>
	 * Title: afterCreateNewModel
	 * </p>
	 * <p>
	 * Description: 自动带出当前登录人、日期、部门
	 * </p>
	 * 
	 * @param request
	 * @param response
	 * @param coreBaseInfo
	 * @throws SHRWebException
	 * @see com.kingdee.shr.base.syssetting.web.handler.EditHandler#afterCreateNewModel(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse,
	 *      com.kingdee.eas.framework.CoreBaseInfo)
	 */
	protected void afterCreateNewModel(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo coreBaseInfo)
			throws SHRWebException {
		super.afterCreateNewModel(request, response, coreBaseInfo);

		Date nowDate = DateTimeUtils.truncateDate(new Date());
		UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(this.ctx);
		EmpSafeguardListInfo info = (EmpSafeguardListInfo) coreBaseInfo;
		BOSUuid userId = currentUserInfo.getPerson().getId();
		PersonPositionInfo personPositionInfo = SHRBillUtil
				.getAdminOrgUnit(userId.toString());
		info.setCreator(currentUserInfo);
		info.setBizDate(nowDate);
		info.setAdminOrg(personPositionInfo.getPersonDep());

	}

	@Override
	protected void beforeSave(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		super.beforeSave(request, response, model);
		EmpSafeguardListInfo bill = (EmpSafeguardListInfo) model;
		bill.setBillState(HRBillStateEnum.SAVED);
	}

	/**
	 * 
	 * <p>
	 * Title: canSubmitEffectAction
	 * </p>
	 * <p>
	 * Description: 提交生效弹框
	 * </p>
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @throws SHRWebException
	 */
	public void canSubmitEffectAction(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap)
			throws SHRWebException {
		Map<String, String> map = new HashMap<String, String>();
		map.put("state", "success");
		JSONUtils.writeJson(response, map);
	}

	/**
	 * 
	 * <p>
	 * Title: getStandardAction
	 * </p>
	 * <p>
	 * Description: 根据考核项目获取评价标准
	 * </p>
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @throws SHRWebException
	 */
	public void getStandardAction(HttpServletRequest request,
			HttpServletResponse response, ModelMap model)
			throws SHRWebException {

		String id = request.getParameter("id");// 考核项目id
		String standard = null;
		int isPass = 0;
		Map<String, Object> standardMap = new HashMap<String, Object>();
		IRowSet rowSet = null;
		StringBuffer sql = new StringBuffer();
		sql
				.append(
						"SELECT CFEvaluationCriterion as standard,CFStatus as status FROM CT_MAN_AssessmentIndexs ")

				.append(" where fid=").append("'" + id + "'");

		System.out.println("============根据考核项目带出评价标准和状态的sql:" + sql.toString()
				+ "===========");
		try {
			rowSet = DbUtil.executeQuery(ctx, sql.toString());
			while (rowSet.next()) {
				standard = rowSet.getString("standard");
				if (!StringUtils.isEmpty(standard)) {
					standardMap.put("standard", standard);
				}
				isPass = rowSet.getInt("status");
				standardMap.put("isPass", isPass);
			}
		} catch (BOSException e) {
			logger.error(e.getMessage(), e);
			throw new SHRWebException("获取评价标准失败!", e);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new SHRWebException("获取评价标准失败!", e);
		}
		JSONUtils.SUCCESS(standardMap);
	}

	/**
	 * 
	 * <p>
	 * Title: submitEffectAction
	 * </p>
	 * <p>
	 * Description: 提交生效
	 * </p>
	 * 
	 * @param req
	 * @param res
	 * @param modelMap
	 * @throws SHRWebException
	 */
	public void submitEffectAction(HttpServletRequest req,
			HttpServletResponse res, ModelMap modelMap) throws SHRWebException {

		CoreBaseInfo model = (CoreBaseInfo) req.getAttribute("dynamic_model");
		try {
			IEmpSafeguardList iEmpSafeguardList = EmpSafeguardListFactory
					.getRemoteInstance();
			IObjectPK objectPK = iEmpSafeguardList.submitEffect(model);
			model.setId(BOSUuid.read(objectPK.toString()));
			System.out.println("部门考核指标清单id：" + model.getId().toString());

			IRowSet rs = null;
			StringBuffer sb = new StringBuffer();
			sb.append("select e.fid as fid,e.cfstate as cfstate,e.cfevalStandard as cfevalStandard,a.FNumber as fnumber from CT_EMP_EmpSafeguardListEntry e left join  CT_MAN_AssessmentIndexs a on a.fid=e.cfexamProjectsid where e.fbillid = '")
			.append(objectPK.toString() + "'");
			rs = DbUtil.executeQuery(ctx, sb.toString());
			int Status = 0;
			/*
			 * String Number = null; String id = null; String
			 * EvaluationCriterion = null;
			 */
			
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			while (rs.next()) {
				Map<String, Object> param = new HashMap<String, Object>();
				EvaluationCriterion = rs.getString("cfevalStandard");// 评价标准
				Status = rs.getInt("cfstate");// 是否启用
				id = rs.getString("fid");// id
				number = rs.getString("fnumber");// 编码
				param.put("EvaluationCriterion", EvaluationCriterion);
				param.put("isStatus", (Status == 1 ? true : false));
				param.put("Number", number);
				list.add(param);
				System.out.println("========查询出编码、评价标准、状态的sql语句======" + sb);
				System.out.println("========查询出Number======" + number);
			}
			for(Map<String, Object> m :list){
				MSFServiceFacadeFactory.getLocalInstance(ctx).processService(
						"updateItemService", m);
			}
			
		} catch (EASBizException e) {
			throw new SHRWebException(e.getMessage());
		} catch (Exception e) {
			throw new ShrWebBizException(e.getMessage());
		}
		writeSuccessData(model.getId().toString());
	}

	/**
	 * Title: beforeSubmit Description: 提交工作流
	 */
	@Override
	protected void beforeSubmit(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		super.beforeSubmit(request, response, model);

		EmpSafeguardListInfo bill = (EmpSafeguardListInfo) model;

		String userID = HRFilterUtils.getCurrentUserId(ctx);
		String functionName = "com.kingdee.eas.custom.empitemlist.app.EmpSafeguardListEditUIFunction";
		String operationName = "actionSubmit";
		try {
			String temp = EnactmentServiceProxy.getEnacementService(ctx)
					.findSubmitProcDef(userID, bill, functionName,
							operationName);
			if ((temp == null) || (temp.trim().equals("")))
				throw new ShrWebBizException("没有可用的部门考核指标清单工作流");
		} catch (WfException e) {
			throw new SHRWebException(e.getMessage());
		} catch (BOSException e) {
			throw new SHRWebException(e.getMessage());
		}

		bill.setBillState(HRBillStateEnum.SUBMITED);
		String operateStatus = request.getParameter("operateState");
		if ((StringUtils.isEmpty(operateStatus))
				|| (!("ADDNEW".equalsIgnoreCase(operateStatus)))) {
			return;
		}
		bill.setExtendedProperty("isAddNew", "isAddNew");
	}
}
