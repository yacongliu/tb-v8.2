package com.kingdee.shr.custom.handler.item;



import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.enterprisedt.util.debug.Logger;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.human.AssessmentIndexsInfo;
import com.kingdee.eas.fi.gl.util.StringUitls;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.hr.perf.KindEnum;
import com.kingdee.eas.hr.perf.PerformTargetItemFactory;
import com.kingdee.eas.hr.perf.PerformTargetItemInfo;
import com.kingdee.eas.hr.perf.PerformTargetTypeFactory;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.ats.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.EditHandler;
import com.kingdee.util.DateTimeUtils;
/**
 * 
 * Title: AssessmentIndexFormHandlerex
 * Description: 考核指标库列表handler 
 * @author 宋佩骏 Email:tjsongpeijun@kingdee.com
 * @date 2019-8-15
 */
public class AssessmentIndexFormHandlerex extends EditHandler {
	public static Logger logger = Logger.getLogger("com.kingdee.shr.custom.handler.item.AssessmentIndexFormHandlerex");
	
	private static Context ctx;
	
	public AssessmentIndexFormHandlerex(){
		AssessmentIndexFormHandlerex.ctx = SHRContext.getInstance().getContext();

	}
	public AssessmentIndexFormHandlerex(Context ctx){
		AssessmentIndexFormHandlerex.ctx=ctx;
	}
	/**
	 * 
	 * <p>Title: afterSave</p>
	 * <p>Description:在数据更新时将数据同步保存到客户端中 </p>
	 * @param request
	 * @param response
	 * @param model
	 * @throws SHRWebException
	 */
	@Override
	protected void afterSave(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo model)
			throws SHRWebException {
		
		super.afterSave(request, response, model);
		AssessmentIndexsInfo info = (AssessmentIndexsInfo) model;
		try {
			//获取客户端考核指标库对象
			PerformTargetItemInfo performTargetItemInfo = savePerformTargetItemInfo(info);
			String performTargetItemId = getCount(performTargetItemInfo.getNumber());
				if(StringUitls.stringIsNull(performTargetItemId)){
					String typeid = getType(info.getId().toString());
					performTargetItemInfo.setType(PerformTargetTypeFactory.getRemoteInstance().getPerformTargetTypeInfo(new ObjectUuidPK(typeid)));
					PerformTargetItemFactory.getRemoteInstance().save(performTargetItemInfo);
				}else{
					PerformTargetItemInfo updatePerformTargetItemInfo = saveUpdatePerformTargetItemInfo(info);
					String typeid = getType(info.getId().toString());
					updatePerformTargetItemInfo.setType(PerformTargetTypeFactory.getRemoteInstance().getPerformTargetTypeInfo(new ObjectUuidPK(typeid)));
					PerformTargetItemFactory.getRemoteInstance().update(new ObjectUuidPK(performTargetItemId), updatePerformTargetItemInfo);
				}
		} catch (EASBizException e) {
			throw new SHRWebException("更新考核指标库信息失败!", e);
		} catch (BOSException e) {
			throw new SHRWebException("更新考核指标库信息失败!", e);
		} catch (SQLException e) {
			throw new SHRWebException("更新考核指标库信息失败!", e);
		}
		
	}
	/**
	 * 
	 * <p>Title: savePerformTargetItemInfo</p>
	 * <p>Description: 组装客户端考核指标对象</p>
	 * @param info 网页端考核指标库对象
	 * @return
	 * @throws ShrWebBizException
	 * @throws SQLException 
	 * @throws BOSException 
	 * @throws EASBizException 
	 */
	private static PerformTargetItemInfo savePerformTargetItemInfo(AssessmentIndexsInfo info) throws  ShrWebBizException, BOSException, SQLException, EASBizException{
		
		String EvaluationCriterion = info.getEvaluationCriterion();//评价标准
		String IndexDescribe = info.getIndexDescribe();//指标描述
		Boolean Status =info.isStatus();//是否启用
		String number = info.getNumber();//指标编号
		String name = info.getName();
		if(StringUitls.stringIsNull(EvaluationCriterion)){
			throw new ShrWebBizException("评价标准不能为空!");
		}
		if(StringUitls.stringIsNull(name)){
			throw new ShrWebBizException("指标名称不能为空!");
		}
		
		PerformTargetItemInfo performTargetItemInfo =  new PerformTargetItemInfo();
		performTargetItemInfo.setStandard(EvaluationCriterion);//评价标准
		performTargetItemInfo.setDescription(IndexDescribe);//指标描述
		performTargetItemInfo.setEnable(Status);//是否启用
		performTargetItemInfo.setContent(name);//考核名称
		performTargetItemInfo.setNumber(number);
		performTargetItemInfo.setKind(KindEnum.QUALITATIVE);
		
		return performTargetItemInfo;
	}
	
	/**
	 * 
	 * <p>Title: saveUpdatePerformTargetItemInfo</p>
	 * <p>Description:  网页端考核指标库对象< /p>
	 * @param info 网页端考核指标库对象
	 * @return
	 * @throws ShrWebBizException
	 * @throws BOSException
	 * @throws SQLException
	 * @throws EASBizException
	 */
	private static PerformTargetItemInfo saveUpdatePerformTargetItemInfo(AssessmentIndexsInfo info) throws  ShrWebBizException, BOSException, SQLException, EASBizException{
		
		String EvaluationCriterion = info.getEvaluationCriterion();// 评价标准
		String IndexDescribe = info.getIndexDescribe();// 指标描述
		Boolean Status =info.isStatus();// 是否启用
		String name = info.getName();
		if(StringUitls.stringIsNull(EvaluationCriterion)){
			throw new ShrWebBizException("评价标准不能为空!");
		}
		if(StringUitls.stringIsNull(name)){
			throw new ShrWebBizException("指标名称不能为空!");
		}
		
		PerformTargetItemInfo performTargetItemInfo =  new PerformTargetItemInfo();
		performTargetItemInfo.setStandard(EvaluationCriterion);//评价标准
		performTargetItemInfo.setDescription(IndexDescribe);//指标描述
		performTargetItemInfo.setEnable(Status);//是否启用
		performTargetItemInfo.setContent(name);//考核名称
		performTargetItemInfo.setKind(KindEnum.QUALITATIVE);
		
		return performTargetItemInfo;
	}
	
	/**
	 * 
	 * <p>Title: getCount</p>
	 * <p>Description:获取出该编码的结果数量</p>
	 * @param number 编码
	 * @return
	 * @throws BOSException
	 * @throws SQLException
	 */
	private static String getCount(String number) throws BOSException, SQLException{
		
		StringBuffer selectsql = new StringBuffer();
		selectsql.append("select fid id from T_PF_PERFITEM where fnumber = '").append(number).append("'");
		IRowSet row = DbUtil.executeQuery(ctx, selectsql.toString());
		while(row.next()){
			return row.getString("id");
		}
		
		return null;
	}
	
	/**
	 * 
	 * <p>Title: getType</p>
	 * <p>Description: 获取考核标准</p>
	 * @param id 页面新生成的考核指标库id
	 * @return
	 * @throws SQLException 
	 * @throws BOSException 
	 */
	private static String getType(String id) throws SQLException, BOSException{
		
		String typeid = null;
		String sql = "select FTargetTypeID typeid  from CT_MAN_AssessmentIndexs where fid = '"+id+"'";
		IRowSet rows = DbUtil.executeQuery(ctx, sql);
		while(rows.next()){
			typeid = rows.getString("typeid");
		}
		
		return typeid;
	}
	
	@Override
	protected void afterCreateNewModel(HttpServletRequest request,
			HttpServletResponse response, CoreBaseInfo coreBaseInfo)
			throws SHRWebException {
		/**
		 * 在页面加载是自动填充用户，时间和所属组织
		 */
		super.afterCreateNewModel(request, response, coreBaseInfo);
		AssessmentIndexsInfo info = (AssessmentIndexsInfo) coreBaseInfo;
		UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(AssessmentIndexFormHandlerex.ctx);
		//BOSUuid userId = currentUserInfo.getPerson().getId();
		//AdminOrgUnitInfo orgInfo = ContextUtil.getCurrentAdminUnit(AssessmentIndexFormHandlerex.ctx); 
		Date nowDate = DateTimeUtils.truncateDate(new Date());
		Timestamp time =new Timestamp(nowDate.getTime());
		info.setCreator(currentUserInfo);// 当前登录人姓名
		info.setCreateTime(time);// 创建时间
		//info.setSubordinate(orgInfo);// 当前登录人所属组织
		BOSUuid userId = currentUserInfo.getPerson().getId();
		PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());
		info.setSubordinate(personPositionInfo.getPersonDep());// 当前登录人所属组织

	}
}
