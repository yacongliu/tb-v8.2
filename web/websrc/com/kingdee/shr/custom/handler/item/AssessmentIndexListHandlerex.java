package com.kingdee.shr.custom.handler.item;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.enterprisedt.util.debug.Logger;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.human.AssessmentIndexsFactory;
import com.kingdee.eas.custom.human.AssessmentIndexsInfo;
import com.kingdee.eas.fi.gl.util.StringUitls;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.ats.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;
/**
 * 
 * Title: AssessmentIndexListHandlerex
 * Description:考核指标库列表handler 
 * @author 宋佩骏 Email:tjsongpeijun@kingdee.com
 * @date 2019-8-15
 */
public class AssessmentIndexListHandlerex extends ListHandler {
public static Logger logger = Logger.getLogger("com.kingdee.shr.custom.handler.item.AssessmentIndexListHandlerex");
	
	private static Context ctx;
	
	public AssessmentIndexListHandlerex(){
		AssessmentIndexListHandlerex.ctx = SHRContext.getInstance().getContext();

	}
	public AssessmentIndexListHandlerex(Context ctx){
		AssessmentIndexListHandlerex.ctx=ctx;
	}
	
	@Override
	protected FilterInfo getDomainFilter(HttpServletRequest request)
	   throws SHRWebException {
		/**
		 * 在页面加载是通过组织机构过滤
		 */
	  super.getDomainFilter(request);
	  FilterInfo filterInfo = new FilterInfo();
	  UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(AssessmentIndexListHandlerex.ctx);
	  BOSUuid userId = currentUserInfo.getPerson().getId();
	  PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());
	  //AdminOrgUnitInfo orgInfo = ContextUtil.getCurrentAdminUnit(AssessmentIndexListHandlerex.ctx); 
	  if(personPositionInfo.getPersonDep().getId().toString().equals("5tQAAAAAFDXM567U")){
		  return filterInfo;
	  }
	  filterInfo.getFilterItems().add(new FilterItemInfo("Subordinate", personPositionInfo.getPersonDep().getId().toString(), CompareType.EQUALS));
	  
	  return filterInfo;
	 }
	
	@Override
	public String deleteAction(HttpServletRequest arg0,
			HttpServletResponse arg1, ModelMap arg2) throws SHRWebException {
		/**
		 * 在页面删除时同时删除gui端考核指标库信息
		 */
		String billId = getBillId(arg0);
		String[] ids = null;
		ids = billId.split(",");
		if(ids.length > 1){
			throw new ShrWebBizException("不允许批量删除");
		}
		String sql = "select fnumber number from CT_MAN_AssessmentIndexs where fid = '"+billId+"'";
		try {
			IRowSet row =  DbUtil.executeQuery(ctx, sql);
			String number = null;
			while(row.next()){
				number = row.getString("number");
			}
			String deletesql = "delete  from T_PF_PERFITEM where fnumber = '"+number+"'";
			DbUtil.execute(ctx, deletesql);
		} catch (BOSException e) {
			throw new SHRWebException("删除考核指标库信息失败", e);
		} catch (SQLException e) {
			throw new SHRWebException("删除考核指标库信息失败", e);
		}
		return super.deleteAction(arg0, arg1, arg2);
		
	}
	
	@Override
		public String initalizeAction(HttpServletRequest request,
				HttpServletResponse response, ModelMap modelMap)
				throws SHRWebException {
		/**
		 * 在页面加载时保存所属组织架构的客户端页面维护的考核指标库信息
		 */
		super.initalizeAction(request, response, modelMap);
		UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(AssessmentIndexListHandlerex.ctx);
		BOSUuid userId = currentUserInfo.getPerson().getId();
		PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());
		//AdminOrgUnitInfo info = ContextUtil.getCurrentAdminUnit(ctx);
		String organizationInfoId = personPositionInfo.getPersonDep().getId().toString();
		IRowSet indexrow;
		String type = null;
		try {
			//获取查询对象
			indexrow = getRow(organizationInfoId);
			//获取考核指标库集合
			List<AssessmentIndexsInfo> assessmentIndexsInfoList = new ArrayList<AssessmentIndexsInfo>();
			while(indexrow.next()){
				AssessmentIndexsInfo assessmentIndexsInfo = new AssessmentIndexsInfo();
				assessmentIndexsInfo.setNumber(indexrow.getString("fnumber"));
				assessmentIndexsInfo.setName(indexrow.getString("fcontent2"));
				assessmentIndexsInfo.setEvaluationCriterion(indexrow.getString("fstandard2"));
				assessmentIndexsInfo.setIndexDescribe(indexrow.getString("fdescription"));
				if(indexrow.getInt("fenable")==0){
					assessmentIndexsInfo.setStatus(false);
				}else{
					assessmentIndexsInfo.setStatus(true);
				}
				AdminOrgUnitInfo adminOrgUnitInfo=new AdminOrgUnitInfo();
				if(!com.kingdee.util.StringUtils.isEmpty(indexrow.getString("ss"))) {
					adminOrgUnitInfo.setId(BOSUuid.read(indexrow.getString("ss")));
				}
				assessmentIndexsInfo.setSubordinate(adminOrgUnitInfo);
				assessmentIndexsInfoList.add(assessmentIndexsInfo);
				type=indexrow.getString("qtype");
				if(StringUitls.stringIsNull(type)){
					throw new ShrWebBizException("没有获取到考核标准，请通知管理员维护");
				}
				String delesql = null;
				if(organizationInfoId.equals("5tQAAAAAFDXM567U")) {
					delesql = "delete FROM CT_MAN_AssessmentIndexs ";
				}else {
					delesql = "delete FROM CT_MAN_AssessmentIndexs where cfsubordinateid = '" + organizationInfoId + "'";
				}
				
				DbUtil.execute(AssessmentIndexListHandlerex.ctx, delesql);
			}
			for(AssessmentIndexsInfo a : assessmentIndexsInfoList){
			if(a.getSubordinate()==null || "".equals(a.getSubordinate())){
				throw new ShrWebBizException("存在没有组织机构的数据，请通知管理员");
			}else{
				IObjectPK objectPK= AssessmentIndexsFactory.getRemoteInstance().save(a);
				String updatesql = "update CT_MAN_AssessmentIndexs set FTargetTypeID = '"+type+"' where fid= '"+objectPK.toString()+"'";
				DbUtil.execute(ctx, updatesql);
			}
				}
		} catch (BOSException e) {
			throw new SHRWebException("保存考核指标库信息失败", e);
		} catch (SQLException e) {
			throw new SHRWebException("保存考核指标库信息失败", e);
		} catch (EASBizException e) {
			throw new SHRWebException("保存考核指标库信息失败", e);
		}
		
			return super.initalizeAction(request, response, modelMap);
	}
	/**
	 * 
	 * <p>Title: getRow</p>
	 * <p>Description: 获取客户端考核指标库查询结果 </p>
	 * @param organizationId 登录人的所属组织id
	 * @return
	 * @throws BOSException
	 */
	private static IRowSet getRow(String organizationId) throws BOSException{
		StringBuffer indexsql = new StringBuffer();
		if(organizationId.equals("5tQAAAAAFDXM567U")) {
			indexsql.append("SELECT pi.fnumber fnumber,pi.fcontent2 fcontent2,pi.fstandard2 fstandard2,pi.fdescription fdescription,pi.fenable fenable,ptt.CFADMINORGUNITID ss,pi.ftype qtype,pi.FCONTROLUNITID  Subordinate FROM T_PF_PERFITEM pi left join T_PF_PerformTargetType ptt on  pi.ftype=ptt.fid ");
		}else {
			indexsql.append("SELECT pi.fnumber fnumber,pi.fcontent2 fcontent2,pi.fstandard2 fstandard2,pi.fdescription fdescription,pi.fenable fenable,ptt.CFADMINORGUNITID ss,pi.ftype qtype,pi.FCONTROLUNITID  Subordinate FROM T_PF_PERFITEM pi left join T_PF_PerformTargetType ptt on  pi.ftype=ptt.fid where CFADMINORGUNITID = '").append(organizationId).append("'");
		}
		IRowSet indexrow = DbUtil.executeQuery(ctx,indexsql.toString());
		return indexrow;
	}
}

