package com.kingdee.shr.custom.handler.assesstarget;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.ctrl.common.util.StringUtil;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.query.IQueryExecutor;
import com.kingdee.bos.dao.query.QueryExecutorFactory;
import com.kingdee.bos.metadata.MetaDataPK;
import com.kingdee.bos.metadata.data.SortType;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.metadata.entity.SorterItemCollection;
import com.kingdee.bos.metadata.entity.SorterItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.dev.helper.SHRHelper;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.hr.perf.BatchEvaluProcessHelper;
import com.kingdee.eas.hr.perf.EvalWFObjectsCollection;
import com.kingdee.eas.hr.perf.EvalWFObjectsFactory;
import com.kingdee.eas.hr.perf.EvalWorkFlowFacadeFactory;
import com.kingdee.eas.hr.perf.EvaluDetailCollection;
import com.kingdee.eas.hr.perf.EvaluDetailInfo;
import com.kingdee.eas.hr.perf.EvaluFormSolutionEntryCollection;
import com.kingdee.eas.hr.perf.EvaluFormSolutionEntryInfo;
import com.kingdee.eas.hr.perf.EvaluFormSolutionFacadeFactory;
import com.kingdee.eas.hr.perf.EvaluFormSolutionMultiTableCollection;
import com.kingdee.eas.hr.perf.EvaluFormSolutionMultiTableInfo;
import com.kingdee.eas.hr.perf.EvaluNodeInfo;
import com.kingdee.eas.hr.perf.EvaluResultFactory;
import com.kingdee.eas.hr.perf.IEvaluResult;
import com.kingdee.eas.hr.perf.INodeProcesser;
import com.kingdee.eas.hr.perf.ITargetEvaluStore;
import com.kingdee.eas.hr.perf.KindEnum;
import com.kingdee.eas.hr.perf.NodeProcesserFactory;
import com.kingdee.eas.hr.perf.NodeProcesserInfo;
import com.kingdee.eas.hr.perf.NodeVisibleCollection;
import com.kingdee.eas.hr.perf.NodeVisibleEnum;
import com.kingdee.eas.hr.perf.NodeVisibleInfo;
import com.kingdee.eas.hr.perf.PrecisionTypeEnum;
import com.kingdee.eas.hr.perf.ProcesserWorkedStateEnum;
import com.kingdee.eas.hr.perf.RankInfo;
import com.kingdee.eas.hr.perf.ReportFrequencyEnum;
import com.kingdee.eas.hr.perf.SolutionPeriodInfo;
import com.kingdee.eas.hr.perf.TargetEvaluStoreCollection;
import com.kingdee.eas.hr.perf.TargetEvaluStoreFactory;
import com.kingdee.eas.hr.perf.TargetEvaluStoreInfo;
import com.kingdee.eas.hr.perf.TargetScoreComputeFacadeFactory;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.affair.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.annotation.IBOSBizCtrl;
import com.kingdee.shr.base.syssetting.app.builder.CommonTreeBuilder;
import com.kingdee.shr.base.syssetting.app.builder.ITreeBuilder;
import com.kingdee.shr.base.syssetting.app.builder.OrgUnitTreeBuilder;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.dynamic.model.ListUIViewInfo;
import com.kingdee.shr.base.syssetting.web.dynamic.model.TreeNavigationInfo;
import com.kingdee.shr.base.syssetting.web.dynamic.util.DynamicUtil;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.perf.service.PersonalTargetEvaluStoreService;
import com.kingdee.shr.perf.util.AssessTargetData;
import com.kingdee.shr.perf.util.CommonUtil;
import com.kingdee.shr.perf.util.MultiTableData;
import com.kingdee.shr.perf.util.NodeCountData;
import com.kingdee.shr.perf.util.NodeParam;
import com.kingdee.shr.perf.util.NodeProcesserData;
import com.kingdee.shr.perf.util.NodeProcesserHelper;
import com.kingdee.shr.perf.util.TargetEvaluManageHelper;
import com.kingdee.shr.perf.web.handler.peopleSelf.AssessTargetEvaluEditHandler;
import com.kingdee.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AssessTargetEvaluEditHandlerExt extends AssessTargetEvaluEditHandler {
    private Context ctx = SHRContext.getInstance().getContext();

    protected PersonalTargetEvaluStoreService targetEvaluStoreBiz = new PersonalTargetEvaluStoreService();

    protected SolutionPeriodInfo solutionPeriodInfo = null;

    protected String objId;

    protected String solutionId;

    @IBOSBizCtrl
    protected ITargetEvaluStore service;

    protected String assignmentId;

    protected NodeParam nodeParam;

    protected String proInstId;

    protected Map evaluFormSolution;

    BigDecimal result_tmp = null;

    private Map datas = new HashMap();

    private Map datasMirror = new HashMap();

    private Map primaryPosition = new HashMap();

    private Map<Object, String> recordEvaluNodeAndColumn = new HashMap<Object, String>();

    private Map<Object, BigDecimal> recordNodeWeightAndNodeScore = new HashMap<Object, BigDecimal>();

    private Map<String, BigDecimal> evaluSumScore = new HashMap<String, BigDecimal>();

    private int currentNodeSeq;

    private List isNotComputeTable = new ArrayList();

    private NodeProcesserInfo info = null;

    private String[] seqNumbers;

    private static final String RANK_ROW = "rankRow";

    private static final String SUM_ROW = "sumRow";

    @IBOSBizCtrl
    protected INodeProcesser service_NodeProcesser;

    private String orgUnitID = null;

    private String assignMentId = null;

    private DecimalFormat numFormat = NodeProcesserHelper.numFormat;

    private String evaluPersonId;

    private Object comment;

    private Object rank;

    private Object totalScore;

    private Map rankToScore = new HashMap();

    private DecimalFormat format = new DecimalFormat("0.00");

    private String reBackReason;

    private static final String RES = "com.kingdee.eas.hr.perf.NodeProcesserWebResource";

    protected static final String perfRES = "com.kingdee.eas.hr.perf.PerfResource";

    public HashMap tablcalculateScore = new HashMap();

    private ArrayList tabHead = new ArrayList();

    private BigDecimal percent = new BigDecimal("0.010000");

    private boolean isShow = false;

    private Map nodeWeightMap = new TreeMap();

    private List<String> objetList = new ArrayList<String>();

    private void initParam() {
        try {
            this.initPaperParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.datas.clear();
        this.isNotComputeTable.clear();
        this.datasMirror.clear();
        this.recordEvaluNodeAndColumn.clear();
        this.recordNodeWeightAndNodeScore.clear();
        this.evaluSumScore.clear();
        this.currentNodeSeq = 0;
        try {
            NodeProcesserHelper.dealExistNode(this.ctx, this.nodeParam, this.datas, this.datasMirror,
                    this.primaryPosition);
        } catch (EASBizException e) {
            e.printStackTrace();
        } catch (BOSException e) {
            e.printStackTrace();
        }
        try {
            NodeProcesserHelper.dealCurrentNode(this.ctx, this.nodeParam, this.datas, this.datasMirror);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.nodeParam.getNodeId() != null && this.nodeParam.getNodeId().equalsIgnoreCase("last")) {
            String nodeSeq = String.valueOf(this.datas.size());
            this.nodeParam.setNodeId(nodeSeq);
        }
        if (this.datas.get(this.nodeParam.getNodeId()) != null) {
            this.nodeParam
                    .setCurrentNodeLevel(((NodeProcesserInfo) this.datas.get(this.nodeParam.getNodeId()))
                            .getEvaluNode().getNodeLevel());
        }
        if (this.nodeParam.getNodeId() != null) {
            this.info = (NodeProcesserInfo) this.datas.get(this.nodeParam.getNodeId());
        }
        try {
            this.initNodeProcesserInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initEvaluSumScore() {
        int size = this.datas.size();
        for (int i = 0; i < size; ++i) {
            NodeProcesserInfo ninfo = (NodeProcesserInfo) this.datas.get(this.seqNumbers[i]);
            if (ninfo.getNodeScore() == null || ninfo.getNodeScore().intValue() == 0)
                continue;
            this.evaluSumScore.put(this.seqNumbers[i],
                    ninfo.getNodeScore().multiply(ninfo.getEvaluNode().getWeight()).multiply(this.percent));
        }
    }

    private boolean checkValid(HashMap<String, Object> result) {
        EvaluDetailCollection coll = this.info.getEvaluDetails();
        for (int i = 0; i < coll.size(); i++) {
            EvaluDetailInfo detailInfo = coll.get(i);
            BigDecimal targetScore = detailInfo.getTargetScore();
            if (targetScore == null) {
                // 跳过分数校验： 隐藏的指标，强制赋值为0
                detailInfo.setTargetScore(new BigDecimal(0));
            }

        }
        return NodeProcesserHelper.checkValid(this.nodeParam, this.info, result);
    }

    private void initOrgUnitID() throws EASBizException, BOSException {
        int evaluObjectType = this.nodeParam.isOrg() ? 101 : 100;
        Map map = EvaluFormSolutionFacadeFactory.getLocalInstance(this.ctx).getHeadEntrys(
                this.nodeParam.getSolutionPeriodID(), this.nodeParam.getEvaluObjectID(), evaluObjectType);
        Map names = (Map) map.get("names");
        Map values = (Map) map.get("values");
        if (values == null) {
            values = Collections.EMPTY_MAP;
        }
        if (this.nodeParam.isOrg()) {
            this.evaluPersonId = null;
            this.orgUnitID = values.get("orgId").toString();
        } else {
            this.orgUnitID = null;
            this.evaluPersonId = values.get("personId").toString();
        }
        if (names == null || names.size() <= 0) {
            return;
        }
        this.objetList.clear();
        if (values.get("personName") != null) {
            this.objetList.add(values.get("personName") + "");
        } else {
            this.objetList.add(values.get("orgResponserName") + "");
        }
    }

    @Override
    public void submitTargetAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        HashMap<String, Object> resultMsg = new HashMap<String, Object>();
        StringBuffer sb = new StringBuffer();
        String datasFromPara = request.getParameter("datas");
        String count_datasFromPara = request.getParameter("count_datas");
        String commentDatas = request.getParameter("commentDatas");
        this.assignmentId = request.getParameter("assignmentId");
        this.initParam();
        this.realSaveTargetForm(datasFromPara, count_datasFromPara, commentDatas, resultMsg);
        this.initEvaluSumScore();
        if ("no".equals(resultMsg.get("opResult"))) {
            String msg = resultMsg.get("msg").toString();
            resultMsg.put("msg", msg);
            JSONUtils.writeJson(response, resultMsg);
            return;
        }
        if (this.checkGradeMode(resultMsg) && this.checkValid(resultMsg)) {
            try {
                this.initOrgUnitID();
            } catch (EASBizException e) {
                e.printStackTrace();
            } catch (BOSException e) {
                e.printStackTrace();
            }
            String orgName = null;
            if (this.nodeParam.isOrg()) {
                orgName = NodeProcesserHelper.getOrgName(this.ctx, this.orgUnitID);
            }
            BigDecimal result = new BigDecimal("0.00");
            BigDecimal score = null;
            for (Map.Entry<String, BigDecimal> map : this.evaluSumScore.entrySet()) {
                score = map.getValue();
                result = result.add(score);
            }
            if (this.nodeParam.isGrade()) {
                if (BatchEvaluProcessHelper.isLastNode(this.info.getEvaluNode(), null)) {
                    this.evaluSumScore.keySet();
                    if (this.nodeParam.isOrg()) {
                        if (orgName == null) {
                            sb.append(this.objetList.get(0) + " " + "的最终评估结果为:"
                                    + result.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                            RoundingMode.HALF_EVEN));
                        } else {
                            sb.append(orgName + " " + "的最终评估结果为:"
                                    + result.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                            RoundingMode.HALF_EVEN));
                        }
                    } else {
                        sb.append(this.objetList.get(0) + " " + "的最终评估结果为:"
                                + result.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                        RoundingMode.HALF_EVEN));
                    }
                    sb.append(",");
                } else if (this.info.getEvaluNode().isCanGrade()) {
                    if (this.info.getNodeScore() != null) {
                        sb.append("该节点对：");
                        sb.append(" ");
                        if (this.nodeParam.isOrg()) {
                            if (orgName == null) {
                                sb.append(this.objetList.get(0));
                            } else {
                                sb.append(orgName);
                            }
                        } else {
                            sb.append(this.objetList.get(0));
                        }
                        sb.append(" ");
                        sb.append("的评估结果为: ");
                        int nodeScorePrecision = this.nodeParam.getPMaintainInfo().getNodeScorePrecision();
                        BigDecimal result_nolast = this.info.getNodeScore().setScale(nodeScorePrecision,
                                RoundingMode.HALF_EVEN);
                        sb.append(result_nolast);
                        sb.append(",");
                    }
                } else if (this.info.getEvaluNode().isCanEvaluateLevel()) {
                    if (this.nodeParam.isOrg()) {
                        if (orgName == null) {
                            sb.append("该节点对 ： " + this.objetList.get(0) + " " + "评估等级为: "
                                    + this.info.getRank());
                        } else {
                            sb.append("该节点对 ： " + orgName + " " + "评估等级为: " + this.info.getRank());
                        }
                    } else {
                        sb.append("该节点对 ： " + this.objetList.get(0) + " " + "评估等级为: " + this.info.getRank());
                    }
                    sb.append(",");
                }
            } else if (BatchEvaluProcessHelper.isLastNode(this.info.getEvaluNode(), null)) {
                if (this.info.getEvaluNode().isCanEvaluateLevel()) {
                    if (this.nodeParam.isOrg()) {
                        if (orgName == null) {
                            sb.append(this.objetList.get(0) + "  最终评分  ： "
                                    + result.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                            RoundingMode.HALF_EVEN)
                                    + " 最终评估等级为 : " + " " + this.info.getRank());
                            this.info.getEvaluResult().setTargetEvaluScore(result);
                            this.result_tmp = result;
                        } else {
                            sb.append(orgName + "  最终评分 ： "
                                    + result.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                            RoundingMode.HALF_EVEN)
                                    + " 最终评估等级为 : " + " " + this.info.getRank());
                            this.info.getEvaluResult().setTargetEvaluScore(result);
                            this.result_tmp = result;
                        }
                    } else {
                        sb.append(this.objetList.get(0) + "  最终评分  ： "
                                + result.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                        RoundingMode.HALF_EVEN)
                                + " 最终评估等级为 : " + " " + this.info.getRank());
                        this.info.getEvaluResult().setTargetEvaluScore(result);
                        this.result_tmp = result;
                    }
                    sb.append(", ");
                }
            } else if (this.info.getEvaluNode().isCanEvaluateLevel()) {
                int nodeScorePrecision = this.nodeParam.getPMaintainInfo().getNodeScorePrecision();
                BigDecimal result_nolast = this.info.getNodeScore().setScale(nodeScorePrecision,
                        RoundingMode.HALF_EVEN);
                if (this.nodeParam.isOrg()) {
                    if (orgName == null) {
                        sb.append("该节点对 ： " + this.objetList.get(0) + " 评分 ： "
                                + result_nolast.setScale(
                                        this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                        RoundingMode.HALF_EVEN)
                                + " 评估等级为: " + this.info.getRank());
                    } else {
                        sb.append("该节点对 ： " + orgName + " 评分 ： "
                                + result_nolast.setScale(
                                        this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                        RoundingMode.HALF_EVEN)
                                + " 评估等级为: " + this.info.getRank());
                    }
                } else {
                    sb.append("该节点对 ： " + this.objetList.get(0) + " 评分 ： "
                            + result_nolast.setScale(this.nodeParam.getPMaintainInfo().getTotalPrecision(),
                                    RoundingMode.HALF_EVEN)
                            + " 评估等级为: " + this.info.getRank());
                }
                sb.append(", ");
            }
            resultMsg.put("opResult", "ok");
            resultMsg.put("evaluMsg", sb.toString());
            if (this.result_tmp != null) {
                resultMsg.put("result_tmp", this.result_tmp);
                try {
                    this.info.getEvaluResult().setTargetEvaluScore(this.result_tmp);
                    IEvaluResult iservice = EvaluResultFactory.getRemoteInstance();
                    iservice.save(this.info.getEvaluResult());
                } catch (BOSException e) {
                    e.printStackTrace();
                } catch (EASBizException e) {
                    e.printStackTrace();
                }
            }
            JSONUtils.SUCCESS(resultMsg);
        } else {
            resultMsg.put("opResult", "no");
            JSONUtils.writeJson(response, resultMsg);
        }
    }

    @Override
    public void submitTargetRealAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        HashMap<String, String> resultMsg = new HashMap<String, String>();
        this.assignmentId = request.getParameter("assignmentId");
        this.initParam();
        try {
            this.info.setExtendedProperty("assignmentID", this.assignmentId);
            this.info.setExtendedProperty("isAddNew", "isAddNew");
            INodeProcesser iservice = NodeProcesserFactory.getRemoteInstance();
            iservice.submit(this.info);
            resultMsg.put("opResult", "ok");
            JSONUtils.SUCCESS(resultMsg);
        } catch (Exception e) {
            resultMsg.put("opResult", "no");
            resultMsg.put("msg", "提交失败");
            JSONUtils.writeJson(response, resultMsg);
        }
    }

    @Override
    public void realSaveTargetForm(String datasFromPara, String count_datasFromPara, String commentDatas,
            HashMap<String, Object> result) {
        JSONObject item = null;
        int num = this.nodeParam.getCurrentNodeLevel();
        String nodeNeq = this.nodeParam.getNodeId();
        JSONArray count_datas = JSONArray.fromObject(count_datasFromPara);
        item = count_datas.getJSONObject(0);
        BigDecimal nodeScore = new BigDecimal("0.00");
        if (item.get("score" + nodeNeq) != null
                && !StringUtils.isEmpty(item.get("score" + nodeNeq).toString())) {
            try {
                nodeScore = BigDecimal.valueOf(Double.parseDouble(item.getString("score" + nodeNeq)));
            } catch (NumberFormatException e) {
                result.put("opResult", "no");
                result.put("msg", "输入得分为非法值,保存失败！");
                return;
            }
            this.info.setNodeScore(nodeScore);
        }
        if (item.get("targetRank" + nodeNeq) != null
                && !StringUtils.isEmpty(item.get("targetRank" + nodeNeq).toString())) {
            item = (JSONObject) item.get("targetRank" + nodeNeq);
            String serial = item.get("serial").toString();
            RankInfo rank = this.nodeParam.getRanks().get(Integer.parseInt(serial));
            this.info.setRank(rank);
        }
        JSONArray comment_Datas = JSONArray.fromObject(commentDatas);
        String comment = comment_Datas.get(comment_Datas.size() - 1).toString();
        this.info.setComment(comment);
        JSONArray datas_JSON = JSONArray.fromObject(datasFromPara);
        String targetID = "";
        EvaluDetailInfo detailInfo = null;
        BigDecimal targetScore = new BigDecimal("0.00");
        if (datas_JSON != null) {
            int size = datas_JSON.size();
            for (int i = 0; i < size; ++i) {
                boolean CanComment;
                item = (JSONObject) datas_JSON.get(i);
                targetID = item.get("id").toString();
                detailInfo = this.info.getEvaluDetailByTargetID(targetID);
                if (item.get("score" + nodeNeq) != null
                        && !StringUtils.isEmpty(item.get("score" + nodeNeq).toString())) {
                    try {
                        targetScore = BigDecimal
                                .valueOf(Double.parseDouble(item.get("score" + nodeNeq).toString()));
                    } catch (NumberFormatException e) {
                        result.put("opResult", "no");
                        result.put("msg", "输入得分为非法值,保存失败！");
                        return;
                    }
                    detailInfo.setTargetScore(targetScore);
                }
                if (item.get("targetComm" + nodeNeq) == null
                        || StringUtils.isEmpty(item.get("targetComm" + nodeNeq).toString())
                        || !(CanComment = this.info.getEvaluNode().isCanTargetComment()))
                    continue;
                detailInfo.setComment(item.get("targetComm" + nodeNeq).toString().trim());
            }
        }
        if (this.checkGradeMode(result)) {
            if (this.info.getWorkedState() == null) {
                this.info.setWorkedState(ProcesserWorkedStateEnum.NOTSUBMIT);
            }
            try {
                INodeProcesser iservice = NodeProcesserFactory.getRemoteInstance();
                IObjectPK pk = iservice.save(this.info);
                this.info.setId(BOSUuid.read(pk.toString()));
            } catch (BOSException e) {
                e.printStackTrace();
            } catch (EASBizException e) {
                e.printStackTrace();
            }
            result.put("opResult", "ok");
        } else {
            result.put("opResult", "no");
        }
    }

    @Override
    public void saveTargetFormAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        HashMap<String, Object> result = new HashMap<String, Object>();
        String datasFromPara = request.getParameter("datas");
        String count_datasFromPara = request.getParameter("count_datas");
        String commentDatas = request.getParameter("commentDatas");
        this.assignmentId = request.getParameter("assignmentId");
        this.initParam();
        this.realSaveTargetForm(datasFromPara, count_datasFromPara, commentDatas, result);
        JSONUtils.writeJson(response, result);
    }

    private boolean checkGradeMode(HashMap<String, Object> result) {
        return NodeProcesserHelper.checkGradeMode(this.nodeParam, this.info, result);
    }

    private void initPaperParam() throws Exception {
        this.nodeParam = new NodeParam();
        NodeProcesserHelper.initNodeParam(this.ctx, this.nodeParam, this.assignmentId);
    }

    private void fillCurrentNodeDetails() {
        NodeProcesserInfo curInfo = (NodeProcesserInfo) this.datas.get(this.nodeParam.getNodeId());
        EvaluDetailCollection curColl = curInfo.getEvaluDetails();
        EvaluDetailInfo curDetail = null;
        EvaluDetailInfo detail = null;
        NodeProcesserInfo info = null;
        String curStoreId = null;
        int flag = 0;
        if (curInfo.getId() == null) {
            int n = curColl.size();
            for (int i = this.datas.size() - 1; i >= 0; --i) {
                info = (NodeProcesserInfo) this.datas.get(this.seqNumbers[i]);
                if (this.nodeParam.getNodeId().equals(this.seqNumbers[i])
                        || this.nodeParam.getCurrentNodeLevel() == info.getEvaluNode().getNodeLevel())
                    continue;
                for (int m = 0; m < n; ++m) {
                    curDetail = curColl.get(m);
                    if (curDetail.getTargetScore() != null || curDetail == null
                            || curDetail.getTargetEvaluStore() == null
                            || curDetail.getTargetEvaluStore().getId() == null
                            || (detail = info.getEvaluDetailByTargetID(
                                    curStoreId = curDetail.getTargetEvaluStore().getId().toString())) == null)
                        continue;
                    if (this.nodeParam.isGrade()) {
                        curDetail.setTargetScore(detail.getTargetScore());
                    } else {
                        curDetail.setRank(detail.getRank());
                    }
                    if (++flag == n)
                        break;
                }
                if (flag == n)
                    break;
            }
        }
    }

    private NodeProcesserInfo getNodeProcesserInfoByNodeParam() {
        NodeProcesserInfo info = null;
        info = this.nodeParam.isAllocateTarget()
                ? NodeProcesserHelper.getNodeProcesserForAllocateTarget(this.datas, this.nodeParam,
                        this.seqNumbers)
                : (this.nodeParam.getNodeId() != null
                        ? (NodeProcesserInfo) this.datas.get(this.nodeParam.getNodeId())
                        : (NodeProcesserInfo) this.datas.get(this.seqNumbers[this.seqNumbers.length - 1]));
        return info;
    }

    private void initNodeProcesserInfo() throws Exception {
        NodeProcesserInfo ninfo;
        EvaluNodeInfo node;
        int size = this.datas.size();
        this.seqNumbers = new String[size];
        try {
            int[] sortNumber = new int[size];
            int index = 0;
            Iterator iter = this.datas.keySet().iterator();
            while (iter.hasNext()) {
                sortNumber[index] = Integer.parseInt((String) iter.next());
                ++index;
            }
            Arrays.sort(sortNumber);
            for (int i = 0; i < size; ++i) {
                this.seqNumbers[i] = "" + sortNumber[i];
            }
        } catch (NumberFormatException e) {
            int i = 0;
            /*
             * for (String this.seqNumbers[i] : this.datas.keySet()) { ++i; }
             */
            Arrays.sort(this.seqNumbers);
        }
        if (this.nodeParam.isScoreToNextNode() && this.nodeParam.getNodeId() != null
                && this.datas.get(this.nodeParam.getNodeId()) != null && this.nodeParam.getComeFromFlag() == 1
                && (node = (ninfo = (NodeProcesserInfo) this.datas.get(this.nodeParam.getNodeId()))
                        .getEvaluNode()).isCanGrade()) {
            this.fillCurrentNodeDetails();
        }
    }

    public static Set getWickedlyColumnSet_ContractTemplate() {
        HashSet<String> wickedlyColumnSet_ContractTemplate = new HashSet<String>();
        wickedlyColumnSet_ContractTemplate.add("scoreEditEnable");
        wickedlyColumnSet_ContractTemplate.add("commentEditEnable");
        wickedlyColumnSet_ContractTemplate.add("dataPrecision");
        wickedlyColumnSet_ContractTemplate.add("precisionType");
        wickedlyColumnSet_ContractTemplate.add("creator");
        wickedlyColumnSet_ContractTemplate.add("creatorPosition");
        wickedlyColumnSet_ContractTemplate.add("score");
        wickedlyColumnSet_ContractTemplate.add("targetComm");
        return wickedlyColumnSet_ContractTemplate;
    }

    private void calFormulaValue(NodeParam nodeParam) throws EASBizException, BOSException {
        TargetScoreComputeFacadeFactory.getLocalInstance(this.ctx)
                .updateTargetRealValue(nodeParam.getSolutionPeriodID(), nodeParam.getEvaluObjectID());
        TargetScoreComputeFacadeFactory.getLocalInstance(this.ctx)
                .computeTargetScore(nodeParam.getSolutionPeriodID(), nodeParam.getEvaluObjectID());
    }

    private boolean isContainFormulaInEvaluTarget() {
        FilterInfo filter = new FilterInfo();
        filter.getFilterItems().add(new FilterItemInfo("solutionPeriod.id",
                this.nodeParam.getSolutionPeriodID(), CompareType.EQUALS));
        filter.getFilterItems().add(
                new FilterItemInfo("evaluObject.id", this.nodeParam.getEvaluObjectID(), CompareType.EQUALS));
        EntityViewInfo evi = new EntityViewInfo();
        evi.setFilter(filter);
        SelectorItemCollection sic = evi.getSelector();
        sic.add(new SelectorItemInfo("id"));
        sic.add(new SelectorItemInfo("multiTable.compute"));
        sic.add(new SelectorItemInfo("evaluTarget.formula.id"));
        TargetEvaluStoreCollection storeColl = null;
        try {
            storeColl = TargetEvaluStoreFactory.getRemoteInstance().getTargetEvaluStoreCollection(evi);
        } catch (BOSException e) {
            // empty catch block
        }
        TargetEvaluStoreInfo storeInfo = null;
        for (int i = 0; i < storeColl.size(); ++i) {
            storeInfo = storeColl.get(i);
            if (!storeInfo.getMultiTable().isCompute() || storeInfo.getEvaluTarget().getFormula() == null)
                continue;
            return true;
        }
        return false;
    }

    @Override
    public void initTargetFormAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SQLException, BOSException, SHRWebException {
        HashMap<String, String> result = new HashMap<String, String>();
        boolean calFormula_flag = false;
        this.assignmentId = request.getParameter("assignmentId");
        this.proInstId = request.getParameter("proInstId");
        String SOLUTIONPERIODID = request.getParameter("SOLUTIONPERIODID");
        String nodeState = request.getParameter("nodeState");
        String evaluResultFlag = request.getParameter("evaluResultFlag");
        if ("".equalsIgnoreCase(nodeState)) {
            nodeState = "0";
        }
        this.initParam();
        if (this.nodeParam.getComeFromFlag() != 2 && this.isContainFormulaInEvaluTarget()) {
            try {
                this.calFormulaValue(this.nodeParam);
                calFormula_flag = true;
            } catch (EASBizException e) {
                result.put("opResult", "no");
                result.put("msg", e.getMessage());
                JSONUtils.writeJson(response, result);
                e.printStackTrace();
                return;
            }
        }
        this.initParam();
        EvaluFormSolutionMultiTableCollection multiTableColl = TargetEvaluManageHelper
                .getMultiTableCollecton(SOLUTIONPERIODID, this.ctx);
        this.solutionPeriodInfo = this.targetEvaluStoreBiz.getSolutionPeriodInfo(SOLUTIONPERIODID);
        ArrayList<MultiTableData> targetForm = new ArrayList<MultiTableData>();
        MultiTableData mtd = new MultiTableData();
        ArrayList multiTableData = new ArrayList();
        int ruleModeValue = this.solutionPeriodInfo.getEvaluSolution().getEvaluRule().getRuleMode()
                .getValue();
        Object data = null;
        List nodeProcesserDataList = null;
        ArrayList<String> colNames = null;
        List<Map<String, Object>> modelColList = null;
        Map<String, Object> gridIdMap = null;
        Object map1 = null;
        Object map2 = null;
        Object map0 = null;
        EvaluFormSolutionEntryCollection evaluFormSolutionEntryColl = null;
        NodeProcesserInfo wholePageNodeProcesserInfo = this.getNodeProcesserInfoByNodeParam();
        Set WickedlyColumnSet = AssessTargetEvaluEditHandler.getWickedlyColumnSet_ContractTemplate();
        for (int i = 0; i < multiTableColl.size(); ++i) {
            EvaluFormSolutionMultiTableInfo multiTableInfo = multiTableColl.get(i);
            mtd = new MultiTableData();
            mtd.setCompute(multiTableInfo.isCompute());
            colNames = new ArrayList<String>();
            modelColList = new ArrayList();
            Set nodes = this.getCurrentVisibleNodes();
            int size = this.datas.size();
            for (int j = 0; j < size; ++j) {
                NodeProcesserInfo noedProcesserInfo = (NodeProcesserInfo) this.datas.get(this.seqNumbers[j]);
                EvaluNodeInfo evaluNodeInfo = noedProcesserInfo.getEvaluNode();
                if (evaluResultFlag == null || !"true".equalsIgnoreCase(evaluResultFlag)) {
                    if (NodeVisibleEnum.part.equals(this.nodeParam.getNodeVisibleMode())
                            && this.hiddenScoreColunm(nodes, evaluNodeInfo))
                        continue;
                    if (this.nodeParam.getCurrentNodeLevel() != -1) {
                        if (evaluNodeInfo.getNodeLevel() > this.nodeParam.getCurrentNodeLevel())
                            break;
                        if (evaluNodeInfo.getNodeLevel() == this.nodeParam.getCurrentNodeLevel()
                                && !this.seqNumbers[j].equals(this.nodeParam.getNodeId()))
                            continue;
                    }
                    if (NodeVisibleEnum.self.equals(this.nodeParam.getNodeVisibleMode())
                            && this.nodeParam.getCurrentNodeLevel() != evaluNodeInfo.getNodeLevel())
                        continue;
                }
                boolean editable_flag = true;
                if (this.nodeParam.getComeFromFlag() == 2) {
                    editable_flag = false;
                }
                if (this.nodeParam.getCurrentNodeLevel() != evaluNodeInfo.getNodeLevel()) {
                    editable_flag = false;
                }
                if (!noedProcesserInfo.getEvaluNode().isCanGrade() || !this.nodeParam.isCanModifyScore()) {
                    editable_flag = false;
                }
                this.addRemarkAndScoreColumn(multiTableInfo, noedProcesserInfo, evaluNodeInfo, colNames,
                        modelColList, multiTableInfo.getId().toString(), evaluNodeInfo.getSeq(),
                        editable_flag, calFormula_flag, nodeState);
            }
            if ("0".equalsIgnoreCase(nodeState)) {
                modelColList = CommonUtil.topModelColList(modelColList, this.nodeParam.getNodeId());
                colNames = (ArrayList<String>) CommonUtil.topColNames(colNames, this.nodeParam.getNodeId());
            }
            colNames.add("ID");
            gridIdMap = new HashMap<String, Object>();
            gridIdMap.put("index", "id");
            gridIdMap.put("name", "id");
            gridIdMap.put("hidden", true);
            modelColList.add(gridIdMap);
            mtd.setId(multiTableInfo.getId().toString());
            mtd.setPartTableDes(multiTableInfo.getPartTableDes());
            mtd.setName(multiTableInfo.getName());
            mtd.setPartTableDesShow(multiTableInfo.isPartTableDesShow());
            evaluFormSolutionEntryColl = TargetEvaluManageHelper.getSingleTableSetInfo(
                    this.solutionPeriodInfo.getFormSolution().getId().toString(),
                    multiTableInfo.getId().toString());
            EvaluFormSolutionEntryInfo evaluFormSolutionEntryInfo = null;
            for (int j = 0; j < evaluFormSolutionEntryColl.size(); ++j) {
                HashMap<String, Object> para;
                evaluFormSolutionEntryInfo = evaluFormSolutionEntryColl.get(j);
                String id = evaluFormSolutionEntryInfo.getCode();
                if (WickedlyColumnSet.contains(id) || !evaluFormSolutionEntryInfo.isVisible())
                    continue;
                String name = evaluFormSolutionEntryInfo.getName();
                if (!StringUtil.isEmptyString(evaluFormSolutionEntryInfo.getAlias())) {
                    name = evaluFormSolutionEntryInfo.getAlias();
                }
                colNames.add(name);
                gridIdMap = new HashMap();
                gridIdMap.put("index", evaluFormSolutionEntryInfo.getCode());
                gridIdMap.put("name", evaluFormSolutionEntryInfo.getCode());
                gridIdMap.put("editable", false);
                gridIdMap.put("width", evaluFormSolutionEntryInfo.getColumnWidth());
                if (evaluFormSolutionEntryInfo.getCode().equals("kind")) {
                    para = new HashMap<String, Object>();
                    para.put("enumSource", KindEnum.getEnumMap());
                    para.put("urlSource", request.getContextPath()
                            + "/selectEnum.do?method=getSelectEnumData&enumSource=com.kingdee.eas.hr.perf.KindEnum");
                    gridIdMap = CommonUtil.setEnum(gridIdMap, para);
                } else if (evaluFormSolutionEntryInfo.getCode().equals("calFormula")) {
                    para = new HashMap();
                    para.put("title", name);
                    para.put("uipk", "com.kingdee.eas.hr.perf.app.Formula.F7");
                    para.put("filter", "");
                    gridIdMap = CommonUtil.setF7(gridIdMap, para);
                } else if (evaluFormSolutionEntryInfo.getCode().equals("reportFrequency")) {
                    para = new HashMap();
                    para.put("enumSource", ReportFrequencyEnum.getEnumMap());
                    para.put("urlSource", request.getContextPath()
                            + "/selectEnum.do?method=getSelectEnumData&enumSource=com.kingdee.eas.hr.perf.ReportFrequencyEnum");
                    gridIdMap = CommonUtil.setEnum(gridIdMap, para);
                } else if (evaluFormSolutionEntryInfo.getCode().equals("precisionType")) {
                    para = new HashMap();
                    para.put("enumSource", PrecisionTypeEnum.getEnumMap());
                    para.put("urlSource", request.getContextPath()
                            + "/selectEnum.do?method=getSelectEnumData&enumSource=com.kingdee.eas.hr.perf.PrecisionTypeEnum");
                    gridIdMap = CommonUtil.setEnum(gridIdMap, para);
                }
                modelColList.add(gridIdMap);
            }
            NodeProcesserInfo nodeSinglePorcesserInfo = NodeProcesserHelper.getSingleNodePorcesserInfo(
                    wholePageNodeProcesserInfo, multiTableInfo.getId().toString());
            nodeProcesserDataList = NodeProcesserHelper.initGridDataForParallel(nodeSinglePorcesserInfo,
                    this.datas, this.primaryPosition, this.nodeParam);

            // 2020-3-27扩展：过滤功能，根据绩效指标的所属组织和当前登录人的所属组织和指标id进行过滤

            // 获取当前登录人id和行政组织
            String currentPersonId = SHRHelper.getCurrPersonInfoNew(ctx).getId().toString();
            String currentOrgId = SHRBillUtil.getAdminOrgUnit(currentPersonId).getPersonDep().getId()
                    .toString();
            System.out.println("当前登录人：" + currentPersonId);

            // 遍历表格数据
            Iterator<NodeProcesserData> iterator = nodeProcesserDataList.iterator();
            while (iterator.hasNext()) {
                NodeProcesserData node = iterator.next();

                // 获取该条考核目标的指定员工
                String personIdStr = getPersonId(node.getId());

                System.out.println("当前指标的指定人员：" + personIdStr);
                // 以，分割获取id数组
                if (!StringUtils.isEmpty(personIdStr)) {
                    String[] personIdArray = personIdStr.split(",");

                    // 将数组转换为list
                    ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(personIdArray));

                    // 定义标识
                    boolean flag = false;

                    // 判断当前登录人是否是指定员工
                    if (arrayList.contains(currentPersonId)) {
                        flag = true;
                    }

                    // 当考核目标所属组织中不包含登录人的组织，将该条数据去掉
                    if (!flag) {
                        iterator.remove();
                    }
                }
            }

            mtd.setNodeProcesserData(nodeProcesserDataList);
            mtd.setColNames(colNames);
            mtd.setModelColList(modelColList);
            mtd.setRuleModeValue(ruleModeValue);
            targetForm.add(mtd);
        }
        this.addNodeGradeDataGrid(targetForm, request);
        JSONUtils.SUCCESS(targetForm);
    }

    /**
     * @description 查询指定的考核目标所指定的人员
     * @title getPersonId
     * @param targetId id
     * @return personIdStr 查询到的人员id字符串
     * @throws BOSException
     * @author Jambin
     * @date 2020年3月30日
     */
    private String getPersonId(String targetId) throws BOSException {
        String sql = "select distinct tpe.fid 考核目标, tpp.cfpersonid 指定员工 from T_PF_EvaluTarget tpe left join T_PF_TargetEvaluStore  tes on tpe.fid=tes.Fevalutargetid left join T_PF_PerfItem tpp on tpp.fid=  tpe.ftargetid where tes.fid='"
                + targetId + "'";

        String personIdStr = "";
        System.out.println("查询指定的考核目标所指定的人员sql:" + sql);
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                personIdStr = rowSet.getString("指定员工");
            }
        } catch (BOSException e) {
            throw new BOSException("查询指定的考核目标所指定的人员");
        } catch (SQLException e) {
            throw new BOSException("查询指定的考核目标所指定的人员");
        }
        return personIdStr;

    }

    private void addNodeGradeDataGrid(List<MultiTableData> targetForm, HttpServletRequest request) {
        MultiTableData mtd = new MultiTableData();
        ArrayList<String> colNames = new ArrayList<String>();
        List<Map<String, Object>> modelColList = new ArrayList<Map<String, Object>>();
        ArrayList<NodeCountData> nodeCountDatas = new ArrayList<NodeCountData>();
        HashMap gridIdMap = null;
        ArrayList<Integer> nodeIsCanComment = new ArrayList<Integer>();
        NodeProcesserInfo nodeProcesserInfo = null;
        EvaluNodeInfo evaluNodeInfo = null;
        NodeCountData nodeCountData = new NodeCountData();
        String evaluResultFlag = request.getParameter("evaluResultFlag");
        for (int i = 0; i < this.datas.size(); ++i) {
            nodeProcesserInfo = (NodeProcesserInfo) this.datas.get(this.seqNumbers[i]);
            evaluNodeInfo = nodeProcesserInfo.getEvaluNode();
            int seq = evaluNodeInfo.getSeq();
            if (evaluResultFlag == null || !"true".equalsIgnoreCase(evaluResultFlag)) {
                Set nodes;
                if (NodeVisibleEnum.part.equals(this.nodeParam.getNodeVisibleMode())
                        && this.hiddenScoreColunm(nodes = this.getCurrentVisibleNodes(), evaluNodeInfo))
                    continue;
                if (this.nodeParam.getCurrentNodeLevel() != -1) {
                    if (evaluNodeInfo.getNodeLevel() > this.nodeParam.getCurrentNodeLevel())
                        break;
                    if (evaluNodeInfo.getNodeLevel() == this.nodeParam.getCurrentNodeLevel()
                            && !this.seqNumbers[i].equals(this.nodeParam.getNodeId()))
                        continue;
                }
                if (NodeVisibleEnum.self.equals(this.nodeParam.getNodeVisibleMode())
                        && this.nodeParam.getCurrentNodeLevel() != evaluNodeInfo.getNodeLevel())
                    continue;
            }
            if (evaluNodeInfo.getCanComment() != null) {
                nodeIsCanComment.add(evaluNodeInfo.getCanComment().getValue());
            }
            colNames.add(this.getShortNodeName(nodeProcesserInfo, evaluNodeInfo) + " 节点总分");
            gridIdMap = new HashMap();
            gridIdMap.put("index", "score" + seq);
            gridIdMap.put("name", "score" + seq);
            gridIdMap.put("width", 130);
            gridIdMap.put("editable", false);
            gridIdMap.put("formatter", "number");
            modelColList.add(gridIdMap);
            colNames.add(this.getShortNodeName(nodeProcesserInfo, evaluNodeInfo) + " 评分等级");
            gridIdMap = new HashMap();
            gridIdMap.put("index", "targetRank" + seq);
            gridIdMap.put("name", "targetRank" + seq);
            gridIdMap.put("editable", false);
            gridIdMap.put("width", 130);
            boolean isCanEvaluateLevel = evaluNodeInfo.isCanEvaluateLevel();
            if (isCanEvaluateLevel && this.nodeParam.getComeFromFlag() != 2
                    && this.nodeParam.getCurrentNodeLevel() == evaluNodeInfo.getNodeLevel()) {
                gridIdMap.put("editable", true);
            }
            HashMap<String, String> para = new HashMap<String, String>();
            para.put("title", "评分等级");
            para.put("uipk", "com.kingdee.eas.hr.perf.app.Rank.F7");
            para.put("filter", "");
            gridIdMap = (HashMap) CommonUtil.setF7(gridIdMap, para);
            modelColList.add(gridIdMap);
            nodeCountData = CommonUtil.setNodescore(seq, nodeCountData, nodeProcesserInfo.getNodeScore());
            nodeCountData = CommonUtil.setTargetRank(seq, nodeCountData, nodeProcesserInfo.getRank());
            nodeCountData = CommonUtil.setComment(seq, nodeCountData, nodeProcesserInfo.getComment());
        }
        nodeCountDatas.add(nodeCountData);
        mtd.setName("评估节点合计");
        if (this.nodeParam.getRanks().size() > 0) {
            String rankFilter = "group='" + this.nodeParam.getRanks().get(0).getGroup().getId().toString()
                    + "'";
            mtd.setRankFilter(rankFilter);
        }
        mtd.setNodeCountDatas(nodeCountDatas);
        mtd.setColNames(colNames);
        mtd.setModelColList(modelColList);
        mtd.setNodeIsCanComment(nodeIsCanComment);
        targetForm.add(mtd);
    }

    @Override
    public boolean currentNodeWeightIsVisable() {
        NodeProcesserInfo currentNodeProcesserInfo = null;
        currentNodeProcesserInfo = (NodeProcesserInfo) this.datas.get(this.nodeParam.getNodeId());
        EvaluNodeInfo currentEvaluNodeInfo = null;
        if (currentNodeProcesserInfo != null) {
            currentEvaluNodeInfo = currentNodeProcesserInfo.getEvaluNode();
        }
        if (currentEvaluNodeInfo != null && currentEvaluNodeInfo.isNodeVisible()) {
            return true;
        }
        return false;
    }

    private String getShortNodeName(NodeProcesserInfo info, EvaluNodeInfo nodeInfo) {
        StringBuffer name = new StringBuffer();
        name.append("[");
        name.append(nodeInfo.getNodeName());
        name.append("] ");
        name.append(info.getEvaluer());
        if (this.currentNodeWeightIsVisable()) {
            name.append(" ");
            BigDecimal weight = nodeInfo.getWeight();
            if (weight != null && weight.doubleValue() % 1.0 == 0.0) {
                name.append(weight.intValue());
            } else {
                name.append(this.format.format(weight));
            }
            name.append("%");
        }
        return name.toString();
    }

    @Override
    public void getSelectDataAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        ArrayList data = new ArrayList();
        this.assignmentId = request.getParameter("assignmentId");
        this.initParam();
        HashMap<String, Object> map = null;
        RankInfo rankInfo = null;
        DecimalFormat df = new DecimalFormat("#.00");
        if (!this.nodeParam.isGrade()) {
            int size = this.nodeParam.getTargetRanks().size();
            for (int i = 0; i < size; ++i) {
                rankInfo = this.nodeParam.getTargetRanks().get(i);
                map = new HashMap<String, Object>();
                map.put("value", rankInfo.getGrade());
                map.put("alias", df.format(rankInfo.getGrade()));
                data.add(map);
            }
        }
        this.writeSuccessData(data);
    }

    private Map<String, Object> getEnumSource() {
        HashMap<String, Object> m1 = new HashMap<String, Object>();
        RankInfo rankInfo = null;
        if (!this.nodeParam.isGrade()) {
            int size = this.nodeParam.getTargetRanks().size();
            DecimalFormat df = new DecimalFormat("#.00");
            HashMap<String, Object> m2 = null;
            for (int i = 0; i < size; ++i) {
                rankInfo = this.nodeParam.getTargetRanks().get(i);
                m2 = new HashMap<String, Object>();
                m2.put("isenum", true);
                m2.put("alias", df.format(rankInfo.getGrade()));
                m2.put("value", rankInfo.getGrade());
                m1.put(rankInfo.getId().toString(), m2);
            }
        }
        return m1;
    }

    private Set getCurrentVisibleNodes() {
        NodeProcesserInfo curNodeProcesserInfo;
        NodeVisibleCollection nodeVisibleColl;
        HashSet<Integer> nodes = new HashSet<Integer>();
        if (this.nodeParam.getNodeId() != null && (curNodeProcesserInfo = this.info) != null
                && (nodeVisibleColl = curNodeProcesserInfo.getEvaluNode().getNodes()) != null
                && nodeVisibleColl.size() > 0) {
            NodeVisibleInfo nodeVisible = null;
            int size = nodeVisibleColl.size();
            for (int i = 0; i < size; ++i) {
                nodeVisible = nodeVisibleColl.get(i);
                if (nodeVisible.getNodeLevel() == 0) {
                    nodes.add(new Integer(nodeVisible.getNodeLevel()));
                    continue;
                }
                nodes.add(new Integer(nodeVisible.getNodeLevel()));
            }
        }
        return nodes;
    }

    private boolean hiddenScoreColunm(Set nodes, EvaluNodeInfo evaluNodeInfo) {
        boolean falg = true;
        if (nodes != null && nodes.size() > 0 && nodes.contains(new Integer(evaluNodeInfo.getNodeLevel()))) {
            falg = false;
        }
        if (evaluNodeInfo.getNodeLevel() == this.nodeParam.getCurrentNodeLevel()) {
            falg = false;
        }
        return falg;
    }

    private void addTargetScoreColumn(EvaluFormSolutionMultiTableInfo multiTableInfo, List<String> colNames,
            List<Map<String, Object>> modelColList, NodeProcesserInfo nodeProcesserInfo,
            EvaluNodeInfo evaluNodeInfo, int i, boolean editable_flag, boolean calFormula_flag,
            String nodeState) {
        String nodeName = this.getShortNodeName(nodeProcesserInfo, evaluNodeInfo);
        colNames.add(nodeName + " (" + i + ")");
        Map<String, Object> gridIdMap = new HashMap<String, Object>();
        gridIdMap.put("index", "score" + i);
        gridIdMap.put("name", "score" + i);
        gridIdMap.put("editable", false);
        gridIdMap.put("width", 100);
        if (!this.nodeParam.isGrade() && "0".equalsIgnoreCase(nodeState)) {
            HashMap<String, Object> para = new HashMap<String, Object>();
            para.put("enumSource", this.getEnumSource());
            para.put("urlSource",
                    "/shr/dynamic.do?handler=com.kingdee.shr.perf.web.handler.peopleSelf.AssessTargetEvaluEditHandler&method=getSelectData&assignmentId="
                            + this.assignmentId);
            gridIdMap = CommonUtil.setEnum(gridIdMap, para);
        } else {
            gridIdMap.put("formatter", "number");
        }
        modelColList.add(gridIdMap);
        if (editable_flag) {
            gridIdMap.put("editable", true);
        }
    }

    private void addTargetCommColumn(String mutiTableID, List<String> colNames,
            List<Map<String, Object>> modelColList, NodeProcesserInfo processerInfo, EvaluNodeInfo nodeInfo,
            int i, boolean editable_flag) {
        Map columnMap = CommonUtil.getColumnMap(mutiTableID, "targetComm");
        boolean canEdit = true;
        String targetCommName = columnMap.get("commName").toString();
        int columnWidth = (Integer) columnMap.get("columnWidth");
        if (targetCommName == null || "".equals(targetCommName)) {
            StringBuffer name = new StringBuffer();
            name.append("[");
            name.append(nodeInfo.getNodeName());
            name.append("] ");
            name.append(processerInfo.getEvaluer());
            targetCommName = name.toString();
        }
        colNames.add(targetCommName + " (" + i + ")");
        HashMap<String, Object> gridIdMap = new HashMap<String, Object>();
        gridIdMap.put("index", "targetComm" + i);
        gridIdMap.put("name", "targetComm" + i);
        gridIdMap.put("editable", false);
        gridIdMap.put("width", columnWidth);
        modelColList.add(gridIdMap);
        if (this.nodeParam.getCurrentNodeLevel() != nodeInfo.getNodeLevel()) {
            canEdit = false;
        }
        if (processerInfo.getEvaluNode().isCanTargetComment() && canEdit) {
            gridIdMap.put("editable", true);
        }
    }

    private void addRemarkAndScoreColumn(EvaluFormSolutionMultiTableInfo multiTableInfo,
            NodeProcesserInfo processerInfo, EvaluNodeInfo nodeInfo, List<String> colNames,
            List<Map<String, Object>> modelColList, String mutiTableID, int seq, boolean editable_flag,
            boolean calFormula_flag, String nodeState) {
        this.addTargetScoreColumn(multiTableInfo, colNames, modelColList, processerInfo, nodeInfo, seq,
                editable_flag, calFormula_flag, nodeState);
        if (processerInfo.getEvaluNode().isCanTargetComment()) {
            this.addTargetCommColumn(mutiTableID, colNames, modelColList, processerInfo, nodeInfo, seq,
                    editable_flag);
        }
    }

    @Override
    public AssessTargetData setAssessTargetForEvalu(AssessTargetData atdate, IRowSet row, String nodeState,
            String evaluResultFlag) throws SQLException {
        String responpositionName;
        atdate = new AssessTargetData();
        if (row.getString("id") != null) {
            atdate.setID(row.getString("id"));
            String assignmentId = row.getString("id");
            if (assignmentId != null && assignmentId.contains("WFWKITEM")) {
                this.assignmentId = assignmentId;
                this.initParam();
                if (this.info.getNodeScore() != null) {
                    atdate.setNODESCORE(this.info.getNodeScore().toString());
                }
            }
        }
        if ("true".equalsIgnoreCase(evaluResultFlag)) {
            if ("101".equalsIgnoreCase(String.valueOf(row.getInt("evaluSolution.solutionType")))) {
                if (row.getString("adminOrgUnit.displayName") != null) {
                    atdate.setORGNAME(row.getString("adminOrgUnit.displayName"));
                }
                if (!StringUtil.isEmptyString(responpositionName = row.getString("person.name"))) {
                    atdate.setPRINCIPAL(responpositionName);
                } else {
                    atdate.setPRINCIPAL("");
                }
                String responserId = row.getString("person.id");
                String principalName = "";
                if (!StringUtil.isEmptyString(responserId)) {
                    atdate.setRESPONSERID(responserId);
                    PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(responserId);
                    principalName = personPositionInfo.getPrimaryPosition().getName();
                }
                if (!StringUtil.isEmptyString(principalName)) {
                    atdate.setRESPONPOSITION(principalName);
                } else {
                    atdate.setRESPONPOSITION("");
                }
                if (row.getString("adminOrgUnit.id") != null) {
                    atdate.setOBJECTID(row.getString("adminOrgUnit.id"));
                }
            } else {
                if (row.getString("adminOrgUnit.name") != null) {
                    atdate.setORG(row.getString("adminOrgUnit.name"));
                }
                if (row.getString("adminOrgUnit.displayName") != null) {
                    atdate.setFULLORGNAME(row.getString("adminOrgUnit.displayName"));
                }
                atdate.setPOSITION(row.getString("position.name"));
                if (row.getString("person.id") != null) {
                    atdate.setOBJECTID(row.getString("person.id"));
                }
            }
            if (row.getString("modifiedScore") != null) {
                atdate.setFINALSCORE(row.getString("modifiedScore"));
            }
            if (row.getString("rank.name") != null) {
                atdate.setRANK(row.getString("rank.name"));
            }
            if (row.getString("modifyCause") != null) {
                atdate.setMODIFYCAUSE(row.getString("modifyCause"));
            }
            if (row.getString("solutionPeriod.id") != null) {
                atdate.setSOLUTIONPERIODID(row.getString("solutionPeriod.id"));
            }
        } else {
            if (row.getString("targetGradeModeLink.name") != null) {
                atdate.setGRADEMODE(String.valueOf(row.getString("targetGradeModeLink.name")));
            }
            atdate.setSOLUTIONTYPE(String.valueOf(row.getInt("solutionType")));
            if ("101".equalsIgnoreCase(atdate.getSOLUTIONTYPE())) {
                if ("1".equalsIgnoreCase(nodeState)) {
                    atdate.setORGNAME(row.getString("evaluOrg.displayName"));
                } else {
                    atdate.setORGNAME(
                            CommonUtil.getAdminOrgUnitInfo(row.getString("orgUnit.id")).getDisplayName());
                }
                responpositionName = row.getString("responser.name");
                if (!StringUtil.isEmptyString(responpositionName)) {
                    atdate.setPRINCIPAL(responpositionName);
                } else {
                    atdate.setPRINCIPAL("");
                }
                String responserId = row.getString("responser.id");
                String principalName = "";
                if (!StringUtil.isEmptyString(responserId)) {
                    atdate.setRESPONSERID(responserId);
                    PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(responserId);
                    principalName = personPositionInfo.getPrimaryPosition().getName();
                }
                if (!StringUtil.isEmptyString(principalName)) {
                    atdate.setRESPONPOSITION(principalName);
                } else {
                    atdate.setRESPONPOSITION("");
                }
            }
            if ("1".equalsIgnoreCase(nodeState)) {
                if (row.getString("person.id") != null) {
                    atdate.setOBJECTID(row.getString("person.id"));
                }
                if (row.getString("adminOrgUnit.name") != null) {
                    atdate.setORG(row.getString("adminOrgUnit.name"));
                }
                if (row.getString("adminOrgUnit.displayName") != null) {
                    atdate.setFULLORGNAME(row.getString("adminOrgUnit.displayName"));
                }
                if (row.getString("solutionPeriodId") != null) {
                    atdate.setSOLUTIONPERIODID(row.getString("solutionPeriodId"));
                }
                if (row.getString("evaluNode.seqNumber") != null) {
                    atdate.setNODESEQ(row.getString("evaluNode.seqNumber"));
                }
                if (row.getBigDecimal("nodeScore") != null) {
                    atdate.setNODESCORE(row.getBigDecimal("nodeScore").toString());
                }
                if (row.getString("rank.name") != null) {
                    atdate.setRANK(row.getString("rank.name"));
                }
            } else if ("0".equalsIgnoreCase(nodeState)) {
                if (row.getString("adminOrgUnit1.name") != null) {
                    atdate.setORG(row.getString("adminOrgUnit1.name"));
                }
                if (row.getString("adminOrgUnit1.displayName") != null) {
                    atdate.setFULLORGNAME(row.getString("adminOrgUnit1.displayName"));
                }
                if (row.getString("procInstID") != null) {
                    atdate.setProcInstID(row.getString("procInstID"));
                }
                if (row.getString("evaluObject.objectID") != null) {
                    atdate.setOBJECTID(row.getString("evaluObject.objectID"));
                }
                if (row.getString("solutionPeriod.id") != null) {
                    atdate.setSOLUTIONPERIODID(row.getString("solutionPeriod.id"));
                }
            }
            atdate.setPOSITION(row.getString("position.name"));
        }
        atdate.setEVALUOBJECTID(row.getString("evaluObject.id"));
        atdate.setSOLUTIONPERIODNAME(row.getString("solutionPeriod.name"));
        atdate.setEVALUOBJECTNUMBER(row.getString("evaluObject.number"));
        atdate.setEVALUOBJECTNAME(row.getString("evaluObject.name"));
        atdate.setEVALUSTARTDATE(
                CommonUtil.dateFormat(row.getDate("solutionPeriod.evaluStartDate"), "yyyy-MM-dd"));
        atdate.setEVALUENDDATE(
                CommonUtil.dateFormat(row.getDate("solutionPeriod.evaluEndDate"), "yyyy-MM-dd"));
        return atdate;
    }

    private FilterInfo setFilter(FilterInfo filter, String nodeState, String evaluResultFlag) {
        Context ctx = SHRContext.getInstance().getContext();
        PersonInfo person = ContextUtil.getCurrentUserInfo(ctx).getPerson();
        UserInfo user = ContextUtil.getCurrentUserInfo(ctx);
        if ("0".equalsIgnoreCase(nodeState)) {
            filter.getFilterItems().add(new FilterItemInfo("person.id", person.getId().toString()));
            filter.getFilterItems().add(new FilterItemInfo("user.id", user.getId().toString()));
            filter.getFilterItems().add(new FilterItemInfo("user.id", null, CompareType.EQUALS));
            filter.getFilterItems().add(new FilterItemInfo("user.id", "", CompareType.EQUALS));
            filter.setMaskString("#0 and (#1 or #2 or #3)");
        } else if ("1".equalsIgnoreCase(nodeState)) {
            boolean isPerson = true;
            if (isPerson) {
                filter.getFilterItems().add(new FilterItemInfo("evaluObject.evaluObjectType", "100"));
            } else {
                filter.getFilterItems().add(new FilterItemInfo("evaluObject.evaluObjectType", "101"));
            }
            filter.getFilterItems().add(new FilterItemInfo("evaluer.id", person.getId().toString()));
        } else if ("true".equalsIgnoreCase(evaluResultFlag)) {
            filter = new FilterInfo();
            filter.getFilterItems().add(new FilterItemInfo("person.id", person.getId().toString()));
        }
        return filter;
    }

    private ITreeBuilder getTreeBuilder(ListUIViewInfo uiViewInfo) throws SHRWebException {
        TreeNavigationInfo treeNavigation = uiViewInfo.getTreeNavigation();
        String treeBuilder = treeNavigation.getBuilder();
        if (StringUtils.isEmpty(treeBuilder)) {
            treeBuilder = !StringUtils.isEmpty(treeNavigation.getTreeModel())
                    ? CommonTreeBuilder.class.getName()
                    : OrgUnitTreeBuilder.class.getName();
        }
        return (ITreeBuilder) DynamicUtil.newInstance(treeBuilder);
    }

    @Override
    public String getTreeDataAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException, EASBizException, BOSException {
        try {
            String rootId = request.getParameter("rootId");
            String nodeId = request.getParameter("nodeId");
            Context ctx = SHRContext.getInstance().getContext();
            ListUIViewInfo uiViewInfo = (ListUIViewInfo) this.getUIViewInfo();
            ITreeBuilder treeBuilder = this.getTreeBuilder(uiViewInfo);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("domain", "[('id','=','$UserAdminRangeFilter')]");
            params.put("treeModel", uiViewInfo.getTreeNavigation().getTreeModel());
            Object result = null;
            if (!StringUtils.isEmpty(nodeId) && StringUtils.isEmpty(rootId)) {
                params.put("nodeId", nodeId);
                result = treeBuilder.getChildren(ctx, params);
            } else {
                params.put("nodeId", rootId);
                params.put("rootAlias", uiViewInfo.getTreeNavigation().getRootAlias());
                result = treeBuilder.getRootNode(ctx, params);
            }
            JSONUtils.writeJson(response, result);
        } catch (SHRWebException e) {
            throw new SHRWebException(e.getMessage());
        }
        return null;
    }

    @Override
    public void getAssessTargetAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) {
        String Id = request.getParameter("Id");
        String nodeState = request.getParameter("nodeState");
        if ("".equalsIgnoreCase(nodeState)) {
            nodeState = "0";
        }
        String evaluResultFlag = request.getParameter("evaluResultFlag");
        String evaluObjectType_result = request.getParameter("evaluObjectType_result");
        String solutionPeriodId = request.getParameter("solutionPeriodId");
        String longNumber = request.getParameter("longNumber");
        EntityViewInfo viewInfo = new EntityViewInfo();
        FilterInfo filter = new FilterInfo();
        ArrayList<AssessTargetData> data = new ArrayList<AssessTargetData>();
        try {
            boolean isForm = true;
            filter = CommonUtil.setFilter(filter, nodeState, evaluResultFlag, solutionPeriodId,
                    evaluObjectType_result, longNumber, isForm);
            if (!StringUtil.isEmptyString(Id)) {
                FilterInfo fi = new FilterInfo();
                fi.getFilterItems().add(new FilterItemInfo("id", Id, CompareType.EQUALS));
                filter.mergeFilter(fi, "and");
            }
            viewInfo.setFilter(filter);
            SorterItemCollection sorterItemColl = viewInfo.getSorter();
            SorterItemInfo sorterItemInfo = null;
            if ("true".equalsIgnoreCase(evaluResultFlag)) {
                sorterItemInfo = new SorterItemInfo("solutionPeriod.evaluStartDate");
                sorterItemInfo.setSortType(SortType.DESCEND);
                sorterItemColl.add(sorterItemInfo);
            } else {
                sorterItemInfo = new SorterItemInfo("acceptTime");
                sorterItemInfo.setSortType(SortType.DESCEND);
                sorterItemColl.add(sorterItemInfo);
            }
            sorterItemInfo = new SorterItemInfo("solutionPeriod.id");
            sorterItemInfo.setSortType(SortType.ASCEND);
            sorterItemColl.add(sorterItemInfo);
            String queryName = "";
            if ("1".equalsIgnoreCase(nodeState)) {
                queryName = "NewTargetTaskHasProcessedQuery";
            } else if ("0".equalsIgnoreCase(nodeState)) {
                queryName = "NewTargetTaskProcessQuery";
            } else if ("true".equalsIgnoreCase(evaluResultFlag)) {
                queryName = "101".equalsIgnoreCase(evaluObjectType_result) ? "NewOrgEvaluResultWebQuery"
                        : "NewPersonalPEvaluResultQuery";
            }
            IQueryExecutor iqec = QueryExecutorFactory
                    .getRemoteInstance(new MetaDataPK("com.kingdee.eas.hr.perf.app", queryName));
            iqec.setObjectView(viewInfo);
            IRowSet rowSet = iqec.executeQuery();
            AssessTargetData atdate = null;
            if (rowSet != null) {
                while (rowSet.next()) {
                    atdate = this.setAssessTargetForEvalu(atdate, rowSet, nodeState, evaluResultFlag);
                    data.add(atdate);
                }
            }
            JSONUtils.SUCCESS(data);
        } catch (BOSException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (SHRWebException e) {
            e.printStackTrace();
        }
    }

    private void deleteCurrentNodeProcesserInfo(Context ctx, String nodeProcesserid)
            throws EASBizException, BOSException {
        INodeProcesser iNodeProcesser = NodeProcesserFactory.getLocalInstance(ctx);
        FilterInfo filterInfo = new FilterInfo();
        filterInfo.getFilterItems().add(new FilterItemInfo("id", nodeProcesserid, CompareType.EQUALS));
        iNodeProcesser.delete(filterInfo);
    }

    @Override
    public void getNodeProcesserIdAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        this.assignmentId = request.getParameter("assignmentId");
        HashMap<String, String> result = new HashMap<String, String>();
        String sql = "select pnode.fid from T_PF_NodeProcesser pnode ,T_PF_EvaluNode enode,T_PF_EvalWorkFlow flow,T_PF_EvalWFObjects wfo,T_WFR_AssignDetail assde,T_WFR_ProcInst pro,t_pm_user user1 where pnode.FEvaluNodeID=enode.FID and enode.FEvalWorkFlowID=flow.FID and wfo.FEvalWorkFlowID=flow.FID and assde.FPROCINSTID=wfo.FProcInstID and assde.FPROCINSTID=pro.FPROCINSTID and pnode.FEvaluerID=user1.fpersonid and pro.FINITIATORID=user1.fid  and assde.FASSIGNID='"
                + this.assignmentId + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(this.ctx, sql);
            if (rowSet.next()) {
                result.put("nodeProcesserId", rowSet.getString("fid"));
            }
        } catch (BOSException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        JSONUtils.writeJson(response, result);
    }

    @Override
    public void getCurrentNodeLevelAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        this.assignmentId = request.getParameter("assignmentId");
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        this.initParam();
        result.put("nodeLevel", this.nodeParam.getCurrentNodeLevel());
        JSONUtils.writeJson(response, result);
    }

    @Override
    public void backTargetAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
            throws SHRWebException {
        this.assignmentId = request.getParameter("assignmentId");
        HashMap<String, String> result = new HashMap<String, String>();
        this.initParam();
        Context ctx = SHRContext.getInstance().getContext();
        PersonInfo person = ContextUtil.getCurrentUserInfo(ctx).getPerson();
        String message = "，打回原因:{0}。";
        String reBackReason = request.getParameter("reBackReason");
        Object[] variables = new Object[] { reBackReason };
        try {
            EvalWorkFlowFacadeFactory.getLocalInstance(ctx).reBack2(this.assignmentId, "",
                    MessageFormat.format(message, variables), this.nodeParam.getSolutionPeriodID(),
                    this.nodeParam.getEvaluObjectID(), this.nodeParam.getCurrentNodeLevel());
        } catch (Exception e) {
            result.put("opResult", "no");
            result.put("msg", "打回失败");
            JSONUtils.writeJson(response, result);
            e.printStackTrace();
            return;
        }
        String backTime = CommonUtil.date2string(new Date(), "yyyy-MM-dd HH:mm:ss");
        NodeProcesserInfo previousInfo = null;
        int size = this.datas.size();
        for (int i = 0; i < size; ++i) {
            previousInfo = (NodeProcesserInfo) this.datas.get(this.seqNumbers[i]);
            if (this.nodeParam.getCurrentNodeLevel() - 1 != previousInfo.getEvaluNode().getNodeLevel())
                continue;
            String oldcomment = previousInfo.getComment() == null ? "" : previousInfo.getComment();
            StringBuilder sb = new StringBuilder();
            sb.append(oldcomment);
            sb.append("\n");
            sb.append("上级节点打回信息:----------start-------------------------");
            sb.append("\n");
            sb.append("打回原因:");
            sb.append(reBackReason == null ? "" : reBackReason);
            sb.append("\n");
            sb.append("打回人:");
            sb.append(person.getName());
            sb.append("\n");
            sb.append("打回时间:");
            sb.append(backTime);
            sb.append("\n");
            sb.append("上级节点打回信息:----------end--------------------------");
            previousInfo.setComment(sb.toString());
            try {
                INodeProcesser iservice = NodeProcesserFactory.getRemoteInstance();
                IObjectPK pk = iservice.save(previousInfo);
                previousInfo.setId(BOSUuid.read(pk.toString()));
                continue;
            } catch (BOSException e) {
                result.put("opResult", "no");
                result.put("msg", "保存打回信息失败");
                JSONUtils.writeJson(response, result);
                e.printStackTrace();
                return;
            } catch (EASBizException e) {
                result.put("opResult", "no");
                result.put("msg", "保存打回信息失败");
                JSONUtils.writeJson(response, result);
                e.printStackTrace();
                return;
            }
        }
        try {
            NodeProcesserInfo nodeProcesserInfo = (NodeProcesserInfo) this.datas
                    .get(this.nodeParam.getNodeId());
            if (nodeProcesserInfo != null && nodeProcesserInfo.getId() != null) {
                this.deleteCurrentNodeProcesserInfo(ctx, nodeProcesserInfo.getId().toString());
            }
            if (this.proInstId == null || this.proInstId.trim().equals("")) {
                FilterInfo filterInfo = new FilterInfo();
                filterInfo.getFilterItems()
                        .add(new FilterItemInfo("evaluObject.id", this.nodeParam.getEvaluObjectID()));
                filterInfo.getFilterItems().add(new FilterItemInfo("evalWorkFlow.solutionPeriod.id",
                        this.nodeParam.getSolutionPeriodID()));
                EntityViewInfo viewInfo = new EntityViewInfo();
                viewInfo.setFilter(filterInfo);
                viewInfo.getSelector().add(new SelectorItemInfo("id"));
                viewInfo.getSelector().add(new SelectorItemInfo("procInstID"));
                EvalWFObjectsCollection collection = EvalWFObjectsFactory.getLocalInstance(ctx)
                        .getEvalWFObjectsCollection(viewInfo);
                if (collection == null || collection.size() == 0) {
                    return;
                }
                this.proInstId = collection.get(0).getProcInstID();
            }
        } catch (Exception e) {
            result.put("opResult", "no");
            result.put("msg", "删除当前节点信息失败");
            JSONUtils.writeJson(response, result);
            e.printStackTrace();
            return;
        }
        result.put("opResult", "ok");
        JSONUtils.writeJson(response, result);
        String sql = "update T_BAS_AssignRead set fsender_l2='" + person.getName()
                + "' ,FPriorPerformName_l2='" + person.getName()
                + "' where fassignid in (select fAssignid from T_WFR_Assign where FPROCINSTID ='"
                + this.proInstId + "' and fstate=1)";
        try {
            Thread.currentThread();
            Thread.sleep(500L);
            DbUtil.execute(ctx, sql);
        } catch (BOSException e) {
            result.put("opResult", "no");
            result.put("msg", " 更新处理人 信息失败");
            JSONUtils.writeJson(response, result);
            e.printStackTrace();
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getOptionSourceAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        ArrayList data = new ArrayList();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("value", 0);
        map.put("alias", "未提交");
        data.add(map);
        map = new HashMap();
        map.put("value", 1);
        map.put("alias", "已提交");
        data.add(map);
        this.writeSuccessData(data);
        return null;
    }
}
