package com.kingdee.shr.custom.handler.assesstarget;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.base.permission.UserInfo;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.onevote.OneVoteCollection;
import com.kingdee.eas.custom.onevote.OneVoteFactory;
import com.kingdee.eas.custom.onevote.OneVoteInfo;
import com.kingdee.eas.hr.emp.PersonPositionInfo;
import com.kingdee.eas.hr.perf.EvaluResultFactory;
import com.kingdee.eas.hr.perf.EvaluResultInfo;
import com.kingdee.eas.hr.perf.EvaluSolutionFactory;
import com.kingdee.eas.hr.perf.EvaluSolutionInfo;
import com.kingdee.eas.hr.perf.SolutionPeriodFactory;
import com.kingdee.eas.hr.perf.SolutionPeriodInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.affair.web.util.SHRBillUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.util.StringUtils;

/**
 * 
 * @copyright 天津金蝶软件有限公司 <br>
 *            Title: AssessTargetEvaluEditHandlerEx<br>
 *            Description: 个人绩效拓展与组织绩效
 * @author 贺军磊 Email:1617340211@qq.com
 * @date 2019-9-4
 * @readme 2020-04-01 相建彬扩展更改：更改继承关系
 */
public class AssessTargetEvaluEditHandlerEx extends AssessTargetEvaluEditHandlerExt {

    private Context ctx = SHRContext.getInstance().getContext();

    /**
     * *
     * <p>
     * Title: getUserOrgScoreAction
     * </p>
     * <p>
     * Description: 获取当前登录人部门的考核评分
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException 获取方案评估活动失败
     */
    public void getUserOrgScoreAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        String solutionId = request.getParameter("solutionId");// 方案评估活动ID
        if (StringUtils.isEmpty(solutionId)) {
            return;
        }
        Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
        map.put("adminOrgScore", getOrgScore(solutionId));
        JSONUtils.SUCCESS(map);
    }

    /**
     * 
     * <p>
     * Title: getOrgScore
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param solutionId
     * @return 获取当前登录人部门考核评分
     * @throws ShrWebBizException
     */
    private BigDecimal getOrgScore(String solutionId) throws ShrWebBizException {
        String orgSolutionId = getOrgSolutionId(solutionId);
        if (StringUtils.isEmpty(orgSolutionId)) {
            return null;
        }
        try {
            EvaluResultInfo info = EvaluResultFactory.getRemoteInstance().getEvaluResultInfo(
                    "select targetEvaluScore,modifiedScore where solutionPeriod.id='" + orgSolutionId + "' ");
            if (info.getModifiedScore() != null) {
                return info.getModifiedScore();
            } else {
                return info.getTargetEvaluScore();
            }
        } catch (EASBizException e) {
            throw new ShrWebBizException("获取部门考核评分失败！" + e);

        } catch (BOSException e) {
            throw new ShrWebBizException("获取部门考核评分失败！" + e);
        }
    }

    /**
     * <p>
     * Title: getOrgScore
     * </p>
     * <p>
     * Description: 获取部门方案评估活动id
     * </p>
     * 
     * @param solutionId 方案评估活动id
     * @return
     * @throws ShrWebBizException 获取考勤周期或当前登录人部门失败
     */
    private String getOrgSolutionId(String solutionId) throws ShrWebBizException {

        String assessPeriodId = getAssessPeriodID(solutionId);// 考核方案周期id
        String orgId = getAdminOrgId();
        if (StringUtils.isEmpty(orgId) || StringUtils.isEmpty(assessPeriodId)) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        sb.append("select sp.fid as id from  T_PF_EvaluObject eo left join T_PF_SolutionPeriod  sp ")
                .append(" on eo.fevalusolutionid =sp.fevalusolutionid  ")
                .append(" where  fevaluobjecttype='101' and sp.fstatus='102' ")
                .append("  and eo.fobjectid='" + orgId + "' and fAssessPeriodid='" + assessPeriodId + "' ");
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sb.toString());
            while (rowSet.next()) {
                return rowSet.getString("id");
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("获取方案评估活动失败！" + e);
        } catch (SQLException e) {
            throw new ShrWebBizException("获取方案评估活动失败！" + e);
        }
        return null;
    }

    /**
     * <p>
     * Title: getAssessPeriodID
     * </p>
     * <p>
     * Description: 获取考核周期id
     * </p>
     * 
     * @param solutionId 方案评估活动id
     * @return 方案评估活动id
     * @throws ShrWebBizException 获取考核周期失败
     */
    public String getAssessPeriodID(String solutionId) throws ShrWebBizException {

        try {
            SolutionPeriodInfo info = SolutionPeriodFactory.getRemoteInstance()
                    .getSolutionPeriodInfo(new ObjectUuidPK(solutionId));
            if (info.getAssessPeriod().getId() != null) {
                return info.getAssessPeriod().getId().toString();
            }
        } catch (EASBizException e) {
            throw new ShrWebBizException("获取考核周期失败！" + e);
        } catch (BOSException e) {
            throw new ShrWebBizException("获取考核周期失败！" + e);
        }
        return null;
    }

    /**
     * <p>
     * Title: getAdminOrgId
     * </p>
     * <p>
     * Description: 获取当前登录人部门ID
     * </p>
     * 
     * @return 部门id
     * @throws ShrWebBizException 获取当前登录人信息失败！或者 获取当前登录人部门失败！
     */
    public String getAdminOrgId() throws ShrWebBizException {
        UserInfo currentUserInfo = ContextUtil.getCurrentUserInfo(this.ctx);
        BOSUuid userId = currentUserInfo.getPerson().getId();
        if (userId == null) {
            throw new ShrWebBizException("获取当前登录人信息失败！");
        }
        PersonPositionInfo personPositionInfo = SHRBillUtil.getAdminOrgUnit(userId.toString());
        AdminOrgUnitInfo personDep = personPositionInfo.getPersonDep();
        if (personDep == null) {
            throw new ShrWebBizException("获取当前登录人部门失败！");
        }
        return personDep.getId().toString();
    }

    /**
     * 
     * <p>
     * Title: getEvaluTypeAction
     * </p>
     * <p>
     * Description: 获取考核方案类型
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     */
    public void getEvaluTypeAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        String id = request.getParameter("id");
        String evaluId = null;// 面试方案ID
        System.out.println("********************************************************入参id" + id);
        Map<String, String> map = new HashMap<String, String>();
        try {
            SolutionPeriodInfo info = SolutionPeriodFactory.getRemoteInstance()
                    .getSolutionPeriodInfo(new ObjectUuidPK(id));
            if (info != null) {
                evaluId = info.getEvaluSolution().getId().toString();
            }
            if (StringUtils.isEmpty(evaluId)) {
                throw new ShrWebBizException("获取考核方案ID失败！");
            }
            EvaluSolutionInfo evaluSolution = EvaluSolutionFactory.getRemoteInstance()
                    .getEvaluSolutionInfo(new ObjectUuidPK(evaluId));
            if (evaluSolution == null) {
                throw new ShrWebBizException("获取考核方案失败！");
            }
            int typeValue = evaluSolution.getSolutionType().getValue();
            System.out.println("********************************************获取考核类型值" + typeValue);
            if (typeValue == 100) {
                map.put("type", "person");
            }
            if (typeValue == 101) {
                map.put("type", "org");
                System.out.println("我执行了!");
            }
        } catch (EASBizException e) {
            throw new ShrWebBizException("获取考核方案类型失败！" + e);
        } catch (BOSException e) {
            throw new ShrWebBizException("获取考核方案类型失败！" + e);
        }
        JSONUtils.SUCCESS(map);

    }

    /**
     * 
     * <p>
     * Title: getRadioData
     * </p>
     * <p>
     * Description:获取一票否决单选框数据
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     */
    @SuppressWarnings("unchecked")
    public void getRadioDataAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        OneVoteInfo info = null;
        List list = new ArrayList();
        Map dataMap = new HashMap();
        try {
            OneVoteCollection oneVote = OneVoteFactory.getRemoteInstance().getOneVoteCollection();
            if (oneVote != null) {
                Iterator oneVoteIter = oneVote.iterator();
                while (oneVoteIter.hasNext()) {
                    Map map = new HashMap();
                    info = (OneVoteInfo) oneVoteIter.next();
                    map.put("name", info.getName());
                    map.put("number", info.getNumber());
                    map.put("readme", info.getDescription());
                    list.add(map);
                }
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("获取一票否决单选框失败！" + e);
        }
        if (list != null && list.size() > 0) {
            dataMap.put("radioData", list);
            JSONUtils.SUCCESS(dataMap);
        }

    }

    @Override
    public void submitTargetRealAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        super.submitTargetRealAction(request, response, modelMap);
        System.out.println("我是调用获取到的信息，如下:");
        String isOrg = request.getParameter("isOrg");// 是否组织考核标识
        String objectId = request.getParameter("objectId");// 考核对象
        String radio = request.getParameter("radioData");// 单选框值
        System.out.println("***************************入参isOrg" + isOrg);
        System.out.println("***************************入参solutionId" + objectId);
        System.out.println("***************************入参radio" + radio);
        if (StringUtils.isEmpty(isOrg) || StringUtils.isEmpty(objectId)) {
            throw new ShrWebBizException("判断是否组织考核失败！");
        }
        if (StringUtils.isEmpty(radio)) {
            return;
        }
        if ("true".equals(isOrg)) {
            String sql = "update T_PF_EvaluObject set cfonevote='" + radio + "'where fid='" + objectId + "'";
            try {
                DbUtil.execute(ctx, sql);
            } catch (BOSException e) {
                throw new ShrWebBizException("保存一票否决数据失败！");
            }
        }
    }

    @Override
    public void saveTargetFormAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        super.saveTargetFormAction(request, response, modelMap);
        String isOrg = request.getParameter("isOrg");// 是否组织考核标识
        String objectId = request.getParameter("objectId");// 考核对象
        String radio = request.getParameter("radioData");// 单选框值
        System.out.println("***************************入参isOrg" + isOrg);
        System.out.println("***************************入参solutionId" + objectId);
        System.out.println("***************************入参radio" + radio);
        if (StringUtils.isEmpty(isOrg) || StringUtils.isEmpty(objectId)) {
            throw new ShrWebBizException("判断是否组织考核失败！");
        }
        if (StringUtils.isEmpty(radio)) {
            return;
        }
        if ("true".equals(isOrg)) {
            String sql = "update T_PF_EvaluObject set cfonevote='" + radio + "'where fid='" + objectId + "'";
            try {
                DbUtil.execute(ctx, sql);
            } catch (BOSException e) {
                throw new ShrWebBizException("保存一票否决数据失败！");
            }
        }

    }

    /**
     * 
     * <p>
     * Title: getRadioCheckedAction
     * </p>
     * <p>
     * Description:根据方案评估活动Id获取单选框中是否有值
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     */
    public void getRadioCheckedAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        String objectId = request.getParameter("objectId");// 方案评估活动Id
        Map<String, String> map = new HashMap<String, String>();
        String radioid = "null";

        if (StringUtils.isEmpty(objectId)) {
            throw new ShrWebBizException("评估对象ID为空！");
        }

        String sql = "select cfonevote from T_PF_EvaluObject where fid='" + objectId + "'";
        try {
            IRowSet rowSet = DbUtil.executeQuery(ctx, sql);
            while (rowSet.next()) {
                radioid = rowSet.getString("cfonevote");
            }
        } catch (BOSException e) {
            throw new ShrWebBizException("根据评估对象ID查询一票否决数据失败！");
        } catch (SQLException e) {
            throw new ShrWebBizException("根据评估对象ID查询一票否决数据失败！");
        }
        map.put("radioid", radioid);
        JSONUtils.SUCCESS(map);
    }

}
