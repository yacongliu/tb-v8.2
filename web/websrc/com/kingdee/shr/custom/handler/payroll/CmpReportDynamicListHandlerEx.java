package com.kingdee.shr.custom.handler.payroll;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.bos.sql.ParserException;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.compensation.web.handler.CmpReportDynamicListHandler;
import com.kingdee.util.StringUtils;

/**
 * @description 员工薪酬查询handler扩展
 * @title CmpReportDynamicListHandlerEx
 * @copyright 天津金蝶软件有限公司
 * @author Jambin Email:tjxiangjianbin@kingdee.com
 * @date 2020年3月4日
 */
public class CmpReportDynamicListHandlerEx extends CmpReportDynamicListHandler {

    /**
     * @description 屏蔽计算规则和组织过滤条件
     * @title assembleFilterInfo
     * @param request
     * @param params
     * @param filterCondition
     * @param effectDate
     * @return
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年3月6日
     */
    @Override
    protected FilterInfo assembleFilterInfo(HttpServletRequest request, Map<String, String> params,
            Map filterCondition, Date effectDate) throws SHRWebException {
        FilterInfo filterInfo = new FilterInfo();
        String filter = params.get("filter");
        if (!StringUtils.isEmpty(filter)) {
            try {
                filterInfo = new FilterInfo(filter);
            } catch (ParserException e) {
                throw new SHRWebException(e.getMessage(), e);
            }
        }
        FilterInfo quickFilter = null;
        FilterInfo settingFilter = null;
        Map tree_params = null;
        if (filterCondition != null) {
            quickFilter = convertQuickFilterInfo((String) filterCondition.get("quickFilter"));
            settingFilter = convertFilterInfo((String) filterCondition.get("settingFilter"));
            tree_params = (Map) filterCondition.get("tree_params");
        }
        try {
            FilterInfo defaultFilterInfo = getDefaultFilter(request, null);
            FilterInfo domainFilterInfo = getDomainFilter(request);
            FilterInfo treeDomainFilter = getTreeDomainFilter(request, tree_params);
            FilterInfo effectDateFilter = getEffectDateFilter(effectDate);
            FilterInfo calSchemeFilter = getCalSchemeFilter(request, filterCondition);

            filterInfo.mergeFilter(defaultFilterInfo, "AND");
            filterInfo.mergeFilter(domainFilterInfo, "AND");
            filterInfo.mergeFilter(quickFilter, "AND");
            filterInfo.mergeFilter(settingFilter, "AND");
            filterInfo.mergeFilter(effectDateFilter, "AND");
            // filterInfo.mergeFilter(treeDomainFilter, "AND");
            // filterInfo.mergeFilter(calSchemeFilter, "AND");
        } catch (BOSException e) {
            throw new SHRWebException(e);
        }
        return filterInfo;
    }

    /**
     * @description 根据员工id设置过滤条件
     * @title getDefaultFilter
     * @param request
     * @param response
     * @return filterInfo
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年3月5日
     */
    @Override
    protected FilterInfo getDefaultFilter(HttpServletRequest request, HttpServletResponse response)
            throws SHRWebException {
        String personId = (String) getRequestParameter(request, "billId");
        System.out.println("获取id:" + personId);
        FilterInfo filterInfo = new FilterInfo();
        filterInfo.getFilterItems().add(new FilterItemInfo("cmp_person.id", personId, CompareType.EQUALS));
        return filterInfo;
    }
    /**
     * 
     * @description 计算规则过滤
     * @title getCalSchemeFilter
     * @param request
     * @param filterCondition
     * @return
     * @throws SHRWebException
     * @author Jambin
     * @date 2020年3月5日
     */

    /*
     * @Override protected FilterInfo getCalSchemeFilter(HttpServletRequest request, Map
     * filterCondition) throws SHRWebException {
     * System.out.println("********************员工薪酬查询计算规则过滤****"); FilterInfo filter = new
     * FilterInfo(); String scheme = null == filterCondition ? "001" : (String)
     * filterCondition.get("infoSetScheme"); if ((null == scheme) || (!"002".equals(scheme))) {
     * return null; } String userCalSchemeRange = util.getUserCalSchemeRange();
     * System.out.println("*******userCalSchemaRange_befor: " + userCalSchemeRange);
     * userCalSchemeRange = "select Fid from T_HR_SCalScheme";
     * System.out.println("*******userCalSchemaRange_after: " + userCalSchemeRange);
     * filter.getFilterItems() .add(new FilterItemInfo("cmp_CTB.calScheme.id", userCalSchemeRange,
     * CompareType.INNER)); return filter; }
     */

}
