/**
 * Title: ClearHolidayLimitListHandler.java
 * Description: 
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019-8-5
 * @version 1.0
 */
package com.kingdee.shr.custom.handler.holiday;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.ModelMap;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.web.handler.ListHandler;

/**
 * 
 * @copyright 天津金蝶软件有限公司 <br>
 *            Title: ClearHolidayLimitListHandler <br>
 *            Description:
 * @author 相建彬 Email:tjxiangjianbin@kingdee.com
 * @date 2019年8月15日
 */
public class ClearHolidayLimitListHandler extends ListHandler {
    private Context ctx;

    public ClearHolidayLimitListHandler(Context ctx) {
        this.ctx = ctx;
    }

    public ClearHolidayLimitListHandler() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * 
     * <p>
     * Title: clearHoliday
     * </p>
     * <p>
     * Description:清除假期额度
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @throws SHRWebException
     */
    public void clearHolidayAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        String ids = request.getParameter("billId");
        String[] billIds = ids.split(",");

        for (int i = 0; i < billIds.length; i++) {
            // 清除额度
            String sql1 = " update T_HR_ATS_HolidayLimit set FStandardLimit = '0', FRealLimit = '0', FUsedLimit= '0', FRemainLimit= '0' where FID = '"
                    + billIds[i] + "'";
            try {
                DbUtil.execute(ctx, sql1);
            } catch (BOSException e) {
                throw new SHRWebException("清除额度失败" + e);
            }
        }

        Map<String, String> map = new HashMap<String, String>(1);
        map.put("state", "success");
        JSONUtils.writeJson(response, map);
    }
}
