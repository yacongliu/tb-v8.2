package com.kingdee.shr.perf.web.handler.peopleSelf;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kingdee.bos.Context;
import com.kingdee.bos.ctrl.extcommon.server.DbUtil;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.util.app.ContextUtil;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.json.GridDataEntity;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: MDassessTargetEvaluListHandlerEx <br>
 *            Description: 年度民主测评评分列表
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-3-5 & 下午01:26:46
 * @since V1.0
 */
public class MDassessTargetEvaluListHandlerEx extends MDassessTargetEvaluListHandler {

    /**
     * （非 Javadoc）
     * <p>
     * Title: afterGetListData
     * </p>
     * <p>
     * Description: 操作列表请求数据，每个考核方案只保留一条
     * </p>
     * 
     * @param request
     * @param response
     * @param gridDataEntity
     * @throws SHRWebException
     * @see com.kingdee.shr.base.syssetting.web.handler.ListHandler#afterGetListData(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse,
     *      com.kingdee.shr.base.syssetting.json.GridDataEntity)
     */
    @Override
    protected void afterGetListData(HttpServletRequest request, HttpServletResponse response,
            GridDataEntity gridDataEntity) throws SHRWebException {
        Set<String> set = new HashSet<String>(8);

        long records = gridDataEntity.getRecords();
        List rows = gridDataEntity.getRows();
        List nowRows = new ArrayList<Object>(8);

        Iterator iterator = rows.iterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            String json = JSONObject.toJSONString(object);
            JSONObject parseObject = JSONObject.parseObject(json);
            // 考核方案Id
            String solutionPeriodId = parseObject.getString("mdSolutionPeriod.id");

            if (set.contains(solutionPeriodId)) {
                // 去掉同一个考核方案的多条记录
                iterator.remove();
            } else {
                // 截取名称，去掉期间
                String solutionPeriodName = parseObject.getString("mdSolutionPeriod.name");
                String[] split = solutionPeriodName.split(" ");
                String name = split[0];
                parseObject.put("mdSolutionPeriod.name", name);
                nowRows.add(JSON.parse(parseObject.toJSONString()));

                set.add(solutionPeriodId);
            }
        }

        gridDataEntity.getRows().clear();
        gridDataEntity.setRows(nowRows);
        gridDataEntity.setRecords(set.size());

        super.afterGetListData(request, response, gridDataEntity);
    }

    /**
     * （非 Javadoc）
     * <p>
     * Title: getMDSolutionPeriodsAction
     * </p>
     * <p>
     * Description: 考核活动-> 去除考核方案的期间
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws SHRWebException
     * @see com.kingdee.shr.perf.web.handler.peopleSelf.MDassessTargetEvaluListHandler#getMDSolutionPeriodsAction(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse, org.springframework.ui.ModelMap)
     */
    @Override
    public String getMDSolutionPeriodsAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException {
        Context ctx = SHRContext.getInstance().getContext();
        PersonInfo person = ContextUtil.getCurrentUserInfo(ctx).getPerson();
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT DISTINCT SP.FName_l2 AS NAME, SP.FID AS ID");
        sql.append("   FROM T_PF_EvalPerson P ");
        sql.append("  INNER JOIN T_PF_EvalReDetail RD ");
        sql.append("     ON P.FEvalReDetailID = RD.FID ");
        sql.append("  INNER JOIN T_PF_MDEvaluator E ");
        sql.append("     ON RD.FMDEvaluatorID = E.FID ");
        sql.append("  INNER JOIN T_PF_MDSolutionPeriod SP ");
        sql.append("     ON E.FSolutionPeriodID = SP.FID ");
        sql.append("   LEFT JOIN T_PF_MultiDimentionSolution S ");
        sql.append("     ON SP.FMDSolutionID = S.FID ");
        sql.append("  WHERE P.FPERSONID = ? ");
        sql
                .append("  AND ( (P.FPaperState = ? AND SP.FSPStatuEnum = ? AND S.FMultiDimntModeE <> ? AND E.FAuditState = ? ) ");
        sql.append(" OR P.FPaperState = ? ) ");
        Object[] params = { person.getId().toString(), Integer.valueOf(10), Integer.valueOf(101),
                Integer.valueOf(100), Integer.valueOf(80), Integer.valueOf(20) };
        try {
            IRowSet rows = DbUtil.executeQuery(ctx, sql.toString(), params);
            List solutionPeriods = new ArrayList();
            Map solutionPeriod = new HashMap();
            solutionPeriod.put("value", "");
            solutionPeriod.put("alias", "所有");
            solutionPeriods.add(solutionPeriod);
            while (rows.next()) {
                solutionPeriod = new HashMap();
                solutionPeriod.put("value", rows.getString("id"));
                // 对方案名称进行截取，去除期间
                String name = rows.getString("name");
                String[] str = name.split(" ");
                solutionPeriod.put("alias", str[0]);
                solutionPeriods.add(solutionPeriod);
            }
            JSONUtils.SUCCESS(solutionPeriods);
            return null;
        } catch (SQLException e) {
            throw new SHRWebException(e);
        }
    }

}
