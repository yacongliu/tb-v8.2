package com.kingdee.shr.dataMapHelper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
//import org.springframework.stereotype.Service;

import com.kingdee.shr.entity.HRPersonal;
import com.kingdee.shr.entity.HRTransferForm4Cadre;
import com.kingdee.shr.utils.ImgUtils;


/**
 * Title: TestDataMaker
 * Description: 模拟数据访问层生成用于测试的数据实体类实例
 * @author fei_tong Email:1076928846@qq.com
 * @date 2019-3-26
 */
public class TestDataMaker {
	Logger log = Logger.getLogger(TestDataMaker.class);
	
	private TestDataMaker(){
		if(TestDataMakerHolder.testDataMaker!=null){
			log.error("TestDataMaker类单例已存在，不能再实例化");
			throw new IllegalStateException();
		}
	}
	
	public static  TestDataMaker getTestDataMaker(){
		return TestDataMakerHolder.testDataMaker;
	}
	
	private static class TestDataMakerHolder{
		private static TestDataMaker testDataMaker = new TestDataMaker();
	}
	
	
	/**生成干部任免审批表的实体类测试实例的方法
	 * @param callChain
	 * @return
	 * @throws IOException 
	 */
	public HRTransferForm4Cadre getTestCadreInstance(String callChain) {
		callChain += "生成干部任免审批表的实体类测试实例的方法：";
		
		HRTransferForm4Cadre t = new HRTransferForm4Cadre();
		t.setName("杨晨");
		t.setGender("男");
		Date bd = new Date();
		String birthday = "1983.06";
//		bd = yearMonthStringToDate(birthday,callChain);
		t.setBirthDay(birthday);
		t.setEthnicity("回族");
		t.setNativePlace("河北博野");
		t.setHomePlace("天津");
		String imagePath = this.getClass().getClassLoader().getResource("").getPath()+"com/kingdee/shr/imgs/officerTrans.PNG";
		log.info("path:"+imagePath);
		String imageBase64=null;
		try {
			imageBase64=ImgUtils.getImgBase64Code(imagePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block e.printStackTrace();
			log.error(callChain+"生成照片的Base64码字符串的程序异常："+e);
//			throw e;
		}
//		System.out.println(imageBase64);
		t.setImageBase64(imageBase64);
		t.setJoinPartyTime("2005.11");
		t.setStartWorkTime("2006.06");
		t.setHealthCondition("健康");
		t.setProfession("经济师");
		t.setEducationLevel("研究生");
		t.setEducationTitle("工商管理硕士");
		t.setInstitution("中国政法大学商学院");
		t.setMajor("工商管理");
		t.setCurrentDuty("天保控股公司人力资源部干部与员工关系主管");
		
		List<Map<String, String>> vitae= new ArrayList<Map<String, String>>(20);

		Map<String, String> map1 = new HashMap<String, String>(3);
		Map<String, String> map2 = new HashMap<String, String>(3);
		Map<String, String> map3 = new HashMap<String, String>(3);
		Map<String, String> map4 = new HashMap<String, String>(3);
		Map<String, String> map5 = new HashMap<String, String>(3);
		Map<String, String> map6 = new HashMap<String, String>(3);
		Map<String, String> map7 = new HashMap<String, String>(3);
		Map<String, String> map8 = new HashMap<String, String>(3);
		Map<String, String> map9 = new HashMap<String, String>(3);
		Map<String, String> map10 = new HashMap<String, String>(3);
		Map<String, String> map11 = new HashMap<String, String>(3);
		Map<String, String> map12 = new HashMap<String, String>(3);
		Map<String, String> map13 = new HashMap<String, String>(3);
		Map<String, String> map14 = new HashMap<String, String>(3);

		map1.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map2.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map3.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map4.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map5.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map6.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map7.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map8.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map9.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map10.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map11.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map12.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map13.put("content", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map14.put("content", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");

		vitae.add(map1 );
		vitae.add(map2 );
		vitae.add(map3 );
		vitae.add(map4 );
		vitae.add(map5 );
		vitae.add(map6 );
		vitae.add(map7 );
		vitae.add(map8 );
		vitae.add(map9 );
		vitae.add(map10);
		vitae.add(map11);
		vitae.add(map12);
		vitae.add(map13);
		vitae.add(map14);

		t.setVitae(vitae);
		
		List<Map<String,String>> rewardPenalty=new ArrayList<Map<String,String>>(3);
		Map<String,String> mapRAP1= new HashMap<String,String>(2);
		Map<String,String> mapRAP2= new HashMap<String,String>(2);
		
		mapRAP1.put("content", "2016.01  获得2014-2015年度天保控股公司重点工作(项目)突出贡献者");
		mapRAP2.put("content", "2016.04  获得2015年度滨海新区五一劳动奖章");
		rewardPenalty.add(mapRAP1);
		rewardPenalty.add(mapRAP2);
		
		t.setRewardAndPenalty(rewardPenalty);
		
		
		List<Map<String, String>> annualResult = new ArrayList<Map<String,String>>(5);
		
		Map<String, String> mapA1 = new HashMap<String,String>(2);
		Map<String, String> mapA2 = new HashMap<String,String>(2);
		Map<String, String> mapA3 = new HashMap<String,String>(2);
		
		mapA1.put("content", "2018-优");
		mapA2.put("content", "2017-良");
		mapA3.put("content", "2016-中");
		
		annualResult.add(mapA1); annualResult.add(mapA2); annualResult.add(mapA3);
		t.setAnnualResult(annualResult);
		
		
		List<Map<String, String>> familyRelatives = new ArrayList<Map<String, String>>(12);

		Map<String, String> mapQ1 = new HashMap<String, String>(7);
		Map<String, String> mapQ2 = new HashMap<String, String>(7);
		Map<String, String> mapQ3 = new HashMap<String, String>(7);
		Map<String, String> mapQ4 = new HashMap<String, String>(7);

		mapQ1.put("relationship", "妻子");
		mapQ1.put("name", "孙红梅");
		mapQ1.put("age", "35");
		mapQ1.put("party", "中共党员");
		mapQ1.put("job", "天津市公安局天塔派出所警长");
		
		mapQ2.put("relationship", "女儿");
		mapQ2.put("name", "杨昱萱");
		mapQ2.put("age", "5");
		mapQ2.put("party", "中共党员");
		mapQ2.put("job", "天津市公安局天塔派出所警长");

		mapQ3.put("relationship", "父亲");
		mapQ3.put("name", "杨震宇");
		mapQ3.put("age", "65");
		mapQ3.put("party", "中共党员");
		mapQ3.put("job", "天津市公安局天塔派出所警长");
		
		mapQ4.put("relationship", "母亲");
		mapQ4.put("name", "张红新");
		mapQ4.put("age", "65");
		mapQ4.put("party", "中共党员");
		mapQ4.put("job", "天津市公安局天塔派出所警长");

		familyRelatives.add(mapQ1);
		familyRelatives.add(mapQ2);
		familyRelatives.add(mapQ3);
		familyRelatives.add(mapQ4);
		
		t.setFamilyAndRelatives(familyRelatives);
		
		t.setApplyingDate("2019.1.1");
		t.setApplyingOffice("");
		
		return t;
	}
	

	/**将yyyy.MM.dd格式的年月日字符串解析成Date对象返回的方法
	 * @param dateStr
	 * @param callChain
	 * @return
	 */
	public Date yearMonthDayStringToDate(String dateStr, String callChain){
		callChain += "将yyyy.MM.dd格式的年月日字符串解析成Date对象返回的方法（参数：" + dateStr + "）："; 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		Date d=null;
		try {
			d=sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.error(callChain+"解析日期字符串异常："+e);
		}
		return d;
	}
	
	/**将yyyy.MM格式的年月字符串解析成Date对象返回的方法
	 * @param dateStr
	 * @param callChain
	 * @return
	 */
	public Date yearMonthStringToDate( String dateStr, String callChain) {
		callChain += "将yyyy.MM格式的年月字符串解析成Date对象返回的方法（参数：" + dateStr + "）："; 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM");
		Date d=null;
		try {
			d=sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.error(callChain+"解析日期字符串异常："+e);
		}
		return d;
	}
	
	/**将Date对象转换为yyyy年MM月dd日格式的年月字符串返回的方法
	 * @param date
	 * @param callChain
	 * @return
	 */
	public String dateToCHYearMonthDayString(Date date, String callChain){
		callChain += "将Date对象转换为yyyy年MM月dd日格式的年月字符串返回的方法：";
		if(date==null){
			log.error(callChain+"Date对象参数为null");
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		String dateStr = sdf.format(date);
		return dateStr;
	}
	
	/**将Date对象转换为yyyy.MM格式的年月字符串返回的方法
	 * @param date
	 * @param callChain
	 * @return
	 */
	public String dateToYearMonthString(Date date, String callChain){
		callChain += "将Date对象转换为yyyy.MM格式的年月字符串返回的方法：";
		if(date==null){
			log.error(callChain+"Date对象参数为null");
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM");
		String dateStr = sdf.format(date);
		return dateStr;
	}
	
	public Map<String, Object> MakeDataMap_officer_template() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "杨晨");

		List<Map<String, String>> list = new ArrayList<Map<String, String>>(3);

		Map<String, String> map1 = new HashMap<String, String>(3);
		Map<String, String> map2 = new HashMap<String, String>(3);
		Map<String, String> map3 = new HashMap<String, String>(3);
		Map<String, String> map4 = new HashMap<String, String>(3);
		Map<String, String> map5 = new HashMap<String, String>(3);
		Map<String, String> map6 = new HashMap<String, String>(3);
		Map<String, String> map7 = new HashMap<String, String>(3);
		Map<String, String> map8 = new HashMap<String, String>(3);
		Map<String, String> map9 = new HashMap<String, String>(3);
		Map<String, String> map10 = new HashMap<String, String>(3);
		Map<String, String> map11 = new HashMap<String, String>(3);
		Map<String, String> map12 = new HashMap<String, String>(3);
		Map<String, String> map13 = new HashMap<String, String>(3);
		Map<String, String> map14 = new HashMap<String, String>(3);

		map1.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map2.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map3.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map4.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map5.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map6.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map7.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map8.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map9.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map10.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map11.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map12.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");
		map13.put("jl", "2002.09－2006.06 天津科技大学经济管理学院信息管理与信息系统专业、法学院法学专业学生");
		map14.put("jl", "2006.06－2009.02 北京索爱普天移动通信有限公司人力资源部人事管理");

		list.add(map1 );
		list.add(map2 );
		list.add(map3 );
		list.add(map4 );
		list.add(map5 );
		list.add(map6 );
		list.add(map7 );
		list.add(map8 );
		list.add(map9 );
		list.add(map10);
		list.add(map11);
		list.add(map12);
		list.add(map13);
		list.add(map14);

		int jlsize=list.size();
		double rate = jlsize/10;
		double size_d=(28/rate);
		int size=(int)size_d;
		map.put("size", size);
		
		map.put("columns", list);

		List<Map<String, String>> objList = new ArrayList<Map<String, String>>(12);

		Map<String, String> mapQ1 = new HashMap<String, String>(7);
		Map<String, String> mapQ2 = new HashMap<String, String>(7);
		Map<String, String> mapQ3 = new HashMap<String, String>(7);
		Map<String, String> mapQ4 = new HashMap<String, String>(7);

		mapQ1.put("q", "妻子");
		mapQ1.put("name", "孙红梅");
		mapQ1.put("age", "35");
		mapQ1.put("party", "中共党员");
		mapQ1.put("job", "天津市公安局天塔派出所警长");
		
		mapQ2.put("q", "女儿");
		mapQ2.put("name", "杨昱萱");
		mapQ2.put("age", "5");
		mapQ2.put("party", "中共党员");
		mapQ2.put("job", "天津市公安局天塔派出所警长");

		mapQ3.put("q", "父亲");
		mapQ3.put("name", "杨震宇");
		mapQ3.put("age", "65");
		mapQ3.put("party", "中共党员");
		mapQ3.put("job", "天津市公安局天塔派出所警长");
		
		mapQ4.put("q", "母亲");
		mapQ4.put("name", "张红新");
		mapQ4.put("age", "65");
		mapQ4.put("party", "中共党员");
		mapQ4.put("job", "天津市公安局天塔派出所警长");

		objList.add(mapQ1);
		objList.add(mapQ2);
		objList.add(mapQ3);
		objList.add(mapQ4);
		objList.add(mapQ1);
		objList.add(mapQ2);
		objList.add(mapQ3);
		objList.add(mapQ4);
		objList.add(mapQ1);
		objList.add(mapQ2);
		objList.add(mapQ3);
		objList.add(mapQ4);

		map.put("cls", objList);

		return map;
	}

	public HRPersonal getTestPersonalInstance(String callChain){
		callChain += "生成个人基本信息实体类测试实例";
		HRPersonal p = new HRPersonal();
		
		p.setName("杨晨");
		p.setGender("男");
		p.setBirthYear("1983");
		p.setBirthMonth("06");
		p.setEthnicity("回族");
		p.setHomePlace("天津");
		p.setJoinPartyYear("2005");
		p.setJoinPartyMonth("11");
		p.setStartWorkYear("2006");
		p.setStartWorkMonth("06");
		p.setEducationLevel("研究生");
		p.setEducationTitle("工商管理硕士");
		p.setMajor("中国政法大学商学院工商管理专业");
		p.setProfession("经济师");
		p.setCompany("天保控股公司");
		p.setPosition("人力资源部干部与员工关系主管");
		
		List<Map<String,String>> vitae = new ArrayList<Map<String,String>>(8);
		
		Map<String,String> map1 = new HashMap<String, String>();
		Map<String,String> map2 = new HashMap<String, String>();
		Map<String,String> map3 = new HashMap<String, String>();
		Map<String,String> map4 = new HashMap<String, String>();
		Map<String,String> map5 = new HashMap<String, String>();
		
		map1.put("indent", "196");
		map1.put("content", "19××.0×－19××.07  ××学校××专业学生；");
		
		map2.put("indent", "196");
		map2.put("content", "19××.0×－19××.05 公司技术员；");
		
		map3.put("indent", "196");
		map3.put("content", "19××.0×20××.05××公司技术员（期间：");
		
		map4.put("indent", "2011");
		map4.put("content", "20××.09－20××.07 天津××学院××专业大专班学习;20××.03－20××.01 天津××大学××专业专接本班学习）；");
		
		map5.put("indent", "196");
		map5.put("content", "20××.06－>××公司××部副部长。");
		
		vitae.add(map1); vitae.add(map2); vitae.add(map3); vitae.add(map4); vitae.add(map5);
		vitae.add(map1); vitae.add(map2); vitae.add(map3); vitae.add(map4); vitae.add(map5);
		
		p.setVitae(vitae);
		
		return p;
	}
}
