package com.kingdee.shr.dataMapHelper;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kingdee.shr.entity.HRPersonal;
import com.kingdee.shr.entity.HRTransferForm4Cadre;


/**
 * Title: MapWork
 * <p>
 * Description: 将给定的实体类翻译成对应的freemarker模板的Map对象，进行实体类null值字段的替换、模板字段（如字体大小）的计算等处理
 * 
 * @author fei_tong
 * @date 
 * @since V1.0
 */
public class MapWork {
	
	private Logger log=Logger.getLogger(MapWork.class);
	
	private MapWork(){
		if(MapWorkHolder.mapWork!=null){
			log.error("MapWork类单例已存在，不能再实例化");
		    throw new IllegalStateException();
		}
	}
	
	public static MapWork getMapWork(){
		return MapWorkHolder.mapWork;
	}
	
	private static class MapWorkHolder{
		private static MapWork mapWork = new MapWork();
	}
	
	/**
	 * 自动映射HRTransferForm4Cadre实体类到freemarker模板map中去的方法：
	 * @param cadre
	 * @param callChain
	 * @return
	 */
	public Map<String, Object > AutoMap(HRTransferForm4Cadre cadre, String callChain){
		if(callChain==null)callChain="";
		callChain += "自动映射HRTransferForm4Cadre实体类到freemarker模板map中去的方法：";
		if(cadre==null){
			callChain+="实体类cadre为null";log.error(callChain);return null;
		}
		
		EntityHelper eh= EntityHelper.getEntityHelper();
		
		cadre=(HRTransferForm4Cadre)eh.transferStringFieldsFromNullToEmpty(cadre, callChain);
		if(cadre==null){
			callChain+="自动转化实体类中空字符成员的方法执行出错：请在日志中查看详细错误信息";
			log.error(callChain);
			return null;
		}
		Map<String, Object> map= new HashMap<String,Object>();
		
		
		//简历单元格字体大小计算
		Integer vitaeCharSize = 
			getRealCharSizeByIdealListAndCharSize(
				10, 28, cadre.getVitae().size(), callChain);
		if(vitaeCharSize==null)vitaeCharSize=28;
		cadre.setVitaeCharSize(vitaeCharSize+"");
		
		//全日制学历学位单元格字体大小计算
		Integer eduTitleLen = cadre.getEducationTitle().length();
		Integer eduTitleSize =
			getRealCharSizeByIdealCharCount(8, 28, eduTitleLen, callChain);
		if(eduTitleSize==null)eduTitleSize=28;
		cadre.setEducationCharSize(eduTitleSize+"");
		
		//在职教育学历学位单元格字体大小计算
		Integer vacTitleLen = cadre.getVacationalTitle().length();
		Integer vacTitleSize =
			getRealCharSizeByIdealCharCount(8, 28, vacTitleLen, callChain);
		if(vacTitleSize==null)vacTitleSize=28;
		cadre.setVacationalCharSize(vacTitleSize+"");
		
		//全日制院校专业单元格字体大小计算
		Integer majorLen = cadre.getMajor().length();
		Integer majorSize =
			getRealCharSizeByIdealCharCount(22, 28, majorLen, callChain);
		if(majorSize==null)majorSize=28;
		cadre.setMajorCharSize(majorSize+"");
		
		//在职教育院校专业单元格字体大小计算
		Integer vacationalMajorLen = cadre.getVacationalMajor().length();
		Integer vacationalMajorSize = 
			getRealCharSizeByIdealCharCount(8, 28, vacationalMajorLen, callChain);
		if(vacationalMajorSize==null)vacationalMajorSize=28;
		cadre.setVacationalMajorCharSize(vacationalMajorSize+"");
		
		//实体映射
		map.put("cadre", cadre);
		
		return map;
	}
	
	/**
	 * 自动映射HRPersonal实体类到freemarker模板map中去的方法：";
	 * @param entity
	 * @param callChain
	 * @return
	 */
	public Map<String, Object > AutoMap(HRPersonal entity, String callChain){
		if(callChain==null)callChain="";
		callChain += "自动映射HRPersonal实体类到freemarker模板map中去的方法：";
		if(entity==null){
			callChain+="HRPersonal实体类参数为null";log.error(callChain);return null;
		}
		EntityHelper eh= EntityHelper.getEntityHelper();
		
		entity=(HRPersonal)eh.transferStringFieldsFromNullToEmpty(entity, callChain);
		if(entity==null){
			callChain+="自动转化实体类中空字符成员的方法执行出错：请在日志中查看详细错误信息";
			log.error(callChain);
			return null;
		}
		
		Map<String, Object> map= new HashMap<String,Object>();
		
		map.put("entity", entity);
		
		return map;
	}
	
	public Integer getRealCharSizeByIdealCharCount(
			Integer idealCharCount,Integer idealCharSize,Integer realCharCount, String callChain){
		if(callChain==null)callChain="";
		callChain += "根据单元格的理想字数和理想字体大小，以及真实字数，来计算真实字体大小的方法：";
		if(idealCharCount==null){
			callChain +="理想字数为null";log.error(callChain);return null;
		}
		if(idealCharSize==null){
			callChain +="理想字体大小为null";log.error(callChain);return null;
		}
		if(realCharCount==null){
			callChain +="真实字数为null";log.error(callChain);return null;
		}
		
		//如果实际字数小于理想字数，则直接返回理想字体大小
		if(realCharCount<idealCharCount){
			return idealCharSize;
		}
		if(idealCharCount==0){
			callChain +="理想字数为0";log.error(callChain);return null;
		}
		if(idealCharSize==0){
			callChain +="理想字体大小为0";log.error(callChain);return null;
		}
		if(realCharCount==0){
			callChain +="真实字数为0";log.error(callChain);return null;
		}
		
		//计算公式为：实际字体大小=理想字体大小*理想字数/实际字数
		float area = idealCharCount*idealCharSize;
		float size_d=(area/(float)realCharCount);
		int size=(int)size_d;
		size++;//根据实际效果进行微调

		return size;
	}

	/**
	 * 根据列表的理想记录数和理想字体大小，以及真实记录数，来计算真实字体大小
	 * @param idealListSize
	 * @param idealCharSize
	 * @param realListSize
	 * @param callChain
	 * @return
	 */
	public Integer getRealCharSizeByIdealListAndCharSize(
			Integer idealListSize, Integer idealCharSize, Integer realListSize, String callChain){
		if(callChain==null)callChain="";
		callChain += "根据列表的理想记录数和理想字体大小，以及真实记录数，来计算真实字体大小的方法：";
		if(idealListSize==null){
			callChain+="理想记录数为null"; log.error(callChain); return null;
		}
		if(idealCharSize==null){
			callChain+="理想字体大小为null"; log.error(callChain); return null;
		}
		if(realListSize==null){
			callChain+="真实记录数为null"; log.error(callChain); return null;
		}
		
		//如果实际记录数小于理想记录数，则直接返回理想字体大小
		if(realListSize<idealListSize){
			return idealCharSize;
		}
		if(idealListSize==0){
			callChain+="理想记录数为0";log.error(callChain);return null;
		}
		if(idealCharSize==0){
			callChain+="理想字体大小为0";log.error(callChain);return null;
		}
		if(realListSize==0){
			callChain+="真实记录数为0";log.error(callChain);return null;
		}
		
		//计算公式为：实际字体大小=理想字体大小*理想记录数/实际记录数
		float area = idealCharSize*idealListSize;
		float size_d=(area/(float)realListSize);
		int size=(int)size_d;
		size--;

		return size;
	}
	
}
