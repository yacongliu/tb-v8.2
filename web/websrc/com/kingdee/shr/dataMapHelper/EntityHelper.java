package com.kingdee.shr.dataMapHelper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

/**
 * Title: EntityHelper
 * <p>
 * Description: 对实体类中String值为null的字段进行替换，将null替换为""，以适应freemarker模板
 * 
 * @author fei_tong
 * @date 2019-3-25 & 下午03:10:03
 * @since V1.0
 */
public class EntityHelper {
	Logger log = Logger.getLogger(EntityHelper.class);
	
	private EntityHelper() {
		if (EntityHelperHolder.entityHelper != null) {
			log.error("EntityHelper类的单例已存在，不能再实例化。");
		    throw new IllegalStateException();
		}
		
	}
	public static EntityHelper getEntityHelper(){
		return EntityHelperHolder.entityHelper;
	}
	private static class EntityHelperHolder{
		private static EntityHelper entityHelper = new EntityHelper();
	}

	
	/**判断某类实例中的每个String类型字段的值是否为null的，是则改为空字符串的方法
	 * @param entity
	 * @param callChain
	 * @return 如果出错会返回null，出错信息已被记录到日志中
	 */
	public Object transferStringFieldsFromNullToEmpty(Object entity, String callChain){
		if(callChain==null)callChain="";
		callChain="判断某类实例中的每个String类型字段的值是否为null的，是则改为空字符串的方法：";
		
		Field[] fields = entity.getClass().getDeclaredFields();
		
		for(int i=0; i<fields.length; i++){
			String key = fields[i].getName();
			String name = key.substring(0, 1).toUpperCase() + key.substring(1); // 将属性的首字符大写，方便构造get，set方法

			String type = fields[i].getGenericType().toString();
			
			if ("class java.lang.String".equals(type)) {
				Method mGet = null;
				try {
					mGet = entity.getClass().getMethod("get" + name);
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					callChain += "get方法反射失败，找不到"+"get" + name+"方法："+e;
					log.error(callChain);
					return null;
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					callChain += "get方法反射失败，方法名："+"get" + name+"："+e;
					log.error(callChain);
					return null;
				}
				String value = null;
				try {
					value = (String) mGet.invoke(entity);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block e.printStackTrace();
					callChain += "get方法执行失败，访问异常，方法名："+"get" + name+"："+e;
					log.error(callChain);
					return null;
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block e.printStackTrace();
					callChain += "get方法执行失败，传参异常，方法名："+"get" + name+"："+e;
					log.error(callChain);
					return null;
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block e.printStackTrace();
					callChain += "get方法执行失败，调用异常，方法名："+"get" + name+"："+e;
					log.error(callChain);
					return null;
				} 
				
				if(value==null){
					Method mSet = null;
					try {
						mSet = entity.getClass().getMethod("set" + name,String.class);
					} catch (NoSuchMethodException e) {
						// TODO Auto-generated catch block e.printStackTrace();
						callChain += "set方法反射失败，找不到"+"get" + name+"方法："+e;
						log.error(callChain);
						return null;
					} catch (SecurityException e) {
						// TODO Auto-generated catch block e.printStackTrace();
						callChain += "set方法反射失败，方法名："+"get" + name+"："+e;
						log.error(callChain);
						return null;
					}
					
					try {
						mSet.invoke(entity, "");
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block e.printStackTrace();
						callChain += "set方法执行失败，访问异常，方法名："+"get" + name+"："+e;
						log.error(callChain);
						return null;
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block e.printStackTrace();
						callChain += "set方法执行失败，传参异常，方法名："+"get" + name+"："+e;
						log.error(callChain);
						return null;
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block e.printStackTrace();
						callChain += "set方法执行失败，调用异常，方法名："+"get" + name+"："+e;
						log.error(callChain);
						return null;
					}
				}
				
			}
		}
		return entity;
	}
}
