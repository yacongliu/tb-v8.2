package com.kingdee.shr.recuritment.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.apache.log4j.Logger;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.eas.util.app.DbUtil;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;

/**
 * 
 * Title: ResumeBaseRecListHandlerEx <br/>
 * Description:  简历筛选   初步筛选  将勾选的简历至  为  合格<br/>
 * @author 马伟楠 Email:tjmaweinan@kingdee.com <br/>
 * @date 2019年8月24日<br/>
 */
public class ResumeBaseRecListHandlerEx extends ResumeBaseRecListHandler {
	private static Logger logger = Logger
            .getLogger(com.kingdee.shr.recuritment.web.handler.ResumeBaseRecListHandlerEx.class);
	private Context ctx;

	public ResumeBaseRecListHandlerEx(Context ctx) {
		this.ctx = ctx;
	}

	public ResumeBaseRecListHandlerEx() {
		this.ctx = SHRContext.getInstance().getContext();
	}

	/**
	 * 
	 * <p>
	 * Title: setStateAction
	 * </p>
	 * <p>
	 * Description: 更新简历筛选状态
	 * </p>
	 * 
	 * @param request
	 * @param response
	 * @param modelMap
	 * @throws SHRWebException
	 * @throws BOSException
	 * @author 马伟楠
	 */
	public void setStateAction(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap)
			throws SHRWebException, BOSException {
		String billId = request.getParameter("billId");
		updateState(billId);
	}

	/**
	 * 
	 * <p>
	 * Title: updateState
	 * </p>
	 * <p>
	 * Description: 更新筛选出简历的结果
	 * </p>
	 * 
	 * @param billid 简历编号
	 * @author 马伟楠
	 * @throws BOSException
	 */
	private void updateState(String billid) throws BOSException {
		//数据取出逗号 2019.8.24
		if(billid.equals(null)||billid.equals("")) {
			logger.error("updateState方法里    简历编号为空，请联系管理员");
			return ;
		}
		String[] billids = billid.split(",");
		for (int i = 0; i < billids.length; i++) {
			String sql = "Update T_REC_ResumeBaseRec set cfresult ='合格' where fid ='" + billids[i] + "'";
			try {
				DbUtil.execute(ctx, sql);
			} catch (BOSException e) {
				e.printStackTrace();
				throw new BOSException("设置单据筛选状态失败", e);
			}
		}
	}

	/**
	 * 
	 * <p>
	 * Title: updateStatSql
	 * </p>
	 * <p>
	 * Description: 更新简历状态不合格SQL语句
	 * </p>
	 * 
	 * @throws BOSException
	 * @author 马伟楠
	 * @deprecated 废弃  设置除筛选结果为合格其他均为不合格
	 */
	@SuppressWarnings("unused")
	private void updateStatSql() throws BOSException {
		String updatesql = "Update T_REC_ResumeBaseRec set cfresult = '不合格' where fid not in (select fid from T_REC_ResumeBaseRec where cfresult='合格')";
		try {
			DbUtil.execute(ctx, updatesql);
		} catch (BOSException e) {
			e.printStackTrace();
			throw new BOSException("更新简历筛选结果状态失败！", e);
		}
	}
}
