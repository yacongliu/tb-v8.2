package com.kingdee.shr.entity;

import java.util.List;
import java.util.Map;

/**
 * 
 * Title: HRPersonal
 * <p>
 * Description: 个人基本情况实体
 * 
 * @author fei_tong
 * @date 2019-3-25 & 下午02:20:31
 * @since V1.0
 */
public class HRPersonal {
    private String name;//姓名

    private String gender;//性别

    private String ethnicity;//民族

    private String birthYear;//出生年

    private String birthMonth;//出生月

    private String homePlace;//出生地

    private String joinPartyYear;//入党年

    private String joinPartyMonth;//入党月

    private String startWorkYear;//加入工作年

    private String startWorkMonth;//加入工作月

    private String major;//全日制院校专业

    private String educationLevel;//全日制学历

    private String educationTitle;//全日制学位
    
    private String vacationalMajor;//在职教育院校专业
    
    private String vacationalLevel;//在职教育学历
    
    private String vacationalTitle;//在职教育学位

    private String profession;//职称

    private String company;//工作单位

    private String position;//职务

    private String imageBase64;//照片编码文本

    private List<Map<String, String>> vitae;//简历

    public HRPersonal() {
        super();
    }

	public HRPersonal(String birthMonth, String birthYear, String company,
			String educationLevel, String educationTitle, String ethnicity,
			String gender, String homePlace, String imageBase64,
			String joinPartyMonth, String joinPartyYear, String major,
			String name, String position, String profession,
			String startWorkMonth, String startWorkYear,
			String vacationalLevel, String vacationalMajor,
			String vacationalTitle, List<Map<String, String>> vitae) {
		super();
		this.birthMonth = birthMonth;
		this.birthYear = birthYear;
		this.company = company;
		this.educationLevel = educationLevel;
		this.educationTitle = educationTitle;
		this.ethnicity = ethnicity;
		this.gender = gender;
		this.homePlace = homePlace;
		this.imageBase64 = imageBase64;
		this.joinPartyMonth = joinPartyMonth;
		this.joinPartyYear = joinPartyYear;
		this.major = major;
		this.name = name;
		this.position = position;
		this.profession = profession;
		this.startWorkMonth = startWorkMonth;
		this.startWorkYear = startWorkYear;
		this.vacationalLevel = vacationalLevel;
		this.vacationalMajor = vacationalMajor;
		this.vacationalTitle = vacationalTitle;
		this.vitae = vitae;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getHomePlace() {
		return homePlace;
	}

	public void setHomePlace(String homePlace) {
		this.homePlace = homePlace;
	}

	public String getJoinPartyYear() {
		return joinPartyYear;
	}

	public void setJoinPartyYear(String joinPartyYear) {
		this.joinPartyYear = joinPartyYear;
	}

	public String getJoinPartyMonth() {
		return joinPartyMonth;
	}

	public void setJoinPartyMonth(String joinPartyMonth) {
		this.joinPartyMonth = joinPartyMonth;
	}

	public String getStartWorkYear() {
		return startWorkYear;
	}

	public void setStartWorkYear(String startWorkYear) {
		this.startWorkYear = startWorkYear;
	}

	public String getStartWorkMonth() {
		return startWorkMonth;
	}

	public void setStartWorkMonth(String startWorkMonth) {
		this.startWorkMonth = startWorkMonth;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getEducationTitle() {
		return educationTitle;
	}

	public void setEducationTitle(String educationTitle) {
		this.educationTitle = educationTitle;
	}

	public String getVacationalMajor() {
		return vacationalMajor;
	}

	public void setVacationalMajor(String vacationalMajor) {
		this.vacationalMajor = vacationalMajor;
	}

	public String getVacationalLevel() {
		return vacationalLevel;
	}

	public void setVacationalLevel(String vacationalLevel) {
		this.vacationalLevel = vacationalLevel;
	}

	public String getVacationalTitle() {
		return vacationalTitle;
	}

	public void setVacationalTitle(String vacationalTitle) {
		this.vacationalTitle = vacationalTitle;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getImageBase64() {
		return imageBase64;
	}

	public void setImageBase64(String imageBase64) {
		this.imageBase64 = imageBase64;
	}

	public List<Map<String, String>> getVitae() {
		return vitae;
	}

	public void setVitae(List<Map<String, String>> vitae) {
		this.vitae = vitae;
	}
    
}
