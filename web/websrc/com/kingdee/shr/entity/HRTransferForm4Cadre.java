package com.kingdee.shr.entity;

import java.util.List;
import java.util.Map;

/**
 * 
 * Title: HRTransferForm4Cadre
 * <p>
 * Description: 是根据干部任免审批表上的项目设计的实体类
 * 
 * @author fei_tong
 * @date 2019-3-25 & 下午02:21:26
 * @since V1.0
 */
public class HRTransferForm4Cadre {
    private String name;

    private String gender;

    private String birthDay;

    private String ethnicity;

    private String nativePlace;// 籍贯

    private String homePlace;// 出生地

    private String joinPartyTime;//入党时间

    private String startWorkTime;//加入工作时间

    private String healthCondition;//健康状态

    private String imageBase64;// 图像数据的Base64格式字符串

    private String profession;// 专业技术职务

    private String speciality;// 熟悉专业有何专长

    private String educationLevel;// 全日制教育学历

    private String educationTitle;// 全日制教育学位
    
    private String educationCharSize;//全日制学历学位单元格字体大小

    private String institution;// 全日制毕业院校

    private String major;// 全日制教育专业
    
    private String majorCharSize;//全日制学校专业单元格字体大小

    private String vacationalLevel;// 在职教育学历

    private String vacationalTitle;// 在职教育毕业院校
    
    private String vacationalCharSize;//在职教育学历学位单元格字体大小

    private String vacationalMajor;// 在职教育专业
    
    private String vacationalMajorCharSize;//在职教育学校专业单元格字体大小

    private String currentDuty;// 现任职务

    private String proposedDuty;// 拟任职务

    private String deposableDuty;// 拟免职务

    private List<Map<String, String>> vitae;// 简历

    private String vitaeCharSize;// 简历数据的字体大小，应根据vitae的size()由程序动态决定

    private List<Map<String, String>> rewardAndPenalty;// 奖惩情况

    private List<Map<String, String>> annualResult;// 年度考核结果

    private String transferReason;// 任免理由

    private List<Map<String, String>> familyAndRelatives;// 家庭成员及重要社会关系

    private String applyingOffice;// 呈报单位

    private String applyingDate;// 呈报日期

    private String approvalOpinion;// 审批意见

    private String approvalDate;// 审批时间

    private String administrativeOpinion;// 行政机关任免意见

    private String administrativeDate;// 行政时间

    private String formFillingPerson;// 填表人

    public HRTransferForm4Cadre() {
        super();
    }

	public HRTransferForm4Cadre(String administrativeDate,
			String administrativeOpinion,
			List<Map<String, String>> annualResult, String applyingDate,
			String applyingOffice, String approvalDate, String approvalOpinion,
			String birthDay, String currentDuty, String deposableDuty,
			String educationCharSize, String educationLevel,
			String educationTitle, String ethnicity,
			List<Map<String, String>> familyAndRelatives,
			String formFillingPerson, String gender, String healthCondition,
			String homePlace, String imageBase64, String institution,
			String joinPartyTime, String major, String majorCharSize,
			String name, String nativePlace, String profession,
			String proposedDuty, List<Map<String, String>> rewardAndPenalty,
			String speciality, String startWorkTime, String transferReason,
			String vacationalCharSize, String vacationalLevel,
			String vacationalMajor, String vacationalMajorCharSize,
			String vacationalTitle, List<Map<String, String>> vitae,
			String vitaeCharSize) {
		super();
		this.administrativeDate = administrativeDate;
		this.administrativeOpinion = administrativeOpinion;
		this.annualResult = annualResult;
		this.applyingDate = applyingDate;
		this.applyingOffice = applyingOffice;
		this.approvalDate = approvalDate;
		this.approvalOpinion = approvalOpinion;
		this.birthDay = birthDay;
		this.currentDuty = currentDuty;
		this.deposableDuty = deposableDuty;
		this.educationCharSize = educationCharSize;
		this.educationLevel = educationLevel;
		this.educationTitle = educationTitle;
		this.ethnicity = ethnicity;
		this.familyAndRelatives = familyAndRelatives;
		this.formFillingPerson = formFillingPerson;
		this.gender = gender;
		this.healthCondition = healthCondition;
		this.homePlace = homePlace;
		this.imageBase64 = imageBase64;
		this.institution = institution;
		this.joinPartyTime = joinPartyTime;
		this.major = major;
		this.majorCharSize = majorCharSize;
		this.name = name;
		this.nativePlace = nativePlace;
		this.profession = profession;
		this.proposedDuty = proposedDuty;
		this.rewardAndPenalty = rewardAndPenalty;
		this.speciality = speciality;
		this.startWorkTime = startWorkTime;
		this.transferReason = transferReason;
		this.vacationalCharSize = vacationalCharSize;
		this.vacationalLevel = vacationalLevel;
		this.vacationalMajor = vacationalMajor;
		this.vacationalMajorCharSize = vacationalMajorCharSize;
		this.vacationalTitle = vacationalTitle;
		this.vitae = vitae;
		this.vitaeCharSize = vitaeCharSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getHomePlace() {
		return homePlace;
	}

	public void setHomePlace(String homePlace) {
		this.homePlace = homePlace;
	}

	public String getJoinPartyTime() {
		return joinPartyTime;
	}

	public void setJoinPartyTime(String joinPartyTime) {
		this.joinPartyTime = joinPartyTime;
	}

	public String getStartWorkTime() {
		return startWorkTime;
	}

	public void setStartWorkTime(String startWorkTime) {
		this.startWorkTime = startWorkTime;
	}

	public String getHealthCondition() {
		return healthCondition;
	}

	public void setHealthCondition(String healthCondition) {
		this.healthCondition = healthCondition;
	}

	public String getImageBase64() {
		return imageBase64;
	}

	public void setImageBase64(String imageBase64) {
		this.imageBase64 = imageBase64;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getEducationTitle() {
		return educationTitle;
	}

	public void setEducationTitle(String educationTitle) {
		this.educationTitle = educationTitle;
	}

	public String getEducationCharSize() {
		return educationCharSize;
	}

	public void setEducationCharSize(String educationCharSize) {
		this.educationCharSize = educationCharSize;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getMajorCharSize() {
		return majorCharSize;
	}

	public void setMajorCharSize(String majorCharSize) {
		this.majorCharSize = majorCharSize;
	}

	public String getVacationalLevel() {
		return vacationalLevel;
	}

	public void setVacationalLevel(String vacationalLevel) {
		this.vacationalLevel = vacationalLevel;
	}

	public String getVacationalTitle() {
		return vacationalTitle;
	}

	public void setVacationalTitle(String vacationalTitle) {
		this.vacationalTitle = vacationalTitle;
	}

	public String getVacationalCharSize() {
		return vacationalCharSize;
	}

	public void setVacationalCharSize(String vacationalCharSize) {
		this.vacationalCharSize = vacationalCharSize;
	}

	public String getVacationalMajor() {
		return vacationalMajor;
	}

	public void setVacationalMajor(String vacationalMajor) {
		this.vacationalMajor = vacationalMajor;
	}

	public String getVacationalMajorCharSize() {
		return vacationalMajorCharSize;
	}

	public void setVacationalMajorCharSize(String vacationalMajorCharSize) {
		this.vacationalMajorCharSize = vacationalMajorCharSize;
	}

	public String getCurrentDuty() {
		return currentDuty;
	}

	public void setCurrentDuty(String currentDuty) {
		this.currentDuty = currentDuty;
	}

	public String getProposedDuty() {
		return proposedDuty;
	}

	public void setProposedDuty(String proposedDuty) {
		this.proposedDuty = proposedDuty;
	}

	public String getDeposableDuty() {
		return deposableDuty;
	}

	public void setDeposableDuty(String deposableDuty) {
		this.deposableDuty = deposableDuty;
	}

	public List<Map<String, String>> getVitae() {
		return vitae;
	}

	public void setVitae(List<Map<String, String>> vitae) {
		this.vitae = vitae;
	}

	public String getVitaeCharSize() {
		return vitaeCharSize;
	}

	public void setVitaeCharSize(String vitaeCharSize) {
		this.vitaeCharSize = vitaeCharSize;
	}

	public List<Map<String, String>> getRewardAndPenalty() {
		return rewardAndPenalty;
	}

	public void setRewardAndPenalty(List<Map<String, String>> rewardAndPenalty) {
		this.rewardAndPenalty = rewardAndPenalty;
	}

	public List<Map<String, String>> getAnnualResult() {
		return annualResult;
	}

	public void setAnnualResult(List<Map<String, String>> annualResult) {
		this.annualResult = annualResult;
	}

	public String getTransferReason() {
		return transferReason;
	}

	public void setTransferReason(String transferReason) {
		this.transferReason = transferReason;
	}

	public List<Map<String, String>> getFamilyAndRelatives() {
		return familyAndRelatives;
	}

	public void setFamilyAndRelatives(List<Map<String, String>> familyAndRelatives) {
		this.familyAndRelatives = familyAndRelatives;
	}

	public String getApplyingOffice() {
		return applyingOffice;
	}

	public void setApplyingOffice(String applyingOffice) {
		this.applyingOffice = applyingOffice;
	}

	public String getApplyingDate() {
		return applyingDate;
	}

	public void setApplyingDate(String applyingDate) {
		this.applyingDate = applyingDate;
	}

	public String getApprovalOpinion() {
		return approvalOpinion;
	}

	public void setApprovalOpinion(String approvalOpinion) {
		this.approvalOpinion = approvalOpinion;
	}

	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getAdministrativeOpinion() {
		return administrativeOpinion;
	}

	public void setAdministrativeOpinion(String administrativeOpinion) {
		this.administrativeOpinion = administrativeOpinion;
	}

	public String getAdministrativeDate() {
		return administrativeDate;
	}

	public void setAdministrativeDate(String administrativeDate) {
		this.administrativeDate = administrativeDate;
	}

	public String getFormFillingPerson() {
		return formFillingPerson;
	}

	public void setFormFillingPerson(String formFillingPerson) {
		this.formFillingPerson = formFillingPerson;
	}
    
}
