package com.kingdee.shr.affair.web.handler.hrman;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ObjectNotFoundException;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.tools.FixSalaryTools;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.costbudget.CmpStdItemFactory;
import com.kingdee.shr.costbudget.CmpStdItemInfo;
import com.kingdee.shr.costbudget.CmpStdValueCollection;
import com.kingdee.shr.costbudget.CmpStdValueFactory;
import com.kingdee.shr.costbudget.CmpStdValueInfo;
import com.kingdee.util.StringUtils;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: FluctuationBizBillHrManEditHandlerEx <br>
 *            Description: 员工调动单
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-12-22 & 上午11:52:22
 * @since V1.0
 */
public class FluctuationBizBillHrManEditHandlerEx extends FluctuationBizBillHrManEditHandler {

    private Context ctx;

    public FluctuationBizBillHrManEditHandlerEx(Context ctx) {
        this.ctx = ctx;
    }

    public FluctuationBizBillHrManEditHandlerEx() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * 
     * <p>
     * Title: getStdCmpItemAction
     * </p>
     * <p>
     * Description: 根据人员 薪酬标准获取其目前最新的薪酬结构记录
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws BOSException
     * @throws SHRWebException
     */
    @SuppressWarnings("unchecked")
    public String getStdCmpItemAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws BOSException, SHRWebException {

        String cmpStandardId = request.getParameter("cmpStandardId");
        String personId = request.getParameter("personId");

        if (StringUtils.isEmpty(personId)) {
            throw new ShrWebBizException("请先选择人员！");
        }
        if (StringUtils.isEmpty(cmpStandardId)) {
            throw new ShrWebBizException("请先选择默认薪酬标准！");
        }

        Map map = FixSalaryTools.getMaxEffectFixSalaryInfo(ctx, cmpStandardId, personId);

        JSONUtils.SUCCESS(map);

        return null;
    }

    /**
     * 
     * <p>
     * Title: getStdValueAction
     * </p>
     * <p>
     * Description: 获取薪值
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws SHRWebException
     * @throws EASBizException
     * @throws BOSException
     */
    public String getStdValueAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException, BOSException {

        String stdPointId = request.getParameter("stdPointId");// 薪级
        String cmpStdLevelId = request.getParameter("cmpStdLevelId"); // 薪等
        String cmpItemId = request.getParameter("cmpItemId");// 薪酬项目
        String cmpStandardId = request.getParameter("cmpStandardId");// 薪酬标准

        CmpStdItemInfo cmpStdItem = null;
        String itemId = null;
        try {
            cmpStdItem = CmpStdItemFactory.getRemoteInstance().getCmpStdItemInfo(
                    " where cmpStandard.id = '" + cmpStandardId + "' and cmpItem.id = '" + cmpItemId + "'");
        } catch (EASBizException e) {
            throw new ShrWebBizException("获取薪值异常", e);
        } catch (ObjectNotFoundException e) {
            throw new ShrWebBizException("未找到您选择的薪酬标准下的薪酬标准项目");
        }

        if (cmpStdItem != null) {
            // 薪酬标准项目ID
            itemId = cmpStdItem.getId().toString();

        }

        if (StringUtils.isEmpty(cmpItemId)) {
            throw new ShrWebBizException("请选择薪酬项目！");
        }
        if (StringUtils.isEmpty(cmpStdLevelId)) {
            throw new ShrWebBizException("请选择薪等！");
        }

        if (StringUtils.isEmpty(stdPointId)) {
            throw new ShrWebBizException("请选择薪级！");
        }

        CmpStdValueCollection cmpStdValueCollection = CmpStdValueFactory.getRemoteInstance()
                .getCmpStdValueCollection(
                        " where cmpStdLevel.id = '" + cmpStdLevelId + "' and cmpStdItem.id = '" + itemId
                                + "' and cmpStdPoint.id = '" + stdPointId + "'");
        double value = 0.0;
        if (cmpStdValueCollection.size() > 0) {
            CmpStdValueInfo cmpStdValueInfo = cmpStdValueCollection.get(0);

            value = cmpStdValueInfo.getValue();
        }

        writeSuccessData(value);

        return null;

    }
}
