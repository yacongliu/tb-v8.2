package com.kingdee.shr.affair.web.handler.hrman;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ObjectNotFoundException;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.shr.base.syssetting.context.SHRContext;
import com.kingdee.shr.base.syssetting.exception.SHRWebException;
import com.kingdee.shr.base.syssetting.exception.ShrWebBizException;
import com.kingdee.shr.base.syssetting.web.json.JSONUtils;
import com.kingdee.shr.costbudget.CmpStdItemCollection;
import com.kingdee.shr.costbudget.CmpStdItemFactory;
import com.kingdee.shr.costbudget.CmpStdItemInfo;
import com.kingdee.shr.costbudget.CmpStdValueCollection;
import com.kingdee.shr.costbudget.CmpStdValueFactory;
import com.kingdee.shr.costbudget.CmpStdValueInfo;
import com.kingdee.util.StringUtils;

/**
 * @Copyright 天津金蝶软件有限公司<br>
 *            Title: EmpEnrollBizBillHrManEditHandlerEx <br>
 *            Description: 入职单
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-12-20 & 上午08:43:24
 * @since V1.0
 */
public class EmpEnrollBizBillHrManEditHandlerEx extends EmpEnrollBizBillHrManEditHandler {

    private Context ctx;

    public EmpEnrollBizBillHrManEditHandlerEx(Context ctx) {
        this.ctx = ctx;
    }

    public EmpEnrollBizBillHrManEditHandlerEx() {
        this.ctx = SHRContext.getInstance().getContext();
    }

    /**
     * 
     * <p>
     * Title: getStdCmpItemAction
     * </p>
     * <p>
     * Description: 根据薪酬标准获取其下的薪酬项目
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws BOSException
     * @throws SHRWebException
     */
    @SuppressWarnings("unchecked")
    public String getStdCmpItemAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws BOSException, SHRWebException {

        String cmpStandardId = request.getParameter("cmpStandardId");
        // String personId = request.getParameter("personId");
        CmpStdItemCollection cmpStdItemCollection = CmpStdItemFactory.getLocalInstance(this.ctx)
                .getCmpStdItemCollection(
                        "select cmpItem.*,cmpStandard.* where cmpStandard.id = '" + cmpStandardId + "'");

        Map ajaxData = new HashMap();
        List datas = new ArrayList();
        for (int i = 0; i < cmpStdItemCollection.size(); i++) {
            CmpStdItemInfo cmpStdItem = cmpStdItemCollection.get(i);
            Map<String, HashMap<String, String>> map = new HashMap<String, HashMap<String, String>>(3);
            HashMap<String, String> f7map = new HashMap<String, String>(2);
            // 薪酬项目
            String itemId = cmpStdItem.getCmpItem().getId().toString();
            String itemName = cmpStdItem.getCmpItem().getName();
            f7map.put("id", itemId);
            f7map.put("name", itemName);
            map.put("cmpItem", f7map);

            // 薪酬标准
            HashMap<String, String> f7mapCmpStd = new HashMap<String, String>(2);
            f7mapCmpStd.put("id", cmpStdItem.getCmpStandard().getId().toString());
            f7mapCmpStd.put("name", cmpStdItem.getCmpStandard().getName());
            map.put("cmpStandard", f7mapCmpStd);

            datas.add(map);

        }

        ajaxData.put("rowData", datas);
        JSONUtils.SUCCESS(ajaxData);

        return null;

    }

    /**
     * 
     * <p>
     * Title: getStdValueAction
     * </p>
     * <p>
     * Description: 获取薪值
     * </p>
     * 
     * @param request
     * @param response
     * @param modelMap
     * @return
     * @throws SHRWebException
     * @throws EASBizException
     * @throws BOSException
     */
    public String getStdValueAction(HttpServletRequest request, HttpServletResponse response,
            ModelMap modelMap) throws SHRWebException, EASBizException, BOSException {

        String stdPointId = request.getParameter("stdPointId");// 薪级
        String cmpStdLevelId = request.getParameter("cmpStdLevelId"); // 薪等
        String cmpItemId = request.getParameter("cmpItemId");// 薪酬项目
        String cmpStandardId = request.getParameter("cmpStandardId");// 薪酬标准

        CmpStdItemInfo cmpStdItem = null;
        String itemId = null;
        try {
            cmpStdItem = CmpStdItemFactory.getRemoteInstance().getCmpStdItemInfo(
                    " where cmpStandard.id = '" + cmpStandardId + "' and cmpItem.id = '" + cmpItemId + "'");
        } catch (EASBizException e) {
            throw new ShrWebBizException("获取薪值异常", e);
        } catch (ObjectNotFoundException e) {
            throw new ShrWebBizException("未找到您选择的薪酬标准下的薪酬标准项目");
        }

        if (cmpStdItem != null) {
            // 薪酬标准项目ID
            itemId = cmpStdItem.getId().toString();

        }

        if (StringUtils.isEmpty(cmpItemId)) {
            throw new ShrWebBizException("请选择薪酬项目！");
        }
        if (StringUtils.isEmpty(cmpStdLevelId)) {
            throw new ShrWebBizException("请选择薪等！");
        }

        if (StringUtils.isEmpty(stdPointId)) {
            throw new ShrWebBizException("请选择薪级！");
        }

        CmpStdValueCollection cmpStdValueCollection = CmpStdValueFactory.getRemoteInstance()
                .getCmpStdValueCollection(
                        " where cmpStdLevel.id = '" + cmpStdLevelId + "' and cmpStdItem.id = '" + itemId
                                + "' and cmpStdPoint.id = '" + stdPointId + "'");
        double value = 0.0;
        if (cmpStdValueCollection.size() > 0) {
            CmpStdValueInfo cmpStdValueInfo = cmpStdValueCollection.get(0);

            value = cmpStdValueInfo.getValue();
        }

        writeSuccessData(value);

        return null;

    }
}
