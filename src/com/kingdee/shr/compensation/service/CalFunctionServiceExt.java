package com.kingdee.shr.compensation.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import com.kingdee.bos.BOSException;
import com.kingdee.shr.compensation.CalSchemeInfo;

/**
 * 
 * Title: CalCustomFunctionService
 * <p>
 * Description: 自定义核算函数
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-8-5 & 下午04:38:07
 * @since V1.0
 */
public class CalFunctionServiceExt {

    /**
     * 
     * <p>
     * Title: getDutyCostData
     * </p>
     * <p>
     * Description: 获取值班费用
     * </p>
     * 
     * @param type 异常类型编码
     * @param paramMap
     * @param tempMap
     * @param calScheme
     * @param mainTableRowFilter
     * @return
     */
    public double getDutyCostData(String type, java.util.Map paramMap, java.util.HashMap tempMap,
            com.kingdee.shr.compensation.CalSchemeInfo calScheme, String mainTableRowFilter) {
        com.kingdee.bos.Context ctx = com.kingdee.shr.compensation.app.formula.data.Tools.getInstance()
                .getCtx();
        //获取对应的键
        String key = "getBusCostCoutData" + calScheme.getNumber() + calScheme.getPeriodYear()
                + calScheme.getPeriodMonth();
        java.util.Map<String, Double> dataMap = new java.util.HashMap<String, Double>();
        //获取personId
        String personId = paramMap.get("T_HR_SCMPCALTABLE_FPERSONID") == null ? null : paramMap.get(
                "T_HR_SCMPCALTABLE_FPERSONID").toString();
        double money = 0.00;
        //判断perssonId是否为空
        if (personId == null) {
            return 0;
        }
      //判断tempMap是否为空和tempMap的key是否为空
        if (null == tempMap || null == tempMap.get(key)) {
            String typeNum;
            String sqlStr = getDutyCostDataSql(personId, mainTableRowFilter, calScheme);
            try {
                com.kingdee.jdbc.rowset.IRowSet rowSet = com.kingdee.eas.util.app.DbUtil.executeQuery(ctx,
                        sqlStr);
                while (rowSet.next()) {
                    personId = rowSet.getString("personId");
                    money = rowSet.getBigDecimal("money").doubleValue();
                    dataMap.put(personId, money);
                }
            } catch (BOSException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            tempMap.put(key, dataMap);
        }

        dataMap = (java.util.Map<String, Double>) tempMap.get(key);
        return dataMap.get(personId) == null ? 0.00 : dataMap.get(personId);

    }

    /**
     * 
     * <p>
     * Title: getAtsExceptionData
     * </p>
     * <p>
     * Description: 获取考勤异常时长
     * </p>
     * 
     * @param type 异常类型编码
     * @param paramMap
     * @param tempMap
     * @param calScheme
     * @param mainTableRowFilter
     * @return
     */
    public double getAtsExceptionData(String type, java.util.Map paramMap, java.util.HashMap tempMap,
            com.kingdee.shr.compensation.CalSchemeInfo calScheme, String mainTableRowFilter) {

        com.kingdee.bos.Context ctx = com.kingdee.shr.compensation.app.formula.data.Tools.getInstance()
                .getCtx();

        String key = "getBusCostCoutData" + calScheme.getNumber() + calScheme.getPeriodYear()
                + calScheme.getPeriodMonth();
        java.util.Map<String, Double> dataMap = new java.util.HashMap<String, Double>();
        String personId = paramMap.get("T_HR_SCMPCALTABLE_FPERSONID") == null ? null : paramMap.get(
                "T_HR_SCMPCALTABLE_FPERSONID").toString();
        double time = 0.00;
        if (personId == null) {
            return 0;
        }

        if (null == tempMap || null == tempMap.get(key)) {
            String typeNum;
            String sqlStr = getAtsExceptionDataSql(personId, mainTableRowFilter, calScheme);
            try {
                com.kingdee.jdbc.rowset.IRowSet rowSet = com.kingdee.eas.util.app.DbUtil.executeQuery(ctx,
                        sqlStr);
                while (rowSet.next()) {
                    personId = rowSet.getString("personId");
                    typeNum = rowSet.getString("type");
                    time = Double.valueOf(rowSet.getString("time"));
                    dataMap.put(personId + "_" + typeNum, time);
                }
            } catch (BOSException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            tempMap.put(key, dataMap);
        }

        dataMap = (java.util.Map<String, Double>) tempMap.get(key);
        return dataMap.get(personId + "_" + type) == null ? 0.00 : dataMap.get(personId + "_" + type);
    }

    /**
     * 
     * <p>
     * Title: getBusCostCoutData
     * </p>
     * <p>
     * Description: 获取班车费用数据
     * </p>
     * 
     * @param type
     * @param paramMap
     * @param tempMap
     * @param calScheme
     * @param mainTableRowFilter
     * @return
     */
    public double getBusCostCoutData(String type, java.util.Map paramMap, java.util.HashMap tempMap,
            com.kingdee.shr.compensation.CalSchemeInfo calScheme, String mainTableRowFilter) {
        com.kingdee.bos.Context ctx = com.kingdee.shr.compensation.app.formula.data.Tools.getInstance()
                .getCtx();

        String key = "getBusCostCoutData" + calScheme.getNumber() + calScheme.getPeriodYear()
                + calScheme.getPeriodMonth();
        java.util.Map<String, Double> dataMap = new java.util.HashMap<String, Double>();
        String personId = paramMap.get("T_HR_SCMPCALTABLE_FPERSONID") == null ? null : paramMap.get(
                "T_HR_SCMPCALTABLE_FPERSONID").toString();
        double money = 0.00;
        if (personId == null) {
            return 0;
        }

        if (null == tempMap || null == tempMap.get(key)) {
            String typeNum;
            String sqlStr = getBusCostCoutDataSql(personId, mainTableRowFilter, calScheme);
            try {
                com.kingdee.jdbc.rowset.IRowSet rowSet = com.kingdee.eas.util.app.DbUtil.executeQuery(ctx,
                        sqlStr);
                while (rowSet.next()) {
                    personId = rowSet.getString("personId");
                    typeNum = rowSet.getString("type");
                    money = Double.valueOf(rowSet.getString("money"));
                    dataMap.put(personId + "_" + typeNum, money);
                }
            } catch (BOSException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            tempMap.put(key, dataMap);
        }

        dataMap = (java.util.Map<String, Double>) tempMap.get(key);
        return dataMap.get(personId + "_" + type) == null ? 0.00 : dataMap.get(personId + "_" + type);

    }

    /**
     * 
     * <p>
     * Title: getPerformanceData
     * </p>
     * <p>
     * Description: 取员工销售数据
     * </p>
     * 
     * @param type
     * @param paramMap
     * @param tempMap
     * @param calScheme
     * @param mainTableRowFilter
     * @return
     */
    public double getPerformanceData(String type, java.util.Map paramMap, java.util.HashMap tempMap,
            com.kingdee.shr.compensation.CalSchemeInfo calScheme, String mainTableRowFilter) {
        com.kingdee.bos.Context ctx = com.kingdee.shr.compensation.app.formula.data.Tools.getInstance()
                .getCtx();
        String key = "getPerformanceData" + calScheme.getNumber() + calScheme.getPeriodYear()
                + calScheme.getPeriodMonth();
        java.util.Map<String, Double> dataMap = new java.util.HashMap<String, Double>();
        String personId = paramMap.get("T_HR_SCMPCALTABLE_FPERSONID") == null ? null : paramMap.get(
                "T_HR_SCMPCALTABLE_FPERSONID").toString();
        double sale = 0.00;
        if (personId == null)
            return 0;
        if (null == tempMap || null == tempMap.get(key)) {
            String productType;
            String sqlStr = getPerformanceDataSql(personId, mainTableRowFilter, calScheme);
            try {
                com.kingdee.jdbc.rowset.IRowSet rowSet = com.kingdee.eas.util.app.DbUtil.executeQuery(ctx,
                        sqlStr);
                while (rowSet.next()) {
                    personId = rowSet.getString("FPersonId");
                    productType = rowSet.getString("FType");
                    sale = rowSet.getDouble("FSale");
                    dataMap.put(personId + "_" + productType, sale);
                }
            } catch (BOSException e) {
                log("获取班车费用数据失败！" + e);
            } catch (SQLException e) {
                log("获取班车费用数据失败！" + e);
            }
            tempMap.put(key, dataMap);
        }
        dataMap = (java.util.Map<String, Double>) tempMap.get(key);
        return dataMap.get(personId + "_" + type) == null ? 0.00 : dataMap.get(personId + "_" + type);
    }

    /**
     * 
     * <p>
     * Title: getDutyCostDataSql
     * </p>
     * <p>
     * Description: 获取值班费sql
     * </p>
     * 
     * @param personId
     * @param mainTableRowFilter
     * @param calScheme
     * @return
     */
    private String getDutyCostDataSql(String personId, String mainTableRowFilter, CalSchemeInfo calScheme) {
        StringBuffer sqlStr = new StringBuffer();

        sqlStr
                .append(" /*dialect*/ select entry.cfpersonNameid as personId,to_char(bill.fbizdate,'yyyy') as year,entry.CFDutyMoney as money ");

        sqlStr.append(" from CT_DUT_DutyCostEntry entry ");
        sqlStr.append(" left join CT_DUT_DutyCost bill on entry.fbillid = bill.fid where ");
        if (null == mainTableRowFilter) {
            sqlStr.append(" entry.cfpersonNameid = '").append(personId).append("' ");
        } else {
            sqlStr.append(" entry.cfpersonNameid in (select FPersonId from t_hr_scmfcaltable ");
            sqlStr.append(" where ").append(mainTableRowFilter);
            sqlStr.append(")");
        }
        sqlStr.append(" and bill.fbillstate = 3 ");
        sqlStr.append(" and to_char(bill.fbizdate,'yyyy') = '").append(calScheme.getPeriodYear()).append("'");
        sqlStr.append(" and bill.CFCostMonth LIKE '%").append(calScheme.getPeriodMonth()).append("'");

        log(" 获取值班费sql：" + sqlStr.toString());
        return sqlStr.toString();
    }

    /**
     * 
     * <p>
     * Title: getAtsExceptionDataSql
     * </p>
     * <p>
     * Description: 获取考勤异常时长sql
     * </p>
     * 
     * @param personId 人员内码
     * @param mainTableRowFilter
     * @param calScheme
     * @return String
     */
    private String getAtsExceptionDataSql(String personId, String mainTableRowFilter, CalSchemeInfo calScheme) {
        StringBuffer sqlStr = new StringBuffer();

        sqlStr
                .append(" /*dialect*/ select entry.fpersonid as personId,to_char(bill.fbizdate,'yyyy') as year,to_char(bill.fbizdate,'MM') as month,entry.CFTime as time,type.fnumber as type ");

        sqlStr.append(" from CT_ATS_AtsExceptionBillEntry entry ");
        sqlStr.append(" left join CT_ATS_AtsExceptionBill bill on entry.fbillid = bill.fid ");
        sqlStr.append(" left join CT_BD_Type type on type.fid = entry.CFTypeID  where");
        if (null == mainTableRowFilter) {
            sqlStr.append(" entry.fpersonid = '").append(personId).append("' ");
        } else {
            sqlStr.append(" entry.fpersonid in (select FPersonId from t_hr_scmpcaltable ");
            sqlStr.append(" where ").append(mainTableRowFilter);
            sqlStr.append(")");
        }
        sqlStr.append(" and bill.fbillstate = 3 ");
        sqlStr.append(" and to_char(bill.fbizdate,'yyyy') = '").append(calScheme.getPeriodYear()).append("'");
        sqlStr.append(" and to_char(bill.fbizdate,'MM') LIKE '%").append(calScheme.getPeriodMonth()).append(
                "'");

        log("获取考勤异常时长sql：" + sqlStr.toString());
        return sqlStr.toString();
    }

    /**
     * 
     * <p>
     * Title: getBusCostCoutDataSql
     * </p>
     * <p>
     * Description: 获取班车费用sql
     * </p>
     * 
     * @param personId
     * @param mainTableRowFilter
     * @param calScheme 计算规则
     * @return String sql
     */
    private String getBusCostCoutDataSql(String personId, String mainTableRowFilter, CalSchemeInfo calScheme) {
        StringBuffer sqlStr = new StringBuffer();
        sqlStr
                .append(" /*dialect*/ SELECT entry.fpersonid as personId,entry.fid as fid,cfafteradjust as money,to_char(bill.fbizdate,'yyyy') as year,type.fnumber as type ");

        sqlStr.append(" FROM CT_BUS_BusCostCountEntry entry ");
        sqlStr.append(" left join CT_BUS_BusCostCount bill on entry.fbillid = bill.fid ");
        sqlStr.append(" left join CT_BUS_AdjustType type on entry.cfadjusttypeid = type.fid  where");
        if (null == mainTableRowFilter) {
            sqlStr.append(" entry.fpersonid = '").append(personId).append("' ");
        } else {
            sqlStr.append(" entry.fpersonid in (select FPersonId from t_hr_scmpcaltable ");
            sqlStr.append(" where ").append(mainTableRowFilter);
            sqlStr.append(")");
        }
        sqlStr.append(" and bill.fbillstate = 3 ");
        sqlStr.append(" and to_char(bill.fbizdate,'yyyy') = '").append(calScheme.getPeriodYear()).append("'");
        sqlStr.append(" and entry.CFAdjustMonth LIKE '%").append(calScheme.getPeriodMonth()).append("'");

        log("获取班车费用sql：" + sqlStr.toString());
        return sqlStr.toString();
    }

    private String getPerformanceDataSql(String personId, String mainTableRowFilter,
            com.kingdee.shr.compensation.CalSchemeInfo calScheme) {
        // mainTableRowFilter 格式：t_hr_scmpcaltable.fid in (select FID from T_HR_SCmpCalTable where
        // FCalState in (1,2,3,4,5,6,7)
        // and FID in (select * from VTZD0RDVX15O2LMJ0F0TWRZ7UYW8YKVSD8D))
        StringBuffer sqlStr = new StringBuffer();
        sqlStr.append(" select FPersonId, FType, FSale from T_HR_SPerformanceExt where ");
        if (null == mainTableRowFilter) {
            sqlStr.append(" FPersonId = '").append(personId).append("' ");
        } else {
            sqlStr.append(" FPersonId in (select FPersonId from t_hr_scmpcaltable ");
            sqlStr.append(" where ").append(mainTableRowFilter);
            sqlStr.append(")");
        }
        sqlStr.append(" and FPeriodYear = ").append(calScheme.getPeriodYear());
        sqlStr.append(" and FPeriodMonth = ").append(calScheme.getPeriodMonth());
        return sqlStr.toString();
    }

    public double getWorkDayCount(String date1, String date2, java.util.Map paramMap,
            java.util.HashMap tempMap, String mainTableRowFilter) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        com.kingdee.bos.Context ctx = com.kingdee.shr.compensation.app.formula.data.Tools.getInstance()
                .getCtx();
        // 定义唯一key值
        String key = "key值根据业务需求自定义，只要保证和其他函数key值不同即可";
        // 从数据库中取值初始化tempMap
        if (null == tempMap || null == tempMap.get(key)) {
            // TODO 初始化tempMap
        }
        // 从初始化完成的tempMap中取值
        double workDay = tempMap.get(key) == null ? 0.0 : (Double) tempMap.get(key);
        return workDay;
    }

    private void log(String log) {
        StringBuffer sb = new StringBuffer("*****自定义核算函数CalFunctionServiceExt**").append(log)
                .append("******");
        System.out.println(sb.toString());
    }

}
