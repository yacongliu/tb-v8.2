package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IDutyCostEntry extends IHRBillBaseEntry
{
    public DutyCostEntryInfo getDutyCostEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public DutyCostEntryInfo getDutyCostEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public DutyCostEntryInfo getDutyCostEntryInfo(String oql) throws BOSException, EASBizException;
    public DutyCostEntryCollection getDutyCostEntryCollection() throws BOSException;
    public DutyCostEntryCollection getDutyCostEntryCollection(EntityViewInfo view) throws BOSException;
    public DutyCostEntryCollection getDutyCostEntryCollection(String oql) throws BOSException;
}