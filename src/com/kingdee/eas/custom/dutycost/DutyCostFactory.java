package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class DutyCostFactory
{
    private DutyCostFactory()
    {
    }
    public static com.kingdee.eas.custom.dutycost.IDutyCost getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCost)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("8E773483") ,com.kingdee.eas.custom.dutycost.IDutyCost.class);
    }
    
    public static com.kingdee.eas.custom.dutycost.IDutyCost getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCost)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("8E773483") ,com.kingdee.eas.custom.dutycost.IDutyCost.class, objectCtx);
    }
    public static com.kingdee.eas.custom.dutycost.IDutyCost getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCost)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("8E773483"));
    }
    public static com.kingdee.eas.custom.dutycost.IDutyCost getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCost)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("8E773483"));
    }
}