package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class DutyCostEntryFactory
{
    private DutyCostEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.dutycost.IDutyCostEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCostEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("83743DAF") ,com.kingdee.eas.custom.dutycost.IDutyCostEntry.class);
    }
    
    public static com.kingdee.eas.custom.dutycost.IDutyCostEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCostEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("83743DAF") ,com.kingdee.eas.custom.dutycost.IDutyCostEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.dutycost.IDutyCostEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCostEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("83743DAF"));
    }
    public static com.kingdee.eas.custom.dutycost.IDutyCostEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.dutycost.IDutyCostEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("83743DAF"));
    }
}