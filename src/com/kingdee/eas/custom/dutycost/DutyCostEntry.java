package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.dutycost.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class DutyCostEntry extends HRBillBaseEntry implements IDutyCostEntry
{
    public DutyCostEntry()
    {
        super();
        registerInterface(IDutyCostEntry.class, this);
    }
    public DutyCostEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IDutyCostEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("83743DAF");
    }
    private DutyCostEntryController getController() throws BOSException
    {
        return (DutyCostEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public DutyCostEntryInfo getDutyCostEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getDutyCostEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public DutyCostEntryInfo getDutyCostEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getDutyCostEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public DutyCostEntryInfo getDutyCostEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getDutyCostEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public DutyCostEntryCollection getDutyCostEntryCollection() throws BOSException
    {
        try {
            return getController().getDutyCostEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public DutyCostEntryCollection getDutyCostEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getDutyCostEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public DutyCostEntryCollection getDutyCostEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getDutyCostEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}