package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class DutyCostCollection extends AbstractObjectCollection 
{
    public DutyCostCollection()
    {
        super(DutyCostInfo.class);
    }
    public boolean add(DutyCostInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(DutyCostCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(DutyCostInfo item)
    {
        return removeObject(item);
    }
    public DutyCostInfo get(int index)
    {
        return(DutyCostInfo)getObject(index);
    }
    public DutyCostInfo get(Object key)
    {
        return(DutyCostInfo)getObject(key);
    }
    public void set(int index, DutyCostInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(DutyCostInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(DutyCostInfo item)
    {
        return super.indexOf(item);
    }
}