package com.kingdee.eas.custom.dutycost;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractDutyCostEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractDutyCostEntryInfo()
    {
        this("id");
    }
    protected AbstractDutyCostEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.dutycost.DutyCostInfo getBill()
    {
        return (com.kingdee.eas.custom.dutycost.DutyCostInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.dutycost.DutyCostInfo item)
    {
        put("bill", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object: ��¼ 's ���� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPersonName()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("personName");
    }
    public void setPersonName(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("personName", item);
    }
    /**
     * Object:��¼'s ���Ž��property 
     */
    public java.math.BigDecimal getDutyMoney()
    {
        return getBigDecimal("dutyMoney");
    }
    public void setDutyMoney(java.math.BigDecimal item)
    {
        setBigDecimal("dutyMoney", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("83743DAF");
    }
}