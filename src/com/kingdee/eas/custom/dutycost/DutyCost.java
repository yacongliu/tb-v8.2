package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.dutycost.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import com.kingdee.eas.hr.base.HRBillStateEnum;

import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.util.enums.Enum;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class DutyCost extends HRBillBase implements IDutyCost
{
    public DutyCost()
    {
        super();
        registerInterface(IDutyCost.class, this);
    }
    public DutyCost(Context ctx)
    {
        super(ctx);
        registerInterface(IDutyCost.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("8E773483");
    }
    private DutyCostController getController() throws BOSException
    {
        return (DutyCostController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public DutyCostCollection getDutyCostCollection() throws BOSException
    {
        try {
            return getController().getDutyCostCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public DutyCostCollection getDutyCostCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getDutyCostCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public DutyCostCollection getDutyCostCollection(String oql) throws BOSException
    {
        try {
            return getController().getDutyCostCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public DutyCostInfo getDutyCostInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getDutyCostInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public DutyCostInfo getDutyCostInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getDutyCostInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public DutyCostInfo getDutyCostInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getDutyCostInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����״̬-User defined method
     *@param billId ��������
     *@param state ״̬
     */
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext(), billId, state);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��-User defined method
     *@param billId ����id
     */
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��״̬-User defined method
     *@param billId ����id
     */
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������״̬-User defined method
     *@param billId ����id
     */
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����δ���״̬-User defined method
     *@param billId ����id
     */
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}