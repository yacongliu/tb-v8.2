package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class DutyCostEntryCollection extends AbstractObjectCollection 
{
    public DutyCostEntryCollection()
    {
        super(DutyCostEntryInfo.class);
    }
    public boolean add(DutyCostEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(DutyCostEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(DutyCostEntryInfo item)
    {
        return removeObject(item);
    }
    public DutyCostEntryInfo get(int index)
    {
        return(DutyCostEntryInfo)getObject(index);
    }
    public DutyCostEntryInfo get(Object key)
    {
        return(DutyCostEntryInfo)getObject(key);
    }
    public void set(int index, DutyCostEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(DutyCostEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(DutyCostEntryInfo item)
    {
        return super.indexOf(item);
    }
}