package com.kingdee.eas.custom.dutycost.app;

import org.apache.log4j.Logger;
import javax.ejb.*;
import java.rmi.RemoteException;
import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK; //import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean; //import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;

import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.framework.ObjectBaseCollection;
import java.lang.String;
import com.kingdee.eas.framework.CoreBillBaseCollection;
import com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo;
import com.kingdee.eas.custom.dutycost.DutyCostCollection;
import com.kingdee.eas.custom.dutycost.DutyCostInfo;
import com.kingdee.eas.hr.base.ApproveTypeEnum;
import com.kingdee.eas.hr.base.HRBillBaseCollection;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseControllerBean;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.util.enums.Enum;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class DutyCostControllerBean extends AbstractDutyCostControllerBean {

    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.custom.dutycost.app.DutyCostControllerBean");

    /**
     * 提交生效
     */
    @Override
    protected IObjectPK _submitEffect(Context ctx, CoreBaseInfo model) throws BOSException, EASBizException {
        DutyCostInfo bill = (DutyCostInfo) model;
        bill.setApproveType(ApproveTypeEnum.DIRECT);
        bill.setBillState(HRBillStateEnum.AUDITED);
        IObjectPK objectPK = super.save(ctx, model);

        _setState(ctx, bill.getId(), HRBillStateEnum.AUDITED);

        return objectPK;
    }
    /**
     * 设置单据状态
     */
    @Override
    public void setState(Context ctx, BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException {
        IObjectPK pk = new ObjectUuidPK(billId);
        DutyCostInfo info = (DutyCostInfo) _getValue(ctx,
                        "select id, billState  where id = '" + pk.toString() + "'");

        info.setBillState(state);
        SelectorItemCollection selectors = new SelectorItemCollection();
        selectors.add(new SelectorItemInfo("id"));
        selectors.add(new SelectorItemInfo("billState"));

        updatePartial(ctx, info, selectors);
    }
    /**
     * 设置审批中状态
     */
    @Override
    public void setApproveState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.AUDITING);
    }
    /**
     * 设置未审核状态
     */
    @Override
    public void setEditState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.SAVED);
    }
    /**
     * 设置审核不通过状态
     */
    @Override
    public void setNoPassState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.AUDITEND);
    }
    /**
     * 设置审核通过状态
     */
    @Override
    public void setPassState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.AUDITEND);
    }

}