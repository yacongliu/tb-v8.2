/**
 * output package name
 */
package com.kingdee.eas.custom.dutycost.app;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.eas.framework.batchHandler.ResponseContext;


/**
 * output class name
 */
public abstract class AbstractDutyCostEditUIHandler extends com.kingdee.eas.hr.base.app.HRBillEditUIHandler

{
}