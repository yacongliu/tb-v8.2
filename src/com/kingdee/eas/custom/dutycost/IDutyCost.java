package com.kingdee.eas.custom.dutycost;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.util.enums.Enum;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public interface IDutyCost extends IHRBillBase
{
    public DutyCostCollection getDutyCostCollection() throws BOSException;
    public DutyCostCollection getDutyCostCollection(EntityViewInfo view) throws BOSException;
    public DutyCostCollection getDutyCostCollection(String oql) throws BOSException;
    public DutyCostInfo getDutyCostInfo(IObjectPK pk) throws BOSException, EASBizException;
    public DutyCostInfo getDutyCostInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public DutyCostInfo getDutyCostInfo(String oql) throws BOSException, EASBizException;
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException;
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException;
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException;
}