/**
 * output package name
 */
package com.kingdee.eas.custom;

import java.util.Map;
import java.util.List;
import java.util.Iterator;
import com.kingdee.util.enums.StringEnum;

/**
 * output class name
 */
public class complaintCategory extends StringEnum
{
    public static final String ONECOMPLAINT_VALUE = "1";//alias=1类投诉
    public static final String TWOCOMPLAINT_VALUE = "2";//alias=2类投诉
    public static final String THREECOMPLAINT_VALUE = "3";//alias=3类投诉
    public static final String FOURCOMPLAINT_VALUE = "4";//alias=4类投诉

    public static final complaintCategory oneComplaint = new complaintCategory("oneComplaint", ONECOMPLAINT_VALUE);
    public static final complaintCategory twoComplaint = new complaintCategory("twoComplaint", TWOCOMPLAINT_VALUE);
    public static final complaintCategory threeComplaint = new complaintCategory("threeComplaint", THREECOMPLAINT_VALUE);
    public static final complaintCategory fourComplaint = new complaintCategory("fourComplaint", FOURCOMPLAINT_VALUE);

    /**
     * construct function
     * @param String complaintCategory
     */
    private complaintCategory(String name, String complaintCategory)
    {
        super(name, complaintCategory);
    }
    
    /**
     * getEnum function
     * @param String arguments
     */
    public static complaintCategory getEnum(String complaintCategory)
    {
        return (complaintCategory)getEnum(complaintCategory.class, complaintCategory);
    }

    /**
     * getEnumMap function
     */
    public static Map getEnumMap()
    {
        return getEnumMap(complaintCategory.class);
    }

    /**
     * getEnumList function
     */
    public static List getEnumList()
    {
         return getEnumList(complaintCategory.class);
    }
    
    /**
     * getIterator function
     */
    public static Iterator iterator()
    {
         return iterator(complaintCategory.class);
    }
}