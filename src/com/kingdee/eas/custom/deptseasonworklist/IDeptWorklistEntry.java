package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IDeptWorklistEntry extends IHRBillBaseEntry
{
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(String oql) throws BOSException, EASBizException;
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection() throws BOSException;
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(EntityViewInfo view) throws BOSException;
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(String oql) throws BOSException;
}