package com.kingdee.eas.custom.deptseasonworklist;

import java.io.Serializable;

public class DeptWorklistInfo extends AbstractDeptWorklistInfo implements Serializable 
{
    public DeptWorklistInfo()
    {
        super();
    }
    protected DeptWorklistInfo(String pkField)
    {
        super(pkField);
    }
}