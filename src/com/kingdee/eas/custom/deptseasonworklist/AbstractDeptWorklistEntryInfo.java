package com.kingdee.eas.custom.deptseasonworklist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractDeptWorklistEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractDeptWorklistEntryInfo()
    {
        this("id");
    }
    protected AbstractDeptWorklistEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.deptseasonworklist.DeptWorklistInfo getBill()
    {
        return (com.kingdee.eas.custom.deptseasonworklist.DeptWorklistInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.deptseasonworklist.DeptWorklistInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ���۱�׼property 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object: ��¼ 's �����嵥��Ŀ property 
     */
    public com.kingdee.eas.custom.human.AssessmentIndexsInfo getNegativeListItem()
    {
        return (com.kingdee.eas.custom.human.AssessmentIndexsInfo)get("negativeListItem");
    }
    public void setNegativeListItem(com.kingdee.eas.custom.human.AssessmentIndexsInfo item)
    {
        put("negativeListItem", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("B6C39372");
    }
}