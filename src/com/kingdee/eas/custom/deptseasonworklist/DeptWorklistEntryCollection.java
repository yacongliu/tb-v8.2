package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class DeptWorklistEntryCollection extends AbstractObjectCollection 
{
    public DeptWorklistEntryCollection()
    {
        super(DeptWorklistEntryInfo.class);
    }
    public boolean add(DeptWorklistEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(DeptWorklistEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(DeptWorklistEntryInfo item)
    {
        return removeObject(item);
    }
    public DeptWorklistEntryInfo get(int index)
    {
        return(DeptWorklistEntryInfo)getObject(index);
    }
    public DeptWorklistEntryInfo get(Object key)
    {
        return(DeptWorklistEntryInfo)getObject(key);
    }
    public void set(int index, DeptWorklistEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(DeptWorklistEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(DeptWorklistEntryInfo item)
    {
        return super.indexOf(item);
    }
}