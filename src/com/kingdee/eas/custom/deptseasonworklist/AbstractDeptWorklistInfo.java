package com.kingdee.eas.custom.deptseasonworklist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractDeptWorklistInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractDeptWorklistInfo()
    {
        this("id");
    }
    protected AbstractDeptWorklistInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.deptseasonworklist.DeptWorklistEntryCollection());
    }
    /**
     * Object: ���ż��ȹ���Ŀ���嵥 's ��¼ property 
     */
    public com.kingdee.eas.custom.deptseasonworklist.DeptWorklistEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.deptseasonworklist.DeptWorklistEntryCollection)get("entrys");
    }
    /**
     * Object: ���ż��ȹ���Ŀ���嵥 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:���ż��ȹ���Ŀ���嵥's ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:���ż��ȹ���Ŀ���嵥's ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("AB8410E0");
    }
}