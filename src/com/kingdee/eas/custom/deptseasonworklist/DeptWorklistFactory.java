package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class DeptWorklistFactory
{
    private DeptWorklistFactory()
    {
    }
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("AB8410E0") ,com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist.class);
    }
    
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("AB8410E0") ,com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist.class, objectCtx);
    }
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("AB8410E0"));
    }
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklist)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("AB8410E0"));
    }
}