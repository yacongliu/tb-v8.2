package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.deptseasonworklist.app.*;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.util.enums.Enum;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class DeptWorklist extends HRBillBase implements IDeptWorklist
{
    public DeptWorklist()
    {
        super();
        registerInterface(IDeptWorklist.class, this);
    }
    public DeptWorklist(Context ctx)
    {
        super(ctx);
        registerInterface(IDeptWorklist.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("AB8410E0");
    }
    private DeptWorklistController getController() throws BOSException
    {
        return (DeptWorklistController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public DeptWorklistCollection getDeptWorklistCollection() throws BOSException
    {
        try {
            return getController().getDeptWorklistCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public DeptWorklistCollection getDeptWorklistCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getDeptWorklistCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public DeptWorklistCollection getDeptWorklistCollection(String oql) throws BOSException
    {
        try {
            return getController().getDeptWorklistCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public DeptWorklistInfo getDeptWorklistInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getDeptWorklistInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public DeptWorklistInfo getDeptWorklistInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getDeptWorklistInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public DeptWorklistInfo getDeptWorklistInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getDeptWorklistInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����״̬-User defined method
     *@param billId ����id
     *@param state ����״̬
     */
    public void setState(BOSUuid billId, Enum state) throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext(), billId, state);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��-User defined method
     *@param billId ����id
     */
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������ͨ��״̬-User defined method
     *@param billId ����id
     */
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������״̬-User defined method
     *@param billId ����id
     */
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����δ����״̬-User defined method
     *@param billId ����id
     */
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}