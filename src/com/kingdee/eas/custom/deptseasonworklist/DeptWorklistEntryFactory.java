package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class DeptWorklistEntryFactory
{
    private DeptWorklistEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("B6C39372") ,com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry.class);
    }
    
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("B6C39372") ,com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("B6C39372"));
    }
    public static com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.deptseasonworklist.IDeptWorklistEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("B6C39372"));
    }
}