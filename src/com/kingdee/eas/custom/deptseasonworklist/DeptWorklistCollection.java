package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class DeptWorklistCollection extends AbstractObjectCollection 
{
    public DeptWorklistCollection()
    {
        super(DeptWorklistInfo.class);
    }
    public boolean add(DeptWorklistInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(DeptWorklistCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(DeptWorklistInfo item)
    {
        return removeObject(item);
    }
    public DeptWorklistInfo get(int index)
    {
        return(DeptWorklistInfo)getObject(index);
    }
    public DeptWorklistInfo get(Object key)
    {
        return(DeptWorklistInfo)getObject(key);
    }
    public void set(int index, DeptWorklistInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(DeptWorklistInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(DeptWorklistInfo item)
    {
        return super.indexOf(item);
    }
}