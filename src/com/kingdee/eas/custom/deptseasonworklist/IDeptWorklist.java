package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.util.enums.Enum;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public interface IDeptWorklist extends IHRBillBase
{
    public DeptWorklistCollection getDeptWorklistCollection() throws BOSException;
    public DeptWorklistCollection getDeptWorklistCollection(EntityViewInfo view) throws BOSException;
    public DeptWorklistCollection getDeptWorklistCollection(String oql) throws BOSException;
    public DeptWorklistInfo getDeptWorklistInfo(IObjectPK pk) throws BOSException, EASBizException;
    public DeptWorklistInfo getDeptWorklistInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public DeptWorklistInfo getDeptWorklistInfo(String oql) throws BOSException, EASBizException;
    public void setState(BOSUuid billId, Enum state) throws BOSException, EASBizException;
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException;
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException;
}