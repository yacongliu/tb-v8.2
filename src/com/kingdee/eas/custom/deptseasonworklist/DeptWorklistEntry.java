package com.kingdee.eas.custom.deptseasonworklist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.deptseasonworklist.app.*;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class DeptWorklistEntry extends HRBillBaseEntry implements IDeptWorklistEntry
{
    public DeptWorklistEntry()
    {
        super();
        registerInterface(IDeptWorklistEntry.class, this);
    }
    public DeptWorklistEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IDeptWorklistEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("B6C39372");
    }
    private DeptWorklistEntryController getController() throws BOSException
    {
        return (DeptWorklistEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getDeptWorklistEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getDeptWorklistEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getDeptWorklistEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection() throws BOSException
    {
        try {
            return getController().getDeptWorklistEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getDeptWorklistEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getDeptWorklistEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}