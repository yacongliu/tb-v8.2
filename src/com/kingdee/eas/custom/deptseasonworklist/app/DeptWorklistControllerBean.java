package com.kingdee.eas.custom.deptseasonworklist.app;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.deptseasonworklist.DeptWorklistInfo;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.ApproveTypeEnum;
import com.kingdee.eas.hr.base.HRBillStateEnum;

/**
 * 
 * Title: DeptWorklistControllerBean Description: 提交工作流
 * 
 * @author saisai_cheng Email:854296216@qq.com
 * @date 2019-8-6
 */
@SuppressWarnings("serial")
public class DeptWorklistControllerBean extends
		AbstractDeptWorklistControllerBean {
	private static Logger logger = Logger
			.getLogger("com.kingdee.eas.custom.deptseasonworklist.app.DeptWorklistControllerBean");

	/**
	 * 提交生效
	 */
	@Override
	protected IObjectPK _submitEffect(Context ctx, CoreBaseInfo model)
			throws BOSException, EASBizException {

		DeptWorklistInfo bill = (DeptWorklistInfo) model;
		bill.setApproveType(ApproveTypeEnum.DIRECT);
		bill.setBillState(HRBillStateEnum.AUDITED);
		IObjectPK objectPK = super.save(ctx, model);

		_setState(ctx, bill.getId(), HRBillStateEnum.AUDITED);

		return objectPK;
	}

	/**
	 * 设置单据状态
	 */
	protected void _setState(Context ctx, BOSUuid billId, HRBillStateEnum state)
			throws BOSException, EASBizException {
		IObjectPK pk = new ObjectUuidPK(billId);
		DeptWorklistInfo info = (DeptWorklistInfo) _getValue(ctx,
				"select id, billState  where id = '" + pk.toString() + "'");

		info.setBillState(state);
		SelectorItemCollection selectors = new SelectorItemCollection();
		selectors.add(new SelectorItemInfo("id"));
		selectors.add(new SelectorItemInfo("billState"));

		updatePartial(ctx, info, selectors);
	}

	/**
	 * 设置审批中状态
	 */
	@Override
	protected void _setApproveState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITING);
	}

	/**
	 * 设置未审核状态
	 */
	@Override
	protected void _setEditState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.SAVED);
	}

	/**
	 * 设置审核不通过状态
	 */
	@Override
	protected void _setNoPassState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITEND);
	}

	/**
	 * 设置审核通过状态
	 */
	@Override
	protected void _setPassState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITEND);
	}

}