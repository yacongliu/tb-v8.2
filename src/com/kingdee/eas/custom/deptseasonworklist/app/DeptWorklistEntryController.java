package com.kingdee.eas.custom.deptseasonworklist.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.eas.custom.deptseasonworklist.DeptWorklistEntryCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.deptseasonworklist.DeptWorklistEntryInfo;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface DeptWorklistEntryController extends HRBillBaseEntryController
{
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public DeptWorklistEntryInfo getDeptWorklistEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(Context ctx) throws BOSException, RemoteException;
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public DeptWorklistEntryCollection getDeptWorklistEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}