package com.kingdee.eas.custom.deptseasonworklist;

import java.io.Serializable;

public class DeptWorklistEntryInfo extends AbstractDeptWorklistEntryInfo implements Serializable 
{
    public DeptWorklistEntryInfo()
    {
        super();
    }
    protected DeptWorklistEntryInfo(String pkField)
    {
        super(pkField);
    }
}