package com.kingdee.eas.custom.onevote;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IOneVote extends IDataBase
{
    public OneVoteInfo getOneVoteInfo(IObjectPK pk) throws BOSException, EASBizException;
    public OneVoteInfo getOneVoteInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public OneVoteInfo getOneVoteInfo(String oql) throws BOSException, EASBizException;
    public OneVoteCollection getOneVoteCollection() throws BOSException;
    public OneVoteCollection getOneVoteCollection(EntityViewInfo view) throws BOSException;
    public OneVoteCollection getOneVoteCollection(String oql) throws BOSException;
}