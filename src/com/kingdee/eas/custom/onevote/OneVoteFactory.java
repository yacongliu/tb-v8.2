package com.kingdee.eas.custom.onevote;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class OneVoteFactory
{
    private OneVoteFactory()
    {
    }
    public static com.kingdee.eas.custom.onevote.IOneVote getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.onevote.IOneVote)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("ECB3A5ED") ,com.kingdee.eas.custom.onevote.IOneVote.class);
    }
    
    public static com.kingdee.eas.custom.onevote.IOneVote getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.onevote.IOneVote)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("ECB3A5ED") ,com.kingdee.eas.custom.onevote.IOneVote.class, objectCtx);
    }
    public static com.kingdee.eas.custom.onevote.IOneVote getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.onevote.IOneVote)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("ECB3A5ED"));
    }
    public static com.kingdee.eas.custom.onevote.IOneVote getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.onevote.IOneVote)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("ECB3A5ED"));
    }
}