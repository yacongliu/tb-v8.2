package com.kingdee.eas.custom.onevote;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.onevote.app.*;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class OneVote extends DataBase implements IOneVote
{
    public OneVote()
    {
        super();
        registerInterface(IOneVote.class, this);
    }
    public OneVote(Context ctx)
    {
        super(ctx);
        registerInterface(IOneVote.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("ECB3A5ED");
    }
    private OneVoteController getController() throws BOSException
    {
        return (OneVoteController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public OneVoteInfo getOneVoteInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getOneVoteInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public OneVoteInfo getOneVoteInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getOneVoteInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public OneVoteInfo getOneVoteInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getOneVoteInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public OneVoteCollection getOneVoteCollection() throws BOSException
    {
        try {
            return getController().getOneVoteCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public OneVoteCollection getOneVoteCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getOneVoteCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public OneVoteCollection getOneVoteCollection(String oql) throws BOSException
    {
        try {
            return getController().getOneVoteCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}