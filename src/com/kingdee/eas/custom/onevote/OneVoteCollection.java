package com.kingdee.eas.custom.onevote;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class OneVoteCollection extends AbstractObjectCollection 
{
    public OneVoteCollection()
    {
        super(OneVoteInfo.class);
    }
    public boolean add(OneVoteInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(OneVoteCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(OneVoteInfo item)
    {
        return removeObject(item);
    }
    public OneVoteInfo get(int index)
    {
        return(OneVoteInfo)getObject(index);
    }
    public OneVoteInfo get(Object key)
    {
        return(OneVoteInfo)getObject(key);
    }
    public void set(int index, OneVoteInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(OneVoteInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(OneVoteInfo item)
    {
        return super.indexOf(item);
    }
}