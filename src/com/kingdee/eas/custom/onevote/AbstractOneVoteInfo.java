package com.kingdee.eas.custom.onevote;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractOneVoteInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractOneVoteInfo()
    {
        this("id");
    }
    protected AbstractOneVoteInfo(String pkField)
    {
        super(pkField);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("ECB3A5ED");
    }
}