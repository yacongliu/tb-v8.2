package com.kingdee.eas.custom.onevote;

import java.io.Serializable;

public class OneVoteInfo extends AbstractOneVoteInfo implements Serializable 
{
    public OneVoteInfo()
    {
        super();
    }
    protected OneVoteInfo(String pkField)
    {
        super(pkField);
    }
}