package com.kingdee.eas.custom.buscostcount;

import java.io.Serializable;

public class BusCostCountInfo extends AbstractBusCostCountInfo implements Serializable 
{
    public BusCostCountInfo()
    {
        super();
    }
    protected BusCostCountInfo(String pkField)
    {
        super(pkField);
    }
}