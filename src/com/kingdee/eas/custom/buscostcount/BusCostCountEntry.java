package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.custom.buscostcount.app.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class BusCostCountEntry extends HRBillBaseEntry implements IBusCostCountEntry
{
    public BusCostCountEntry()
    {
        super();
        registerInterface(IBusCostCountEntry.class, this);
    }
    public BusCostCountEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IBusCostCountEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("F066102F");
    }
    private BusCostCountEntryController getController() throws BOSException
    {
        return (BusCostCountEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public BusCostCountEntryInfo getBusCostCountEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getBusCostCountEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public BusCostCountEntryInfo getBusCostCountEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getBusCostCountEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public BusCostCountEntryInfo getBusCostCountEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getBusCostCountEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public BusCostCountEntryCollection getBusCostCountEntryCollection() throws BOSException
    {
        try {
            return getController().getBusCostCountEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public BusCostCountEntryCollection getBusCostCountEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getBusCostCountEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public BusCostCountEntryCollection getBusCostCountEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getBusCostCountEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}