package com.kingdee.eas.custom.buscostcount;

import java.io.Serializable;

public class BusCostCountEntryInfo extends AbstractBusCostCountEntryInfo implements Serializable 
{
    public BusCostCountEntryInfo()
    {
        super();
    }
    protected BusCostCountEntryInfo(String pkField)
    {
        super(pkField);
    }
}