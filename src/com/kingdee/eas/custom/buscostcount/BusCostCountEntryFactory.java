package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class BusCostCountEntryFactory
{
    private BusCostCountEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.buscostcount.IBusCostCountEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCountEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("F066102F") ,com.kingdee.eas.custom.buscostcount.IBusCostCountEntry.class);
    }
    
    public static com.kingdee.eas.custom.buscostcount.IBusCostCountEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCountEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("F066102F") ,com.kingdee.eas.custom.buscostcount.IBusCostCountEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.buscostcount.IBusCostCountEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCountEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("F066102F"));
    }
    public static com.kingdee.eas.custom.buscostcount.IBusCostCountEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCountEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("F066102F"));
    }
}