package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IAdjustType extends IDataBase
{
    public AdjustTypeInfo getAdjustTypeInfo(IObjectPK pk) throws BOSException, EASBizException;
    public AdjustTypeInfo getAdjustTypeInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public AdjustTypeInfo getAdjustTypeInfo(String oql) throws BOSException, EASBizException;
    public AdjustTypeCollection getAdjustTypeCollection() throws BOSException;
    public AdjustTypeCollection getAdjustTypeCollection(EntityViewInfo view) throws BOSException;
    public AdjustTypeCollection getAdjustTypeCollection(String oql) throws BOSException;
}