package com.kingdee.eas.custom.buscostcount;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractBusCostCountEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractBusCostCountEntryInfo()
    {
        this("id");
    }
    protected AbstractBusCostCountEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.buscostcount.BusCostCountInfo getBill()
    {
        return (com.kingdee.eas.custom.buscostcount.BusCostCountInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.buscostcount.BusCostCountInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��¼'s ����ǰ�೵��property 
     */
    public String getBeforeAdjust()
    {
        return getString("BeforeAdjust");
    }
    public void setBeforeAdjust(String item)
    {
        setString("BeforeAdjust", item);
    }
    /**
     * Object:��¼'s ������೵��property 
     */
    public String getAfterAdjust()
    {
        return getString("AfterAdjust");
    }
    public void setAfterAdjust(String item)
    {
        setString("AfterAdjust", item);
    }
    /**
     * Object:��¼'s �������˵��property 
     */
    public String getAdjustDes()
    {
        return getString("AdjustDes");
    }
    public void setAdjustDes(String item)
    {
        setString("AdjustDes", item);
    }
    /**
     * Object: ��¼ 's �������� property 
     */
    public com.kingdee.eas.custom.buscostcount.AdjustTypeInfo getAdjustType()
    {
        return (com.kingdee.eas.custom.buscostcount.AdjustTypeInfo)get("AdjustType");
    }
    public void setAdjustType(com.kingdee.eas.custom.buscostcount.AdjustTypeInfo item)
    {
        put("AdjustType", item);
    }
    /**
     * Object: ��¼ 's ���� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPersonName()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("PersonName");
    }
    public void setPersonName(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("PersonName", item);
    }
    /**
     * Object:��¼'s �����·�property 
     */
    public int getAdjustMonth()
    {
        return getInt("AdjustMonth");
    }
    public void setAdjustMonth(int item)
    {
        setInt("AdjustMonth", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("F066102F");
    }
}