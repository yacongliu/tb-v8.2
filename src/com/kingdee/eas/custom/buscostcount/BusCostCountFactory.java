package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class BusCostCountFactory
{
    private BusCostCountFactory()
    {
    }
    public static com.kingdee.eas.custom.buscostcount.IBusCostCount getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCount)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("894BD203") ,com.kingdee.eas.custom.buscostcount.IBusCostCount.class);
    }
    
    public static com.kingdee.eas.custom.buscostcount.IBusCostCount getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCount)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("894BD203") ,com.kingdee.eas.custom.buscostcount.IBusCostCount.class, objectCtx);
    }
    public static com.kingdee.eas.custom.buscostcount.IBusCostCount getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCount)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("894BD203"));
    }
    public static com.kingdee.eas.custom.buscostcount.IBusCostCount getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IBusCostCount)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("894BD203"));
    }
}