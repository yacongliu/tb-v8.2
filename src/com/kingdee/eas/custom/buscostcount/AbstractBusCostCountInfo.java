package com.kingdee.eas.custom.buscostcount;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractBusCostCountInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractBusCostCountInfo()
    {
        this("id");
    }
    protected AbstractBusCostCountInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.buscostcount.BusCostCountEntryCollection());
    }
    /**
     * Object: �೵�ѱ䶯����� 's ��¼ property 
     */
    public com.kingdee.eas.custom.buscostcount.BusCostCountEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.buscostcount.BusCostCountEntryCollection)get("entrys");
    }
    /**
     * Object: �೵�ѱ䶯����� 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:�೵�ѱ䶯�����'s ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:�೵�ѱ䶯�����'s ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    /**
     * Object:�೵�ѱ䶯�����'s �䶯�·�property 
     */
    public int getChangeMonth()
    {
        return getInt("changeMonth");
    }
    public void setChangeMonth(int item)
    {
        setInt("changeMonth", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("894BD203");
    }
}