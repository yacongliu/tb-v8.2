package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.custom.buscostcount.app.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class AdjustType extends DataBase implements IAdjustType
{
    public AdjustType()
    {
        super();
        registerInterface(IAdjustType.class, this);
    }
    public AdjustType(Context ctx)
    {
        super(ctx);
        registerInterface(IAdjustType.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C905970A");
    }
    private AdjustTypeController getController() throws BOSException
    {
        return (AdjustTypeController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public AdjustTypeInfo getAdjustTypeInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAdjustTypeInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public AdjustTypeInfo getAdjustTypeInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAdjustTypeInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public AdjustTypeInfo getAdjustTypeInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAdjustTypeInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public AdjustTypeCollection getAdjustTypeCollection() throws BOSException
    {
        try {
            return getController().getAdjustTypeCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public AdjustTypeCollection getAdjustTypeCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAdjustTypeCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public AdjustTypeCollection getAdjustTypeCollection(String oql) throws BOSException
    {
        try {
            return getController().getAdjustTypeCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}