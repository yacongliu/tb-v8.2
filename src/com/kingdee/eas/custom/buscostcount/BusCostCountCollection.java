package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class BusCostCountCollection extends AbstractObjectCollection 
{
    public BusCostCountCollection()
    {
        super(BusCostCountInfo.class);
    }
    public boolean add(BusCostCountInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(BusCostCountCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(BusCostCountInfo item)
    {
        return removeObject(item);
    }
    public BusCostCountInfo get(int index)
    {
        return(BusCostCountInfo)getObject(index);
    }
    public BusCostCountInfo get(Object key)
    {
        return(BusCostCountInfo)getObject(key);
    }
    public void set(int index, BusCostCountInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(BusCostCountInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(BusCostCountInfo item)
    {
        return super.indexOf(item);
    }
}