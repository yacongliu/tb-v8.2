package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AdjustTypeFactory
{
    private AdjustTypeFactory()
    {
    }
    public static com.kingdee.eas.custom.buscostcount.IAdjustType getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IAdjustType)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C905970A") ,com.kingdee.eas.custom.buscostcount.IAdjustType.class);
    }
    
    public static com.kingdee.eas.custom.buscostcount.IAdjustType getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IAdjustType)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C905970A") ,com.kingdee.eas.custom.buscostcount.IAdjustType.class, objectCtx);
    }
    public static com.kingdee.eas.custom.buscostcount.IAdjustType getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IAdjustType)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C905970A"));
    }
    public static com.kingdee.eas.custom.buscostcount.IAdjustType getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.buscostcount.IAdjustType)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C905970A"));
    }
}