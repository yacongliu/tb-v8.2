package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IBusCostCountEntry extends IHRBillBaseEntry
{
    public BusCostCountEntryInfo getBusCostCountEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public BusCostCountEntryInfo getBusCostCountEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public BusCostCountEntryInfo getBusCostCountEntryInfo(String oql) throws BOSException, EASBizException;
    public BusCostCountEntryCollection getBusCostCountEntryCollection() throws BOSException;
    public BusCostCountEntryCollection getBusCostCountEntryCollection(EntityViewInfo view) throws BOSException;
    public BusCostCountEntryCollection getBusCostCountEntryCollection(String oql) throws BOSException;
}