package com.kingdee.eas.custom.buscostcount;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class BusCostCountEntryCollection extends AbstractObjectCollection 
{
    public BusCostCountEntryCollection()
    {
        super(BusCostCountEntryInfo.class);
    }
    public boolean add(BusCostCountEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(BusCostCountEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(BusCostCountEntryInfo item)
    {
        return removeObject(item);
    }
    public BusCostCountEntryInfo get(int index)
    {
        return(BusCostCountEntryInfo)getObject(index);
    }
    public BusCostCountEntryInfo get(Object key)
    {
        return(BusCostCountEntryInfo)getObject(key);
    }
    public void set(int index, BusCostCountEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(BusCostCountEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(BusCostCountEntryInfo item)
    {
        return super.indexOf(item);
    }
}