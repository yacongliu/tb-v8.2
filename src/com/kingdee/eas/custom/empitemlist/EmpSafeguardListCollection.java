package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class EmpSafeguardListCollection extends AbstractObjectCollection 
{
    public EmpSafeguardListCollection()
    {
        super(EmpSafeguardListInfo.class);
    }
    public boolean add(EmpSafeguardListInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(EmpSafeguardListCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(EmpSafeguardListInfo item)
    {
        return removeObject(item);
    }
    public EmpSafeguardListInfo get(int index)
    {
        return(EmpSafeguardListInfo)getObject(index);
    }
    public EmpSafeguardListInfo get(Object key)
    {
        return(EmpSafeguardListInfo)getObject(key);
    }
    public void set(int index, EmpSafeguardListInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(EmpSafeguardListInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(EmpSafeguardListInfo item)
    {
        return super.indexOf(item);
    }
}