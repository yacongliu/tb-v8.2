package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.eas.custom.empitemlist.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class EmpItemListEntry extends HRBillBaseEntry implements IEmpItemListEntry
{
    public EmpItemListEntry()
    {
        super();
        registerInterface(IEmpItemListEntry.class, this);
    }
    public EmpItemListEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IEmpItemListEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("CF0024D3");
    }
    private EmpItemListEntryController getController() throws BOSException
    {
        return (EmpItemListEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public EmpItemListEntryInfo getEmpItemListEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpItemListEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public EmpItemListEntryInfo getEmpItemListEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpItemListEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public EmpItemListEntryInfo getEmpItemListEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpItemListEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public EmpItemListEntryCollection getEmpItemListEntryCollection() throws BOSException
    {
        try {
            return getController().getEmpItemListEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public EmpItemListEntryCollection getEmpItemListEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getEmpItemListEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public EmpItemListEntryCollection getEmpItemListEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getEmpItemListEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}