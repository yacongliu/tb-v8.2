package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SafeguardTypeFactory
{
    private SafeguardTypeFactory()
    {
    }
    public static com.kingdee.eas.custom.empitemlist.ISafeguardType getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.ISafeguardType)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("449C48B8") ,com.kingdee.eas.custom.empitemlist.ISafeguardType.class);
    }
    
    public static com.kingdee.eas.custom.empitemlist.ISafeguardType getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.ISafeguardType)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("449C48B8") ,com.kingdee.eas.custom.empitemlist.ISafeguardType.class, objectCtx);
    }
    public static com.kingdee.eas.custom.empitemlist.ISafeguardType getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.ISafeguardType)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("449C48B8"));
    }
    public static com.kingdee.eas.custom.empitemlist.ISafeguardType getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.ISafeguardType)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("449C48B8"));
    }
}