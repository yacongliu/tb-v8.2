package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.eas.custom.empitemlist.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class EmpSafeguardListEntry extends HRBillBaseEntry implements IEmpSafeguardListEntry
{
    public EmpSafeguardListEntry()
    {
        super();
        registerInterface(IEmpSafeguardListEntry.class, this);
    }
    public EmpSafeguardListEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IEmpSafeguardListEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("1513792A");
    }
    private EmpSafeguardListEntryController getController() throws BOSException
    {
        return (EmpSafeguardListEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public EmpSafeguardListEntryInfo getEmpSafeguardListEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpSafeguardListEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public EmpSafeguardListEntryInfo getEmpSafeguardListEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpSafeguardListEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public EmpSafeguardListEntryInfo getEmpSafeguardListEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpSafeguardListEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public EmpSafeguardListEntryCollection getEmpSafeguardListEntryCollection() throws BOSException
    {
        try {
            return getController().getEmpSafeguardListEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public EmpSafeguardListEntryCollection getEmpSafeguardListEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getEmpSafeguardListEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public EmpSafeguardListEntryCollection getEmpSafeguardListEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getEmpSafeguardListEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}