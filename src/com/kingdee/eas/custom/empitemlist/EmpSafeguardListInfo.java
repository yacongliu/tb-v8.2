package com.kingdee.eas.custom.empitemlist;

import java.io.Serializable;

public class EmpSafeguardListInfo extends AbstractEmpSafeguardListInfo implements Serializable 
{
    public EmpSafeguardListInfo()
    {
        super();
    }
    protected EmpSafeguardListInfo(String pkField)
    {
        super(pkField);
    }
}