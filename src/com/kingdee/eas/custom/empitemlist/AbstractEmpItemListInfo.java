package com.kingdee.eas.custom.empitemlist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractEmpItemListInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractEmpItemListInfo()
    {
        this("id");
    }
    protected AbstractEmpItemListInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.empitemlist.EmpItemListEntryCollection());
    }
    /**
     * Object: �����嵥�б� 's ��¼ property 
     */
    public com.kingdee.eas.custom.empitemlist.EmpItemListEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.empitemlist.EmpItemListEntryCollection)get("entrys");
    }
    /**
     * Object: �����嵥�б� 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:�����嵥�б�'s ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:�����嵥�б�'s ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("46D866DF");
    }
}