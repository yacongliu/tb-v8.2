package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class EmpItemListEntryFactory
{
    private EmpItemListEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpItemListEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemListEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("CF0024D3") ,com.kingdee.eas.custom.empitemlist.IEmpItemListEntry.class);
    }
    
    public static com.kingdee.eas.custom.empitemlist.IEmpItemListEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemListEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("CF0024D3") ,com.kingdee.eas.custom.empitemlist.IEmpItemListEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpItemListEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemListEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("CF0024D3"));
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpItemListEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemListEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("CF0024D3"));
    }
}