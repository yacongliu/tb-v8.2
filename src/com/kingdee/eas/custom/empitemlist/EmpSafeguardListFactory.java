package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class EmpSafeguardListFactory
{
    private EmpSafeguardListFactory()
    {
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardList getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardList)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("48597828") ,com.kingdee.eas.custom.empitemlist.IEmpSafeguardList.class);
    }
    
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardList getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardList)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("48597828") ,com.kingdee.eas.custom.empitemlist.IEmpSafeguardList.class, objectCtx);
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardList getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardList)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("48597828"));
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardList getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardList)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("48597828"));
    }
}