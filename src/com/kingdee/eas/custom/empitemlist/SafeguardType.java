package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.empitemlist.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class SafeguardType extends DataBase implements ISafeguardType
{
    public SafeguardType()
    {
        super();
        registerInterface(ISafeguardType.class, this);
    }
    public SafeguardType(Context ctx)
    {
        super(ctx);
        registerInterface(ISafeguardType.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("449C48B8");
    }
    private SafeguardTypeController getController() throws BOSException
    {
        return (SafeguardTypeController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public SafeguardTypeInfo getSafeguardTypeInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getSafeguardTypeInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public SafeguardTypeInfo getSafeguardTypeInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getSafeguardTypeInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public SafeguardTypeInfo getSafeguardTypeInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getSafeguardTypeInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public SafeguardTypeCollection getSafeguardTypeCollection() throws BOSException
    {
        try {
            return getController().getSafeguardTypeCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public SafeguardTypeCollection getSafeguardTypeCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getSafeguardTypeCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public SafeguardTypeCollection getSafeguardTypeCollection(String oql) throws BOSException
    {
        try {
            return getController().getSafeguardTypeCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}