package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IEmpSafeguardList extends IHRBillBase
{
    public EmpSafeguardListCollection getEmpSafeguardListCollection() throws BOSException;
    public EmpSafeguardListCollection getEmpSafeguardListCollection(EntityViewInfo view) throws BOSException;
    public EmpSafeguardListCollection getEmpSafeguardListCollection(String oql) throws BOSException;
    public EmpSafeguardListInfo getEmpSafeguardListInfo(IObjectPK pk) throws BOSException, EASBizException;
    public EmpSafeguardListInfo getEmpSafeguardListInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public EmpSafeguardListInfo getEmpSafeguardListInfo(String oql) throws BOSException, EASBizException;
}