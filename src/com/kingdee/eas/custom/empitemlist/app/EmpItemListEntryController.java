package com.kingdee.eas.custom.empitemlist.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.eas.custom.empitemlist.EmpItemListEntryInfo;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.empitemlist.EmpItemListEntryCollection;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface EmpItemListEntryController extends HRBillBaseEntryController
{
    public EmpItemListEntryInfo getEmpItemListEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public EmpItemListEntryInfo getEmpItemListEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public EmpItemListEntryInfo getEmpItemListEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public EmpItemListEntryCollection getEmpItemListEntryCollection(Context ctx) throws BOSException, RemoteException;
    public EmpItemListEntryCollection getEmpItemListEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public EmpItemListEntryCollection getEmpItemListEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}