package com.kingdee.eas.custom.empitemlist.app;

import org.apache.log4j.Logger;
import javax.ejb.*;
import java.rmi.RemoteException;
import com.kingdee.bos.*;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.rule.RuleExecutor;
import com.kingdee.bos.metadata.MetaDataPK;
//import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.framework.ejb.AbstractEntityControllerBean;
import com.kingdee.bos.framework.ejb.AbstractBizControllerBean;
//import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.dao.IObjectCollection;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.bos.service.IServiceContext;

import com.kingdee.eas.custom.empitemlist.EmpSafeguardListCollection;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.framework.ObjectBaseCollection;
import java.lang.String;
import com.kingdee.eas.custom.empitemlist.EmpSafeguardListInfo;
import com.kingdee.eas.framework.CoreBillBaseCollection;
import com.kingdee.eas.hr.base.ApproveTypeEnum;
import com.kingdee.eas.hr.base.HRBillBaseCollection;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseControllerBean;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class EmpSafeguardListControllerBean extends AbstractEmpSafeguardListControllerBean
{
    private static Logger logger =
        Logger.getLogger("com.kingdee.eas.custom.empitemlist.app.EmpSafeguardListControllerBean");
    /**
	 * 提交生效
	 */
	@Override
	protected IObjectPK _submitEffect(Context ctx, CoreBaseInfo model)
			throws BOSException, EASBizException {

		EmpSafeguardListInfo bill = (EmpSafeguardListInfo) model;
		bill.setApproveType(ApproveTypeEnum.DIRECT);
		bill.setBillState(HRBillStateEnum.AUDITED);
		IObjectPK objectPK = super.save(ctx, model);

		_setState(ctx, bill.getId(), HRBillStateEnum.AUDITED);

		return objectPK;
	}

	/**
	 * 设置单据状态
	 */
	@Override
	protected void _setState(Context ctx, BOSUuid billId, HRBillStateEnum state)
			throws BOSException, EASBizException {
		IObjectPK pk = new ObjectUuidPK(billId);
		EmpSafeguardListInfo info = (EmpSafeguardListInfo) _getValue(ctx,
				"select id, billState  where id = '" + pk.toString() + "'");

		info.setBillState(state);
		SelectorItemCollection selectors = new SelectorItemCollection();
		selectors.add(new SelectorItemInfo("id"));
		selectors.add(new SelectorItemInfo("billState"));

		updatePartial(ctx, info, selectors);
	}

	/**
	 * 设置审批中状态
	 */
	@Override
	protected void _setApproveState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITING);
	}

	/**
	 * 设置未审核状态
	 */
	@Override
	protected void _setEditState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.SAVED);
	}

	/**
	 * 设置审核不通过状态
	 */
	@Override
	protected void _setNoPassState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITEND);
	}

	/**
	 * 设置审核通过状态
	 */
	@Override
	protected void _setPassState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITEND);
	}
    
    
}