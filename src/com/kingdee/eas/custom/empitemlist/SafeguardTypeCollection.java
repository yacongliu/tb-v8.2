package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SafeguardTypeCollection extends AbstractObjectCollection 
{
    public SafeguardTypeCollection()
    {
        super(SafeguardTypeInfo.class);
    }
    public boolean add(SafeguardTypeInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SafeguardTypeCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SafeguardTypeInfo item)
    {
        return removeObject(item);
    }
    public SafeguardTypeInfo get(int index)
    {
        return(SafeguardTypeInfo)getObject(index);
    }
    public SafeguardTypeInfo get(Object key)
    {
        return(SafeguardTypeInfo)getObject(key);
    }
    public void set(int index, SafeguardTypeInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SafeguardTypeInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SafeguardTypeInfo item)
    {
        return super.indexOf(item);
    }
}