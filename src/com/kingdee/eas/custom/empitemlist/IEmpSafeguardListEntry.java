package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IEmpSafeguardListEntry extends IHRBillBaseEntry
{
    public EmpSafeguardListEntryInfo getEmpSafeguardListEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public EmpSafeguardListEntryInfo getEmpSafeguardListEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public EmpSafeguardListEntryInfo getEmpSafeguardListEntryInfo(String oql) throws BOSException, EASBizException;
    public EmpSafeguardListEntryCollection getEmpSafeguardListEntryCollection() throws BOSException;
    public EmpSafeguardListEntryCollection getEmpSafeguardListEntryCollection(EntityViewInfo view) throws BOSException;
    public EmpSafeguardListEntryCollection getEmpSafeguardListEntryCollection(String oql) throws BOSException;
}