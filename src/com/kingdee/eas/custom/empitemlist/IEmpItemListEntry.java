package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IEmpItemListEntry extends IHRBillBaseEntry
{
    public EmpItemListEntryInfo getEmpItemListEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public EmpItemListEntryInfo getEmpItemListEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public EmpItemListEntryInfo getEmpItemListEntryInfo(String oql) throws BOSException, EASBizException;
    public EmpItemListEntryCollection getEmpItemListEntryCollection() throws BOSException;
    public EmpItemListEntryCollection getEmpItemListEntryCollection(EntityViewInfo view) throws BOSException;
    public EmpItemListEntryCollection getEmpItemListEntryCollection(String oql) throws BOSException;
}