package com.kingdee.eas.custom.empitemlist;

import java.io.Serializable;

public class EmpSafeguardListEntryInfo extends AbstractEmpSafeguardListEntryInfo implements Serializable 
{
    public EmpSafeguardListEntryInfo()
    {
        super();
    }
    protected EmpSafeguardListEntryInfo(String pkField)
    {
        super(pkField);
    }
}