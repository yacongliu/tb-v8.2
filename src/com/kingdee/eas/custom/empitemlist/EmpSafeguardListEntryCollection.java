package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class EmpSafeguardListEntryCollection extends AbstractObjectCollection 
{
    public EmpSafeguardListEntryCollection()
    {
        super(EmpSafeguardListEntryInfo.class);
    }
    public boolean add(EmpSafeguardListEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(EmpSafeguardListEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(EmpSafeguardListEntryInfo item)
    {
        return removeObject(item);
    }
    public EmpSafeguardListEntryInfo get(int index)
    {
        return(EmpSafeguardListEntryInfo)getObject(index);
    }
    public EmpSafeguardListEntryInfo get(Object key)
    {
        return(EmpSafeguardListEntryInfo)getObject(key);
    }
    public void set(int index, EmpSafeguardListEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(EmpSafeguardListEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(EmpSafeguardListEntryInfo item)
    {
        return super.indexOf(item);
    }
}