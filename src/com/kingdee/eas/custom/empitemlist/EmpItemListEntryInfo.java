package com.kingdee.eas.custom.empitemlist;

import java.io.Serializable;

public class EmpItemListEntryInfo extends AbstractEmpItemListEntryInfo implements Serializable 
{
    public EmpItemListEntryInfo()
    {
        super();
    }
    protected EmpItemListEntryInfo(String pkField)
    {
        super(pkField);
    }
}