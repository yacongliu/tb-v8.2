package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.empitemlist.app.*;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class EmpItemList extends HRBillBase implements IEmpItemList
{
    public EmpItemList()
    {
        super();
        registerInterface(IEmpItemList.class, this);
    }
    public EmpItemList(Context ctx)
    {
        super(ctx);
        registerInterface(IEmpItemList.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("46D866DF");
    }
    private EmpItemListController getController() throws BOSException
    {
        return (EmpItemListController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public EmpItemListCollection getEmpItemListCollection() throws BOSException
    {
        try {
            return getController().getEmpItemListCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public EmpItemListCollection getEmpItemListCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getEmpItemListCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public EmpItemListCollection getEmpItemListCollection(String oql) throws BOSException
    {
        try {
            return getController().getEmpItemListCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public EmpItemListInfo getEmpItemListInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpItemListInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public EmpItemListInfo getEmpItemListInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpItemListInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public EmpItemListInfo getEmpItemListInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpItemListInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����״̬-User defined method
     *@param billId ����id
     *@param state ����״̬
     */
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext(), billId, state);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��-User defined method
     *@param billId ����id
     */
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������ͨ��״̬-User defined method
     *@param billId ����id
     */
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������״̬-User defined method
     *@param billId ����id
     */
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����δ����״̬-User defined method
     *@param billId ����id
     */
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}