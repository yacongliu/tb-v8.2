package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface ISafeguardType extends IDataBase
{
    public SafeguardTypeInfo getSafeguardTypeInfo(IObjectPK pk) throws BOSException, EASBizException;
    public SafeguardTypeInfo getSafeguardTypeInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public SafeguardTypeInfo getSafeguardTypeInfo(String oql) throws BOSException, EASBizException;
    public SafeguardTypeCollection getSafeguardTypeCollection() throws BOSException;
    public SafeguardTypeCollection getSafeguardTypeCollection(EntityViewInfo view) throws BOSException;
    public SafeguardTypeCollection getSafeguardTypeCollection(String oql) throws BOSException;
}