package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class EmpItemListFactory
{
    private EmpItemListFactory()
    {
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpItemList getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemList)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("46D866DF") ,com.kingdee.eas.custom.empitemlist.IEmpItemList.class);
    }
    
    public static com.kingdee.eas.custom.empitemlist.IEmpItemList getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemList)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("46D866DF") ,com.kingdee.eas.custom.empitemlist.IEmpItemList.class, objectCtx);
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpItemList getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemList)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("46D866DF"));
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpItemList getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpItemList)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("46D866DF"));
    }
}