package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class EmpSafeguardListEntryFactory
{
    private EmpSafeguardListEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("1513792A") ,com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry.class);
    }
    
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("1513792A") ,com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("1513792A"));
    }
    public static com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.empitemlist.IEmpSafeguardListEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("1513792A"));
    }
}