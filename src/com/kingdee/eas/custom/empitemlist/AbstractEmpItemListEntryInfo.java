package com.kingdee.eas.custom.empitemlist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractEmpItemListEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractEmpItemListEntryInfo()
    {
        this("id");
    }
    protected AbstractEmpItemListEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.empitemlist.EmpItemListInfo getBill()
    {
        return (com.kingdee.eas.custom.empitemlist.EmpItemListInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.empitemlist.EmpItemListInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ���۱�׼property 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��¼'s ������Ŀproperty 
     */
    public String getExamProject()
    {
        return getString("examProject");
    }
    public void setExamProject(String item)
    {
        setString("examProject", item);
    }
    /**
     * Object:��¼'s ָ������property 
     */
    public String getItemDesc()
    {
        return getString("itemDesc");
    }
    public void setItemDesc(String item)
    {
        setString("itemDesc", item);
    }
    /**
     * Object:��¼'s ָ�����property 
     */
    public String getItemNum()
    {
        return getString("itemNum");
    }
    public void setItemNum(String item)
    {
        setString("itemNum", item);
    }
    /**
     * Object: ��¼ 's ָ������ property 
     */
    public com.kingdee.eas.hr.perf.PerformTargetItemInfo getItemType1()
    {
        return (com.kingdee.eas.hr.perf.PerformTargetItemInfo)get("itemType1");
    }
    public void setItemType1(com.kingdee.eas.hr.perf.PerformTargetItemInfo item)
    {
        put("itemType1", item);
    }
    /**
     * Object: ��¼ 's ָ������ property 
     */
    public com.kingdee.eas.hr.perf.PerformTargetTypeInfo getItemlistType()
    {
        return (com.kingdee.eas.hr.perf.PerformTargetTypeInfo)get("itemlistType");
    }
    public void setItemlistType(com.kingdee.eas.hr.perf.PerformTargetTypeInfo item)
    {
        put("itemlistType", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("CF0024D3");
    }
}