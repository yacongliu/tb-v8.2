package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class EmpItemListEntryCollection extends AbstractObjectCollection 
{
    public EmpItemListEntryCollection()
    {
        super(EmpItemListEntryInfo.class);
    }
    public boolean add(EmpItemListEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(EmpItemListEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(EmpItemListEntryInfo item)
    {
        return removeObject(item);
    }
    public EmpItemListEntryInfo get(int index)
    {
        return(EmpItemListEntryInfo)getObject(index);
    }
    public EmpItemListEntryInfo get(Object key)
    {
        return(EmpItemListEntryInfo)getObject(key);
    }
    public void set(int index, EmpItemListEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(EmpItemListEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(EmpItemListEntryInfo item)
    {
        return super.indexOf(item);
    }
}