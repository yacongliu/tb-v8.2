package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class EmpItemListCollection extends AbstractObjectCollection 
{
    public EmpItemListCollection()
    {
        super(EmpItemListInfo.class);
    }
    public boolean add(EmpItemListInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(EmpItemListCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(EmpItemListInfo item)
    {
        return removeObject(item);
    }
    public EmpItemListInfo get(int index)
    {
        return(EmpItemListInfo)getObject(index);
    }
    public EmpItemListInfo get(Object key)
    {
        return(EmpItemListInfo)getObject(key);
    }
    public void set(int index, EmpItemListInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(EmpItemListInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(EmpItemListInfo item)
    {
        return super.indexOf(item);
    }
}