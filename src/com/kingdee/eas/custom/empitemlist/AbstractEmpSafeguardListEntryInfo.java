package com.kingdee.eas.custom.empitemlist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractEmpSafeguardListEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractEmpSafeguardListEntryInfo()
    {
        this("id");
    }
    protected AbstractEmpSafeguardListEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.empitemlist.EmpSafeguardListInfo getBill()
    {
        return (com.kingdee.eas.custom.empitemlist.EmpSafeguardListInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.empitemlist.EmpSafeguardListInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object: ��¼ 's ά������ property 
     */
    public com.kingdee.eas.custom.empitemlist.SafeguardTypeInfo getSafeguardType()
    {
        return (com.kingdee.eas.custom.empitemlist.SafeguardTypeInfo)get("safeguardType");
    }
    public void setSafeguardType(com.kingdee.eas.custom.empitemlist.SafeguardTypeInfo item)
    {
        put("safeguardType", item);
    }
    /**
     * Object:��¼'s ���۱�׼property 
     */
    public String getEvalStandard()
    {
        return getString("evalStandard");
    }
    public void setEvalStandard(String item)
    {
        setString("evalStandard", item);
    }
    /**
     * Object:��¼'s ״̬property 
     */
    public boolean isState()
    {
        return getBoolean("state");
    }
    public void setState(boolean item)
    {
        setBoolean("state", item);
    }
    /**
     * Object: ��¼ 's ������Ŀ property 
     */
    public com.kingdee.eas.custom.human.AssessmentIndexsInfo getExamProjects()
    {
        return (com.kingdee.eas.custom.human.AssessmentIndexsInfo)get("examProjects");
    }
    public void setExamProjects(com.kingdee.eas.custom.human.AssessmentIndexsInfo item)
    {
        put("examProjects", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("1513792A");
    }
}