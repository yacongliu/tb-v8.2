package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public interface IEmpItemList extends IHRBillBase
{
    public EmpItemListCollection getEmpItemListCollection() throws BOSException;
    public EmpItemListCollection getEmpItemListCollection(EntityViewInfo view) throws BOSException;
    public EmpItemListCollection getEmpItemListCollection(String oql) throws BOSException;
    public EmpItemListInfo getEmpItemListInfo(IObjectPK pk) throws BOSException, EASBizException;
    public EmpItemListInfo getEmpItemListInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public EmpItemListInfo getEmpItemListInfo(String oql) throws BOSException, EASBizException;
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException;
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException;
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException;
}