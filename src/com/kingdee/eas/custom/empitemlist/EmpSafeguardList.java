package com.kingdee.eas.custom.empitemlist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.empitemlist.app.*;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class EmpSafeguardList extends HRBillBase implements IEmpSafeguardList
{
    public EmpSafeguardList()
    {
        super();
        registerInterface(IEmpSafeguardList.class, this);
    }
    public EmpSafeguardList(Context ctx)
    {
        super(ctx);
        registerInterface(IEmpSafeguardList.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("48597828");
    }
    private EmpSafeguardListController getController() throws BOSException
    {
        return (EmpSafeguardListController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public EmpSafeguardListCollection getEmpSafeguardListCollection() throws BOSException
    {
        try {
            return getController().getEmpSafeguardListCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public EmpSafeguardListCollection getEmpSafeguardListCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getEmpSafeguardListCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public EmpSafeguardListCollection getEmpSafeguardListCollection(String oql) throws BOSException
    {
        try {
            return getController().getEmpSafeguardListCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public EmpSafeguardListInfo getEmpSafeguardListInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpSafeguardListInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public EmpSafeguardListInfo getEmpSafeguardListInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpSafeguardListInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public EmpSafeguardListInfo getEmpSafeguardListInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpSafeguardListInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}