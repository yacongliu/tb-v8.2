/**
 * output package name
 */
package com.kingdee.eas.custom.human.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractAssessmentIndexsEditUI extends com.kingdee.eas.framework.client.EditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractAssessmentIndexsEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer kDLabelContainer1;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer kDLabelContainer2;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer kDLabelContainer3;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer kDLabelContainer4;
    protected com.kingdee.bos.ctrl.swing.KDCheckBox chkstatus;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contindexDescribe;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contevaluationCriterion;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcreateName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcreatetm;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contSubordinate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contptype;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizMultiLangBox txtName;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtSimpleName;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizMultiLangBox txtDescription;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtindexDescribe;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtevaluationCriterion;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcreateName;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker pkcreatetm;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtSubordinate;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtptype;
    protected com.kingdee.eas.custom.human.AssessmentIndexsInfo editData = null;
    /**
     * output class constructor
     */
    public AbstractAssessmentIndexsEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractAssessmentIndexsEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        this.kDLabelContainer1 = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDLabelContainer2 = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDLabelContainer3 = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kDLabelContainer4 = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.chkstatus = new com.kingdee.bos.ctrl.swing.KDCheckBox();
        this.contindexDescribe = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contevaluationCriterion = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcreateName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcreatetm = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contSubordinate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contptype = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtName = new com.kingdee.bos.ctrl.extendcontrols.KDBizMultiLangBox();
        this.txtSimpleName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtDescription = new com.kingdee.bos.ctrl.extendcontrols.KDBizMultiLangBox();
        this.txtindexDescribe = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtevaluationCriterion = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtcreateName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.pkcreatetm = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.prmtSubordinate = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtptype = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.kDLabelContainer1.setName("kDLabelContainer1");
        this.kDLabelContainer2.setName("kDLabelContainer2");
        this.kDLabelContainer3.setName("kDLabelContainer3");
        this.kDLabelContainer4.setName("kDLabelContainer4");
        this.chkstatus.setName("chkstatus");
        this.contindexDescribe.setName("contindexDescribe");
        this.contevaluationCriterion.setName("contevaluationCriterion");
        this.contcreateName.setName("contcreateName");
        this.contcreatetm.setName("contcreatetm");
        this.contSubordinate.setName("contSubordinate");
        this.contptype.setName("contptype");
        this.txtNumber.setName("txtNumber");
        this.txtName.setName("txtName");
        this.txtSimpleName.setName("txtSimpleName");
        this.txtDescription.setName("txtDescription");
        this.txtindexDescribe.setName("txtindexDescribe");
        this.txtevaluationCriterion.setName("txtevaluationCriterion");
        this.txtcreateName.setName("txtcreateName");
        this.pkcreatetm.setName("pkcreatetm");
        this.prmtSubordinate.setName("prmtSubordinate");
        this.txtptype.setName("txtptype");
        // CoreUI		
        this.btnPrint.setVisible(false);		
        this.btnPrintPreview.setVisible(false);		
        this.menuItemPrint.setVisible(false);		
        this.menuItemPrintPreview.setVisible(false);
        // kDLabelContainer1		
        this.kDLabelContainer1.setBoundLabelText(resHelper.getString("kDLabelContainer1.boundLabelText"));		
        this.kDLabelContainer1.setBoundLabelLength(100);		
        this.kDLabelContainer1.setBoundLabelUnderline(true);
        // kDLabelContainer2		
        this.kDLabelContainer2.setBoundLabelText(resHelper.getString("kDLabelContainer2.boundLabelText"));		
        this.kDLabelContainer2.setBoundLabelLength(100);		
        this.kDLabelContainer2.setBoundLabelUnderline(true);
        // kDLabelContainer3		
        this.kDLabelContainer3.setBoundLabelText(resHelper.getString("kDLabelContainer3.boundLabelText"));		
        this.kDLabelContainer3.setBoundLabelLength(100);		
        this.kDLabelContainer3.setBoundLabelUnderline(true);
        // kDLabelContainer4		
        this.kDLabelContainer4.setBoundLabelText(resHelper.getString("kDLabelContainer4.boundLabelText"));		
        this.kDLabelContainer4.setBoundLabelLength(100);		
        this.kDLabelContainer4.setBoundLabelUnderline(true);		
        this.kDLabelContainer4.setBoundLabelAlignment(7);		
        this.kDLabelContainer4.setVisible(true);
        // chkstatus		
        this.chkstatus.setText(resHelper.getString("chkstatus.text"));		
        this.chkstatus.setVisible(true);		
        this.chkstatus.setHorizontalAlignment(2);
        // contindexDescribe		
        this.contindexDescribe.setBoundLabelText(resHelper.getString("contindexDescribe.boundLabelText"));		
        this.contindexDescribe.setBoundLabelLength(100);		
        this.contindexDescribe.setBoundLabelUnderline(true);		
        this.contindexDescribe.setVisible(true);
        // contevaluationCriterion		
        this.contevaluationCriterion.setBoundLabelText(resHelper.getString("contevaluationCriterion.boundLabelText"));		
        this.contevaluationCriterion.setBoundLabelLength(100);		
        this.contevaluationCriterion.setBoundLabelUnderline(true);		
        this.contevaluationCriterion.setVisible(true);
        // contcreateName		
        this.contcreateName.setBoundLabelText(resHelper.getString("contcreateName.boundLabelText"));		
        this.contcreateName.setBoundLabelLength(100);		
        this.contcreateName.setBoundLabelUnderline(true);		
        this.contcreateName.setVisible(true);
        // contcreatetm		
        this.contcreatetm.setBoundLabelText(resHelper.getString("contcreatetm.boundLabelText"));		
        this.contcreatetm.setBoundLabelLength(100);		
        this.contcreatetm.setBoundLabelUnderline(true);		
        this.contcreatetm.setVisible(true);
        // contSubordinate		
        this.contSubordinate.setBoundLabelText(resHelper.getString("contSubordinate.boundLabelText"));		
        this.contSubordinate.setBoundLabelLength(100);		
        this.contSubordinate.setBoundLabelUnderline(true);		
        this.contSubordinate.setVisible(true);
        // contptype		
        this.contptype.setBoundLabelText(resHelper.getString("contptype.boundLabelText"));		
        this.contptype.setBoundLabelLength(100);		
        this.contptype.setBoundLabelUnderline(true);		
        this.contptype.setVisible(true);
        // txtNumber		
        this.txtNumber.setMaxLength(80);
        // txtName
        // txtSimpleName		
        this.txtSimpleName.setMaxLength(80);
        // txtDescription
        // txtindexDescribe		
        this.txtindexDescribe.setVisible(true);		
        this.txtindexDescribe.setHorizontalAlignment(2);		
        this.txtindexDescribe.setMaxLength(255);		
        this.txtindexDescribe.setRequired(false);
        // txtevaluationCriterion		
        this.txtevaluationCriterion.setVisible(true);		
        this.txtevaluationCriterion.setHorizontalAlignment(2);		
        this.txtevaluationCriterion.setMaxLength(255);		
        this.txtevaluationCriterion.setRequired(false);
        // txtcreateName		
        this.txtcreateName.setVisible(true);		
        this.txtcreateName.setHorizontalAlignment(2);		
        this.txtcreateName.setMaxLength(100);		
        this.txtcreateName.setRequired(false);
        // pkcreatetm		
        this.pkcreatetm.setVisible(true);		
        this.pkcreatetm.setRequired(false);
        // prmtSubordinate		
        this.prmtSubordinate.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");		
        this.prmtSubordinate.setVisible(true);		
        this.prmtSubordinate.setEditable(true);		
        this.prmtSubordinate.setDisplayFormat("$name$");		
        this.prmtSubordinate.setEditFormat("$number$");		
        this.prmtSubordinate.setCommitFormat("$number$");		
        this.prmtSubordinate.setRequired(false);
        // txtptype		
        this.txtptype.setVisible(true);		
        this.txtptype.setHorizontalAlignment(2);		
        this.txtptype.setMaxLength(100);		
        this.txtptype.setRequired(false);
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {chkstatus,txtindexDescribe,txtevaluationCriterion,txtcreateName,pkcreatetm,prmtSubordinate,txtptype}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 149));
        this.setLayout(null);
        kDLabelContainer1.setBounds(new Rectangle(10, 10, 270, 19));
        this.add(kDLabelContainer1, null);
        kDLabelContainer2.setBounds(new Rectangle(672, 10, 270, 19));
        this.add(kDLabelContainer2, null);
        kDLabelContainer3.setBounds(new Rectangle(10, 34, 270, 19));
        this.add(kDLabelContainer3, null);
        kDLabelContainer4.setBounds(new Rectangle(10, 58, 270, 19));
        this.add(kDLabelContainer4, null);
        chkstatus.setBounds(new Rectangle(341, 58, 270, 19));
        this.add(chkstatus, null);
        contindexDescribe.setBounds(new Rectangle(672, 82, 270, 19));
        this.add(contindexDescribe, null);
        contevaluationCriterion.setBounds(new Rectangle(10, 82, 270, 19));
        this.add(contevaluationCriterion, null);
        contcreateName.setBounds(new Rectangle(10, 106, 270, 19));
        this.add(contcreateName, null);
        contcreatetm.setBounds(new Rectangle(672, 58, 270, 19));
        this.add(contcreatetm, null);
        contSubordinate.setBounds(new Rectangle(341, 10, 270, 19));
        this.add(contSubordinate, null);
        contptype.setBounds(new Rectangle(341, 82, 270, 19));
        this.add(contptype, null);
        //kDLabelContainer1
        kDLabelContainer1.setBoundEditor(txtNumber);
        //kDLabelContainer2
        kDLabelContainer2.setBoundEditor(txtName);
        //kDLabelContainer3
        kDLabelContainer3.setBoundEditor(txtSimpleName);
        //kDLabelContainer4
        kDLabelContainer4.setBoundEditor(txtDescription);
        //contindexDescribe
        contindexDescribe.setBoundEditor(txtindexDescribe);
        //contevaluationCriterion
        contevaluationCriterion.setBoundEditor(txtevaluationCriterion);
        //contcreateName
        contcreateName.setBoundEditor(txtcreateName);
        //contcreatetm
        contcreatetm.setBoundEditor(pkcreatetm);
        //contSubordinate
        contSubordinate.setBoundEditor(prmtSubordinate);
        //contptype
        contptype.setBoundEditor(txtptype);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        //menuTool
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnReset);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnSave);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("status", boolean.class, this.chkstatus, "selected");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("name", String.class, this.txtName, "_multiLangItem");
		dataBinder.registerBinding("simpleName", String.class, this.txtSimpleName, "text");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "_multiLangItem");
		dataBinder.registerBinding("indexDescribe", String.class, this.txtindexDescribe, "text");
		dataBinder.registerBinding("evaluationCriterion", String.class, this.txtevaluationCriterion, "text");
		dataBinder.registerBinding("createName", String.class, this.txtcreateName, "text");
		dataBinder.registerBinding("createtm", java.util.Date.class, this.pkcreatetm, "value");
		dataBinder.registerBinding("Subordinate", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtSubordinate, "data");
		dataBinder.registerBinding("ptype", String.class, this.txtptype, "text");		
	}
	//Regiester UI State
	private void registerUIState(){
	        getActionManager().registerUIState(STATUS_ADDNEW, this.txtName, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_ADDNEW, this.txtDescription, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_ADDNEW, this.txtNumber, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_ADDNEW, this.txtSimpleName, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_EDIT, this.txtName, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_EDIT, this.txtDescription, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_EDIT, this.txtNumber, ActionStateConst.ENABLED);
	        getActionManager().registerUIState(STATUS_EDIT, this.txtSimpleName, ActionStateConst.ENABLED);					 	        		
	        getActionManager().registerUIState(STATUS_VIEW, this.txtName, ActionStateConst.DISABLED);					 	        		
	        getActionManager().registerUIState(STATUS_VIEW, this.txtDescription, ActionStateConst.DISABLED);					 	        		
	        getActionManager().registerUIState(STATUS_VIEW, this.txtNumber, ActionStateConst.DISABLED);					 	        		
	        getActionManager().registerUIState(STATUS_VIEW, this.txtSimpleName, ActionStateConst.DISABLED);		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.human.app.AssessmentIndexsEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.chkstatus.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.human.AssessmentIndexsInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"NONE",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("NONE");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("status", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("simpleName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("indexDescribe", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("evaluationCriterion", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createtm", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("Subordinate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("ptype", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
		            this.txtName.setEnabled(true);
		            this.txtDescription.setEnabled(true);
		            this.txtNumber.setEnabled(true);
		            this.txtSimpleName.setEnabled(true);
        } else if (STATUS_EDIT.equals(this.oprtState)) {
		            this.txtName.setEnabled(true);
		            this.txtDescription.setEnabled(true);
		            this.txtNumber.setEnabled(true);
		            this.txtSimpleName.setEnabled(true);
        } else if (STATUS_VIEW.equals(this.oprtState)) {
		            this.txtName.setEnabled(false);
		            this.txtDescription.setEnabled(false);
		            this.txtNumber.setEnabled(false);
		            this.txtSimpleName.setEnabled(false);
        }
    }

    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
        sic.add(new SelectorItemInfo("status"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("name"));
        sic.add(new SelectorItemInfo("simpleName"));
        sic.add(new SelectorItemInfo("description"));
        sic.add(new SelectorItemInfo("indexDescribe"));
        sic.add(new SelectorItemInfo("evaluationCriterion"));
        sic.add(new SelectorItemInfo("createName"));
        sic.add(new SelectorItemInfo("createtm"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("Subordinate.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("Subordinate.id"));
        	sic.add(new SelectorItemInfo("Subordinate.number"));
        	sic.add(new SelectorItemInfo("Subordinate.name"));
		}
        sic.add(new SelectorItemInfo("ptype"));
        return sic;
    }        

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.human.client", "AssessmentIndexsEditUI");
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.human.client.AssessmentIndexsEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.human.AssessmentIndexsFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.human.AssessmentIndexsInfo objectValue = new com.kingdee.eas.custom.human.AssessmentIndexsInfo();
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }



    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {        
        return null;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}