package com.kingdee.eas.custom.human;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AssessmentIndexsFactory
{
    private AssessmentIndexsFactory()
    {
    }
    public static com.kingdee.eas.custom.human.IAssessmentIndexs getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.human.IAssessmentIndexs)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C5803BA9") ,com.kingdee.eas.custom.human.IAssessmentIndexs.class);
    }
    
    public static com.kingdee.eas.custom.human.IAssessmentIndexs getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.human.IAssessmentIndexs)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C5803BA9") ,com.kingdee.eas.custom.human.IAssessmentIndexs.class, objectCtx);
    }
    public static com.kingdee.eas.custom.human.IAssessmentIndexs getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.human.IAssessmentIndexs)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C5803BA9"));
    }
    public static com.kingdee.eas.custom.human.IAssessmentIndexs getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.human.IAssessmentIndexs)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C5803BA9"));
    }
}