package com.kingdee.eas.custom.human;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAssessmentIndexsInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractAssessmentIndexsInfo()
    {
        this("id");
    }
    protected AbstractAssessmentIndexsInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:����ָ���'s ״̬property 
     */
    public boolean isStatus()
    {
        return getBoolean("status");
    }
    public void setStatus(boolean item)
    {
        setBoolean("status", item);
    }
    /**
     * Object:����ָ���'s ָ������property 
     */
    public String getIndexDescribe()
    {
        return getString("indexDescribe");
    }
    public void setIndexDescribe(String item)
    {
        setString("indexDescribe", item);
    }
    /**
     * Object:����ָ���'s ���۱�׼property 
     */
    public String getEvaluationCriterion()
    {
        return getString("evaluationCriterion");
    }
    public void setEvaluationCriterion(String item)
    {
        setString("evaluationCriterion", item);
    }
    /**
     * Object:����ָ���'s ������property 
     */
    public String getCreateName()
    {
        return getString("createName");
    }
    public void setCreateName(String item)
    {
        setString("createName", item);
    }
    /**
     * Object:����ָ���'s ��������property 
     */
    public java.util.Date getCreatetm()
    {
        return getDate("createtm");
    }
    public void setCreatetm(java.util.Date item)
    {
        setDate("createtm", item);
    }
    /**
     * Object: ����ָ��� 's �������� property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getSubordinate()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("Subordinate");
    }
    public void setSubordinate(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("Subordinate", item);
    }
    /**
     * Object:����ָ���'s ����/����property 
     */
    public String getPtype()
    {
        return getString("ptype");
    }
    public void setPtype(String item)
    {
        setString("ptype", item);
    }
    /**
     * Object: ����ָ��� 's ����ָ�� property 
     */
    public com.kingdee.eas.hr.perf.PerformTargetTypeInfo getTargetType()
    {
        return (com.kingdee.eas.hr.perf.PerformTargetTypeInfo)get("TargetType");
    }
    public void setTargetType(com.kingdee.eas.hr.perf.PerformTargetTypeInfo item)
    {
        put("TargetType", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("C5803BA9");
    }
}