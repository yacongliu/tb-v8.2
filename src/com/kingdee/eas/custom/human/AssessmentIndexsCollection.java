package com.kingdee.eas.custom.human;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AssessmentIndexsCollection extends AbstractObjectCollection 
{
    public AssessmentIndexsCollection()
    {
        super(AssessmentIndexsInfo.class);
    }
    public boolean add(AssessmentIndexsInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AssessmentIndexsCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AssessmentIndexsInfo item)
    {
        return removeObject(item);
    }
    public AssessmentIndexsInfo get(int index)
    {
        return(AssessmentIndexsInfo)getObject(index);
    }
    public AssessmentIndexsInfo get(Object key)
    {
        return(AssessmentIndexsInfo)getObject(key);
    }
    public void set(int index, AssessmentIndexsInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AssessmentIndexsInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AssessmentIndexsInfo item)
    {
        return super.indexOf(item);
    }
}