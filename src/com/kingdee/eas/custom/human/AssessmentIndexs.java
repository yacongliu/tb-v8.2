package com.kingdee.eas.custom.human;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.human.app.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class AssessmentIndexs extends DataBase implements IAssessmentIndexs
{
    public AssessmentIndexs()
    {
        super();
        registerInterface(IAssessmentIndexs.class, this);
    }
    public AssessmentIndexs(Context ctx)
    {
        super(ctx);
        registerInterface(IAssessmentIndexs.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C5803BA9");
    }
    private AssessmentIndexsController getController() throws BOSException
    {
        return (AssessmentIndexsController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public AssessmentIndexsInfo getAssessmentIndexsInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAssessmentIndexsInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public AssessmentIndexsInfo getAssessmentIndexsInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAssessmentIndexsInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public AssessmentIndexsInfo getAssessmentIndexsInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAssessmentIndexsInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public AssessmentIndexsCollection getAssessmentIndexsCollection() throws BOSException
    {
        try {
            return getController().getAssessmentIndexsCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public AssessmentIndexsCollection getAssessmentIndexsCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAssessmentIndexsCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public AssessmentIndexsCollection getAssessmentIndexsCollection(String oql) throws BOSException
    {
        try {
            return getController().getAssessmentIndexsCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}