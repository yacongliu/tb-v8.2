package com.kingdee.eas.custom.human;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IAssessmentIndexs extends IDataBase
{
    public AssessmentIndexsInfo getAssessmentIndexsInfo(IObjectPK pk) throws BOSException, EASBizException;
    public AssessmentIndexsInfo getAssessmentIndexsInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public AssessmentIndexsInfo getAssessmentIndexsInfo(String oql) throws BOSException, EASBizException;
    public AssessmentIndexsCollection getAssessmentIndexsCollection() throws BOSException;
    public AssessmentIndexsCollection getAssessmentIndexsCollection(EntityViewInfo view) throws BOSException;
    public AssessmentIndexsCollection getAssessmentIndexsCollection(String oql) throws BOSException;
}