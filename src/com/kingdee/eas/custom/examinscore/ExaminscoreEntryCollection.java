package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ExaminscoreEntryCollection extends AbstractObjectCollection 
{
    public ExaminscoreEntryCollection()
    {
        super(ExaminscoreEntryInfo.class);
    }
    public boolean add(ExaminscoreEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ExaminscoreEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ExaminscoreEntryInfo item)
    {
        return removeObject(item);
    }
    public ExaminscoreEntryInfo get(int index)
    {
        return(ExaminscoreEntryInfo)getObject(index);
    }
    public ExaminscoreEntryInfo get(Object key)
    {
        return(ExaminscoreEntryInfo)getObject(key);
    }
    public void set(int index, ExaminscoreEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ExaminscoreEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ExaminscoreEntryInfo item)
    {
        return super.indexOf(item);
    }
}