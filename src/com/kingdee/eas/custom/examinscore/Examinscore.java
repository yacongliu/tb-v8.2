package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.custom.examinscore.app.*;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class Examinscore extends HRBillBase implements IExaminscore
{
    public Examinscore()
    {
        super();
        registerInterface(IExaminscore.class, this);
    }
    public Examinscore(Context ctx)
    {
        super(ctx);
        registerInterface(IExaminscore.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C5F61DC9");
    }
    private ExaminscoreController getController() throws BOSException
    {
        return (ExaminscoreController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ExaminscoreCollection getExaminscoreCollection() throws BOSException
    {
        try {
            return getController().getExaminscoreCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ExaminscoreCollection getExaminscoreCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getExaminscoreCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ExaminscoreCollection getExaminscoreCollection(String oql) throws BOSException
    {
        try {
            return getController().getExaminscoreCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ExaminscoreInfo getExaminscoreInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getExaminscoreInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ExaminscoreInfo getExaminscoreInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getExaminscoreInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ExaminscoreInfo getExaminscoreInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getExaminscoreInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����״̬-User defined method
     */
    public void setState() throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��-User defined method
     */
    public void setPassState() throws BOSException, EASBizException
    {
        try {
            getController().setPassState(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������ͨ��-User defined method
     */
    public void setNoPassState() throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������״̬-User defined method
     */
    public void setApproveState() throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����δ����״̬-User defined method
     */
    public void setEditState() throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}