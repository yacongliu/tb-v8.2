package com.kingdee.eas.custom.examinscore.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.custom.examinscore.ExaminscoreEntryCollection;
import com.kingdee.eas.custom.examinscore.ExaminscoreEntryInfo;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface ExaminscoreEntryController extends HRBillBaseEntryController
{
    public ExaminscoreEntryInfo getExaminscoreEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public ExaminscoreEntryInfo getExaminscoreEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public ExaminscoreEntryInfo getExaminscoreEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public ExaminscoreEntryCollection getExaminscoreEntryCollection(Context ctx) throws BOSException, RemoteException;
    public ExaminscoreEntryCollection getExaminscoreEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public ExaminscoreEntryCollection getExaminscoreEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}