package com.kingdee.eas.custom.examinscore.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.examinscore.ExaminscoreCollection;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.app.HRBillBaseController;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.custom.examinscore.ExaminscoreInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface ExaminscoreController extends HRBillBaseController
{
    public ExaminscoreCollection getExaminscoreCollection(Context ctx) throws BOSException, RemoteException;
    public ExaminscoreCollection getExaminscoreCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public ExaminscoreCollection getExaminscoreCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public ExaminscoreInfo getExaminscoreInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public ExaminscoreInfo getExaminscoreInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public ExaminscoreInfo getExaminscoreInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public void setState(Context ctx) throws BOSException, EASBizException, RemoteException;
    public void setPassState(Context ctx) throws BOSException, EASBizException, RemoteException;
    public void setNoPassState(Context ctx) throws BOSException, EASBizException, RemoteException;
    public void setApproveState(Context ctx) throws BOSException, EASBizException, RemoteException;
    public void setEditState(Context ctx) throws BOSException, EASBizException, RemoteException;
}