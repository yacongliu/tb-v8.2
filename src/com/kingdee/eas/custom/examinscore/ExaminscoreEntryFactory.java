package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ExaminscoreEntryFactory
{
    private ExaminscoreEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.examinscore.IExaminscoreEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscoreEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("5B523029") ,com.kingdee.eas.custom.examinscore.IExaminscoreEntry.class);
    }
    
    public static com.kingdee.eas.custom.examinscore.IExaminscoreEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscoreEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("5B523029") ,com.kingdee.eas.custom.examinscore.IExaminscoreEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.examinscore.IExaminscoreEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscoreEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("5B523029"));
    }
    public static com.kingdee.eas.custom.examinscore.IExaminscoreEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscoreEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("5B523029"));
    }
}