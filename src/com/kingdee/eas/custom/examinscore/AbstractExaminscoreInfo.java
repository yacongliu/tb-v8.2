package com.kingdee.eas.custom.examinscore;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractExaminscoreInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractExaminscoreInfo()
    {
        this("id");
    }
    protected AbstractExaminscoreInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.examinscore.ExaminscoreEntryCollection());
    }
    /**
     * Object: �������� 's ��¼ property 
     */
    public com.kingdee.eas.custom.examinscore.ExaminscoreEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.examinscore.ExaminscoreEntryCollection)get("entrys");
    }
    /**
     * Object: �������� 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:��������'s ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:��������'s ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    /**
     * Object: �������� 's �������� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object: �������� 's ��λ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��������'s ���˳ɼ�property 
     */
    public int getScore()
    {
        return getInt("score");
    }
    public void setScore(int item)
    {
        setInt("score", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("C5F61DC9");
    }
}