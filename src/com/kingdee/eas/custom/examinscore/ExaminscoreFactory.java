package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ExaminscoreFactory
{
    private ExaminscoreFactory()
    {
    }
    public static com.kingdee.eas.custom.examinscore.IExaminscore getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscore)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C5F61DC9") ,com.kingdee.eas.custom.examinscore.IExaminscore.class);
    }
    
    public static com.kingdee.eas.custom.examinscore.IExaminscore getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscore)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C5F61DC9") ,com.kingdee.eas.custom.examinscore.IExaminscore.class, objectCtx);
    }
    public static com.kingdee.eas.custom.examinscore.IExaminscore getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscore)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C5F61DC9"));
    }
    public static com.kingdee.eas.custom.examinscore.IExaminscore getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.examinscore.IExaminscore)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C5F61DC9"));
    }
}