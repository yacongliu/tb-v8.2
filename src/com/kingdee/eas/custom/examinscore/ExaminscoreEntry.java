package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import com.kingdee.eas.custom.examinscore.app.*;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class ExaminscoreEntry extends HRBillBaseEntry implements IExaminscoreEntry
{
    public ExaminscoreEntry()
    {
        super();
        registerInterface(IExaminscoreEntry.class, this);
    }
    public ExaminscoreEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IExaminscoreEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("5B523029");
    }
    private ExaminscoreEntryController getController() throws BOSException
    {
        return (ExaminscoreEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ExaminscoreEntryInfo getExaminscoreEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getExaminscoreEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ExaminscoreEntryInfo getExaminscoreEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getExaminscoreEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ExaminscoreEntryInfo getExaminscoreEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getExaminscoreEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ExaminscoreEntryCollection getExaminscoreEntryCollection() throws BOSException
    {
        try {
            return getController().getExaminscoreEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ExaminscoreEntryCollection getExaminscoreEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getExaminscoreEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ExaminscoreEntryCollection getExaminscoreEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getExaminscoreEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}