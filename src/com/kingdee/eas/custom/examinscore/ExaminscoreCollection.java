package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ExaminscoreCollection extends AbstractObjectCollection 
{
    public ExaminscoreCollection()
    {
        super(ExaminscoreInfo.class);
    }
    public boolean add(ExaminscoreInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ExaminscoreCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ExaminscoreInfo item)
    {
        return removeObject(item);
    }
    public ExaminscoreInfo get(int index)
    {
        return(ExaminscoreInfo)getObject(index);
    }
    public ExaminscoreInfo get(Object key)
    {
        return(ExaminscoreInfo)getObject(key);
    }
    public void set(int index, ExaminscoreInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ExaminscoreInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ExaminscoreInfo item)
    {
        return super.indexOf(item);
    }
}