package com.kingdee.eas.custom.examinscore;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IExaminscore extends IHRBillBase
{
    public ExaminscoreCollection getExaminscoreCollection() throws BOSException;
    public ExaminscoreCollection getExaminscoreCollection(EntityViewInfo view) throws BOSException;
    public ExaminscoreCollection getExaminscoreCollection(String oql) throws BOSException;
    public ExaminscoreInfo getExaminscoreInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ExaminscoreInfo getExaminscoreInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ExaminscoreInfo getExaminscoreInfo(String oql) throws BOSException, EASBizException;
    public void setState() throws BOSException, EASBizException;
    public void setPassState() throws BOSException, EASBizException;
    public void setNoPassState() throws BOSException, EASBizException;
    public void setApproveState() throws BOSException, EASBizException;
    public void setEditState() throws BOSException, EASBizException;
}