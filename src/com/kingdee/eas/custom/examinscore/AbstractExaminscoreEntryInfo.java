package com.kingdee.eas.custom.examinscore;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractExaminscoreEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractExaminscoreEntryInfo()
    {
        this("id");
    }
    protected AbstractExaminscoreEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.examinscore.ExaminscoreInfo getBill()
    {
        return (com.kingdee.eas.custom.examinscore.ExaminscoreInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.examinscore.ExaminscoreInfo item)
    {
        put("bill", item);
    }
    /**
     * Object:��¼'s �������˵��property 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object:��¼'s ������Ŀproperty 
     */
    public String getProject()
    {
        return getString("project");
    }
    public void setProject(String item)
    {
        setString("project", item);
    }
    /**
     * Object:��¼'s ���۱�׼property 
     */
    public String getStandard()
    {
        return getString("standard");
    }
    public void setStandard(String item)
    {
        setString("standard", item);
    }
    /**
     * Object:��¼'s �۷�property 
     */
    public int getDeduction()
    {
        return getInt("deduction");
    }
    public void setDeduction(int item)
    {
        setInt("deduction", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("5B523029");
    }
}