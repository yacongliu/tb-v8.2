package com.kingdee.eas.custom.atsexc;

import java.io.Serializable;

public class AtsExceptionBillEntryInfo extends AbstractAtsExceptionBillEntryInfo implements Serializable 
{
    public AtsExceptionBillEntryInfo()
    {
        super();
    }
    protected AtsExceptionBillEntryInfo(String pkField)
    {
        super(pkField);
    }
}