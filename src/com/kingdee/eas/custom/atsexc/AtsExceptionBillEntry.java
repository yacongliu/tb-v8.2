package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.custom.atsexc.app.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class AtsExceptionBillEntry extends HRBillBaseEntry implements IAtsExceptionBillEntry
{
    public AtsExceptionBillEntry()
    {
        super();
        registerInterface(IAtsExceptionBillEntry.class, this);
    }
    public AtsExceptionBillEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IAtsExceptionBillEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("B48732A9");
    }
    private AtsExceptionBillEntryController getController() throws BOSException
    {
        return (AtsExceptionBillEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public AtsExceptionBillEntryInfo getAtsExceptionBillEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAtsExceptionBillEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public AtsExceptionBillEntryInfo getAtsExceptionBillEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAtsExceptionBillEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public AtsExceptionBillEntryInfo getAtsExceptionBillEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAtsExceptionBillEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public AtsExceptionBillEntryCollection getAtsExceptionBillEntryCollection() throws BOSException
    {
        try {
            return getController().getAtsExceptionBillEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public AtsExceptionBillEntryCollection getAtsExceptionBillEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAtsExceptionBillEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public AtsExceptionBillEntryCollection getAtsExceptionBillEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getAtsExceptionBillEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}