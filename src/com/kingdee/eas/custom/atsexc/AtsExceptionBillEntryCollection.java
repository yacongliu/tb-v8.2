package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AtsExceptionBillEntryCollection extends AbstractObjectCollection 
{
    public AtsExceptionBillEntryCollection()
    {
        super(AtsExceptionBillEntryInfo.class);
    }
    public boolean add(AtsExceptionBillEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AtsExceptionBillEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AtsExceptionBillEntryInfo item)
    {
        return removeObject(item);
    }
    public AtsExceptionBillEntryInfo get(int index)
    {
        return(AtsExceptionBillEntryInfo)getObject(index);
    }
    public AtsExceptionBillEntryInfo get(Object key)
    {
        return(AtsExceptionBillEntryInfo)getObject(key);
    }
    public void set(int index, AtsExceptionBillEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AtsExceptionBillEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AtsExceptionBillEntryInfo item)
    {
        return super.indexOf(item);
    }
}