package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AtsExceptionBillFactory
{
    private AtsExceptionBillFactory()
    {
    }
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBill getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBill)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("A3498B49") ,com.kingdee.eas.custom.atsexc.IAtsExceptionBill.class);
    }
    
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBill getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBill)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("A3498B49") ,com.kingdee.eas.custom.atsexc.IAtsExceptionBill.class, objectCtx);
    }
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBill getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBill)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("A3498B49"));
    }
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBill getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBill)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("A3498B49"));
    }
}