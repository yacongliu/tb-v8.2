/**
 * output package name
 */
package com.kingdee.eas.custom.atsexc.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractAtsExceptionBillEditUI extends com.kingdee.eas.hr.base.client.HRBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractAtsExceptionBillEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplier;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAdminOrg;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplyDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBillState;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtApplier;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAdminOrg;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker dpApplyDate;
    protected com.kingdee.bos.ctrl.swing.KDComboBox cbBillState;
    protected com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo editData = null;
    /**
     * output class constructor
     */
    public AbstractAtsExceptionBillEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractAtsExceptionBillEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setBindWorkFlow(true);
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionAudit
        actionAudit.setEnabled(true);
        actionAudit.setDaemonRun(false);

        actionAudit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl A"));
        _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
        actionAudit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
        actionAudit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionAudit.NAME");
        actionAudit.putValue(ItemAction.NAME, _tempStr);
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionUnaudit
        actionUnaudit.setEnabled(true);
        actionUnaudit.setDaemonRun(false);

        actionUnaudit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl U"));
        _tempStr = resHelper.getString("ActionUnaudit.SHORT_DESCRIPTION");
        actionUnaudit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionUnaudit.LONG_DESCRIPTION");
        actionUnaudit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionUnaudit.NAME");
        actionUnaudit.putValue(ItemAction.NAME, _tempStr);
         this.actionUnaudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kdtEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.contApplier = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAdminOrg = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contApplyDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBillState = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtApplier = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAdminOrg = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.dpApplyDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.cbBillState = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.contNumber.setName("contNumber");
        this.contDescription.setName("contDescription");
        this.kdtEntrys.setName("kdtEntrys");
        this.contApplier.setName("contApplier");
        this.contAdminOrg.setName("contAdminOrg");
        this.contApplyDate.setName("contApplyDate");
        this.contBillState.setName("contBillState");
        this.txtNumber.setName("txtNumber");
        this.txtDescription.setName("txtDescription");
        this.txtApplier.setName("txtApplier");
        this.prmtAdminOrg.setName("prmtAdminOrg");
        this.dpApplyDate.setName("dpApplyDate");
        this.cbBillState.setName("cbBillState");
        // CoreUI		
        this.btnPrint.setVisible(false);		
        this.btnPrintPreview.setVisible(false);		
        this.menuItemPrint.setVisible(false);		
        this.menuItemPrintPreview.setVisible(false);		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateFrom.setVisible(false);		
        this.btnAddLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateFrom.setVisible(false);		
        this.menuItemCopyFrom.setVisible(false);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);		
        this.contHROrg.setBoundLabelText(resHelper.getString("contHROrg.boundLabelText"));		
        this.contHROrg.setBoundLabelLength(90);		
        this.contHROrg.setBoundLabelUnderline(true);		
        this.contHROrg.setBoundLabelAlignment(7);		
        this.contHROrg.setVisible(true);		
        this.contApproveType.setBoundLabelText(resHelper.getString("contApproveType.boundLabelText"));		
        this.contApproveType.setBoundLabelLength(90);		
        this.contApproveType.setBoundLabelUnderline(true);		
        this.contApproveType.setBoundLabelAlignment(7);		
        this.contApproveType.setVisible(true);		
        this.prmtHROrg.setRequired(true);		
        this.prmtHROrg.setEnabled(true);		
        this.cbApproveType.setRequired(true);		
        this.cbApproveType.setEnabled(true);
        this.btnAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.btnAudit.setText(resHelper.getString("btnAudit.text"));		
        this.btnAudit.setToolTipText(resHelper.getString("btnAudit.toolTipText"));		
        this.btnAudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_audit"));
        this.btnUnaudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnaudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.btnUnaudit.setText(resHelper.getString("btnUnaudit.text"));		
        this.btnUnaudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_unaudit"));		
        this.btnUnaudit.setToolTipText(resHelper.getString("btnUnaudit.toolTipText"));		
        this.btnUnaudit.setVisible(false);
        this.menuItemAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.menuItemAudit.setText(resHelper.getString("menuItemAudit.text"));		
        this.menuItemAudit.setToolTipText(resHelper.getString("menuItemAudit.toolTipText"));		
        this.menuItemAudit.setMnemonic(65);		
        this.menuItemAudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_audit"));
        this.menuItemUnaudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnaudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.menuItemUnaudit.setText(resHelper.getString("menuItemUnaudit.text"));		
        this.menuItemUnaudit.setToolTipText(resHelper.getString("menuItemUnaudit.toolTipText"));		
        this.menuItemUnaudit.setMnemonic(85);		
        this.menuItemUnaudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_unaudit"));		
        this.menuItemUnaudit.setVisible(false);
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(90);		
        this.contNumber.setBoundLabelUnderline(true);		
        this.contNumber.setBoundLabelAlignment(7);		
        this.contNumber.setVisible(true);
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(90);		
        this.contDescription.setBoundLabelUnderline(true);		
        this.contDescription.setBoundLabelAlignment(7);		
        this.contDescription.setVisible(true);
        // kdtEntrys
		String kdtEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol1\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol2\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol6\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol7\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol8\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol9\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol10\"><c:Protection locked=\"true\" hidden=\"true\" /></c:Style><c:Style id=\"sCol11\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol13\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol14\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol15\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol17\"><c:NumberFormat>yyyy-mm-dd</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"person\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"1\" t:styleID=\"sCol1\" /><t:Column t:key=\"oldAdminOrg\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" t:styleID=\"sCol2\" /><t:Column t:key=\"time\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" /><t:Column t:key=\"description\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" /><t:Column t:key=\"type\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"5\" /><t:Column t:key=\"empNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"6\" t:styleID=\"sCol6\" /><t:Column t:key=\"empName\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"7\" t:styleID=\"sCol7\" /><t:Column t:key=\"oldPosition\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"8\" t:styleID=\"sCol8\" /><t:Column t:key=\"oldJob\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"9\" t:styleID=\"sCol9\" /><t:Column t:key=\"oldJobLevel\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"10\" t:styleID=\"sCol10\" /><t:Column t:key=\"oldJobGrade\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"11\" t:styleID=\"sCol11\" /><t:Column t:key=\"position\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"true\" t:index=\"12\" /><t:Column t:key=\"adminOrg\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"13\" t:styleID=\"sCol13\" /><t:Column t:key=\"job\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"14\" t:styleID=\"sCol14\" /><t:Column t:key=\"jobLevel\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"15\" t:styleID=\"sCol15\" /><t:Column t:key=\"jobGrade\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"bizDate\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"true\" t:index=\"-1\" t:styleID=\"sCol17\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{person}</t:Cell><t:Cell>$Resource{oldAdminOrg}</t:Cell><t:Cell>$Resource{time}</t:Cell><t:Cell>$Resource{description}</t:Cell><t:Cell>$Resource{type}</t:Cell><t:Cell>$Resource{empNumber}</t:Cell><t:Cell>$Resource{empName}</t:Cell><t:Cell>$Resource{oldPosition}</t:Cell><t:Cell>$Resource{oldJob}</t:Cell><t:Cell>$Resource{oldJobLevel}</t:Cell><t:Cell>$Resource{oldJobGrade}</t:Cell><t:Cell>$Resource{position}</t:Cell><t:Cell>$Resource{adminOrg}</t:Cell><t:Cell>$Resource{job}</t:Cell><t:Cell>$Resource{jobLevel}</t:Cell><t:Cell>$Resource{jobGrade}</t:Cell><t:Cell>$Resource{bizDate}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtEntrys.setFormatXml(resHelper.translateString("kdtEntrys",kdtEntrysStrXML));
        this.kdtEntrys.addKDTEditListener(new com.kingdee.bos.ctrl.kdf.table.event.KDTEditAdapter() {
            public void editStopping(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtEntrys_editStopping(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
            public void editStopped(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtEntrys_editStopped(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
        });

                this.kdtEntrys.putBindContents("editData",new String[] {"id","person","oldAdminOrg","time","description","type","person.number","person.name","oldPosition","oldJob","oldJobLevel","oldJobGrade","position","adminOrg","job","jobLevel","jobGrade","bizDate"});


        this.kdtEntrys.checkParsed();
        final KDBizPromptBox kdtEntrys_person_PromptBox = new KDBizPromptBox();
        kdtEntrys_person_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtEntrys_person_PromptBox.setVisible(true);
        kdtEntrys_person_PromptBox.setEditable(true);
        kdtEntrys_person_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_person_PromptBox.setEditFormat("$number$");
        kdtEntrys_person_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_person_CellEditor = new KDTDefaultCellEditor(kdtEntrys_person_PromptBox);
        this.kdtEntrys.getColumn("person").setEditor(kdtEntrys_person_CellEditor);
        ObjectValueRender kdtEntrys_person_OVR = new ObjectValueRender();
        kdtEntrys_person_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("person").setRenderer(kdtEntrys_person_OVR);
        final KDBizPromptBox kdtEntrys_oldAdminOrg_PromptBox = new KDBizPromptBox();
        kdtEntrys_oldAdminOrg_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");
        kdtEntrys_oldAdminOrg_PromptBox.setVisible(true);
        kdtEntrys_oldAdminOrg_PromptBox.setEditable(true);
        kdtEntrys_oldAdminOrg_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_oldAdminOrg_PromptBox.setEditFormat("$number$");
        kdtEntrys_oldAdminOrg_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_oldAdminOrg_CellEditor = new KDTDefaultCellEditor(kdtEntrys_oldAdminOrg_PromptBox);
        this.kdtEntrys.getColumn("oldAdminOrg").setEditor(kdtEntrys_oldAdminOrg_CellEditor);
        ObjectValueRender kdtEntrys_oldAdminOrg_OVR = new ObjectValueRender();
        kdtEntrys_oldAdminOrg_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("oldAdminOrg").setRenderer(kdtEntrys_oldAdminOrg_OVR);
        KDTextField kdtEntrys_time_TextField = new KDTextField();
        kdtEntrys_time_TextField.setName("kdtEntrys_time_TextField");
        kdtEntrys_time_TextField.setMaxLength(100);
        KDTDefaultCellEditor kdtEntrys_time_CellEditor = new KDTDefaultCellEditor(kdtEntrys_time_TextField);
        this.kdtEntrys.getColumn("time").setEditor(kdtEntrys_time_CellEditor);
        KDTextField kdtEntrys_description_TextField = new KDTextField();
        kdtEntrys_description_TextField.setName("kdtEntrys_description_TextField");
        kdtEntrys_description_TextField.setMaxLength(200);
        KDTDefaultCellEditor kdtEntrys_description_CellEditor = new KDTDefaultCellEditor(kdtEntrys_description_TextField);
        this.kdtEntrys.getColumn("description").setEditor(kdtEntrys_description_CellEditor);
        final KDBizPromptBox kdtEntrys_type_PromptBox = new KDBizPromptBox();
        kdtEntrys_type_PromptBox.setQueryInfo("com.kingdee.eas.custom.atsexc.bd.app.TypeQuery");
        kdtEntrys_type_PromptBox.setVisible(true);
        kdtEntrys_type_PromptBox.setEditable(true);
        kdtEntrys_type_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_type_PromptBox.setEditFormat("$number$");
        kdtEntrys_type_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_type_CellEditor = new KDTDefaultCellEditor(kdtEntrys_type_PromptBox);
        this.kdtEntrys.getColumn("type").setEditor(kdtEntrys_type_CellEditor);
        ObjectValueRender kdtEntrys_type_OVR = new ObjectValueRender();
        kdtEntrys_type_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("type").setRenderer(kdtEntrys_type_OVR);
        KDTextField kdtEntrys_empName_TextField = new KDTextField();
        kdtEntrys_empName_TextField.setName("kdtEntrys_empName_TextField");
        kdtEntrys_empName_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_empName_CellEditor = new KDTDefaultCellEditor(kdtEntrys_empName_TextField);
        this.kdtEntrys.getColumn("empName").setEditor(kdtEntrys_empName_CellEditor);
        final KDBizPromptBox kdtEntrys_oldPosition_PromptBox = new KDBizPromptBox();
        kdtEntrys_oldPosition_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.PositionQuery");
        kdtEntrys_oldPosition_PromptBox.setVisible(true);
        kdtEntrys_oldPosition_PromptBox.setEditable(true);
        kdtEntrys_oldPosition_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_oldPosition_PromptBox.setEditFormat("$number$");
        kdtEntrys_oldPosition_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_oldPosition_CellEditor = new KDTDefaultCellEditor(kdtEntrys_oldPosition_PromptBox);
        this.kdtEntrys.getColumn("oldPosition").setEditor(kdtEntrys_oldPosition_CellEditor);
        ObjectValueRender kdtEntrys_oldPosition_OVR = new ObjectValueRender();
        kdtEntrys_oldPosition_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("oldPosition").setRenderer(kdtEntrys_oldPosition_OVR);
        final KDBizPromptBox kdtEntrys_oldJob_PromptBox = new KDBizPromptBox();
        kdtEntrys_oldJob_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.AssignJobF7Query");
        kdtEntrys_oldJob_PromptBox.setVisible(true);
        kdtEntrys_oldJob_PromptBox.setEditable(true);
        kdtEntrys_oldJob_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_oldJob_PromptBox.setEditFormat("$number$");
        kdtEntrys_oldJob_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_oldJob_CellEditor = new KDTDefaultCellEditor(kdtEntrys_oldJob_PromptBox);
        this.kdtEntrys.getColumn("oldJob").setEditor(kdtEntrys_oldJob_CellEditor);
        ObjectValueRender kdtEntrys_oldJob_OVR = new ObjectValueRender();
        kdtEntrys_oldJob_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("oldJob").setRenderer(kdtEntrys_oldJob_OVR);
        final KDBizPromptBox kdtEntrys_oldJobLevel_PromptBox = new KDBizPromptBox();
        kdtEntrys_oldJobLevel_PromptBox.setQueryInfo("com.kingdee.eas.hr.org.app.JobLevelListQuery");
        kdtEntrys_oldJobLevel_PromptBox.setVisible(true);
        kdtEntrys_oldJobLevel_PromptBox.setEditable(true);
        kdtEntrys_oldJobLevel_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_oldJobLevel_PromptBox.setEditFormat("$number$");
        kdtEntrys_oldJobLevel_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_oldJobLevel_CellEditor = new KDTDefaultCellEditor(kdtEntrys_oldJobLevel_PromptBox);
        this.kdtEntrys.getColumn("oldJobLevel").setEditor(kdtEntrys_oldJobLevel_CellEditor);
        ObjectValueRender kdtEntrys_oldJobLevel_OVR = new ObjectValueRender();
        kdtEntrys_oldJobLevel_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("oldJobLevel").setRenderer(kdtEntrys_oldJobLevel_OVR);
        final KDBizPromptBox kdtEntrys_oldJobGrade_PromptBox = new KDBizPromptBox();
        kdtEntrys_oldJobGrade_PromptBox.setQueryInfo("com.kingdee.eas.hr.org.app.JobGradeListQuery");
        kdtEntrys_oldJobGrade_PromptBox.setVisible(true);
        kdtEntrys_oldJobGrade_PromptBox.setEditable(true);
        kdtEntrys_oldJobGrade_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_oldJobGrade_PromptBox.setEditFormat("$number$");
        kdtEntrys_oldJobGrade_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_oldJobGrade_CellEditor = new KDTDefaultCellEditor(kdtEntrys_oldJobGrade_PromptBox);
        this.kdtEntrys.getColumn("oldJobGrade").setEditor(kdtEntrys_oldJobGrade_CellEditor);
        ObjectValueRender kdtEntrys_oldJobGrade_OVR = new ObjectValueRender();
        kdtEntrys_oldJobGrade_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("oldJobGrade").setRenderer(kdtEntrys_oldJobGrade_OVR);
        final KDBizPromptBox kdtEntrys_position_PromptBox = new KDBizPromptBox();
        kdtEntrys_position_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.PositionQuery");
        kdtEntrys_position_PromptBox.setVisible(true);
        kdtEntrys_position_PromptBox.setEditable(true);
        kdtEntrys_position_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_position_PromptBox.setEditFormat("$number$");
        kdtEntrys_position_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_position_CellEditor = new KDTDefaultCellEditor(kdtEntrys_position_PromptBox);
        this.kdtEntrys.getColumn("position").setEditor(kdtEntrys_position_CellEditor);
        ObjectValueRender kdtEntrys_position_OVR = new ObjectValueRender();
        kdtEntrys_position_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("position").setRenderer(kdtEntrys_position_OVR);
        final KDBizPromptBox kdtEntrys_adminOrg_PromptBox = new KDBizPromptBox();
        kdtEntrys_adminOrg_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");
        kdtEntrys_adminOrg_PromptBox.setVisible(true);
        kdtEntrys_adminOrg_PromptBox.setEditable(true);
        kdtEntrys_adminOrg_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_adminOrg_PromptBox.setEditFormat("$number$");
        kdtEntrys_adminOrg_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_adminOrg_CellEditor = new KDTDefaultCellEditor(kdtEntrys_adminOrg_PromptBox);
        this.kdtEntrys.getColumn("adminOrg").setEditor(kdtEntrys_adminOrg_CellEditor);
        ObjectValueRender kdtEntrys_adminOrg_OVR = new ObjectValueRender();
        kdtEntrys_adminOrg_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("adminOrg").setRenderer(kdtEntrys_adminOrg_OVR);
        final KDBizPromptBox kdtEntrys_job_PromptBox = new KDBizPromptBox();
        kdtEntrys_job_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.AssignJobF7Query");
        kdtEntrys_job_PromptBox.setVisible(true);
        kdtEntrys_job_PromptBox.setEditable(true);
        kdtEntrys_job_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_job_PromptBox.setEditFormat("$number$");
        kdtEntrys_job_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_job_CellEditor = new KDTDefaultCellEditor(kdtEntrys_job_PromptBox);
        this.kdtEntrys.getColumn("job").setEditor(kdtEntrys_job_CellEditor);
        ObjectValueRender kdtEntrys_job_OVR = new ObjectValueRender();
        kdtEntrys_job_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("job").setRenderer(kdtEntrys_job_OVR);
        final KDBizPromptBox kdtEntrys_jobLevel_PromptBox = new KDBizPromptBox();
        kdtEntrys_jobLevel_PromptBox.setQueryInfo("com.kingdee.eas.hr.org.app.JobLevelListQuery");
        kdtEntrys_jobLevel_PromptBox.setVisible(true);
        kdtEntrys_jobLevel_PromptBox.setEditable(true);
        kdtEntrys_jobLevel_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_jobLevel_PromptBox.setEditFormat("$number$");
        kdtEntrys_jobLevel_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_jobLevel_CellEditor = new KDTDefaultCellEditor(kdtEntrys_jobLevel_PromptBox);
        this.kdtEntrys.getColumn("jobLevel").setEditor(kdtEntrys_jobLevel_CellEditor);
        ObjectValueRender kdtEntrys_jobLevel_OVR = new ObjectValueRender();
        kdtEntrys_jobLevel_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("jobLevel").setRenderer(kdtEntrys_jobLevel_OVR);
        final KDBizPromptBox kdtEntrys_jobGrade_PromptBox = new KDBizPromptBox();
        kdtEntrys_jobGrade_PromptBox.setQueryInfo("com.kingdee.eas.hr.org.app.JobGradeListQuery");
        kdtEntrys_jobGrade_PromptBox.setVisible(true);
        kdtEntrys_jobGrade_PromptBox.setEditable(true);
        kdtEntrys_jobGrade_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_jobGrade_PromptBox.setEditFormat("$number$");
        kdtEntrys_jobGrade_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_jobGrade_CellEditor = new KDTDefaultCellEditor(kdtEntrys_jobGrade_PromptBox);
        this.kdtEntrys.getColumn("jobGrade").setEditor(kdtEntrys_jobGrade_CellEditor);
        ObjectValueRender kdtEntrys_jobGrade_OVR = new ObjectValueRender();
        kdtEntrys_jobGrade_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("jobGrade").setRenderer(kdtEntrys_jobGrade_OVR);
        KDDatePicker kdtEntrys_bizDate_DatePicker = new KDDatePicker();
        kdtEntrys_bizDate_DatePicker.setName("kdtEntrys_bizDate_DatePicker");
        kdtEntrys_bizDate_DatePicker.setVisible(true);
        kdtEntrys_bizDate_DatePicker.setEditable(true);
        KDTDefaultCellEditor kdtEntrys_bizDate_CellEditor = new KDTDefaultCellEditor(kdtEntrys_bizDate_DatePicker);
        this.kdtEntrys.getColumn("bizDate").setEditor(kdtEntrys_bizDate_CellEditor);
        // contApplier		
        this.contApplier.setBoundLabelText(resHelper.getString("contApplier.boundLabelText"));		
        this.contApplier.setBoundLabelLength(90);		
        this.contApplier.setBoundLabelUnderline(true);		
        this.contApplier.setBoundLabelAlignment(7);		
        this.contApplier.setVisible(true);
        // contAdminOrg		
        this.contAdminOrg.setBoundLabelText(resHelper.getString("contAdminOrg.boundLabelText"));		
        this.contAdminOrg.setBoundLabelLength(90);		
        this.contAdminOrg.setBoundLabelUnderline(true);		
        this.contAdminOrg.setBoundLabelAlignment(7);		
        this.contAdminOrg.setVisible(true);
        // contApplyDate		
        this.contApplyDate.setBoundLabelText(resHelper.getString("contApplyDate.boundLabelText"));		
        this.contApplyDate.setBoundLabelLength(90);		
        this.contApplyDate.setBoundLabelUnderline(true);		
        this.contApplyDate.setBoundLabelAlignment(7);		
        this.contApplyDate.setVisible(true);
        // contBillState		
        this.contBillState.setBoundLabelText(resHelper.getString("contBillState.boundLabelText"));		
        this.contBillState.setBoundLabelLength(90);		
        this.contBillState.setBoundLabelUnderline(true);		
        this.contBillState.setBoundLabelAlignment(7);		
        this.contBillState.setVisible(false);
        // txtNumber		
        this.txtNumber.setMaxLength(80);		
        this.txtNumber.setRequired(true);		
        this.txtNumber.setEnabled(true);		
        this.txtNumber.setHorizontalAlignment(2);
        // txtDescription		
        this.txtDescription.setMaxLength(200);		
        this.txtDescription.setEnabled(true);		
        this.txtDescription.setHorizontalAlignment(2);		
        this.txtDescription.setRequired(false);
        // txtApplier		
        this.txtApplier.setEnabled(false);		
        this.txtApplier.setMaxLength(80);		
        this.txtApplier.setVisible(true);		
        this.txtApplier.setHorizontalAlignment(2);		
        this.txtApplier.setRequired(false);
        // prmtAdminOrg		
        this.prmtAdminOrg.setRequired(true);		
        this.prmtAdminOrg.setEnabled(true);
        // dpApplyDate		
        this.dpApplyDate.setEnabled(false);		
        this.dpApplyDate.setRequired(false);
        // cbBillState		
        this.cbBillState.addItems(EnumUtils.getEnumList("com.kingdee.eas.hr.base.HRBillStateEnum").toArray());		
        this.cbBillState.setEnabled(false);		
        this.cbBillState.setVisible(false);		
        this.cbBillState.setRequired(false);
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {txtApplier,prmtHROrg,cbApproveType,txtDescription,txtNumber,cbBillState,prmtAdminOrg,dpApplyDate,kdtEntrys}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 629));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 629));
        contHROrg.setBounds(new Rectangle(10, 10, 270, 19));
        this.add(contHROrg, new KDLayout.Constraints(10, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT_SCALE));
        contApproveType.setBounds(new Rectangle(732, 54, 270, 19));
        this.add(contApproveType, new KDLayout.Constraints(732, 54, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT_SCALE | KDLayout.Constraints.ANCHOR_RIGHT));
        contNumber.setBounds(new Rectangle(10, 32, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(10, 32, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT_SCALE));
        contDescription.setBounds(new Rectangle(376, 54, 270, 19));
        this.add(contDescription, new KDLayout.Constraints(376, 54, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT_SCALE | KDLayout.Constraints.ANCHOR_RIGHT_SCALE));
        kdtEntrys.setBounds(new Rectangle(10, 76, 993, 543));
        kdtEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtEntrys,new com.kingdee.eas.custom.atsexc.AtsExceptionBillEntryInfo(),null,false);
        this.add(kdtEntrys_detailPanel, new KDLayout.Constraints(10, 76, 993, 543, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApplier.setBounds(new Rectangle(10, 54, 270, 19));
        this.add(contApplier, new KDLayout.Constraints(10, 54, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT_SCALE));
        contAdminOrg.setBounds(new Rectangle(376, 32, 270, 19));
        this.add(contAdminOrg, new KDLayout.Constraints(376, 32, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT_SCALE | KDLayout.Constraints.ANCHOR_RIGHT_SCALE));
        contApplyDate.setBounds(new Rectangle(732, 32, 270, 19));
        this.add(contApplyDate, new KDLayout.Constraints(732, 32, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT_SCALE | KDLayout.Constraints.ANCHOR_RIGHT));
        contBillState.setBounds(new Rectangle(732, 10, 270, 19));
        this.add(contBillState, new KDLayout.Constraints(732, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_LEFT_SCALE | KDLayout.Constraints.ANCHOR_RIGHT));
        //contHROrg
        contHROrg.setBoundEditor(prmtHROrg);
        //contApproveType
        contApproveType.setBoundEditor(cbApproveType);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contApplier
        contApplier.setBoundEditor(txtApplier);
        //contAdminOrg
        contAdminOrg.setBoundEditor(prmtAdminOrg);
        //contApplyDate
        contApplyDate.setBoundEditor(dpApplyDate);
        //contBillState
        contBillState.setBoundEditor(cbBillState);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(menuItemMapping);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemSendMail);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(menuItemColumnCopyAll);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(menuItemColumnCopySelect);
        menuEdit.add(separatorEdit1);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(menuItemAudit);
        menuBiz.add(menuItemUnaudit);
        menuBiz.add(menuItemPerson);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnColumnCopyAll);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnColumnCopySelect);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);
        this.toolBar.add(btnAudit);
        this.toolBar.add(btnUnaudit);
        this.toolBar.add(btnPerson);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("hrOrgUnit", com.kingdee.eas.basedata.org.HROrgUnitInfo.class, this.prmtHROrg, "data");
		dataBinder.registerBinding("approveType", com.kingdee.eas.hr.base.ApproveTypeEnum.class, this.cbApproveType, "selectedItem");
		dataBinder.registerBinding("entrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtEntrys, "id.text");
		dataBinder.registerBinding("entrys", com.kingdee.eas.custom.atsexc.AtsExceptionBillEntryInfo.class, this.kdtEntrys, "userObject");
		dataBinder.registerBinding("entrys.bizDate", java.util.Date.class, this.kdtEntrys, "bizDate.text");
		dataBinder.registerBinding("entrys.description", String.class, this.kdtEntrys, "description.text");
		dataBinder.registerBinding("entrys.person", com.kingdee.eas.basedata.person.PersonInfo.class, this.kdtEntrys, "person.text");
		dataBinder.registerBinding("entrys.person.number", String.class, this.kdtEntrys, "empNumber.text");
		dataBinder.registerBinding("entrys.person.name", String.class, this.kdtEntrys, "empName.text");
		dataBinder.registerBinding("entrys.oldPosition", com.kingdee.eas.basedata.org.PositionInfo.class, this.kdtEntrys, "oldPosition.text");
		dataBinder.registerBinding("entrys.oldAdminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.kdtEntrys, "oldAdminOrg.text");
		dataBinder.registerBinding("entrys.oldJob", com.kingdee.eas.basedata.org.JobInfo.class, this.kdtEntrys, "oldJob.text");
		dataBinder.registerBinding("entrys.oldJobLevel", com.kingdee.eas.hr.org.JobLevelInfo.class, this.kdtEntrys, "oldJobLevel.text");
		dataBinder.registerBinding("entrys.position", com.kingdee.eas.basedata.org.PositionInfo.class, this.kdtEntrys, "position.text");
		dataBinder.registerBinding("entrys.adminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.kdtEntrys, "adminOrg.text");
		dataBinder.registerBinding("entrys.job", com.kingdee.eas.basedata.org.JobInfo.class, this.kdtEntrys, "job.text");
		dataBinder.registerBinding("entrys.jobLevel", com.kingdee.eas.hr.org.JobLevelInfo.class, this.kdtEntrys, "jobLevel.text");
		dataBinder.registerBinding("entrys.jobGrade", com.kingdee.eas.hr.org.JobGradeInfo.class, this.kdtEntrys, "jobGrade.text");
		dataBinder.registerBinding("entrys.oldJobGrade", com.kingdee.eas.hr.org.JobGradeInfo.class, this.kdtEntrys, "oldJobGrade.text");
		dataBinder.registerBinding("entrys.time", String.class, this.kdtEntrys, "time.text");
		dataBinder.registerBinding("entrys.type", java.lang.Object.class, this.kdtEntrys, "type.text");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("applier.name", String.class, this.txtApplier, "text");
		dataBinder.registerBinding("adminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtAdminOrg, "data");
		dataBinder.registerBinding("applyDate", java.util.Date.class, this.dpApplyDate, "value");
		dataBinder.registerBinding("billState", com.kingdee.eas.hr.base.HRBillStateEnum.class, this.cbBillState, "selectedItem");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.atsexc.app.AtsExceptionBillEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.txtApplier.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"NONE",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("NONE");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("hrOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("approveType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person.number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person.name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.oldPosition", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.oldAdminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.oldJob", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.oldJobLevel", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.position", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.adminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.job", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.jobLevel", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.jobGrade", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.oldJobGrade", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.time", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.type", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("applier.name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("applyDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("billState", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }

    /**
     * output kdtEntrys_editStopping method
     */
    protected void kdtEntrys_editStopping(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output kdtEntrys_editStopped method
     */
    protected void kdtEntrys_editStopped(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("hrOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("hrOrgUnit.id"));
        	sic.add(new SelectorItemInfo("hrOrgUnit.number"));
        	sic.add(new SelectorItemInfo("hrOrgUnit.name"));
		}
        sic.add(new SelectorItemInfo("approveType"));
    	sic.add(new SelectorItemInfo("entrys.id"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.*"));
		}
		else{
		}
    	sic.add(new SelectorItemInfo("entrys.bizDate"));
    	sic.add(new SelectorItemInfo("entrys.description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.person.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.person.id"));
			sic.add(new SelectorItemInfo("entrys.person.name"));
        	sic.add(new SelectorItemInfo("entrys.person.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.oldPosition.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.oldPosition.id"));
			sic.add(new SelectorItemInfo("entrys.oldPosition.name"));
        	sic.add(new SelectorItemInfo("entrys.oldPosition.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.oldAdminOrg.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.oldAdminOrg.id"));
			sic.add(new SelectorItemInfo("entrys.oldAdminOrg.name"));
        	sic.add(new SelectorItemInfo("entrys.oldAdminOrg.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.oldJob.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.oldJob.id"));
			sic.add(new SelectorItemInfo("entrys.oldJob.name"));
        	sic.add(new SelectorItemInfo("entrys.oldJob.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.oldJobLevel.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.oldJobLevel.id"));
			sic.add(new SelectorItemInfo("entrys.oldJobLevel.name"));
        	sic.add(new SelectorItemInfo("entrys.oldJobLevel.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.position.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.position.id"));
			sic.add(new SelectorItemInfo("entrys.position.name"));
        	sic.add(new SelectorItemInfo("entrys.position.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.adminOrg.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.adminOrg.id"));
			sic.add(new SelectorItemInfo("entrys.adminOrg.name"));
        	sic.add(new SelectorItemInfo("entrys.adminOrg.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.job.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.job.id"));
			sic.add(new SelectorItemInfo("entrys.job.name"));
        	sic.add(new SelectorItemInfo("entrys.job.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.jobLevel.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.jobLevel.id"));
			sic.add(new SelectorItemInfo("entrys.jobLevel.name"));
        	sic.add(new SelectorItemInfo("entrys.jobLevel.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.jobGrade.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.jobGrade.id"));
			sic.add(new SelectorItemInfo("entrys.jobGrade.name"));
        	sic.add(new SelectorItemInfo("entrys.jobGrade.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.oldJobGrade.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.oldJobGrade.id"));
			sic.add(new SelectorItemInfo("entrys.oldJobGrade.name"));
        	sic.add(new SelectorItemInfo("entrys.oldJobGrade.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.time"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.type.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.type.id"));
			sic.add(new SelectorItemInfo("entrys.type.name"));
        	sic.add(new SelectorItemInfo("entrys.type.number"));
		}
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("description"));
        sic.add(new SelectorItemInfo("applier.name"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrg.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrg.id"));
        	sic.add(new SelectorItemInfo("adminOrg.number"));
        	sic.add(new SelectorItemInfo("adminOrg.name"));
		}
        sic.add(new SelectorItemInfo("applyDate"));
        sic.add(new SelectorItemInfo("billState"));
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
    	if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.print(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
        if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.printPreview(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionAudit_actionPerformed(e);
    }
    	

    /**
     * output actionUnaudit_actionPerformed method
     */
    public void actionUnaudit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionUnaudit_actionPerformed(e);
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionAudit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnaudit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionUnaudit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnaudit() {
    	return false;
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.atsexc.client", "AtsExceptionBillEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.atsexc.client.AtsExceptionBillEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.atsexc.AtsExceptionBillFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo objectValue = new com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo();
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/atsexc/AtsExceptionBill";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.atsexc.app.AtsExceptionBillQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtEntrys;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}