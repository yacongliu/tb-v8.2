package com.kingdee.eas.custom.atsexc;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAtsExceptionBillEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractAtsExceptionBillEntryInfo()
    {
        this("id");
    }
    protected AbstractAtsExceptionBillEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo getBill()
    {
        return (com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ԭ��property 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getOldAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("oldAdminOrg");
    }
    public void setOldAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("oldAdminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getOldPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("oldPosition");
    }
    public void setOldPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("oldPosition", item);
    }
    /**
     * Object: ��¼ 's ��ǰְ�� property 
     */
    public com.kingdee.eas.basedata.org.JobInfo getOldJob()
    {
        return (com.kingdee.eas.basedata.org.JobInfo)get("oldJob");
    }
    public void setOldJob(com.kingdee.eas.basedata.org.JobInfo item)
    {
        put("oldJob", item);
    }
    /**
     * Object: ��¼ 's ��ǰְ�� property 
     */
    public com.kingdee.eas.hr.org.JobLevelInfo getOldJobLevel()
    {
        return (com.kingdee.eas.hr.org.JobLevelInfo)get("oldJobLevel");
    }
    public void setOldJobLevel(com.kingdee.eas.hr.org.JobLevelInfo item)
    {
        put("oldJobLevel", item);
    }
    /**
     * Object: ��¼ 's ��ǰְ�� property 
     */
    public com.kingdee.eas.hr.org.JobGradeInfo getOldJobGrade()
    {
        return (com.kingdee.eas.hr.org.JobGradeInfo)get("oldJobGrade");
    }
    public void setOldJobGrade(com.kingdee.eas.hr.org.JobGradeInfo item)
    {
        put("oldJobGrade", item);
    }
    /**
     * Object: ��¼ 's Ŀ��������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's Ŀ��ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object: ��¼ 's Ŀ��ְ�� property 
     */
    public com.kingdee.eas.basedata.org.JobInfo getJob()
    {
        return (com.kingdee.eas.basedata.org.JobInfo)get("job");
    }
    public void setJob(com.kingdee.eas.basedata.org.JobInfo item)
    {
        put("job", item);
    }
    /**
     * Object: ��¼ 's Ŀ��ְ�� property 
     */
    public com.kingdee.eas.hr.org.JobLevelInfo getJobLevel()
    {
        return (com.kingdee.eas.hr.org.JobLevelInfo)get("jobLevel");
    }
    public void setJobLevel(com.kingdee.eas.hr.org.JobLevelInfo item)
    {
        put("jobLevel", item);
    }
    /**
     * Object: ��¼ 's Ŀ��ְ�� property 
     */
    public com.kingdee.eas.hr.org.JobGradeInfo getJobGrade()
    {
        return (com.kingdee.eas.hr.org.JobGradeInfo)get("jobGrade");
    }
    public void setJobGrade(com.kingdee.eas.hr.org.JobGradeInfo item)
    {
        put("jobGrade", item);
    }
    /**
     * Object:��¼'s ʱ��property 
     */
    public String getTime()
    {
        return getString("time");
    }
    public void setTime(String item)
    {
        setString("time", item);
    }
    /**
     * Object: ��¼ 's �쳣���� property 
     */
    public com.kingdee.eas.custom.atsexc.bd.TypeInfo getType()
    {
        return (com.kingdee.eas.custom.atsexc.bd.TypeInfo)get("type");
    }
    public void setType(com.kingdee.eas.custom.atsexc.bd.TypeInfo item)
    {
        put("type", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("B48732A9");
    }
}