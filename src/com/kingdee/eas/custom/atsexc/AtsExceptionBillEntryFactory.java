package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AtsExceptionBillEntryFactory
{
    private AtsExceptionBillEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("B48732A9") ,com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry.class);
    }
    
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("B48732A9") ,com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("B48732A9"));
    }
    public static com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.IAtsExceptionBillEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("B48732A9"));
    }
}