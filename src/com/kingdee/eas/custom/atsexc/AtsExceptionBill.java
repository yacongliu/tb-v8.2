package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.custom.atsexc.app.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class AtsExceptionBill extends HRBillBase implements IAtsExceptionBill
{
    public AtsExceptionBill()
    {
        super();
        registerInterface(IAtsExceptionBill.class, this);
    }
    public AtsExceptionBill(Context ctx)
    {
        super(ctx);
        registerInterface(IAtsExceptionBill.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("A3498B49");
    }
    private AtsExceptionBillController getController() throws BOSException
    {
        return (AtsExceptionBillController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public AtsExceptionBillCollection getAtsExceptionBillCollection() throws BOSException
    {
        try {
            return getController().getAtsExceptionBillCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public AtsExceptionBillCollection getAtsExceptionBillCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAtsExceptionBillCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public AtsExceptionBillCollection getAtsExceptionBillCollection(String oql) throws BOSException
    {
        try {
            return getController().getAtsExceptionBillCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public AtsExceptionBillInfo getAtsExceptionBillInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAtsExceptionBillInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public AtsExceptionBillInfo getAtsExceptionBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAtsExceptionBillInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public AtsExceptionBillInfo getAtsExceptionBillInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAtsExceptionBillInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����״̬-User defined method
     *@param billId ����id
     *@param state ����״̬
     */
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext(), billId, state);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��-User defined method
     *@param billId ����id
     */
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������ͨ��״̬-User defined method
     *@param billId ����id
     */
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������״̬-User defined method
     *@param billId ����id
     */
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����δ���״̬-User defined method
     *@param billId ����id
     */
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}