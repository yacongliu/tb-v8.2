package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public interface IAtsExceptionBill extends IHRBillBase
{
    public AtsExceptionBillCollection getAtsExceptionBillCollection() throws BOSException;
    public AtsExceptionBillCollection getAtsExceptionBillCollection(EntityViewInfo view) throws BOSException;
    public AtsExceptionBillCollection getAtsExceptionBillCollection(String oql) throws BOSException;
    public AtsExceptionBillInfo getAtsExceptionBillInfo(IObjectPK pk) throws BOSException, EASBizException;
    public AtsExceptionBillInfo getAtsExceptionBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public AtsExceptionBillInfo getAtsExceptionBillInfo(String oql) throws BOSException, EASBizException;
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException;
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException;
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException;
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException;
}