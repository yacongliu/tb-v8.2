package com.kingdee.eas.custom.atsexc.app;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.ApproveTypeEnum;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hrcus.worklist.EnrollInfo;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: EnrollControllerBean <br>
 *            Description: 考勤异常单-服务端
 * @author 杨旭东 Email:tjyangxudong@kingdee.com
 * @date 2019-07-31
 * @since V1.0
 */
public class AtsExceptionBillControllerBean extends
		AbstractAtsExceptionBillControllerBean {
	private static final long serialVersionUID = -2680585496810747495L;
	private static Logger logger = Logger
			.getLogger("com.kingdee.eas.custom.atsexc.app.AtsExceptionBillControllerBean");

	/**
	 * 提交生效
	 */
	@Override
	protected IObjectPK _submitEffect(Context ctx, CoreBaseInfo model)
			throws BOSException, EASBizException {

		AtsExceptionBillInfo bill = (AtsExceptionBillInfo) model;
		bill.setApproveType(ApproveTypeEnum.DIRECT);
		bill.setBillState(HRBillStateEnum.AUDITED);
		IObjectPK objectPK = super.save(ctx, model);

		_setState(ctx, bill.getId(), HRBillStateEnum.AUDITED);

		return objectPK;
	}

	/**
	 * 设置单据状态
	 */
	@Override
	protected void _setState(Context ctx, BOSUuid billId, HRBillStateEnum state)
			throws BOSException, EASBizException {
		IObjectPK pk = new ObjectUuidPK(billId);
		AtsExceptionBillInfo info = (AtsExceptionBillInfo) _getValue(ctx,
				"select id, billState  where id = '" + pk.toString() + "'");

		info.setBillState(state);
		SelectorItemCollection selectors = new SelectorItemCollection();
		selectors.add(new SelectorItemInfo("id"));
		selectors.add(new SelectorItemInfo("billState"));

		updatePartial(ctx, info, selectors);
	}

	/**
	 * 设置审批中状态
	 */
	@Override
	protected void _setApproveState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITING);
	}

	/**
	 * 设置未审核状态
	 */
	@Override
	protected void _setEditState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.SAVED);
	}

	/**
	 * 设置审核不通过状态
	 */
	@Override
	protected void _setNoPassState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITEND);
	}

	/**
	 * 设置审核通过状态
	 */
	@Override
	protected void _setPassState(Context ctx, BOSUuid billId)
			throws BOSException, EASBizException {
		_setState(ctx, billId, HRBillStateEnum.AUDITEND);
	}

}