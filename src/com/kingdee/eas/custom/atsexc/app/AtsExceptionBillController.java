package com.kingdee.eas.custom.atsexc.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.eas.custom.atsexc.AtsExceptionBillCollection;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.custom.atsexc.AtsExceptionBillInfo;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.app.HRBillBaseController;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface AtsExceptionBillController extends HRBillBaseController
{
    public AtsExceptionBillCollection getAtsExceptionBillCollection(Context ctx) throws BOSException, RemoteException;
    public AtsExceptionBillCollection getAtsExceptionBillCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public AtsExceptionBillCollection getAtsExceptionBillCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public AtsExceptionBillInfo getAtsExceptionBillInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public AtsExceptionBillInfo getAtsExceptionBillInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public AtsExceptionBillInfo getAtsExceptionBillInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public void setState(Context ctx, BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException, RemoteException;
    public void setPassState(Context ctx, BOSUuid billId) throws BOSException, EASBizException, RemoteException;
    public void setNoPassState(Context ctx, BOSUuid billId) throws BOSException, EASBizException, RemoteException;
    public void setApproveState(Context ctx, BOSUuid billId) throws BOSException, EASBizException, RemoteException;
    public void setEditState(Context ctx, BOSUuid billId) throws BOSException, EASBizException, RemoteException;
}