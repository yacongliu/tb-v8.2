package com.kingdee.eas.custom.atsexc;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AtsExceptionBillCollection extends AbstractObjectCollection 
{
    public AtsExceptionBillCollection()
    {
        super(AtsExceptionBillInfo.class);
    }
    public boolean add(AtsExceptionBillInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AtsExceptionBillCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AtsExceptionBillInfo item)
    {
        return removeObject(item);
    }
    public AtsExceptionBillInfo get(int index)
    {
        return(AtsExceptionBillInfo)getObject(index);
    }
    public AtsExceptionBillInfo get(Object key)
    {
        return(AtsExceptionBillInfo)getObject(key);
    }
    public void set(int index, AtsExceptionBillInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AtsExceptionBillInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AtsExceptionBillInfo item)
    {
        return super.indexOf(item);
    }
}