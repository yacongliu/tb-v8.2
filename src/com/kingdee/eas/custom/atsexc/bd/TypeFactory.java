package com.kingdee.eas.custom.atsexc.bd;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class TypeFactory
{
    private TypeFactory()
    {
    }
    public static com.kingdee.eas.custom.atsexc.bd.IType getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.bd.IType)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("0BED13AD") ,com.kingdee.eas.custom.atsexc.bd.IType.class);
    }
    
    public static com.kingdee.eas.custom.atsexc.bd.IType getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.bd.IType)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("0BED13AD") ,com.kingdee.eas.custom.atsexc.bd.IType.class, objectCtx);
    }
    public static com.kingdee.eas.custom.atsexc.bd.IType getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.bd.IType)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("0BED13AD"));
    }
    public static com.kingdee.eas.custom.atsexc.bd.IType getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.atsexc.bd.IType)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("0BED13AD"));
    }
}