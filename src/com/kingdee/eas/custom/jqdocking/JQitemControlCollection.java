package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class JQitemControlCollection extends AbstractObjectCollection 
{
    public JQitemControlCollection()
    {
        super(JQitemControlInfo.class);
    }
    public boolean add(JQitemControlInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(JQitemControlCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(JQitemControlInfo item)
    {
        return removeObject(item);
    }
    public JQitemControlInfo get(int index)
    {
        return(JQitemControlInfo)getObject(index);
    }
    public JQitemControlInfo get(Object key)
    {
        return(JQitemControlInfo)getObject(key);
    }
    public void set(int index, JQitemControlInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(JQitemControlInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(JQitemControlInfo item)
    {
        return super.indexOf(item);
    }
}