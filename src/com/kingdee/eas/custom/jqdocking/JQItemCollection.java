package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class JQItemCollection extends AbstractObjectCollection 
{
    public JQItemCollection()
    {
        super(JQItemInfo.class);
    }
    public boolean add(JQItemInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(JQItemCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(JQItemInfo item)
    {
        return removeObject(item);
    }
    public JQItemInfo get(int index)
    {
        return(JQItemInfo)getObject(index);
    }
    public JQItemInfo get(Object key)
    {
        return(JQItemInfo)getObject(key);
    }
    public void set(int index, JQItemInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(JQItemInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(JQItemInfo item)
    {
        return super.indexOf(item);
    }
}