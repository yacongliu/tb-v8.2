package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBase;

public interface IAnnuitySlip extends IHRBillBase
{
    public AnnuitySlipCollection getAnnuitySlipCollection() throws BOSException;
    public AnnuitySlipCollection getAnnuitySlipCollection(EntityViewInfo view) throws BOSException;
    public AnnuitySlipCollection getAnnuitySlipCollection(String oql) throws BOSException;
    public AnnuitySlipInfo getAnnuitySlipInfo(IObjectPK pk) throws BOSException, EASBizException;
    public AnnuitySlipInfo getAnnuitySlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public AnnuitySlipInfo getAnnuitySlipInfo(String oql) throws BOSException, EASBizException;
}