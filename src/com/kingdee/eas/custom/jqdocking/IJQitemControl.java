package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBase;

public interface IJQitemControl extends IHRBillBase
{
    public JQitemControlCollection getJQitemControlCollection() throws BOSException;
    public JQitemControlCollection getJQitemControlCollection(EntityViewInfo view) throws BOSException;
    public JQitemControlCollection getJQitemControlCollection(String oql) throws BOSException;
    public JQitemControlInfo getJQitemControlInfo(IObjectPK pk) throws BOSException, EASBizException;
    public JQitemControlInfo getJQitemControlInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public JQitemControlInfo getJQitemControlInfo(String oql) throws BOSException, EASBizException;
}