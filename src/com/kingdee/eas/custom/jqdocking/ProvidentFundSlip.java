package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.ProvidentFundSlipController;
import com.kingdee.eas.hr.base.HRBillBase;

public class ProvidentFundSlip extends HRBillBase implements IProvidentFundSlip
{
    public ProvidentFundSlip()
    {
        super();
        registerInterface(IProvidentFundSlip.class, this);
    }
    public ProvidentFundSlip(Context ctx)
    {
        super(ctx);
        registerInterface(IProvidentFundSlip.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("15EE8D81");
    }
    private ProvidentFundSlipController getController() throws BOSException
    {
        return (ProvidentFundSlipController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ProvidentFundSlipCollection getProvidentFundSlipCollection() throws BOSException
    {
        try {
            return getController().getProvidentFundSlipCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getProvidentFundSlipCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(String oql) throws BOSException
    {
        try {
            return getController().getProvidentFundSlipCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getProvidentFundSlipInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getProvidentFundSlipInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getProvidentFundSlipInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}