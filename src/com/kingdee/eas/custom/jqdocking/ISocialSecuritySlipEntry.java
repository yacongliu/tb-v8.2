package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;

public interface ISocialSecuritySlipEntry extends IHRBillBaseEntry
{
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(String oql) throws BOSException, EASBizException;
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection() throws BOSException;
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(EntityViewInfo view) throws BOSException;
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(String oql) throws BOSException;
}