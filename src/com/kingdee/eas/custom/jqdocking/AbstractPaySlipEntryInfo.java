package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractPaySlipEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractPaySlipEntryInfo()
    {
        this("id");
    }
    protected AbstractPaySlipEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.jqdocking.PaySlipInfo getBill()
    {
        return (com.kingdee.eas.custom.jqdocking.PaySlipInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.jqdocking.PaySlipInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��¼'s ����property 
     */
    public java.math.BigDecimal getWage()
    {
        return getBigDecimal("wage");
    }
    public void setWage(java.math.BigDecimal item)
    {
        setBigDecimal("wage", item);
    }
    /**
     * Object:��¼'s ����Ͳ���property 
     */
    public java.math.BigDecimal getSubsidy()
    {
        return getBigDecimal("subsidy");
    }
    public void setSubsidy(java.math.BigDecimal item)
    {
        setBigDecimal("subsidy", item);
    }
    /**
     * Object:��¼'s �����property 
     */
    public java.math.BigDecimal getWelfareFee()
    {
        return getBigDecimal("welfareFee");
    }
    public void setWelfareFee(java.math.BigDecimal item)
    {
        setBigDecimal("welfareFee", item);
    }
    /**
     * Object:��¼'s ��ͨ����property 
     */
    public java.math.BigDecimal getTransportSubsidy()
    {
        return getBigDecimal("transportSubsidy");
    }
    public void setTransportSubsidy(java.math.BigDecimal item)
    {
        setBigDecimal("transportSubsidy", item);
    }
    /**
     * Object:��¼'s ��λҽ�Ʊ���property 
     */
    public java.math.BigDecimal getUnitMedical()
    {
        return getBigDecimal("unitMedical");
    }
    public void setUnitMedical(java.math.BigDecimal item)
    {
        setBigDecimal("unitMedical", item);
    }
    /**
     * Object:��¼'s ��λ���ϱ���property 
     */
    public java.math.BigDecimal getUnitPension()
    {
        return getBigDecimal("unitPension");
    }
    public void setUnitPension(java.math.BigDecimal item)
    {
        setBigDecimal("unitPension", item);
    }
    /**
     * Object:��¼'s ��λʧҵ����property 
     */
    public java.math.BigDecimal getUnitUnemployment()
    {
        return getBigDecimal("unitUnemployment");
    }
    public void setUnitUnemployment(java.math.BigDecimal item)
    {
        setBigDecimal("unitUnemployment", item);
    }
    /**
     * Object:��¼'s ��λ���˱���property 
     */
    public java.math.BigDecimal getUnitInjury()
    {
        return getBigDecimal("unitInjury");
    }
    public void setUnitInjury(java.math.BigDecimal item)
    {
        setBigDecimal("unitInjury", item);
    }
    /**
     * Object:��¼'s ��λ������property 
     */
    public java.math.BigDecimal getUnitMaternity()
    {
        return getBigDecimal("unitMaternity");
    }
    public void setUnitMaternity(java.math.BigDecimal item)
    {
        setBigDecimal("unitMaternity", item);
    }
    /**
     * Object:��¼'s �������ϱ���property 
     */
    public java.math.BigDecimal getPerPension()
    {
        return getBigDecimal("perPension");
    }
    public void setPerPension(java.math.BigDecimal item)
    {
        setBigDecimal("perPension", item);
    }
    /**
     * Object:��¼'s ����ҽ�Ʊ���property 
     */
    public java.math.BigDecimal getPerMedical()
    {
        return getBigDecimal("perMedical");
    }
    public void setPerMedical(java.math.BigDecimal item)
    {
        setBigDecimal("perMedical", item);
    }
    /**
     * Object:��¼'s ����ʧҵ����property 
     */
    public java.math.BigDecimal getPerUnemployment()
    {
        return getBigDecimal("perUnemployment");
    }
    public void setPerUnemployment(java.math.BigDecimal item)
    {
        setBigDecimal("perUnemployment", item);
    }
    /**
     * Object:��¼'s ��λ�����property 
     */
    public java.math.BigDecimal getUnitProvidentFund()
    {
        return getBigDecimal("unitProvidentFund");
    }
    public void setUnitProvidentFund(java.math.BigDecimal item)
    {
        setBigDecimal("unitProvidentFund", item);
    }
    /**
     * Object:��¼'s ���˹����property 
     */
    public java.math.BigDecimal getPerProvidentFund()
    {
        return getBigDecimal("perProvidentFund");
    }
    public void setPerProvidentFund(java.math.BigDecimal item)
    {
        setBigDecimal("perProvidentFund", item);
    }
    /**
     * Object:��¼'s ���乫���property 
     */
    public java.math.BigDecimal getAddProvidentFund()
    {
        return getBigDecimal("addProvidentFund");
    }
    public void setAddProvidentFund(java.math.BigDecimal item)
    {
        setBigDecimal("addProvidentFund", item);
    }
    /**
     * Object:��¼'s ��λ��ҵ���property 
     */
    public java.math.BigDecimal getUnitAnnuity()
    {
        return getBigDecimal("unitAnnuity");
    }
    public void setUnitAnnuity(java.math.BigDecimal item)
    {
        setBigDecimal("unitAnnuity", item);
    }
    /**
     * Object:��¼'s ������ҵ���property 
     */
    public java.math.BigDecimal getPerAnnuity()
    {
        return getBigDecimal("perAnnuity");
    }
    public void setPerAnnuity(java.math.BigDecimal item)
    {
        setBigDecimal("perAnnuity", item);
    }
    /**
     * Object:��¼'s ����˰�ۿ�property 
     */
    public java.math.BigDecimal getPersonalIncomeTax()
    {
        return getBigDecimal("personalIncomeTax");
    }
    public void setPersonalIncomeTax(java.math.BigDecimal item)
    {
        setBigDecimal("personalIncomeTax", item);
    }
    /**
     * Object:��¼'s ʵ�����ʶ�property 
     */
    public java.math.BigDecimal getRealWage()
    {
        return getBigDecimal("realWage");
    }
    public void setRealWage(java.math.BigDecimal item)
    {
        setBigDecimal("realWage", item);
    }
    /**
     * Object:��¼'s �����Ա��property 
     */
    public java.math.BigDecimal getUnionMemberFee()
    {
        return getBigDecimal("unionMemberFee");
    }
    public void setUnionMemberFee(java.math.BigDecimal item)
    {
        setBigDecimal("unionMemberFee", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("C93CA795");
    }
}