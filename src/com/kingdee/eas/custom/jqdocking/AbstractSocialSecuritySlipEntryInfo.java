package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractSocialSecuritySlipEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractSocialSecuritySlipEntryInfo()
    {
        this("id");
    }
    protected AbstractSocialSecuritySlipEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.jqdocking.SocialSecuritySlipInfo getBill()
    {
        return (com.kingdee.eas.custom.jqdocking.SocialSecuritySlipInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.jqdocking.SocialSecuritySlipInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��¼'s ��λҽ�Ʊ���property 
     */
    public java.math.BigDecimal getUnitMedical()
    {
        return getBigDecimal("unitMedical");
    }
    public void setUnitMedical(java.math.BigDecimal item)
    {
        setBigDecimal("unitMedical", item);
    }
    /**
     * Object:��¼'s ��λ���ϱ���property 
     */
    public java.math.BigDecimal getUnitPension()
    {
        return getBigDecimal("unitPension");
    }
    public void setUnitPension(java.math.BigDecimal item)
    {
        setBigDecimal("unitPension", item);
    }
    /**
     * Object:��¼'s ��λʧҵ����property 
     */
    public java.math.BigDecimal getUnitUnemployment()
    {
        return getBigDecimal("unitUnemployment");
    }
    public void setUnitUnemployment(java.math.BigDecimal item)
    {
        setBigDecimal("unitUnemployment", item);
    }
    /**
     * Object:��¼'s ��λ���˱���property 
     */
    public java.math.BigDecimal getUnitInjury()
    {
        return getBigDecimal("unitInjury");
    }
    public void setUnitInjury(java.math.BigDecimal item)
    {
        setBigDecimal("unitInjury", item);
    }
    /**
     * Object:��¼'s ��λ������property 
     */
    public java.math.BigDecimal getUnitMaternity()
    {
        return getBigDecimal("unitMaternity");
    }
    public void setUnitMaternity(java.math.BigDecimal item)
    {
        setBigDecimal("unitMaternity", item);
    }
    /**
     * Object:��¼'s �������ϱ���property 
     */
    public java.math.BigDecimal getPerPension()
    {
        return getBigDecimal("perPension");
    }
    public void setPerPension(java.math.BigDecimal item)
    {
        setBigDecimal("perPension", item);
    }
    /**
     * Object:��¼'s ����ҽ�Ʊ���property 
     */
    public java.math.BigDecimal getPerMedical()
    {
        return getBigDecimal("perMedical");
    }
    public void setPerMedical(java.math.BigDecimal item)
    {
        setBigDecimal("perMedical", item);
    }
    /**
     * Object:��¼'s ����ʧҵ����property 
     */
    public java.math.BigDecimal getPerUnemployment()
    {
        return getBigDecimal("perUnemployment");
    }
    public void setPerUnemployment(java.math.BigDecimal item)
    {
        setBigDecimal("perUnemployment", item);
    }
    /**
     * Object:��¼'s ��������property 
     */
    public com.kingdee.eas.custom.jqdocking.app.releaseType getReleaseType()
    {
        return com.kingdee.eas.custom.jqdocking.app.releaseType.getEnum(getString("releaseType"));
    }
    public void setReleaseType(com.kingdee.eas.custom.jqdocking.app.releaseType item)
    {
		if (item != null) {
        setString("releaseType", item.getValue());
		}
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("1CFD069A");
    }
}