package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class AnnuitySlipEntryCollection extends AbstractObjectCollection 
{
    public AnnuitySlipEntryCollection()
    {
        super(AnnuitySlipEntryInfo.class);
    }
    public boolean add(AnnuitySlipEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AnnuitySlipEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AnnuitySlipEntryInfo item)
    {
        return removeObject(item);
    }
    public AnnuitySlipEntryInfo get(int index)
    {
        return(AnnuitySlipEntryInfo)getObject(index);
    }
    public AnnuitySlipEntryInfo get(Object key)
    {
        return(AnnuitySlipEntryInfo)getObject(key);
    }
    public void set(int index, AnnuitySlipEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AnnuitySlipEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AnnuitySlipEntryInfo item)
    {
        return super.indexOf(item);
    }
}