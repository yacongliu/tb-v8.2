package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

public class SocialSecuritySlipEntryInfo extends AbstractSocialSecuritySlipEntryInfo implements Serializable 
{
    public SocialSecuritySlipEntryInfo()
    {
        super();
    }
    protected SocialSecuritySlipEntryInfo(String pkField)
    {
        super(pkField);
    }
}