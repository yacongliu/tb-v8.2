package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.JQItemController;
import com.kingdee.eas.framework.DataBase;

public class JQItem extends DataBase implements IJQItem
{
    public JQItem()
    {
        super();
        registerInterface(IJQItem.class, this);
    }
    public JQItem(Context ctx)
    {
        super(ctx);
        registerInterface(IJQItem.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("B0A3E965");
    }
    private JQItemController getController() throws BOSException
    {
        return (JQItemController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public JQItemInfo getJQItemInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getJQItemInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public JQItemInfo getJQItemInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getJQItemInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public JQItemInfo getJQItemInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getJQItemInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public JQItemCollection getJQItemCollection() throws BOSException
    {
        try {
            return getController().getJQItemCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public JQItemCollection getJQItemCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getJQItemCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public JQItemCollection getJQItemCollection(String oql) throws BOSException
    {
        try {
            return getController().getJQItemCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}