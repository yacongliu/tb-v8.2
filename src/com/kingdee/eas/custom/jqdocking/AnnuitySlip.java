package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.AnnuitySlipController;
import com.kingdee.eas.hr.base.HRBillBase;

public class AnnuitySlip extends HRBillBase implements IAnnuitySlip
{
    public AnnuitySlip()
    {
        super();
        registerInterface(IAnnuitySlip.class, this);
    }
    public AnnuitySlip(Context ctx)
    {
        super(ctx);
        registerInterface(IAnnuitySlip.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("AF27402F");
    }
    private AnnuitySlipController getController() throws BOSException
    {
        return (AnnuitySlipController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public AnnuitySlipCollection getAnnuitySlipCollection() throws BOSException
    {
        try {
            return getController().getAnnuitySlipCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public AnnuitySlipCollection getAnnuitySlipCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAnnuitySlipCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public AnnuitySlipCollection getAnnuitySlipCollection(String oql) throws BOSException
    {
        try {
            return getController().getAnnuitySlipCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public AnnuitySlipInfo getAnnuitySlipInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAnnuitySlipInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public AnnuitySlipInfo getAnnuitySlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAnnuitySlipInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public AnnuitySlipInfo getAnnuitySlipInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAnnuitySlipInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}