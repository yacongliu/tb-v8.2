package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

public class ProvidentFundSlipInfo extends AbstractProvidentFundSlipInfo implements Serializable 
{
    public ProvidentFundSlipInfo()
    {
        super();
    }
    protected ProvidentFundSlipInfo(String pkField)
    {
        super(pkField);
    }
}