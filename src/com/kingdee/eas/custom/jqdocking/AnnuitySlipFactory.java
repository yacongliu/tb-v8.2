package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class AnnuitySlipFactory
{
    private AnnuitySlipFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlip getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlip)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("AF27402F") ,com.kingdee.eas.custom.jqdocking.IAnnuitySlip.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlip getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlip)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("AF27402F") ,com.kingdee.eas.custom.jqdocking.IAnnuitySlip.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlip getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlip)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("AF27402F"));
    }
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlip getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlip)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("AF27402F"));
    }
}