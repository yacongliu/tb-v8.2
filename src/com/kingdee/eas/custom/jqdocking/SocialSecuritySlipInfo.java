package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

public class SocialSecuritySlipInfo extends AbstractSocialSecuritySlipInfo implements Serializable 
{
    public SocialSecuritySlipInfo()
    {
        super();
    }
    protected SocialSecuritySlipInfo(String pkField)
    {
        super(pkField);
    }
}