package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractJQItemInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractJQItemInfo()
    {
        this("id");
    }
    protected AbstractJQItemInfo(String pkField)
    {
        super(pkField);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("B0A3E965");
    }
}