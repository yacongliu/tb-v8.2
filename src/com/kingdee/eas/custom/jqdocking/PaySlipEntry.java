package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.PaySlipEntryController;
import com.kingdee.eas.hr.base.HRBillBaseEntry;

public class PaySlipEntry extends HRBillBaseEntry implements IPaySlipEntry
{
    public PaySlipEntry()
    {
        super();
        registerInterface(IPaySlipEntry.class, this);
    }
    public PaySlipEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IPaySlipEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C93CA795");
    }
    private PaySlipEntryController getController() throws BOSException
    {
        return (PaySlipEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public PaySlipEntryInfo getPaySlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getPaySlipEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public PaySlipEntryInfo getPaySlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getPaySlipEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public PaySlipEntryInfo getPaySlipEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getPaySlipEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public PaySlipEntryCollection getPaySlipEntryCollection() throws BOSException
    {
        try {
            return getController().getPaySlipEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public PaySlipEntryCollection getPaySlipEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getPaySlipEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public PaySlipEntryCollection getPaySlipEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getPaySlipEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}