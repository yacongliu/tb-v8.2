package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class SocialSecuritySlipEntryCollection extends AbstractObjectCollection 
{
    public SocialSecuritySlipEntryCollection()
    {
        super(SocialSecuritySlipEntryInfo.class);
    }
    public boolean add(SocialSecuritySlipEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SocialSecuritySlipEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SocialSecuritySlipEntryInfo item)
    {
        return removeObject(item);
    }
    public SocialSecuritySlipEntryInfo get(int index)
    {
        return(SocialSecuritySlipEntryInfo)getObject(index);
    }
    public SocialSecuritySlipEntryInfo get(Object key)
    {
        return(SocialSecuritySlipEntryInfo)getObject(key);
    }
    public void set(int index, SocialSecuritySlipEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SocialSecuritySlipEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SocialSecuritySlipEntryInfo item)
    {
        return super.indexOf(item);
    }
}