package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class ProvidentFundSlipEntryCollection extends AbstractObjectCollection 
{
    public ProvidentFundSlipEntryCollection()
    {
        super(ProvidentFundSlipEntryInfo.class);
    }
    public boolean add(ProvidentFundSlipEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ProvidentFundSlipEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ProvidentFundSlipEntryInfo item)
    {
        return removeObject(item);
    }
    public ProvidentFundSlipEntryInfo get(int index)
    {
        return(ProvidentFundSlipEntryInfo)getObject(index);
    }
    public ProvidentFundSlipEntryInfo get(Object key)
    {
        return(ProvidentFundSlipEntryInfo)getObject(key);
    }
    public void set(int index, ProvidentFundSlipEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ProvidentFundSlipEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ProvidentFundSlipEntryInfo item)
    {
        return super.indexOf(item);
    }
}