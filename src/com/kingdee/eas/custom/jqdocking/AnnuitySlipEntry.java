package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.AnnuitySlipEntryController;
import com.kingdee.eas.hr.base.HRBillBaseEntry;

public class AnnuitySlipEntry extends HRBillBaseEntry implements IAnnuitySlipEntry
{
    public AnnuitySlipEntry()
    {
        super();
        registerInterface(IAnnuitySlipEntry.class, this);
    }
    public AnnuitySlipEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IAnnuitySlipEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("3B949D83");
    }
    private AnnuitySlipEntryController getController() throws BOSException
    {
        return (AnnuitySlipEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getAnnuitySlipEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getAnnuitySlipEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getAnnuitySlipEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection() throws BOSException
    {
        try {
            return getController().getAnnuitySlipEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getAnnuitySlipEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getAnnuitySlipEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}