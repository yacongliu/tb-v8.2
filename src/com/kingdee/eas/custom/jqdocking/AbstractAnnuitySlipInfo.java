package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractAnnuitySlipInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractAnnuitySlipInfo()
    {
        this("id");
    }
    protected AbstractAnnuitySlipInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryCollection());
    }
    /**
     * Object: ��𸶿 's ��¼ property 
     */
    public com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryCollection)get("entrys");
    }
    /**
     * Object: ��𸶿 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:��𸶿's ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:��𸶿's ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    /**
     * Object:��𸶿's �����·�property 
     */
    public String getReleaseMonth()
    {
        return getString("releaseMonth");
    }
    public void setReleaseMonth(String item)
    {
        setString("releaseMonth", item);
    }
    /**
     * Object:��𸶿's ��������property 
     */
    public int getAnnexNumber()
    {
        return getInt("annexNumber");
    }
    public void setAnnexNumber(int item)
    {
        setInt("annexNumber", item);
    }
    /**
     * Object:��𸶿's ��עproperty 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: ��𸶿 's ��Ӧ�� property 
     */
    public com.kingdee.eas.basedata.master.cssp.SupplierInfo getSupplier()
    {
        return (com.kingdee.eas.basedata.master.cssp.SupplierInfo)get("supplier");
    }
    public void setSupplier(com.kingdee.eas.basedata.master.cssp.SupplierInfo item)
    {
        put("supplier", item);
    }
    /**
     * Object: ��𸶿 's ���� property 
     */
    public com.kingdee.eas.basedata.assistant.BankInfo getBank()
    {
        return (com.kingdee.eas.basedata.assistant.BankInfo)get("bank");
    }
    public void setBank(com.kingdee.eas.basedata.assistant.BankInfo item)
    {
        put("bank", item);
    }
    /**
     * Object: ��𸶿 's �����˻� property 
     */
    public com.kingdee.eas.basedata.assistant.AccountBankInfo getBankAccount()
    {
        return (com.kingdee.eas.basedata.assistant.AccountBankInfo)get("bankAccount");
    }
    public void setBankAccount(com.kingdee.eas.basedata.assistant.AccountBankInfo item)
    {
        put("bankAccount", item);
    }
    /**
     * Object:��𸶿's ��������property 
     */
    public String getSendContent()
    {
        return getString("sendContent");
    }
    public void setSendContent(String item)
    {
        setString("sendContent", item);
    }
    /**
     * Object:��𸶿's ��������property 
     */
    public String getAcceptContent()
    {
        return getString("acceptContent");
    }
    public void setAcceptContent(String item)
    {
        setString("acceptContent", item);
    }
    /**
     * Object:��𸶿's ״̬property 
     */
    public com.kingdee.eas.custom.jqdocking.app.JQSlipState getState()
    {
        return com.kingdee.eas.custom.jqdocking.app.JQSlipState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.jqdocking.app.JQSlipState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    /**
     * Object:��𸶿's ��������property 
     */
    public com.kingdee.eas.custom.jqdocking.app.releaseType getReleaseType()
    {
        return com.kingdee.eas.custom.jqdocking.app.releaseType.getEnum(getString("releaseType"));
    }
    public void setReleaseType(com.kingdee.eas.custom.jqdocking.app.releaseType item)
    {
		if (item != null) {
        setString("releaseType", item.getValue());
		}
    }
    /**
     * Object:��𸶿's �����˻�property 
     */
    public String getBankAccountStr()
    {
        return getString("bankAccountStr");
    }
    public void setBankAccountStr(String item)
    {
        setString("bankAccountStr", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("AF27402F");
    }
}