package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractPaySlipInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractPaySlipInfo()
    {
        this("id");
    }
    protected AbstractPaySlipInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.jqdocking.PaySlipEntryCollection());
    }
    /**
     * Object: н�긶� 's ��¼ property 
     */
    public com.kingdee.eas.custom.jqdocking.PaySlipEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.jqdocking.PaySlipEntryCollection)get("entrys");
    }
    /**
     * Object: н�긶� 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:н�긶�'s ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:н�긶�'s ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    /**
     * Object:н�긶�'s �����·�property 
     */
    public String getReleaseMonth()
    {
        return getString("releaseMonth");
    }
    public void setReleaseMonth(String item)
    {
        setString("releaseMonth", item);
    }
    /**
     * Object:н�긶�'s ��������property 
     */
    public com.kingdee.eas.custom.jqdocking.app.releaseType getReleaseType()
    {
        return com.kingdee.eas.custom.jqdocking.app.releaseType.getEnum(getString("releaseType"));
    }
    public void setReleaseType(com.kingdee.eas.custom.jqdocking.app.releaseType item)
    {
		if (item != null) {
        setString("releaseType", item.getValue());
		}
    }
    /**
     * Object:н�긶�'s ��������property 
     */
    public int getAnnexNumber()
    {
        return getInt("annexNumber");
    }
    public void setAnnexNumber(int item)
    {
        setInt("annexNumber", item);
    }
    /**
     * Object:н�긶�'s ��עproperty 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object: н�긶� 's ��Ӧ�� property 
     */
    public com.kingdee.eas.basedata.master.cssp.SupplierInfo getSupplier()
    {
        return (com.kingdee.eas.basedata.master.cssp.SupplierInfo)get("supplier");
    }
    public void setSupplier(com.kingdee.eas.basedata.master.cssp.SupplierInfo item)
    {
        put("supplier", item);
    }
    /**
     * Object: н�긶� 's ���� property 
     */
    public com.kingdee.eas.basedata.assistant.BankInfo getBank()
    {
        return (com.kingdee.eas.basedata.assistant.BankInfo)get("bank");
    }
    public void setBank(com.kingdee.eas.basedata.assistant.BankInfo item)
    {
        put("bank", item);
    }
    /**
     * Object: н�긶� 's �����˻� property 
     */
    public com.kingdee.eas.basedata.assistant.AccountBankInfo getBankAccount()
    {
        return (com.kingdee.eas.basedata.assistant.AccountBankInfo)get("bankAccount");
    }
    public void setBankAccount(com.kingdee.eas.basedata.assistant.AccountBankInfo item)
    {
        put("bankAccount", item);
    }
    /**
     * Object:н�긶�'s ��������property 
     */
    public String getSendContent()
    {
        return getString("sendContent");
    }
    public void setSendContent(String item)
    {
        setString("sendContent", item);
    }
    /**
     * Object:н�긶�'s ��������property 
     */
    public String getAcceptContent()
    {
        return getString("acceptContent");
    }
    public void setAcceptContent(String item)
    {
        setString("acceptContent", item);
    }
    /**
     * Object:н�긶�'s ״̬property 
     */
    public com.kingdee.eas.custom.jqdocking.app.JQSlipState getState()
    {
        return com.kingdee.eas.custom.jqdocking.app.JQSlipState.getEnum(getString("state"));
    }
    public void setState(com.kingdee.eas.custom.jqdocking.app.JQSlipState item)
    {
		if (item != null) {
        setString("state", item.getValue());
		}
    }
    /**
     * Object:н�긶�'s �����˻�property 
     */
    public String getBankAccountStr()
    {
        return getString("bankAccountStr");
    }
    public void setBankAccountStr(String item)
    {
        setString("bankAccountStr", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("BF21B2DD");
    }
}