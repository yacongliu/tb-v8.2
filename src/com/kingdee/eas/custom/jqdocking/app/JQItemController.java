package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.JQItemCollection;
import com.kingdee.eas.custom.jqdocking.JQItemInfo;
import com.kingdee.eas.framework.app.DataBaseController;

public interface JQItemController extends DataBaseController
{
    public JQItemInfo getJQItemInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public JQItemInfo getJQItemInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public JQItemInfo getJQItemInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public JQItemCollection getJQItemCollection(Context ctx) throws BOSException, RemoteException;
    public JQItemCollection getJQItemCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public JQItemCollection getJQItemCollection(Context ctx, String oql) throws BOSException, RemoteException;
}