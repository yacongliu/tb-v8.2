/**
 * output package name
 */
package com.kingdee.eas.custom.jqdocking.app;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.kingdee.util.enums.StringEnum;

/**
 * output class name
 */
public class JQSlipState extends StringEnum
{
    public static final String NOTSYN_VALUE = "0";//alias=δͬ��
    public static final String SYSCED_VALUE = "1";//alias=��ͬ��
    public static final String REJECT_VALUE = "2";//alias=����

    public static final JQSlipState NOTSYN = new JQSlipState("NOTSYN", NOTSYN_VALUE);
    public static final JQSlipState SYSCED = new JQSlipState("SYSCED", SYSCED_VALUE);
    public static final JQSlipState REJECT = new JQSlipState("REJECT", REJECT_VALUE);

    /**
     * construct function
     * @param String jQSlipState
     */
    private JQSlipState(String name, String jQSlipState)
    {
        super(name, jQSlipState);
    }
    
    /**
     * getEnum function
     * @param String arguments
     */
    public static JQSlipState getEnum(String jQSlipState)
    {
        return (JQSlipState)getEnum(JQSlipState.class, jQSlipState);
    }

    /**
     * getEnumMap function
     */
    public static Map getEnumMap()
    {
        return getEnumMap(JQSlipState.class);
    }

    /**
     * getEnumList function
     */
    public static List getEnumList()
    {
         return getEnumList(JQSlipState.class);
    }
    
    /**
     * getIterator function
     */
    public static Iterator iterator()
    {
         return iterator(JQSlipState.class);
    }
}