package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.JQitemControlCollection;
import com.kingdee.eas.custom.jqdocking.JQitemControlInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseController;

public interface JQitemControlController extends HRBillBaseController
{
    public JQitemControlCollection getJQitemControlCollection(Context ctx) throws BOSException, RemoteException;
    public JQitemControlCollection getJQitemControlCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public JQitemControlCollection getJQitemControlCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public JQitemControlInfo getJQitemControlInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public JQitemControlInfo getJQitemControlInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public JQitemControlInfo getJQitemControlInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
}