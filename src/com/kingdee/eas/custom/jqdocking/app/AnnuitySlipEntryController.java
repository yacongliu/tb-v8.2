package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryCollection;
import com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;

public interface AnnuitySlipEntryController extends HRBillBaseEntryController
{
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(Context ctx) throws BOSException, RemoteException;
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}