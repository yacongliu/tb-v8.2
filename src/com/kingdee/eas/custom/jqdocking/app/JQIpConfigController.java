package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.JQIpConfigCollection;
import com.kingdee.eas.custom.jqdocking.JQIpConfigInfo;
import com.kingdee.eas.framework.app.DataBaseController;

public interface JQIpConfigController extends DataBaseController
{
    public JQIpConfigInfo getJQIpConfigInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public JQIpConfigInfo getJQIpConfigInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public JQIpConfigInfo getJQIpConfigInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public JQIpConfigCollection getJQIpConfigCollection(Context ctx) throws BOSException, RemoteException;
    public JQIpConfigCollection getJQIpConfigCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public JQIpConfigCollection getJQIpConfigCollection(Context ctx, String oql) throws BOSException, RemoteException;
}