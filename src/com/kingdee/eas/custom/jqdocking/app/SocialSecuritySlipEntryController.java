package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.SocialSecuritySlipEntryCollection;
import com.kingdee.eas.custom.jqdocking.SocialSecuritySlipEntryInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;

public interface SocialSecuritySlipEntryController extends HRBillBaseEntryController
{
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(Context ctx) throws BOSException, RemoteException;
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}