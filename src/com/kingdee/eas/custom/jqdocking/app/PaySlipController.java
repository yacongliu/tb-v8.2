package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.PaySlipCollection;
import com.kingdee.eas.custom.jqdocking.PaySlipInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseController;

public interface PaySlipController extends HRBillBaseController
{
    public PaySlipCollection getPaySlipCollection(Context ctx) throws BOSException, RemoteException;
    public PaySlipCollection getPaySlipCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public PaySlipCollection getPaySlipCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public PaySlipInfo getPaySlipInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public PaySlipInfo getPaySlipInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public PaySlipInfo getPaySlipInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
}