package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.ProvidentFundSlipCollection;
import com.kingdee.eas.custom.jqdocking.ProvidentFundSlipInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseController;

public interface ProvidentFundSlipController extends HRBillBaseController
{
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(Context ctx) throws BOSException, RemoteException;
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
}