/**
 * output package name
 */
package com.kingdee.eas.custom.jqdocking.app;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.kingdee.util.enums.StringEnum;

/**
 * output class name
 */
public class releaseType extends StringEnum
{
    public static final String YG_VALUE = "01";//alias=����Ա��н��
    public static final String LWPQ_VALUE = "02";//alias=����������ǲ��Աн��
    public static final String YDSB_VALUE = "03";//alias=��������籣

    public static final releaseType YG = new releaseType("YG", YG_VALUE);
    public static final releaseType LWPQ = new releaseType("LWPQ", LWPQ_VALUE);
    public static final releaseType YDSB = new releaseType("YDSB", YDSB_VALUE);

    /**
     * construct function
     * @param String releaseType
     */
    private releaseType(String name, String releaseType)
    {
        super(name, releaseType);
    }
    
    /**
     * getEnum function
     * @param String arguments
     */
    public static releaseType getEnum(String releaseType)
    {
        return (releaseType)getEnum(releaseType.class, releaseType);
    }

    /**
     * getEnumMap function
     */
    public static Map getEnumMap()
    {
        return getEnumMap(releaseType.class);
    }

    /**
     * getEnumList function
     */
    public static List getEnumList()
    {
         return getEnumList(releaseType.class);
    }
    
    /**
     * getIterator function
     */
    public static Iterator iterator()
    {
         return iterator(releaseType.class);
    }
}