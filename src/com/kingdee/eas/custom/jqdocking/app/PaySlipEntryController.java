package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.PaySlipEntryCollection;
import com.kingdee.eas.custom.jqdocking.PaySlipEntryInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;

public interface PaySlipEntryController extends HRBillBaseEntryController
{
    public PaySlipEntryInfo getPaySlipEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public PaySlipEntryInfo getPaySlipEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public PaySlipEntryInfo getPaySlipEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public PaySlipEntryCollection getPaySlipEntryCollection(Context ctx) throws BOSException, RemoteException;
    public PaySlipEntryCollection getPaySlipEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public PaySlipEntryCollection getPaySlipEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}