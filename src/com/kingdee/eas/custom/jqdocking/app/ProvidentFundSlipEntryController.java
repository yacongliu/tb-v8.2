package com.kingdee.eas.custom.jqdocking.app;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.ProvidentFundSlipEntryCollection;
import com.kingdee.eas.custom.jqdocking.ProvidentFundSlipEntryInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseEntryController;

public interface ProvidentFundSlipEntryController extends HRBillBaseEntryController
{
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(Context ctx) throws BOSException, RemoteException;
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(Context ctx, String oql) throws BOSException, RemoteException;
}