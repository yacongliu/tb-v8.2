package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBase;

public interface IProvidentFundSlip extends IHRBillBase
{
    public ProvidentFundSlipCollection getProvidentFundSlipCollection() throws BOSException;
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(EntityViewInfo view) throws BOSException;
    public ProvidentFundSlipCollection getProvidentFundSlipCollection(String oql) throws BOSException;
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ProvidentFundSlipInfo getProvidentFundSlipInfo(String oql) throws BOSException, EASBizException;
}