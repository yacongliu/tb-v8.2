package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractAnnuitySlipEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractAnnuitySlipEntryInfo()
    {
        this("id");
    }
    protected AbstractAnnuitySlipEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo getBill()
    {
        return (com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��¼'s ��λ��ҵ���property 
     */
    public java.math.BigDecimal getUnitAnnuity()
    {
        return getBigDecimal("unitAnnuity");
    }
    public void setUnitAnnuity(java.math.BigDecimal item)
    {
        setBigDecimal("unitAnnuity", item);
    }
    /**
     * Object:��¼'s ������ҵ���property 
     */
    public java.math.BigDecimal getPerAnnuity()
    {
        return getBigDecimal("perAnnuity");
    }
    public void setPerAnnuity(java.math.BigDecimal item)
    {
        setBigDecimal("perAnnuity", item);
    }
    /**
     * Object:��¼'s ��������property 
     */
    public com.kingdee.eas.custom.jqdocking.app.releaseType getReleaseType()
    {
        return com.kingdee.eas.custom.jqdocking.app.releaseType.getEnum(getString("releaseType"));
    }
    public void setReleaseType(com.kingdee.eas.custom.jqdocking.app.releaseType item)
    {
		if (item != null) {
        setString("releaseType", item.getValue());
		}
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("3B949D83");
    }
}