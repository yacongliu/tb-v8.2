package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class JQitemControlEntryCollection extends AbstractObjectCollection 
{
    public JQitemControlEntryCollection()
    {
        super(JQitemControlEntryInfo.class);
    }
    public boolean add(JQitemControlEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(JQitemControlEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(JQitemControlEntryInfo item)
    {
        return removeObject(item);
    }
    public JQitemControlEntryInfo get(int index)
    {
        return(JQitemControlEntryInfo)getObject(index);
    }
    public JQitemControlEntryInfo get(Object key)
    {
        return(JQitemControlEntryInfo)getObject(key);
    }
    public void set(int index, JQitemControlEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(JQitemControlEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(JQitemControlEntryInfo item)
    {
        return super.indexOf(item);
    }
}