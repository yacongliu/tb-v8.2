package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class JQIpConfigCollection extends AbstractObjectCollection 
{
    public JQIpConfigCollection()
    {
        super(JQIpConfigInfo.class);
    }
    public boolean add(JQIpConfigInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(JQIpConfigCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(JQIpConfigInfo item)
    {
        return removeObject(item);
    }
    public JQIpConfigInfo get(int index)
    {
        return(JQIpConfigInfo)getObject(index);
    }
    public JQIpConfigInfo get(Object key)
    {
        return(JQIpConfigInfo)getObject(key);
    }
    public void set(int index, JQIpConfigInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(JQIpConfigInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(JQIpConfigInfo item)
    {
        return super.indexOf(item);
    }
}