package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.JQIpConfigController;
import com.kingdee.eas.framework.DataBase;

public class JQIpConfig extends DataBase implements IJQIpConfig
{
    public JQIpConfig()
    {
        super();
        registerInterface(IJQIpConfig.class, this);
    }
    public JQIpConfig(Context ctx)
    {
        super(ctx);
        registerInterface(IJQIpConfig.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("72C8B89B");
    }
    private JQIpConfigController getController() throws BOSException
    {
        return (JQIpConfigController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public JQIpConfigInfo getJQIpConfigInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getJQIpConfigInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public JQIpConfigInfo getJQIpConfigInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getJQIpConfigInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public JQIpConfigInfo getJQIpConfigInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getJQIpConfigInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public JQIpConfigCollection getJQIpConfigCollection() throws BOSException
    {
        try {
            return getController().getJQIpConfigCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public JQIpConfigCollection getJQIpConfigCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getJQIpConfigCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public JQIpConfigCollection getJQIpConfigCollection(String oql) throws BOSException
    {
        try {
            return getController().getJQIpConfigCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}