package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

public class PaySlipEntryInfo extends AbstractPaySlipEntryInfo implements Serializable 
{
    public PaySlipEntryInfo()
    {
        super();
    }
    protected PaySlipEntryInfo(String pkField)
    {
        super(pkField);
    }
}