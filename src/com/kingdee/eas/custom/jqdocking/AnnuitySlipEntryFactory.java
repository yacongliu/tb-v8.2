package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class AnnuitySlipEntryFactory
{
    private AnnuitySlipEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("3B949D83") ,com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("3B949D83") ,com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("3B949D83"));
    }
    public static com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IAnnuitySlipEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("3B949D83"));
    }
}