package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.JQitemControlEntryController;
import com.kingdee.eas.hr.base.HRBillBaseEntry;

public class JQitemControlEntry extends HRBillBaseEntry implements IJQitemControlEntry
{
    public JQitemControlEntry()
    {
        super();
        registerInterface(IJQitemControlEntry.class, this);
    }
    public JQitemControlEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IJQitemControlEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("08998EFA");
    }
    private JQitemControlEntryController getController() throws BOSException
    {
        return (JQitemControlEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public JQitemControlEntryInfo getJQitemControlEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getJQitemControlEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public JQitemControlEntryInfo getJQitemControlEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getJQitemControlEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public JQitemControlEntryInfo getJQitemControlEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getJQitemControlEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public JQitemControlEntryCollection getJQitemControlEntryCollection() throws BOSException
    {
        try {
            return getController().getJQitemControlEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public JQitemControlEntryCollection getJQitemControlEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getJQitemControlEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public JQitemControlEntryCollection getJQitemControlEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getJQitemControlEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}