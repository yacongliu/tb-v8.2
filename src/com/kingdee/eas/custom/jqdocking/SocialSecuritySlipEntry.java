package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.SocialSecuritySlipEntryController;
import com.kingdee.eas.hr.base.HRBillBaseEntry;

public class SocialSecuritySlipEntry extends HRBillBaseEntry implements ISocialSecuritySlipEntry
{
    public SocialSecuritySlipEntry()
    {
        super();
        registerInterface(ISocialSecuritySlipEntry.class, this);
    }
    public SocialSecuritySlipEntry(Context ctx)
    {
        super(ctx);
        registerInterface(ISocialSecuritySlipEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("1CFD069A");
    }
    private SocialSecuritySlipEntryController getController() throws BOSException
    {
        return (SocialSecuritySlipEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getSocialSecuritySlipEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getSocialSecuritySlipEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public SocialSecuritySlipEntryInfo getSocialSecuritySlipEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getSocialSecuritySlipEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection() throws BOSException
    {
        try {
            return getController().getSocialSecuritySlipEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getSocialSecuritySlipEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public SocialSecuritySlipEntryCollection getSocialSecuritySlipEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getSocialSecuritySlipEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}