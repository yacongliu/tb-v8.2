package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class PaySlipEntryFactory
{
    private PaySlipEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IPaySlipEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlipEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C93CA795") ,com.kingdee.eas.custom.jqdocking.IPaySlipEntry.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IPaySlipEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlipEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C93CA795") ,com.kingdee.eas.custom.jqdocking.IPaySlipEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IPaySlipEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlipEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C93CA795"));
    }
    public static com.kingdee.eas.custom.jqdocking.IPaySlipEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlipEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C93CA795"));
    }
}