package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.IDataBase;

public interface IJQItem extends IDataBase
{
    public JQItemInfo getJQItemInfo(IObjectPK pk) throws BOSException, EASBizException;
    public JQItemInfo getJQItemInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public JQItemInfo getJQItemInfo(String oql) throws BOSException, EASBizException;
    public JQItemCollection getJQItemCollection() throws BOSException;
    public JQItemCollection getJQItemCollection(EntityViewInfo view) throws BOSException;
    public JQItemCollection getJQItemCollection(String oql) throws BOSException;
}