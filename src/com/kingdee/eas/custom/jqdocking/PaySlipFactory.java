package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class PaySlipFactory
{
    private PaySlipFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IPaySlip getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlip)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("BF21B2DD") ,com.kingdee.eas.custom.jqdocking.IPaySlip.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IPaySlip getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlip)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("BF21B2DD") ,com.kingdee.eas.custom.jqdocking.IPaySlip.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IPaySlip getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlip)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("BF21B2DD"));
    }
    public static com.kingdee.eas.custom.jqdocking.IPaySlip getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IPaySlip)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("BF21B2DD"));
    }
}