package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class ProvidentFundSlipEntryFactory
{
    private ProvidentFundSlipEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("EA42D371") ,com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("EA42D371") ,com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("EA42D371"));
    }
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlipEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("EA42D371"));
    }
}