package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class SocialSecuritySlipFactory
{
    private SocialSecuritySlipFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("5C2944B8") ,com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("5C2944B8") ,com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("5C2944B8"));
    }
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlip)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("5C2944B8"));
    }
}