/**
 * output package name
 */
package com.kingdee.eas.custom.jqdocking.client;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.KeyStroke;

import org.apache.log4j.Logger;

import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.ctrl.extendcontrols.BizDataFormat;
import com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox;
import com.kingdee.bos.ctrl.kdf.table.KDTDefaultCellEditor;
import com.kingdee.bos.ctrl.kdf.table.KDTable;
import com.kingdee.bos.ctrl.kdf.util.render.ObjectValueRender;
import com.kingdee.bos.ctrl.swing.KDComboBox;
import com.kingdee.bos.ctrl.swing.KDDatePicker;
import com.kingdee.bos.ctrl.swing.KDFormattedTextField;
import com.kingdee.bos.ctrl.swing.KDLayout;
import com.kingdee.bos.ctrl.swing.KDTextField;
import com.kingdee.bos.ctrl.swing.StringUtils;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.IObjectValue;
import com.kingdee.bos.metadata.IMetaDataPK;
import com.kingdee.bos.metadata.MetaDataPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.ui.face.CoreUIObject;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.bos.ui.face.ItemAction;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.util.enums.EnumUtils;


/**
 * output class name
 */
public abstract class AbstractAnnuitySlipEditUI extends com.kingdee.eas.hr.base.client.HRBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractAnnuitySlipEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplier;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAdminOrg;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplyDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBillState;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contreleaseMonth;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contannexNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contremark;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contsupplier;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contbank;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contsendContent;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contacceptContent;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contstate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contreleaseType;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contbankAccountStr;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtApplier;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAdminOrg;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker dpApplyDate;
    protected com.kingdee.bos.ctrl.swing.KDComboBox cbBillState;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtreleaseMonth;
    protected com.kingdee.bos.ctrl.swing.KDFormattedTextField txtannexNumber;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPaneremark;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtremark;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtsupplier;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtbank;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPanesendContent;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtsendContent;
    protected com.kingdee.bos.ctrl.swing.KDScrollPane scrollPaneacceptContent;
    protected com.kingdee.bos.ctrl.swing.KDTextArea txtacceptContent;
    protected com.kingdee.bos.ctrl.swing.KDComboBox state;
    protected com.kingdee.bos.ctrl.swing.KDComboBox releaseType;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtbankAccountStr;
    protected com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo editData = null;
    /**
     * output class constructor
     */
    public AbstractAnnuitySlipEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractAnnuitySlipEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setBindWorkFlow(true);
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionAudit
        actionAudit.setEnabled(true);
        actionAudit.setDaemonRun(false);

        actionAudit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl A"));
        _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
        actionAudit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
        actionAudit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionAudit.NAME");
        actionAudit.putValue(ItemAction.NAME, _tempStr);
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionUnaudit
        actionUnaudit.setEnabled(true);
        actionUnaudit.setDaemonRun(false);

        actionUnaudit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl U"));
        _tempStr = resHelper.getString("ActionUnaudit.SHORT_DESCRIPTION");
        actionUnaudit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionUnaudit.LONG_DESCRIPTION");
        actionUnaudit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionUnaudit.NAME");
        actionUnaudit.putValue(ItemAction.NAME, _tempStr);
         this.actionUnaudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kdtEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.contApplier = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAdminOrg = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contApplyDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBillState = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contreleaseMonth = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contannexNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contremark = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contsupplier = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contbank = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contsendContent = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contacceptContent = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contstate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contreleaseType = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contbankAccountStr = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtApplier = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAdminOrg = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.dpApplyDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.cbBillState = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.txtreleaseMonth = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtannexNumber = new com.kingdee.bos.ctrl.swing.KDFormattedTextField();
        this.scrollPaneremark = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtremark = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.prmtsupplier = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.prmtbank = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.scrollPanesendContent = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtsendContent = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.scrollPaneacceptContent = new com.kingdee.bos.ctrl.swing.KDScrollPane();
        this.txtacceptContent = new com.kingdee.bos.ctrl.swing.KDTextArea();
        this.state = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.releaseType = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.txtbankAccountStr = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.contNumber.setName("contNumber");
        this.contDescription.setName("contDescription");
        this.kdtEntrys.setName("kdtEntrys");
        this.contApplier.setName("contApplier");
        this.contAdminOrg.setName("contAdminOrg");
        this.contApplyDate.setName("contApplyDate");
        this.contBillState.setName("contBillState");
        this.contreleaseMonth.setName("contreleaseMonth");
        this.contannexNumber.setName("contannexNumber");
        this.contremark.setName("contremark");
        this.contsupplier.setName("contsupplier");
        this.contbank.setName("contbank");
        this.contsendContent.setName("contsendContent");
        this.contacceptContent.setName("contacceptContent");
        this.contstate.setName("contstate");
        this.contreleaseType.setName("contreleaseType");
        this.contbankAccountStr.setName("contbankAccountStr");
        this.txtNumber.setName("txtNumber");
        this.txtDescription.setName("txtDescription");
        this.txtApplier.setName("txtApplier");
        this.prmtAdminOrg.setName("prmtAdminOrg");
        this.dpApplyDate.setName("dpApplyDate");
        this.cbBillState.setName("cbBillState");
        this.txtreleaseMonth.setName("txtreleaseMonth");
        this.txtannexNumber.setName("txtannexNumber");
        this.scrollPaneremark.setName("scrollPaneremark");
        this.txtremark.setName("txtremark");
        this.prmtsupplier.setName("prmtsupplier");
        this.prmtbank.setName("prmtbank");
        this.scrollPanesendContent.setName("scrollPanesendContent");
        this.txtsendContent.setName("txtsendContent");
        this.scrollPaneacceptContent.setName("scrollPaneacceptContent");
        this.txtacceptContent.setName("txtacceptContent");
        this.state.setName("state");
        this.releaseType.setName("releaseType");
        this.txtbankAccountStr.setName("txtbankAccountStr");
        // CoreUI		
        this.btnPrint.setVisible(false);		
        this.btnPrintPreview.setVisible(false);		
        this.menuItemPrint.setVisible(false);		
        this.menuItemPrintPreview.setVisible(false);		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateFrom.setVisible(false);		
        this.btnAddLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateFrom.setVisible(false);		
        this.menuItemCopyFrom.setVisible(false);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);		
        this.contHROrg.setBoundLabelText(resHelper.getString("contHROrg.boundLabelText"));		
        this.contHROrg.setBoundLabelLength(90);		
        this.contHROrg.setBoundLabelUnderline(true);		
        this.contHROrg.setBoundLabelAlignment(7);		
        this.contHROrg.setVisible(true);		
        this.contApproveType.setBoundLabelText(resHelper.getString("contApproveType.boundLabelText"));		
        this.contApproveType.setBoundLabelLength(90);		
        this.contApproveType.setBoundLabelUnderline(true);		
        this.contApproveType.setBoundLabelAlignment(7);		
        this.contApproveType.setVisible(true);		
        this.prmtHROrg.setRequired(true);		
        this.prmtHROrg.setVisible(true);		
        this.prmtHROrg.setEnabled(true);		
        this.cbApproveType.setRequired(true);		
        this.cbApproveType.setVisible(true);		
        this.cbApproveType.setEnabled(true);
        this.btnAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.btnAudit.setText(resHelper.getString("btnAudit.text"));		
        this.btnAudit.setToolTipText(resHelper.getString("btnAudit.toolTipText"));		
        this.btnAudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_audit"));
        this.btnUnaudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnaudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.btnUnaudit.setText(resHelper.getString("btnUnaudit.text"));		
        this.btnUnaudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_unaudit"));		
        this.btnUnaudit.setToolTipText(resHelper.getString("btnUnaudit.toolTipText"));		
        this.btnUnaudit.setVisible(false);
        this.menuItemAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.menuItemAudit.setText(resHelper.getString("menuItemAudit.text"));		
        this.menuItemAudit.setToolTipText(resHelper.getString("menuItemAudit.toolTipText"));		
        this.menuItemAudit.setMnemonic(65);		
        this.menuItemAudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_audit"));
        this.menuItemUnaudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnaudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.menuItemUnaudit.setText(resHelper.getString("menuItemUnaudit.text"));		
        this.menuItemUnaudit.setToolTipText(resHelper.getString("menuItemUnaudit.toolTipText"));		
        this.menuItemUnaudit.setMnemonic(85);		
        this.menuItemUnaudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_unaudit"));		
        this.menuItemUnaudit.setVisible(false);
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(90);		
        this.contNumber.setBoundLabelUnderline(true);		
        this.contNumber.setBoundLabelAlignment(7);		
        this.contNumber.setVisible(true);
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(90);		
        this.contDescription.setBoundLabelUnderline(true);		
        this.contDescription.setBoundLabelAlignment(7);		
        this.contDescription.setVisible(true);
        // kdtEntrys
		String kdtEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol1\"><c:Protection locked=\"true\" hidden=\"true\" /></c:Style><c:Style id=\"sCol2\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol3\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol4\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol5\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol6\"><c:NumberFormat>yyyy-mm-dd</c:NumberFormat></c:Style><c:Style id=\"sCol8\"><c:NumberFormat>&amp;double</c:NumberFormat></c:Style><c:Style id=\"sCol9\"><c:NumberFormat>&amp;double</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"person\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" t:styleID=\"sCol1\" /><t:Column t:key=\"empNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" t:styleID=\"sCol2\" /><t:Column t:key=\"empName\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" t:styleID=\"sCol3\" /><t:Column t:key=\"position\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"9\" t:styleID=\"sCol4\" /><t:Column t:key=\"adminOrg\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"10\" t:styleID=\"sCol5\" /><t:Column t:key=\"bizDate\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"true\" t:index=\"14\" t:styleID=\"sCol6\" /><t:Column t:key=\"description\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"15\" /><t:Column t:key=\"unitAnnuity\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol8\" /><t:Column t:key=\"perAnnuity\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol9\" /><t:Column t:key=\"releaseType\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{person}</t:Cell><t:Cell>$Resource{empNumber}</t:Cell><t:Cell>$Resource{empName}</t:Cell><t:Cell>$Resource{position}</t:Cell><t:Cell>$Resource{adminOrg}</t:Cell><t:Cell>$Resource{bizDate}</t:Cell><t:Cell>$Resource{description}</t:Cell><t:Cell>$Resource{unitAnnuity}</t:Cell><t:Cell>$Resource{perAnnuity}</t:Cell><t:Cell>$Resource{releaseType}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtEntrys.setFormatXml(resHelper.translateString("kdtEntrys",kdtEntrysStrXML));
        this.kdtEntrys.addKDTEditListener(new com.kingdee.bos.ctrl.kdf.table.event.KDTEditAdapter() {
            public void editStopping(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtEntrys_editStopping(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
            public void editStopped(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtEntrys_editStopped(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
        });

                this.kdtEntrys.putBindContents("editData",new String[] {"id","person","person.number","person.name","position","adminOrg","bizDate","description","unitAnnuity","perAnnuity","releaseType"});


        this.kdtEntrys.checkParsed();
        final KDBizPromptBox kdtEntrys_person_PromptBox = new KDBizPromptBox();
        kdtEntrys_person_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtEntrys_person_PromptBox.setVisible(true);
        kdtEntrys_person_PromptBox.setEditable(true);
        kdtEntrys_person_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_person_PromptBox.setEditFormat("$number$");
        kdtEntrys_person_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_person_CellEditor = new KDTDefaultCellEditor(kdtEntrys_person_PromptBox);
        this.kdtEntrys.getColumn("person").setEditor(kdtEntrys_person_CellEditor);
        ObjectValueRender kdtEntrys_person_OVR = new ObjectValueRender();
        kdtEntrys_person_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("person").setRenderer(kdtEntrys_person_OVR);
        KDTextField kdtEntrys_empNumber_TextField = new KDTextField();
        kdtEntrys_empNumber_TextField.setName("kdtEntrys_empNumber_TextField");
        kdtEntrys_empNumber_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_empNumber_CellEditor = new KDTDefaultCellEditor(kdtEntrys_empNumber_TextField);
        this.kdtEntrys.getColumn("empNumber").setEditor(kdtEntrys_empNumber_CellEditor);
        KDTextField kdtEntrys_empName_TextField = new KDTextField();
        kdtEntrys_empName_TextField.setName("kdtEntrys_empName_TextField");
        kdtEntrys_empName_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_empName_CellEditor = new KDTDefaultCellEditor(kdtEntrys_empName_TextField);
        this.kdtEntrys.getColumn("empName").setEditor(kdtEntrys_empName_CellEditor);
        final KDBizPromptBox kdtEntrys_position_PromptBox = new KDBizPromptBox();
        kdtEntrys_position_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.PositionQuery");
        kdtEntrys_position_PromptBox.setVisible(true);
        kdtEntrys_position_PromptBox.setEditable(true);
        kdtEntrys_position_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_position_PromptBox.setEditFormat("$number$");
        kdtEntrys_position_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_position_CellEditor = new KDTDefaultCellEditor(kdtEntrys_position_PromptBox);
        this.kdtEntrys.getColumn("position").setEditor(kdtEntrys_position_CellEditor);
        ObjectValueRender kdtEntrys_position_OVR = new ObjectValueRender();
        kdtEntrys_position_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("position").setRenderer(kdtEntrys_position_OVR);
        final KDBizPromptBox kdtEntrys_adminOrg_PromptBox = new KDBizPromptBox();
        kdtEntrys_adminOrg_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");
        kdtEntrys_adminOrg_PromptBox.setVisible(true);
        kdtEntrys_adminOrg_PromptBox.setEditable(true);
        kdtEntrys_adminOrg_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_adminOrg_PromptBox.setEditFormat("$number$");
        kdtEntrys_adminOrg_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_adminOrg_CellEditor = new KDTDefaultCellEditor(kdtEntrys_adminOrg_PromptBox);
        this.kdtEntrys.getColumn("adminOrg").setEditor(kdtEntrys_adminOrg_CellEditor);
        ObjectValueRender kdtEntrys_adminOrg_OVR = new ObjectValueRender();
        kdtEntrys_adminOrg_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("adminOrg").setRenderer(kdtEntrys_adminOrg_OVR);
        KDDatePicker kdtEntrys_bizDate_DatePicker = new KDDatePicker();
        kdtEntrys_bizDate_DatePicker.setName("kdtEntrys_bizDate_DatePicker");
        kdtEntrys_bizDate_DatePicker.setVisible(true);
        kdtEntrys_bizDate_DatePicker.setEditable(true);
        KDTDefaultCellEditor kdtEntrys_bizDate_CellEditor = new KDTDefaultCellEditor(kdtEntrys_bizDate_DatePicker);
        this.kdtEntrys.getColumn("bizDate").setEditor(kdtEntrys_bizDate_CellEditor);
        KDTextField kdtEntrys_description_TextField = new KDTextField();
        kdtEntrys_description_TextField.setName("kdtEntrys_description_TextField");
        kdtEntrys_description_TextField.setMaxLength(200);
        KDTDefaultCellEditor kdtEntrys_description_CellEditor = new KDTDefaultCellEditor(kdtEntrys_description_TextField);
        this.kdtEntrys.getColumn("description").setEditor(kdtEntrys_description_CellEditor);
        KDFormattedTextField kdtEntrys_unitAnnuity_TextField = new KDFormattedTextField();
        kdtEntrys_unitAnnuity_TextField.setName("kdtEntrys_unitAnnuity_TextField");
        kdtEntrys_unitAnnuity_TextField.setVisible(true);
        kdtEntrys_unitAnnuity_TextField.setEditable(true);
        kdtEntrys_unitAnnuity_TextField.setHorizontalAlignment(2);
        kdtEntrys_unitAnnuity_TextField.setDataType(1);
        	kdtEntrys_unitAnnuity_TextField.setMinimumValue(new java.math.BigDecimal("-1.0E18"));
        	kdtEntrys_unitAnnuity_TextField.setMaximumValue(new java.math.BigDecimal("1.0E18"));
        kdtEntrys_unitAnnuity_TextField.setPrecision(10);
        KDTDefaultCellEditor kdtEntrys_unitAnnuity_CellEditor = new KDTDefaultCellEditor(kdtEntrys_unitAnnuity_TextField);
        this.kdtEntrys.getColumn("unitAnnuity").setEditor(kdtEntrys_unitAnnuity_CellEditor);
        KDFormattedTextField kdtEntrys_perAnnuity_TextField = new KDFormattedTextField();
        kdtEntrys_perAnnuity_TextField.setName("kdtEntrys_perAnnuity_TextField");
        kdtEntrys_perAnnuity_TextField.setVisible(true);
        kdtEntrys_perAnnuity_TextField.setEditable(true);
        kdtEntrys_perAnnuity_TextField.setHorizontalAlignment(2);
        kdtEntrys_perAnnuity_TextField.setDataType(1);
        	kdtEntrys_perAnnuity_TextField.setMinimumValue(new java.math.BigDecimal("-1.0E18"));
        	kdtEntrys_perAnnuity_TextField.setMaximumValue(new java.math.BigDecimal("1.0E18"));
        kdtEntrys_perAnnuity_TextField.setPrecision(10);
        KDTDefaultCellEditor kdtEntrys_perAnnuity_CellEditor = new KDTDefaultCellEditor(kdtEntrys_perAnnuity_TextField);
        this.kdtEntrys.getColumn("perAnnuity").setEditor(kdtEntrys_perAnnuity_CellEditor);
        KDComboBox kdtEntrys_releaseType_ComboBox = new KDComboBox();
        kdtEntrys_releaseType_ComboBox.setName("kdtEntrys_releaseType_ComboBox");
        kdtEntrys_releaseType_ComboBox.setVisible(true);
        kdtEntrys_releaseType_ComboBox.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.jqdocking.app.releaseType").toArray());
        KDTDefaultCellEditor kdtEntrys_releaseType_CellEditor = new KDTDefaultCellEditor(kdtEntrys_releaseType_ComboBox);
        this.kdtEntrys.getColumn("releaseType").setEditor(kdtEntrys_releaseType_CellEditor);
        // contApplier		
        this.contApplier.setBoundLabelText(resHelper.getString("contApplier.boundLabelText"));		
        this.contApplier.setBoundLabelLength(90);		
        this.contApplier.setBoundLabelUnderline(true);		
        this.contApplier.setBoundLabelAlignment(7);		
        this.contApplier.setVisible(true);
        // contAdminOrg		
        this.contAdminOrg.setBoundLabelText(resHelper.getString("contAdminOrg.boundLabelText"));		
        this.contAdminOrg.setBoundLabelLength(90);		
        this.contAdminOrg.setBoundLabelUnderline(true);		
        this.contAdminOrg.setBoundLabelAlignment(7);		
        this.contAdminOrg.setVisible(true);
        // contApplyDate		
        this.contApplyDate.setBoundLabelText(resHelper.getString("contApplyDate.boundLabelText"));		
        this.contApplyDate.setBoundLabelLength(90);		
        this.contApplyDate.setBoundLabelUnderline(true);		
        this.contApplyDate.setBoundLabelAlignment(7);		
        this.contApplyDate.setVisible(true);
        // contBillState		
        this.contBillState.setBoundLabelText(resHelper.getString("contBillState.boundLabelText"));		
        this.contBillState.setBoundLabelLength(90);		
        this.contBillState.setBoundLabelUnderline(true);		
        this.contBillState.setBoundLabelAlignment(7);		
        this.contBillState.setVisible(false);
        // contreleaseMonth		
        this.contreleaseMonth.setBoundLabelText(resHelper.getString("contreleaseMonth.boundLabelText"));		
        this.contreleaseMonth.setBoundLabelLength(100);		
        this.contreleaseMonth.setBoundLabelUnderline(true);		
        this.contreleaseMonth.setVisible(true);
        // contannexNumber		
        this.contannexNumber.setBoundLabelText(resHelper.getString("contannexNumber.boundLabelText"));		
        this.contannexNumber.setBoundLabelLength(100);		
        this.contannexNumber.setBoundLabelUnderline(true);		
        this.contannexNumber.setVisible(true);
        // contremark		
        this.contremark.setBoundLabelText(resHelper.getString("contremark.boundLabelText"));		
        this.contremark.setBoundLabelLength(100);		
        this.contremark.setBoundLabelUnderline(true);		
        this.contremark.setVisible(true);
        // contsupplier		
        this.contsupplier.setBoundLabelText(resHelper.getString("contsupplier.boundLabelText"));		
        this.contsupplier.setBoundLabelLength(100);		
        this.contsupplier.setBoundLabelUnderline(true);		
        this.contsupplier.setVisible(true);
        // contbank		
        this.contbank.setBoundLabelText(resHelper.getString("contbank.boundLabelText"));		
        this.contbank.setBoundLabelLength(100);		
        this.contbank.setBoundLabelUnderline(true);		
        this.contbank.setVisible(true);
        // contsendContent		
        this.contsendContent.setBoundLabelText(resHelper.getString("contsendContent.boundLabelText"));		
        this.contsendContent.setBoundLabelLength(100);		
        this.contsendContent.setBoundLabelUnderline(true);		
        this.contsendContent.setVisible(true);
        // contacceptContent		
        this.contacceptContent.setBoundLabelText(resHelper.getString("contacceptContent.boundLabelText"));		
        this.contacceptContent.setBoundLabelLength(100);		
        this.contacceptContent.setBoundLabelUnderline(true);		
        this.contacceptContent.setVisible(true);
        // contstate		
        this.contstate.setBoundLabelText(resHelper.getString("contstate.boundLabelText"));		
        this.contstate.setBoundLabelLength(100);		
        this.contstate.setBoundLabelUnderline(true);		
        this.contstate.setVisible(true);
        // contreleaseType		
        this.contreleaseType.setBoundLabelText(resHelper.getString("contreleaseType.boundLabelText"));		
        this.contreleaseType.setBoundLabelLength(100);		
        this.contreleaseType.setBoundLabelUnderline(true);		
        this.contreleaseType.setVisible(true);
        // contbankAccountStr		
        this.contbankAccountStr.setBoundLabelText(resHelper.getString("contbankAccountStr.boundLabelText"));		
        this.contbankAccountStr.setBoundLabelLength(100);		
        this.contbankAccountStr.setBoundLabelUnderline(true);		
        this.contbankAccountStr.setVisible(true);
        // txtNumber		
        this.txtNumber.setMaxLength(80);		
        this.txtNumber.setRequired(true);		
        this.txtNumber.setVisible(true);		
        this.txtNumber.setEnabled(true);		
        this.txtNumber.setHorizontalAlignment(2);
        // txtDescription		
        this.txtDescription.setMaxLength(200);		
        this.txtDescription.setVisible(true);		
        this.txtDescription.setEnabled(true);		
        this.txtDescription.setHorizontalAlignment(2);		
        this.txtDescription.setRequired(false);
        // txtApplier		
        this.txtApplier.setEnabled(false);		
        this.txtApplier.setMaxLength(80);		
        this.txtApplier.setVisible(true);		
        this.txtApplier.setHorizontalAlignment(2);		
        this.txtApplier.setRequired(false);
        // prmtAdminOrg		
        this.prmtAdminOrg.setRequired(true);		
        this.prmtAdminOrg.setVisible(true);		
        this.prmtAdminOrg.setEnabled(true);
        // dpApplyDate		
        this.dpApplyDate.setEnabled(false);		
        this.dpApplyDate.setVisible(true);		
        this.dpApplyDate.setRequired(false);
        // cbBillState		
        this.cbBillState.addItems(EnumUtils.getEnumList("com.kingdee.eas.hr.base.HRBillStateEnum").toArray());		
        this.cbBillState.setEnabled(false);		
        this.cbBillState.setVisible(false);		
        this.cbBillState.setRequired(false);
        // txtreleaseMonth		
        this.txtreleaseMonth.setVisible(true);		
        this.txtreleaseMonth.setHorizontalAlignment(2);		
        this.txtreleaseMonth.setMaxLength(100);		
        this.txtreleaseMonth.setRequired(false);
        // txtannexNumber		
        this.txtannexNumber.setVisible(true);		
        this.txtannexNumber.setHorizontalAlignment(2);		
        this.txtannexNumber.setDataType(0);		
        this.txtannexNumber.setSupportedEmpty(true);		
        this.txtannexNumber.setRequired(false);
        // scrollPaneremark
        // txtremark		
        this.txtremark.setVisible(true);		
        this.txtremark.setRequired(false);		
        this.txtremark.setMaxLength(255);
        // prmtsupplier		
        this.prmtsupplier.setQueryInfo("com.kingdee.eas.basedata.master.cssp.app.PSupplierQuery");		
        this.prmtsupplier.setVisible(true);		
        this.prmtsupplier.setEditable(true);		
        this.prmtsupplier.setDisplayFormat("$name$");		
        this.prmtsupplier.setEditFormat("$number$");		
        this.prmtsupplier.setCommitFormat("$number$");		
        this.prmtsupplier.setRequired(false);
        // prmtbank		
        this.prmtbank.setQueryInfo("com.kingdee.eas.basedata.assistant.app.BankQuery");		
        this.prmtbank.setVisible(true);		
        this.prmtbank.setEditable(true);		
        this.prmtbank.setDisplayFormat("$name$");		
        this.prmtbank.setEditFormat("$number$");		
        this.prmtbank.setCommitFormat("$number$");		
        this.prmtbank.setRequired(false);
        // scrollPanesendContent
        // txtsendContent		
        this.txtsendContent.setVisible(true);		
        this.txtsendContent.setRequired(false);		
        this.txtsendContent.setMaxLength(255);
        // scrollPaneacceptContent
        // txtacceptContent		
        this.txtacceptContent.setVisible(true);		
        this.txtacceptContent.setRequired(false);		
        this.txtacceptContent.setMaxLength(255);
        // state		
        this.state.setVisible(true);		
        this.state.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.jqdocking.app.JQSlipState").toArray());		
        this.state.setRequired(false);
        // releaseType		
        this.releaseType.setVisible(true);		
        this.releaseType.addItems(EnumUtils.getEnumList("com.kingdee.eas.custom.jqdocking.app.releaseType").toArray());		
        this.releaseType.setRequired(false);
        // txtbankAccountStr		
        this.txtbankAccountStr.setVisible(true);		
        this.txtbankAccountStr.setHorizontalAlignment(2);		
        this.txtbankAccountStr.setMaxLength(255);		
        this.txtbankAccountStr.setRequired(false);
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {txtApplier,txtDescription,txtNumber,cbBillState,prmtAdminOrg,dpApplyDate,kdtEntrys,txtreleaseMonth,txtannexNumber,txtremark,prmtsupplier,cbApproveType,prmtHROrg,prmtbank,txtsendContent,txtacceptContent,state,releaseType,txtbankAccountStr}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 1249));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 1249));
        contHROrg.setBounds(new Rectangle(672, 58, 270, 19));
        this.add(contHROrg, new KDLayout.Constraints(672, 58, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApproveType.setBounds(new Rectangle(672, 106, 270, 19));
        this.add(contApproveType, new KDLayout.Constraints(672, 106, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contNumber.setBounds(new Rectangle(672, 82, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(672, 82, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contDescription.setBounds(new Rectangle(341, 130, 270, 19));
        this.add(contDescription, new KDLayout.Constraints(341, 130, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        kdtEntrys.setBounds(new Rectangle(10, 154, 993, 543));
        kdtEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtEntrys,new com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryInfo(),null,false);
        this.add(kdtEntrys_detailPanel, new KDLayout.Constraints(10, 154, 993, 543, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
		kdtEntrys_detailPanel.addAddListener(new com.kingdee.eas.framework.client.multiDetail.IDetailPanelListener() {
			public void beforeEvent(com.kingdee.eas.framework.client.multiDetail.DetailPanelEvent event) throws Exception {
				IObjectValue vo = event.getObjectValue();
vo.put("releaseType","01");
			}
			public void afterEvent(com.kingdee.eas.framework.client.multiDetail.DetailPanelEvent event) throws Exception {
			}
		});
        contApplier.setBounds(new Rectangle(10, 130, 270, 19));
        this.add(contApplier, new KDLayout.Constraints(10, 130, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contAdminOrg.setBounds(new Rectangle(10, 106, 270, 19));
        this.add(contAdminOrg, new KDLayout.Constraints(10, 106, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApplyDate.setBounds(new Rectangle(341, 106, 270, 19));
        this.add(contApplyDate, new KDLayout.Constraints(341, 106, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contBillState.setBounds(new Rectangle(341, 82, 270, 19));
        this.add(contBillState, new KDLayout.Constraints(341, 82, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contreleaseMonth.setBounds(new Rectangle(341, 10, 270, 19));
        this.add(contreleaseMonth, new KDLayout.Constraints(341, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contannexNumber.setBounds(new Rectangle(672, 10, 270, 19));
        this.add(contannexNumber, new KDLayout.Constraints(672, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contremark.setBounds(new Rectangle(10, 34, 270, 19));
        this.add(contremark, new KDLayout.Constraints(10, 34, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contsupplier.setBounds(new Rectangle(341, 34, 270, 19));
        this.add(contsupplier, new KDLayout.Constraints(341, 34, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contbank.setBounds(new Rectangle(672, 34, 270, 19));
        this.add(contbank, new KDLayout.Constraints(672, 34, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contsendContent.setBounds(new Rectangle(10, 58, 270, 19));
        this.add(contsendContent, new KDLayout.Constraints(10, 58, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contacceptContent.setBounds(new Rectangle(341, 58, 270, 19));
        this.add(contacceptContent, new KDLayout.Constraints(341, 58, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contstate.setBounds(new Rectangle(10, 82, 270, 19));
        this.add(contstate, new KDLayout.Constraints(10, 82, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contreleaseType.setBounds(new Rectangle(672, 130, 270, 19));
        this.add(contreleaseType, new KDLayout.Constraints(672, 130, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contbankAccountStr.setBounds(new Rectangle(10, 10, 270, 19));
        this.add(contbankAccountStr, new KDLayout.Constraints(10, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        //contHROrg
        contHROrg.setBoundEditor(prmtHROrg);
        //contApproveType
        contApproveType.setBoundEditor(cbApproveType);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contApplier
        contApplier.setBoundEditor(txtApplier);
        //contAdminOrg
        contAdminOrg.setBoundEditor(prmtAdminOrg);
        //contApplyDate
        contApplyDate.setBoundEditor(dpApplyDate);
        //contBillState
        contBillState.setBoundEditor(cbBillState);
        //contreleaseMonth
        contreleaseMonth.setBoundEditor(txtreleaseMonth);
        //contannexNumber
        contannexNumber.setBoundEditor(txtannexNumber);
        //contremark
        contremark.setBoundEditor(scrollPaneremark);
        //scrollPaneremark
        scrollPaneremark.getViewport().add(txtremark, null);
        //contsupplier
        contsupplier.setBoundEditor(prmtsupplier);
        //contbank
        contbank.setBoundEditor(prmtbank);
        //contsendContent
        contsendContent.setBoundEditor(scrollPanesendContent);
        //scrollPanesendContent
        scrollPanesendContent.getViewport().add(txtsendContent, null);
        //contacceptContent
        contacceptContent.setBoundEditor(scrollPaneacceptContent);
        //scrollPaneacceptContent
        scrollPaneacceptContent.getViewport().add(txtacceptContent, null);
        //contstate
        contstate.setBoundEditor(state);
        //contreleaseType
        contreleaseType.setBoundEditor(releaseType);
        //contbankAccountStr
        contbankAccountStr.setBoundEditor(txtbankAccountStr);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(menuItemMapping);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemSendMail);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(menuItemColumnCopyAll);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(menuItemColumnCopySelect);
        menuEdit.add(separatorEdit1);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(menuItemAudit);
        menuBiz.add(menuItemUnaudit);
        menuBiz.add(menuItemPerson);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnColumnCopyAll);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnColumnCopySelect);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);
        this.toolBar.add(btnAudit);
        this.toolBar.add(btnUnaudit);
        this.toolBar.add(btnPerson);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("hrOrgUnit", com.kingdee.eas.basedata.org.HROrgUnitInfo.class, this.prmtHROrg, "data");
		dataBinder.registerBinding("approveType", com.kingdee.eas.hr.base.ApproveTypeEnum.class, this.cbApproveType, "selectedItem");
		dataBinder.registerBinding("entrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtEntrys, "id.text");
		dataBinder.registerBinding("entrys", com.kingdee.eas.custom.jqdocking.AnnuitySlipEntryInfo.class, this.kdtEntrys, "userObject");
		dataBinder.registerBinding("entrys.bizDate", java.util.Date.class, this.kdtEntrys, "bizDate.text");
		dataBinder.registerBinding("entrys.description", String.class, this.kdtEntrys, "description.text");
		dataBinder.registerBinding("entrys.person", com.kingdee.eas.basedata.person.PersonInfo.class, this.kdtEntrys, "person.text");
		dataBinder.registerBinding("entrys.person.number", String.class, this.kdtEntrys, "empNumber.text");
		dataBinder.registerBinding("entrys.person.name", String.class, this.kdtEntrys, "empName.text");
		dataBinder.registerBinding("entrys.position", com.kingdee.eas.basedata.org.PositionInfo.class, this.kdtEntrys, "position.text");
		dataBinder.registerBinding("entrys.adminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.kdtEntrys, "adminOrg.text");
		dataBinder.registerBinding("entrys.unitAnnuity", java.math.BigDecimal.class, this.kdtEntrys, "unitAnnuity.text");
		dataBinder.registerBinding("entrys.perAnnuity", java.math.BigDecimal.class, this.kdtEntrys, "perAnnuity.text");
		dataBinder.registerBinding("entrys.releaseType", com.kingdee.util.enums.Enum.class, this.kdtEntrys, "releaseType.text");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("applier.name", String.class, this.txtApplier, "text");
		dataBinder.registerBinding("adminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtAdminOrg, "data");
		dataBinder.registerBinding("applyDate", java.util.Date.class, this.dpApplyDate, "value");
		dataBinder.registerBinding("billState", com.kingdee.eas.hr.base.HRBillStateEnum.class, this.cbBillState, "selectedItem");
		dataBinder.registerBinding("releaseMonth", String.class, this.txtreleaseMonth, "text");
		dataBinder.registerBinding("annexNumber", int.class, this.txtannexNumber, "value");
		dataBinder.registerBinding("remark", String.class, this.txtremark, "text");
		dataBinder.registerBinding("supplier", com.kingdee.eas.basedata.master.cssp.SupplierInfo.class, this.prmtsupplier, "data");
		dataBinder.registerBinding("bank", com.kingdee.eas.basedata.assistant.BankInfo.class, this.prmtbank, "data");
		dataBinder.registerBinding("sendContent", String.class, this.txtsendContent, "text");
		dataBinder.registerBinding("acceptContent", String.class, this.txtacceptContent, "text");
		dataBinder.registerBinding("state", com.kingdee.eas.custom.jqdocking.app.JQSlipState.class, this.state, "selectedItem");
		dataBinder.registerBinding("releaseType", com.kingdee.eas.custom.jqdocking.app.releaseType.class, this.releaseType, "selectedItem");
		dataBinder.registerBinding("bankAccountStr", String.class, this.txtbankAccountStr, "text");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.jqdocking.app.AnnuitySlipEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.txtApplier.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"NONE",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("NONE");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("hrOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("approveType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person.number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person.name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.position", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.adminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.unitAnnuity", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.perAnnuity", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.releaseType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("applier.name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("applyDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("billState", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("releaseMonth", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("annexNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("remark", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("supplier", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("bank", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("sendContent", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("acceptContent", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("state", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("releaseType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("bankAccountStr", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }

    /**
     * output kdtEntrys_editStopping method
     */
    protected void kdtEntrys_editStopping(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output kdtEntrys_editStopped method
     */
    protected void kdtEntrys_editStopped(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("hrOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("hrOrgUnit.id"));
        	sic.add(new SelectorItemInfo("hrOrgUnit.number"));
        	sic.add(new SelectorItemInfo("hrOrgUnit.name"));
		}
        sic.add(new SelectorItemInfo("approveType"));
    	sic.add(new SelectorItemInfo("entrys.id"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.*"));
		}
		else{
		}
    	sic.add(new SelectorItemInfo("entrys.bizDate"));
    	sic.add(new SelectorItemInfo("entrys.description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.person.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.person.id"));
			sic.add(new SelectorItemInfo("entrys.person.name"));
        	sic.add(new SelectorItemInfo("entrys.person.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.position.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.position.id"));
			sic.add(new SelectorItemInfo("entrys.position.name"));
        	sic.add(new SelectorItemInfo("entrys.position.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.adminOrg.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.adminOrg.id"));
			sic.add(new SelectorItemInfo("entrys.adminOrg.name"));
        	sic.add(new SelectorItemInfo("entrys.adminOrg.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.unitAnnuity"));
    	sic.add(new SelectorItemInfo("entrys.perAnnuity"));
    	sic.add(new SelectorItemInfo("entrys.releaseType"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("description"));
        sic.add(new SelectorItemInfo("applier.name"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrg.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrg.id"));
        	sic.add(new SelectorItemInfo("adminOrg.number"));
        	sic.add(new SelectorItemInfo("adminOrg.name"));
		}
        sic.add(new SelectorItemInfo("applyDate"));
        sic.add(new SelectorItemInfo("billState"));
        sic.add(new SelectorItemInfo("releaseMonth"));
        sic.add(new SelectorItemInfo("annexNumber"));
        sic.add(new SelectorItemInfo("remark"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("supplier.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("supplier.id"));
        	sic.add(new SelectorItemInfo("supplier.number"));
        	sic.add(new SelectorItemInfo("supplier.name"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("bank.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("bank.id"));
        	sic.add(new SelectorItemInfo("bank.number"));
        	sic.add(new SelectorItemInfo("bank.name"));
		}
        sic.add(new SelectorItemInfo("sendContent"));
        sic.add(new SelectorItemInfo("acceptContent"));
        sic.add(new SelectorItemInfo("state"));
        sic.add(new SelectorItemInfo("releaseType"));
        sic.add(new SelectorItemInfo("bankAccountStr"));
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
    	if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.print(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
        if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.printPreview(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionAudit_actionPerformed(e);
    }
    	

    /**
     * output actionUnaudit_actionPerformed method
     */
    public void actionUnaudit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionUnaudit_actionPerformed(e);
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionAudit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnaudit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionUnaudit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnaudit() {
    	return false;
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.jqdocking.client", "AnnuitySlipEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.jqdocking.client.AnnuitySlipEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.jqdocking.AnnuitySlipFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo objectValue = new com.kingdee.eas.custom.jqdocking.AnnuitySlipInfo();
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/jqdocking/AnnuitySlip";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.jqdocking.app.AnnuitySlipQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtEntrys;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
		vo.put("state","0");
vo.put("releaseType","01");
        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}