package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;

public interface IProvidentFundSlipEntry extends IHRBillBaseEntry
{
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(String oql) throws BOSException, EASBizException;
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection() throws BOSException;
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(EntityViewInfo view) throws BOSException;
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(String oql) throws BOSException;
}