package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.IDataBase;

public interface IJQIpConfig extends IDataBase
{
    public JQIpConfigInfo getJQIpConfigInfo(IObjectPK pk) throws BOSException, EASBizException;
    public JQIpConfigInfo getJQIpConfigInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public JQIpConfigInfo getJQIpConfigInfo(String oql) throws BOSException, EASBizException;
    public JQIpConfigCollection getJQIpConfigCollection() throws BOSException;
    public JQIpConfigCollection getJQIpConfigCollection(EntityViewInfo view) throws BOSException;
    public JQIpConfigCollection getJQIpConfigCollection(String oql) throws BOSException;
}