package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.SocialSecuritySlipController;
import com.kingdee.eas.hr.base.HRBillBase;

public class SocialSecuritySlip extends HRBillBase implements ISocialSecuritySlip
{
    public SocialSecuritySlip()
    {
        super();
        registerInterface(ISocialSecuritySlip.class, this);
    }
    public SocialSecuritySlip(Context ctx)
    {
        super(ctx);
        registerInterface(ISocialSecuritySlip.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("5C2944B8");
    }
    private SocialSecuritySlipController getController() throws BOSException
    {
        return (SocialSecuritySlipController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public SocialSecuritySlipCollection getSocialSecuritySlipCollection() throws BOSException
    {
        try {
            return getController().getSocialSecuritySlipCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public SocialSecuritySlipCollection getSocialSecuritySlipCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getSocialSecuritySlipCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public SocialSecuritySlipCollection getSocialSecuritySlipCollection(String oql) throws BOSException
    {
        try {
            return getController().getSocialSecuritySlipCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public SocialSecuritySlipInfo getSocialSecuritySlipInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getSocialSecuritySlipInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public SocialSecuritySlipInfo getSocialSecuritySlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getSocialSecuritySlipInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public SocialSecuritySlipInfo getSocialSecuritySlipInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getSocialSecuritySlipInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}