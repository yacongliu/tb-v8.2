package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class SocialSecuritySlipCollection extends AbstractObjectCollection 
{
    public SocialSecuritySlipCollection()
    {
        super(SocialSecuritySlipInfo.class);
    }
    public boolean add(SocialSecuritySlipInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SocialSecuritySlipCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SocialSecuritySlipInfo item)
    {
        return removeObject(item);
    }
    public SocialSecuritySlipInfo get(int index)
    {
        return(SocialSecuritySlipInfo)getObject(index);
    }
    public SocialSecuritySlipInfo get(Object key)
    {
        return(SocialSecuritySlipInfo)getObject(key);
    }
    public void set(int index, SocialSecuritySlipInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SocialSecuritySlipInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SocialSecuritySlipInfo item)
    {
        return super.indexOf(item);
    }
}