package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractJQitemControlEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractJQitemControlEntryInfo()
    {
        this("id");
    }
    protected AbstractJQitemControlEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.jqdocking.JQitemControlInfo getBill()
    {
        return (com.kingdee.eas.custom.jqdocking.JQitemControlInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.jqdocking.JQitemControlInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's Ա�� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object: ��¼ 's ������Ŀ property 
     */
    public com.kingdee.eas.custom.jqdocking.JQItemInfo getJQItem()
    {
        return (com.kingdee.eas.custom.jqdocking.JQItemInfo)get("JQItem");
    }
    public void setJQItem(com.kingdee.eas.custom.jqdocking.JQItemInfo item)
    {
        put("JQItem", item);
    }
    /**
     * Object: ��¼ 's н����Ŀ property 
     */
    public com.kingdee.shr.compensation.CmpItemInfo getCMPItem()
    {
        return (com.kingdee.shr.compensation.CmpItemInfo)get("CMPItem");
    }
    public void setCMPItem(com.kingdee.shr.compensation.CmpItemInfo item)
    {
        put("CMPItem", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("08998EFA");
    }
}