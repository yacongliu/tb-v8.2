package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class ProvidentFundSlipFactory
{
    private ProvidentFundSlipFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlip getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlip)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("15EE8D81") ,com.kingdee.eas.custom.jqdocking.IProvidentFundSlip.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlip getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlip)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("15EE8D81") ,com.kingdee.eas.custom.jqdocking.IProvidentFundSlip.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlip getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlip)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("15EE8D81"));
    }
    public static com.kingdee.eas.custom.jqdocking.IProvidentFundSlip getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IProvidentFundSlip)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("15EE8D81"));
    }
}