package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractJQitemControlInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractJQitemControlInfo()
    {
        this("id");
    }
    protected AbstractJQitemControlInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.jqdocking.JQitemControlEntryCollection());
    }
    /**
     * Object: ����н����Ŀ���� 's ��¼ property 
     */
    public com.kingdee.eas.custom.jqdocking.JQitemControlEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.jqdocking.JQitemControlEntryCollection)get("entrys");
    }
    /**
     * Object: ����н����Ŀ���� 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:����н����Ŀ����'s ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:����н����Ŀ����'s ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    /**
     * Object:����н����Ŀ����'s ���property 
     */
    public String getName()
    {
        return getString("name");
    }
    public void setName(String item)
    {
        setString("name", item);
    }
    /**
     * Object:����н����Ŀ����'s ״̬property 
     */
    public com.kingdee.eas.basedata.hraux.StateEnum getState()
    {
        return com.kingdee.eas.basedata.hraux.StateEnum.getEnum(getInt("state"));
    }
    public void setState(com.kingdee.eas.basedata.hraux.StateEnum item)
    {
		if (item != null) {
        setInt("state", item.getValue());
		}
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("08310058");
    }
}