package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class AnnuitySlipCollection extends AbstractObjectCollection 
{
    public AnnuitySlipCollection()
    {
        super(AnnuitySlipInfo.class);
    }
    public boolean add(AnnuitySlipInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AnnuitySlipCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AnnuitySlipInfo item)
    {
        return removeObject(item);
    }
    public AnnuitySlipInfo get(int index)
    {
        return(AnnuitySlipInfo)getObject(index);
    }
    public AnnuitySlipInfo get(Object key)
    {
        return(AnnuitySlipInfo)getObject(key);
    }
    public void set(int index, AnnuitySlipInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AnnuitySlipInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AnnuitySlipInfo item)
    {
        return super.indexOf(item);
    }
}