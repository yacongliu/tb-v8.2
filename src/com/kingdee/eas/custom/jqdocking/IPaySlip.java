package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBase;

public interface IPaySlip extends IHRBillBase
{
    public PaySlipCollection getPaySlipCollection() throws BOSException;
    public PaySlipCollection getPaySlipCollection(EntityViewInfo view) throws BOSException;
    public PaySlipCollection getPaySlipCollection(String oql) throws BOSException;
    public PaySlipInfo getPaySlipInfo(IObjectPK pk) throws BOSException, EASBizException;
    public PaySlipInfo getPaySlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public PaySlipInfo getPaySlipInfo(String oql) throws BOSException, EASBizException;
}