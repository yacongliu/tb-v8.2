package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class JQItemFactory
{
    private JQItemFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IJQItem getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQItem)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("B0A3E965") ,com.kingdee.eas.custom.jqdocking.IJQItem.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IJQItem getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQItem)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("B0A3E965") ,com.kingdee.eas.custom.jqdocking.IJQItem.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IJQItem getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQItem)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("B0A3E965"));
    }
    public static com.kingdee.eas.custom.jqdocking.IJQItem getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQItem)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("B0A3E965"));
    }
}