package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class JQitemControlEntryFactory
{
    private JQitemControlEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IJQitemControlEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControlEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("08998EFA") ,com.kingdee.eas.custom.jqdocking.IJQitemControlEntry.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IJQitemControlEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControlEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("08998EFA") ,com.kingdee.eas.custom.jqdocking.IJQitemControlEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IJQitemControlEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControlEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("08998EFA"));
    }
    public static com.kingdee.eas.custom.jqdocking.IJQitemControlEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControlEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("08998EFA"));
    }
}