package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class PaySlipCollection extends AbstractObjectCollection 
{
    public PaySlipCollection()
    {
        super(PaySlipInfo.class);
    }
    public boolean add(PaySlipInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(PaySlipCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(PaySlipInfo item)
    {
        return removeObject(item);
    }
    public PaySlipInfo get(int index)
    {
        return(PaySlipInfo)getObject(index);
    }
    public PaySlipInfo get(Object key)
    {
        return(PaySlipInfo)getObject(key);
    }
    public void set(int index, PaySlipInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(PaySlipInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(PaySlipInfo item)
    {
        return super.indexOf(item);
    }
}