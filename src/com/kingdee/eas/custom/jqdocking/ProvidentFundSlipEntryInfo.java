package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

public class ProvidentFundSlipEntryInfo extends AbstractProvidentFundSlipEntryInfo implements Serializable 
{
    public ProvidentFundSlipEntryInfo()
    {
        super();
    }
    protected ProvidentFundSlipEntryInfo(String pkField)
    {
        super(pkField);
    }
}