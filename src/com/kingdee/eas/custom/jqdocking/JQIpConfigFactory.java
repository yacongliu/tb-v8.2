package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class JQIpConfigFactory
{
    private JQIpConfigFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IJQIpConfig getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQIpConfig)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("72C8B89B") ,com.kingdee.eas.custom.jqdocking.IJQIpConfig.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IJQIpConfig getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQIpConfig)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("72C8B89B") ,com.kingdee.eas.custom.jqdocking.IJQIpConfig.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IJQIpConfig getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQIpConfig)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("72C8B89B"));
    }
    public static com.kingdee.eas.custom.jqdocking.IJQIpConfig getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQIpConfig)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("72C8B89B"));
    }
}