package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class SocialSecuritySlipEntryFactory
{
    private SocialSecuritySlipEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("1CFD069A") ,com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("1CFD069A") ,com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("1CFD069A"));
    }
    public static com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.ISocialSecuritySlipEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("1CFD069A"));
    }
}