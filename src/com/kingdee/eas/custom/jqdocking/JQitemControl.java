package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.JQitemControlController;
import com.kingdee.eas.hr.base.HRBillBase;

public class JQitemControl extends HRBillBase implements IJQitemControl
{
    public JQitemControl()
    {
        super();
        registerInterface(IJQitemControl.class, this);
    }
    public JQitemControl(Context ctx)
    {
        super(ctx);
        registerInterface(IJQitemControl.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("08310058");
    }
    private JQitemControlController getController() throws BOSException
    {
        return (JQitemControlController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public JQitemControlCollection getJQitemControlCollection() throws BOSException
    {
        try {
            return getController().getJQitemControlCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public JQitemControlCollection getJQitemControlCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getJQitemControlCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public JQitemControlCollection getJQitemControlCollection(String oql) throws BOSException
    {
        try {
            return getController().getJQitemControlCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public JQitemControlInfo getJQitemControlInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getJQitemControlInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public JQitemControlInfo getJQitemControlInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getJQitemControlInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public JQitemControlInfo getJQitemControlInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getJQitemControlInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}