package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.Context;
import com.kingdee.bos.util.BOSObjectType;

public class JQitemControlFactory
{
    private JQitemControlFactory()
    {
    }
    public static com.kingdee.eas.custom.jqdocking.IJQitemControl getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControl)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("08310058") ,com.kingdee.eas.custom.jqdocking.IJQitemControl.class);
    }
    
    public static com.kingdee.eas.custom.jqdocking.IJQitemControl getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControl)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("08310058") ,com.kingdee.eas.custom.jqdocking.IJQitemControl.class, objectCtx);
    }
    public static com.kingdee.eas.custom.jqdocking.IJQitemControl getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControl)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("08310058"));
    }
    public static com.kingdee.eas.custom.jqdocking.IJQitemControl getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.jqdocking.IJQitemControl)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("08310058"));
    }
}