package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class PaySlipEntryCollection extends AbstractObjectCollection 
{
    public PaySlipEntryCollection()
    {
        super(PaySlipEntryInfo.class);
    }
    public boolean add(PaySlipEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(PaySlipEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(PaySlipEntryInfo item)
    {
        return removeObject(item);
    }
    public PaySlipEntryInfo get(int index)
    {
        return(PaySlipEntryInfo)getObject(index);
    }
    public PaySlipEntryInfo get(Object key)
    {
        return(PaySlipEntryInfo)getObject(key);
    }
    public void set(int index, PaySlipEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(PaySlipEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(PaySlipEntryInfo item)
    {
        return super.indexOf(item);
    }
}