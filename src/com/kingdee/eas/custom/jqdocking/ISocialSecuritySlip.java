package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBase;

public interface ISocialSecuritySlip extends IHRBillBase
{
    public SocialSecuritySlipCollection getSocialSecuritySlipCollection() throws BOSException;
    public SocialSecuritySlipCollection getSocialSecuritySlipCollection(EntityViewInfo view) throws BOSException;
    public SocialSecuritySlipCollection getSocialSecuritySlipCollection(String oql) throws BOSException;
    public SocialSecuritySlipInfo getSocialSecuritySlipInfo(IObjectPK pk) throws BOSException, EASBizException;
    public SocialSecuritySlipInfo getSocialSecuritySlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public SocialSecuritySlipInfo getSocialSecuritySlipInfo(String oql) throws BOSException, EASBizException;
}