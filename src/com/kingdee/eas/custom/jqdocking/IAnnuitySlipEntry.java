package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;

public interface IAnnuitySlipEntry extends IHRBillBaseEntry
{
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public AnnuitySlipEntryInfo getAnnuitySlipEntryInfo(String oql) throws BOSException, EASBizException;
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection() throws BOSException;
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(EntityViewInfo view) throws BOSException;
    public AnnuitySlipEntryCollection getAnnuitySlipEntryCollection(String oql) throws BOSException;
}