package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;

public interface IJQitemControlEntry extends IHRBillBaseEntry
{
    public JQitemControlEntryInfo getJQitemControlEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public JQitemControlEntryInfo getJQitemControlEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public JQitemControlEntryInfo getJQitemControlEntryInfo(String oql) throws BOSException, EASBizException;
    public JQitemControlEntryCollection getJQitemControlEntryCollection() throws BOSException;
    public JQitemControlEntryCollection getJQitemControlEntryCollection(EntityViewInfo view) throws BOSException;
    public JQitemControlEntryCollection getJQitemControlEntryCollection(String oql) throws BOSException;
}