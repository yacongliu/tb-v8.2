package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.dao.AbstractObjectCollection;

public class ProvidentFundSlipCollection extends AbstractObjectCollection 
{
    public ProvidentFundSlipCollection()
    {
        super(ProvidentFundSlipInfo.class);
    }
    public boolean add(ProvidentFundSlipInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ProvidentFundSlipCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ProvidentFundSlipInfo item)
    {
        return removeObject(item);
    }
    public ProvidentFundSlipInfo get(int index)
    {
        return(ProvidentFundSlipInfo)getObject(index);
    }
    public ProvidentFundSlipInfo get(Object key)
    {
        return(ProvidentFundSlipInfo)getObject(key);
    }
    public void set(int index, ProvidentFundSlipInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ProvidentFundSlipInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ProvidentFundSlipInfo item)
    {
        return super.indexOf(item);
    }
}