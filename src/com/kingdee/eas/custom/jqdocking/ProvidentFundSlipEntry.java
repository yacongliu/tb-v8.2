package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.ProvidentFundSlipEntryController;
import com.kingdee.eas.hr.base.HRBillBaseEntry;

public class ProvidentFundSlipEntry extends HRBillBaseEntry implements IProvidentFundSlipEntry
{
    public ProvidentFundSlipEntry()
    {
        super();
        registerInterface(IProvidentFundSlipEntry.class, this);
    }
    public ProvidentFundSlipEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IProvidentFundSlipEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("EA42D371");
    }
    private ProvidentFundSlipEntryController getController() throws BOSException
    {
        return (ProvidentFundSlipEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getProvidentFundSlipEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getProvidentFundSlipEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ProvidentFundSlipEntryInfo getProvidentFundSlipEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getProvidentFundSlipEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection() throws BOSException
    {
        try {
            return getController().getProvidentFundSlipEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getProvidentFundSlipEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ProvidentFundSlipEntryCollection getProvidentFundSlipEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getProvidentFundSlipEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}