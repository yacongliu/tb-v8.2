package com.kingdee.eas.custom.jqdocking;

import java.rmi.RemoteException;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.custom.jqdocking.app.PaySlipController;
import com.kingdee.eas.hr.base.HRBillBase;

public class PaySlip extends HRBillBase implements IPaySlip
{
    public PaySlip()
    {
        super();
        registerInterface(IPaySlip.class, this);
    }
    public PaySlip(Context ctx)
    {
        super(ctx);
        registerInterface(IPaySlip.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("BF21B2DD");
    }
    private PaySlipController getController() throws BOSException
    {
        return (PaySlipController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public PaySlipCollection getPaySlipCollection() throws BOSException
    {
        try {
            return getController().getPaySlipCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public PaySlipCollection getPaySlipCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getPaySlipCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public PaySlipCollection getPaySlipCollection(String oql) throws BOSException
    {
        try {
            return getController().getPaySlipCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public PaySlipInfo getPaySlipInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getPaySlipInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public PaySlipInfo getPaySlipInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getPaySlipInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public PaySlipInfo getPaySlipInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getPaySlipInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}