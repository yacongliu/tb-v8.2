package com.kingdee.eas.custom.jqdocking;

import java.io.Serializable;

import com.kingdee.bos.util.BOSObjectType;


public class AbstractJQIpConfigInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractJQIpConfigInfo()
    {
        this("id");
    }
    protected AbstractJQIpConfigInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:����ӿ�IP����'s ����ӿ�URLproperty 
     */
    public String getJiuQiURL()
    {
        return getString("JiuQiURL");
    }
    public void setJiuQiURL(String item)
    {
        setString("JiuQiURL", item);
    }
    /**
     * Object:����ӿ�IP����'s ����ӿ���property 
     */
    public String getJiuQiClass()
    {
        return getString("JiuQiClass");
    }
    public void setJiuQiClass(String item)
    {
        setString("JiuQiClass", item);
    }
    /**
     * Object:����ӿ�IP����'s ����ӿڷ���property 
     */
    public String getJiuQiMethod()
    {
        return getString("JiuQiMethod");
    }
    public void setJiuQiMethod(String item)
    {
        setString("JiuQiMethod", item);
    }
    /**
     * Object:����ӿ�IP����'s ����property 
     */
    public String getPassword()
    {
        return getString("password");
    }
    public void setPassword(String item)
    {
        setString("password", item);
    }
    /**
     * Object:����ӿ�IP����'s �û�property 
     */
    public String getUser()
    {
        return getString("user");
    }
    public void setUser(String item)
    {
        setString("user", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("72C8B89B");
    }
}