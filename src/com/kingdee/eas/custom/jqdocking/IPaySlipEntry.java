package com.kingdee.eas.custom.jqdocking;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;

public interface IPaySlipEntry extends IHRBillBaseEntry
{
    public PaySlipEntryInfo getPaySlipEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public PaySlipEntryInfo getPaySlipEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public PaySlipEntryInfo getPaySlipEntryInfo(String oql) throws BOSException, EASBizException;
    public PaySlipEntryCollection getPaySlipEntryCollection() throws BOSException;
    public PaySlipEntryCollection getPaySlipEntryCollection(EntityViewInfo view) throws BOSException;
    public PaySlipEntryCollection getPaySlipEntryCollection(String oql) throws BOSException;
}