package com.kingdee.eas.custom.assure;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IExternalAssureBizBill extends IDataBase
{
    public ExternalAssureBizBillInfo getExternalAssureBizBillInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ExternalAssureBizBillInfo getExternalAssureBizBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ExternalAssureBizBillInfo getExternalAssureBizBillInfo(String oql) throws BOSException, EASBizException;
    public ExternalAssureBizBillCollection getExternalAssureBizBillCollection() throws BOSException;
    public ExternalAssureBizBillCollection getExternalAssureBizBillCollection(EntityViewInfo view) throws BOSException;
    public ExternalAssureBizBillCollection getExternalAssureBizBillCollection(String oql) throws BOSException;
}