package com.kingdee.eas.custom.assure;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractExternalAssureBizBillInfo extends com.kingdee.eas.framework.DataBaseInfo implements Serializable 
{
    public AbstractExternalAssureBizBillInfo()
    {
        this("id");
    }
    protected AbstractExternalAssureBizBillInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:���ⵣ��ҵ��'s ��������property 
     */
    public String getBillNumber()
    {
        return getString("billNumber");
    }
    public void setBillNumber(String item)
    {
        setString("billNumber", item);
    }
    /**
     * Object:���ⵣ��ҵ��'s ��������property 
     */
    public com.kingdee.eas.custom.assure.app.FundsTypeEnum getType()
    {
        return com.kingdee.eas.custom.assure.app.FundsTypeEnum.getEnum(getString("type"));
    }
    public void setType(com.kingdee.eas.custom.assure.app.FundsTypeEnum item)
    {
		if (item != null) {
        setString("type", item.getValue());
		}
    }
    /**
     * Object:���ⵣ��ҵ��'s ���property 
     */
    public java.math.BigDecimal getMoney()
    {
        return getBigDecimal("money");
    }
    public void setMoney(java.math.BigDecimal item)
    {
        setBigDecimal("money", item);
    }
    /**
     * Object:���ⵣ��ҵ��'s ҵ������property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("593C9C4C");
    }
}