package com.kingdee.eas.custom.assure;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ExternalAssureBizBillCollection extends AbstractObjectCollection 
{
    public ExternalAssureBizBillCollection()
    {
        super(ExternalAssureBizBillInfo.class);
    }
    public boolean add(ExternalAssureBizBillInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ExternalAssureBizBillCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ExternalAssureBizBillInfo item)
    {
        return removeObject(item);
    }
    public ExternalAssureBizBillInfo get(int index)
    {
        return(ExternalAssureBizBillInfo)getObject(index);
    }
    public ExternalAssureBizBillInfo get(Object key)
    {
        return(ExternalAssureBizBillInfo)getObject(key);
    }
    public void set(int index, ExternalAssureBizBillInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ExternalAssureBizBillInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ExternalAssureBizBillInfo item)
    {
        return super.indexOf(item);
    }
}