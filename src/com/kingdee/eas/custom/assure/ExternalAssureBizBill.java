package com.kingdee.eas.custom.assure;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.assure.app.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.DataBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.IDataBase;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class ExternalAssureBizBill extends DataBase implements IExternalAssureBizBill
{
    public ExternalAssureBizBill()
    {
        super();
        registerInterface(IExternalAssureBizBill.class, this);
    }
    public ExternalAssureBizBill(Context ctx)
    {
        super(ctx);
        registerInterface(IExternalAssureBizBill.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("593C9C4C");
    }
    private ExternalAssureBizBillController getController() throws BOSException
    {
        return (ExternalAssureBizBillController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ExternalAssureBizBillInfo getExternalAssureBizBillInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getExternalAssureBizBillInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ExternalAssureBizBillInfo getExternalAssureBizBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getExternalAssureBizBillInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ExternalAssureBizBillInfo getExternalAssureBizBillInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getExternalAssureBizBillInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ExternalAssureBizBillCollection getExternalAssureBizBillCollection() throws BOSException
    {
        try {
            return getController().getExternalAssureBizBillCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ExternalAssureBizBillCollection getExternalAssureBizBillCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getExternalAssureBizBillCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ExternalAssureBizBillCollection getExternalAssureBizBillCollection(String oql) throws BOSException
    {
        try {
            return getController().getExternalAssureBizBillCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}