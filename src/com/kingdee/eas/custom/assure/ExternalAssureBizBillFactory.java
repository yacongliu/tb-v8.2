package com.kingdee.eas.custom.assure;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ExternalAssureBizBillFactory
{
    private ExternalAssureBizBillFactory()
    {
    }
    public static com.kingdee.eas.custom.assure.IExternalAssureBizBill getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.assure.IExternalAssureBizBill)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("593C9C4C") ,com.kingdee.eas.custom.assure.IExternalAssureBizBill.class);
    }
    
    public static com.kingdee.eas.custom.assure.IExternalAssureBizBill getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.assure.IExternalAssureBizBill)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("593C9C4C") ,com.kingdee.eas.custom.assure.IExternalAssureBizBill.class, objectCtx);
    }
    public static com.kingdee.eas.custom.assure.IExternalAssureBizBill getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.assure.IExternalAssureBizBill)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("593C9C4C"));
    }
    public static com.kingdee.eas.custom.assure.IExternalAssureBizBill getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.assure.IExternalAssureBizBill)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("593C9C4C"));
    }
}