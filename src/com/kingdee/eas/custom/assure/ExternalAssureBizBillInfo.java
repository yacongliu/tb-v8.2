package com.kingdee.eas.custom.assure;

import java.io.Serializable;

public class ExternalAssureBizBillInfo extends AbstractExternalAssureBizBillInfo implements Serializable 
{
    public ExternalAssureBizBillInfo()
    {
        super();
    }
    protected ExternalAssureBizBillInfo(String pkField)
    {
        super(pkField);
    }
}