/**
 * output package name
 */
package com.kingdee.eas.custom.assure.app;

import java.util.Map;
import java.util.List;
import java.util.Iterator;
import com.kingdee.util.enums.StringEnum;

/**
 * output class name
 */
public class FundsTypeEnum extends StringEnum
{
    public static final String BORROW_VALUE = "1";//alias=���
    public static final String PAY_VALUE = "-1";//alias=����

    public static final FundsTypeEnum borrow = new FundsTypeEnum("borrow", BORROW_VALUE);
    public static final FundsTypeEnum pay = new FundsTypeEnum("pay", PAY_VALUE);

    /**
     * construct function
     * @param String fundsTypeEnum
     */
    private FundsTypeEnum(String name, String fundsTypeEnum)
    {
        super(name, fundsTypeEnum);
    }
    
    /**
     * getEnum function
     * @param String arguments
     */
    public static FundsTypeEnum getEnum(String fundsTypeEnum)
    {
        return (FundsTypeEnum)getEnum(FundsTypeEnum.class, fundsTypeEnum);
    }

    /**
     * getEnumMap function
     */
    public static Map getEnumMap()
    {
        return getEnumMap(FundsTypeEnum.class);
    }

    /**
     * getEnumList function
     */
    public static List getEnumList()
    {
         return getEnumList(FundsTypeEnum.class);
    }
    
    /**
     * getIterator function
     */
    public static Iterator iterator()
    {
         return iterator(FundsTypeEnum.class);
    }
}