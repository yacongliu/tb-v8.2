/**
 * output package name
 */
package com.kingdee.eas.custom.assure.app;

import com.kingdee.bos.Context;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.eas.framework.batchHandler.ResponseContext;


/**
 * output class name
 */
public abstract class AbstractExternalAssureBizBillListUIHandler extends com.kingdee.eas.framework.app.ListUIHandler

{
	public void handleActionApprovex(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionApprovex(request,response,context);
	}
	protected void _handleActionApprovex(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
	public void handleActionUnApprovex(RequestContext request,ResponseContext response, Context context) throws Exception {
		_handleActionUnApprovex(request,response,context);
	}
	protected void _handleActionUnApprovex(RequestContext request,ResponseContext response, Context context) throws Exception {
	}
}