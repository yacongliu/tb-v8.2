package com.kingdee.eas.custom.assure.client;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.kingdee.bos.ui.face.CoreUIObject;
import com.kingdee.eas.rptclient.newrpt.util.MsgBox;
import com.kingdee.util.StringUtils;

/**
 * 
 * Title: ExternalAssureBillEditUICTEx
 * <p>
 * Description:对外担保业务表单
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-6-18 & 下午05:59:16
 * @since V1.0
 */
public class ExternalAssureBizBillEditUICTEx extends ExternalAssureBizBillEditUI {

    private static final long serialVersionUID = 2230883155656409073L;

    private static final Logger logger = CoreUIObject.getLogger(ExternalAssureBizBillEditUICTEx.class);

    public ExternalAssureBizBillEditUICTEx() throws Exception {
        super();
    }

    @Override
    public void onLoad() throws Exception {

        buttonSetting();

        // DEP传来的选中行单据内码
        String id = (String) getUIContext().get("id");
        String billNumber = (String) getUIContext().get("fnumber");
        logger.info("DEP更新对外担保余额界面传来的选中担保单内码：" + id);

        super.onLoad();

        if (!StringUtils.isEmpty(billNumber)) {
            this.txtbillNumber.setText(billNumber);
        }

    }

    @Override
    public void actionSave_actionPerformed(ActionEvent e) throws Exception {

        if (dataCheck()) {
            super.actionSave_actionPerformed(e);
            // 关闭当前窗口
            getUIWindow().close();
        }
    }

    @Override
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception {
        if (dataCheck()) {
            super.actionSubmit_actionPerformed(e);
            // 关闭当前窗口
            getUIWindow().close();
        }

    }

    /**
     * <p>
     * Title: buttonSetting
     * </p>
     * <p>
     * Description: 按钮设置
     * </p>
     */
    private void buttonSetting() {
        this.btnSave.setVisible(false);
        this.btnSave.setEnabled(false);
        this.btnAddNew.setVisible(false);
        this.btnAddNew.setEnabled(false);

        this.txtName.setEnabled(false);

    }

    /**
     * 
     * <p>
     * Title: dataCheck
     * </p>
     * <p>
     * Description: 数据校验
     * </p>
     */
    private boolean dataCheck() {

        String billNumber = this.txtbillNumber.getText();// 担保单号
        String type = this.type.getSelectedItem().toString();// 款项性质
        Object bizDate = this.pkbizDate.getValue();
        BigDecimal money = (BigDecimal) this.txtmoney.getValue(BigDecimal.class);// 金额

        if (StringUtils.isEmpty(billNumber)) {
            MsgBox.showWarning("担保单号不能为空！");
            return false;
        }

        if (StringUtils.isEmpty(type)) {
            MsgBox.showWarning("款项性质还没有选择哦！");
            return false;
        }

        if (money == null) {
            MsgBox.showWarning("金额还没有填写哦！");
            return false;
        }

        if (bizDate == null) {
            MsgBox.showWarning("业务日期还没有填写哦！");
            return false;
        }

        return true;
    }

}
