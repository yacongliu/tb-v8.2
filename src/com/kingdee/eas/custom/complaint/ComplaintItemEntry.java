package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.complaint.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class ComplaintItemEntry extends HRBillBaseEntry implements IComplaintItemEntry
{
    public ComplaintItemEntry()
    {
        super();
        registerInterface(IComplaintItemEntry.class, this);
    }
    public ComplaintItemEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IComplaintItemEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("AEF89E5C");
    }
    private ComplaintItemEntryController getController() throws BOSException
    {
        return (ComplaintItemEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public ComplaintItemEntryInfo getComplaintItemEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getComplaintItemEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public ComplaintItemEntryInfo getComplaintItemEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getComplaintItemEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public ComplaintItemEntryInfo getComplaintItemEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getComplaintItemEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public ComplaintItemEntryCollection getComplaintItemEntryCollection() throws BOSException
    {
        try {
            return getController().getComplaintItemEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public ComplaintItemEntryCollection getComplaintItemEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getComplaintItemEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public ComplaintItemEntryCollection getComplaintItemEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getComplaintItemEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}