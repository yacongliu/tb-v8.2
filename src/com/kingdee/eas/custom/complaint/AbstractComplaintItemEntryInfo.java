package com.kingdee.eas.custom.complaint;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractComplaintItemEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractComplaintItemEntryInfo()
    {
        this("id");
    }
    protected AbstractComplaintItemEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: 分录 's 单据头 property 
     */
    public com.kingdee.eas.custom.complaint.ComplaintItemInfo getBill()
    {
        return (com.kingdee.eas.custom.complaint.ComplaintItemInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.complaint.ComplaintItemInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: 分录 's 员工 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:分录's 生效日期property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:分录's 详细说明property 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: 分录 's 所属行政组织 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: 分录 's 职位 property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object: 分录 's 投诉部门 property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getComplaintDept()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("complaintDept");
    }
    public void setComplaintDept(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("complaintDept", item);
    }
    /**
     * Object:分录's 投诉次数property 
     */
    public int getComplaintNum()
    {
        return getInt("complaintNum");
    }
    public void setComplaintNum(int item)
    {
        setInt("complaintNum", item);
    }
    /**
     * Object:分录's 投诉类别property 
     */
    public com.kingdee.eas.custom.complaintCategory getComplaintType()
    {
        return com.kingdee.eas.custom.complaintCategory.getEnum(getString("complaintType"));
    }
    public void setComplaintType(com.kingdee.eas.custom.complaintCategory item)
    {
		if (item != null) {
        setString("complaintType", item.getValue());
		}
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("AEF89E5C");
    }
}