package com.kingdee.eas.custom.complaint;

import java.io.Serializable;

public class ComplaintItemInfo extends AbstractComplaintItemInfo implements Serializable 
{
    public ComplaintItemInfo()
    {
        super();
    }
    protected ComplaintItemInfo(String pkField)
    {
        super(pkField);
    }
}