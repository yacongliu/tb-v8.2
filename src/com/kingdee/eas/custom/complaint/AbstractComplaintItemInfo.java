package com.kingdee.eas.custom.complaint;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractComplaintItemInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractComplaintItemInfo()
    {
        this("id");
    }
    protected AbstractComplaintItemInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.complaint.ComplaintItemEntryCollection());
    }
    /**
     * Object: 投诉单 's 分录 property 
     */
    public com.kingdee.eas.custom.complaint.ComplaintItemEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.complaint.ComplaintItemEntryCollection)get("entrys");
    }
    /**
     * Object: 投诉单 's 申请人 property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:投诉单's 申请日期property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:投诉单's 辅助状态property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("CCDA5BB6");
    }
}