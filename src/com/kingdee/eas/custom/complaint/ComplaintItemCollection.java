package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ComplaintItemCollection extends AbstractObjectCollection 
{
    public ComplaintItemCollection()
    {
        super(ComplaintItemInfo.class);
    }
    public boolean add(ComplaintItemInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ComplaintItemCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ComplaintItemInfo item)
    {
        return removeObject(item);
    }
    public ComplaintItemInfo get(int index)
    {
        return(ComplaintItemInfo)getObject(index);
    }
    public ComplaintItemInfo get(Object key)
    {
        return(ComplaintItemInfo)getObject(key);
    }
    public void set(int index, ComplaintItemInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ComplaintItemInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ComplaintItemInfo item)
    {
        return super.indexOf(item);
    }
}