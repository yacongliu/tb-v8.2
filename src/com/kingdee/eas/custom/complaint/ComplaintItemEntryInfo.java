package com.kingdee.eas.custom.complaint;

import java.io.Serializable;

public class ComplaintItemEntryInfo extends AbstractComplaintItemEntryInfo implements Serializable 
{
    public ComplaintItemEntryInfo()
    {
        super();
    }
    protected ComplaintItemEntryInfo(String pkField)
    {
        super(pkField);
    }
}