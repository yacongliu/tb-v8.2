package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class ComplaintItemEntryCollection extends AbstractObjectCollection 
{
    public ComplaintItemEntryCollection()
    {
        super(ComplaintItemEntryInfo.class);
    }
    public boolean add(ComplaintItemEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(ComplaintItemEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(ComplaintItemEntryInfo item)
    {
        return removeObject(item);
    }
    public ComplaintItemEntryInfo get(int index)
    {
        return(ComplaintItemEntryInfo)getObject(index);
    }
    public ComplaintItemEntryInfo get(Object key)
    {
        return(ComplaintItemEntryInfo)getObject(key);
    }
    public void set(int index, ComplaintItemEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(ComplaintItemEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(ComplaintItemEntryInfo item)
    {
        return super.indexOf(item);
    }
}