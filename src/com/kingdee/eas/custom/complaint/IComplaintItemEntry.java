package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IComplaintItemEntry extends IHRBillBaseEntry
{
    public ComplaintItemEntryInfo getComplaintItemEntryInfo(IObjectPK pk) throws BOSException, EASBizException;
    public ComplaintItemEntryInfo getComplaintItemEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public ComplaintItemEntryInfo getComplaintItemEntryInfo(String oql) throws BOSException, EASBizException;
    public ComplaintItemEntryCollection getComplaintItemEntryCollection() throws BOSException;
    public ComplaintItemEntryCollection getComplaintItemEntryCollection(EntityViewInfo view) throws BOSException;
    public ComplaintItemEntryCollection getComplaintItemEntryCollection(String oql) throws BOSException;
}