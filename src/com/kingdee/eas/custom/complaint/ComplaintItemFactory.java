package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ComplaintItemFactory
{
    private ComplaintItemFactory()
    {
    }
    public static com.kingdee.eas.custom.complaint.IComplaintItem getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItem)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("CCDA5BB6") ,com.kingdee.eas.custom.complaint.IComplaintItem.class);
    }
    
    public static com.kingdee.eas.custom.complaint.IComplaintItem getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItem)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("CCDA5BB6") ,com.kingdee.eas.custom.complaint.IComplaintItem.class, objectCtx);
    }
    public static com.kingdee.eas.custom.complaint.IComplaintItem getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItem)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("CCDA5BB6"));
    }
    public static com.kingdee.eas.custom.complaint.IComplaintItem getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItem)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("CCDA5BB6"));
    }
}