package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class ComplaintItemEntryFactory
{
    private ComplaintItemEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.complaint.IComplaintItemEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItemEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("AEF89E5C") ,com.kingdee.eas.custom.complaint.IComplaintItemEntry.class);
    }
    
    public static com.kingdee.eas.custom.complaint.IComplaintItemEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItemEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("AEF89E5C") ,com.kingdee.eas.custom.complaint.IComplaintItemEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.complaint.IComplaintItemEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItemEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("AEF89E5C"));
    }
    public static com.kingdee.eas.custom.complaint.IComplaintItemEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.complaint.IComplaintItemEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("AEF89E5C"));
    }
}