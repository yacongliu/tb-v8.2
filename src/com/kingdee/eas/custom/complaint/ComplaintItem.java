package com.kingdee.eas.custom.complaint;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.custom.complaint.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class ComplaintItem extends HRBillBase implements IComplaintItem
{
    public ComplaintItem()
    {
        super();
        registerInterface(IComplaintItem.class, this);
    }
    public ComplaintItem(Context ctx)
    {
        super(ctx);
        registerInterface(IComplaintItem.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("CCDA5BB6");
    }
    private ComplaintItemController getController() throws BOSException
    {
        return (ComplaintItemController)getBizController();
    }
    /**
     *取集合-System defined method
     *@return
     */
    public ComplaintItemCollection getComplaintItemCollection() throws BOSException
    {
        try {
            return getController().getComplaintItemCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param view 取集合
     *@return
     */
    public ComplaintItemCollection getComplaintItemCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getComplaintItemCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取集合-System defined method
     *@param oql 取集合
     *@return
     */
    public ComplaintItemCollection getComplaintItemCollection(String oql) throws BOSException
    {
        try {
            return getController().getComplaintItemCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@return
     */
    public ComplaintItemInfo getComplaintItemInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getComplaintItemInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param pk 取值
     *@param selector 取值
     *@return
     */
    public ComplaintItemInfo getComplaintItemInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getComplaintItemInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *取值-System defined method
     *@param oql 取值
     *@return
     */
    public ComplaintItemInfo getComplaintItemInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getComplaintItemInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *设置单据状态-User defined method
     *@param billId 单据id
     *@param state 单据状态
     */
    public void setState(BOSUuid billId, HRBillStateEnum state) throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext(), billId, state);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *设置审批通过-User defined method
     *@param billId 单据id
     */
    public void setPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *设置审批不通过状态-User defined method
     *@param billId 单据id
     */
    public void setNoPassState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *设置审批中状态-User defined method
     *@param billId 单据id
     */
    public void setApproveState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *设置未审批状态-User defined method
     *@param billId 单据id
     */
    public void setEditState(BOSUuid billId) throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext(), billId);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}