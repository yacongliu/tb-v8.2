package com.kingdee.eas.custom.recruitment;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractInterviewresultsInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractInterviewresultsInfo()
    {
        this("id");
    }
    protected AbstractInterviewresultsInfo(String pkField)
    {
        super(pkField);
        put("entrys", new com.kingdee.eas.custom.recruitment.InterviewresultsEntryCollection());
    }
    /**
     * Object: ���Խ�� 's ��¼ property 
     */
    public com.kingdee.eas.custom.recruitment.InterviewresultsEntryCollection getEntrys()
    {
        return (com.kingdee.eas.custom.recruitment.InterviewresultsEntryCollection)get("entrys");
    }
    /**
     * Object: ���Խ�� 's ������ property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getApplier()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("applier");
    }
    public void setApplier(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("applier", item);
    }
    /**
     * Object:���Խ��'s ��������property 
     */
    public java.util.Date getApplyDate()
    {
        return getDate("applyDate");
    }
    public void setApplyDate(java.util.Date item)
    {
        setDate("applyDate", item);
    }
    /**
     * Object:���Խ��'s ����״̬property 
     */
    public int getInnerState()
    {
        return getInt("innerState");
    }
    public void setInnerState(int item)
    {
        setInt("innerState", item);
    }
    /**
     * Object:���Խ��'s ����property 
     */
    public String getInterviewPersonName()
    {
        return getString("InterviewPersonName");
    }
    public void setInterviewPersonName(String item)
    {
        setString("InterviewPersonName", item);
    }
    /**
     * Object:���Խ��'s ����property 
     */
    public String getInterLink()
    {
        return getString("InterLink");
    }
    public void setInterLink(String item)
    {
        setString("InterLink", item);
    }
    /**
     * Object: ���Խ�� 's ӦƸ���� property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getApplyOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("ApplyOrg");
    }
    public void setApplyOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("ApplyOrg", item);
    }
    /**
     * Object:���Խ��'s ����property 
     */
    public int getRanking()
    {
        return getInt("Ranking");
    }
    public void setRanking(int item)
    {
        setInt("Ranking", item);
    }
    /**
     * Object:���Խ��'s ����property 
     */
    public int getRatingSocre()
    {
        return getInt("RatingSocre");
    }
    public void setRatingSocre(int item)
    {
        setInt("RatingSocre", item);
    }
    /**
     * Object:���Խ��'s ͨ����property 
     */
    public String getPassRate()
    {
        return getString("passRate");
    }
    public void setPassRate(String item)
    {
        setString("passRate", item);
    }
    /**
     * Object: ���Խ�� 's ӦƸְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getApplyJob()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("ApplyJob");
    }
    public void setApplyJob(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("ApplyJob", item);
    }
    /**
     * Object:���Խ��'s ���֤��property 
     */
    public String getIdNumber()
    {
        return getString("IdNumber");
    }
    public void setIdNumber(String item)
    {
        setString("IdNumber", item);
    }
    /**
     * Object:���Խ��'s ��Ա����property 
     */
    public String getInterviewPersonNumber()
    {
        return getString("InterviewPersonNumber");
    }
    public void setInterviewPersonNumber(String item)
    {
        setString("InterviewPersonNumber", item);
    }
    /**
     * Object:���Խ��'s �������property 
     */
    public String getCreateYear()
    {
        return getString("createYear");
    }
    public void setCreateYear(String item)
    {
        setString("createYear", item);
    }
    /**
     * Object: ���Խ�� 's ���Է��� property 
     */
    public com.kingdee.shr.recuritment.InterviewProcessInfo getInterviewPlan()
    {
        return (com.kingdee.shr.recuritment.InterviewProcessInfo)get("InterviewPlan");
    }
    public void setInterviewPlan(com.kingdee.shr.recuritment.InterviewProcessInfo item)
    {
        put("InterviewPlan", item);
    }
    /**
     * Object:���Խ��'s ��������property 
     */
    public int getFinalRating()
    {
        return getInt("finalRating");
    }
    public void setFinalRating(int item)
    {
        setInt("finalRating", item);
    }
    /**
     * Object:���Խ��'s ��������property 
     */
    public int getFinalRanking()
    {
        return getInt("finalRanking");
    }
    public void setFinalRanking(int item)
    {
        setInt("finalRanking", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("DF114CAC");
    }
}