package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IInterviewresults extends IHRBillBase
{
    public InterviewresultsCollection getInterviewresultsCollection() throws BOSException;
    public InterviewresultsCollection getInterviewresultsCollection(EntityViewInfo view) throws BOSException;
    public InterviewresultsCollection getInterviewresultsCollection(String oql) throws BOSException;
    public InterviewresultsInfo getInterviewresultsInfo(IObjectPK pk) throws BOSException, EASBizException;
    public InterviewresultsInfo getInterviewresultsInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public InterviewresultsInfo getInterviewresultsInfo(String oql) throws BOSException, EASBizException;
}