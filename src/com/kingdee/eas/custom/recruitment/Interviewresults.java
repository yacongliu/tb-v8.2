package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.recruitment.app.*;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class Interviewresults extends HRBillBase implements IInterviewresults
{
    public Interviewresults()
    {
        super();
        registerInterface(IInterviewresults.class, this);
    }
    public Interviewresults(Context ctx)
    {
        super(ctx);
        registerInterface(IInterviewresults.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("DF114CAC");
    }
    private InterviewresultsController getController() throws BOSException
    {
        return (InterviewresultsController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public InterviewresultsCollection getInterviewresultsCollection() throws BOSException
    {
        try {
            return getController().getInterviewresultsCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public InterviewresultsCollection getInterviewresultsCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getInterviewresultsCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public InterviewresultsCollection getInterviewresultsCollection(String oql) throws BOSException
    {
        try {
            return getController().getInterviewresultsCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public InterviewresultsInfo getInterviewresultsInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getInterviewresultsInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public InterviewresultsInfo getInterviewresultsInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getInterviewresultsInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public InterviewresultsInfo getInterviewresultsInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getInterviewresultsInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}