package com.kingdee.eas.custom.recruitment.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.eas.custom.recruitment.InterviewresultsCollection;
import com.kingdee.eas.custom.recruitment.InterviewresultsInfo;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.app.HRBillBaseController;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface InterviewresultsController extends HRBillBaseController
{
    public InterviewresultsCollection getInterviewresultsCollection(Context ctx) throws BOSException, RemoteException;
    public InterviewresultsCollection getInterviewresultsCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public InterviewresultsCollection getInterviewresultsCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public InterviewresultsInfo getInterviewresultsInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public InterviewresultsInfo getInterviewresultsInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public InterviewresultsInfo getInterviewresultsInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
}