package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class InterviewresultsFactory
{
    private InterviewresultsFactory()
    {
    }
    public static com.kingdee.eas.custom.recruitment.IInterviewresults getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresults)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("DF114CAC") ,com.kingdee.eas.custom.recruitment.IInterviewresults.class);
    }
    
    public static com.kingdee.eas.custom.recruitment.IInterviewresults getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresults)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("DF114CAC") ,com.kingdee.eas.custom.recruitment.IInterviewresults.class, objectCtx);
    }
    public static com.kingdee.eas.custom.recruitment.IInterviewresults getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresults)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("DF114CAC"));
    }
    public static com.kingdee.eas.custom.recruitment.IInterviewresults getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresults)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("DF114CAC"));
    }
}