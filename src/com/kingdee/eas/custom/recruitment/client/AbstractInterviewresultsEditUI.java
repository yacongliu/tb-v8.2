/**
 * output package name
 */
package com.kingdee.eas.custom.recruitment.client;

import org.apache.log4j.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.BorderFactory;
import javax.swing.event.*;
import javax.swing.KeyStroke;

import com.kingdee.bos.ctrl.swing.*;
import com.kingdee.bos.ctrl.kdf.table.*;
import com.kingdee.bos.ctrl.kdf.data.event.*;
import com.kingdee.bos.dao.*;
import com.kingdee.bos.dao.query.*;
import com.kingdee.bos.metadata.*;
import com.kingdee.bos.metadata.entity.*;
import com.kingdee.bos.ui.face.*;
import com.kingdee.bos.ui.util.ResourceBundleHelper;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.bos.service.ServiceContext;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.util.enums.EnumUtils;
import com.kingdee.bos.ui.face.UIRuleUtil;
import com.kingdee.bos.ctrl.swing.event.*;
import com.kingdee.bos.ctrl.kdf.table.event.*;
import com.kingdee.bos.ctrl.extendcontrols.*;
import com.kingdee.bos.ctrl.kdf.util.render.*;
import com.kingdee.bos.ui.face.IItemAction;
import com.kingdee.eas.framework.batchHandler.RequestContext;
import com.kingdee.bos.ui.util.IUIActionPostman;
import com.kingdee.bos.appframework.client.servicebinding.ActionProxyFactory;
import com.kingdee.bos.appframework.uistatemanage.ActionStateConst;
import com.kingdee.bos.appframework.validator.ValidateHelper;
import com.kingdee.bos.appframework.uip.UINavigator;


/**
 * output class name
 */
public abstract class AbstractInterviewresultsEditUI extends com.kingdee.eas.hr.base.client.HRBillEditUI
{
    private static final Logger logger = CoreUIObject.getLogger(AbstractInterviewresultsEditUI.class);
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contDescription;
    protected com.kingdee.bos.ctrl.kdf.table.KDTable kdtEntrys;
	protected com.kingdee.eas.framework.client.multiDetail.DetailPanel kdtEntrys_detailPanel = null;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplier;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contAdminOrg;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplyDate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contBillState;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contInterviewPersonName;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contInterLink;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplyOrg;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contRanking;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contRatingSocre;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contpassRate;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contApplyJob;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contIdNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contInterviewPersonNumber;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contcreateYear;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contfinalRating;
    protected com.kingdee.bos.ctrl.swing.KDLabelContainer contfinalRanking;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtDescription;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtApplier;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtAdminOrg;
    protected com.kingdee.bos.ctrl.swing.KDDatePicker dpApplyDate;
    protected com.kingdee.bos.ctrl.swing.KDComboBox cbBillState;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtInterviewPersonName;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtInterLink;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtApplyOrg;
    protected com.kingdee.bos.ctrl.swing.KDFormattedTextField txtRanking;
    protected com.kingdee.bos.ctrl.swing.KDFormattedTextField txtRatingSocre;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtpassRate;
    protected com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox prmtApplyJob;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtIdNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtInterviewPersonNumber;
    protected com.kingdee.bos.ctrl.swing.KDTextField txtcreateYear;
    protected com.kingdee.bos.ctrl.swing.KDFormattedTextField txtfinalRating;
    protected com.kingdee.bos.ctrl.swing.KDFormattedTextField txtfinalRanking;
    protected com.kingdee.eas.custom.recruitment.InterviewresultsInfo editData = null;
    /**
     * output class constructor
     */
    public AbstractInterviewresultsEditUI() throws Exception
    {
        super();
        this.defaultObjectName = "editData";
        jbInit();
        
        initUIP();
    }

    /**
     * output jbInit method
     */
    private void jbInit() throws Exception
    {
        this.resHelper = new ResourceBundleHelper(AbstractInterviewresultsEditUI.class.getName());
        this.setUITitle(resHelper.getString("this.title"));
        //actionSubmit
        String _tempStr = null;
        actionSubmit.setEnabled(true);
        actionSubmit.setDaemonRun(false);

        actionSubmit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        _tempStr = resHelper.getString("ActionSubmit.SHORT_DESCRIPTION");
        actionSubmit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.LONG_DESCRIPTION");
        actionSubmit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionSubmit.NAME");
        actionSubmit.putValue(ItemAction.NAME, _tempStr);
        this.actionSubmit.setBindWorkFlow(true);
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionSubmit.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrint
        actionPrint.setEnabled(true);
        actionPrint.setDaemonRun(false);

        actionPrint.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl P"));
        _tempStr = resHelper.getString("ActionPrint.SHORT_DESCRIPTION");
        actionPrint.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.LONG_DESCRIPTION");
        actionPrint.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrint.NAME");
        actionPrint.putValue(ItemAction.NAME, _tempStr);
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrint.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionPrintPreview
        actionPrintPreview.setEnabled(true);
        actionPrintPreview.setDaemonRun(false);

        actionPrintPreview.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl P"));
        _tempStr = resHelper.getString("ActionPrintPreview.SHORT_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.LONG_DESCRIPTION");
        actionPrintPreview.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionPrintPreview.NAME");
        actionPrintPreview.putValue(ItemAction.NAME, _tempStr);
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.PermissionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.NetFunctionService());
         this.actionPrintPreview.addService(new com.kingdee.eas.framework.client.service.UserMonitorService());
        //actionAudit
        actionAudit.setEnabled(true);
        actionAudit.setDaemonRun(false);

        actionAudit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl A"));
        _tempStr = resHelper.getString("ActionAudit.SHORT_DESCRIPTION");
        actionAudit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionAudit.LONG_DESCRIPTION");
        actionAudit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionAudit.NAME");
        actionAudit.putValue(ItemAction.NAME, _tempStr);
         this.actionAudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        //actionUnaudit
        actionUnaudit.setEnabled(true);
        actionUnaudit.setDaemonRun(false);

        actionUnaudit.putValue(ItemAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl U"));
        _tempStr = resHelper.getString("ActionUnaudit.SHORT_DESCRIPTION");
        actionUnaudit.putValue(ItemAction.SHORT_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionUnaudit.LONG_DESCRIPTION");
        actionUnaudit.putValue(ItemAction.LONG_DESCRIPTION, _tempStr);
        _tempStr = resHelper.getString("ActionUnaudit.NAME");
        actionUnaudit.putValue(ItemAction.NAME, _tempStr);
         this.actionUnaudit.addService(new com.kingdee.eas.framework.client.service.PermissionService());
        this.contNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contDescription = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.kdtEntrys = new com.kingdee.bos.ctrl.kdf.table.KDTable();
        this.contApplier = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contAdminOrg = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contApplyDate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contBillState = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contInterviewPersonName = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contInterLink = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contApplyOrg = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contRanking = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contRatingSocre = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contpassRate = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contApplyJob = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contIdNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contInterviewPersonNumber = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contcreateYear = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contfinalRating = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.contfinalRanking = new com.kingdee.bos.ctrl.swing.KDLabelContainer();
        this.txtNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtDescription = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtApplier = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtAdminOrg = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.dpApplyDate = new com.kingdee.bos.ctrl.swing.KDDatePicker();
        this.cbBillState = new com.kingdee.bos.ctrl.swing.KDComboBox();
        this.txtInterviewPersonName = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtInterLink = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtApplyOrg = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtRanking = new com.kingdee.bos.ctrl.swing.KDFormattedTextField();
        this.txtRatingSocre = new com.kingdee.bos.ctrl.swing.KDFormattedTextField();
        this.txtpassRate = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.prmtApplyJob = new com.kingdee.bos.ctrl.extendcontrols.KDBizPromptBox();
        this.txtIdNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtInterviewPersonNumber = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtcreateYear = new com.kingdee.bos.ctrl.swing.KDTextField();
        this.txtfinalRating = new com.kingdee.bos.ctrl.swing.KDFormattedTextField();
        this.txtfinalRanking = new com.kingdee.bos.ctrl.swing.KDFormattedTextField();
        this.contNumber.setName("contNumber");
        this.contDescription.setName("contDescription");
        this.kdtEntrys.setName("kdtEntrys");
        this.contApplier.setName("contApplier");
        this.contAdminOrg.setName("contAdminOrg");
        this.contApplyDate.setName("contApplyDate");
        this.contBillState.setName("contBillState");
        this.contInterviewPersonName.setName("contInterviewPersonName");
        this.contInterLink.setName("contInterLink");
        this.contApplyOrg.setName("contApplyOrg");
        this.contRanking.setName("contRanking");
        this.contRatingSocre.setName("contRatingSocre");
        this.contpassRate.setName("contpassRate");
        this.contApplyJob.setName("contApplyJob");
        this.contIdNumber.setName("contIdNumber");
        this.contInterviewPersonNumber.setName("contInterviewPersonNumber");
        this.contcreateYear.setName("contcreateYear");
        this.contfinalRating.setName("contfinalRating");
        this.contfinalRanking.setName("contfinalRanking");
        this.txtNumber.setName("txtNumber");
        this.txtDescription.setName("txtDescription");
        this.txtApplier.setName("txtApplier");
        this.prmtAdminOrg.setName("prmtAdminOrg");
        this.dpApplyDate.setName("dpApplyDate");
        this.cbBillState.setName("cbBillState");
        this.txtInterviewPersonName.setName("txtInterviewPersonName");
        this.txtInterLink.setName("txtInterLink");
        this.prmtApplyOrg.setName("prmtApplyOrg");
        this.txtRanking.setName("txtRanking");
        this.txtRatingSocre.setName("txtRatingSocre");
        this.txtpassRate.setName("txtpassRate");
        this.prmtApplyJob.setName("prmtApplyJob");
        this.txtIdNumber.setName("txtIdNumber");
        this.txtInterviewPersonNumber.setName("txtInterviewPersonNumber");
        this.txtcreateYear.setName("txtcreateYear");
        this.txtfinalRating.setName("txtfinalRating");
        this.txtfinalRanking.setName("txtfinalRanking");
        // CoreUI		
        this.btnPrint.setVisible(false);		
        this.btnPrintPreview.setVisible(false);		
        this.menuItemPrint.setVisible(false);		
        this.menuItemPrintPreview.setVisible(false);		
        this.btnTraceUp.setVisible(false);		
        this.btnTraceDown.setVisible(false);		
        this.btnCreateFrom.setVisible(false);		
        this.btnAddLine.setVisible(false);		
        this.btnInsertLine.setVisible(false);		
        this.btnRemoveLine.setVisible(false);		
        this.btnAuditResult.setVisible(false);		
        this.separator1.setVisible(false);		
        this.menuItemCreateFrom.setVisible(false);		
        this.menuItemCopyFrom.setVisible(false);		
        this.separator3.setVisible(false);		
        this.menuItemTraceUp.setVisible(false);		
        this.menuItemTraceDown.setVisible(false);		
        this.menuItemAddLine.setVisible(false);		
        this.menuItemInsertLine.setVisible(false);		
        this.menuItemRemoveLine.setVisible(false);		
        this.menuItemViewSubmitProccess.setVisible(false);		
        this.menuItemViewDoProccess.setVisible(false);		
        this.menuItemAuditResult.setVisible(false);		
        this.contHROrg.setBoundLabelText(resHelper.getString("contHROrg.boundLabelText"));		
        this.contHROrg.setBoundLabelLength(90);		
        this.contHROrg.setBoundLabelUnderline(true);		
        this.contHROrg.setBoundLabelAlignment(7);		
        this.contHROrg.setVisible(true);		
        this.contApproveType.setBoundLabelText(resHelper.getString("contApproveType.boundLabelText"));		
        this.contApproveType.setBoundLabelLength(90);		
        this.contApproveType.setBoundLabelUnderline(true);		
        this.contApproveType.setBoundLabelAlignment(7);		
        this.contApproveType.setVisible(true);		
        this.prmtHROrg.setRequired(true);		
        this.prmtHROrg.setEnabled(true);		
        this.cbApproveType.setRequired(true);		
        this.cbApproveType.setEnabled(true);
        this.btnAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.btnAudit.setText(resHelper.getString("btnAudit.text"));		
        this.btnAudit.setToolTipText(resHelper.getString("btnAudit.toolTipText"));		
        this.btnAudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_audit"));
        this.btnUnaudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnaudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.btnUnaudit.setText(resHelper.getString("btnUnaudit.text"));		
        this.btnUnaudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_unaudit"));		
        this.btnUnaudit.setToolTipText(resHelper.getString("btnUnaudit.toolTipText"));		
        this.btnUnaudit.setVisible(false);
        this.menuItemAudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionAudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.menuItemAudit.setText(resHelper.getString("menuItemAudit.text"));		
        this.menuItemAudit.setToolTipText(resHelper.getString("menuItemAudit.toolTipText"));		
        this.menuItemAudit.setMnemonic(65);		
        this.menuItemAudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_audit"));
        this.menuItemUnaudit.setAction((IItemAction)ActionProxyFactory.getProxy(actionUnaudit, new Class[] { IItemAction.class }, getServiceContext()));		
        this.menuItemUnaudit.setText(resHelper.getString("menuItemUnaudit.text"));		
        this.menuItemUnaudit.setToolTipText(resHelper.getString("menuItemUnaudit.toolTipText"));		
        this.menuItemUnaudit.setMnemonic(85);		
        this.menuItemUnaudit.setIcon(com.kingdee.eas.util.client.EASResource.getIcon("imgTbtn_unaudit"));		
        this.menuItemUnaudit.setVisible(false);
        // contNumber		
        this.contNumber.setBoundLabelText(resHelper.getString("contNumber.boundLabelText"));		
        this.contNumber.setBoundLabelLength(90);		
        this.contNumber.setBoundLabelUnderline(true);		
        this.contNumber.setBoundLabelAlignment(7);		
        this.contNumber.setVisible(true);
        // contDescription		
        this.contDescription.setBoundLabelText(resHelper.getString("contDescription.boundLabelText"));		
        this.contDescription.setBoundLabelLength(90);		
        this.contDescription.setBoundLabelUnderline(true);		
        this.contDescription.setBoundLabelAlignment(7);		
        this.contDescription.setVisible(true);
        // kdtEntrys
		String kdtEntrysStrXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DocRoot xmlns:c=\"http://www.kingdee.com/Common\" xmlns:f=\"http://www.kingdee.com/Form\" xmlns:t=\"http://www.kingdee.com/Table\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.kingdee.com/KDF KDFSchema.xsd\" version=\"0.0\"><Styles><c:Style id=\"sCol0\"><c:Protection hidden=\"true\" /></c:Style><c:Style id=\"sCol1\"><c:Protection locked=\"true\" hidden=\"true\" /></c:Style><c:Style id=\"sCol2\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol3\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol4\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol5\"><c:Protection locked=\"true\" /></c:Style><c:Style id=\"sCol6\"><c:NumberFormat>yyyy-mm-dd</c:NumberFormat></c:Style><c:Style id=\"sCol9\"><c:NumberFormat>&amp;int</c:NumberFormat></c:Style></Styles><Table id=\"KDTable\"><t:Sheet name=\"sheet1\"><t:Table t:selectMode=\"15\" t:mergeMode=\"0\" t:dataRequestMode=\"0\" t:pageRowCount=\"100\"><t:ColumnGroup><t:Column t:key=\"id\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol0\" /><t:Column t:key=\"person\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"2\" t:styleID=\"sCol1\" /><t:Column t:key=\"empNumber\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"3\" t:styleID=\"sCol2\" /><t:Column t:key=\"empName\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"4\" t:styleID=\"sCol3\" /><t:Column t:key=\"position\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"9\" t:styleID=\"sCol4\" /><t:Column t:key=\"adminOrg\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"10\" t:styleID=\"sCol5\" /><t:Column t:key=\"bizDate\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"true\" t:index=\"14\" t:styleID=\"sCol6\" /><t:Column t:key=\"description\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"15\" /><t:Column t:key=\"isThough\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" /><t:Column t:key=\"InterviewScore\" t:width=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\" t:moveable=\"true\" t:group=\"false\" t:required=\"false\" t:index=\"-1\" t:styleID=\"sCol9\" /></t:ColumnGroup><t:Head><t:Row t:name=\"header\" t:height=\"-1\" t:mergeable=\"true\" t:resizeable=\"true\"><t:Cell>$Resource{id}</t:Cell><t:Cell>$Resource{person}</t:Cell><t:Cell>$Resource{empNumber}</t:Cell><t:Cell>$Resource{empName}</t:Cell><t:Cell>$Resource{position}</t:Cell><t:Cell>$Resource{adminOrg}</t:Cell><t:Cell>$Resource{bizDate}</t:Cell><t:Cell>$Resource{description}</t:Cell><t:Cell>$Resource{isThough}</t:Cell><t:Cell>$Resource{InterviewScore}</t:Cell></t:Row></t:Head></t:Table><t:SheetOptions><t:MergeBlocks><t:Head /></t:MergeBlocks></t:SheetOptions></t:Sheet></Table></DocRoot>";
		
        this.kdtEntrys.setFormatXml(resHelper.translateString("kdtEntrys",kdtEntrysStrXML));
        this.kdtEntrys.addKDTEditListener(new com.kingdee.bos.ctrl.kdf.table.event.KDTEditAdapter() {
            public void editStopping(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtEntrys_editStopping(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
            public void editStopped(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) {
                try {
                    kdtEntrys_editStopped(e);
                } catch(Exception exc) {
                    handUIException(exc);
                }
            }
        });

                this.kdtEntrys.putBindContents("editData",new String[] {"id","person","person.number","person.name","position","adminOrg","bizDate","description","isThough","InterviewScore"});


        this.kdtEntrys.checkParsed();
        final KDBizPromptBox kdtEntrys_person_PromptBox = new KDBizPromptBox();
        kdtEntrys_person_PromptBox.setQueryInfo("com.kingdee.eas.basedata.person.app.PersonQuery");
        kdtEntrys_person_PromptBox.setVisible(true);
        kdtEntrys_person_PromptBox.setEditable(true);
        kdtEntrys_person_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_person_PromptBox.setEditFormat("$number$");
        kdtEntrys_person_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_person_CellEditor = new KDTDefaultCellEditor(kdtEntrys_person_PromptBox);
        this.kdtEntrys.getColumn("person").setEditor(kdtEntrys_person_CellEditor);
        ObjectValueRender kdtEntrys_person_OVR = new ObjectValueRender();
        kdtEntrys_person_OVR.setFormat(new BizDataFormat("$gender$"));
        this.kdtEntrys.getColumn("person").setRenderer(kdtEntrys_person_OVR);
        KDTextField kdtEntrys_empName_TextField = new KDTextField();
        kdtEntrys_empName_TextField.setName("kdtEntrys_empName_TextField");
        kdtEntrys_empName_TextField.setMaxLength(80);
        KDTDefaultCellEditor kdtEntrys_empName_CellEditor = new KDTDefaultCellEditor(kdtEntrys_empName_TextField);
        this.kdtEntrys.getColumn("empName").setEditor(kdtEntrys_empName_CellEditor);
        final KDBizPromptBox kdtEntrys_position_PromptBox = new KDBizPromptBox();
        kdtEntrys_position_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.PositionQuery");
        kdtEntrys_position_PromptBox.setVisible(true);
        kdtEntrys_position_PromptBox.setEditable(true);
        kdtEntrys_position_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_position_PromptBox.setEditFormat("$number$");
        kdtEntrys_position_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_position_CellEditor = new KDTDefaultCellEditor(kdtEntrys_position_PromptBox);
        this.kdtEntrys.getColumn("position").setEditor(kdtEntrys_position_CellEditor);
        ObjectValueRender kdtEntrys_position_OVR = new ObjectValueRender();
        kdtEntrys_position_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("position").setRenderer(kdtEntrys_position_OVR);
        final KDBizPromptBox kdtEntrys_adminOrg_PromptBox = new KDBizPromptBox();
        kdtEntrys_adminOrg_PromptBox.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");
        kdtEntrys_adminOrg_PromptBox.setVisible(true);
        kdtEntrys_adminOrg_PromptBox.setEditable(true);
        kdtEntrys_adminOrg_PromptBox.setDisplayFormat("$number$");
        kdtEntrys_adminOrg_PromptBox.setEditFormat("$number$");
        kdtEntrys_adminOrg_PromptBox.setCommitFormat("$number$");
        KDTDefaultCellEditor kdtEntrys_adminOrg_CellEditor = new KDTDefaultCellEditor(kdtEntrys_adminOrg_PromptBox);
        this.kdtEntrys.getColumn("adminOrg").setEditor(kdtEntrys_adminOrg_CellEditor);
        ObjectValueRender kdtEntrys_adminOrg_OVR = new ObjectValueRender();
        kdtEntrys_adminOrg_OVR.setFormat(new BizDataFormat("$name$"));
        this.kdtEntrys.getColumn("adminOrg").setRenderer(kdtEntrys_adminOrg_OVR);
        KDDatePicker kdtEntrys_bizDate_DatePicker = new KDDatePicker();
        kdtEntrys_bizDate_DatePicker.setName("kdtEntrys_bizDate_DatePicker");
        kdtEntrys_bizDate_DatePicker.setVisible(true);
        kdtEntrys_bizDate_DatePicker.setEditable(true);
        KDTDefaultCellEditor kdtEntrys_bizDate_CellEditor = new KDTDefaultCellEditor(kdtEntrys_bizDate_DatePicker);
        this.kdtEntrys.getColumn("bizDate").setEditor(kdtEntrys_bizDate_CellEditor);
        KDTextField kdtEntrys_description_TextField = new KDTextField();
        kdtEntrys_description_TextField.setName("kdtEntrys_description_TextField");
        kdtEntrys_description_TextField.setMaxLength(200);
        KDTDefaultCellEditor kdtEntrys_description_CellEditor = new KDTDefaultCellEditor(kdtEntrys_description_TextField);
        this.kdtEntrys.getColumn("description").setEditor(kdtEntrys_description_CellEditor);
        KDComboBox kdtEntrys_isThough_ComboBox = new KDComboBox();
        kdtEntrys_isThough_ComboBox.setName("kdtEntrys_isThough_ComboBox");
        kdtEntrys_isThough_ComboBox.setVisible(true);
        kdtEntrys_isThough_ComboBox.addItems(EnumUtils.getEnumList("com.kingdee.eas.hr.train.TrainRecordIsPassedEnum").toArray());
        KDTDefaultCellEditor kdtEntrys_isThough_CellEditor = new KDTDefaultCellEditor(kdtEntrys_isThough_ComboBox);
        this.kdtEntrys.getColumn("isThough").setEditor(kdtEntrys_isThough_CellEditor);
        KDFormattedTextField kdtEntrys_InterviewScore_TextField = new KDFormattedTextField();
        kdtEntrys_InterviewScore_TextField.setName("kdtEntrys_InterviewScore_TextField");
        kdtEntrys_InterviewScore_TextField.setVisible(true);
        kdtEntrys_InterviewScore_TextField.setEditable(true);
        kdtEntrys_InterviewScore_TextField.setHorizontalAlignment(2);
        kdtEntrys_InterviewScore_TextField.setDataType(0);
        KDTDefaultCellEditor kdtEntrys_InterviewScore_CellEditor = new KDTDefaultCellEditor(kdtEntrys_InterviewScore_TextField);
        this.kdtEntrys.getColumn("InterviewScore").setEditor(kdtEntrys_InterviewScore_CellEditor);
        // contApplier		
        this.contApplier.setBoundLabelText(resHelper.getString("contApplier.boundLabelText"));		
        this.contApplier.setBoundLabelLength(90);		
        this.contApplier.setBoundLabelUnderline(true);		
        this.contApplier.setBoundLabelAlignment(7);		
        this.contApplier.setVisible(true);
        // contAdminOrg		
        this.contAdminOrg.setBoundLabelText(resHelper.getString("contAdminOrg.boundLabelText"));		
        this.contAdminOrg.setBoundLabelLength(90);		
        this.contAdminOrg.setBoundLabelUnderline(true);		
        this.contAdminOrg.setBoundLabelAlignment(7);		
        this.contAdminOrg.setVisible(true);
        // contApplyDate		
        this.contApplyDate.setBoundLabelText(resHelper.getString("contApplyDate.boundLabelText"));		
        this.contApplyDate.setBoundLabelLength(90);		
        this.contApplyDate.setBoundLabelUnderline(true);		
        this.contApplyDate.setBoundLabelAlignment(7);		
        this.contApplyDate.setVisible(true);
        // contBillState		
        this.contBillState.setBoundLabelText(resHelper.getString("contBillState.boundLabelText"));		
        this.contBillState.setBoundLabelLength(90);		
        this.contBillState.setBoundLabelUnderline(true);		
        this.contBillState.setBoundLabelAlignment(7);		
        this.contBillState.setVisible(false);
        // contInterviewPersonName		
        this.contInterviewPersonName.setBoundLabelText(resHelper.getString("contInterviewPersonName.boundLabelText"));		
        this.contInterviewPersonName.setBoundLabelLength(100);		
        this.contInterviewPersonName.setBoundLabelUnderline(true);		
        this.contInterviewPersonName.setVisible(true);
        // contInterLink		
        this.contInterLink.setBoundLabelText(resHelper.getString("contInterLink.boundLabelText"));		
        this.contInterLink.setBoundLabelLength(100);		
        this.contInterLink.setBoundLabelUnderline(true);		
        this.contInterLink.setVisible(true);
        // contApplyOrg		
        this.contApplyOrg.setBoundLabelText(resHelper.getString("contApplyOrg.boundLabelText"));		
        this.contApplyOrg.setBoundLabelLength(100);		
        this.contApplyOrg.setBoundLabelUnderline(true);		
        this.contApplyOrg.setVisible(true);
        // contRanking		
        this.contRanking.setBoundLabelText(resHelper.getString("contRanking.boundLabelText"));		
        this.contRanking.setBoundLabelLength(100);		
        this.contRanking.setBoundLabelUnderline(true);		
        this.contRanking.setVisible(true);
        // contRatingSocre		
        this.contRatingSocre.setBoundLabelText(resHelper.getString("contRatingSocre.boundLabelText"));		
        this.contRatingSocre.setBoundLabelLength(100);		
        this.contRatingSocre.setBoundLabelUnderline(true);		
        this.contRatingSocre.setVisible(true);
        // contpassRate		
        this.contpassRate.setBoundLabelText(resHelper.getString("contpassRate.boundLabelText"));		
        this.contpassRate.setBoundLabelLength(100);		
        this.contpassRate.setBoundLabelUnderline(true);		
        this.contpassRate.setVisible(true);
        // contApplyJob		
        this.contApplyJob.setBoundLabelText(resHelper.getString("contApplyJob.boundLabelText"));		
        this.contApplyJob.setBoundLabelLength(100);		
        this.contApplyJob.setBoundLabelUnderline(true);		
        this.contApplyJob.setVisible(true);
        // contIdNumber		
        this.contIdNumber.setBoundLabelText(resHelper.getString("contIdNumber.boundLabelText"));		
        this.contIdNumber.setBoundLabelLength(100);		
        this.contIdNumber.setBoundLabelUnderline(true);		
        this.contIdNumber.setVisible(true);
        // contInterviewPersonNumber		
        this.contInterviewPersonNumber.setBoundLabelText(resHelper.getString("contInterviewPersonNumber.boundLabelText"));		
        this.contInterviewPersonNumber.setBoundLabelLength(100);		
        this.contInterviewPersonNumber.setBoundLabelUnderline(true);		
        this.contInterviewPersonNumber.setVisible(true);
        // contcreateYear		
        this.contcreateYear.setBoundLabelText(resHelper.getString("contcreateYear.boundLabelText"));		
        this.contcreateYear.setBoundLabelLength(100);		
        this.contcreateYear.setBoundLabelUnderline(true);		
        this.contcreateYear.setVisible(true);
        // contfinalRating		
        this.contfinalRating.setBoundLabelText(resHelper.getString("contfinalRating.boundLabelText"));		
        this.contfinalRating.setBoundLabelLength(100);		
        this.contfinalRating.setBoundLabelUnderline(true);		
        this.contfinalRating.setVisible(true);
        // contfinalRanking		
        this.contfinalRanking.setBoundLabelText(resHelper.getString("contfinalRanking.boundLabelText"));		
        this.contfinalRanking.setBoundLabelLength(100);		
        this.contfinalRanking.setBoundLabelUnderline(true);		
        this.contfinalRanking.setVisible(true);
        // txtNumber		
        this.txtNumber.setMaxLength(80);		
        this.txtNumber.setRequired(true);		
        this.txtNumber.setEnabled(true);		
        this.txtNumber.setHorizontalAlignment(2);
        // txtDescription		
        this.txtDescription.setMaxLength(200);		
        this.txtDescription.setEnabled(true);		
        this.txtDescription.setHorizontalAlignment(2);		
        this.txtDescription.setRequired(false);
        // txtApplier		
        this.txtApplier.setEnabled(false);		
        this.txtApplier.setMaxLength(80);		
        this.txtApplier.setVisible(true);		
        this.txtApplier.setHorizontalAlignment(2);		
        this.txtApplier.setRequired(false);
        // prmtAdminOrg		
        this.prmtAdminOrg.setRequired(true);		
        this.prmtAdminOrg.setEnabled(true);
        // dpApplyDate		
        this.dpApplyDate.setEnabled(false);		
        this.dpApplyDate.setRequired(false);
        // cbBillState		
        this.cbBillState.addItems(EnumUtils.getEnumList("com.kingdee.eas.hr.base.HRBillStateEnum").toArray());		
        this.cbBillState.setEnabled(false);		
        this.cbBillState.setVisible(false);		
        this.cbBillState.setRequired(false);
        // txtInterviewPersonName		
        this.txtInterviewPersonName.setHorizontalAlignment(2);		
        this.txtInterviewPersonName.setMaxLength(100);		
        this.txtInterviewPersonName.setRequired(false);
        // txtInterLink		
        this.txtInterLink.setHorizontalAlignment(2);		
        this.txtInterLink.setMaxLength(100);		
        this.txtInterLink.setRequired(false);
        // prmtApplyOrg		
        this.prmtApplyOrg.setQueryInfo("com.kingdee.eas.basedata.org.app.AdminItemQuery");		
        this.prmtApplyOrg.setEditable(true);		
        this.prmtApplyOrg.setDisplayFormat("$name$");		
        this.prmtApplyOrg.setEditFormat("$number$");		
        this.prmtApplyOrg.setCommitFormat("$number$");		
        this.prmtApplyOrg.setRequired(false);
        // txtRanking		
        this.txtRanking.setHorizontalAlignment(2);		
        this.txtRanking.setDataType(0);		
        this.txtRanking.setSupportedEmpty(true);		
        this.txtRanking.setRequired(false);
        // txtRatingSocre		
        this.txtRatingSocre.setHorizontalAlignment(2);		
        this.txtRatingSocre.setDataType(0);		
        this.txtRatingSocre.setSupportedEmpty(true);		
        this.txtRatingSocre.setRequired(false);
        // txtpassRate		
        this.txtpassRate.setHorizontalAlignment(2);		
        this.txtpassRate.setMaxLength(100);		
        this.txtpassRate.setRequired(false);
        // prmtApplyJob		
        this.prmtApplyJob.setQueryInfo("com.kingdee.eas.hr.org.app.PositionHRListQuery");		
        this.prmtApplyJob.setEditable(true);		
        this.prmtApplyJob.setDisplayFormat("$effectDate$");		
        this.prmtApplyJob.setEditFormat("$number$");		
        this.prmtApplyJob.setCommitFormat("$number$");		
        this.prmtApplyJob.setRequired(false);
        // txtIdNumber		
        this.txtIdNumber.setHorizontalAlignment(2);		
        this.txtIdNumber.setMaxLength(100);		
        this.txtIdNumber.setRequired(false);
        // txtInterviewPersonNumber		
        this.txtInterviewPersonNumber.setHorizontalAlignment(2);		
        this.txtInterviewPersonNumber.setMaxLength(100);		
        this.txtInterviewPersonNumber.setRequired(false);
        // txtcreateYear		
        this.txtcreateYear.setHorizontalAlignment(2);		
        this.txtcreateYear.setMaxLength(100);		
        this.txtcreateYear.setRequired(false);
        // txtfinalRating		
        this.txtfinalRating.setHorizontalAlignment(2);		
        this.txtfinalRating.setDataType(0);		
        this.txtfinalRating.setSupportedEmpty(true);		
        this.txtfinalRating.setRequired(false);
        // txtfinalRanking		
        this.txtfinalRanking.setHorizontalAlignment(2);		
        this.txtfinalRanking.setDataType(0);		
        this.txtfinalRanking.setSupportedEmpty(true);		
        this.txtfinalRanking.setRequired(false);
        this.setFocusTraversalPolicy(new com.kingdee.bos.ui.UIFocusTraversalPolicy(new java.awt.Component[] {txtApplier,txtNumber,txtInterviewPersonNumber,txtInterviewPersonName,txtInterLink,txtRanking,txtRatingSocre,txtpassRate,prmtApplyOrg,prmtApplyJob,txtcreateYear,txtDescription,cbBillState,prmtAdminOrg,dpApplyDate,txtIdNumber,cbApproveType,prmtHROrg,txtfinalRating,txtfinalRanking,kdtEntrys}));
        this.setFocusCycleRoot(true);
		//Register control's property binding
		registerBindings();
		registerUIState();


    }

	public com.kingdee.bos.ctrl.swing.KDToolBar[] getUIMultiToolBar(){
		java.util.List list = new java.util.ArrayList();
		com.kingdee.bos.ctrl.swing.KDToolBar[] bars = super.getUIMultiToolBar();
		if (bars != null) {
			list.addAll(java.util.Arrays.asList(bars));
		}
		return (com.kingdee.bos.ctrl.swing.KDToolBar[])list.toArray(new com.kingdee.bos.ctrl.swing.KDToolBar[list.size()]);
	}




    /**
     * output initUIContentLayout method
     */
    public void initUIContentLayout()
    {
        this.setBounds(new Rectangle(0, 0, 1013, 1273));
        this.setLayout(new KDLayout());
        this.putClientProperty("OriginalBounds", new Rectangle(0, 0, 1013, 1273));
        contHROrg.setBounds(new Rectangle(672, 58, 270, 19));
        this.add(contHROrg, new KDLayout.Constraints(672, 58, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApproveType.setBounds(new Rectangle(672, 106, 270, 19));
        this.add(contApproveType, new KDLayout.Constraints(672, 106, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contNumber.setBounds(new Rectangle(672, 82, 270, 19));
        this.add(contNumber, new KDLayout.Constraints(672, 82, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contDescription.setBounds(new Rectangle(672, 130, 270, 19));
        this.add(contDescription, new KDLayout.Constraints(672, 130, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        kdtEntrys.setBounds(new Rectangle(10, 178, 993, 543));
        kdtEntrys_detailPanel = (com.kingdee.eas.framework.client.multiDetail.DetailPanel)com.kingdee.eas.framework.client.multiDetail.HMDUtils.buildDetail(this,dataBinder,kdtEntrys,new com.kingdee.eas.custom.recruitment.InterviewresultsEntryInfo(),null,false);
        this.add(kdtEntrys_detailPanel, new KDLayout.Constraints(10, 178, 993, 543, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
		kdtEntrys_detailPanel.addAddListener(new com.kingdee.eas.framework.client.multiDetail.IDetailPanelListener() {
			public void beforeEvent(com.kingdee.eas.framework.client.multiDetail.DetailPanelEvent event) throws Exception {
				IObjectValue vo = event.getObjectValue();
vo.put("isThough",new Integer(10));
			}
			public void afterEvent(com.kingdee.eas.framework.client.multiDetail.DetailPanelEvent event) throws Exception {
			}
		});
        contApplier.setBounds(new Rectangle(341, 154, 270, 19));
        this.add(contApplier, new KDLayout.Constraints(341, 154, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contAdminOrg.setBounds(new Rectangle(341, 130, 270, 19));
        this.add(contAdminOrg, new KDLayout.Constraints(341, 130, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApplyDate.setBounds(new Rectangle(10, 154, 270, 19));
        this.add(contApplyDate, new KDLayout.Constraints(10, 154, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contBillState.setBounds(new Rectangle(341, 106, 270, 19));
        this.add(contBillState, new KDLayout.Constraints(341, 106, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contInterviewPersonName.setBounds(new Rectangle(672, 34, 270, 19));
        this.add(contInterviewPersonName, new KDLayout.Constraints(672, 34, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contInterLink.setBounds(new Rectangle(10, 82, 270, 19));
        this.add(contInterLink, new KDLayout.Constraints(10, 82, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApplyOrg.setBounds(new Rectangle(341, 82, 270, 19));
        this.add(contApplyOrg, new KDLayout.Constraints(341, 82, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contRanking.setBounds(new Rectangle(10, 106, 270, 19));
        this.add(contRanking, new KDLayout.Constraints(10, 106, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contRatingSocre.setBounds(new Rectangle(10, 130, 270, 19));
        this.add(contRatingSocre, new KDLayout.Constraints(10, 130, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contpassRate.setBounds(new Rectangle(10, 58, 270, 19));
        this.add(contpassRate, new KDLayout.Constraints(10, 58, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contApplyJob.setBounds(new Rectangle(341, 58, 270, 19));
        this.add(contApplyJob, new KDLayout.Constraints(341, 58, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contIdNumber.setBounds(new Rectangle(672, 10, 270, 19));
        this.add(contIdNumber, new KDLayout.Constraints(672, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contInterviewPersonNumber.setBounds(new Rectangle(10, 34, 270, 19));
        this.add(contInterviewPersonNumber, new KDLayout.Constraints(10, 34, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contcreateYear.setBounds(new Rectangle(341, 34, 270, 19));
        this.add(contcreateYear, new KDLayout.Constraints(341, 34, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contfinalRating.setBounds(new Rectangle(10, 10, 270, 19));
        this.add(contfinalRating, new KDLayout.Constraints(10, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        contfinalRanking.setBounds(new Rectangle(341, 10, 270, 19));
        this.add(contfinalRanking, new KDLayout.Constraints(341, 10, 270, 19, KDLayout.Constraints.ANCHOR_TOP | KDLayout.Constraints.ANCHOR_BOTTOM | KDLayout.Constraints.ANCHOR_LEFT | KDLayout.Constraints.ANCHOR_RIGHT));
        //contHROrg
        contHROrg.setBoundEditor(prmtHROrg);
        //contApproveType
        contApproveType.setBoundEditor(cbApproveType);
        //contNumber
        contNumber.setBoundEditor(txtNumber);
        //contDescription
        contDescription.setBoundEditor(txtDescription);
        //contApplier
        contApplier.setBoundEditor(txtApplier);
        //contAdminOrg
        contAdminOrg.setBoundEditor(prmtAdminOrg);
        //contApplyDate
        contApplyDate.setBoundEditor(dpApplyDate);
        //contBillState
        contBillState.setBoundEditor(cbBillState);
        //contInterviewPersonName
        contInterviewPersonName.setBoundEditor(txtInterviewPersonName);
        //contInterLink
        contInterLink.setBoundEditor(txtInterLink);
        //contApplyOrg
        contApplyOrg.setBoundEditor(prmtApplyOrg);
        //contRanking
        contRanking.setBoundEditor(txtRanking);
        //contRatingSocre
        contRatingSocre.setBoundEditor(txtRatingSocre);
        //contpassRate
        contpassRate.setBoundEditor(txtpassRate);
        //contApplyJob
        contApplyJob.setBoundEditor(prmtApplyJob);
        //contIdNumber
        contIdNumber.setBoundEditor(txtIdNumber);
        //contInterviewPersonNumber
        contInterviewPersonNumber.setBoundEditor(txtInterviewPersonNumber);
        //contcreateYear
        contcreateYear.setBoundEditor(txtcreateYear);
        //contfinalRating
        contfinalRating.setBoundEditor(txtfinalRating);
        //contfinalRanking
        contfinalRanking.setBoundEditor(txtfinalRanking);

    }


    /**
     * output initUIMenuBarLayout method
     */
    public void initUIMenuBarLayout()
    {
        this.menuBar.add(menuFile);
        this.menuBar.add(menuEdit);
        this.menuBar.add(MenuService);
        this.menuBar.add(menuView);
        this.menuBar.add(menuBiz);
        this.menuBar.add(menuTable1);
        this.menuBar.add(menuTool);
        this.menuBar.add(menuWorkflow);
        this.menuBar.add(menuHelp);
        //menuFile
        menuFile.add(menuItemAddNew);
        menuFile.add(kDSeparator1);
        menuFile.add(menuItemCloudFeed);
        menuFile.add(menuItemSave);
        menuFile.add(menuItemCloudScreen);
        menuFile.add(menuItemSubmit);
        menuFile.add(menuItemCloudShare);
        menuFile.add(menuSubmitOption);
        menuFile.add(kdSeparatorFWFile1);
        menuFile.add(rMenuItemSubmit);
        menuFile.add(rMenuItemSubmitAndAddNew);
        menuFile.add(rMenuItemSubmitAndPrint);
        menuFile.add(separatorFile1);
        menuFile.add(menuItemMapping);
        menuFile.add(MenuItemAttachment);
        menuFile.add(kDSeparator2);
        menuFile.add(menuItemPageSetup);
        menuFile.add(menuItemPrint);
        menuFile.add(menuItemPrintPreview);
        menuFile.add(kDSeparator6);
        menuFile.add(kDSeparator3);
        menuFile.add(menuItemSendMail);
        menuFile.add(menuItemExitCurrent);
        //menuSubmitOption
        menuSubmitOption.add(chkMenuItemSubmitAndAddNew);
        menuSubmitOption.add(chkMenuItemSubmitAndPrint);
        //menuEdit
        menuEdit.add(menuItemCopy);
        menuEdit.add(menuItemEdit);
        menuEdit.add(menuItemRemove);
        menuEdit.add(kDSeparator4);
        menuEdit.add(menuItemReset);
        menuEdit.add(separator1);
        menuEdit.add(menuItemCreateFrom);
        menuEdit.add(menuItemCreateTo);
        menuEdit.add(menuItemCopyFrom);
        menuEdit.add(menuItemColumnCopyAll);
        menuEdit.add(menuItemEnterToNextRow);
        menuEdit.add(menuItemColumnCopySelect);
        menuEdit.add(separatorEdit1);
        menuEdit.add(separator2);
        //MenuService
        MenuService.add(MenuItemKnowStore);
        MenuService.add(MenuItemAnwser);
        MenuService.add(SepratorService);
        MenuService.add(MenuItemRemoteAssist);
        //menuView
        menuView.add(menuItemFirst);
        menuView.add(menuItemPre);
        menuView.add(menuItemNext);
        menuView.add(menuItemLast);
        menuView.add(separator3);
        menuView.add(menuItemTraceUp);
        menuView.add(menuItemTraceDown);
        menuView.add(kDSeparator7);
        menuView.add(menuItemLocate);
        //menuBiz
        menuBiz.add(menuItemCancelCancel);
        menuBiz.add(menuItemCancel);
        menuBiz.add(MenuItemVoucher);
        menuBiz.add(menuItemDelVoucher);
        menuBiz.add(menuItemAudit);
        menuBiz.add(menuItemUnaudit);
        menuBiz.add(menuItemPerson);
        //menuTable1
        menuTable1.add(menuItemAddLine);
        menuTable1.add(menuItemCopyLine);
        menuTable1.add(menuItemInsertLine);
        menuTable1.add(menuItemRemoveLine);
        //menuTool
        menuTool.add(menuItemSendMessage);
        menuTool.add(menuItemMsgFormat);
        menuTool.add(menuItemCalculator);
        menuTool.add(menuItemToolBarCustom);
        //menuWorkflow
        menuWorkflow.add(menuItemStartWorkFlow);
        menuWorkflow.add(separatorWF1);
        menuWorkflow.add(menuItemViewSubmitProccess);
        menuWorkflow.add(menuItemViewDoProccess);
        menuWorkflow.add(MenuItemWFG);
        menuWorkflow.add(menuItemWorkFlowList);
        menuWorkflow.add(separatorWF2);
        menuWorkflow.add(menuItemMultiapprove);
        menuWorkflow.add(menuItemNextPerson);
        menuWorkflow.add(menuItemAuditResult);
        menuWorkflow.add(kDSeparator5);
        menuWorkflow.add(kDMenuItemSendMessage);
        //menuHelp
        menuHelp.add(menuItemHelp);
        menuHelp.add(kDSeparator12);
        menuHelp.add(menuItemRegPro);
        menuHelp.add(menuItemPersonalSite);
        menuHelp.add(helpseparatorDiv);
        menuHelp.add(menuitemProductval);
        menuHelp.add(kDSeparatorProduct);
        menuHelp.add(menuItemAbout);

    }

    /**
     * output initUIToolBarLayout method
     */
    public void initUIToolBarLayout()
    {
        this.toolBar.add(btnAddNew);
        this.toolBar.add(btnCloud);
        this.toolBar.add(btnEdit);
        this.toolBar.add(btnXunTong);
        this.toolBar.add(btnSave);
        this.toolBar.add(kDSeparatorCloud);
        this.toolBar.add(btnReset);
        this.toolBar.add(btnSubmit);
        this.toolBar.add(btnCopy);
        this.toolBar.add(btnRemove);
        this.toolBar.add(btnCancelCancel);
        this.toolBar.add(btnCancel);
        this.toolBar.add(btnAttachment);
        this.toolBar.add(separatorFW1);
        this.toolBar.add(btnPageSetup);
        this.toolBar.add(btnColumnCopyAll);
        this.toolBar.add(btnPrint);
        this.toolBar.add(btnColumnCopySelect);
        this.toolBar.add(btnPrintPreview);
        this.toolBar.add(separatorFW2);
        this.toolBar.add(btnFirst);
        this.toolBar.add(btnPre);
        this.toolBar.add(btnNext);
        this.toolBar.add(btnLast);
        this.toolBar.add(separatorFW3);
        this.toolBar.add(btnTraceUp);
        this.toolBar.add(btnTraceDown);
        this.toolBar.add(btnWorkFlowG);
        this.toolBar.add(btnSignature);
        this.toolBar.add(btnViewSignature);
        this.toolBar.add(separatorFW4);
        this.toolBar.add(btnNumberSign);
        this.toolBar.add(separatorFW7);
        this.toolBar.add(btnCreateFrom);
        this.toolBar.add(btnCopyFrom);
        this.toolBar.add(btnCreateTo);
        this.toolBar.add(separatorFW5);
        this.toolBar.add(separatorFW8);
        this.toolBar.add(btnAddLine);
        this.toolBar.add(btnCopyLine);
        this.toolBar.add(btnInsertLine);
        this.toolBar.add(btnRemoveLine);
        this.toolBar.add(separatorFW6);
        this.toolBar.add(separatorFW9);
        this.toolBar.add(btnVoucher);
        this.toolBar.add(btnDelVoucher);
        this.toolBar.add(btnAuditResult);
        this.toolBar.add(btnMultiapprove);
        this.toolBar.add(btnWFViewdoProccess);
        this.toolBar.add(btnWFViewSubmitProccess);
        this.toolBar.add(btnNextPerson);
        this.toolBar.add(btnAudit);
        this.toolBar.add(btnUnaudit);
        this.toolBar.add(btnPerson);


    }

	//Regiester control's property binding.
	private void registerBindings(){
		dataBinder.registerBinding("hrOrgUnit", com.kingdee.eas.basedata.org.HROrgUnitInfo.class, this.prmtHROrg, "data");
		dataBinder.registerBinding("approveType", com.kingdee.eas.hr.base.ApproveTypeEnum.class, this.cbApproveType, "selectedItem");
		dataBinder.registerBinding("entrys.id", com.kingdee.bos.util.BOSUuid.class, this.kdtEntrys, "id.text");
		dataBinder.registerBinding("entrys", com.kingdee.eas.custom.recruitment.InterviewresultsEntryInfo.class, this.kdtEntrys, "userObject");
		dataBinder.registerBinding("entrys.bizDate", java.util.Date.class, this.kdtEntrys, "bizDate.text");
		dataBinder.registerBinding("entrys.description", String.class, this.kdtEntrys, "description.text");
		dataBinder.registerBinding("entrys.person", com.kingdee.eas.basedata.person.PersonInfo.class, this.kdtEntrys, "person.text");
		dataBinder.registerBinding("entrys.person.number", String.class, this.kdtEntrys, "empNumber.text");
		dataBinder.registerBinding("entrys.person.name", String.class, this.kdtEntrys, "empName.text");
		dataBinder.registerBinding("entrys.position", com.kingdee.eas.basedata.org.PositionInfo.class, this.kdtEntrys, "position.text");
		dataBinder.registerBinding("entrys.adminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.kdtEntrys, "adminOrg.text");
		dataBinder.registerBinding("entrys.isThough", com.kingdee.util.enums.Enum.class, this.kdtEntrys, "isThough.text");
		dataBinder.registerBinding("entrys.InterviewScore", int.class, this.kdtEntrys, "InterviewScore.text");
		dataBinder.registerBinding("number", String.class, this.txtNumber, "text");
		dataBinder.registerBinding("description", String.class, this.txtDescription, "text");
		dataBinder.registerBinding("applier.name", String.class, this.txtApplier, "text");
		dataBinder.registerBinding("adminOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtAdminOrg, "data");
		dataBinder.registerBinding("applyDate", java.util.Date.class, this.dpApplyDate, "value");
		dataBinder.registerBinding("billState", com.kingdee.eas.hr.base.HRBillStateEnum.class, this.cbBillState, "selectedItem");
		dataBinder.registerBinding("InterviewPersonName", String.class, this.txtInterviewPersonName, "text");
		dataBinder.registerBinding("InterLink", String.class, this.txtInterLink, "text");
		dataBinder.registerBinding("ApplyOrg", com.kingdee.eas.basedata.org.AdminOrgUnitInfo.class, this.prmtApplyOrg, "data");
		dataBinder.registerBinding("Ranking", int.class, this.txtRanking, "value");
		dataBinder.registerBinding("RatingSocre", int.class, this.txtRatingSocre, "value");
		dataBinder.registerBinding("passRate", String.class, this.txtpassRate, "text");
		dataBinder.registerBinding("ApplyJob", com.kingdee.eas.basedata.org.PositionInfo.class, this.prmtApplyJob, "data");
		dataBinder.registerBinding("IdNumber", String.class, this.txtIdNumber, "text");
		dataBinder.registerBinding("InterviewPersonNumber", String.class, this.txtInterviewPersonNumber, "text");
		dataBinder.registerBinding("createYear", String.class, this.txtcreateYear, "text");
		dataBinder.registerBinding("finalRating", int.class, this.txtfinalRating, "value");
		dataBinder.registerBinding("finalRanking", int.class, this.txtfinalRanking, "value");		
	}
	//Regiester UI State
	private void registerUIState(){		
	}
	public String getUIHandlerClassName() {
	    return "com.kingdee.eas.custom.recruitment.app.InterviewresultsEditUIHandler";
	}
	public IUIActionPostman prepareInit() {
		IUIActionPostman clientHanlder = super.prepareInit();
		if (clientHanlder != null) {
			RequestContext request = new RequestContext();
    		request.setClassName(getUIHandlerClassName());
			clientHanlder.setRequestContext(request);
		}
		return clientHanlder;
    }
	
	public boolean isPrepareInit() {
    	return false;
    }
    protected void initUIP() {
        super.initUIP();
    }


    /**
     * output onShow method
     */
    public void onShow() throws Exception
    {
        super.onShow();
        this.txtApplier.requestFocusInWindow();
    }

	
	

    /**
     * output setDataObject method
     */
    public void setDataObject(IObjectValue dataObject)
    {
        IObjectValue ov = dataObject;        	    	
        super.setDataObject(ov);
        this.editData = (com.kingdee.eas.custom.recruitment.InterviewresultsInfo)ov;
    }
    protected void removeByPK(IObjectPK pk) throws Exception {
    	IObjectValue editData = this.editData;
    	super.removeByPK(pk);
    	recycleNumberByOrg(editData,"NONE",editData.getString("number"));
    }
    
    protected void recycleNumberByOrg(IObjectValue editData,String orgType,String number) {
        if (!StringUtils.isEmpty(number))
        {
            try {
            	String companyID = null;            
            	com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID =com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}				
				if (!StringUtils.isEmpty(companyID) && iCodingRuleManager.isExist(editData, companyID) && iCodingRuleManager.isUseIntermitNumber(editData, companyID)) {
					iCodingRuleManager.recycleNumber(editData,companyID,number);					
				}
            }
            catch (Exception e)
            {
                handUIException(e);
            }
        }
    }
    protected void setAutoNumberByOrg(String orgType) {
    	if (editData == null) return;
		if (editData.getNumber() == null) {
            try {
            	String companyID = null;
				if(!com.kingdee.util.StringUtils.isEmpty(orgType) && !"NONE".equalsIgnoreCase(orgType) && com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType))!=null) {
					companyID = com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit(com.kingdee.eas.basedata.org.OrgType.getEnum(orgType)).getString("id");
				}
				else if (com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit() != null) {
					companyID = ((com.kingdee.eas.basedata.org.OrgUnitInfo)com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentOrgUnit()).getString("id");
            	}
				com.kingdee.eas.base.codingrule.ICodingRuleManager iCodingRuleManager = com.kingdee.eas.base.codingrule.CodingRuleManagerFactory.getRemoteInstance();
		        if (iCodingRuleManager.isExist(editData, companyID)) {
		            if (iCodingRuleManager.isAddView(editData, companyID)) {
		            	editData.setNumber(iCodingRuleManager.getNumber(editData,companyID));
		            }
	                txtNumber.setEnabled(false);
		        }
            }
            catch (Exception e) {
                handUIException(e);
                this.oldData = editData;
                com.kingdee.eas.util.SysUtil.abort();
            } 
        } 
        else {
            if (editData.getNumber().trim().length() > 0) {
                txtNumber.setText(editData.getNumber());
            }
        }
    }

    /**
     * output loadFields method
     */
    public void loadFields()
    {
        		setAutoNumberByOrg("NONE");
        dataBinder.loadFields();
    }
		protected void setOrgF7(KDBizPromptBox f7,com.kingdee.eas.basedata.org.OrgType orgType) throws Exception
		{
			com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer oufip = new com.kingdee.eas.basedata.org.client.f7.NewOrgUnitFilterInfoProducer(orgType);
			oufip.getModel().setIsCUFilter(true);
			f7.setFilterInfoProducer(oufip);
		}

    /**
     * output storeFields method
     */
    public void storeFields()
    {
		dataBinder.storeFields();
    }

	/**
	 * ????????��??
	 */
	protected void registerValidator() {
    	getValidateHelper().setCustomValidator( getValidator() );
		getValidateHelper().registerBindProperty("hrOrgUnit", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("approveType", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.id", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.bizDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person.number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.person.name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.position", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.adminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.isThough", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("entrys.InterviewScore", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("number", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("description", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("applier.name", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("adminOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("applyDate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("billState", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("InterviewPersonName", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("InterLink", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("ApplyOrg", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("Ranking", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("RatingSocre", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("passRate", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("ApplyJob", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("IdNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("InterviewPersonNumber", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("createYear", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("finalRating", ValidateHelper.ON_SAVE);    
		getValidateHelper().registerBindProperty("finalRanking", ValidateHelper.ON_SAVE);    		
	}



    /**
     * output setOprtState method
     */
    public void setOprtState(String oprtType)
    {
        super.setOprtState(oprtType);
        if (STATUS_ADDNEW.equals(this.oprtState)) {
        } else if (STATUS_EDIT.equals(this.oprtState)) {
        } else if (STATUS_VIEW.equals(this.oprtState)) {
        } else if (STATUS_FINDVIEW.equals(this.oprtState)) {
        }
    }

    /**
     * output kdtEntrys_editStopping method
     */
    protected void kdtEntrys_editStopping(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output kdtEntrys_editStopped method
     */
    protected void kdtEntrys_editStopped(com.kingdee.bos.ctrl.kdf.table.event.KDTEditEvent e) throws Exception
    {
    }

    /**
     * output getSelectors method
     */
    public SelectorItemCollection getSelectors()
    {
        SelectorItemCollection sic = new SelectorItemCollection();
		String selectorAll = System.getProperty("selector.all");
		if(StringUtils.isEmpty(selectorAll)){
			selectorAll = "true";
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("hrOrgUnit.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("hrOrgUnit.id"));
        	sic.add(new SelectorItemInfo("hrOrgUnit.number"));
        	sic.add(new SelectorItemInfo("hrOrgUnit.name"));
		}
        sic.add(new SelectorItemInfo("approveType"));
    	sic.add(new SelectorItemInfo("entrys.id"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.*"));
		}
		else{
		}
    	sic.add(new SelectorItemInfo("entrys.bizDate"));
    	sic.add(new SelectorItemInfo("entrys.description"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.person.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.person.id"));
			sic.add(new SelectorItemInfo("entrys.person.gender"));
			sic.add(new SelectorItemInfo("entrys.person.name"));
        	sic.add(new SelectorItemInfo("entrys.person.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.position.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.position.id"));
			sic.add(new SelectorItemInfo("entrys.position.name"));
        	sic.add(new SelectorItemInfo("entrys.position.number"));
		}
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("entrys.adminOrg.*"));
		}
		else{
	    	sic.add(new SelectorItemInfo("entrys.adminOrg.id"));
			sic.add(new SelectorItemInfo("entrys.adminOrg.name"));
        	sic.add(new SelectorItemInfo("entrys.adminOrg.number"));
		}
    	sic.add(new SelectorItemInfo("entrys.isThough"));
    	sic.add(new SelectorItemInfo("entrys.InterviewScore"));
        sic.add(new SelectorItemInfo("number"));
        sic.add(new SelectorItemInfo("description"));
        sic.add(new SelectorItemInfo("applier.name"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("adminOrg.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("adminOrg.id"));
        	sic.add(new SelectorItemInfo("adminOrg.number"));
        	sic.add(new SelectorItemInfo("adminOrg.name"));
		}
        sic.add(new SelectorItemInfo("applyDate"));
        sic.add(new SelectorItemInfo("billState"));
        sic.add(new SelectorItemInfo("InterviewPersonName"));
        sic.add(new SelectorItemInfo("InterLink"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("ApplyOrg.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("ApplyOrg.id"));
        	sic.add(new SelectorItemInfo("ApplyOrg.number"));
        	sic.add(new SelectorItemInfo("ApplyOrg.name"));
		}
        sic.add(new SelectorItemInfo("Ranking"));
        sic.add(new SelectorItemInfo("RatingSocre"));
        sic.add(new SelectorItemInfo("passRate"));
		if(selectorAll.equalsIgnoreCase("true"))
		{
			sic.add(new SelectorItemInfo("ApplyJob.*"));
		}
		else{
        	sic.add(new SelectorItemInfo("ApplyJob.id"));
        	sic.add(new SelectorItemInfo("ApplyJob.number"));
        	sic.add(new SelectorItemInfo("ApplyJob.name"));
        	sic.add(new SelectorItemInfo("ApplyJob.effectDate"));
		}
        sic.add(new SelectorItemInfo("IdNumber"));
        sic.add(new SelectorItemInfo("InterviewPersonNumber"));
        sic.add(new SelectorItemInfo("createYear"));
        sic.add(new SelectorItemInfo("finalRating"));
        sic.add(new SelectorItemInfo("finalRanking"));
        return sic;
    }        
    	

    /**
     * output actionSubmit_actionPerformed method
     */
    public void actionSubmit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionSubmit_actionPerformed(e);
    }
    	

    /**
     * output actionPrint_actionPerformed method
     */
    public void actionPrint_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
    	if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.print(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionPrintPreview_actionPerformed method
     */
    public void actionPrintPreview_actionPerformed(ActionEvent e) throws Exception
    {
        ArrayList idList = new ArrayList();
        if (editData != null && !StringUtils.isEmpty(editData.getString("id"))) {
    		idList.add(editData.getString("id"));
    	}
        if (idList == null || idList.size() == 0 || getTDQueryPK() == null || getTDFileName() == null)
            return;
        com.kingdee.bos.ctrl.kdf.data.impl.BOSQueryDelegate data = new com.kingdee.eas.framework.util.CommonDataProvider(idList,getTDQueryPK());
        com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper appHlp = new com.kingdee.bos.ctrl.report.forapp.kdnote.client.KDNoteHelper();
        appHlp.printPreview(getTDFileName(), data, javax.swing.SwingUtilities.getWindowAncestor(this));
    }
    	

    /**
     * output actionAudit_actionPerformed method
     */
    public void actionAudit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionAudit_actionPerformed(e);
    }
    	

    /**
     * output actionUnaudit_actionPerformed method
     */
    public void actionUnaudit_actionPerformed(ActionEvent e) throws Exception
    {
        super.actionUnaudit_actionPerformed(e);
    }
	public RequestContext prepareActionSubmit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionSubmit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionSubmit() {
    	return false;
    }
	public RequestContext prepareActionPrint(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrint(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrint() {
    	return false;
    }
	public RequestContext prepareActionPrintPreview(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionPrintPreview(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionPrintPreview() {
    	return false;
    }
	public RequestContext prepareActionAudit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionAudit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionAudit() {
    	return false;
    }
	public RequestContext prepareActionUnaudit(IItemAction itemAction) throws Exception {
			RequestContext request = super.prepareActionUnaudit(itemAction);		
		if (request != null) {
    		request.setClassName(getUIHandlerClassName());
		}
		return request;
    }
	
	public boolean isPrepareActionUnaudit() {
    	return false;
    }

    /**
     * output getMetaDataPK method
     */
    public IMetaDataPK getMetaDataPK()
    {
        return new MetaDataPK("com.kingdee.eas.custom.recruitment.client", "InterviewresultsEditUI");
    }
    /**
     * output isBindWorkFlow method
     */
    public boolean isBindWorkFlow()
    {
        return true;
    }

    /**
     * output getEditUIName method
     */
    protected String getEditUIName()
    {
        return com.kingdee.eas.custom.recruitment.client.InterviewresultsEditUI.class.getName();
    }

    /**
     * output getBizInterface method
     */
    protected com.kingdee.eas.framework.ICoreBase getBizInterface() throws Exception
    {
        return com.kingdee.eas.custom.recruitment.InterviewresultsFactory.getRemoteInstance();
    }

    /**
     * output createNewData method
     */
    protected IObjectValue createNewData()
    {
        com.kingdee.eas.custom.recruitment.InterviewresultsInfo objectValue = new com.kingdee.eas.custom.recruitment.InterviewresultsInfo();
        objectValue.setCreator((com.kingdee.eas.base.permission.UserInfo)(com.kingdee.eas.common.client.SysContext.getSysContext().getCurrentUser()));		
        return objectValue;
    }


    	protected String getTDFileName() {
    	return "/bim/custom/recruitment/Interviewresults";
	}
    protected IMetaDataPK getTDQueryPK() {
    	return new MetaDataPK("com.kingdee.eas.custom.recruitment.app.InterviewresultsQuery");
	}
    

    /**
     * output getDetailTable method
     */
    protected KDTable getDetailTable() {
        return kdtEntrys;
	}
    /**
     * output applyDefaultValue method
     */
    protected void applyDefaultValue(IObjectValue vo) {        
    }        
	protected void setFieldsNull(com.kingdee.bos.dao.AbstractObjectValue arg0) {
		super.setFieldsNull(arg0);
		arg0.put("number",null);
	}

}