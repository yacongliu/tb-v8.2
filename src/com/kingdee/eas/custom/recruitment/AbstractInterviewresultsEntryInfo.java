package com.kingdee.eas.custom.recruitment;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractInterviewresultsEntryInfo extends com.kingdee.eas.hr.base.HRBillBaseEntryInfo implements Serializable 
{
    public AbstractInterviewresultsEntryInfo()
    {
        this("id");
    }
    protected AbstractInterviewresultsEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object: ��¼ 's ����ͷ property 
     */
    public com.kingdee.eas.custom.recruitment.InterviewresultsInfo getBill()
    {
        return (com.kingdee.eas.custom.recruitment.InterviewresultsInfo)get("bill");
    }
    public void setBill(com.kingdee.eas.custom.recruitment.InterviewresultsInfo item)
    {
        put("bill", item);
    }
    /**
     * Object: ��¼ 's ���Թ� property 
     */
    public com.kingdee.eas.basedata.person.PersonInfo getPerson()
    {
        return (com.kingdee.eas.basedata.person.PersonInfo)get("person");
    }
    public void setPerson(com.kingdee.eas.basedata.person.PersonInfo item)
    {
        put("person", item);
    }
    /**
     * Object:��¼'s ��Ч����property 
     */
    public java.util.Date getBizDate()
    {
        return getDate("bizDate");
    }
    public void setBizDate(java.util.Date item)
    {
        setDate("bizDate", item);
    }
    /**
     * Object:��¼'s ��עproperty 
     */
    public String getDescription()
    {
        return getString("description");
    }
    public void setDescription(String item)
    {
        setString("description", item);
    }
    /**
     * Object: ��¼ 's ����������֯ property 
     */
    public com.kingdee.eas.basedata.org.AdminOrgUnitInfo getAdminOrg()
    {
        return (com.kingdee.eas.basedata.org.AdminOrgUnitInfo)get("adminOrg");
    }
    public void setAdminOrg(com.kingdee.eas.basedata.org.AdminOrgUnitInfo item)
    {
        put("adminOrg", item);
    }
    /**
     * Object: ��¼ 's ְλ property 
     */
    public com.kingdee.eas.basedata.org.PositionInfo getPosition()
    {
        return (com.kingdee.eas.basedata.org.PositionInfo)get("position");
    }
    public void setPosition(com.kingdee.eas.basedata.org.PositionInfo item)
    {
        put("position", item);
    }
    /**
     * Object:��¼'s �Ƿ�ͨ��property 
     */
    public com.kingdee.eas.hr.train.TrainRecordIsPassedEnum getIsThough()
    {
        return com.kingdee.eas.hr.train.TrainRecordIsPassedEnum.getEnum(getInt("isThough"));
    }
    public void setIsThough(com.kingdee.eas.hr.train.TrainRecordIsPassedEnum item)
    {
		if (item != null) {
        setInt("isThough", item.getValue());
		}
    }
    /**
     * Object:��¼'s ����property 
     */
    public int getInterviewScore()
    {
        return getInt("InterviewScore");
    }
    public void setInterviewScore(int item)
    {
        setInt("InterviewScore", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("BD5FD726");
    }
}