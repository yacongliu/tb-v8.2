package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBaseEntry;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.custom.recruitment.app.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.IHRBillBaseEntry;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class InterviewresultsEntry extends HRBillBaseEntry implements IInterviewresultsEntry
{
    public InterviewresultsEntry()
    {
        super();
        registerInterface(IInterviewresultsEntry.class, this);
    }
    public InterviewresultsEntry(Context ctx)
    {
        super(ctx);
        registerInterface(IInterviewresultsEntry.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("BD5FD726");
    }
    private InterviewresultsEntryController getController() throws BOSException
    {
        return (InterviewresultsEntryController)getBizController();
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public InterviewresultsEntryInfo getInterviewresultsEntryInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getInterviewresultsEntryInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public InterviewresultsEntryInfo getInterviewresultsEntryInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getInterviewresultsEntryInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public InterviewresultsEntryInfo getInterviewresultsEntryInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getInterviewresultsEntryInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public InterviewresultsEntryCollection getInterviewresultsEntryCollection() throws BOSException
    {
        try {
            return getController().getInterviewresultsEntryCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public InterviewresultsEntryCollection getInterviewresultsEntryCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getInterviewresultsEntryCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public InterviewresultsEntryCollection getInterviewresultsEntryCollection(String oql) throws BOSException
    {
        try {
            return getController().getInterviewresultsEntryCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}