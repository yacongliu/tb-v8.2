package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class InterviewresultsEntryCollection extends AbstractObjectCollection 
{
    public InterviewresultsEntryCollection()
    {
        super(InterviewresultsEntryInfo.class);
    }
    public boolean add(InterviewresultsEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(InterviewresultsEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(InterviewresultsEntryInfo item)
    {
        return removeObject(item);
    }
    public InterviewresultsEntryInfo get(int index)
    {
        return(InterviewresultsEntryInfo)getObject(index);
    }
    public InterviewresultsEntryInfo get(Object key)
    {
        return(InterviewresultsEntryInfo)getObject(key);
    }
    public void set(int index, InterviewresultsEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(InterviewresultsEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(InterviewresultsEntryInfo item)
    {
        return super.indexOf(item);
    }
}