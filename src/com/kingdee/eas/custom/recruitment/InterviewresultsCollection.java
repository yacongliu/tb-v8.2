package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class InterviewresultsCollection extends AbstractObjectCollection 
{
    public InterviewresultsCollection()
    {
        super(InterviewresultsInfo.class);
    }
    public boolean add(InterviewresultsInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(InterviewresultsCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(InterviewresultsInfo item)
    {
        return removeObject(item);
    }
    public InterviewresultsInfo get(int index)
    {
        return(InterviewresultsInfo)getObject(index);
    }
    public InterviewresultsInfo get(Object key)
    {
        return(InterviewresultsInfo)getObject(key);
    }
    public void set(int index, InterviewresultsInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(InterviewresultsInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(InterviewresultsInfo item)
    {
        return super.indexOf(item);
    }
}