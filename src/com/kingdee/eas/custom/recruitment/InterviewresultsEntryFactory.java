package com.kingdee.eas.custom.recruitment;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class InterviewresultsEntryFactory
{
    private InterviewresultsEntryFactory()
    {
    }
    public static com.kingdee.eas.custom.recruitment.IInterviewresultsEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresultsEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("BD5FD726") ,com.kingdee.eas.custom.recruitment.IInterviewresultsEntry.class);
    }
    
    public static com.kingdee.eas.custom.recruitment.IInterviewresultsEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresultsEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("BD5FD726") ,com.kingdee.eas.custom.recruitment.IInterviewresultsEntry.class, objectCtx);
    }
    public static com.kingdee.eas.custom.recruitment.IInterviewresultsEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresultsEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("BD5FD726"));
    }
    public static com.kingdee.eas.custom.recruitment.IInterviewresultsEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.custom.recruitment.IInterviewresultsEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("BD5FD726"));
    }
}