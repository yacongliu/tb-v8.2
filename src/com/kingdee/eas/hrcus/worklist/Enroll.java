package com.kingdee.eas.hrcus.worklist;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.eas.hrcus.worklist.app.*;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.eas.hr.base.HRBillBase;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public class Enroll extends HRBillBase implements IEnroll
{
    public Enroll()
    {
        super();
        registerInterface(IEnroll.class, this);
    }
    public Enroll(Context ctx)
    {
        super(ctx);
        registerInterface(IEnroll.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("00A5BE36");
    }
    private EnrollController getController() throws BOSException
    {
        return (EnrollController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public EnrollCollection getEnrollCollection() throws BOSException
    {
        try {
            return getController().getEnrollCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public EnrollCollection getEnrollCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getEnrollCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public EnrollCollection getEnrollCollection(String oql) throws BOSException
    {
        try {
            return getController().getEnrollCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public EnrollInfo getEnrollInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getEnrollInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public EnrollInfo getEnrollInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getEnrollInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public EnrollInfo getEnrollInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getEnrollInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����״̬-User defined method
     *@param billID ����id
     *@param state ״̬
     */
    public void setState(BOSUuid billID, HRBillStateEnum state) throws BOSException, EASBizException
    {
        try {
            getController().setState(getContext(), billID, state);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������ͨ��״̬_��ְ�����嵥-User defined method
     *@param billID ����id
     */
    public void setPassState_Enroll(BOSUuid billID) throws BOSException, EASBizException
    {
        try {
            getController().setPassState_Enroll(getContext(), billID);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����������ͨ��״̬-User defined method
     *@param billID ����id
     */
    public void setNoPassState(BOSUuid billID) throws BOSException, EASBizException
    {
        try {
            getController().setNoPassState(getContext(), billID);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��������״̬-User defined method
     *@param billID ����id
     */
    public void setApproveState(BOSUuid billID) throws BOSException, EASBizException
    {
        try {
            getController().setApproveState(getContext(), billID);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *����δ���״̬-User defined method
     *@param billID ����id
     */
    public void setEditState(BOSUuid billID) throws BOSException, EASBizException
    {
        try {
            getController().setEditState(getContext(), billID);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}