package com.kingdee.eas.hrcus.worklist;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.hr.base.IHRBillBase;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

public interface IEnroll extends IHRBillBase
{
    public EnrollCollection getEnrollCollection() throws BOSException;
    public EnrollCollection getEnrollCollection(EntityViewInfo view) throws BOSException;
    public EnrollCollection getEnrollCollection(String oql) throws BOSException;
    public EnrollInfo getEnrollInfo(IObjectPK pk) throws BOSException, EASBizException;
    public EnrollInfo getEnrollInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public EnrollInfo getEnrollInfo(String oql) throws BOSException, EASBizException;
    public void setState(BOSUuid billID, HRBillStateEnum state) throws BOSException, EASBizException;
    public void setPassState_Enroll(BOSUuid billID) throws BOSException, EASBizException;
    public void setNoPassState(BOSUuid billID) throws BOSException, EASBizException;
    public void setApproveState(BOSUuid billID) throws BOSException, EASBizException;
    public void setEditState(BOSUuid billID) throws BOSException, EASBizException;
}