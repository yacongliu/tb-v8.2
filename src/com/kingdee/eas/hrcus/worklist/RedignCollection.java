package com.kingdee.eas.hrcus.worklist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class RedignCollection extends AbstractObjectCollection 
{
    public RedignCollection()
    {
        super(RedignInfo.class);
    }
    public boolean add(RedignInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(RedignCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(RedignInfo item)
    {
        return removeObject(item);
    }
    public RedignInfo get(int index)
    {
        return(RedignInfo)getObject(index);
    }
    public RedignInfo get(Object key)
    {
        return(RedignInfo)getObject(key);
    }
    public void set(int index, RedignInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(RedignInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(RedignInfo item)
    {
        return super.indexOf(item);
    }
}