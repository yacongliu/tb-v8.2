package com.kingdee.eas.hrcus.worklist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractEnrollInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractEnrollInfo()
    {
        this("id");
    }
    protected AbstractEnrollInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:��ְ�����嵥's ��ְ������property 
     */
    public String getEnrollBillNum()
    {
        return getString("enrollBillNum");
    }
    public void setEnrollBillNum(String item)
    {
        setString("enrollBillNum", item);
    }
    /**
     * Object:��ְ�����嵥's Ա������property 
     */
    public String getEmpNum()
    {
        return getString("empNum");
    }
    public void setEmpNum(String item)
    {
        setString("empNum", item);
    }
    /**
     * Object:��ְ�����嵥's Ա������property 
     */
    public String getEmpName()
    {
        return getString("empName");
    }
    public void setEmpName(String item)
    {
        setString("empName", item);
    }
    /**
     * Object:��ְ�����嵥's ���֤��property 
     */
    public String getIDNum()
    {
        return getString("IDNum");
    }
    public void setIDNum(String item)
    {
        setString("IDNum", item);
    }
    /**
     * Object:��ְ�����嵥's ���ձ���property 
     */
    public String getPassportNum()
    {
        return getString("passportNum");
    }
    public void setPassportNum(String item)
    {
        setString("passportNum", item);
    }
    /**
     * Object:��ְ�����嵥's �Ա�property 
     */
    public com.kingdee.eas.basedata.person.Genders getGender()
    {
        return com.kingdee.eas.basedata.person.Genders.getEnum(getInt("gender"));
    }
    public void setGender(com.kingdee.eas.basedata.person.Genders item)
    {
		if (item != null) {
        setInt("gender", item.getValue());
		}
    }
    /**
     * Object:��ְ�����嵥's ��������property 
     */
    public java.util.Date getBirthDay()
    {
        return getDate("birthDay");
    }
    public void setBirthDay(java.util.Date item)
    {
        setDate("birthDay", item);
    }
    /**
     * Object:��ְ�����嵥's ְλproperty 
     */
    public String getPosition()
    {
        return getString("position");
    }
    public void setPosition(String item)
    {
        setString("position", item);
    }
    /**
     * Object:��ְ�����嵥's ְ��property 
     */
    public String getPost()
    {
        return getString("post");
    }
    public void setPost(String item)
    {
        setString("post", item);
    }
    /**
     * Object:��ְ�����嵥's ������֯property 
     */
    public String getOrgName()
    {
        return getString("orgName");
    }
    public void setOrgName(String item)
    {
        setString("orgName", item);
    }
    /**
     * Object:��ְ�����嵥's ��ְ����property 
     */
    public java.util.Date getEntryDate()
    {
        return getDate("entryDate");
    }
    public void setEntryDate(java.util.Date item)
    {
        setDate("entryDate", item);
    }
    /**
     * Object:��ְ�����嵥's ������property 
     */
    public String getTryMonth()
    {
        return getString("tryMonth");
    }
    public void setTryMonth(String item)
    {
        setString("tryMonth", item);
    }
    /**
     * Object:��ְ�����嵥's �ֻ�����property 
     */
    public String getPhone()
    {
        return getString("phone");
    }
    public void setPhone(String item)
    {
        setString("phone", item);
    }
    /**
     * Object:��ְ�����嵥's ��עproperty 
     */
    public String getRemark()
    {
        return getString("remark");
    }
    public void setRemark(String item)
    {
        setString("remark", item);
    }
    /**
     * Object:��ְ�����嵥's �Ƿ�ǩ����ͬproperty 
     */
    public boolean isContract()
    {
        return getBoolean("contract");
    }
    public void setContract(boolean item)
    {
        setBoolean("contract", item);
    }
    /**
     * Object:��ְ�����嵥's ��������property 
     */
    public boolean isSafe()
    {
        return getBoolean("safe");
    }
    public void setSafe(boolean item)
    {
        setBoolean("safe", item);
    }
    /**
     * Object:��ְ�����嵥's �Ƿ��ҵ�Ǽ�property 
     */
    public boolean isJobCheck()
    {
        return getBoolean("jobCheck");
    }
    public void setJobCheck(boolean item)
    {
        setBoolean("jobCheck", item);
    }
    /**
     * Object:��ְ�����嵥's ����������property 
     */
    public boolean isFund()
    {
        return getBoolean("fund");
    }
    public void setFund(boolean item)
    {
        setBoolean("fund", item);
    }
    /**
     * Object:��ְ�����嵥's �籣����property 
     */
    public boolean isSecurity()
    {
        return getBoolean("security");
    }
    public void setSecurity(boolean item)
    {
        setBoolean("security", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("00A5BE36");
    }
}