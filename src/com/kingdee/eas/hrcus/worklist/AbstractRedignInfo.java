package com.kingdee.eas.hrcus.worklist;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractRedignInfo extends com.kingdee.eas.hr.base.HRBillBaseInfo implements Serializable 
{
    public AbstractRedignInfo()
    {
        this("id");
    }
    protected AbstractRedignInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:��ְ�����嵥's ��ְ������property 
     */
    public String getResignBillNum()
    {
        return getString("resignBillNum");
    }
    public void setResignBillNum(String item)
    {
        setString("resignBillNum", item);
    }
    /**
     * Object:��ְ�����嵥's Ա������property 
     */
    public String getEmpNum()
    {
        return getString("empNum");
    }
    public void setEmpNum(String item)
    {
        setString("empNum", item);
    }
    /**
     * Object:��ְ�����嵥's Ա������property 
     */
    public String getEmpName()
    {
        return getString("empName");
    }
    public void setEmpName(String item)
    {
        setString("empName", item);
    }
    /**
     * Object:��ְ�����嵥's ְ��property 
     */
    public String getPost()
    {
        return getString("post");
    }
    public void setPost(String item)
    {
        setString("post", item);
    }
    /**
     * Object:��ְ�����嵥's ְλproperty 
     */
    public String getPosition()
    {
        return getString("position");
    }
    public void setPosition(String item)
    {
        setString("position", item);
    }
    /**
     * Object:��ְ�����嵥's ������֯property 
     */
    public String getOrgName()
    {
        return getString("orgName");
    }
    public void setOrgName(String item)
    {
        setString("orgName", item);
    }
    /**
     * Object:��ְ�����嵥's �䶯����property 
     */
    public String getCOperation()
    {
        return getString("cOperation");
    }
    public void setCOperation(String item)
    {
        setString("cOperation", item);
    }
    /**
     * Object:��ְ�����嵥's �ù�״̬property 
     */
    public String getEmpState()
    {
        return getString("empState");
    }
    public void setEmpState(String item)
    {
        setString("empState", item);
    }
    /**
     * Object:��ְ�����嵥's �䶯����property 
     */
    public String getCType()
    {
        return getString("cType");
    }
    public void setCType(String item)
    {
        setString("cType", item);
    }
    /**
     * Object:��ְ�����嵥's ��Ч����property 
     */
    public java.util.Date getEffectDate()
    {
        return getDate("effectDate");
    }
    public void setEffectDate(java.util.Date item)
    {
        setDate("effectDate", item);
    }
    /**
     * Object:��ְ�����嵥's ��ְ֤��property 
     */
    public boolean isRemove()
    {
        return getBoolean("remove");
    }
    public void setRemove(boolean item)
    {
        setBoolean("remove", item);
    }
    /**
     * Object:��ְ�����嵥's �˹��Ǽ�property 
     */
    public boolean isRetried()
    {
        return getBoolean("retried");
    }
    public void setRetried(boolean item)
    {
        setBoolean("retried", item);
    }
    /**
     * Object:��ְ�����嵥's ����ת��property 
     */
    public boolean isFile()
    {
        return getBoolean("file");
    }
    public void setFile(boolean item)
    {
        setBoolean("file", item);
    }
    /**
     * Object:��ְ�����嵥's �籣����property 
     */
    public boolean isSecurity()
    {
        return getBoolean("security");
    }
    public void setSecurity(boolean item)
    {
        setBoolean("security", item);
    }
    /**
     * Object:��ְ�����嵥's ���������property 
     */
    public boolean isFund()
    {
        return getBoolean("fund");
    }
    public void setFund(boolean item)
    {
        setBoolean("fund", item);
    }
    /**
     * Object:��ְ�����嵥's ������property 
     */
    public boolean isAnnuity()
    {
        return getBoolean("annuity");
    }
    public void setAnnuity(boolean item)
    {
        setBoolean("annuity", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("164F766F");
    }
}