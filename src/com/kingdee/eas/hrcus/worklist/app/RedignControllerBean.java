package com.kingdee.eas.hrcus.worklist.app;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.ApproveTypeEnum;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hrcus.worklist.RedignInfo;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: RedignControllerBean <br>
 *            Description: 离职工作清单
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-12-25 & 下午07:08:22
 * @since V1.0
 */
public class RedignControllerBean extends AbstractRedignControllerBean {
    /** serialVersionUID */
    private static final long serialVersionUID = -5186267636302767706L;

    @SuppressWarnings("unused")
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.hrcus.worklist.app.RedignControllerBean");

    /**
     * （非 Javadoc）
     * <p>
     * Title: _submitEffect
     * </p>
     * <p>
     * Description: 提交生效
     * </p>
     * 
     * @param ctx
     * @param model
     * @return
     * @throws BOSException
     * @throws EASBizException
     * @see com.kingdee.eas.hr.base.app.HRBillBaseControllerBean#_submitEffect(com.kingdee.bos.Context,
     *      com.kingdee.eas.framework.CoreBaseInfo)
     */
    @Override
    protected IObjectPK _submitEffect(Context ctx, CoreBaseInfo model) throws BOSException, EASBizException {
        RedignInfo bill = (RedignInfo) model;
        bill.setApproveType(ApproveTypeEnum.DIRECT);
        bill.setBillState(HRBillStateEnum.AUDITED);
        IObjectPK objectPK = super.save(ctx, model);

        _setState(ctx, bill.getId(), HRBillStateEnum.AUDITED);

        return objectPK;
    }

    protected void _setState(Context ctx, BOSUuid billId, HRBillStateEnum state) throws BOSException,
            EASBizException {
        IObjectPK pk = new ObjectUuidPK(billId);
        RedignInfo info = (RedignInfo) _getValue(ctx, "select id, billState  where id = '" + pk.toString()
                + "'");

        info.setBillState(state);
        SelectorItemCollection selectors = new SelectorItemCollection();
        selectors.add(new SelectorItemInfo("id"));
        selectors.add(new SelectorItemInfo("billState"));

        updatePartial(ctx, info, selectors);
    }

    protected void _setApproveState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.AUDITING);
    }

    protected void _setEditState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.SAVED);
    }

    protected void _setNoPassState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.AUDITEND);
    }

    protected void _setPassState(Context ctx, BOSUuid billId) throws BOSException, EASBizException {
        _setState(ctx, billId, HRBillStateEnum.AUDITED);
    }

}