package com.kingdee.eas.hrcus.worklist.app;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.hr.base.ApproveTypeEnum;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hrcus.worklist.EnrollInfo;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: EnrollControllerBean <br>
 *            Description: 入职工作清单
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-12-25 & 下午07:07:36
 * @since V1.0
 */
public class EnrollControllerBean extends AbstractEnrollControllerBean {
    /** serialVersionUID */
    private static final long serialVersionUID = -3557476026136539233L;

    @SuppressWarnings("unused")
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.hrcus.worklist.app.EnrollControllerBean");

    /**
     * <p>
     * Title: _submitEffect
     * </p>
     * <p>
     * Description: 提交生效
     * </p>
     * 
     * @param ctx
     * @param model
     * @return
     * @throws BOSException
     * @throws EASBizException
     * @see com.kingdee.eas.hr.base.app.HRBillBaseControllerBean#_submitEffect(com.kingdee.bos.Context,
     *      com.kingdee.eas.framework.CoreBaseInfo)
     */
    protected IObjectPK _submitEffect(Context ctx, CoreBaseInfo model) throws BOSException, EASBizException {
        EnrollInfo bill = (EnrollInfo) model;
        bill.setApproveType(ApproveTypeEnum.DIRECT);
        bill.setBillState(HRBillStateEnum.AUDITED);
        IObjectPK objectPK = super.save(ctx, model);

        _setState(ctx, bill.getId(), HRBillStateEnum.AUDITED);

        return objectPK;
    }

    protected void _setState(Context ctx, BOSUuid billId, HRBillStateEnum state) throws BOSException,
            EASBizException {
        IObjectPK pk = new ObjectUuidPK(billId);
        EnrollInfo info = (EnrollInfo) _getValue(ctx, "select id, billState  where id = '" + pk.toString()
                + "'");

        info.setBillState(state);
        SelectorItemCollection selectors = new SelectorItemCollection();
        selectors.add(new SelectorItemInfo("id"));
        selectors.add(new SelectorItemInfo("billState"));

        updatePartial(ctx, info, selectors);
    }

    protected void _setApproveState(Context ctx, BOSUuid billID) throws BOSException, EASBizException {
        _setState(ctx, billID, HRBillStateEnum.AUDITING);
    }

    protected void _setEditState(Context ctx, BOSUuid billID) throws BOSException, EASBizException {
        _setState(ctx, billID, HRBillStateEnum.SAVED);
    }

    protected void _setNoPassState(Context ctx, BOSUuid billID) throws BOSException, EASBizException {
        _setState(ctx, billID, HRBillStateEnum.AUDITEND);
    }

    protected void _setPassState_Enroll(Context ctx, BOSUuid billID) throws BOSException, EASBizException {
        _setState(ctx, billID, HRBillStateEnum.AUDITED);
    }

}