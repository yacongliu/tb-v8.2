package com.kingdee.eas.hrcus.worklist.app;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import java.lang.String;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.Context;
import com.kingdee.eas.hrcus.worklist.EnrollInfo;
import com.kingdee.eas.hr.base.app.HRBillBaseController;
import com.kingdee.eas.hrcus.worklist.EnrollCollection;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.util.*;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;

import java.rmi.RemoteException;
import com.kingdee.bos.framework.ejb.BizController;

public interface EnrollController extends HRBillBaseController
{
    public EnrollCollection getEnrollCollection(Context ctx) throws BOSException, RemoteException;
    public EnrollCollection getEnrollCollection(Context ctx, EntityViewInfo view) throws BOSException, RemoteException;
    public EnrollCollection getEnrollCollection(Context ctx, String oql) throws BOSException, RemoteException;
    public EnrollInfo getEnrollInfo(Context ctx, IObjectPK pk) throws BOSException, EASBizException, RemoteException;
    public EnrollInfo getEnrollInfo(Context ctx, IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException, RemoteException;
    public EnrollInfo getEnrollInfo(Context ctx, String oql) throws BOSException, EASBizException, RemoteException;
    public void setState(Context ctx, BOSUuid billID, HRBillStateEnum state) throws BOSException, EASBizException, RemoteException;
    public void setPassState_Enroll(Context ctx, BOSUuid billID) throws BOSException, EASBizException, RemoteException;
    public void setNoPassState(Context ctx, BOSUuid billID) throws BOSException, EASBizException, RemoteException;
    public void setApproveState(Context ctx, BOSUuid billID) throws BOSException, EASBizException, RemoteException;
    public void setEditState(Context ctx, BOSUuid billID) throws BOSException, EASBizException, RemoteException;
}