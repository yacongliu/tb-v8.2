package com.kingdee.eas.hrcus.worklist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class EnrollFactory
{
    private EnrollFactory()
    {
    }
    public static com.kingdee.eas.hrcus.worklist.IEnroll getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IEnroll)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("00A5BE36") ,com.kingdee.eas.hrcus.worklist.IEnroll.class);
    }
    
    public static com.kingdee.eas.hrcus.worklist.IEnroll getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IEnroll)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("00A5BE36") ,com.kingdee.eas.hrcus.worklist.IEnroll.class, objectCtx);
    }
    public static com.kingdee.eas.hrcus.worklist.IEnroll getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IEnroll)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("00A5BE36"));
    }
    public static com.kingdee.eas.hrcus.worklist.IEnroll getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IEnroll)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("00A5BE36"));
    }
}