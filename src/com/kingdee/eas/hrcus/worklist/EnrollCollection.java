package com.kingdee.eas.hrcus.worklist;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class EnrollCollection extends AbstractObjectCollection 
{
    public EnrollCollection()
    {
        super(EnrollInfo.class);
    }
    public boolean add(EnrollInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(EnrollCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(EnrollInfo item)
    {
        return removeObject(item);
    }
    public EnrollInfo get(int index)
    {
        return(EnrollInfo)getObject(index);
    }
    public EnrollInfo get(Object key)
    {
        return(EnrollInfo)getObject(key);
    }
    public void set(int index, EnrollInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(EnrollInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(EnrollInfo item)
    {
        return super.indexOf(item);
    }
}