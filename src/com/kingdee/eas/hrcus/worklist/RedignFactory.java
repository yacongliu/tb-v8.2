package com.kingdee.eas.hrcus.worklist;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class RedignFactory
{
    private RedignFactory()
    {
    }
    public static com.kingdee.eas.hrcus.worklist.IRedign getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IRedign)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("164F766F") ,com.kingdee.eas.hrcus.worklist.IRedign.class);
    }
    
    public static com.kingdee.eas.hrcus.worklist.IRedign getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IRedign)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("164F766F") ,com.kingdee.eas.hrcus.worklist.IRedign.class, objectCtx);
    }
    public static com.kingdee.eas.hrcus.worklist.IRedign getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IRedign)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("164F766F"));
    }
    public static com.kingdee.eas.hrcus.worklist.IRedign getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.hrcus.worklist.IRedign)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("164F766F"));
    }
}