package com.kingdee.eas.hr.tools;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.basedata.org.AdminOrgUnitFactory;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.IAdminOrgUnit;
import com.kingdee.eas.basedata.org.IJob;
import com.kingdee.eas.basedata.org.IPosition;
import com.kingdee.eas.basedata.org.JobFactory;
import com.kingdee.eas.basedata.org.JobInfo;
import com.kingdee.eas.basedata.org.PositionFactory;
import com.kingdee.eas.basedata.org.PositionInfo;
import com.kingdee.eas.basedata.person.IPerson;
import com.kingdee.eas.basedata.person.PersonFactory;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.base.AffairActionReasonFactory;
import com.kingdee.eas.hr.base.AffairActionReasonInfo;
import com.kingdee.eas.hr.base.EmployeeTypeFactory;
import com.kingdee.eas.hr.base.EmployeeTypeInfo;
import com.kingdee.eas.hr.base.HRBizDefineFactory;
import com.kingdee.eas.hr.base.HRBizDefineInfo;
import com.kingdee.eas.hr.base.IAffairActionReason;
import com.kingdee.eas.hr.base.IEmployeeType;
import com.kingdee.eas.hr.base.IHRBizDefine;
import com.kingdee.eas.hr.org.IJobGrade;
import com.kingdee.eas.hr.org.JobGradeFactory;
import com.kingdee.eas.hr.org.JobGradeInfo;
import com.kingdee.util.StringUtils;

/**
 * Copyright 版权所有：天津金蝶软件有限公司 <br>
 * Title: BaseDataTools
 * 
 * Description: 基础资料工具类
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-1-18 & 上午10:50:16
 * @since V1.0
 */
public class BaseDataTools {

    private static IAdminOrgUnit iAdminOrgUnit = null;

    private static IPosition iPosition = null;

    private static IJobGrade iJobGrade = null;

    private static IPerson iPerson = null;

    private static IJob iJob = null;

    private static IHRBizDefine iHrBizDeine = null;

    private static IEmployeeType iEmployeeType = null;

    private static IAffairActionReason iAffairActionReason = null;

    /**
     * 
     * <p>
     * Title: getAdminOrgUnitInfo
     * </p>
     * <p>
     * Description: 获取行政组织信息
     * </p>
     * 
     * @param ctx
     * @param id
     * @return
     * @throws EASBizException
     * @throws BOSException
     */
    public static AdminOrgUnitInfo getAdminOrgUnitInfo(Context ctx, String id) throws EASBizException,
            BOSException {

        if (StringUtils.isEmpty(id)) {
            return null;
        }

        iAdminOrgUnit = (iAdminOrgUnit != null) ? iAdminOrgUnit : AdminOrgUnitFactory.getLocalInstance(ctx);

        return iAdminOrgUnit.getAdminOrgUnitInfo(new ObjectUuidPK(id));

    }

    /**
     * 
     * <p>
     * Title: getPositionInfo
     * </p>
     * <p>
     * Description: 获取职位信息
     * </p>
     * 
     * @param ctx
     * @param id
     * @return
     * @throws EASBizException
     * @throws BOSException
     */
    public static PositionInfo getPositionInfo(Context ctx, String id) throws EASBizException, BOSException {

        if (StringUtils.isEmpty(id)) {
            return null;
        }

        iPosition = (iPosition != null) ? iPosition : PositionFactory.getLocalInstance(ctx);

        return iPosition.getPositionInfo(new ObjectUuidPK(id));

    }

    /**
     * 
     * <p>
     * Title: getJobGradeInfo
     * </p>
     * <p>
     * Description: 获取职等信息
     * </p>
     * 
     * @param ctx
     * @param id
     * @return
     * @throws EASBizException
     * @throws BOSException
     */
    public static JobGradeInfo getJobGradeInfo(Context ctx, String id) throws EASBizException, BOSException {

        if (StringUtils.isEmpty(id)) {
            return null;
        }

        iJobGrade = (iJobGrade != null) ? iJobGrade : JobGradeFactory.getLocalInstance(ctx);

        return iJobGrade.getJobGradeInfo(new ObjectUuidPK(id));

    }

    /**
     * 
     * <p>
     * Title: getPersonInfo
     * </p>
     * <p>
     * Description: 获取职员信息
     * </p>
     * 
     * @param ctx
     * @param id
     * @return
     * @throws EASBizException
     * @throws BOSException
     */
    public static PersonInfo getPersonInfo(Context ctx, String id) throws EASBizException, BOSException {

        if (StringUtils.isEmpty(id)) {
            return null;
        }

        iPerson = (iPerson != null) ? iPerson : PersonFactory.getLocalInstance(ctx);

        return iPerson.getPersonInfo(new ObjectUuidPK(id));

    }

    /**
     * 
     * <p>
     * Title: getJobInfo
     * </p>
     * <p>
     * Description: 获取职务信息
     * </p>
     * 
     * @param ctx
     * @param jobId
     * @return
     * @throws BOSException
     * @throws EASBizException
     */
    public static JobInfo getJobInfo(Context ctx, String jobId) throws BOSException, EASBizException {

        if (StringUtils.isEmpty(jobId)) {
            return null;
        }

        iJob = (iJob != null) ? iJob : JobFactory.getLocalInstance(ctx);

        return iJob.getJobInfo(new ObjectUuidPK(jobId));
    }

    /**
     * 
     * <p>
     * Title: getHRBizDefine
     * </p>
     * <p>
     * Description: 获取变动操作信息
     * </p>
     * 
     * @param ctx
     * @param hrBizDefineId
     * @return
     * @throws BOSException
     * @throws EASBizException
     */
    public static HRBizDefineInfo getHRBizDefine(Context ctx, String hrBizDefineId) throws BOSException,
            EASBizException {

        if (StringUtils.isEmpty(hrBizDefineId)) {
            return null;
        }

        iHrBizDeine = (iHrBizDeine != null) ? iHrBizDeine : HRBizDefineFactory.getLocalInstance(ctx);

        return iHrBizDeine.getHRBizDefineInfo(new ObjectUuidPK(hrBizDefineId));
    }

    /**
     * 
     * <p>
     * Title: getEmployeeType
     * </p>
     * <p>
     * Description: 获取用工关系状态信息
     * </p>
     * 
     * @param ctx
     * @param hrBizDefineId
     * @return
     * @throws BOSException
     * @throws EASBizException
     */
    public static EmployeeTypeInfo getEmployeeType(Context ctx, String empTypeId) throws BOSException,
            EASBizException {

        if (StringUtils.isEmpty(empTypeId)) {
            return null;
        }

        iEmployeeType = (iEmployeeType != null) ? iEmployeeType : EmployeeTypeFactory.getLocalInstance(ctx);

        return iEmployeeType.getEmployeeTypeInfo(new ObjectUuidPK(empTypeId));
    }

    /**
     * 
     * <p>
     * Title: getAffairActionReason
     * </p>
     * <p>
     * Description: 获取变动类型信息
     * </p>
     * 
     * @param ctx
     * @param reasonId
     * @return
     * @throws BOSException
     * @throws EASBizException
     */
    public static AffairActionReasonInfo getAffairActionReason(Context ctx, String reasonId)
            throws BOSException, EASBizException {

        if (StringUtils.isEmpty(reasonId)) {
            return null;
        }

        iAffairActionReason = (iAffairActionReason != null) ? iAffairActionReason : AffairActionReasonFactory
                .getLocalInstance(ctx);

        return iAffairActionReason.getAffairActionReasonInfo(new ObjectUuidPK(reasonId));
    }
}
