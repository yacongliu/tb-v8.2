package com.kingdee.eas.hr.tools;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import com.kingdee.bos.metadata.entity.FilterInfo;
import com.kingdee.bos.metadata.entity.FilterItemInfo;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.metadata.entity.SelectorItemInfo;
import com.kingdee.bos.metadata.entity.SorterItemCollection;
import com.kingdee.bos.metadata.entity.SorterItemInfo;
import com.kingdee.bos.metadata.query.util.CompareType;
import com.kingdee.shr.compensation.FixAdjustSalaryCollection;
import com.kingdee.shr.compensation.FixAdjustSalaryFactory;
import com.kingdee.shr.compensation.FixAdjustSalaryInfo;
import com.kingdee.shr.compensation.IFixAdjustSalary;
import com.kingdee.util.StringUtils;

/**
 * @Copyright 版权所有：天津金蝶软件有限公司 <br>
 *            Title: FixSalaryTools <br>
 *            Description: 薪酬核算-薪酬档案-薪酬结构记录
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-12-22 & 上午10:43:34
 * @since V1.0
 */
public class FixSalaryTools {

    @SuppressWarnings("unchecked")
    public static Map getMaxEffectFixSalaryInfo(String cmpStandardId, String personId) throws BOSException {
        return getMaxEffectFixSalaryInfo(null, cmpStandardId, personId);
    }

    @SuppressWarnings("unchecked")
    public static Map getMaxEffectFixSalaryInfo(Context ctx, String cmpStandardId, String personId)
            throws BOSException {

        if (StringUtils.isEmpty(cmpStandardId) || StringUtils.isEmpty(personId)) {
            System.out.println("FixSalaryTools——getMaxEffectFixSalaryInfo 参数为空");
            return null;
        }

        IFixAdjustSalary iFixAdjustSalary = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map ajaxData = new HashMap();

        if (ctx == null) {
            iFixAdjustSalary = FixAdjustSalaryFactory.getRemoteInstance();
        } else {
            iFixAdjustSalary = FixAdjustSalaryFactory.getLocalInstance(ctx);
        }

        FixAdjustSalaryCollection collection = iFixAdjustSalary
                .getFixAdjustSalaryCollection(getEntityViewInfo(personId, cmpStandardId));
        List datas = new ArrayList();
        if (collection.size() > 0) {
            for (int i = 0; i < collection.size(); i++) {
                FixAdjustSalaryInfo fixAdjustSalaryInfo = collection.get(i);
                Map map = new HashMap(3);

                // 薪酬项目
                HashMap<String, String> f7Itemmap = new HashMap<String, String>(2);
                String cmpItemId = fixAdjustSalaryInfo.getCmpItem().getId().toString();
                String cmpItemName = fixAdjustSalaryInfo.getCmpItem().getName();
                f7Itemmap.put("id", cmpItemId);
                f7Itemmap.put("name", cmpItemName);
                map.put("cmpItem", f7Itemmap);

                // 薪酬标准
                HashMap<String, String> f7Satandmap = new HashMap<String, String>(2);
                String standId = fixAdjustSalaryInfo.getStandard().getId().toString();
                String standName = fixAdjustSalaryInfo.getStandard().getName();
                f7Satandmap.put("id", standId);
                f7Satandmap.put("name", standName);
                map.put("standard", f7Satandmap);

                // 薪等
                HashMap<String, String> f7Levelmap = new HashMap<String, String>(2);
                String levelId = fixAdjustSalaryInfo.getStdLevel().getId().toString();
                String levelName = fixAdjustSalaryInfo.getStdLevel().getName();
                f7Levelmap.put("id", levelId);
                f7Levelmap.put("name", levelName);
                map.put("stdLevel", f7Levelmap);
                // 薪级
                HashMap<String, String> f7Pointmap = new HashMap<String, String>(2);
                String pointId = fixAdjustSalaryInfo.getStdPoint().getId().toString();
                String pointName = fixAdjustSalaryInfo.getStdPoint().getName();
                f7Pointmap.put("id", pointId);
                f7Pointmap.put("name", pointName);
                map.put("stdPoint", f7Pointmap);

                // 金额
                BigDecimal money = fixAdjustSalaryInfo.getMoney();
                map.put("money", money);
                // 生效日期
                Date effectDay = fixAdjustSalaryInfo.getEffectDay();
                map.put("effectDate", sdf.format(effectDay));

                datas.add(map);

            }

        }

        ajaxData.put("rowData", datas);

        return ajaxData;

    }

    /**
     * 
     * <p>
     * Title: getEntityViewInfo
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param personId 人员
     * @param cmpStandardId 薪酬标准
     * @return EntityViewInfo
     */
    private static EntityViewInfo getEntityViewInfo(String personId, String cmpStandardId) {

        EntityViewInfo evi = new EntityViewInfo();
        evi.setFilter(getFilter(personId, cmpStandardId));
        evi.setSelector(getSelector());
        evi.setSorter(getSort());

        return evi;
    }

    /**
     * 
     * <p>
     * Title: getFilter
     * </p>
     * <p>
     * Description: 查询条件
     * </p>
     * 
     * @param personId 人员
     * @param cmpStandardId 薪酬标准
     * @return
     */
    private static FilterInfo getFilter(String personId, String cmpStandardId) {

        FilterInfo filter = new FilterInfo();
        filter.getFilterItems().add(new FilterItemInfo("person", personId));
        filter.getFilterItems().add(new FilterItemInfo("standard", cmpStandardId));

        String sql = "select d.fid from (select a.FCmpItemID,b.fName_l2,max(a.FEffectDay) as maxDateVal from T_HR_SFixAdjustSalary a left join T_HR_SCmpItem b on a.FCmpItemID=b.fid  where a.FPersonID='"
                + personId
                + "'"
                + " group by a.FCmpItemID,b.fName_l2) AS cc left join T_HR_SFixAdjustSalary d on cc.FCmpItemID=d.FCmpItemID and cc.maxDateVal=d.FEffectDay"
                + " where d.FPersonID='" + personId + "'";

        filter.getFilterItems().add(new FilterItemInfo("id", sql, CompareType.INNER));

        return filter;
    }

    /**
     * 
     * <p>
     * Title: getSelector
     * </p>
     * <p>
     * Description: 查询内容
     * </p>
     * 
     * @return
     */
    private static SelectorItemCollection getSelector() {

        SelectorItemCollection selectorItemCollection = new SelectorItemCollection();
        selectorItemCollection.add(new SelectorItemInfo("cmpItem.id"));
        selectorItemCollection.add(new SelectorItemInfo("cmpItem.name"));
        selectorItemCollection.add(new SelectorItemInfo("standard.id"));
        selectorItemCollection.add(new SelectorItemInfo("standard.name"));
        selectorItemCollection.add(new SelectorItemInfo("stdLevel.id"));
        selectorItemCollection.add(new SelectorItemInfo("stdLevel.name"));
        selectorItemCollection.add(new SelectorItemInfo("stdPoint.id"));
        selectorItemCollection.add(new SelectorItemInfo("stdPoint.name"));
        selectorItemCollection.add(new SelectorItemInfo("money"));
        selectorItemCollection.add(new SelectorItemInfo("effectDay"));

        return selectorItemCollection;
    }

    /**
     * 
     * <p>
     * Title: getSort
     * </p>
     * <p>
     * Description: 排序
     * </p>
     * 
     * @return
     */
    private static SorterItemCollection getSort() {

        SorterItemCollection sortColl = new SorterItemCollection();
        sortColl.add(new SorterItemInfo("cmpItem.name"));
        sortColl.add(new SorterItemInfo("effectDay"));

        return sortColl;
    }

}
