package com.kingdee.eas.hr.tools;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.bos.util.BOSUuid;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.PositionInfo;
import com.kingdee.eas.basedata.person.PersonFactory;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.org.JobGradeInfo;
import com.kingdee.jdbc.rowset.IRowSet;
import com.kingdee.shr.compensation.util.SqlUtil;
import com.kingdee.shr.costbudget.AdjustSalaryBillInfo;
import com.kingdee.util.StringUtils;

/**
 * @Copyright 版权所有： 天津金蝶软件有限公司 <br>
 *            Title: AdjustSalaryTools <br>
 *            Description: 定调薪单工具类
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-12-18 & 下午03:57:01
 * @since V1.0
 */
public class AdjustSalaryTools {

    public static Map<String, Object> getPersonOtherInfo(Context ctx, String personId)
            throws EASBizException, BOSException {
        AdjustSalaryBillInfo billInfo = new AdjustSalaryBillInfo();
        PersonInfo personInfo = PersonFactory.getLocalInstance(ctx).getPersonInfo(new ObjectUuidPK(personId));
        HashMap<String, Object> map = new HashMap<String, Object>(12);

        String sql = "select T_ORG_Admin.fid,T_ORG_Admin.FName_L2,T_ORG_Position.fid,T_ORG_Position.FName_l2,T_HR_JobGrade.Fid,T_HR_JobGrade.FName_L2 from t_bd_person left join T_HR_PersonPosition on T_HR_PersonPosition.fpersonId = t_bd_person.fid LEFT  JOIN T_ORG_Admin ON T_HR_PersonPosition.FPersonDep = T_ORG_Admin.FID LEFT  JOIN T_ORG_Position ON T_HR_PersonPosition.FPrimaryPositionID = T_ORG_Position.FID LEFT  JOIN T_HR_JobGrade ON T_HR_PersonPosition.FJobGrade = T_HR_JobGrade.FID  where t_bd_person.fid='"
                + personId + "'";
        try {
            IRowSet set = SqlUtil.executeQuery(ctx, sql);
            while (set.next()) {
                String orgId = set.getString(1);
                String orgName = set.getString(2);
                String positionId = set.getString(3);
                String positionName = set.getString(4);
                String jobGradeId = set.getString(5);
                String jobGradeName = set.getString(6);
                AdminOrgUnitInfo orgItem = new AdminOrgUnitInfo();
                orgItem.setId(BOSUuid.read(orgId));
                orgItem.setName(orgName);
                Map<String, String> orgItem_map = new HashMap<String, String>(2);
                orgItem_map.put("id", orgItem.getId().toString());
                orgItem_map.put("name", orgItem.getName());
                map.put("NowOrg", orgItem_map);
                if (!(StringUtils.isEmpty(positionId))) {
                    PositionInfo positionInfoItem = new PositionInfo();
                    positionInfoItem.setId(BOSUuid.read(positionId));
                    positionInfoItem.setName(positionName);
                    Map<String, String> nowPosition_map = new HashMap<String, String>(2);
                    nowPosition_map.put("id", positionInfoItem.getId().toString());
                    nowPosition_map.put("name", positionInfoItem.getName());
                    billInfo.setNowPosition(positionInfoItem);
                    map.put("NowPosition", nowPosition_map);
                }
                if (!(StringUtils.isEmpty(jobGradeId))) {
                    JobGradeInfo jobGradeInfoItem = new JobGradeInfo();
                    jobGradeInfoItem.setId(BOSUuid.read(jobGradeId));
                    jobGradeInfoItem.setName(jobGradeName);
                    Map<String, String> nowJobGrade_map = new HashMap<String, String>(2);
                    nowJobGrade_map.put("id", jobGradeInfoItem.getId().toString());
                    nowJobGrade_map.put("name", jobGradeInfoItem.getName());
                    billInfo.setNowJobGrade(jobGradeInfoItem);
                    map.put("NowJobGrade", nowJobGrade_map);
                }
            }
        } catch (BOSException e) {
            throw new BOSException("获取现组织现职位时出错");
        } catch (SQLException e) {
            throw new BOSException("获取现组织现职位遍历时出错");
        }

        map.put("person_number", personInfo.getNumber());

        return map;

    }

}
