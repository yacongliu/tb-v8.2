package com.kingdee.eas.hr.affair;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.hr.affair.app.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public class EmpEnrollBizBill extends HRAffairBizBill implements IEmpEnrollBizBill
{
    public EmpEnrollBizBill()
    {
        super();
        registerInterface(IEmpEnrollBizBill.class, this);
    }
    public EmpEnrollBizBill(Context ctx)
    {
        super(ctx);
        registerInterface(IEmpEnrollBizBill.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("B41CAA3F");
    }
    private EmpEnrollBizBillController getController() throws BOSException
    {
        return (EmpEnrollBizBillController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public EmpEnrollBizBillCollection getEmpEnrollBizBillCollection() throws BOSException
    {
        try {
            return getController().getEmpEnrollBizBillCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public EmpEnrollBizBillCollection getEmpEnrollBizBillCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getEmpEnrollBizBillCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public EmpEnrollBizBillCollection getEmpEnrollBizBillCollection(String oql) throws BOSException
    {
        try {
            return getController().getEmpEnrollBizBillCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public EmpEnrollBizBillInfo getEmpEnrollBizBillInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpEnrollBizBillInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public EmpEnrollBizBillInfo getEmpEnrollBizBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpEnrollBizBillInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public EmpEnrollBizBillInfo getEmpEnrollBizBillInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getEmpEnrollBizBillInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *��Է�¼-User defined method
     *@param entryInfo entryInfo
     */
    public void reviseEmpEnroll(EmpEnrollBizBillEntryInfo entryInfo) throws BOSException, EASBizException
    {
        try {
            getController().reviseEmpEnroll(getContext(), entryInfo);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}