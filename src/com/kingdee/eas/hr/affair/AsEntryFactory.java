package com.kingdee.eas.hr.affair;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class AsEntryFactory
{
    private AsEntryFactory()
    {
    }
    public static com.kingdee.eas.hr.affair.IAsEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IAsEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C396C40D") ,com.kingdee.eas.hr.affair.IAsEntry.class);
    }
    
    public static com.kingdee.eas.hr.affair.IAsEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IAsEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C396C40D") ,com.kingdee.eas.hr.affair.IAsEntry.class, objectCtx);
    }
    public static com.kingdee.eas.hr.affair.IAsEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IAsEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C396C40D"));
    }
    public static com.kingdee.eas.hr.affair.IAsEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IAsEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C396C40D"));
    }
}