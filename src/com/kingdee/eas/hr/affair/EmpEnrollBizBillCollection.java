package com.kingdee.eas.hr.affair;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class EmpEnrollBizBillCollection extends AbstractObjectCollection 
{
    public EmpEnrollBizBillCollection()
    {
        super(EmpEnrollBizBillInfo.class);
    }
    public boolean add(EmpEnrollBizBillInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(EmpEnrollBizBillCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(EmpEnrollBizBillInfo item)
    {
        return removeObject(item);
    }
    public EmpEnrollBizBillInfo get(int index)
    {
        return(EmpEnrollBizBillInfo)getObject(index);
    }
    public EmpEnrollBizBillInfo get(Object key)
    {
        return(EmpEnrollBizBillInfo)getObject(key);
    }
    public void set(int index, EmpEnrollBizBillInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(EmpEnrollBizBillInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(EmpEnrollBizBillInfo item)
    {
        return super.indexOf(item);
    }
}