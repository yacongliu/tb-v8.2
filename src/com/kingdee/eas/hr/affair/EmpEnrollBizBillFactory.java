package com.kingdee.eas.hr.affair;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class EmpEnrollBizBillFactory
{
    private EmpEnrollBizBillFactory()
    {
    }
    public static com.kingdee.eas.hr.affair.IEmpEnrollBizBill getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IEmpEnrollBizBill)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("B41CAA3F") ,com.kingdee.eas.hr.affair.IEmpEnrollBizBill.class);
    }
    
    public static com.kingdee.eas.hr.affair.IEmpEnrollBizBill getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IEmpEnrollBizBill)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("B41CAA3F") ,com.kingdee.eas.hr.affair.IEmpEnrollBizBill.class, objectCtx);
    }
    public static com.kingdee.eas.hr.affair.IEmpEnrollBizBill getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IEmpEnrollBizBill)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("B41CAA3F"));
    }
    public static com.kingdee.eas.hr.affair.IEmpEnrollBizBill getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IEmpEnrollBizBill)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("B41CAA3F"));
    }
}