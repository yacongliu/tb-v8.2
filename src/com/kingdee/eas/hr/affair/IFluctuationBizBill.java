package com.kingdee.eas.hr.affair;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IFluctuationBizBill extends IHRAffairBizBill
{
    public FluctuationBizBillCollection getFluctuationBizBillCollection() throws BOSException;
    public FluctuationBizBillCollection getFluctuationBizBillCollection(EntityViewInfo view) throws BOSException;
    public FluctuationBizBillCollection getFluctuationBizBillCollection(String oql) throws BOSException;
    public FluctuationBizBillInfo getFluctuationBizBillInfo(IObjectPK pk) throws BOSException, EASBizException;
    public FluctuationBizBillInfo getFluctuationBizBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public FluctuationBizBillInfo getFluctuationBizBillInfo(String oql) throws BOSException, EASBizException;
}