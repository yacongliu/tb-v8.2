package com.kingdee.eas.hr.affair;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractEmpEnrollBizBillInfo extends com.kingdee.eas.hr.affair.HRAffairBizBillInfo implements Serializable 
{
    public AbstractEmpEnrollBizBillInfo()
    {
        this("id");
    }
    protected AbstractEmpEnrollBizBillInfo(String pkField)
    {
        super(pkField);
        put("asentry", new com.kingdee.eas.hr.affair.AsEntryCollection());
        put("entrys", new com.kingdee.eas.hr.affair.EmpEnrollBizBillEntryCollection());
    }
    /**
     * Object: Ա����ְ 's ��¼ property 
     */
    public com.kingdee.eas.hr.affair.EmpEnrollBizBillEntryCollection getEntrys()
    {
        return (com.kingdee.eas.hr.affair.EmpEnrollBizBillEntryCollection)get("entrys");
    }
    /**
     * Object:Ա����ְ's ����ϵͳ����ְproperty 
     */
    public String getEnrollFromOthers()
    {
        return getString("enrollFromOthers");
    }
    public void setEnrollFromOthers(String item)
    {
        setString("enrollFromOthers", item);
    }
    /**
     * Object:Ա����ְ's �Ƿ���¼��ʶproperty 
     */
    public boolean isIsMulti()
    {
        return getBoolean("isMulti");
    }
    public void setIsMulti(boolean item)
    {
        setBoolean("isMulti", item);
    }
    /**
     * Object:Ա����ְ's ��ԴSSCproperty 
     */
    public boolean isIsFromSSC()
    {
        return getBoolean("isFromSSC");
    }
    public void setIsFromSSC(boolean item)
    {
        setBoolean("isFromSSC", item);
    }
    /**
     * Object: Ա����ְ 's null property 
     */
    public com.kingdee.eas.hr.affair.AsEntryCollection getAsentry()
    {
        return (com.kingdee.eas.hr.affair.AsEntryCollection)get("asentry");
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("B41CAA3F");
    }
}