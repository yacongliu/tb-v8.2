package com.kingdee.eas.hr.affair;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class FluctuationBizBillCollection extends AbstractObjectCollection 
{
    public FluctuationBizBillCollection()
    {
        super(FluctuationBizBillInfo.class);
    }
    public boolean add(FluctuationBizBillInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(FluctuationBizBillCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(FluctuationBizBillInfo item)
    {
        return removeObject(item);
    }
    public FluctuationBizBillInfo get(int index)
    {
        return(FluctuationBizBillInfo)getObject(index);
    }
    public FluctuationBizBillInfo get(Object key)
    {
        return(FluctuationBizBillInfo)getObject(key);
    }
    public void set(int index, FluctuationBizBillInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(FluctuationBizBillInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(FluctuationBizBillInfo item)
    {
        return super.indexOf(item);
    }
}