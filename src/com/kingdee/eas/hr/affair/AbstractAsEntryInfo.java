package com.kingdee.eas.hr.affair;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractAsEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractAsEntryInfo()
    {
        this("id");
    }
    protected AbstractAsEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:н�������¼'s �귢�Ŵ���property 
     */
    public String getSendCount()
    {
        return getString("sendCount");
    }
    public void setSendCount(String item)
    {
        setString("sendCount", item);
    }
    /**
     * Object:н�������¼'s ���property 
     */
    public java.math.BigDecimal getMoney()
    {
        return getBigDecimal("money");
    }
    public void setMoney(java.math.BigDecimal item)
    {
        setBigDecimal("money", item);
    }
    /**
     * Object: н�������¼ 's null property 
     */
    public com.kingdee.eas.hr.affair.EmpEnrollBizBillInfo getParent()
    {
        return (com.kingdee.eas.hr.affair.EmpEnrollBizBillInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.hr.affair.EmpEnrollBizBillInfo item)
    {
        put("parent", item);
    }
    /**
     * Object: н�������¼ 's н���׼ property 
     */
    public com.kingdee.shr.costbudget.CmpStandardInfo getCmpStandard()
    {
        return (com.kingdee.shr.costbudget.CmpStandardInfo)get("cmpStandard");
    }
    public void setCmpStandard(com.kingdee.shr.costbudget.CmpStandardInfo item)
    {
        put("cmpStandard", item);
    }
    /**
     * Object: н�������¼ 's н����Ŀ property 
     */
    public com.kingdee.shr.compensation.CmpItemInfo getCmpItem()
    {
        return (com.kingdee.shr.compensation.CmpItemInfo)get("cmpItem");
    }
    public void setCmpItem(com.kingdee.shr.compensation.CmpItemInfo item)
    {
        put("cmpItem", item);
    }
    /**
     * Object: н�������¼ 's н�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdLevelInfo getCmpLevel()
    {
        return (com.kingdee.shr.costbudget.CmpStdLevelInfo)get("cmpLevel");
    }
    public void setCmpLevel(com.kingdee.shr.costbudget.CmpStdLevelInfo item)
    {
        put("cmpLevel", item);
    }
    /**
     * Object:н�������¼'s ��Ч����property 
     */
    public java.util.Date getEffectDate()
    {
        return getDate("effectDate");
    }
    public void setEffectDate(java.util.Date item)
    {
        setDate("effectDate", item);
    }
    /**
     * Object:н�������¼'s ԭ���property 
     */
    public java.math.BigDecimal getOldMoney()
    {
        return getBigDecimal("oldMoney");
    }
    public void setOldMoney(java.math.BigDecimal item)
    {
        setBigDecimal("oldMoney", item);
    }
    /**
     * Object: н�������¼ 's ԭн�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdLevelInfo getOldCmpLevel()
    {
        return (com.kingdee.shr.costbudget.CmpStdLevelInfo)get("oldCmpLevel");
    }
    public void setOldCmpLevel(com.kingdee.shr.costbudget.CmpStdLevelInfo item)
    {
        put("oldCmpLevel", item);
    }
    /**
     * Object:н�������¼'s ԭ��Ч����property 
     */
    public java.util.Date getOldEffectDate()
    {
        return getDate("oldEffectDate");
    }
    public void setOldEffectDate(java.util.Date item)
    {
        setDate("oldEffectDate", item);
    }
    /**
     * Object: н�������¼ 's н�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdPointInfo getStdPoint()
    {
        return (com.kingdee.shr.costbudget.CmpStdPointInfo)get("stdPoint");
    }
    public void setStdPoint(com.kingdee.shr.costbudget.CmpStdPointInfo item)
    {
        put("stdPoint", item);
    }
    /**
     * Object: н�������¼ 's ԭн�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdPointInfo getOldStdPoint()
    {
        return (com.kingdee.shr.costbudget.CmpStdPointInfo)get("oldStdPoint");
    }
    public void setOldStdPoint(com.kingdee.shr.costbudget.CmpStdPointInfo item)
    {
        put("oldStdPoint", item);
    }
    /**
     * Object: н�������¼ 's ���н�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdScopeInfo getStdScope()
    {
        return (com.kingdee.shr.costbudget.CmpStdScopeInfo)get("stdScope");
    }
    public void setStdScope(com.kingdee.shr.costbudget.CmpStdScopeInfo item)
    {
        put("stdScope", item);
    }
    /**
     * Object: н�������¼ 's ԭ���н�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdScopeInfo getOldStdScope()
    {
        return (com.kingdee.shr.costbudget.CmpStdScopeInfo)get("oldStdScope");
    }
    public void setOldStdScope(com.kingdee.shr.costbudget.CmpStdScopeInfo item)
    {
        put("oldStdScope", item);
    }
    /**
     * Object: н�������¼ 's ԭн���׼ property 
     */
    public com.kingdee.shr.costbudget.CmpStandardInfo getOldCmpStandard()
    {
        return (com.kingdee.shr.costbudget.CmpStandardInfo)get("oldCmpStandard");
    }
    public void setOldCmpStandard(com.kingdee.shr.costbudget.CmpStandardInfo item)
    {
        put("oldCmpStandard", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("C396C40D");
    }
}