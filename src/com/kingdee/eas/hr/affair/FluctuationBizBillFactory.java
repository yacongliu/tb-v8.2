package com.kingdee.eas.hr.affair;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class FluctuationBizBillFactory
{
    private FluctuationBizBillFactory()
    {
    }
    public static com.kingdee.eas.hr.affair.IFluctuationBizBill getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IFluctuationBizBill)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("C0DAD00D") ,com.kingdee.eas.hr.affair.IFluctuationBizBill.class);
    }
    
    public static com.kingdee.eas.hr.affair.IFluctuationBizBill getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IFluctuationBizBill)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("C0DAD00D") ,com.kingdee.eas.hr.affair.IFluctuationBizBill.class, objectCtx);
    }
    public static com.kingdee.eas.hr.affair.IFluctuationBizBill getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IFluctuationBizBill)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("C0DAD00D"));
    }
    public static com.kingdee.eas.hr.affair.IFluctuationBizBill getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.IFluctuationBizBill)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("C0DAD00D"));
    }
}