package com.kingdee.eas.hr.affair;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class AsEntryCollection extends AbstractObjectCollection 
{
    public AsEntryCollection()
    {
        super(AsEntryInfo.class);
    }
    public boolean add(AsEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(AsEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(AsEntryInfo item)
    {
        return removeObject(item);
    }
    public AsEntryInfo get(int index)
    {
        return(AsEntryInfo)getObject(index);
    }
    public AsEntryInfo get(Object key)
    {
        return(AsEntryInfo)getObject(key);
    }
    public void set(int index, AsEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(AsEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(AsEntryInfo item)
    {
        return super.indexOf(item);
    }
}