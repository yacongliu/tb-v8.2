package com.kingdee.eas.hr.affair;

import com.kingdee.bos.framework.ejb.EJBRemoteException;
import com.kingdee.bos.util.BOSObjectType;
import java.rmi.RemoteException;
import com.kingdee.bos.framework.AbstractBizCtrl;
import com.kingdee.bos.orm.template.ORMObject;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.hr.affair.app.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public class FluctuationBizBill extends HRAffairBizBill implements IFluctuationBizBill
{
    public FluctuationBizBill()
    {
        super();
        registerInterface(IFluctuationBizBill.class, this);
    }
    public FluctuationBizBill(Context ctx)
    {
        super(ctx);
        registerInterface(IFluctuationBizBill.class, this);
    }
    public BOSObjectType getType()
    {
        return new BOSObjectType("C0DAD00D");
    }
    private FluctuationBizBillController getController() throws BOSException
    {
        return (FluctuationBizBillController)getBizController();
    }
    /**
     *ȡ����-System defined method
     *@return
     */
    public FluctuationBizBillCollection getFluctuationBizBillCollection() throws BOSException
    {
        try {
            return getController().getFluctuationBizBillCollection(getContext());
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param view ȡ����
     *@return
     */
    public FluctuationBizBillCollection getFluctuationBizBillCollection(EntityViewInfo view) throws BOSException
    {
        try {
            return getController().getFluctuationBizBillCollection(getContext(), view);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡ����-System defined method
     *@param oql ȡ����
     *@return
     */
    public FluctuationBizBillCollection getFluctuationBizBillCollection(String oql) throws BOSException
    {
        try {
            return getController().getFluctuationBizBillCollection(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@return
     */
    public FluctuationBizBillInfo getFluctuationBizBillInfo(IObjectPK pk) throws BOSException, EASBizException
    {
        try {
            return getController().getFluctuationBizBillInfo(getContext(), pk);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param pk ȡֵ
     *@param selector ȡֵ
     *@return
     */
    public FluctuationBizBillInfo getFluctuationBizBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException
    {
        try {
            return getController().getFluctuationBizBillInfo(getContext(), pk, selector);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
    /**
     *ȡֵ-System defined method
     *@param oql ȡֵ
     *@return
     */
    public FluctuationBizBillInfo getFluctuationBizBillInfo(String oql) throws BOSException, EASBizException
    {
        try {
            return getController().getFluctuationBizBillInfo(getContext(), oql);
        }
        catch(RemoteException err) {
            throw new EJBRemoteException(err);
        }
    }
}