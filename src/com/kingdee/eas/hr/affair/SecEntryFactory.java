package com.kingdee.eas.hr.affair;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.BOSObjectFactory;
import com.kingdee.bos.util.BOSObjectType;
import com.kingdee.bos.Context;

public class SecEntryFactory
{
    private SecEntryFactory()
    {
    }
    public static com.kingdee.eas.hr.affair.ISecEntry getRemoteInstance() throws BOSException
    {
        return (com.kingdee.eas.hr.affair.ISecEntry)BOSObjectFactory.createRemoteBOSObject(new BOSObjectType("496C9394") ,com.kingdee.eas.hr.affair.ISecEntry.class);
    }
    
    public static com.kingdee.eas.hr.affair.ISecEntry getRemoteInstanceWithObjectContext(Context objectCtx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.ISecEntry)BOSObjectFactory.createRemoteBOSObjectWithObjectContext(new BOSObjectType("496C9394") ,com.kingdee.eas.hr.affair.ISecEntry.class, objectCtx);
    }
    public static com.kingdee.eas.hr.affair.ISecEntry getLocalInstance(Context ctx) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.ISecEntry)BOSObjectFactory.createBOSObject(ctx, new BOSObjectType("496C9394"));
    }
    public static com.kingdee.eas.hr.affair.ISecEntry getLocalInstance(String sessionID) throws BOSException
    {
        return (com.kingdee.eas.hr.affair.ISecEntry)BOSObjectFactory.createBOSObject(sessionID, new BOSObjectType("496C9394"));
    }
}