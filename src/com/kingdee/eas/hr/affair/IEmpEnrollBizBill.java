package com.kingdee.eas.hr.affair;

import com.kingdee.bos.BOSException;
//import com.kingdee.bos.metadata.*;
import com.kingdee.bos.framework.*;
import com.kingdee.bos.util.*;
import com.kingdee.bos.Context;

import com.kingdee.bos.Context;
import com.kingdee.bos.BOSException;
import com.kingdee.bos.dao.IObjectPK;
import com.kingdee.bos.metadata.entity.EntityViewInfo;
import java.lang.String;
import com.kingdee.eas.framework.CoreBaseInfo;
import com.kingdee.eas.framework.CoreBaseCollection;
import com.kingdee.bos.framework.*;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.bos.metadata.entity.SelectorItemCollection;
import com.kingdee.bos.util.*;

public interface IEmpEnrollBizBill extends IHRAffairBizBill
{
    public EmpEnrollBizBillCollection getEmpEnrollBizBillCollection() throws BOSException;
    public EmpEnrollBizBillCollection getEmpEnrollBizBillCollection(EntityViewInfo view) throws BOSException;
    public EmpEnrollBizBillCollection getEmpEnrollBizBillCollection(String oql) throws BOSException;
    public EmpEnrollBizBillInfo getEmpEnrollBizBillInfo(IObjectPK pk) throws BOSException, EASBizException;
    public EmpEnrollBizBillInfo getEmpEnrollBizBillInfo(IObjectPK pk, SelectorItemCollection selector) throws BOSException, EASBizException;
    public EmpEnrollBizBillInfo getEmpEnrollBizBillInfo(String oql) throws BOSException, EASBizException;
    public void reviseEmpEnroll(EmpEnrollBizBillEntryInfo entryInfo) throws BOSException, EASBizException;
}