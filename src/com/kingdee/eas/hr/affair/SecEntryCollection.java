package com.kingdee.eas.hr.affair;

import com.kingdee.bos.dao.AbstractObjectCollection;
import com.kingdee.bos.dao.IObjectPK;

public class SecEntryCollection extends AbstractObjectCollection 
{
    public SecEntryCollection()
    {
        super(SecEntryInfo.class);
    }
    public boolean add(SecEntryInfo item)
    {
        return addObject(item);
    }
    public boolean addCollection(SecEntryCollection item)
    {
        return addObjectCollection(item);
    }
    public boolean remove(SecEntryInfo item)
    {
        return removeObject(item);
    }
    public SecEntryInfo get(int index)
    {
        return(SecEntryInfo)getObject(index);
    }
    public SecEntryInfo get(Object key)
    {
        return(SecEntryInfo)getObject(key);
    }
    public void set(int index, SecEntryInfo item)
    {
        setObject(index, item);
    }
    public boolean contains(SecEntryInfo item)
    {
        return containsObject(item);
    }
    public boolean contains(Object key)
    {
        return containsKey(key);
    }
    public int indexOf(SecEntryInfo item)
    {
        return super.indexOf(item);
    }
}