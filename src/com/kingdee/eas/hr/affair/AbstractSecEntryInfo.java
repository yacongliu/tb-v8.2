package com.kingdee.eas.hr.affair;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractSecEntryInfo extends com.kingdee.eas.framework.CoreBillEntryBaseInfo implements Serializable 
{
    public AbstractSecEntryInfo()
    {
        this("id");
    }
    protected AbstractSecEntryInfo(String pkField)
    {
        super(pkField);
    }
    /**
     * Object:null's ���property 
     */
    public java.math.BigDecimal getMoney()
    {
        return getBigDecimal("money");
    }
    public void setMoney(java.math.BigDecimal item)
    {
        setBigDecimal("money", item);
    }
    /**
     * Object:null's ��Ч����property 
     */
    public java.util.Date getEffectDate()
    {
        return getDate("effectDate");
    }
    public void setEffectDate(java.util.Date item)
    {
        setDate("effectDate", item);
    }
    /**
     * Object: null 's н���׼ property 
     */
    public com.kingdee.shr.costbudget.CmpStandardInfo getCmpStandard()
    {
        return (com.kingdee.shr.costbudget.CmpStandardInfo)get("cmpStandard");
    }
    public void setCmpStandard(com.kingdee.shr.costbudget.CmpStandardInfo item)
    {
        put("cmpStandard", item);
    }
    /**
     * Object: null 's н����Ŀ property 
     */
    public com.kingdee.shr.compensation.CmpItemInfo getCmpItem()
    {
        return (com.kingdee.shr.compensation.CmpItemInfo)get("cmpItem");
    }
    public void setCmpItem(com.kingdee.shr.compensation.CmpItemInfo item)
    {
        put("cmpItem", item);
    }
    /**
     * Object: null 's н�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdLevelInfo getCmpLevel()
    {
        return (com.kingdee.shr.costbudget.CmpStdLevelInfo)get("cmpLevel");
    }
    public void setCmpLevel(com.kingdee.shr.costbudget.CmpStdLevelInfo item)
    {
        put("cmpLevel", item);
    }
    /**
     * Object: null 's н�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdPointInfo getStdPoint()
    {
        return (com.kingdee.shr.costbudget.CmpStdPointInfo)get("stdPoint");
    }
    public void setStdPoint(com.kingdee.shr.costbudget.CmpStdPointInfo item)
    {
        put("stdPoint", item);
    }
    /**
     * Object: null 's ԭн���׼ property 
     */
    public com.kingdee.shr.costbudget.CmpStandardInfo getOldCmpStandard()
    {
        return (com.kingdee.shr.costbudget.CmpStandardInfo)get("oldCmpStandard");
    }
    public void setOldCmpStandard(com.kingdee.shr.costbudget.CmpStandardInfo item)
    {
        put("oldCmpStandard", item);
    }
    /**
     * Object: null 's ԭн�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdLevelInfo getOldCmpLevel()
    {
        return (com.kingdee.shr.costbudget.CmpStdLevelInfo)get("oldCmpLevel");
    }
    public void setOldCmpLevel(com.kingdee.shr.costbudget.CmpStdLevelInfo item)
    {
        put("oldCmpLevel", item);
    }
    /**
     * Object: null 's ԭн�� property 
     */
    public com.kingdee.shr.costbudget.CmpStdPointInfo getOldStdPoint()
    {
        return (com.kingdee.shr.costbudget.CmpStdPointInfo)get("oldStdPoint");
    }
    public void setOldStdPoint(com.kingdee.shr.costbudget.CmpStdPointInfo item)
    {
        put("oldStdPoint", item);
    }
    /**
     * Object:null's ԭֵproperty 
     */
    public java.math.BigDecimal getOldMoney()
    {
        return getBigDecimal("oldMoney");
    }
    public void setOldMoney(java.math.BigDecimal item)
    {
        setBigDecimal("oldMoney", item);
    }
    /**
     * Object: null 's null property 
     */
    public com.kingdee.eas.hr.affair.FluctuationBizBillInfo getParent()
    {
        return (com.kingdee.eas.hr.affair.FluctuationBizBillInfo)get("parent");
    }
    public void setParent(com.kingdee.eas.hr.affair.FluctuationBizBillInfo item)
    {
        put("parent", item);
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("496C9394");
    }
}