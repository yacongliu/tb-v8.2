package com.kingdee.eas.hr.affair;

import java.io.Serializable;
import com.kingdee.bos.dao.AbstractObjectValue;
import java.util.Locale;
import com.kingdee.util.TypeConversionUtils;
import com.kingdee.bos.util.BOSObjectType;


public class AbstractFluctuationBizBillInfo extends com.kingdee.eas.hr.affair.HRAffairBizBillInfo implements Serializable 
{
    public AbstractFluctuationBizBillInfo()
    {
        this("id");
    }
    protected AbstractFluctuationBizBillInfo(String pkField)
    {
        super(pkField);
        put("secentry", new com.kingdee.eas.hr.affair.SecEntryCollection());
        put("entrys", new com.kingdee.eas.hr.affair.FluctuationBizBillEntryCollection());
    }
    /**
     * Object: Ա������ 's ��¼ property 
     */
    public com.kingdee.eas.hr.affair.FluctuationBizBillEntryCollection getEntrys()
    {
        return (com.kingdee.eas.hr.affair.FluctuationBizBillEntryCollection)get("entrys");
    }
    /**
     * Object:Ա������'s �Ƿ�����property 
     */
    public boolean isIsLoanBill()
    {
        return getBoolean("isLoanBill");
    }
    public void setIsLoanBill(boolean item)
    {
        setBoolean("isLoanBill", item);
    }
    /**
     * Object:Ա������'s �Ƿ���˵���property 
     */
    public boolean isIsMulti()
    {
        return getBoolean("isMulti");
    }
    public void setIsMulti(boolean item)
    {
        setBoolean("isMulti", item);
    }
    /**
     * Object: Ա������ 's null property 
     */
    public com.kingdee.eas.hr.affair.SecEntryCollection getSecentry()
    {
        return (com.kingdee.eas.hr.affair.SecEntryCollection)get("secentry");
    }
    public BOSObjectType getBOSType()
    {
        return new BOSObjectType("C0DAD00D");
    }
}