package com.kingdee.eas.hr.affair.app;

import java.util.Date;

import org.apache.log4j.Logger;

import com.kingdee.bos.BOSException;
import com.kingdee.bos.Context;
import com.kingdee.bos.dao.ormapping.ObjectUuidPK;
import com.kingdee.eas.basedata.org.AdminOrgUnitInfo;
import com.kingdee.eas.basedata.org.IAdminOrgUnit;
import com.kingdee.eas.basedata.org.IPosition;
import com.kingdee.eas.basedata.org.JobInfo;
import com.kingdee.eas.basedata.org.PositionInfo;
import com.kingdee.eas.basedata.person.PersonInfo;
import com.kingdee.eas.common.EASBizException;
import com.kingdee.eas.hr.affair.HRAffairBizBillEntryInfo;
import com.kingdee.eas.hr.affair.ResignBizBillEntryInfo;
import com.kingdee.eas.hr.affair.ResignBizBillFactory;
import com.kingdee.eas.hr.affair.ResignBizBillInfo;
import com.kingdee.eas.hr.base.AffairActionReasonInfo;
import com.kingdee.eas.hr.base.EmployeeTypeInfo;
import com.kingdee.eas.hr.base.HRBillStateEnum;
import com.kingdee.eas.hr.base.HRBizDefineInfo;
import com.kingdee.eas.hr.tools.BaseDataTools;
import com.kingdee.eas.hrcus.worklist.IRedign;
import com.kingdee.eas.hrcus.worklist.RedignFactory;
import com.kingdee.eas.hrcus.worklist.RedignInfo;
import com.kingdee.util.NumericExceptionSubItem;
import com.kingdee.util.StringUtils;

/**
 * @Copyright 天津金蝶软件有限公式 <br>
 *            Title: ResignBizBillControllerBeanEx <br>
 *            Description: 离职单扩展类
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-1-15 & 上午09:54:40
 * @since V1.0
 */
public class ResignBizBillControllerBeanEx extends ResignBizBillControllerBean {
    private static Logger logger = Logger
            .getLogger("com.kingdee.eas.hr.affair.app.ResignBizBillControllerBeanEx");

    private IAdminOrgUnit iAdminOrgUnit = null;

    private IPosition iPosition = null;

    @Override
    protected void _doWithCommonRelationEffect(Context ctx, HRAffairBizBillEntryInfo entry)
            throws BOSException, EASBizException {
        super._doWithCommonRelationEffect(ctx, entry);

        // TODO: 同步离职工作清单
        System.out.println("*************************************离职单已触发开始同步离职工作清单数据");

        ResignBizBillEntryInfo resignEntry = (ResignBizBillEntryInfo) entry;
        ResignBizBillInfo bill = resignEntry.getBill();

        saveResignWorkList(ctx, bill);

        System.out.println("*************************************离职单同步离职工作清单数据已完成");
    }

    @Override
    protected void _untiCheckBizBill(Context ctx, String billId) throws BOSException, EASBizException {
        // 撤回离职工作清单 撤回规则：离职单编码
        ResignBizBillInfo billInfo = ResignBizBillFactory.getLocalInstance(ctx).getResignBizBillInfo(
                new ObjectUuidPK(billId));
        revokeRedignWorkList(ctx, billInfo.getNumber());

        super._untiCheckBizBill(ctx, billId);
    }

    /**
     * 
     * <p>
     * Title: revokeRedignWorkList
     * </p>
     * <p>
     * Description: 撤回离职工作清单
     * </p>
     * 
     * @param ctx
     * @param number
     * @throws EASBizException
     * @throws BOSException
     */
    private void revokeRedignWorkList(Context ctx, String number) throws EASBizException, BOSException {

        if (StringUtils.isEmpty(number)) {
            logger.error("**********************撤回离职工作清单数据参数 number is  null");
            return;
        }

        IRedign iRedign = RedignFactory.getLocalInstance(ctx);
        // 离职单据编码存在则删除，不存在则不操作
        boolean exist = iRedign.exists(" where resignBillNum = '" + number + "'");
        if (exist) {

            try {
                iRedign.delete(" where resignBillNum = '" + number + "'");

                logger.info("******************************************撤回离职工作清单数据完成");
            } catch (EASBizException e) {
                throw new EASBizException(new NumericExceptionSubItem("01", "删除离职单编码为'" + number
                        + "' 的离职工作清单失败 "));
            } catch (BOSException e) {
                throw new BOSException(e);
            }
        }
    }

    /**
     * 
     * <p>
     * Title: saveResignWorkList
     * </p>
     * <p>
     * Description: 同步离职工作清单
     * </p>
     * 
     * @param ctx
     * @param bill
     * @throws EASBizException
     * @throws BOSException
     */
    private void saveResignWorkList(Context ctx, ResignBizBillInfo bill) throws EASBizException, BOSException {

        if (bill == null) {
            logger.error("**************************同步离职工作清单参数：ResignBizBillInfo is null");
            return;
        }

        // 离职单编码
        String resignBizBillnumber = bill.getNumber();
        ResignBizBillEntryInfo entryInfo = bill.getEntrys().get(0);

        PersonInfo person = entryInfo.getPerson();
        String personId = null;
        if (person != null) {
            personId = person.getId().toString();
        }

        PersonInfo personInfo = BaseDataTools.getPersonInfo(ctx, personId);
        // 员工姓名
        String personName = null;
        // 员工编码
        String empNumber = null;
        if (personInfo != null) {
            personName = personInfo.getName();
            empNumber = personInfo.getNumber();
        }

        // 职务
        JobInfo job = entryInfo.getJob();
        String jobId = null;
        if (job != null) {
            jobId = job.getId().toString();
        }
        JobInfo jobInfo = BaseDataTools.getJobInfo(ctx, jobId);
        String jobName = null;
        if (jobInfo != null) {
            jobName = jobInfo.getName();
        }

        // 所属组织
        AdminOrgUnitInfo oldAdminOrg = entryInfo.getOldAdminOrg();
        String adminOrgId = null;
        if (oldAdminOrg != null) {
            adminOrgId = oldAdminOrg.getId().toString();
        }
        AdminOrgUnitInfo adminOrgUnitInfo = BaseDataTools.getAdminOrgUnitInfo(ctx, adminOrgId);
        String adminOrgName = null;
        if (adminOrgUnitInfo != null) {
            adminOrgName = adminOrgUnitInfo.getName();
        }
        // 职位
        PositionInfo position = entryInfo.getPosition();
        String positionId = null;
        if (position != null) {
            positionId = position.getId().toString();
        }
        PositionInfo positionInfo = BaseDataTools.getPositionInfo(ctx, positionId);
        String positionName = null;
        if (positionInfo != null) {
            positionName = positionInfo.getName();
        }

        // 变动操作
        HRBizDefineInfo hrBizDefine = entryInfo.getHrBizDefine();
        String hrBizDefineId = null;
        if (hrBizDefine != null) {
            hrBizDefineId = hrBizDefine.getId().toString();
        }
        HRBizDefineInfo bizDefineInfo = BaseDataTools.getHRBizDefine(ctx, hrBizDefineId);
        String operation = null;
        if (bizDefineInfo != null) {
            operation = bizDefineInfo.getName();
        }

        // 变动类型
        AffairActionReasonInfo affairActionReason = entryInfo.getAffairActionReason();
        String reasonId = null;
        if (affairActionReason != null) {
            reasonId = affairActionReason.getId().toString();
        }
        AffairActionReasonInfo affairActionReasonInfo = BaseDataTools.getAffairActionReason(ctx, reasonId);
        String type = null;
        if (affairActionReasonInfo != null) {
            type = affairActionReasonInfo.getName();
        }

        // 目标用工状态
        EmployeeTypeInfo empType = entryInfo.getEmpType();
        String empTypeId = null;
        if (empType != null) {
            empTypeId = empType.getId().toString();
        }
        EmployeeTypeInfo employeeTypeInfo = BaseDataTools.getEmployeeType(ctx, empTypeId);
        String empState = null;
        if (employeeTypeInfo != null) {
            empState = employeeTypeInfo.getName();
        }

        // 离职生效日期
        Date bizDate = entryInfo.getBizDate();

        // 构造离职工作清单
        RedignInfo redignInfo = new RedignInfo();
        redignInfo.setResignBillNum(resignBizBillnumber);
        redignInfo.setEmpNum(empNumber);
        redignInfo.setEmpName(personName);
        redignInfo.setPost(jobName);
        redignInfo.setPosition(positionName);
        redignInfo.setOrgName(adminOrgName);
        redignInfo.setCOperation(operation);
        redignInfo.setCType(type);
        redignInfo.setEmpState(empState);
        redignInfo.setEffectDate(bizDate);
        redignInfo.setBillState(HRBillStateEnum.SAVED);

        RedignFactory.getLocalInstance(ctx).save(redignInfo);

    }

}
