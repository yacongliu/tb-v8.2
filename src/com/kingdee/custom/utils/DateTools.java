package com.kingdee.custom.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * Title: DateTools
 * <p>
 * Description: 日期工具类
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-8-6 & 上午09:40:51
 * @since V1.0
 */
public class DateTools {

    private static Calendar CALENDER = Calendar.getInstance();

    public static int getYear(Date date) {
        CALENDER.setTime(date);
        return CALENDER.get(Calendar.YEAR);
    }
    
    public static int getCurrentYear() {
        CALENDER.setTime(getCurrentDate());
        return CALENDER.get(Calendar.YEAR);
    }

    public static int getMonth(Date date) {
        CALENDER.setTime(date);
        return CALENDER.get(Calendar.MONTH) + 1;
    }
    
    public static int getCurrentMonth() {
        CALENDER.setTime(getCurrentDate());
        return CALENDER.get(Calendar.MONTH) + 1;
    }

    public static int getDay(Date date) {
        CALENDER.setTime(date);
        return CALENDER.get(Calendar.DAY_OF_MONTH);
    }
    
    public static int getCurrentDay() {
        CALENDER.setTime(getCurrentDate());
        return CALENDER.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 
     * <p>
     * Title: getDate
     * </p>
     * <p>
     * Description: 参数日期为空时 返回系统当前日期
     * </p>
     * 
     * @param date
     * @return Date
     */
    public static Date getDate(Date date) {
        if (date == null) {
            return getCurrentDate();
        }

        return date;
    }

    /**
     * 
     * <p>
     * Title: getcurrentDate
     * </p>
     * <p>
     * Description: 获取系统当前日期
     * </p>
     * 
     * @return Date
     */
    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

}
