package com.kingdee.custom.utils;

import com.kingdee.util.StringUtils;

/**
 * 
 * Title: StringTools
 * <p>
 * Description: 字符串工具类
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2019-8-6 & 上午09:14:31
 * @since V1.0
 */
public class StringTools {

    /**
     * 
     * <p>
     * Title: convertObjectToString
     * </p>
     * <p>
     * Description: 对象转字符串 为空时返回 ""
     * </p>
     * 
     * @param obj
     * @return String
     */
    public static String convertObjectToString(Object obj) {
        return convertObjectToString(obj, "");
    }

    /**
     * 
     * <p>
     * Title: convertObjectToString
     * </p>
     * <p>
     * Description: 对象转字符串
     * </p>
     * 
     * @param obj
     * @param defaultValue 默认值 （对象为空时并且默认值不为空时 返回默认值）
     * @return String
     */
    public static String convertObjectToString(Object obj, String defaultValue) {

        String value = "";

        if (obj != null) {
            value = obj.toString();
        }

        if (isNotEmpty(defaultValue)) {
            value = defaultValue;
        }
        return value;
    }

    public static boolean isEmpty(String value) {
        return StringUtils.isEmpty(value);
    }

    public static boolean isNotEmpty(String value) {
        return !StringUtils.isEmpty(value);
    }

}
