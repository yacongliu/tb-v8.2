# TB 开发记录 非常重要!!!!

## 特别注意

- **push代码之前必须先pull文件下来，不能直接push代码到仓库中！**

- **如有多人需要对同一文件进行修改时，私下进行合并处理，合并之后再进行push!**

- **不允许提交与项目开发无关内容，及测试内容！**

- **模块中需详细描述开发记录！**

### 考勤异常单

开发者：杨旭东

- 需求 

  1.考勤异常单 选择多人进行录入考勤异常信息

  2.选择员工自动携带部门信息

  3.考勤函数





视图：

考勤异常类型form：com.kingdee.eas.custom.atsexc.bd.app.Type.form

考勤异常类型list：    com.kingdee.eas.custom.atsexc.bd.app.Type.list

考勤异常单form：    com.kingdee.eas.custom.atsexc.app.AtsExceptionBill.form

考勤异常单list：        com.kingdee.eas.custom.atsexc.app.AtsExceptionBill.list

Js:

考勤异常单form：        custom/web/js/shr/ats/atsexceptionEdit.js

考勤异常单list：            custom/web/js/shr/ats/AtsExceptionBilllist.js

Handler: `com.kingdee.eas.custom.web.handler.atsexception.AtsExceptionBillEditHandler`





### 值班单

- 开发者：马伟楠

- 需求

1.单据包括姓名、部门（由姓名带出）、值班费用（金额、手动输入） 、发放时间（文本、手动输入） 

  包括提交、提交生效和反审批功能

2. 薪酬函数

- uipk:

1.值班费用核算list：

```
com.kingdee.eas.custom.dutycost.app.DutyCost.list值班费用核算form：
```

```
com.kingdee.eas.custom.dutycost.app.DutyCost.formJs:
```

2.值班费用核算form：

```
custom/web/js/shr/duty/DutyCostJs.js值班费用核算 list： 
```

```
custom/web/js/shr/duty/DutyCostList.js
```

3.Handler: 

```
com.kingdee.eas.custom.web.handler.dutycost.DutyCostBillHandler
```

```
com.kingdee.eas.custom.web.handler.dutycost.DutyCostListHandler
```





### 班车补助单

开发者：程赛赛

- 需求

1. 单据包括姓名、部门（由姓名带出）、调整类型（基础资料）、调整前班车费、调整后班车费、调整月份、调整情况说明。                         
2. 包括提交、提交生效和反审批功能
3. 薪酬函数



视图：

​	uipk:    

```
com.kingdee.eas.custom.buscostchange.app.BusCostChange.list
```

```
 com.kingdee.eas.custom.buscostchange.app.BusCostChange.form
```

Js：

```
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\buscost\buscostcount.js

```

```
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\buscost\buscostcountlist.js

```

Handler：

```
com.kingdee.shr.custom.handler.buscostcount.BusCostCountEditHandler	

```

```
com.kingdee.shr.custom.handler.buscostcount.BusCostCountListHandler

```



### 自定义核算函数 

核算函数对应表：t_hr_sformulafunc

#### 取班车费用

函数定义：FunBusCostCount

函数类型：公用业务函数

```
public double FunBusCostCount(String type) {
	com.kingdee.shr.compensation.service.CalFunctionServiceExt functionService = new com.kingdee.shr.compensation.service.CalFunctionServiceExt();
	return functionService.getBusCostCoutData(type, paramMap,getTempMap(),getCalScheme(),getMainTableRowFilter());
}

```

后台数据 Insert:

```
insert into t_hr_sformulafunc(FID, FTYPE, FDEFINE, FRETURNTYPE, FPARAMS, FSORTSN, FNAME_L1, FNAME_L2, FNAME_L3, FNUMBER, FDESCRIPTION_L1, FDESCRIPTION_L2, FDESCRIPTION_L3, FSIMPLENAME, FCREATORID, FCREATETIME, FLASTUPDATEUSERID, FLASTUPDATETIME, FCONTROLUNITID, FCONTENT)
 values 
(N'7jsAAAAF91gkATlo', 6, N'FunBusCostCount', 0, N'String', 3, null, N'取班车费用', null, null, null, N'参数说明：
type：调整类型编码', null, null, '00000000-0000-0000-0000-00000000000013B7DE7F', {ts'2019-08-06 11:41:00'}, '00000000-0000-0000-0000-00000000000013B7DE7F', {ts'2019-08-06 16:42:15'}, '00000000-0000-0000-0000-000000000000CCE7AED4', N'public double FunBusCostCount(String type) {
	com.kingdee.shr.compensation.service.CalFunctionServiceExt functionService = new com.kingdee.shr.compensation.service.CalFunctionServiceExt();
	return functionService.getBusCostCoutData(type, paramMap,getTempMap(),getCalScheme(),getMainTableRowFilter());
}');


```





### 请假单

开发者：宋佩骏

- 需求

1. 请假为年休假 增加提交功能内校验

视图：

​	uipk:    

```
com.kingdee.eas.hr.ats.app.AtsLeaveBillForm

```

```
om.kingdee.eas.hr.ats.app.AtsLeaveBillAllForm

```

```
com.kingdee.eas.hr.ats.app.AtsLeaveBillAllList

```

Js：

```
D:\kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\attendmanage\web\js\shr\ats\atsLeaveBillBatchEdit.js

```

Handler：

```
com.kingdee.shr.ats.web.handler.custom.AtsLeaveBillEditHandlerEx

```





### 请假单自动显示紧急联系人、请假助手

开发者：程赛赛

- 需求

  根据请假单，自动显示当前登录人的紧急联系人及联系信息

  添加一个请假助手按钮，并弹出请假助手弹框

视图：

​	uipk：

```
com.kingdee.eas.hr.ats.app.AtsLeaveBillAllForm

```

Js:

```
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\atsbill\atsLeaveBillEditExt.js

```

Handler：

```
com.kingdee.shr.ats.web.handler.custom.AtsLeaveBillEditHandlerExt

```



### 清除调休假剩余额度

- 开发者：相建彬

- 需求

  在假期列表页通过点击‘’清除额度‘按钮跳转到清除额度列表页面，在清除额度页面以“生效日期”和“调休假”为查询条件进行查询。点击“清除额度”按钮对所选信息进行额度清除（剩余额度，标准额度，实际额度，已用额度置为0)

- 视图： 

  假期额度列表：`com.kingdee.eas.hr.ats.app.HolidayLimit.list`

  清除假期额度列表：`com.kingdee.eas.hr.ats.app.ClearHolidayLimit.list`

- js： 

  假期额度列表：`custom/web/js/shr/holiday/holidayLimitListEx.js`
  清除假期额度列表：`custom/web/js/shr/holiday/ClearholidayLimitList.js`

- handler：

  `com.kingdee.shr.custom.handler.holiday.ClearHolidayLimitListHandler`

### 考核评分

- 开发者：杨旭东

- 需求：

  1) 单据编号：根据编码规则，系统自动生成。

  2) 提交人：系统自动生成，显示当前登陆用户。

  3) 提交日期：系统自动生成，显示当前系统日期。

  4) 部门：默认显示当前登陆用户所在部门名称。

  5) 被考核人：必选，根据员工基础数据选择。

  6) 被考核人岗位：系统自动带出被考核人主要任职岗位名称。

  7) 负面清单项目、评价标准：不可维护，默认与已审批通过的《部门季度工作目标清单》一致。

  8) 补充说明：当负面清单和评价标准有调整或者有其它需要说明的内容时，维护“补充说明”，非必录。

  9) 扣分：整数，必录，手工录入。分数录入范围是0—100。

  10) 考核成绩：系统根据扣分自动计算，考核成绩=100-扣分。

  11)部门负责人打完分数提交后，通过工作流流转至被考核人进行考核确认。确认后流转至集团人力资源部备案。

- 视图： 

  考核评分列表：`com.kingdee.eas.custom.examinscore.app.Examinscores.list

  考核评分表单：`com.kingdee.eas.custom.examinscore.app.Examinscores.form

- js： 

  假期额度列表：custom/web/js/shr/exam/examinscore.js

- handler：

  `com.kingdee.shr.custom.handler.examinscore.ExaminScoreHandler
  

### 部门季度工作目标清单

开发者：程赛赛

- 需求
单据字段说明：
1)单据编号：根据编码规则，系统自动生成。
2)提交人：系统自动生成，显示当前登陆用户。
3)提交日期：系统自动生成，显示当前系统日期。
4)部门：默认显示当前登陆用户所在部门名称。
5)负面清单项目：从指标库基础数据中选择绩效指标，不可直接录入。
6)评价标准：在选择负面清单项目后自动带出，可编辑。
  

视图：

​	uipk：

```
com.kingdee.eas.custom.deptseasonworklist.app.DeptWorklist.list
com.kingdee.eas.custom.deptseasonworklist.app.DeptWorklist.form

```

Js:

```
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\deptworklist\deptSeasonWorklist.js
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\deptworklist\deptSeasonWorklistlist.js

```

Handler：

```
com.kingdee.shr.custom.handler.deptworklist.DeptWorklistEditHandlerEx
com.kingdee.shr.custom.handler.deptworklist.DeptWorklistListHandlerEx

```

部门绩效考核清单

开发者：程赛赛

需求：
一张新增单据，一张维护单据，一张维护类型基础资料

视图：
uipk:
com.kingdee.eas.custom.empitemlist.app.EmpSafeguardList.list
com.kingdee.eas.custom.empitemlist.app.EmpSafeguardList.form
com.kingdee.eas.custom.empitemlist.app.EmpItemList.list
com.kingdee.eas.custom.empitemlist.app.EmpItemList.form
com.kingdee.eas.custom.empitemlist.app.SafeguardType.list
com.kingdee.eas.custom.empitemlist.app.SafeguardType.form

js:
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\empitem\empitemlist.js
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\empitem\empitemlistlist.js
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\empitem\empsafeguardlist.js
D:\TOEC\Kingdee\eas\server\deploy\easweb.ear\shr_web.war\addon\custom\web\js\shr\empitem\empsafeguardlistlist.js

Handler:
com.kingdee.shr.custom.handler.empitemlist.EmpItemEditHandler
com.kingdee.shr.custom.handler.empitemlist.EmpItemListHandler
com.kingdee.shr.custom.handler.empitemlist.EmpSafeguardEditHandler
com.kingdee.shr.custom.handler.empitemlist.EmpSafeguardListHandler
  
### 招聘模块-面试结果单据
- 开发者：相建彬
- 需求：
1、面试方案为基础数据； 
2、面试环节自动显示，单选 
3、排名规则待定，点击排名后数据写入到单据中； 
4、选择通过后，平均分、通过率、排名都根据编码同步到对应的面试环节的HR面试结果中。 

- 视图：

		`com.kingdee.eas.custom.recruitment.app.Interviewresults.list`
		`com.kingdee.eas.custom.recruitment.app.Interviewresults.form`
- js：`\easweb.ear\shr_web.war\addon\custom\web\js\shr\recruitment\interviewResultsList.js`
- handler：`D:\kingdee\eas\server\lib\addon\custom\recruitment\interviewResultsList_handler.jar`