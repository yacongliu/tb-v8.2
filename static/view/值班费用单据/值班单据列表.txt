名称   值班费用核算列表

视图类型   列表

模型     com.kingdee.eas.custom.dutycost.app.DutyCost

UIPK com.kingdee.eas.custom.dutycost.app.DutyCost.list
扩展
<?xml version="1.0"?>
<list name="DutyCost" sorterItems="number asc">
<header>
	<title value="值班费用列表"></title>
          <script src="${appPath}/custom/web/js/shr/duty/DutyCostListJs.js"></script>
		<jsClass name="shr.duty.DutyCostListJs"/>
		<handler class="com.kingdee.shr.custom.handler.dutycost.DutyCostEditHandler"/>
	<button name="againstApprove" visable="true" caption="反审批"></button>
</header>

	<field name="number" />
	<field name="creator" label="制单人"/>
	<field name="bizDate"  label="制单日期" ></field>
	<field name="billState" label="单据状态"/>
</list>