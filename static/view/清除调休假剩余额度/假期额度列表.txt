模型：com.kingdee.eas.hr.ats.app.HolidayLimit
uipk：com.kingdee.eas.hr.ats.app.HolidayLimit.list
视图：
<?xml version="1.0"?>
<navigateList  relatedField="adminOrgUnit"> 
  <header>
        <searchView uipk="com.kingdee.eas.hr.ats.app.HolidayLimit.search" />
        <title value="假期额度列表"/>
  	<script src="${appPath}/${appCode}/web/js/shr/ats/holidayLimitList.js"></script>
	<script src="${appPath}/custom/web/js/shr/holiday/holidayLimitListEx.js"></script>
	<jsClass name="shr.holiday.holidayLimitListEx" />
        <handler class="com.kingdee.shr.ats.web.handler.HolidayLimitListHandler" />

       <toolbar>
		<button name="addNew" visible="false" />
		<button name="delete" visible="false" />
		<button name="generate" visible="true" caption="生成额度" class="btn-primary" />
		<button name="toclearview" visible="true" caption="清除额度" class="btn-primary" />
		<button name="batchExtension" caption="批量延期" class="shrbtn-primary"/>
                <button name="leftGenerate" caption="离职额度处理" class="shrbtn-primary"/>
		<dropDownButton name="imp" caption="导入">
			<button name="addImport" visible="true" caption="新增导入" class="btn-primary" />
			<button name="modifyImport" visible="true" caption="修改导入" class="btn-primary" />
			<button name="initalizeImport" visible="true" caption="初始化导入" class="btn-primary" />
		</dropDownButton>
		<dropDownButton name="exp" caption="导出">
			<button name="exportCurrent" caption="导出选中" />
			<button name="exportToExcel" caption="导出全部" class="shrbtn-primary"/>	
		</dropDownButton>
	<dropDownButton name="more" caption="更多">
		<button name="delete" caption="删除" visible="true" />
		<button name="audit" caption="审核" class="shrbtn-primary"/>
		<button name="antiAudit" caption="反审核" class="shrbtn-primary"/>
	</dropDownButton>
	</toolbar>
     </header>

     <treeNavigation>
		<navDomain> [('id','=','$UserAdminRangeFilter')] </navDomain>
    </treeNavigation>
<grid  name="holidayLimitList"   shrinkToFit="false"  rowList="[15, 30, 50]" rowNum="30" multiselect="true"  query="com.kingdee.eas.hr.ats.app.HolidayLimitQuery" sorterItems="id">
  <field name="id"  type="hidden" frozen="true"></field>
  <field name="holidayPolicy.id" type="hidden" frozen="true"></field>
  <field name="holidayPolicy.holidayType.id" type="hidden" frozen="true"></field>

  <field name="proposer.id" type="hidden" frozen="true"></field>
  <field name="proposer.number" label="员工编码" width="100" frozen="true"></field>
  <field name="proposer.name" label="姓名" width="100" frozen="true"></field>
  <field name="holidayType.name" label="假期类型" width="100"  frozen="true"></field>
  
  <field name="cycleBeginDate" label="周期开始日期"></field>
  <field name="cycleEndDate" label="周期结束日期"></field>
  <field name="effectDate" label="生效日期"></field>
  <field name="delayDate" label="延期日期"></field>
  
  <field name="holidayUnit" label="假期单位"  width="55"></field>
  
  <field name="standardLimit" label="标准额度" width="55" ></field>
  <field name="addOrSubLimit" label="增减额度"  width="55"></field>
  <field name="realLimit" label="实际额度"  width="55"></field>
  <field name="usedLimit" label="已用额度" width="55" ></field>
  <field name="freezeLimit" label="在途额度"  width="55"></field>
  <field name="preOverdraftLimit" label="上期透支额度"  width="80"></field>
  <field name="remainLimit" label="剩余额度"  width="55"></field>
  <field name="employeeType.name" label="用工关系状态"  width="55"></field>
  <field name="employeeType.isInCount" label="是否占人头"  type="hidden"></field>
  <field name="status" label="状态" width="50"></field>
</grid>
<div id="main">
 <div id="dialogTransaction" style="overflow:hidden;">   
   <iframe id="operationDialog-frame" name="operationDialog-frame" width="100%" height="100%" frameborder="0" allowtransparency="true" src=""></iframe>
  </div>
</div>
</navigateList>