UIPK
com.kingdee.shr.recuritment.app.LinkAndPositionSetting
视图扩展：
<?xml version="1.0" encoding="utf-8"?>
<main>
	<header>
		<title value="面试环节设置"/>
		<jsClass name="shr.rec.LinkAndPositionSetting"/>
		<script src="${appPath}/${appCode}/web/js/shr/recuritment/interviewProcessSetting/handlebars-1.0.0.beta.6.js"></script> 
		<style href="${appPath}/${appCode}/web/css/shr/recuritment/interviewProcessSetting/link_post_setting.css"/>
		<handler class="com.kingdee.shr.recuritment.web.handler.LinkAndPositionSettingHandler"/>
		<script src="${appPath}/${appCode}/web/js/shr/recuritment/interviewProcessSetting/linkAndPositionSetting.js"></script>
		<toolbar>
			<button name="addNew" visible="false"/>
			<button name="delete" visible="false"/>  
			<button name="save" caption="保存" class="btn-primary" />
		</toolbar>      
	</header>
	<body>
		<div class="wrap">
			<div class="view_manager_body">
				<div class="title">
					<h5>添加/编辑面试环节</h5>
				</div>
				<table class="tab_interview_process" >
					<thead>
						<tr>
							<th>面试顺序</th>
							<th>环节名称</th>
							<th>最终环节</th>
							<th>必要环节</th>
							<th>权重</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody id="interview_process">
						
					</tbody>
				</table>
				<div class="addtrwrap"><span class="addtr"></span>添加环节</div>
				<div class="permissions">
					<h5>权限设置</h5>
					<ul class="ul_permissions">
						<li>
							<span class="radiowrap" power="1"><!-- inp_radio_select -->
								<input type="radio" id="checkbox1"  name="permissions" />
							</span>
							<label for="checkbox1" style="float:left;">面试官不能查看之后所有环节的面试结果</label>
						</li>
						<li>
							<span class="radiowrap" power="2">
								<input type="radio" id="checkbox2"  name="permissions" />
							</span>
							<label for="checkbox2" style="float:left;">面试官只能查看当前环节的面试结果</label>
						</li>
						<li>
							<span class="radiowrap" power="3">
								<input type="radio" id="checkbox3"  name="permissions" />
							</span>
							<label for="checkbox3" style="float:left;">面试官能查看所有环节的面试结果</label>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</body>
</main>