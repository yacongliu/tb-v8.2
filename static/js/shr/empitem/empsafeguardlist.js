shr.defineClass("shr.empitem.empsafeguardlist",shr.framework.Edit,{
	initalizeDOM:function(){
		var that = this ;
		shr.empitem.empsafeguardlist.superClass.initalizeDOM.call(this);
		
			if(this.getOperateState() != "VIEW"){
			that.listenCSS();
		}
		
		that.setButtonVisible();

	},
	/**
	 * 
	 * (HRBillStateEnum)		||  (BizStateEnum)
	 * 设置编辑按钮是否隐藏		||  对应EAS7.5 Version 审批状态字段值<br/>
	 * 0-save  	  未提交			||  -1  未提交					   	<br/>
	 * 1-submited 未审批			||   0  未审核					   	<br/>
	 * 2-auditing 审批中			||   1  审核中					     <br/>
	 * 3-audited  审批通过		||   3  审核完成					   	 <br/>
	 * 4-auditend 审批不通过		||   4  审核终止					 <br/>
	 *
	 */
	setButtonVisible: function () {

		var self = this;
		var billState = $("#billState").val();
		if(billState != '未提交' && billState!=undefined && billState != 0){
			$("#submit").hide();
			$("#submitEffect").hide();
			$("#edit").hide();
			$("#auditResult").hide();
		}
		if(billState==0 && self.getOperateState() == 'VIEW'){
			$("#submit").hide();
			$("#submitEffect").hide();
		}
	},
	listenCSS(){
		var a=this;
		
		var grid = waf("#entrys");
		
		//console.log("进入考核指标监听menthod");
		grid.wafGrid("option", {
			afterSaveCell:function (rowid, cellname, value, iRow, iCol) {
               	if(cellname == "examProjects"){
					var id = value.id;
					
					a.remoteCall( {
						method : 'getStandard',
						param : {
							id : id
						},
						success : function(data) {
							var val = data.standard;
							var value = data.isPass;
							$("#entrys").jqGrid('setCell',rowid,"evalStandard",val);
							$("#entrys").jqGrid('setCell',rowid,"state",value);
						}
						
					});
				}
            }
        });
	},
	/**
	 * 单据提交生效
	 * @param {} event
	 */
	submitEffectAction : function (event) {
		var _self = this;
		_self.remoteCall({
			method:'canSubmitEffect',
			success:function(data){
				if("success" == data.state){
					if(_self.getOperateState() == 'VIEW' ){//&& _self.checkBillState()
						shr.showConfirm('您确认要提交生效吗？', function() {		
							_self.prepareSubmitEffect(event, 'submitEffect');
							_self.back();
						});
					}else{
						if (_self.validate() && _self.verify() ) {	//&& _self.checkBillState()
							shr.showConfirm('您确认要提交生效吗？', function() {		
								_self.prepareSubmitEffect(event, 'submitEffect');
							});
						}
					}
				}
			}
		});
	},
	prepareSubmitEffect : function (event, action){
		var _self = this;
		var data = _self.assembleSaveData(action);
		
		var target;
		if (event && event.currentTarget) {
			target = event.currentTarget;
		}

		shr.doAction({
			target: target,
			url: _self.dynamicPage_url,
			type: 'post', 
			data: data,
			success : function(response) {
				_self.back();
			}
		});	
	},
	//组装保存时传至服务端的数据
	assembleSaveData: function(action){
		var _self = this;
		var data =_self.prepareParam(action + 'Action');
		data.method = action;
		data.operateState = _self.getOperateState();
		data.model = shr.toJSON(_self.assembleModel()); 

		var model = data.model;
		if(model != undefined && model != "") {
			data.model = model.replace(/<pre\sclass=\\"textarea-format\\">/gi,"").replace(/<\/pre>/gi,""); //抹掉因为在view界面中对富文本调.html()方法而带出的标签
		}

		// relatedFieldId
		var relatedFieldId = this.getRelatedFieldId();
		if (relatedFieldId) {
			data.relatedFieldId = relatedFieldId;
		}
		
		return data;
	}
	
});