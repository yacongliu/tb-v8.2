shr.defineClass("shr.holiday.holidayLimitListEx", shr.ats.HolidayLimitList, {
	setParamValue :function(){
		 var $grid = $(this.gridId);
		 var billId = $.getUrlParam('billId');
		 if(billId != undefined && billId != null && billId != ''){
		 	//$("#generate").hide();
		 	$("#delete").hide();
		 	/*
		 	 * add by:great_chen
		 	 * date: 2014-08-06
		 	 * function：将下面的按钮隐藏
		 	 */
		 	$("#addImport").hide(); //新增导入
		 	$("#modifyImport").hide(); //修改导入
		 	$("#initalizeImport").hide(); //初始化导入
		 }
		 else{
		 	//$("#generate").show();
		 	$("#delete").show();
		 	$("#addImport").show(); //新增导入
		 	$("#modifyImport").show(); //修改导入
		 	$("#initalizeImport").show(); //初始化导入
		 }
		 var myPostData={
			  billId: decodeURIComponent(billId)
		};
		$grid.jqGrid("option",'postData', myPostData);//postData来传递我们  定制的参数 jquery.jqGrid.extend.js
		document.documentElement.style.overflow='visible';
	}, 
	initalizeDOM : function () {
		var _self = this;
		_self.setParamValue();
		_self.pageFormatter();//页面微调
		shr.holiday.holidayLimitListEx.superClass.initalizeDOM.call(this);
	}
	
	,pageFormatter : function () {
		//从考勤档案、假期档案进入此页面，此页面中部分按钮需要去掉，去掉的按钮包括：导入、查看后台事务、批量延期
		var previousURL = document.referrer;
		//在假期额度页面点击生成额度再返回假期额度页面会破坏previousURL，故读取面包屑中的数据
		var breadcrumbDatas = shrDataManager.pageNavigationStore.getDatas();
		if(breadcrumbDatas[breadcrumbDatas.length-2]==undefined){
			return;
		}
		var previousURL_two = breadcrumbDatas[breadcrumbDatas.length-2].url;//倒数第二个页面的url是要监控的url
		if (previousURL.indexOf("com.kingdee.eas.hr.ats.app.AttendanceFileView.form")>=0
		|| previousURL.indexOf("com.kingdee.eas.hr.ats.app.HolidayFileView.form")>=0 
		|| previousURL_two.indexOf("com.kingdee.eas.hr.ats.app.AttendanceFileView.form")>=0
		|| previousURL_two.indexOf("com.kingdee.eas.hr.ats.app.HolidayFileView.form")>=0){
			$("#batchExtension").remove();
			$('.caret').eq(0).parent().remove();
			$("#viewTransaction").remove();
		}
		
	},
	
	/**
	 * 删除
	 */
	deleteRecord:function(selectedIds) {
		var _self = this;
		var billId = $("#grid").jqGrid("getSelectedRows");
		if (billId == undefined || billId.length==0) {
	        shr.showInfo({message: '请选中行'});
			return ;
	    }
		var isAllDelete=true;
	    var ids = [];
		for (var i = 0; i < billId.length; i++) {
			var limitstatus = $("#grid").jqGrid("getRowData",billId[i]).status;
			console.log($("#grid").jqGrid("getRowData",billId[i]));
			var holidayType = $("#grid").jqGrid("getRowData",billId[i])['holidayPolicy.holidayType.name'];
			if(limitstatus==0 && holidayType!='调休假' ){
				ids.push($("#grid").jqGrid("getCell",billId[i], "id"));
			}
			else{
				isAllDelete=false;
			}
		}
		var confirmMess="";
		if(isAllDelete==false){
			confirmMess="未审核状态且非调休假的记录能删除，已审核状态及调休假的记录会被忽略，是否继续？";
		}
		else{
			confirmMess="您确认要删除额度记录吗？";
		}
		if(ids.length>0){
			ids=ids.join(",");
		}
		else{
			shr.showWarning({message: '没有可以删除的记录！'});
			return ;
		}
		shr.showConfirm(confirmMess, function(){
			top.Messenger().hideAll();
			
			_self.doRemoteAction({
				method: 'delete',
				billId: ids
			});
		});
	}
	
	,auditAction : function (event) {
			var billId = $("#grid").jqGrid("getSelectedRows");
			if (billId == undefined || billId.length==0) {
		        shr.showInfo({message: '请选中行'});
				return ;
		    }
//		    if(limitstatus == 1){
//		    	shr.showError({message: "未审核状态的额度记录才能进行审核，请重新选择"});
//		    	return false;
//		    }
			var isAllAudit=true;
		    var ids = [];
			for (var i = 0; i < billId.length; i++) {
				var limitstatus = $("#grid").jqGrid("getRowData",billId[i]).status;
				if(limitstatus==0){
					ids.push($("#grid").jqGrid("getCell",billId[i], "id"));
				}
				else{
					isAllAudit=false;
				}
			}
			var confirmMess="";
			if(isAllAudit==false){
				confirmMess="未审核状态的记录能审核，已审核状态的记录会被忽略，是否继续？";
			}
			else{
				confirmMess="您确认要审核额度记录吗？";
			}
			if(ids.length>0){
				ids=ids.join(",");
			}
			else{
				shr.showInfo({message: '没有可以审核的记录！'});
				return ;
			}
			var _self = this;
			shr.showConfirm(confirmMess, function(){
				top.Messenger().hideAll();
				
				var data = {
					method: 'audit',
					ids: ids
				};
				data = $.extend(_self.prepareParam(), data);
				
				shr.doAction({
					url: _self.dynamicPage_url,
					type: 'post', 
						data: data,
						success : function(response) {					
							_self.reloadGrid();
						}
				});	
				
			});
		}
		
		,antiAuditAction : function (event) {
			var billId = $("#grid").jqGrid("getSelectedRows");
			
			var limitstatus;
			for (var i = 0; i < billId.length; i++) {
				if($("#grid").jqGrid("getRowData",billId[i]).status==0){
					limitstatus = 0;
				}
			}
			
			if (billId == undefined || billId.length==0) {
		        shr.showInfo({message: '请选中行'});
				return ;
		    }
//			 var ids = [];
//				for (var i = 0; i < billId.length; i++) {
//					ids.push($("#grid").jqGrid("getCell",billId[i], "id"));
//		    //获取行数据
//		    var rowDate =  $("#grid").jqGrid("getRowData", billId[i]);
//		     if(rowDate["holidayPolicy.holidayType.id"]=="+ZM5jTmrS0KHCjDSYGcFLf0tUpg="){
//		     	shr.showInfo({message: '调休假不能反审核！'});
//				return ;
//		     }
//				}
		     
//		    if(limitstatus == 0){
//		    	shr.showError({message: "审核状态的额度记录才能进行反审核，请重新选择"});
//		    	return false;
//		    }
			var isAllAntiAudit=true;
		    var ids = [];
			for (var i = 0; i < billId.length; i++) {
				var limitstatus = $("#grid").jqGrid("getRowData",billId[i]).status;
				if(limitstatus==1){
					ids.push($("#grid").jqGrid("getCell",billId[i], "id"));
				}
				else{
					isAllAntiAudit=false;
				}
			}
			var confirmMess="";
			if(isAllAntiAudit==false){
				confirmMess="已审核状态的记录能反审核，未审核状态的记录会被忽略，是否继续？";
			}
			else{
				confirmMess="您确认要反审核额度记录吗？";
			}
			if(ids.length>0){
				ids=ids.join(",");
			}
			else{
				shr.showInfo({message: '没有可以反审核的记录！'});
				return ;
			}
			var _self = this;
			shr.showConfirm(confirmMess, function(){
				top.Messenger().hideAll();
				
				var data = {
					method: 'antiAudit',
					billId: ids
				};
				data = $.extend(_self.prepareParam(), data);
				
				shr.doAction({
					url: _self.dynamicPage_url,
					type: 'post', 
						data: data,
						success : function(response) {					
							_self.reloadGrid();
						}
				});	
				
			});
		}
		
		
    ,generateAction:function(){
    	var _self = this;
    	var billId = $.getUrlParam('billId');
    	 if(billId != undefined && billId != null && billId != ''){
    	 	var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitListHandler&method=getPerson";
    	 	$.ajax({
				url: url,
				data: {
					billId:decodeURIComponent(billId)
				},
				success:function(res){
					_self.reloadPage({
    					uipk: "com.kingdee.eas.hr.ats.app.GenerateHolidayLimit",
    					name: encodeURIComponent(res.name),
    					personId: res.personId,
    					orgName : encodeURIComponent(res.orgName)
    				});
			   }
			});
        
    	 }
    	 else{
    	 	_self.reloadPage({
    			uipk: "com.kingdee.eas.hr.ats.app.GenerateHolidayLimit"
    		});
    	 }
        /*
    	var selectedRows = $("#grid").jqGrid("getSelectedRows");
    	if(selectedRows == null || selectedRows.length == 0){
    		_self.reloadPage({
    			uipk: "com.kingdee.eas.hr.ats.app.GenerateHolidayLimit"
    		});
    	}
    	else{
    		if(selectedRows.length > 1){
    			shr.showWarning({message: '请选择一条假期额度信息数据'});
    		}
    		else{
    	   	 	//var number = $("#grid").jqGrid("getCell", selectedRows[0], "proposer.number");
    	   	 	var name = $("#grid").jqGrid("getCell", selectedRows[0], "proposer.name");
    	   	 	var personId = $("#grid").jqGrid("getCell", selectedRows[0], "proposer.id");
    	   	 	_self.reloadPage({
    				uipk: "com.kingdee.eas.hr.ats.app.GenerateHolidayLimit",
    				name: encodeURIComponent(name),
    				personId: personId
    			});
    		}
    		
    	}
    	*/
    }
  
    ,initalizeImportAction:function(){
    	document.documentElement.style.overflow='hidden';
    	this.doImportData(undefined,undefined,'initalize');
		var  importHTML= ''
			+ '<div id="photoCtrl">' 
			+	'<p>假期额度初始化导入说明</p>'
			+	'<div class="photoState">1. 上传文件不能修改模板文件的格式</div>'
			+	'<div class="photoState">2. 支持上传文件格式为xls,xlsx的excel文件</div>'
			+   '<div class="photoState">3. 使用场景：<br/>'
			+   '&nbsp;&nbsp;若系统初次使用时,先通过系统生成额度，再批量导入用户之前已审核通过的请假单,然后再通过初始化导入更新剩余额度和实际额度；<br/>'
			+	'&nbsp;&nbsp;导入当前额度的剩余额度,系统自动计算出额度的实际值;<br/>'
			+   '&nbsp;&nbsp;导入后: 实际额度 = 剩余额度 + 已用额度;'
            +    '</div>'
			+   '<br>'
			+       '</div>'
			+		'<div style="clear: both;"></div>'
			+	'</div>'
			+ '</div>';
	  	$('#importDiv').css('height','705px');
	
		if ($.browser.msie) {  // 通过iframe 等方式来填充页面的
	  		setTimeout(function(){
				jQuery(window.frames["importFrame"].document).find("#container").before(importHTML);
				var rowBlockClass = $(jQuery(window.frames["importFrame"].document)).find("#workAreaDiv").find(".row-block");
				for(var i=0;i<rowBlockClass.length;i++){
				   $(rowBlockClass[i]).removeClass("row-block");
				}
				$(jQuery(window.frames["importFrame"].document)).find("body").css("line-height", "18px");
		 	},2000);
	  	}else{  // 通过 div 等方式来填充页面的 
			setTimeout(function(){
		 		$('#container').before(importHTML);
		 		$("button[id^=download]").closest(".row-block").find(".row-block").removeClass("row-block");
		 		$("button[id^=download]").closest(".row-block").removeClass("row-block");
		 	
		 	},1000);
		}
		$(".ui-dialog-titlebar-close").unbind().bind("click" , function(){
			$('#importDiv').dialog("close");
			document.documentElement.style.overflow='visible';
			//未解决ie缓存问题
			if($.browser.msie)
			location.reload();
		});	
		
		return;
		/*
		var  that = this;
		//关闭状态  1 为 正常  0 为 导入成功时关闭
		var colseState = 1;
		var  importHTML= ''
			+ '<div id="photoCtrl">' 
			+	'<p>假期额度初始化导入说明</p>'
			+	'<div class="photoState">1. 上传文件不能修改模板文件的格式</div>'
			+	'<div class="photoState">2. 支持上传文件格式为xls,xlsx的excel文件</div>'
			+   '<div class="photoState">3. 使用场景：<br/>'
			+   '&nbsp;&nbsp;若系统初次使用时,先通过系统生成额度，再批量导入用户之前已审核通过的请假单,然后再通过初始化导入更新剩余额度和实际额度；<br/>'
			+	'&nbsp;&nbsp;导入当前额度的剩余额度,系统自动计算出额度的实际值;<br/>'
			+   '&nbsp;&nbsp;导入后: 实际额度 = 剩余额度 + 已用额度;'
            +    '</div>'
			+   '<br>'
			+ 	'<p>请选择所需要的操作</p>'
			+ 	'<div class="photoCtrlBox">'
			+		'<div class="photoCtrlRadio"><input type="radio" name="inAndout" id="importRadioInitalize" checked ></span></div>'
			+       '<div class="photoCtrlUpload"><span>请选择上传文件</span></div>'
			+		'<div class="photoCtrlUpload1"><input type="file" name="file_upload" id="file_upload"></div>'
			+ 		'<div style="clear:both"><span style="color:red;display:none" id="file_warring">未选择上传文件</span></div>'
			+	    '</div>'
			+ '<div class="photoCtrlBox"><div id="exportBox"><div class="photoCtrlRadio"><input type="radio" name="inAndout" id="exportRadioInitalize"></div><span>引出</span><span>额度初始化模板 </span></div>  <div style="clear: both;"></div></div>'
			+       '</div>'
			+		'<div style="clear: both;"></div>'
			+	'</div>'
			+ '</div>';
		$(document.body).append(importHTML);
		//$('#importRadioInitalize').shrRadio();
		$('#importRadioInitalize, #exportRadioInitalize').shrRadio();
		// 初始化上传按钮
		var data = {
			method: "uploadFile" 
		};
			
		data = $.extend(that.prepareParam(), data);
		//var url = that.dynamicPage_url + "?method=uploadFile&uipk="+data.uipk;
		var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=uploadFile";
		url += "&" + getJSessionCookie();
		//在点击确定前，对文件进行上传处理
		var handleFlag = false;
		$("#file_upload").uploadify({
		   	swf: "jslib/uploadify/uploadify.swf",
		    uploader: url,
		    buttonText: "选择文件",
		    buttonClass: "shrbtn-primary shrbtn-upload",
		    fileTypeDesc: "Excel",
		    fileTypeExts: "*.xls;*.xlsx",
		    async: false,
		    multi: false,
		    removeCompleted: false,
		    onUploadStart: function() {
		    	//openLoader(0); //遮罩层
			},
			onUploadComplete: function(file) {
				handleFlag = true;
				$('#file_warring').hide();
				//alert("onUploadSuccess 导入成功=="+JSON.stringify(data));
				//shr.showInfo({message: '上传成功'});
				//error_path = data;
				//$('#photoCtrlInitalize').dialog('close');
				//$(this).dialog( "close" );
				//刷新表格
				//$("#grid").jqGrid().jqGrid("reloadGrid");
			}
		});

		$('#photoCtrl').dialog({
			title: '假期额度初始化导入',
			width: 600,
			height: 540,
			modal: true,
			resizable: false,
			position: {
				my: 'center',
				at: 'top+20%',
				of: window
			},
			close: function(event, ui) { 
			if(colseState==1){
				var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=deleteFile";
		        	$.ajax({
		        		url: url
		        	})
		        	$(this).dialog( "close" );
		        	$('#photoCtrl').remove();
			}
		        	} ,
			buttons: {
		        "确认": function() {
		        if ($('#exportRadioInitalize').shrRadio('isSelected')) {
						that.exportHolidayLimitInitalizeTemplateAction();
					}
		        	else if(handleFlag){
		        		that.importFileData();
		        		colseState=0;
		        		$(this).dialog( "close" );
		        		colseState=1;
		        		$('#photoCtrl').remove();
		        	}
		        	else{
		        		$('#file_warring').show();
		        	}
		        },
		        "关闭": function() {
		        	var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=deleteFile";
		        	$.ajax({
		        		url: url
		        	})
		        	$(this).dialog( "close" );
		        	$('#photoCtrl').remove();
		        }
		    }
		});
		*/
	}
	,importFileData: function(){
		
		//alert("读取服务器目录文件 解析");
		var that=this;
		var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=importFileData";
		var fileName = $("#fileName").val();
		var realTotalNum = 0;
		var successTotalNum = 0;
		var failTotalNum = 0;
		
		$.ajax({
			url: url,
			beforeSend: function(){
				openLoader(1);
			},
			data: {
				fileName: fileName
			},
			success: function(msg){
				closeLoader();
				if((msg.importantError==null||msg.importantError=="")&&(msg.realTotalNum==null||msg.realTotalNum=="")){
				shr.showError({message: "导入异常，请检查您的导入模板和导入数据"});
				}else{
				if(msg.importantError!=null&&msg.importantError!=""){
				shr.showError({message: msg.importantError});
				}else{
				 realTotalNum = msg.realTotalNum;
				 successTotalNum = msg.successTotalNum;
				 failTotalNum = msg.failTotalNum;
				
				var tip="";
					tip ="假期额度初始化数据导入完毕<br/>";
					tip = tip +  " 导入的文件中共" + realTotalNum+ "条记录<br/>" ;
					tip = tip +  " 导入成功的记录有" + successTotalNum + "条<br/>" ;
				
				if (msg.failTotalNum > 0) {
						tip = tip +  " <font color='red'>导入失败" + failTotalNum + "条</font><br/>" ;
						tip = tip +  "导入失败的原因如下：<br/>" ;
						for(i=0;i<msg.errorStringList.length;i++){
							tip = tip + "  <font color='red'> " +　msg.errorStringList[i] + "</font><br/>" ;
						}
					}
				var options={
				   message:tip
				};
				$.extend(options, {
					type: 'info',
					hideAfter: null,
					showCloseButton: true
				});
				top.Messenger().post(options);
				$('#photoCtrl').remove();
				// 刷新表格
				$("#grid").jqGrid().jqGrid("reloadGrid");
				}
				}
			},
			error: function(msg){
				closeLoader();
				shr.showError({message: "导入失败"});
			},
			complete: function(){
				closeLoader();
			}
		});
	}

	  ,modifyImportAction:function(){
	  	document.documentElement.style.overflow='hidden';
	  	this.doImportData(undefined,undefined,'modify');
		var  importHTML= ''
		+ '<div id="photoCtrl">' 
		+	'<p>假期额度修改导入说明</p>'
		+	'<div class="photoState">1. 上传文件不能修改模板文件的格式</div>'
		+	'<div class="photoState">2. 支持上传文件格式为xls,xlsx的excel文件</div>'
		+	'<div class="photoState">3. 使用场景：<br/>'
		+   '&nbsp;&nbsp;若员工该假期类型对应的假期额度已生成,且是最新的周期,可以通过修改导入<br/>' 
		+	'&nbsp;&nbsp;批量修改增减额度和延期日期;<br/>'
		+	' &nbsp;&nbsp;导入后: 实际额度  = 标准额度  + 增减额度<br/>'
		+		' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剩余额度 = 实际额度 - 已用额度 - 在途额度 - 上期透支额度;'
        +   '</div>'
		+   '<br>'
		+       '</div>'
		+	'</div>'
		+ '</div>';
	  	$('#importDiv').css('height','705px');
		if ($.browser.msie) {  // 通过iframe 等方式来填充页面的
	  		setTimeout(function(){
	  			//$('iframe')[1].contentWindow.$('#container').before(importHTML);//s-hr3.0不能这样用了。
				jQuery(window.frames["importFrame"].document).find("#container").before(importHTML);
				var rowBlockClass = $(jQuery(window.frames["importFrame"].document)).find("#workAreaDiv").find(".row-block");
				for(var i=0;i<rowBlockClass.length;i++){
				   $(rowBlockClass[i]).removeClass("row-block");
				}
				$(jQuery(window.frames["importFrame"].document)).find("body").css("line-height", "18px");
				
		 	},2000);
	  	}else{  // 通过 div 等方式来填充页面的 
			setTimeout(function(){
		 		$('#container').before(importHTML);
		 		$("button[id^=download]").closest(".row-block").find(".row-block").removeClass("row-block");
		 		$("button[id^=download]").closest(".row-block").removeClass("row-block");
		 	
		 	},1000);
		}
		$(".ui-dialog-titlebar-close").unbind().bind("click" , function(){
			$('#importDiv').dialog("close");
			document.documentElement.style.overflow='visible';
			//未解决ie缓存问题
			if($.browser.msie)
			location.reload();
		});	
		return;
		/*
		var  that = this;
		//关闭状态  1 为 正常  0 为 导入成功时关闭
		var colseState = 1;
		var  importHTML= ''
			+ '<div id="photoCtrl">' 
			+	'<p>假期额度修改导入说明</p>'
			+	'<div class="photoState">1. 上传文件不能修改模板文件的格式</div>'
			+	'<div class="photoState">2. 支持上传文件格式为xls,xlsx的excel文件</div>'
			+	'<div class="photoState">3. 使用场景：<br/>'
			+   '&nbsp;&nbsp;若员工该假期类型对应的假期额度已生成,且是最新的周期,可以通过修改导入<br/>' 
			+	'&nbsp;&nbsp;批量修改增减额度和延期日期;<br/>'
			+	' &nbsp;&nbsp;导入后: 实际额度  = 标准额度  + 增减额度<br/>'
			+		' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剩余额度 = 实际额度 - 已用额度 - 冻结额度 - 上期透支额度;'
            +   '</div>'
			+   '<br>'
			+ 	'<p>请选择所需要的操作</p>'
			+ 	'<div class="photoCtrlBox">'
			+		'<div class="photoCtrlRadio"><input type="radio" name="inAndout" id="importRadioModify" checked ></span></div>'
			+       '<div class="photoCtrlUpload"><span>请选择上传文件</span></div>'
			+		'<div class="photoCtrlUpload1"><input type="file" name="file_upload" id="file_upload"></div>'
			+ 		'<div style="clear:both"><span style="color:red;display:none" id="file_warring">未选择上传文件</span></div>'
			+	    '</div>'
			+ '<div class="photoCtrlBox"><div id="exportBox"><div class="photoCtrlRadio"><input type="radio" name="inAndout" id="exportRadioModify"></div><span>引出</span><span>额度修改模板 </span></div>  <div style="clear: both;"></div></div>'
			+       '</div>'
			+		'<div style="clear: both;"></div>'
			+	'</div>'
			+ '</div>';
		$(document.body).append(importHTML);
		//$('#importRadioModify').shrRadio();
		$('#importRadioModify, #exportRadioModify').shrRadio();
		// 初始化上传按钮
		var data = {
			method: "uploadFile" 
		};
			
		data = $.extend(that.prepareParam(), data);
		//var url = that.dynamicPage_url + "?method=uploadFile&uipk="+data.uipk;
		var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=uploadFile";
		url += "&" + getJSessionCookie();
		//在点击确定前，对文件进行上传处理
		var handleFlag = false;
		$("#file_upload").uploadify({
		   	swf: "jslib/uploadify/uploadify.swf",
		    uploader: url,
		    buttonText: "选择文件",
		    buttonClass: "shrbtn-primary shrbtn-upload",
		    fileTypeDesc: "Excel",
		    fileTypeExts: "*.xls;*.xlsx",
		    async: false,
		    multi: false,
		    removeCompleted: false,
		    onUploadStart: function() {
		    	//openLoader(0); //遮罩层
			},
			onUploadComplete: function(file) {
				handleFlag = true;
				$('#file_warring').hide();
				//alert("onUploadSuccess 导入成功=="+JSON.stringify(data));
				//shr.showInfo({message: '上传成功'});
				//error_path = data;
				//$('#photoCtrl').dialog('close');
				//$(this).dialog( "close" );
				//刷新表格
				//$("#grid").jqGrid().jqGrid("reloadGrid");
			}
		});

		$('#photoCtrl').dialog({
			title: '假期额度修改导入',
			width: 600,
			height: 540,
			modal: true,
			resizable: false,
			position: {
				my: 'center',
				at: 'top+20%',
				of: window
			},
			close: function(event, ui) { 
			   if(colseState==1){
			var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=deleteFile";
		        	$.ajax({
		        		url: url
		        	})
		        	$(this).dialog( "close" );
		        	$('#photoCtrl').remove();
			   }
		        	} ,
			buttons: {
		        "确认": function() {
		        if ($('#exportRadioModify').shrRadio('isSelected')) {
						that.exportHolidayLimitModifyTemplateAction();
					}
		        	else if(handleFlag){
		        		that.importModifyFileData();
		        		colseState=0;
		        		$(this).dialog( "close" );
		        		colseState=1;
		        		$('#photoCtrl').remove();
		        	}
		        	else{
		        		$('#file_warring').show();
		        	}
		        },
		        "关闭": function() {
		        	var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=deleteFile";
		        	$.ajax({
		        		url: url
		        	})
		        	$(this).dialog( "close" );
		        	$('#photoCtrl').remove();
		        }
		    }
		});
		*/
	}
	,importModifyFileData: function(){
		//alert("读取服务器目录文件 解析");
		var that=this;
		var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=importModifyFileData";
		var fileName = $("#fileName").val();
		var realTotalNum = 0;
		var successTotalNum = 0;
		var failTotalNum = 0;
		$.ajax({
			url: url,
			beforeSend: function(){
				openLoader(1);
			},
			data: {
				fileName: fileName
			},
			success: function(msg){
				closeLoader();
				if((msg.importantError==null||msg.importantError=="")&&(msg.realTotalNum==null||msg.realTotalNum=="")){
				shr.showError({message: "导入异常，请检查您的导入模板和导入数据"});
				}else{
				if(msg.importantError!=null&&msg.importantError!=""){
				shr.showError({message: msg.importantError});
				}else{
				 realTotalNum = msg.realTotalNum;
				 successTotalNum = msg.successTotalNum;
				 failTotalNum = msg.failTotalNum;
					
				var tip="";
					tip ="假期额度修改数据导入完毕<br/>";
					tip = tip +  " 导入的文件中共" + realTotalNum+ "条记录<br/>" ;
					tip = tip +  " 导入成功的记录有" + successTotalNum + "条<br/>" ;
				
				if (msg.failTotalNum > 0) {
						tip = tip +  " <font color='red'>导入失败" + failTotalNum + "条</font><br/>" ;
						tip = tip +  "导入失败的原因如下：<br/>" ;
						for(i=0;i<msg.errorStringList.length;i++){
							tip = tip + "  <font color='red'> " +　msg.errorStringList[i] + "</font><br/>" ;
						}
					}
				var options={
				   message:tip
				};
				$.extend(options, {
					type: 'info',
					hideAfter: null,
					showCloseButton: true
				});
				top.Messenger().post(options);
				$('#photoCtrl').remove();
				// 刷新表格
				$("#grid").jqGrid().jqGrid("reloadGrid");
				}
				}
			},
			error: function(msg){
				closeLoader();
				shr.showError({message: "导入失败"});
			},
			complete: function(){
				closeLoader();
			}
		});
	}
	
	,viewTransactionAction : function () {
		//查看后台事务
		var url =  shr.getContextPath()+"/dynamic.do?uipk=com.kingdee.eas.base.job.app.JobInst.list";
		$("#dialogTransaction").children("iframe").attr('src', url);
		$("#dialogTransaction").dialog({
			width:950,
			height:650,
			modal: true,
			resizable: false,
			draggable: true,
			position: {
				my: 'center',
				at: 'top+15%',
				of: window
			}
		});
		//ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix
		//$(".ui-dialog-titlebar").remove();
		//$("#dialogTransaction").parent().find('.ui-dialog-titlebar').remove(); // 只是删除 查看后台事务
		$("#dialogTransaction").css({height:570});
	}
	//批量延期
	,batchExtensionAction : function () {
		var _self = this;
		var quickSearch = $('#grid').jqGrid("getGridParam").filterItems;
		var nodeLongNumber = eval("("+ $('#grid').jqGrid("getGridParam").tree_params +")").nodeLongNumber;
		var domainFilter = "";
		_self.remoteCall({
				type:"post",
				method:"getDomainFilter",
				async: false,
				param:{
					uipk : "com.kingdee.eas.hr.ats.app.HolidayLimit.list"
				},
				success:function(res){
					domainFilter =  res.domainFilter;
				}
		});
		var temp_rows1 = $("#grid").jqGrid("getSelectedRows");
		var temp_rows2 = $("#grid").jqGrid("getCol","id");
		var hasLeft = "false";
		var realTempRow1 = [];
		for(var i=0;i<temp_rows1.length;i++){
			var trId = temp_rows1[i];
			var isIncount = $("tr[id^='" + trId + "']").children("td[aria-describedby='grid_employeeType.isInCount']").text();
			if("true" == isIncount){
				realTempRow1.push(trId);
			}else{
				hasLeft = "true";
			}
		}
		
		//第一种延期方式（全部延期）的标记，当全部记录大于一页记录时，才重新查询，否则直接通过页面的ID去更新全部
		var flag = $('#grid').getGridParam().records>$('#grid').getGridParam().rowNum;
		var records = $("#grid").getGridParam().records;

		var selectedRows =  encodeURIComponent(realTempRow1);
		var currentPageRows = encodeURIComponent(temp_rows2);
		if("true" == hasLeft){
			shr.showConfirm('选中的记录包含离职员工，将不会对离职员工进行延期处理。', function() {
		       _self.gotoBatchExtensionPage(selectedRows,currentPageRows,quickSearch,nodeLongNumber,domainFilter,flag,records);
		    });
		}else{
			  _self.gotoBatchExtensionPage(selectedRows,currentPageRows,quickSearch,nodeLongNumber,domainFilter,flag,records);
		}
		
	},
	gotoBatchExtensionPage : function(selectedRows,currentPageRows,quickSearch,nodeLongNumber,domainFilter,flag,records){
		localStorage.removeItem("holidayLimitSelectedRows");
		localStorage.removeItem("holidayLimitCurrentPageRows");
		localStorage.setItem("holidayLimitSelectedRows", selectedRows);
		localStorage.setItem("holidayLimitCurrentPageRows",currentPageRows);
		var temp_rows1 = $("#grid").jqGrid("getSelectedRows");
		if(temp_rows1.length > 300){
			shr.showWarning({"message" : "所选记录超过300行，请分批对选中记录进行延期！"});
			return false;
		}
		this.reloadPage({
    			uipk: "com.kingdee.eas.hr.ats.app.HolidayLimitBatchExtension",
				billId:"",//框架会自动拼上billId,这里不需要这个，已经传了selectedRows。就是billId,以免url过长。params = $.extend(this.prepareParam(), params);直接覆盖掉
				quickSearch : encodeURIComponent(quickSearch),
				nodeLongNumber : encodeURIComponent(nodeLongNumber),
				domainFilter : encodeURIComponent(domainFilter),
				flag : encodeURIComponent(flag),
				records :encodeURIComponent(records) //总记录条数，用于后续页面的提示信息
    	});
	}
	//离职重算年假额度
	,leftGenerateAction:function(){
		var that = this;
		
		var billId = $.getUrlParam('billId');
		if(billId != undefined && billId != null && billId != ''){
		 	var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitListHandler&method=getPerson";
		 	$.ajax({
				url: url,
				data: {
					billId:decodeURIComponent(billId)
				},
				success:function(res){
					that.reloadPage({
						uipk: "com.kingdee.eas.hr.ats.app.QuitHolidayLimit",
						name: encodeURIComponent(res.name),
						personId: res.personId,
						orgName : encodeURIComponent(res.orgName)
					});
			   }
			});
	    
		}else{
		 	this.reloadPage({
			uipk:"com.kingdee.eas.hr.ats.app.QuitHolidayLimit"
			});
		}
		
	}
	//新增导入
	  ,addImportAction:function(){
		document.documentElement.style.overflow='hidden';
		this.doImportData(undefined,undefined,'add');
			var  importHTML= ''
			+ '<div id="photoCtrl">' 
			+	'<p>假期额度新增导入说明</p>'
			+	'<div class="photoState">1. 上传文件不能修改模板文件的格式</div>'
			+	'<div class="photoState">2. 支持上传文件格式为xls,xlsx的excel文件</div>'
			+　   '<div class="photoState">3. 使用场景：<br/>'
			+	    ' &nbsp;&nbsp;若员工该假期类型导入周期内没有生成过假期额度,可以通过新增导入来生成;<br/>' 
			+		' &nbsp;&nbsp;新增导入的额度项目有标准额度,增减额度,已用额度和上期透支额度;<br/>' 
			+		' &nbsp;&nbsp;导入后: 实际额度  = 标准额度  + 增减额度<br/>'
			+		' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剩余额度 = 实际额度 - 已用额度 - 上期透支额度;'

			+   '<br>'
			+       '</div>'
			+		'<div style="clear: both;"></div>'
			+	'</div>'
			+ '</div>';
	  	$('#importDiv').css('height','705px');
	  	
	  	if ($.browser.msie) {  //浏览器为IE的时候， 通过iframe 等方式来填充页面的
	  		setTimeout(function(){
	  			//$('iframe')[1].contentWindow.$('#container').before(importHTML);//S-hr3.0不能这样使用了
				jQuery(window.frames["importFrame"].document).find("#container").before(importHTML);
				var rowBlockClass = $(jQuery(window.frames["importFrame"].document)).find("#workAreaDiv").find(".row-block");
				for(var i=0;i<rowBlockClass.length;i++){
				   $(rowBlockClass[i]).removeClass("row-block");
				}
				$(jQuery(window.frames["importFrame"].document)).find("body").css("line-height", "18px");
		 	},2000);
	  	}else{  // 通过 div 等方式来填充页面的 
			setTimeout(function(){
		 		$('#container').before(importHTML);
		 		$("button[id^=download]").closest(".row-block").find(".row-block").removeClass("row-block");
		 		$("button[id^=download]").closest(".row-block").removeClass("row-block");
		 		
		 	},1000);
		}
		$(".ui-dialog-titlebar-close").unbind().bind("click" , function(){
			$('#importDiv').dialog("close");
			document.documentElement.style.overflow='visible';
			//未解决ie缓存问题
			if($.browser.msie)
			location.reload();
		});	
		return;
		/*
		var  that = this;
		//关闭状态  1 为 正常  0 为 导入成功时关闭
		var colseState = 1;
		var  importHTML= ''
			+ '<div id="photoCtrl">' 
			+	'<p>假期额度新增导入说明</p>'
			+	'<div class="photoState">1. 上传文件不能修改模板文件的格式</div>'
			+	'<div class="photoState">2. 支持上传文件格式为xls,xlsx的excel文件</div>'
			+　   '<div class="photoState">3. 使用场景：<br/>'
			+	    ' &nbsp;&nbsp;若员工该假期类型导入周期内没有生成过假期额度,可以通过新增导入来生成;<br/>' 
			+		' &nbsp;&nbsp;新增导入的额度项目有标准额度,增减额度,已用额度和上期透支额度;<br/>' 
			+		' &nbsp;&nbsp;导入后: 实际额度  = 标准额度  + 增减额度<br/>'
			+		' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剩余额度 = 实际额度 - 已用额度 - 上期透支额度;'

			+   '<br>'
			+ 	'<p>请选择所需要的操作</p>'
			+ 	'<div class="photoCtrlBox">'
			+		'<div class="photoCtrlRadio"><input type="radio" name="inAndout" id="importRadioAdd" checked ></span></div>'
			+       '<div class="photoCtrlUpload"><span>请选择上传文件</span></div>'
			+		'<div class="photoCtrlUpload1"><input type="file" name="file_upload" id="file_upload"></div>'
			+ 		'<div style="clear:both"><span style="color:red;display:none" id="file_warring">未选择上传文件</span></div>'
			+	    '</div>'
			+ '<div class="photoCtrlBox"><div id="exportBox"><div class="photoCtrlRadio"><input type="radio" name="inAndout" id="exportRadioAdd"></div><span>引出</span><span>额度新增模板 </span></div>  <div style="clear: both;"></div></div>'
			+       '</div>'
			+		'<div style="clear: both;"></div>'
			+	'</div>'
			+ '</div>';
		$(document.body).append(importHTML);
		//$('#importRadioModify').shrRadio();
		$('#importRadioAdd, #exportRadioAdd').shrRadio();
		// 初始化上传按钮
		var data = {
			method: "uploadFile" 
		};
			
		data = $.extend(that.prepareParam(), data);
		//var url = that.dynamicPage_url + "?method=uploadFile&uipk="+data.uipk;
		var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=uploadFile";
		url += "&" + getJSessionCookie();
		//在点击确定前，对文件进行上传处理
		var handleFlag = false;
		$("#file_upload").uploadify({
		   	swf: "jslib/uploadify/uploadify.swf",
		    uploader: url,
		    buttonText: "选择文件",
		    buttonClass: "shrbtn-primary shrbtn-upload",
		    fileTypeDesc: "Excel",
		    fileTypeExts: "*.xls;*.xlsx",
		    async: false,
		    multi: false,
		    removeCompleted: false,
		    onUploadStart: function() {
		    	//openLoader(0); //遮罩层
			},
			onUploadComplete: function(file) {
				handleFlag = true;
				$('#file_warring').hide();
				//alert("onUploadSuccess 导入成功=="+JSON.stringify(data));
				//shr.showInfo({message: '上传成功'});
				//error_path = data;
				//$('#photoCtrl').dialog('close');
				//$(this).dialog( "close" );
				//刷新表格
				//$("#grid").jqGrid().jqGrid("reloadGrid");
			}
		});

		$('#photoCtrl').dialog({
			title: '假期额度新增导入',
			width: 600,
			height: 520,
			modal: true,
			resizable: false,
			position: {
				my: 'center',
				at: 'top+20%',
				of: window
			},
			close: function(event, ui) { 
			   if(colseState==1){
			var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=deleteFile";
		        	$.ajax({
		        		url: url
		        	})
		        	$(this).dialog( "close" );
		        	$('#photoCtrl').remove();
			   }
		        	} ,
			buttons: {
		        "确认": function() {
		        if ($('#exportRadioAdd').shrRadio('isSelected')) {
						that.exportHolidayLimitAddTemplateAction();
					}
		        	else if(handleFlag){
		        		that.importAddFileData();
		        		colseState=0;
		        		$(this).dialog( "close" );
		        		colseState=1;
		        		$('#photoCtrl').remove();
		        	}
		        	else{
		        		$('#file_warring').show();
		        	}
		        },
		        "关闭": function() {
		        	var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=deleteFile";
		        	$.ajax({
		        		url: url
		        	})
		        	$(this).dialog( "close" );
		        	$('#photoCtrl').remove();
		        }
		    }
		});
		*/
	}
	,importAddFileData: function(){
		//alert("读取服务器目录文件 解析");
		var that=this;
		var url = shr.getContextPath() + "/dynamic.do?handler=com.kingdee.shr.ats.web.handler.HolidayLimitImportEditHandler&method=importAddFileData";
		var fileName = $("#fileName").val();
		var realTotalNum = 0;
		var successTotalNum = 0;
		var failTotalNum = 0;
		$.ajax({
			url: url,
			beforeSend: function(){
				openLoader(1);
			},
			data: {
				fileName: fileName
			},
			success: function(msg){
				closeLoader();
				if((msg.importantError==null||msg.importantError=="")&&(msg.realTotalNum==null||msg.realTotalNum=="")){
				shr.showError({message: "导入异常，请检查您的导入模板和导入数据"});
				}else{
				if(msg.importantError!=null&&msg.importantError!=""){
				shr.showError({message: msg.importantError});
				}else{
				 realTotalNum = msg.realTotalNum;
				 successTotalNum = msg.successTotalNum;
				 failTotalNum = msg.failTotalNum;
					
				var tip="";
					tip ="假期额度新增数据导入完毕<br/>";
					tip = tip +  " 导入的文件中共" + realTotalNum+ "条记录<br/>" ;
					tip = tip +  " 导入成功的记录有" + successTotalNum + "条<br/>" ;
				
				if (msg.failTotalNum > 0) {
						tip = tip +  " <font color='red'>导入失败" + failTotalNum + "条</font><br/>" ;
						tip = tip +  "导入失败的原因如下：<br/>" ;
						for(i=0;i<msg.errorStringList.length;i++){
							tip = tip + "  <font color='red'> " +　msg.errorStringList[i] + "</font><br/>" ;
						}
					}
				var options={
				   message:tip
				};
				$.extend(options, {
					type: 'info',
					hideAfter: null,
					showCloseButton: true
				});
				top.Messenger().post(options);
				$('#photoCtrl').remove();
				// 刷新表格
				$("#grid").jqGrid().jqGrid("reloadGrid");
				}
				}
			},
			error: function(msg){
				closeLoader();
				shr.showError({message: "导入失败"});
			},
			complete: function(){
				closeLoader();
			}
		});
	}
	
	
	//导出模板
	,exportHolidayLimitAddTemplateAction : function () {
		var self = this;
		$grid = $('#grid');
		var postData = $grid.jqGrid("getGridParam", "postData");
		url = shr.getContextPath() + shr.dynamicURL + "?method=exportHolidayLimitAddTemplate";
		//标题
		url += "&title=假期额度新增导入模板";
		// set PostData
		$grid._pingPostData(postData);
		var param = postData;
		if (param.queryMode) {
			delete param.queryMode
		}
		if (!param.uipk) {
			param.uipk = self.reportUipk;
		}

		param.downSum = $('#downSum').attr('checked') == "checked" ? 1 : 0;

		param.isAll = true;
		url += "&" + $.param(param);

		url += "&queryMode=";

		var colModel = $grid.jqGrid("getGridParam", "colModel");
		for (var i = 0; i < colModel.length; i++) {
			var optionColumn = colModel[i];
			var hidden = optionColumn.hidden,
			label = optionColumn.label || optionColumn.name,
			index = optionColumn.index,
			dataColumnType = optionColumn.dataColumnType;
			if (index != null && index != "") {
				url += index + "," + hidden + "," + label + "," + dataColumnType + ";";
			}
		}

		document.location.href = url;
	}
	
	
	//导出模板
	,exportHolidayLimitInitalizeTemplateAction : function () {
		var self = this;
		$grid = $('#grid');
		var postData = $grid.jqGrid("getGridParam", "postData");
		url = shr.getContextPath() + shr.dynamicURL + "?method=exportHolidayLimitInitalizeTemplate";
		//标题
		url += "&title=假期额度初始化导入模板";
		// set PostData
		$grid._pingPostData(postData);
		var param = postData;
		if (param.queryMode) {
			delete param.queryMode
		}
		if (!param.uipk) {
			param.uipk = self.reportUipk;
		}

		param.downSum = $('#downSum').attr('checked') == "checked" ? 1 : 0;

		param.isAll = true;
		url += "&" + $.param(param);

		url += "&queryMode=";

		var colModel = $grid.jqGrid("getGridParam", "colModel");
		for (var i = 0; i < colModel.length; i++) {
			var optionColumn = colModel[i];
			var hidden = optionColumn.hidden,
			label = optionColumn.label || optionColumn.name,
			index = optionColumn.index,
			dataColumnType = optionColumn.dataColumnType;
			if (index != null && index != "") {
				url += index + "," + hidden + "," + label + "," + dataColumnType + ";";
			}
		}

		document.location.href = url;
	}
	//导出模板
	,exportHolidayLimitModifyTemplateAction : function () {
		var self = this,
		$grid = $('#grid'),
		postData = $grid.jqGrid("getGridParam", "postData"),
		url = shr.getContextPath() + shr.dynamicURL + "?method=exportHolidayLimitModifyTemplate";
		
		//标题
		url += "&title=假期额度修改导入模板";
		// set PostData
		$grid._pingPostData(postData);

		var param = postData;
		if (param.queryMode) {
			delete param.queryMode
		}
		if (!param.uipk) {
			param.uipk = self.reportUipk;
		}

		param.downSum = $('#downSum').attr('checked') == "checked" ? 1 : 0;

		param.isAll = true;
		url += "&" + $.param(param);

		url += "&queryMode=";

		var colModel = $grid.jqGrid("getGridParam", "colModel");
		for (var i = 0; i < colModel.length; i++) {
			var optionColumn = colModel[i];
			var hidden = optionColumn.hidden,
			label = optionColumn.label || optionColumn.name,
			index = optionColumn.index,
			dataColumnType = optionColumn.dataColumnType;
			if (index != null && index != "") {
				url += index + "," + hidden + "," + label + "," + dataColumnType + ";";
			}
		}
		document.location.href = url;
	},
	
	/**
	 * 覆盖父类中的导入
	 */
	doImportData: function(curIOModelString, customData,classify) {
		if (typeof curIOModelString == 'undefined') {
			curIOModelString = this.getImportModel();
		}
	
		var importDiv = $('#importDiv');
		if (importDiv.length > 0) {
			importDiv.data('curIOModelString', curIOModelString);
			importDiv.data('customData', customData);
			importDiv.data('classify', classify);
			importDiv.dialog('open');
			return;
		}
		
		// 未生成dialog
		importDiv = $('<div id="importDiv"></div>').appendTo($('body'));
		importDiv.data('curIOModelString', curIOModelString);
		importDiv.data('customData', customData);
		importDiv.data('classify', classify);
		
		var _self = this;
		if(_self.checkUpload()){
			importDiv.dialog({
				autoOpen: true,		
				width: 708,
				height: 700,
				title: "导入数据",
				resizable: true,
				position: ['top','top'],
				modal: true,
				open: function(event, ui) {
					if ($.browser.msie) {
						var url = shr.assembleURL('com.kingdee.shr.io.app.ImportInfo', 'view', {
							curIOModelString: curIOModelString,
							customData: customData,
							classify:classify
						});
						var content = '<iframe id="importFrame" name="importFrame" width="700" height="600" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>';
						importDiv.append(content);
					} else {
						importDiv.css('padding', "0 20px");
						var url = shr.assembleURL('com.kingdee.shr.io.app.ImportInfo$page', 'view');
						shr.loadHTML({
							url: url,
							success: function(response) {
								importDiv.append(response);
							}
						});
					}
				},
				close: function(event, ui) {
					importDiv.empty();
					$(_self.gridId).jqGrid("reloadGrid");
				} 
			});
		}
		
		$(".ui-dialog-titlebar-close").bind("click" , function(){
			importDiv.dialog("close");
		});		
	},
	toclearviewAction: function() {
			var _self = this;
			_self.reloadPage({
				uipk: "com.kingdee.eas.hr.ats.app.ClearHolidayLimit.list",
			});

		},
	
	
});

//自动生成额度的Categorynumber为302!3005
function getJobFilter(){
	return "jobDefCategory.longNumber like '302!3005%' OR JOBDEF.mutex like '%Holiday%'" ;
}