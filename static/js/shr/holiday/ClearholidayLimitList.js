shr.defineClass("shr.holiday.ClearholidayLimitList", shr.framework.List, {
	initalizeDOM: function () {
		shr.holiday.ClearholidayLimitList.superClass.initalizeDOM.call(this);
		var that = this;
	},

	getCustomFilterItems: function () {
		var that = this;
		var filterStr = "";
		var beginDate = $("#beginDate").val();
		var endDate = $("#endDate").val();
		var name = '调休假'
		//初始化
		if (name != null && name != "") {
			filterStr += "and holidayType.name = '" + name + "' ";
		}
		if (beginDate != null && beginDate != "") {
			filterStr += "and effectDate >= '" + beginDate + "' ";
		}
		if (endDate != null && endDate != "") {
			filterStr += "and effectDate <= '" + endDate + "' ";
		}

		//去掉第一个and
		filterStr = filterStr.substring(3);

		return filterStr;
	},

	queryAction: function () {
		var that = this;
		var filterStr = that.getCustomFilterItems();
		$("#grid").jqGrid('option', 'page', 1);
		$("#grid").jqGrid("option", "filterItems", filterStr).jqGrid("reloadGrid");
	},
	/**
	 * 清除额度
	 */
	clearAction: function () {
		var that = this;
		var ids = [];
		var billId = $("#grid").jqGrid("getSelectedRows");

		if (billId == undefined || billId.length == 0) {
			shr.showInfo({
				message: '请选中行'
			});
			return;
		}

		for (var i = 0; i < billId.length; i++) {
			ids.push($("#grid").jqGrid("getCell", billId[i], "id"));
		}
		if (ids.length > 0) {
			ids = ids.join(",")
		}

		if (ids === 'false') {
			shr.showInfo({
				message: "没有数据要清除！"
			})
		} else {
			shr.showConfirm('确定清除额度么？', function () {
				that.remoteCall({
					method: 'clearHoliday',
					param: {
						billId: ids
					},
					success: (function (res) {
						if (res.state === 'success') {
							shr.showInfo({
								message: "清除额度成功"
							})
							that.reloadGrid();
						}
					})
				})
			})
		}
	}
});