shr.defineClass("shr.duty.DutyCostList",shr.framework.List,{
	initalizeDOM:function(){
		var that=this;
		shr.duty.DutyCostList.superClass.initalizeDOM.call(that);
	},

	/**
	*删除
	*/
    deleteAction:function(){
    	var self = this;
    	var selectedIds = shr.duty.DutyCostList.superClass.getSelectedIds();
  /*每次只允许删除一条记录*/
  		if (selectedIds.split(',').length > 1) {
   		shr.showWarning({
    	message:"请选择一条记录进行删除！",
    	hideAfter:3
   	});
   		return;
  		}
  /*判断单据状态 只有保存状态的单据可以删除*/
  		self.remoteCall({
   		method : 'isDeleteState',
   		param : {
    	billId : selectedIds
   		},
   		success : function(data) {
    	var messages;
    	if(data != null && data != undefined){
    	 messages = data.msg;
    	}
    	if(messages == 'SUCCESS'&& selectedIds){
     //执行删除
    	 self.deleteRecord(selectedIds);
    	}else{
    	 shr.showWarning({
     	 message:"该单据不允许删除！",
      	hideAfter:3
     	});
      }
   	}
    
   });

 },
	/**
	* 反审批
	*/
	againstApproveAction : function(){
		var self = this;
	
		var billIds = $("#grid").jqGrid("getSelectedRows");

		/*每次只允许反审批一条记录*/
		
		if (billIds.length > 1) {
			shr.showWarning({
				message:"请选择一条记录进行反审批！",
				hideAfter:3
			});
			return;
		}
		
		if (billIds == undefined || billIds.length==0 || (billIds && billIds.length == 1 && billIds[0] == "")) {
			shr.showError({message: "请选中行！ "});
			return ;
		}else{
			self.remoteCall({
				method:'isApprove', // 是否审批通过
				param : {
					billId : billIds[0]
				},
				success:function(res){
					var info = res; 

					if(info != null && !info.msg){
						var error = "不能对没有审批通过的单据进行反审批";
						shr.showError({message: error});
						return;
					}
				}
			});

			
		}

		shr.showConfirm('您确认要反审批吗？', function(){
			var billIds = $("#grid").jqGrid("getSelectedRows");
			   self.remoteCall({
				method:"againstApprove",
				param : {
					billId : billIds[0]
				},
				success:function(res){
					if(res.flag == "1")
					{
						shr.showInfo({message: "反审批成功"});
						self.queryGrid();
					}else{
						shr.showError({message: "反审批失败"});
					} 
				}
			});
		});
	}
});
