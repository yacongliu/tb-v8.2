shr.defineClass("shr.jqdocking.JQIpConfigEdit", shr.framework.Edit, {
    initalizeDOM: function() {
        shr.jqdocking.JQIpConfigEdit.superClass.initalizeDOM.call(this);
        var that = this;
        that.hiddenButton();
    },
    /**
     * 测试链接
     */
    testAction: function() {
        var that = this;
        that.remoteCall({
            method: 'test',
            success: (function(res) {
                if (res.state === "success") {
                    shr.showInfo({
                        message: "链接成功，接口状态可用！",
                        hideAfter: 3
                    })
                }
            })
        });
    },
    /**
     * 按钮隐藏
     */
    hiddenButton: function() {
        var that = this;
        var method = shr.getUrlRequestParam("method");
        if (method === "edit") {
            $("#test").hide();
        }
    }
})