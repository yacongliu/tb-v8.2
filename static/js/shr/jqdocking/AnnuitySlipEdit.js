shr.defineClass("shr.jqdocking.AnnuitySlipEdit", shr.framework.Edit, {
    initalizeDOM: function() {
        shr.jqdocking.AnnuitySlipEdit.superClass.initalizeDOM.call(this);
        var that = this;
        that.hiddenButton();
    },
    /**
     * 久其接口同步
     */
    synchronizeAction: function() {
        var _self = this;
        if (_self.validate() && _self.verify()) {
            _self.doSynchronize(event, 'synchronize');
        } else {
            if (_self != top) {
                shr.setIframeHeight(window.name);
            }

        }
    },
    /**
     * 真正的同步方法
     */
    doSynchronize: function(event, action) {
        var _self = this;
        var data = _self.assembleSaveData(action);
        var target;
        if (event && event.currentTarget) {
            target = event.currentTarget;
        }
        shr.doAction({
            target: target,
            url: _self.dynamicPage_url,
            type: 'post',
            data: data,
            success: (function(response) {
                // 普通保存，去除最后一个面包屑，防止修改名字造成面包屑重复
                shrDataManager.pageNavigationStore.pop();

                _self.viewAction(response);
            })
        });
    },
    /**
     * 组装保存时传至服务端的数据
     */
    assembleSaveData: function(action) {
        var _self = this;
        var data = _self.prepareParam(action + 'Action');
        data.method = action;
        data.operateState = _self.getOperateState();
        data.model = shr.toJSON(_self.assembleModel());

        var relatedFieldId = this.getRelatedFieldId();
        if (relatedFieldId) {
            data.relatedFieldId = relatedFieldId;
        }

        return data;
    },
    /**
     * 查看
     */
    viewAction: function(options) {
        var param;
        if ($.isPlainObject(options)) {
            param = options;
        } else {
            param = { method: 'view' };
        }
        // 兼容以前参数为billId的情况
        if (options) {
            param.billId = options;
        }
        var isShrBill = shr.getUrlParam("isShrBill");
        if (isShrBill != null && isShrBill == "true") {
            param.isShrBill = true;
        }
        this.reloadPage(param);
    },
    /**
     * 数据校验 
     */
    validate: function() {
        var workArea = this.getWorkarea(),
            $form = $('form', workArea);
        var flag = $form.wafFormValidator("validateForm", true);
        if (!flag) {
            shr.showWarning({
                message: '数据校验未通过，请修正后再处理',
                hideAfter: 5
            });
        }

        return flag;
    },
    /**
     * 按钮隐藏
     */
    hiddenButton: function() {
        var that = this;
        var state = $("#state").val();
        var method = shr.getUrlRequestParam("method");
        if (state === "1") {
            $("#edit").hide();
            $("#synchronize").hide();
        }
        if (method === "view") {
            $("#synchronize").hide();
        }
    }
})