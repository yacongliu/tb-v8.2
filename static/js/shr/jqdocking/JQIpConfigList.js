shr.defineClass("shr.jqdocking.JQIpConfigList", shr.framework.List, {
    initalizeDOM: function() {
        shr.jqdocking.JQIpConfigList.superClass.initalizeDOM.call(this);
        var that = this;
        that.getListNumber();
    },
    /**
     * 获取列表数量
     */
    getListNumber: function() {
        var that = this;
        that.remoteCall({
            method: 'getListNumber',
            success: (function(res) {
                if (res.state === "success") {
                    var number = res.number;
                    if (number == "0") {
                        $("#addNew").show();
                    } else {
                        $("#addNew").hide();
                    }
                }
            })
        });
    }


})