shr.defineClass("shr.jqdocking.JQCalSchemeList", shr.framework.List, {

    initalizeDOM: function() {
        shr.jqdocking.JQCalSchemeList.superClass.initalizeDOM.call(this);
        var that = this;
        that.queryAction();
    },
    /**
     * 自定义查询条件
     */
    getCustomFilterItems: function() {
        var that = this;
        var filterStr = "";
        var calState = '12'; //核算状态（已审批）
        var isJQ = 1; //是否转久其
        //初始化
        if (isJQ != null && isJQ != "") {
            filterStr += "and isJQ = " + isJQ + " ";
        }
        if (calState != null && calState != "") {
            filterStr += "and calState = '" + calState + "' ";
        }
        //去掉第一个and
        filterStr = filterStr.substring(3);

        return filterStr;
    },
    /**
     * 查询
     */
    queryAction: function() {
        var that = this;
        var filterStr = that.getCustomFilterItems();
        $("#grid").jqGrid('option', 'page', 1);
        $("#grid").jqGrid("option", "filterItems", filterStr).jqGrid("reloadGrid");
    },
    /** 
     * 跳转到薪酬付款单
     */
    xinchouAction: function() {
        var that = this;
        var bid = $("#grid").jqGrid("getSelectedRows");
        if (bid == undefined || bid.length == 0 || (bid && bid.length == 1 && bid[0] == "")) {
            shr.showWarning({
                message: "请选择数据进行操作!",
                hideAfter: 3
            });
            return;
        }
        if (bid.length > 1) {
            shr.showWarning({
                message: "一次只能操作一条数据哦!",
                hideAfter: 3
            });
            return;
        }
        that.reloadPage({
            uipk: "com.kingdee.eas.custom.jqdocking.app.PaySlip.form",
            method: "addNew"
        });
    },
    /** 
     * 跳转到社保付款单
     */
    shebaoAction: function() {
        var that = this;
        var bid = $("#grid").jqGrid("getSelectedRows");
        if (bid == undefined || bid.length == 0 || (bid && bid.length == 1 && bid[0] == "")) {
            shr.showWarning({
                message: "请选择数据进行操作!",
                hideAfter: 3
            });
            return;
        }
        if (bid.length > 1) {
            shr.showWarning({
                message: "一次只能操作一条数据哦!",
                hideAfter: 3
            });
            return;
        }
        that.reloadPage({
            uipk: "com.kingdee.eas.custom.jqdocking.app.SocialSecuritySlip.form",
            method: "addNew"
        });
    },
    /** 
     * 跳转到年金付款单
     */
    nianjinAction: function() {
        var that = this;
        var bid = $("#grid").jqGrid("getSelectedRows");
        if (bid == undefined || bid.length == 0 || (bid && bid.length == 1 && bid[0] == "")) {
            shr.showWarning({
                message: "请选择数据进行操作!",
                hideAfter: 3
            });
            return;
        }
        if (bid.length > 1) {
            shr.showWarning({
                message: "一次只能操作一条数据哦!",
                hideAfter: 3
            });
            return;
        }
        that.reloadPage({
            uipk: "com.kingdee.eas.custom.jqdocking.app.AnnuitySlip.form",
            method: "addNew"
        });
    },

    /** 
     * 跳转到公积金付款单
     */
    gongjijinAction: function() {
        var that = this;
        var bid = $("#grid").jqGrid("getSelectedRows");
        if (bid == undefined || bid.length == 0 || (bid && bid.length == 1 && bid[0] == "")) {
            shr.showWarning({
                message: "请选择数据进行操作!",
                hideAfter: 3
            });
            return;
        }
        if (bid.length > 1) {
            shr.showWarning({
                message: "一次只能操作一条数据哦!",
                hideAfter: 3
            });
            return;
        }
        that.reloadPage({
            uipk: "com.kingdee.eas.custom.jqdocking.app.ProvidentFundSlip.form",
            method: "addNew"
        });
    },

});