shr.defineClass("shr.jqdocking.JQitemControlEdit", shr.framework.Edit, {
    initalizeDOM: function() {
        shr.jqdocking.JQitemControlEdit.superClass.initalizeDOM.call(this);
        var that = this;
    },
    //复制
    copyAction: function() {
        var that = this;
        var billId = shr.getUrlRequestParam("billId");
        that.remoteCall({
            method: "copy",
            param: {
                billId: billId
            },
            success: function(newId) {
                that.reloadPage({
                    billId: newId
                });
            }
        });
    }
})