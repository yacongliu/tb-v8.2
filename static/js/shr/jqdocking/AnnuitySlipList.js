shr.defineClass("shr.jqdocking.AnnuitySlipList", shr.framework.List, {
    initalizeDOM: function () {
        shr.jqdocking.AnnuitySlipList.superClass.initalizeDOM.call(this);
        var that = this;
    },
    /**
     *删除
     */
    deleteAction: function () {
        var self = this;
        var selectedIds = shr.jqdocking.AnnuitySlipList.superClass.getSelectedIds();
        /*每次只允许删除一条记录*/
        if (selectedIds.split(',').length > 1) {
            shr.showWarning({
                message: "每次只能对一条数据进行操作哦！",
                hideAfter: 3
            });
            return;
        }
        /*判断单据状态 只有保存状态的单据可以删除*/
        self.remoteCall({
            method: 'getState',
            param: {
                billId: selectedIds
            },
            success: function (data) {
                var messages;
                if (data != null && data != undefined) {
                    messages = data.msg;
                }
                if (messages == 'success' && selectedIds != null) {
                    //执行删除
                    self.deleteRecord(selectedIds);
                } else {
                    shr.showWarning({
                        message: "该单据目前的状态不支持删除哦！",
                        hideAfter: 3
                    });
                }
            }

        });

    },

})