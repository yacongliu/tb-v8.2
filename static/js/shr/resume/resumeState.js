/**
 * 简历初步筛选
 */
shr.defineClass("shr.duty.resumeState",shr.framework.List,{
	initalizeDOM:function(){
		var that=this;
		shr.duty.resumeState.superClass.initalizeDOM.call(that);
	},
	/**
	* 设置筛选结果状态
	*/
    setStateAction:function(){
    	var self = this;
    	var billIds = shr.duty.resumeState.superClass.getSelectedIds();
		
		if (billIds == undefined || billIds.length==0 || (billIds && billIds.length == 1 && billIds[0] == "")) {
			shr.showError({message: "请选中行！ "});
			return ;
		}else{
			self.remoteCall({
				method:'setState', //简历筛选
				 param:{
					billId : billIds
				},
				
			});
		}
	}
	
});
