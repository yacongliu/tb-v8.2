var InterLink = '';
var isLastLink = false;
var lastLink = '';
shr.defineClass("shr.recruitment.interviewResultsList", shr.framework.List, {
    initalizeDOM: function () {
        shr.recruitment.interviewResultsList.superClass.initalizeDOM.call(this);
        var that = this;
        that.processF7ChangeEvent();
        that.setButtonHidden();
        $('#finalRank').hide();
        //导入
        layui.use('upload', function () {
            var upload = layui.upload;
            var param = {
                handler: 'com.kingdee.shr.custom.handler.recuritment.InterviewresultsListHandler',
                method: 'importFile'
            }
            var url = shr.getContextPath() + shr.dynamicURL + "?" + $.param(param);
            upload.render({
                elem: '#importExcel',
                url: url,
                //普通文件
                accept: 'file',
                //只允许上传excel
                exts: 'xls|xlsx',
                done: function (res) {
                    if (res.state === "1") {
                        shr.showWarning({
                            message: "导入失败，请检查当前Excel是否为导入模板！",
                            hideAfter: 3
                        })
                    } else if (res.state === "2") {
                        shr.showWarning({
                            message: "导入失败，请检查当前Excel的文件内容！",
                            hideAfter: 3
                        })
                    } else if (res.state === "3") {
                        shr.showWarning({
                            message: "导入失败！",
                            hideAfter: 3
                        })
                    } else if (res.state === "4") {
                        shr.showWarning({
                            message: "导入失败！只能导入后缀名为.xlsx的Excel文件",
                            hideAfter: 3
                        })
                    } else if (res.state === "success") {
                        var msgs = res.msg;
                        var msg = msgs.substring(0, msgs.lastIndexOf('、'));
                        shr.showInfo({
                            message: "导入成功！" + msg,
                            hideAfter: 5
                        })
                        that.reloadGrid();
                    }

                }
            });
        })
    },
    //计算当前排名
    calAndRankAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定计算所选数据的平均分并排名么？', function () {
                that.remoteCall({
                    method: 'averageScore',
                    param: {
                        billId: ids
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            that.remoteCall({
                                method: 'ranking',
                                param: {
                                    billId: ids
                                },
                                success: (function (res) {
                                    if (res.state === "success") {
                                        shr.showInfo({
                                            message: "计算成功"
                                        })
                                        that.reloadGrid();
                                    }
                                })
                            });
                        }
                    })
                });
            })
        }
    },
    //计算最终排名
    finalRankAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定计算所选数据的最终评分么？', function () {
                that.remoteCall({
                    method: 'averageFinalScore',
                    param: {
                        billId: ids
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            that.remoteCall({
                                method: 'finalRanking',
                                param: {
                                    billId: ids,
                                    lastLink: lastLink
                                },
                                success: (function (res) {
                                    if (res.state === "success") {
                                        shr.showInfo({
                                            message: "计算成功"
                                        })
                                        that.reloadGrid();
                                    }
                                })
                            });
                        }
                    })
                });
            })
        }
    },
    //待定
    undeterminedAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定进行待定操作么？', function () {
                that.remoteCall({
                    method: 'operation',
                    param: {
                        billId: ids,
                        operation: "undetermined"
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            shr.showInfo({
                                message: "操作成功"
                            })
                            that.reloadGrid();
                        }
                    })
                });
            })
        }
    },
    //通过
    passAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定进行通过操作么？', function () {
                that.remoteCall({
                    method: 'operation',
                    param: {
                        billId: ids,
                        operation: "pass"
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            shr.showInfo({
                                message: "操作成功"
                            })
                            that.reloadGrid();
                        }
                    })
                });
            })
        }
    },
    //不通过
    nopassAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定进行不通过操作么？', function () {
                that.remoteCall({
                    method: 'operation',
                    param: {
                        billId: ids,
                        operation: "nopass"
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            shr.showInfo({
                                message: "操作成功"
                            })
                            that.reloadGrid();
                        }
                    })
                });
            })
        }
    },
    //放弃面试
    giveupinterviewAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定进行放弃面试操作么？', function () {
                that.remoteCall({
                    method: 'operation',
                    param: {
                        billId: ids,
                        operation: "giveupinterview"
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            shr.showInfo({
                                message: "操作成功"
                            })
                            that.reloadGrid();
                        }
                    })
                });
            })
        }
    },
    //终止面试
    endinterviewAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定进行终止面试操作么？', function () {
                that.remoteCall({
                    method: 'operation',
                    param: {
                        billId: ids,
                        operation: "endinterview"
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            shr.showInfo({
                                message: "操作成功"
                            })
                            that.reloadGrid();
                        }
                    })
                });
            })
        }
    },
    //录用报批
    approvalAction: function () {
        var that = this;
        var ids = that.getSelectIds();
        if (ids.length == 0) {
            shr.showWarning({
                message: "请先选择要操作的数据",
                hideAfter: 3
            })
        } else {
            shr.showConfirm('确定进行录用报批么？', function () {
                that.remoteCall({
                    method: 'approval',
                    param: {
                        billId: ids
                    },
                    success: (function (res) {
                        if (res.state === "success") {
                            shr.showInfo({
                                message: "录用报批成功"
                            })
                            that.reloadGrid();
                        }
                    })
                });
            })
        }
    },
    //用来屏蔽action
    xxxAction: function () {
        return;
    },
    //获取所选行的ID
    getSelectIds: function () {
        var that = this;
        var bId = $("#grid").jqGrid("getSelectedRows");
        var ids = [];
        for (var i = 0; i < bId.length; i++) {
            ids.push($("#grid").jqGrid("getCell", bId[i], "id"));
        }
        if (ids.length > 0) {
            ids = ids.join(",")
        }
        return ids;
    },
    /**
     * 隐藏创建 删除 按钮
     */
    setButtonHidden: function () {
        $('#addNew').hide();
    },
    processF7ChangeEvent: function (that) {
        var that = this;
        $('#InterviewScheme').shrPromptBox("option", {
            onchange: function (e, value) {
                if (value.current != "" && value.current.id != "") {
                    InterLink = '';
                    var schemeId = value.current.id;
                    that.initBodyDomWithInitScheme(that, schemeId);
                }
            }
        });
    },
    /**
     * 根据基础资料中的面试方案环节信息进行初始化
     * @param that
     * @param schemeId 方案id
     */
    initBodyDomWithInitScheme: function (that, schemeId) {
        var that = this;

        that.remoteCall({
            method: 'getBodyData',
            param: {
                schemeId: schemeId
            },
            success: (function (res) {
                if (res != null || res != "") {
                    var links = eval(res.data);
                    var len = links.length;
                    if (len <= 0) {
                        shr.showWarning({
                            message: "该面试方案尚未设置面试环节！",
                            hideAfter: 3
                        });
                    } else {
                        var recTypeHTML = "";
                        for (var i = 0; i < len; i++) {
                            var plan = links[i];
                            recTypeHTML += '<input name="interviewPlan" type="radio" id="interviewPlan' + plan.ipid + '" class="radio" data-state="' + plan.isLastLink + '" data-name="' + plan.interviewStageName + '" /> <label for="interviewPlan' + plan.ipid + '" class="radio_label">' + plan.interviewStageName + '</label>';
                        }
                        $("#interviewPlan").find(".radiowrap").html(recTypeHTML);
                    }
                } else {
                    shr.showWarning({
                        message: "数据获取失败！",
                        hideAfter: 3
                    });
                }
            })

        });
    },
    //监听单选框
    monitorinterviewPlan: function () {
        $(document).off(".radio").on('click', '.radio', function () {
            var type = $("#interviewPlan input[type='radio']:checked");
            InterLink = type.attr("data-name");
            isLastLink = false;
            if (type.attr("data-state") == 'true') {
                isLastLink = true;
                lastLink = type.attr("data-name");
            }
        })
    },
    //过滤条件
    getCustomFilterItems: function () {
        var that = this;
        var filterStr = "";
        var date = new Date;
        var year = date.getFullYear();
        that.monitorinterviewPlan();
        var plan = $('#InterviewScheme').val();
        if (InterLink != null && InterLink != "") {
            filterStr += "and InterLink = '" + InterLink + "'";
        }
        if (plan != null && plan != "") {
            filterStr += "and InterviewPlan.name = '" + plan + "'";
        }
        if (year != null && year != "") {
            filterStr += "and createYear ='" + year + "'";
        }

        //去掉第一个and
        filterStr = filterStr.substring(3);

        return filterStr;
    },
    //查询
    queryAction: function () {
        var that = this;
        var filterStr = that.getCustomFilterItems();
        $("#grid").jqGrid('option', 'page', 1);
        $("#grid").jqGrid("option", "filterItems", filterStr).jqGrid("reloadGrid");
        //隐藏最终排名按妞
        if (isLastLink) {
            $('#finalRank').show();
        } else {
            $('#finalRank').hide();
        }

    },
})