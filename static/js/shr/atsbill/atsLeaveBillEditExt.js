shr.defineClass("shr.atsbill.AtsLeaveBillEditExt", shr.ats.AtsLeaveBillEdit, {

    initalizeDOM: function () {
        var _self = this;
        shr.atsbill.AtsLeaveBillEditExt.superClass.initalizeDOM.call(this);
    },
 
	assistantAction:function(){
		var _slef=this;
		//进入到请假助手按钮
		//console.log("请假助手进来了");
		$('body').append(this.getAssistantDivHtml());
		_slef.reasonOnClick();
	}

	,getAssistantDivHtml: function() {		
	return ['<div id="assistant_div" class="modal hide fade">',
				'<div class="modal-header">',
					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
					'<h5>请假助手</h5>',
				'</div>',
				'<div id="assistant-content"  class="modal-body">',
				'</div>',
			'</div>'].join('');
	},
	
	
	reasonOnClick: function() {
		this.remoteCall({
			method:'getHolidaySystem',
			success:function(data){
				var b=data;
				if(b.holidaySystem!=undefined){
					$('#assistant-content').html(b.holidaySystem);	
				}
			}
		});	
		$('#assistant_div').modal('show');
	}


});