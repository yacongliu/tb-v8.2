shr.defineClass("shr.exs.ExaminscoreEdit", shr.framework.Edit, {
	initalizeDOM : function () {
		var _self = this;
		shr.exs.ExaminscoreEdit.superClass.initalizeDOM.call(this);
		if(this.getOperateState() != "VIEW"){
			if(this.getOperateState() == 'ADDNEW'){
			 $("#score").val(100);
			 _self.initalRecruitment();
			}
			_self.monitorEntryPersonF7Change();
			_self.monitorEntryDeductionChange();
			
			
		}	
	},
	/**
	*带出负面项目清单
	*/
	initalRecruitment: function() {
    	var self = this;
    	if (this.getHistoryOperateState() != "VIEW") {
        
         var adminOrg=$("input[name='adminOrg']").attr("value");
		 if (adminOrg!=undefined){
			 	self.remoteCall({
                    	method: 'getProject',
                    	success: function(data) {
                        	self.initalEntry(data.orgIds);
                    	}
                	});
		 }            	
        	
    	}
	},
	//根据提交人部门，初始化负面清单和评分标准
	initalEntry: function(data){
		var rowdatas = data;
		if(data!=undefined){
			var len=rowdatas.length;
			waf("#entrys").wafGrid("clearGridData");
			for(var i = 0 ; i < len; i++){
				var datarow = {
					"project":rowdatas[i].project,
					"standard":rowdatas[i].standard,
				};
				waf("#entrys").jqGrid('addRow',{data:datarow});
			}			
		}else{
		shr.showError({message: "没有已审批通过的负面项目清单，无法进考核评分！"});}
	},
	/**
	* 监听被考核人员F7值改变
	*/
	monitorEntryPersonF7Change: function(){
		var self = this;
        //var grid = waf("#person");
	$("#person").shrPromptBox("option",{
		onchange:function(e,value){
			if(value.current==null){
				return;
			} 
			var personId = value.current.id;
			self.remoteCall({
						method : 'getJobInfo',
						param : {
							personId : personId
						},
						success : function(data) {
							
							
							var  position= data.position;
							if (position != undefined) {
							var data2 = {
								id : position.id,
								name : position.name
							};
								$("#position").shrPromptBox("setValue", data2);
								}
							
							
						}
						
					});
		}
	});
	},
	/**
	 *考核成绩
	 */
	monitorEntryDeductionChange:function(){
		var self = this;
						
		var grid = waf("#entrys");
        grid.wafGrid("option", {
		afterSaveCell:function (rowid, cellname, value, iRow, iCol) {
               	if(cellname == "deduction"){
										
					var deduction = value;
					if(isNaN(deduction) ||deduction<0 || deduction>100){
					shr.showError({message: "扣分输入有误，分数值最小为0最大为100！"});
					}else{	
					var score=$("input[name='score']").attr("value");					
					var count =Number(score)-Number(deduction);
					if(count>=0){
					 $("#score").val(count);
				
					}else{
						shr.showError({message: "扣分输入有误，扣分数值总和最小为0最大为100！"});
					}
					}

				}
            }
        });

	}
});