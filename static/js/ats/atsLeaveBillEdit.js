﻿$().ready(function() {
	//解决新增可以动态校验而编辑不可以动态校验的问题
	$.validator.messages.maxlength = $.validator.format( "字符长度不能超过{0}" );
	$("#entries_reason").attr("validate","{maxlength:255}");
});
var _unitType = 0;
var yearGolbal = new Date().getFullYear();
var ev_ev = [];
var yearVacationId = '3T54RtSQRIqAL6cffMh60P0tUpg=';
var _shiftTime;
var _isHalfDayOff = false;
var _defaultAmBeginTime = "09:00";
var _defaultAmEndTime = "12:00";
var _defaultPmBeginTime = "13:00";
var _defaultPmEndTime = "17:00";
var _viewBeginLeaveTime = "";
var _viewEndLeaveTime = "";
var _attendanceFileMark="";
// 共用的js
// var  pageUipk = "com.kingdee.eas.hr.ats.app.AtsLeaveBillAllForm";
var  yearHolidayTypeId = "3T54RtSQRIqAL6cffMh60P0tUpg="
shr.defineClass("shr.ats.AtsLeaveBillEdit", shr.framework.Edit, {
	_unitType:0,
	initalizeDOM:function(){
		shr.ats.AtsLeaveBillEdit.superClass.initalizeDOM.call(this);
		var that = this ;
		that.setNavigateLine();
		that.setButtonVisible(); //初始化页面安装状态,如果是已经提交的或者审批通过的单据编辑按钮不显示
		/*if ($("#bill_flag").val() != "commissioner") {
			$("#message_head").show();
			that.initPersonalLeaveMess();
		}else if ($("#bill_flag").val() == "commissioner") {
			$("#message_head").hide();  
		}
		*/
		// 将温馨提醒 加入专员中
		$("#message_head").show();
		that.initPersonalLeaveMess();
		that.setAdminOrgUnitStyle();
		//定制校验
		that.myExtendValidate();
		that.myDefaultValidate();
		
		//选择人员的时候，人员的组织信息和电话信息变动的处理方法
		that.processPersonF7ChangeEvent();
		//初始化假期类型
		that.loadTimeAttendanceType();
		//能否修改请假长度
		that.isLeaveLengthEdit();
		//初始化页面设置请假长度单位描述
		that.setLeaveLengthUnit();
		that.setLeaveReasonTextAreaStyle();
		//初始化 是否启动半天假
		if(that.getOperateState() == 'ADDNEW' || that.getOperateState() == 'EDIT'){
			that.getSetIsCtrlHalfDayOff();
			//创建和编辑界面隐藏是否销假
			$("#entries_isCancelLeave").closest("div[data-ctrlrole='labelContainer']").hide();
		}
		//初始化dialog框
		that.initDialog();
		
		if(that.getOperateState() == "VIEW"){//不显示秒
			if($("#entries_beginTime").html()!=""){
				var str = $("#entries_beginTime").html();
				$("#entries_beginTime").html(str.substring(0,str.lastIndexOf(":")));
			}
			if($("#entries_endTime").html()!=""){
				var str = $("#entries_endTime").html();
				$("#entries_endTime").html(str.substring(0,str.lastIndexOf(":")));
			}
		}
		//展示请假单下边的销假单信息
		if (that.getOperateState() == 'VIEW') {
			//给请假时长后面加上请假单位
//		    $('#entries_leaveLength').append('&nbsp;&nbsp;'+ $('#entries_remark').val());	导入的单据remark字段为空/列表是用realUnit字段，保持统一			
			if($('#entries_realUnit').val() == 1 ){
				$('#entries_leaveLength').append('&nbsp;&nbsp;'+ '天');				
			}else{
				$('#entries_leaveLength').append('&nbsp;&nbsp;'+ '小时');		
			}
			that.getPolicyRemark($('#entries_policy').val());
			that.getCancelLeaveBillInfo();
			//隐藏提交生效按钮
			$("#submitEffect").hide();
		}
		if (that.getOperateState() == 'VIEW' || that.getOperateState() == 'ADDNEW' || that.getOperateState() == 'EDIT') {
			$("#cancelLeaveBillInfoDes").hide();
		}
		//计算请假 时间长度,查看界面不计算
		/*if (that.getOperateState() != 'VIEW') {
			that.calculateLeaveLength();
		}*/
		//处理4个隐藏字段
		if (that.getOperateState() != 'VIEW') {
			that.processHiddenFiled();
		}
		
		
		
		//longon need
		//选择时间自动带出上午和下午
		that.setBeginOrEnd();
		
		/*
		//计算请假时间长度,查看界面不计算
		if (that.getOperateState() != 'VIEW') {
		    //获得班次信息
			//that.calculateLeaveLength_Longon();
		}
		*/
		that.setBeginTimeAndEndTime();
		that.setNumberFieldEnable();
		
		$('#entries_person').bind('change',function(){
			that.initPersonalLeaveMess();
			that.getSetIsCtrlHalfDayOff();
			that.leaveTimeChangeDealOfDay();
		});
		
		that.leaveTimeChangeDealOfDay();
	
		if(that.getOperateState() == 'EDIT'){
		//that.EditShowStartEndTime();
			if(that.isFromWF()){ // 来自任务中心
				$('#cancelAll').hide();
			}
		}
		
		//隐藏提交生效按钮
		if (that.getOperateState() == 'VIEW') {			
			if(that.isFromWF()){ // 来自任务中心
				$('#submitEffect').hide();
				$('#cancelAll').hide();
				$('#submit').text("提交");
//				$('#edit').hide();
			}
		}
		
		if (shr.getCurrentViewPage().uipk == "com.kingdee.eas.hr.ats.app.AtsLeaveBillForm") {
			if (shrDataManager.pageNavigationStore.getDatas().length == 1) {
				$("#breadcrumb").find("li.active").html("我要请假");
				var a = shrDataManager.pageNavigationStore.getDatas()[0];
				a.name = '我要请假';
				shrDataManager.pageNavigationStore.pop();
				shrDataManager.pageNavigationStore.addItem(a);
			}
			if (shrDataManager.pageNavigationStore.getDatas().length == 2) {
				$("#breadcrumb li")[1].remove();
				$("#breadcrumb li").html("我要请假");
				shrDataManager.pageNavigationStore.pop();
			}

			if (shrDataManager.pageNavigationStore.getDatas().length == 3) {
				$($("#breadcrumb li")[2]).html("创建");
			}

		}
		
		if(that.isFromWF()){
			$("#addInstanceToDeskItem").css('display','none');
		}

		var paramMethod = shr.getUrlRequestParam("method");
     	if(paramMethod == null){
     	    $("#breadcrumb").find(".active").text("我要请假");
     	    if(shrDataManager.pageNavigationStore.getDatas().length==0){
     	    	var object_bread_1 = {
     	    			name: "我要请假",
     	    			     
						url: window.location.href,
     	    			workPlatformId: "Qz9UfhLqB0+vmMJ4+80EykRLkh4="
     	    	}
     	    	shrDataManager.pageNavigationStore.pop();
				shrDataManager.pageNavigationStore.addItem(object_bread_1);
     	    }
     	}
		
	}
	,setAdminOrgUnitStyle : function(){
		//$("#entries_adminOrgUnit").parent().parent().parent().parent().css('margin-top','40px');
		//$("div[title='所属行政组织']").parent().css('margin-top','40px');
		//$(".col-lg-4").css('min-height','50px');
	}
	,getSetIsCtrlHalfDayOff : function(){
	var that = this;
	var personId = $("#entries_person_el").val();
	if(personId==null||personId==""){
	personId = $('#entries_person').attr('value');
	}
	//alert(personId);
	that.remoteCall({
			type:"post",
			async: false,
			method:"getSetIsCtrlHalfDayOff",
			param:{personId:personId},
			success:function(res){
				info =  res;
				_isHalfDayOff = info.isHalfDayOff;
				that.setHalfDayOff(info);
			}
		});
	
	}
	,setHalfDayOff : function(info){
	var that = this;
		if(info.isHalfDayOff){
		_defaultAmBeginTime = info.amBeginTime;
		_defaultAmEndTime = info.amEndTime;
		_defaultPmBeginTime = info.pmBeginTime;
		_defaultPmEndTime = info.pmEndTime;
		$('#entries_beginTime').closest('.field-ctrl').eq(0).html('<div id="entries_beginTime" ctrlrole="dateSpanPicker" validate="{required:true,myDtHrMiVldt:true}">'
			+'<div class="dataSpan-time">'
			+'<div class="ui-datepicker-layout">'
			+'<input id="entries_beginTime-time" class="input-height" type="text" validate="{required:true}" name="entries.beginTime-time" ctrlrole="datepicker">'
			+'</div>'
			+'</div>'
			+'<div class="dataSpan-ap">'
			+'<div class="ui-select-layout">'
			+'<input id="entries_beginTime-ap_el" type="hidden" value="null" name="entries.beginTime-ap_el">'
			+'<input id="entries_beginTime-ap" class="input-height cursor-pointer" type="text" validate="{required:true}" name="entries.beginTime-ap" ctrlrole="select">'
			+'</div>'
			+'</div>'
			+'<input class="dateSpanPicker" type="hidden" name="entries.beginTime">'
			+'</div>'
			+'<script type="text/javascript">'
			+'$(function() {'
			+'var dateSpanPicker_json = {'
			+'id: "entries_beginTime",'
			+'readonly: "",'
			+'value: "",'
			+'onChange: null,'
			+'beginOrEnd: "begin",'
			+'enumOptions: "[{\\"value\\": \\"09:00-12:00\\", \\"alias\\": \\"' + "上午 " + _defaultAmBeginTime+'\\"},{\\"value\\": \\"12:00-13:00\\", \\"alias\\": \\"' + "下午 " +_defaultPmBeginTime+'\\"}]",'
			+'validate: "{required:true}"'
			+'};'
			+'$("#entries_beginTime").shrDateSpanPicker(dateSpanPicker_json);'
			+'});'
			+'</script>');
			
			$('#entries_endTime').closest('.field-ctrl').eq(0).html('<div id="entries_endTime" ctrlrole="dateSpanPicker" validate="{required:true,myDtHrMiVldt:true}">'
			+'<div class="dataSpan-time">'
			+'<div class="ui-datepicker-layout">'
			+'<input id="entries_endTime-time" class="input-height" type="text" validate="{required:true}" name="entries.endTime-time" ctrlrole="datepicker">'
			+'</div>'
			+'</div>'
			+'<div class="dataSpan-ap">'
			+'<div class="ui-select-layout">'
			+'<input id="entries_endTime-ap_el" type="hidden" value="null" name="entries.endTime-ap_el">'
			+'<input id="entries_endTime-ap" class="input-height cursor-pointer" type="text" validate="{required:true}" name="entries.beginTime-ap" ctrlrole="select">'
			+'</div>'
			+'</div>'
			+'<input class="dateSpanPicker" type="hidden" name="entries.endTime">'
			+'</div>'
			+'<script type="text/javascript">'
			+'$(function() {'
			+'var dateSpanPicker_json = {'
			+'id: "entries_endTime",'
			+'readonly: "",'
			+'value: "",'
			+'onChange: null,'
			+'beginOrEnd: "end",'
			+'enumOptions: "[{\\"value\\": \\"09:00-12:00\\", \\"alias\\": \\"'+ "上午 " +_defaultAmEndTime+'\\"},{\\"value\\": \\"12:00-13:00\\", \\"alias\\": \\"'+ "下午 " +_defaultPmEndTime+'\\"}]",'
			+'validate: "{required:true}"'
			+'};'
			+'$("#entries_endTime").shrDateSpanPicker(dateSpanPicker_json);'
			+'});'
			+'</script>');
		
		//选择时间自动带出上午和下午
		that.setBeginOrEnd();
		//设置 上下午上下班默认时间
		
		
		}else {
			$('#entries_beginTime').closest('.field-ctrl').eq(0).html('<div>'
			+'<input id="entries_beginTime" class="block-father input-height" type="text" dataextenal="" placeholder=""' 
			+'validate="{required:true,myDtHrMiVldt:true}" value="" name="entries.beginTime" ctrlrole="datetimepicker">'
			+'<div class="ui-datepicker-icon">'
			+'</div>'
			+'<script type="text/javascript">'
			+'$(function() {'
			+'var dateTimePicker_json = {};'
			+'dateTimePicker_json.readonly = "";'
			+'dateTimePicker_json.validate = "{required:true}";'
			+'$("#entries_beginTime").shrDateTimePicker(dateTimePicker_json);'
			+'});'
			+'</script>'
			);
			
			$('#entries_endTime').closest('.field-ctrl').eq(0).html('<div>'
			+'<input id="entries_endTime" class="block-father input-height" type="text" dataextenal="" placeholder=""' 
			+'validate="{required:true,myDtHrMiVldt:true}" value="" name="entries.endTime" ctrlrole="datetimepicker">'
			+'<div class="ui-datepicker-icon">'
			+'</div>'
			+'<script type="text/javascript">'
			+'$(function() {'
			+'var dateTimePicker_json = {};'
			+'dateTimePicker_json.readonly = "";'
			+'dateTimePicker_json.validate = "{required:true}";'
			+'$("#entries_endTime").shrDateTimePicker(dateTimePicker_json);'
			+'});'
			+'</script>'
			);


			$('#entries_beginTime,#entries_endTime').blur(function(){
				var value =$(this).val();
				if (value =="" || value == null || value.trim() ==""){
					var t = $(this).closest('.field-ctrl').find("label.error");
					if (t !=undefined && t.length!=0){
						$(t).remove();
					}
					$('<label for="entries_reason" generated="true" class="error" style="display: block;">必录字段</label>"').insertAfter($(this).closest('.field-ctrl').children()[0]);
				}
				
			});
			
			$('#entries_beginTime,#entries_endTime').focus(function(){
				var t = $(this).closest('.field-ctrl').find("label.error");
				if (t !=undefined && t.length!=0){
					$(t).remove();
				}
			});
			
		}
	
			//编辑界面 设置请假开始结束时间
		that.getSetBeginEndTime();
			
	  }
	,getSetBeginEndTime:function(){
		var that = this;
		//赋值请假开始结束时间
		if(that.getOperateState() == 'EDIT'){
		  that.remoteCall({
			type:"post",
			async: true,
			method:"getLeaveBillInfoByBillId",
			param:{billId:$('#id').val()},
			success:function(res){
				info =  res;
				_viewBeginLeaveTime = info.beginTime;
				_viewEndLeaveTime = info.endTime;
				
		  $('#entries_beginTime').val(_viewBeginLeaveTime.substring(0,16));
		  $('#entries_endTime').val(_viewEndLeaveTime.substring(0,16));

		  
		  $('#entries_beginTime-time_el').val(_viewBeginLeaveTime.substring(0,10));
		  $('#entries_endTime-time_el').val(_viewEndLeaveTime.substring(0,10));
		  $('#entries_beginTime-time').val(_viewBeginLeaveTime.substring(0,10));
		  $('#entries_endTime-time').val(_viewEndLeaveTime.substring(0,10));
		 // alert(_viewBeginLeaveTime.substring(0,10));
		  
		  $('#entries_beginTime-ap_el').val(_viewBeginLeaveTime.substring(11,16));
		  $('#entries_endTime-ap_el').val( _viewEndLeaveTime.substring(11,16));
		  
		  var preBeginTime = _viewBeginLeaveTime.substring(11,13)<=12?"上午 ":"下午 ";
		  var preEndTime = _viewEndLeaveTime.substring(11,13)<=12?"上午 ":"下午 ";
		  $('#entries_beginTime-ap').val(preBeginTime + _viewBeginLeaveTime.substring(11,16));
		  $('#entries_endTime-ap').val(preEndTime + _viewEndLeaveTime.substring(11,16));
		  
		  
		 // alert(_viewEndLeaveTime.length);
		   /*
		  var beginLeaveTimeInt = parseInt(_viewBeginLeaveTime.substring(11,13));
		  var endLeaveTimeInt = parseInt(_viewEndLeaveTime.substring(11,13));

		  if(beginLeaveTimeInt<=12){
		  $('#entries_beginTime-ap').val('上午');
		  }else{
		  $('#entries_beginTime-ap').val('下午');
		  }
		  
		  if(endLeaveTimeInt<=12){
		  $('#entries_endTime-ap').val('上午');
		  }else{
		  $('#entries_endTime-ap').val('下午');
		  }
		  */
		  
			}
		});
		
		  
		  
		}
	
	}
	/**
	 * 设置编码字段是否可编辑
	 */
	,setNumberFieldEnable : function() {
		var that = this ;
		if (that.getOperateState().toUpperCase() == 'EDIT' ||　that.getOperateState().toUpperCase() == 'ADDNEW') {
			var leaveBillNumberFieldCanEdit = that.initData.leaveBillNumberFieldCanEdit;
			if (typeof leaveBillNumberFieldCanEdit != 'undefined' && !leaveBillNumberFieldCanEdit) {
				that.getField('number').shrTextField('option', 'readonly', true);
			}
		}
	}
	
	,setBeginOrEnd:function(){
		var that = this ;
		if (that.getOperateState() != 'VIEW') {
			$("#entries_beginTime-time").change(function(){
				if (that._unitType == 3||$("#entries_remark").val()=="分钟" || that._unitType == 4 || $("#entries_remark").val()=="次" ) {
					$("#entries_beginTime-ap").val("");
					$("#entries_beginTime-ap_el").val("");
				}else{
					/*
					$("#entries_beginTime-ap").val("上午");
					_shiftTime = _defaultAmBeginTime;
					that.getShiftTime($("#entries_beginTime-time").val(),"begin","am");
					$("#entries_beginTime-ap_el").val(_shiftTime);
					*/
					var entrits_beginTime = $("#entries_beginTime-time").val();
					
					if(!$("#entries_endTime-time").val())
					{
						
						$("#entries_endTime-time").val(entrits_beginTime);
						$("#entries_endTime-ap").val("下午 " + _defaultPmEndTime);
						$("#entries_endTime-ap_el").val(_defaultPmEndTime);
					}else if(new Date(entrits_beginTime).getTime() > new Date($("#entries_endTime-time").val()).getTime()){
						
						$("#entries_endTime-time").val(entrits_beginTime);
						$("#entries_endTime-ap").val("下午 " + _defaultPmEndTime);
						$("#entries_endTime-ap_el").val(_defaultPmEndTime);
						
					}
					$("#entries_beginTime-ap").val("上午 " + _defaultAmBeginTime);
					$("#entries_beginTime-ap_el").val(_defaultAmBeginTime);
				}
			});
			$("#entries_endTime-time").change(function(){
				if (that._unitType == 3||$("#entries_remark").val()=="分钟" || that._unitType == 4 || $("#entries_remark").val()=="次" ) {
					$("#entries_endTime-ap").val("");
					$("#entries_endTime-ap_el").val("");
				}else{
					/*
					$("#entries_endTime-ap").val("下午");
					
					_shiftTime = _defaultPmEndTime;
					that.getShiftTime($("#entries_endTime-time").val(),"end","pm");
					
					$("#entries_endTime-ap_el").val(_shiftTime);
					*/
					var entries_endTime = $("#entries_endTime-time").val();
					
					if(!$("#entries_beginTime-time").val())
					{
						$("#entries_beginTime-time").val(entries_endTime);
						$("#entries_beginTime-ap").val("上午 " + _defaultAmBeginTime);
						$("#entries_beginTime-ap_el").val(_defaultAmBeginTime);
					}else if(new Date(entries_endTime).getTime() < new Date($("#entries_beginTime-time").val()).getTime()){
						
						$("#entries_beginTime-time").val(entries_endTime);
						$("#entries_beginTime-ap").val("上午 " + _defaultAmBeginTime);
						$("#entries_beginTime-ap_el").val(_defaultAmBeginTime);
						
					}
					
					
					
					$("#entries_endTime-ap").val("下午 " + _defaultPmEndTime);
					$("#entries_endTime-ap_el").val(_defaultPmEndTime);
				}
			});
			$("#entries_beginTime-ap").change(function(){
			
				$("#entries_beginTime-ap_el").val($("#entries_beginTime-ap").val().substring(3));
				/*
				if($("#entries_beginTime-ap").val()=="09:00"){
				
					 _shiftTime = _defaultAmBeginTime;
					//that.getShiftTime($("#entries_beginTime-time").val(),"begin","am");
					
					$("#entries_beginTime-ap_el").val(_shiftTime);
				}else if($("#entries_beginTime-ap").val()=="13:00"){
					 _shiftTime = _defaultPmBeginTime;
					//that.getShiftTime($("#entries_beginTime-time").val(),"begin","pm");
									
				 $("#entries_beginTime-ap_el").val(_shiftTime);
				}
				*/
			});
			$("#entries_endTime-ap").change(function(){
			
			
				$("#entries_endTime-ap_el").val($("#entries_endTime-ap").val().substring(3));
				/*
				if($("#entries_endTime-ap").val()=="12:00"){
			
					 _shiftTime = _defaultAmEndTime;
					//that.getShiftTime($("#entries_endTime-time").val(),"end","am");
					$("#entries_endTime-ap_el").val(_shiftTime);
				}else if($("#entries_endTime-ap").val()=="17:00"){
					 _shiftTime = _defaultPmEndTime;
					//that.getShiftTime($("#entries_endTime-time").val(),"end","pm");
					$("#entries_endTime-ap_el").val(_shiftTime);
				}
				*/
			});
			
		}
	}
	,getPolicyRemark:function(policyId){
		var _self = this;
		_self.remoteCall({
			type:"post",
			async: true,
			method:"getPolicyRemark",
			param:{policyId:policyId},
			success:function(res){
				info =  res;
				var remark = "";
				if(info.policyRemark){
					remark = "" + info.policyRemark ;
				}
				$('#entries_policy').attr('title',remark);
			}
		});
		
	}
	,getShiftTime:function(date,beginOrEnd,amOrPm){
	var that = this;
	var personId = $("#entries_person_el").val();
	that.remoteCall({
			type:"post",
			async: true,
			method:"getShiftTime",
			param:{personId:personId,date:date,beginOrEnd:beginOrEnd,amOrPm:amOrPm},
			success:function(res){
				info =  res;
				_shiftTime = info.shiftTime;
			}
		});
	}
	//提交即生效
	,submitEffectAction : function (event) {
		var _self = this,
		workArea = _self.getWorkarea(),
		$form = $('form', workArea);
		
		if(_attendanceFileMark !=""){
			shr.showWarning({message: _attendanceFileMark});
			return;
		}
		if ($form.valid() && _self.verify()&&_self.checkAnnualLeaveAction()) {
			if(shr.atsBillUtil.isInWorkFlow(_self.billId)){
				shr.showConfirm('工作流已产生，提交生效将废弃工作流，确认废弃？', function() {
					//start 如果是否启动半天假为是  处理上下午的请假时间 合成为时间
					if(_isHalfDayOff){
					$('#entries_beginTime').val($('#entries_beginTime-time').val()+" "+$('#entries_beginTime-ap_el').val());
					$('#entries_endTime').val($('#entries_endTime-time').val()+" "+$('#entries_endTime-ap_el').val());
					}
					//end	
					
					_self.prepareSubmitEffect(event, 'submitEffect');
				});
			}else{
				shr.showConfirm('您确认要提交生效吗？', function() {
					//start 如果是否启动半天假为是  处理上下午的请假时间 合成为时间
					if(_isHalfDayOff){
					$('#entries_beginTime').val($('#entries_beginTime-time').val()+" "+$('#entries_beginTime-ap_el').val());
					$('#entries_endTime').val($('#entries_endTime-time').val()+" "+$('#entries_endTime-ap_el').val());
					}
					//end	
					
					_self.prepareSubmitEffect(event, 'submitEffect');
				});
			}
		}	
	}
	
	//检查年假
	,checkAnnualLeaveAction : function () {
		var that = this ;
		//获取到人员id
		var personId =  $("#entries_person_el").val();
		//获取假期类型
		var policy =  $("#entries_policy").val();
		if(policy == "年假"){
			var options = {
			action: 'checkAnnualLeave',
			handler: 'com.kingdee.shr.ats.web.handler.custom.AtsLeaveBillEditHandlerEx',
			type: 'POST',
			param:{personId:personId,policy: policy},
			success: function (res) {
				var info = res;
				if(!info.checkResult && info.checkResult != undefined){
					shr.showWarning({message: "您不符合请假制度"});
					return false;
					}
				}
			};
			shr.callHandler(options);
		}
		
		return true;
	}
	
	/**
	 * 提交
	 */
	,submitAction: function(event) {
		var _self = this,
			workArea = _self.getWorkarea(),
			$form = $('form', workArea);
		
		if(_attendanceFileMark !=""){
			shr.showWarning({message: _attendanceFileMark});
			return;
		}
		
		if ($form.valid() && _self.verify()&& _self.checkAnnualLeaveAction()) {
			shr.showConfirm('您确认要提交吗？', function() {
				//start 如果是否启动半天假为是  处理上下午的请假时间 合成为时间
				if(_self.getOperateState() != 'VIEW'){
					if(_isHalfDayOff){
						$('#entries_beginTime').val($('#entries_beginTime-time').val()+" "+$('#entries_beginTime-ap_el').val());
						$('#entries_endTime').val($('#entries_endTime-time').val()+" "+$('#entries_endTime-ap_el').val());
					}
				}
				_self.doSubmit(event, 'submit');
			});
		}		
	},
	
	doSubmit: function(event, action) {
		var _self = this;
		var uipk  = _self.uipk;
		var assembleModelobj = _self.assembleModel();
		assembleModelobj.entries.policy = $("#entries_policy_el").val();
		if(_self.getOperateState() == 'ADDNEW' || _self.getOperateState() == 'EDIT'){
			assembleModelobj.entries.leaveLength = $("#entries_leaveLength").val().replace(/[^0-9.]/ig,"");
		}else{
			assembleModelobj.entries.leaveLength = $("#entries_leaveLength").text().replace(/[^0-9.]/ig,"");
		}
		var model = shr.toJSON(assembleModelobj);
		shr.callService({
			serviceName: 'getWfNextPersonsInfoService',
			async: true,
			param: {
				uipk: uipk,
				model: model
			},
			success: function(data){
				if(data.WFISSETNEXTPERSON && (data.WFISSETNEXTPERSON == 'true' || data.WFISSETNEXTPERSON == true) && !(shr.getUrlParam("isShrBill"))){
					if(data.nextpermsg && data.nextpermsg.length > 0){
						_self.getWorkFlowHelper().handlerSetNextPerson(_self,data.nextpermsg,function(){_self.actSubmit(event, 'submit');});
					}
				}else{
					_self.actSubmit(event, 'submit');
				}
			}
		});
	}
	
	,assembleSaveData: function(action) {
		var _self = this;
		var data = _self.prepareParam(action + 'Action');
		data.method = action;
		data.operateState = _self.getOperateState();
		if(_self.getOperateState() !='VIEW')
		{
			data.model = shr.toJSON(_self.assembleModel()); 
		}else{
			var model = shr.assembleModel(_self.fields, _self.getWorkarea(), _self.uuid);
			//var model = [] ;
			var entries = [] ;
			//var entries={};
			var entrie = {
	 			adminOrgUnit: $("#entries_adminOrgUnit").val(),
				beginTime:  $("#entries_beginTime").text(),
				endTime: $("#entries_endTime").text(),
			
				leaveLength: $("#entries_leaveLength").text().replace(/[^0-9.]/ig,""),
				person : $("#entries_person").val(),
						
				policy:  $("#entries_policy").val(),
				realBeginTime : $("#entries_realBeginTime").val(),
			    realEndTime : $("#entries_realEndTime").val(),
			    realLeaveLength : $("#entries_realLeaveLength").val(),
			    realUnit : $("#entries_realUnit").val(),
			    reason :$("#entries_reason").text(),
			    remark :$("#entries_remark").val()
				
	 		}
	 		var id = $("#id").val() 
			var number = $("#number").text()
			var proposer = $("#proposer").val() 
	 		model.entries = entrie;
	 		model.id = id;
	 		model.number = number;
	 		model.proposer = proposer;
			
			data.model = shr.toJSON(model); 
		}
		
		
		// relatedFieldId
		var relatedFieldId = this.getRelatedFieldId();
		if (relatedFieldId) {
			data.relatedFieldId = relatedFieldId;
		}
		
		return data;
	}
	,prepareSubmitEffect : function (event, action){
		var _self = this;
		var data = _self.assembleSaveData(action);
		
		var target;
		if (event && event.currentTarget) {
			target = event.currentTarget;
		}
		shr.doAction({
			target: target,
			url: _self.dynamicPage_url,
			type: 'post', 
			data: data,
			success : function(response) {
				_self.back();
			}
		});	
	}

 ,saveAction: function(event) {
//		if ($(".error").length>0) return;
		var _self = this;
		
		if(_attendanceFileMark !=""){
			shr.showWarning({message: _attendanceFileMark});
			return;
		}
			
		if (_self.validate() && _self.verify()) {
		if(_isHalfDayOff){
		$('#entries_beginTime').val($('#entries_beginTime-time').val()+" "+$('#entries_beginTime-ap_el').val());
		$('#entries_endTime').val($('#entries_endTime-time').val()+" "+$('#entries_endTime-ap_el').val());
		}		
		//alert($('#entries_beginTime').val());
		//alert($('#entries_beginTime-ap').val());
		//alert($('#entries_beginTime-ap_el').val());
		//$('#entries_beginTime-ap').val($('#entries_beginTime-ap_el').val());
		//$('#entries_endTime-ap').val($('#entries_endTime-ap_el').val());	
			_self.doSave(event, 'save');
		}	
	}
	,myExtendValidate:function(){ //扩展自定义校验
	      jQuery.extend(jQuery.validator.messages, {  
			myTmVldt: "请输入正确的日期时间" //日期的格式和有效性
		  });  
		  jQuery.validator.addMethod("myDtHrMiVldt", function(value, element) {  
	    	   var v=value||'';
	    	   if ( (new String(v)).length != 16 && (new String(v)).length != 19 ) {
	    	   	  return false;
	    	   }
	    	   
	    	   //2014-01-23 09:00
	    	   //日期有效性
	    	   var vd=(new String(v)).substring(0,10);
	    	   /*日期无效则'=='左边值为NaN，右边无论是什么(''、null、undefined、NaN等)结果都为false.
	    	    *通过新建的Date对象(new Date(date))，可以识别出该日期是否正确，如果不正确则返回Invalid Date
				但这样会有一个bug，当日期的值在1-31之间，new Date总返回一个新的对象，不论该月份是否存在这个日期(如2013-02-30将返回日期对象Sat Mar 02 2013 08:00:00 GMT+0800 (中国标准时间))，返回结果是下个月的日期而不报错
				所以要用getDate()方法获取日期(new Date('2013-02-30')对象的getDate()方法将返回2)
				date.substring(date.length-2)会获取到字符串最后两位也就是日期的部分，这与Date对象的日期部分做比较，如果相等则说明日期有效，否则为无效日期
	    	    * */
				vd = vd=vd.replace(/-/g,'/');//解决兼容性问题2017-01-01这种格式无法被所有浏览器兼容
	    	   if( (new Date(vd).getDate()==vd.substring(vd.length-2)) ){
	    	   }else{
	    	     return false;
	    	   }
	    	   
	    	   //时分/或者秒
	    	   var vt= (new String(v)).substring(11);
	    	   if(/[0-5][0-9]:[0-5][0-9]/.test(vt) || /[0-5][0-9]:[0-5][0-9]:[0-5][0-9]/.test(vt)){
	    	   	  var h=new Number(vt.substr(0,2));
	    	   	  if(h<24){
	    	   	  
	    	   	  }
	    	   	  else
	    	   	    return false;
	    	   }else{
	    	   	  return false;
	    	   }
	    	   
	    	   return true;
		  }, "请输入正确的日期时间"); 
	}
	,myDefaultValidate:function(){
		$('#entries_endTime,#entries_beginTime').attr("validate","{required:true,myDtHrMiVldt:true}");
	}
	,setLeaveReasonTextAreaStyle : function(){
//		$("#entries_reason").parents(".span9").prev().remove();
	}
	/**
	 * 点击取消按钮 返回到个人请假列表list(个人) ||  com.kingdee.eas.hr.ats.app.AtsLeaveBill
	 */
	,cancelAction:function(){
		/*var that = this ;
	 	window.location.href = shr.getContextPath()+"/dynamic.do?uipk=com.kingdee.eas.hr.ats.app.AtsLeaveBillList";*/
		if (!this.isFromWF()) {
			this.reloadPage({
				uipk: 'com.kingdee.eas.hr.ats.app.AtsLeaveBillList'
			});
		}else{
			//$("#workAreaDiv").remove();
			//$(this).dialog('close');
		}
		
	 }
	//专员返回专员列表的取消按钮
	,cancelAllAction:function(){
		/*var that = this ;
	 	window.location.href = shr.getContextPath()+"/dynamic.do?uipk=com.kingdee.eas.hr.ats.app.AtsLeaveBillAllList";*/
	 	this.reloadPage({
			uipk: 'com.kingdee.eas.hr.ats.app.AtsLeaveBillAllList'
		});
	}
	
	//返回个人请假列表链接跳转
	,returnToLeaveBillListAction:function(){
	  // window.location.href = shr.getContextPath()+"/dynamic.do?uipk=com.kingdee.eas.hr.ats.app.AtsLeaveBillList";
	   this.reloadPage({
			uipk: 'com.kingdee.eas.hr.ats.app.AtsLeaveBillList'
		});
	}
	
	,processPersonF7ChangeEvent : function(){
		var that = this;
		if (that.getOperateState() != 'VIEW') {
			$("#entries_person").shrPromptBox("option", {
				onchange : function(e, value) {
					var info = value.current;
					if(info != null){
				    if(info.hasOwnProperty("id")){
					
						info["id"] = info["id"];
						info["name"] = info["name"];
						that.remoteCall({
							type:"post",
							method:"getPersonInfos",
							param:{personId: info.id},
							success:function(res){
								$("#entries_person_number").val(res.personNum);
								$('#entries_adminOrgUnit_el').val(res.adminOrgUintId);		//部门ID
								$('#entries_adminOrgUnit').val(res.adminOrgUintName.name);	//部门名称
								that.reloadTimeAttendanceType();
								that.isLeaveLengthEdit();
							}
						});
				    }
					}
//					var url = shr.getContextPath() + "/leaveBill.do?method=getPersonInfos";
//					$.post(url, {personId: info.id}, function(datas) {
//						var info = datas;
//						$('#entries_org_el').val( info.data.adminOrgUintId );		//部门ID
//						$('#entries_org').val( info.data.adminOrgUintName.name);		//部门名称
//						$('#phone').val( info.data.officePhone );//联系电话
//						
//					});
				}
			});
		}
	}
	,reloadTimeAttendanceType:function(){
		var that = this;
		if ($("#bill_flag").val() == "commissioner") {
			that.loadTimeAttendanceType();
		}
	}
	
	,goNextPage: function(source) {
		if ($("#bill_flag").val() == "commissioner"){
			_self.reloadPage({
				uipk: "com.kingdee.eas.hr.ats.app.AtsLeaveBillAllList"
			});
		}else{
			_self.reloadPage({
				uipk: "com.kingdee.eas.hr.ats.app.AtsLeaveBillList"
			});
		}
	}
	
	,initDialog:function(){
		var that = this ;
		var dialog_Html = "<div id='dialogViewMore' title=''>" +
				"<p id='ppppp'></p>" +
				"<div class='longDemo demo'>" +
				"	<h2 style='font: 14px Microsoft Yahei; text-align: center; margin-left:95px; width:700px;'>" +
				"		<font style=' font-size:0px;'>test</font>" +
				"		<span style='float:left; display:block;'><a id='a_pre' style='cursor:pointer'>前一年</a></span>" + //onClick='pre()' 
				"		<span style='float:right;display:block;'><a id='a_next' style='cursor:pointer'>后一年</a></span>" + //onClick='next()'
				"	</h2>" +
				"	<div id='longTimeLine'></div>" +
				"</div>" +
				"</div>";
		//$("#container").append(dialog_Html);
			//审核界面  特殊处理
		if($('#breadcrumb').attr('id')!='breadcrumb' && parent.window.shr.getCurrentViewPage().uipk!="com.kingdee.eas.hr.ats.app.WorkCalendar.empATSDeskTop"
			&& parent.window.shr.getCurrentViewPage().uipk!="com.kingdee.eas.hr.ats.app.WorkCalendarItem.listSelf"){
			
			$("#message_info").hide();
		//$("#message_head").hide();
		/*dialog_Html = "<div id='dialogViewMore' style = 'font-size: 12px; padding: 10px; width: 93%' title=''>" +
				"<p id='ppppp'></p>" +
				"<div class='longDemo demo'>" +
				"	<h2 style='font: 14px Microsoft Yahei; margin:0 auto; margin-left:5px;'>" +
				"		<font style=' font-size:0px;'>test</font>" +
				"		<span style='float:left; display:block;'><a id='a_pre' style='cursor:pointer'>前一年</a></span>" + //onClick='pre()' 
				"		<span style='float:right;display:block;'><a id='a_next' style='cursor:pointer'>后一年</a></span>" + //onClick='next()'
				"	</h2>" +
				"	<div id='longTimeLine' style='margin:0 auto;'></div>" +
				"</div>" +
				"</div>";
				
			
			$('#message_head').append("<div id='showViewMore'></div>");
			$('#showViewMore').html(dialog_Html);*/
			//$('#message_head').append(dialog_Html);
		}else{
		$(document.body).append(dialog_Html);
		}
		
		$(".longDemo").hide();
		if($('#breadcrumb').attr('id')!='breadcrumb' && parent.window.shr.getCurrentViewPage().uipk!="com.kingdee.eas.hr.ats.app.WorkCalendar.empATSDeskTop"
			&& parent.window.shr.getCurrentViewPage().uipk!="com.kingdee.eas.hr.ats.app.WorkCalendarItem.listSelf"){
	    	that.showMoreBindClick();
			}else{
		//点击更多请假信息的方法
	    $('#showMoreLeaveInfo').click(function(e){
	    	yearGolbal = new Date().getFullYear();
	    	$(".longDemo").show();
	    	that.clearEventDataInfos();
	  
	        that.showViewMoreDialog();
			
	        that.ajaxLoadAllLeaveBillDatas(yearGolbal);
	    });
	    
		}
	    //绑定点击事件--放在里边会出现绑定多次点击事件
        $('#a_pre').click(function(e){
        	that.clearEventDataInfos();
        	that.loadPreLeaveBillDatas();
        });
        $('#a_next').click(function(e){
        	that.clearEventDataInfos();
        	that.loadNextLeaveBillDatas();
        });
	}
 	/**
	 * 清空ev_ev数据 否则会有重复数据
	 */
	,clearEventDataInfos:function(){
		ev_ev.splice(0,ev_ev.length);//由于ev_ev是全局变量 所以要清空下数据
	}
	/**
     * 默认第一次点击弹出框的时候,默认加载系统当前年份的所有请假记录信息
     * 获取任何一年的所有请假单数据信息
     */
	,ajaxLoadAllLeaveBillDatas:function(yeargolbal){
		var that = this ;
        //var url = shr.getContextPath() + "/leaveBillNew.do?method=getAnyYearLeaveBillDataInfos";
        var personId = that.getFieldValue("entries_person");
		//$.post(url,{personId: personId,currentYear:yeargolbal},function(datas) {
		that.remoteCall({
				type:"post",
				method:"getAnyYearLeaveBillDataInfos",
				param:{personId:personId,currentYear:yeargolbal},
				success:function(res){
					var info = res;
					//alert(JSON.stringify(info));
					var leaveBillColls = JSON.parse(info.AllAtsLeaveBillList);
					var len = leaveBillColls.length;
					//ev = leaveBillColls;
					//ev_ev = leaveBillColls;
					for(var i = 0;i<len;i++){
						var applyDate = leaveBillColls[i].applyDate;//"2013-06-08 00:00:00"
						var beginTime = leaveBillColls[i].entries[0].beginTime;//2013-09-15 08:30:00
						var endTime = leaveBillColls[i].entries[0].endTime;
						var typeName = leaveBillColls[i].entries[0].policy.holidayType.name;
						var length = "";
						if ( typeof(leaveBillColls[i].entries[0].leaveLength) != "undefined" ) {
							length = leaveBillColls[i].entries[0].leaveLength;
						}
						var unitType = "";
						if ( typeof(leaveBillColls[i].entries[0].remark) != "undefined" ) {
							unitType = leaveBillColls[i].entries[0].remark ;
						}
//						if ( typeof(leaveBillColls[i].entries[0].policy.unit) != "undefined" ) {
//							unitType = leaveBillColls[i].entries[0].policy.unit.alias
//						}
						var reason = "";
						if (typeof(leaveBillColls[i].entries[0].reason) != "undefined") {
							reason = leaveBillColls[i].entries[0].reason;
						}
						var state = leaveBillColls[i].billState.alias;
						var rowi={};
						rowi.id=i+1;	//编号
						rowi.beginTime = beginTime;//开始时间
						rowi.endTime = endTime;//结束时间
						rowi.typeName = typeName;//事假
						rowi.length = length;//1
						rowi.unitType = unitType;//天
						rowi.reason = reason;//原因
						rowi.state = state;//审批状态
						rowi.applyDate = applyDate;//申请日期
						//rowi.name="5555";
						var regEx = new RegExp("\\-","gi");
						applyDate = applyDate.replace(regEx,"/");
						beginTime = beginTime.replace(regEx,"/");
						rowi.on= new Date(beginTime);//记录的是请假的开始时间
						ev_ev.push(rowi);
					}
					//that.initJqtimeLine();
					$('.gt-timeline').remove();
			        /*默认函数值*/
					/*groupEventWithinPx 参数是将显示在此范围内PX的共同提示事件，为了防止很近节点重叠问题。设置小点。
					  设置为0，框架默认值是6.完全重合的时候，才叠加。
					*/
					$('#longTimeLine').jqtimeline({
						events : ev_ev,
						numYears:1,
						gap : 55, 
						groupEventWithinPx : 0,
						startYear:yeargolbal
					
					});
			}//success end 
		});//remoteCall end 
	}
	
	,initJqtimeLine : function(){
	}
	,showViewMoreDialog:function(){
	 	$("#dialogViewMore").dialog({
	 		autoOpen: true,
			title: '请假记录',
			width:  850,
			height: 550,
			modal: true,
			resizable: true,
			position: {
				my: 'center',
				at: 'top+47%',
				of: window
			}
			/*,
			close: function() {
				$(this).dialog("close");
				$(".a_pre").remove();
			}*/
		});
	}
	//dialog框 加载前一年的请假单数据
	,loadPreLeaveBillDatas : function(){
		var that = this ;
		//ev_ev.splice(0,ev_ev.length);
		yearGolbal = yearGolbal - 1 ;
		//alert("yearGolbal=="+yearGolbal);
		that.ajaxLoadAllLeaveBillDatas(yearGolbal);
	}
	//dialog框 加载后一年的请假单数据
	,loadNextLeaveBillDatas : function(){
		var that = this ;
		//ev_ev.splice(0,ev_ev.length);
		yearGolbal = yearGolbal + 1 ;
		that.ajaxLoadAllLeaveBillDatas(yearGolbal);
	}
	//请假长度可否编辑
	,isLeaveLengthEdit : function(){
		var that = this ;
		if (that.getOperateState() != 'VIEW'){
			var personId =  $("#entries_person_el").val();
			that.remoteCall({
				type:"post",
				method:"isLeaveLengthEdit",
				param:{personId:personId},
				async: true,
				success:function(res){
					var info = res;
					if(info.errorString){
						shr.showWarning({message: info.errorString});
					}else{
						if(info.editable=='true'){
							$("#entries_leaveLength").removeAttr('readonly').removeAttr('disabled');
							$("#entries_leaveLength").parent().removeClass("disabled");
						}else{
							$("#entries_leaveLength").attr("readonly","readonly");
							$("#entries_leaveLength").parent().addClass("disabled");
						}
					}
				}
			});
		}
	}
	
	/**
	 * 	  加载所有的假期类型<br/>
	 * 1. 假勤项目ID 是 FAttendCatalogID ='00000000-0000-0000-0000-000000000002BE0D0183'<br/>
	 * 2. FHROrgUnitID is null or FHROrgUnitID='当前用户的HROrgUint'<br/>
	 * 3. FEnable = 1 启用状态的<br/>
	 */
	,loadTimeAttendanceType : function(){
		var that = this ;
		//如果新增或者编辑页面,直接从后台返回该人员的可用年假
		if (that.getOperateState() != 'VIEW') {
			//先将假期类型框清空
			$("#message_holidayType").html("");
			
			var personId =  $("#entries_person_el").val();
			$("#attend_type").hide();
			var beginTime = "",endTime = "";
			beginTime = $("#entries_beginTime-time").val();
			if(beginTime){
			}
			else{
				beginTime = $("#entries_beginTime").val();
			}
			
			endTime = $("#entries_endTime-time").val();
			if(endTime){
			}
			else{
				endTime = $("#entries_endTime").val();
			}
			_attendanceFileMark = "";
			that.remoteCall({
				type:"post",
				method:"getTimeAttendanceType",
				param:{personId:personId,startTime: beginTime, endTime: endTime},
				async: true,
				success:function(res){
					var info = res;
					if(info.errorString){
						_attendanceFileMark = info.errorString;
						shr.showWarning({message: info.errorString});
					}else{
					//alert(JSON.stringify(info));
					that.initTableAndDiv(info);
					}
				}
			});
		}else if (that.getOperateState() == 'VIEW') {
			//alert('查看界面');
			$("#attend_type").show();
			$("#attend_type_category").hide();
			//获取当前单据的人员ID 查询该人员的可用年假
		}
	}
	//初始化假期类型框框信息,包括名称,单位,额度
	,initTableAndDiv : function(info){
		var that = this ;
		var attendColl = JSON.parse(info.timeAttendanceCollection);
		//alert(typeof(jsonObj));
		//var attendColl = info.data.timeAttendanceCollection;
		var size = info.timeAttendanceCollectionSize;
		var remainValue = info.timeAttendRemainValue;//年假的剩余额度
		//年假剩余额度赋给隐藏域 在保存的时候做判断使用
		$("#entries_msgValue").val(remainValue);
		
		var table = "";
	   	table = table+
		   "<table >" +
		   " <tr> " + 
		   	" <td id='info_mess' class='td_typeinfo'> " +
			" </td> " +
		   " </tr> " + 
		   "</table> "
		//$("#wrap").css('overflow-y','auto');
		//$("#leaveBillNew_divForm").css('overflow','auto');
		//document.getElementById("leaveBillNew_divForm").style.height=document.getElementById("leaveBillNew_divForm").offsetHeight-100 + "px";
		$("#message_holidayType").html(table);	
		 
		var td_div = "";
		for(var j = 0; j<size; j++){
			holidayPolicyId = attendColl[j].id;
			attendTypeId = attendColl[j].holidayType.id;
			attendTypeName = attendColl[j].name;
			var leaveRemark = "" ;
			if(attendColl[j].remark){
				leaveRemark = attendColl[j].remark ;
			}
			//alert( attendTypeId +"   " + attendTypeName);
			//attendUnit = attendColl[j].unitType;//is obj
			var unitTypeName="";
			var unitTypeValue="";//保存的时候 做个校验???? 
			if (attendColl[j].unit != undefined) {
				unitTypeName = attendColl[j].unit.alias;
				unitTypeValue = attendColl[j].unit.value;
				//that._unitType = unitTypeValue;//单位类型的值
			}
			remainValue_mess = "";
			 vacationRemain = info.vacationRemain;
			  freeRemain = info.freeRemain;
			//循环map
			for (var prop in vacationRemain) {  
  			if (vacationRemain.hasOwnProperty(prop)) {
  				//if(yearHolidayTypeId == prop) // 年假
  				//{
  					if(prop==attendTypeId){
		  			if(vacationRemain[prop]!=""){
		  				remainValue_mess = "<font class='remain_info'>剩余["+ vacationRemain[prop]  +"]"+",在途["+freeRemain[prop]+"]</font>";
		  			}
		  			else{
		  				remainValue_mess = "<font class='remain_info' style='display:none;'>剩余["+ vacationRemain[prop]  +"]"+",在途"+freeRemain[prop]+"</font>";
		  			}
  				} 
  			 // }
  				  
    		//alert("prop: " + prop + " value: " + allVacationMap[prop].remainLimitValue)  
  			}  
			}  
			//3T54RtSQRIqAL6cffMh60P0tUpg=年假ID 的值-初始化数据的
			/*var remainValue_mess = "";
			if (attendTypeId != "3T54RtSQRIqAL6cffMh60P0tUpg=") {//年假 00000000-0000-0000-0000-000000000008649E3405
				remainValue_mess = "";//不控制额度
			}else{
				remainValue_mess = "<font class='remain_info'>剩余额度"+ remainValue  +"天</font>"; 
			}*/
			
			if(j>=4){//如果大于4 设置隐藏 显示更多
			td_div = td_div  + 
			"<div id='div"+j+"' style='display:none' title = '"+leaveRemark+"' class='div_blockinfo' onclick='changeColor("+j+","+size+",\""+holidayPolicyId+"\",\""+attendTypeId+"\",\""+attendTypeName+"\",\""+unitTypeName+"\",\""+unitTypeValue+"\")'><font class='attendTypeName_info'>"+attendTypeName+"</font>&nbsp;&nbsp;("+unitTypeName+" )<br/> "+remainValue_mess+"</div>"
			}else{
			td_div = td_div  + 
			"<div id='div"+j+"' class='div_blockinfo' title = '"+leaveRemark+"' onclick='changeColor("+j+","+size+",\""+holidayPolicyId+"\",\""+attendTypeId+"\",\""+attendTypeName+"\",\""+unitTypeName+"\",\""+unitTypeValue+"\")'><font class='attendTypeName_info'>"+attendTypeName+"</font>&nbsp;&nbsp;("+unitTypeName+" )<br/> "+remainValue_mess+"</div>"
			}
		}
		// 更多按钮
		if(size>4){
			td_div = td_div  + 
			"<div id='div"+(size)+"' class='div_blockinfo' class='attendTypeName_info'><div style='font-size: 16px; padding-top:13px;padding-left:15px;'>其他假期&gt;&gt;</div> </div>"
		}
		$("#info_mess").html(td_div);
		
		for(var i = 0;i<size ; i++)
		{
			$('#div'+ i).bind('click',function()
			{
				//重新计算请假时长
				that.getRealLeaveLengthOfDay();
			});
		}
		
		//审核界面为编辑状态时
		if(that.isFromWF() && that.getOperateState() == 'EDIT' &&  $("#billState").val() != 0)
		{
			$("#info_mess .div_blockinfo").attr("onclick","").css("cursor","default");
		}
		
		// 注册更多按钮事件
		$('#div'+(size)).bind('click',function(){
			for(var i = 4;i<size;i++ ){
				// 显示大于4的假期类型
				$('#div'+i).attr('style','display:block');
			}
			//隐藏更多按钮
			$('#div'+(size)).attr('style','display:none');
		
			if(that.isFromWF() && that.getOperateState() == 'EDIT')
			{
				$("#info_mess .div_blockinfo").attr("onclick","").css("cursor","default");
			}
			//设置默认假期类型
			that.setAttendTypeDefault(attendColl,size,unitTypeName);
		});
		
		//首次进来 设置默认选中年休假 这个假期类型
		that.setAttendTypeDefault(attendColl,size,unitTypeName);//所有假期类型集合,集合大小
		//如果是编辑界面 点击到编辑的时候 还需要把假期类型反填写到页面上
		if (that.getOperateState() == 'EDIT') {
			//that.setEditPageAttendTypeColor(attendColl);
		}
	}
	
	 //编辑页面设置背景颜色
 	,setEditPageAttendTypeColor : function(attendColl){
 		var attendValue = $("#entries_policy_el").val();
 		//alert(attendColl);
 		for (index in attendColl) {
 			if (attendColl[index].id == attendValue) {
 				$("#div"+index).css({ "background-color":"#E4E4E4" }); //橙色(F39814) 背景灰(E4E4E4)  边框浅蓝
 				$("#div"+index).css({ "border":"1px solid #428BCA" }); //边框浅蓝(428BCA)
 			}
 		}
 	}
	/**
	 * 初始化设置假期默认年假的假期类型
	 * 再写个方法 返回所有的假期 类型 ----放在json里
	 * unitTypeName:假期类型名称
	 */
	,setAttendTypeDefault:function(attendColl,size,unitTypeName){
		var that = this ;
		if (that.getOperateState() == 'ADDNEW') {
			/*_unitType = 0;
			$("#div0").css({ "background-color":"#E4E4E4" });
			$("#div0").css({ "border":"1px solid #428BCA" });
			$("#entries_policy").val("年休假");
			$("#entries_policy_el").val("00000000-0000-0000-0000-000000000008649E3405");
			//年休假 00000000-0000-0000-0000-000000000008649E3405*/
			//0  11  00000000-0000-0000-0000-000000000008649E3405  年休假  天  0
			//changeColor(0,1,"00000000-0000-0000-0000-000000000008649E3405","年休假","天",0);
			//changeColorDefault(0,size,attendColl,"","3T54RtSQRIqAL6cffMh60P0tUpg=","年假","天",1);
			  for(i=0; i<size; i++){ 
		  		if(attendColl[i].holidayType.id == attendColl[0].holidayType.id){
		  		holidayPolicyId = attendColl[i].id;
				attendTypeId = attendColl[i].holidayType.id;
				attendTypeName = attendColl[i].name;
				var unitTypeName="";
				var unitTypeValue="";
				if (attendColl[i].unit != undefined) {
					unitTypeName = attendColl[i].unit.alias;
					unitTypeValue = attendColl[i].unit.value;
				}
				//alert(holidayPolicyId);
  			  changeColorDefault(0,size,attendColl,holidayPolicyId,attendTypeId,attendTypeName,unitTypeName,unitTypeValue);
  			 // alert(holidayPolicyId);
			  }
			}
			// 默认选中年假类型 放后面执行
			for(i=0; i<size; i++){ 
			  if(attendColl[i].holidayType.id == "3T54RtSQRIqAL6cffMh60P0tUpg="){
			  	holidayPolicyId = attendColl[i].id;
				attendTypeId = attendColl[i].holidayType.id;
				attendTypeName = attendColl[i].name;
				var unitTypeName="";
				var unitTypeValue="";
				if (attendColl[i].unit != undefined) {
					unitTypeName = attendColl[i].unit.alias;
					unitTypeValue = attendColl[i].unit.value;
				}
			  changeColorDefault(0,size,attendColl,holidayPolicyId,attendTypeId,attendTypeName,unitTypeName,unitTypeValue);
				}
			  }
		}else if(that.getOperateState() == 'EDIT'){
		var temp = 0;
			for(i=0; i<size; i++){
			 //alert(attendColl[i].name);
			 //alert($("#entries_policy").val());
			  if($("#entries_policy").val()==attendColl[i].name){
			  temp = 1;
		  	holidayPolicyId = attendColl[i].id;
			attendTypeId = attendColl[i].holidayType.id;
			attendTypeName = attendColl[i].name;
			var unitTypeName="";
			var unitTypeValue="";//保存的时候 做个校验???? 
			if (attendColl[i].unit != undefined) {
				unitTypeName = attendColl[i].unit.alias;
				unitTypeValue = attendColl[i].unit.value;
			}
			//alert(holidayPolicyId);
			//alert(attendTypeName);
		  changeColorDefault(0,size,attendColl,holidayPolicyId,attendTypeId,attendTypeName,unitTypeName,unitTypeValue);
		  }
			}
			if(temp==0){
				$("#entries_policy").attr('value','');
				shr.showWarning({message: "请重新选择假期类型！"}); 
			}
				}
	}
	,setLeaveLengthUnit:function(){
		var that = this ;
		//var aa = $(".span3 div:contains('entries_leaveLength') ")
		//var aa = $("div:has(input[id='entries_leaveLength'])")
		//alert( JSON.stringify(aa));
		var unit_info = "<div class='appendUnit'>天</div>";
		//$("#entries_leaveLength").width(160);//设置文本框的长度
		//$("#entries_leaveLength").parents(".ui-text-frame").width(176);//设置文本框外层的div的长度
		$("#entries_leaveLength").parents(".ui-text-frame").append(unit_info);
		//$("#entries_leaveLength").append(ss);
		/**
		 * 向文本框后面追加文字描述
		 */
		//$("#entries_leaveLength").parents(".ui-text-frame").parents(".span3").next().append("测试");
		//$("#entries_leaveLength").parents(".span3").append("测试描述1"); //加div 不会横排
		//alert(that.getOperateState());// ADDNEW ||  VIEW ||  EDIT
		if (that.getOperateState() == 'ADDNEW') {
			//默认填充单位天
		}else if (that.getOperateState() == 'EDIT') {
			$(".appendUnit").html($("#entries_remark").val());
		}else if (that.getOperateState() == 'VIEW') {
			var unit_view_info = "<span class='appendUnit_ViewPage'></span>";
			$("#entries_leaveLength").parents(".span3").append(unit_view_info);
			//$(".appendUnit_ViewPage").html($("#entries_remark").html());//查看页面用html()的取法
			$(".appendUnit_ViewPage").html($("#entries_remark").val());
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 初始化人员的请假次数信息和上次的请假时间 					<br/>
	 * 查看更多写在里边会有问题,因为这个时候dom还没有初始化完全 	<br/>
	 * 绑定click事件 无效果	
	 */
	,initPersonalLeaveMess : function(){
		var that = this ;
		var wenxin_tip = "温馨提示";
		$("#message_info").addClass('message_info');
		//改变方法 后续所有的全部走handler 形式访问数据
		//for test method 
		/*that.remoteCall({
			type:"post",
			method:"getInfoTest",
			param:{personId:"987654"},
			success:function(res){
				alert(res.statues + res.coll + res.personId);
			}
		});*/
		var personId = "";
		if($('#entries_person_el').val()!=null&&$('#entries_person_el').val()!=""){
		 personId = $('#entries_person_el').val();
		}else{
		 personId = $('#entries_person').val();
		}
		that.remoteCall({
			type:"post",
			method:"getLeaveTimesAndDateInfo",
			param:{personId:personId},
			success:function(res){
				var info = res;
				if(info == null){
					return ;
				}
				if(!info.hasOwnProperty("personName")){return;}
				//alert(JSON.stringify(info));
				var leaveDate = info.lastLeaveDate;
				
				var personShowName = "";
					if($('#breadcrumb').attr('id')!='breadcrumb'){
					if($('#entries_person').attr('title')==null
						||$('#entries_person').attr('title')==""){
						personShowName = $('#entries_person').text();
						}else{
						personShowName = $('#entries_person').attr('title');
						}
					}else{
					if ($("#bill_flag").val() != "commissioner") {
						personShowName = "您";
					}else if ($("#bill_flag").val() == "commissioner") {
						if($('#entries_person').attr('title')==null
						||$('#entries_person').attr('title')==""){
						personShowName = $('#entries_person').text();
						}else{
						personShowName = $('#entries_person').attr('title');
						}
					}
					}
					
				if (leaveDate != "") {
					leaveDate = leaveDate.substr(0,10);
					var leaveTimes = info.leaveTimes;
					
					
		
					if (leaveTimes == 0) {
						//onclick='showViewMoreDialog()'
						// info.data.personName
						var mes = "&nbsp;&nbsp; "+wenxin_tip+"： 本月"+personShowName+"已提交<font id='leaveDes'>请假申请"+leaveTimes+"次</font>，" +
							"上一次的请假时间为：<font id='leaveDes_Time'> "+leaveDate+"</font> " //+
							//" <div class='view_more_LeaveMessage'> <a id='showsssss'> 查看更多请假记录 </a> </div>   ";
					}else if (info.leaveAuditMidTimes > 0 || info.leaveNotAuditTimes > 0) {
						//info.data.personName
						var mes = "&nbsp;&nbsp; "+wenxin_tip+"： 本月"+personShowName+"已提交<font id='leaveDes'>请假申请"+leaveTimes+"次</font>，" +
							//"其中 未审核单据"+info.data.leaveNotAuditTimes+"条，审核中单据"+info.data.leaveAuditMidTimes+"条，" +
							//"审核完成单据"+info.data.leaveAuditCompleteTimes+"条，" +
							"上一次的请假时间为：<font id='leaveDes_Time'>"+leaveDate+"</font> ";
					}else{
						//info.data.personName
						var mes = "&nbsp;&nbsp; "+wenxin_tip+"： 本月"+personShowName+"已提交<font id='leaveDes'>请假申请"+leaveTimes+"次</font>，" +
							//"全部审核通过，" +
							"上一次的请假时间为：<font id='leaveDes_Time'>"+leaveDate+"</font> ";
					}

					$("#show_info").html("");
					$("#show_info").append(mes);
					$("#leaveDes").addClass('leaveDes');
					$("#leaveDes_Time").addClass('leaveDes_Time');
				}else{
					//info.data.personName
					var mes = "&nbsp;&nbsp; "+wenxin_tip+"： 本月"+personShowName+"已提交<font id='leaveDes'>请假申请"+info.leaveTimes+"次</font>" 
					$("#show_info").html("");
					$("#show_info").append(mes);
					$("#leaveDes").addClass('leaveDes');
				}
			}
		});
		//如果是查看界面,这里不显示"温馨提示"
		if (that.getOperateState() == 'VIEW') {
			$("#message_head").hide();
		}
		
		//判断页面来源  来自于审核页面 特殊处理
		if(that.isFromWF()){
			$("#message_head").show();
		}
		
		if(that.isFromWF()){
			
			var personId ;
			var holidayPolicyId;
			var billId ;
			if(that.getOperateState() == "VIEW")
			{
				personId = $('#entries_person').val();
				holidayPolicyId = $('#entries_policy').val();
				billId = $('#id').val();
			}else if(that.getOperateState() == "EDIT"){
				personId = $('#entries_person_el').val();
				holidayPolicyId = $('#entries_policy_el').val();
				billId = $('#id').val();
			}
			that.remoteCall({
				type:"post",
				async: false,
				method:"validateIsControlHolidayInfo",
				param:{holidayPolicyId:holidayPolicyId},
				success:function(res){
					var message  =  res.Validate;
					
					if(message == "TRUE")
					{ 
						var info = that.getRealAndRemainLimit(personId,holidayPolicyId,billId);
						$('#entries_policy').closest(".span3").eq(0).attr('style','width:400px');	
						$('#entries_policy').append('(总额度：'+info.realLimit+''+info.unit+',剩余额度：'+info.remainLimit+''+info.unit+',在途额度:'+info.freezeLimit+')');
					}
				}
			});
	
			
		}
	
	}
	
 
	/**
	 * HRBillStateEnum(与转正,调动,离职单据的一致) || BizStateEnum 这个是 EAS7.5版的请假单使用的审批状态值,后续不用这个了<br/>
	 * 后续的加班,出差,请假,补签卡都用HRBillStateEnum这个单据状态,以便可以统一修改<br/>
	 * view: <field name="billState"  label="单据状态" type="text"></field>	   <br/>
	 * 查看页面取值 var billState = $("#billState").html(); 
	 * view: <field name="billState"  label="单据状态" type="text"></field>	   <br/>
	 * 查看页面取值 var billState = $("#billState").val();
	 * 
	 * (HRBillStateEnum)		||  (BizStateEnum)
	 * 设置编辑按钮是否隐藏		||  对应EAS7.5 Version 审批状态字段值<br/>
	 * 0-save  	  未提交			||  -1  未提交					   	<br/>
	 * 1-submited 未审批			||   0  未审核					   	<br/>
	 * 2-auditing 审批中			||   1  审核中					   	<br/>
	 * 3-audited  审批通过		||   3  审核完成					   	<br/>
	 * 4-auditend 审批不通过		||   4  审核终止					   	<br/>
	 */
	,setButtonVisible:function(){
		
		shr.ats.AtsLeaveBillEdit.superClass.setButtonVisible.call(this);
		
		var billState = $("#billState").val();
		//alert(billState);
		if (billState) {
			if (billState==3 || "审批通过"==billState || billState ==4||"审批不通过"==billState || billState ==2||"审批中"==billState ) {
				$("#edit").hide();
				$("#submit").hide();
				$("#submitEffect").hide();
			} else if (1==billState || "未审批"== billState || 2 == billState || "审批中"==billState ) { //未审批或审批中
				if(!this.isFromWF()){
					$("#edit").hide();
					$("#submit").hide();
					$("#submitEffect").hide();
				}
			}
		}
		if (this.getOperateState().toUpperCase() == 'VIEW') { //查看状态下不允许提交
			$("#submit").hide();
			$("#submitEffect").hide();
			if(billState == 0)
			{
		         $("#submit").show();
		    }
		}
		//如果是工作流打回,界面上的"返回请假列表"不显示
		if (this.isFromWF()) {
			$("#returnToLeaveBillList").hide(); 
			$("#cancel").hide(); 
		}
		//增加员工编码,再流程审批的时候显示员工编码
		$("#entries-person-number").hide();
		if (this.isFromWF()) {
			$("#entries-person-number").show();
		}
		if (this.getOperateState().toUpperCase() == 'ADDNEW' || this.getOperateState().toUpperCase() == 'EDIT' ) {
			$("#returnToLeaveBillList").hide();
		}
	}
	
	//获取请假单下边的销假单信息
	,getCancelLeaveBillInfo:function(){
		var that = this;
		//var billId = $("#id").val();//请假单的id
		var billId = $('form').find('input[id^=id]').val(); // 为了在专员看板能访问id 
		that.remoteCall({
			type:"post",
			method:"getCancelLeaveBillInfoById",
			param:{billId:billId},
			success:function(res){
				that.showCancelLeaveBillInfo(res);
			}
		});
	}
	
	//展示销假单的信息
	,showCancelLeaveBillInfo:function(res){
		if (res.cancelLeaveBillCollSize > 0) {
			$("#cancelLeaveBillInfoDes").show();	
		}
		var infoColl = JSON.parse(res.cancelLeaveBillColl);
		var size = res.cancelLeaveBillCollSize;
		var html = '';
		for (var i = 0; i < size; i++) {
			
			/*
			html += '<tr class="ui-widget-content jqgrow ui-row-ltr view_div_table_tr" >';
		    html += '  <td colspan="4" align="left" style="padding-left:10px;"> '+'销假单' +(i+1)+' </td>';
		    html += '</tr>';
		    
		 	html += '<tr class="ui-widget-content jqgrow ui-row-ltr view_div_table_tr">';
		    html += '  <td class="field_label view_div_table_td"> '+'实际请假开始时间'+' </td>';
		    html += '  <td class="field_label view_div_table_td"> '+infoColl[i].realLeaBeginTime+' </td>';
		    html += '  <td class="field_label view_div_table_td"> '+'实际请假结束时间'+' </td>';
		    html += '  <td class="field_label view_div_table_td"> '+infoColl[i].realLeaEndTime+' </td>';
		    html += '</tr>';
		    html += '<tr class="ui-widget-content jqgrow ui-row-ltr view_div_table_tr">';
		    html += '  <td class="field_label view_div_table_td"> '+'实际请假长度'+' </td>';
		    html += '  <td class="field_label view_div_table_td"> '+infoColl[i].cancelLeaveLength+' '+infoColl[i].remark+' </td>';
		    html += '  <td class="field_label view_div_table_td"> '+'销假申请日期'+' </td>';   
		    html += '  <td class="field_label view_div_table_td"> '+infoColl[i].bill.applyDate.substring(0,10)    +' </td>';
		    html += '</tr>';
		    */
			html +='<h5 class="groupTitle">销假单信息</h5>';
			html += '<div class="row-fluid row-block " id="">';
			html += '<div data-ctrlrole="labelContainer">';
			html += '<div class="col-lg-4">';
			html += '<div class="field_label">实际开始时间</div>';
			html += '</div>';				
			html += '<div class="col-lg-6 field-ctrl">';
			html += '<span class="field_input">'+infoColl[i].realLeaBeginTime+'</span>';
		
			html += '</div>';
			html += '<div class="col-lg-2 field-desc"></div>';
			html += '</div>';
	
			html += '<div data-ctrlrole="labelContainer">';
			html += '<div class="col-lg-4">';
			html += '<div class="field_label">实际结束时间</div>';
			html += '</div>';				
			html += '<div class="col-lg-6 field-ctrl">';
			html += '<span class="field_input">'+infoColl[i].realLeaEndTime+'</span>';
			html += '</div>';
			html += '<div class="col-lg-2 field-desc"></div>';
			html += '</div>';
	  		html += '</div>'; 
		
			html += '<div class="row-fluid row-block " id="">';
			html += '<div data-ctrlrole="labelContainer">';
			html += '<div class="col-lg-4">';
			html += '<div class="field_label">实际请假长度</div>';
			html += '</div>';				
			html += '<div class="col-lg-6 field-ctrl">';
			html += '<span class="field_input">'+infoColl[i].cancelLeaveLength+' '+infoColl[i].remark+'</span>';
	
			html += '</div>';
			html += '<div class="col-lg-2 field-desc"></div>';
			html += '</div>';
	
			html += '<div data-ctrlrole="labelContainer">';
			html += '<div class="col-lg-4">';
			html += '<div class="field_label">销假申请日期</div>';
			html += '</div>';				
			html += '<div class="col-lg-6 field-ctrl">';
			html += '<span class="field_input">'+infoColl[i].bill.applyDate.substring(0,10)+'</span>';
			html += '</div>';
			html += '<div class="col-lg-2 field-desc"></div>';
			html += '</div>';
			
			html += '<div data-ctrlrole="labelContainer">';
			html += '<div class="col-lg-4">';
			html += '<div class="field_label">单据状态</div>';
			html += '</div>';				
			html += '<div class="col-lg-6 field-ctrl">';
			html += '<span class="field_input">'+infoColl[i].bill.billState.alias+'</span>';
			html += '</div>';
			html += '<div class="col-lg-2 field-desc"></div>';
			html += '</div>';
	  		html += '</div>'; 
		    
		    
		}
		//$("#wrap").css('overflow-y','auto');
		//$("#leaveBillNew_divForm").css('overflow','auto');
		//document.getElementById("leaveBillNew_divForm").style.height=document.getElementById("leaveBillNew_divForm").offsetHeight-100 + "px";
		$("#cancelLeaveBillInfoDes").html(html);
	}
	
	//请假时间改变  绑定 事件
	,leaveTimeChangeDealOfDay:function(){
		var that = this ;
		/*
		$("#entries_beginTime-time").change(function(){
			that.getRealLeaveLengthOfDay();
		});
		$("#entries_endTime-time").change(function(){
			that.getRealLeaveLengthOfDay();
		});
		$("#entries_beginTime-ap").change(function(){
			that.getRealLeaveLengthOfDay();
		});
		$("#entries_endTime-ap").change(function(){
			that.loadTimeAttendanceType();
			that.getRealLeaveLengthOfDay();
		});
		*/
		$("#entries_beginTime").change(function(){
			if(!(that.isFromWF() && that.getOperateState() == 'EDIT'))
			{
				that.getRemainLimit();
			}
			
			if($("input[id=entries_endTime-time]").val() || $("input[id=entries_endTime]").val()){
				that.getRealLeaveLengthOfDay();
			}
		});
		$("#entries_endTime").change(function(){
			if(!(that.isFromWF() && that.getOperateState() == 'EDIT'))
			{
				that.getRemainLimit();
			}
			
			
			if($("input[id=entries_beginTime-time]").val() || $("input[id=entries_beginTime]").val()){
				that.getRealLeaveLengthOfDay();
			}
		});
		
		$("#entries_beginTime-ap").change(function(){
			if($("#entries_beginTime").val() || $("#entries_endTime").val())
			{
				that.getRealLeaveLengthOfDay();
			}
			
		});
		$("#entries_endTime-ap").change(function(){
			if($("#entries_beginTime").val() || $("#entries_endTime").val())
			{
				that.getRealLeaveLengthOfDay();
			}
			
		});
		
	}/*
		当改变时间时动态改变剩余额度
	*/
	,getRemainLimit:function(){
		var that = this;
		var personId =  $("#entries_person_el").val();
		var beginTime = "",endTime = "";
		beginTime = $("#entries_beginTime-time").val();
		if(!beginTime){
			beginTime = $("#entries_beginTime").val();
		}
		
		endTime = $("#entries_endTime-time").val();
		if(!endTime){
			endTime = $("#entries_endTime").val();
		}
		
		if(!beginTime){
			beginTime = endTime;
		}
		else if(!endTime){
			endTime = beginTime;
		}
		else{
		}
		that.remoteCall({
			type:"post",
			method:"getTimeAttendanceType",
			param:{personId:personId,startTime: beginTime, endTime: endTime},
			async: true,
			success:function(res){
				if(info.errorString){
					shr.showWarning({message: info.errorString});
				}else{
					that.changeTableAndDiv(res);
				}
		}
		});
	}
	,changeTableAndDiv: function(res){
	    if(res){
			vacationRemain = res.vacationRemain;
			freeRemain = res.freeRemain;
			//for (var prop in vacationRemain) {
			//	if(yearHolidayTypeId == prop) //年假
			//	{
					for(var i=0;i <$('div[id^="div"]').size();i++)
					{
						var divObject = $($('div[id^="div"]').eq(i));
						var fontObject = divObject.find(".remain_info");
						var indexOfProp = divObject.attr("onclick");
						var  hasProp = false;
						if(indexOfProp){
							for (var prop in vacationRemain)
							{
								var indexNum = indexOfProp.indexOf(prop);// 有的话说明返回有值，没有的话说明没有值
								if(indexNum > -1){
								   	hasProp = true ;
									var oldString = fontObject.text();
									if(vacationRemain[prop] == ''){
										fontObject.css('display','none')
									}
									else{
										fontObject.css('display','block');
										if(fontObject.length != 0){
										    var newString = "剩余["+ vacationRemain[prop]  +"]"+",在途["+freeRemain[prop]+"]";
											fontObject.text(newString);    
										}else{
										    var newString = "剩余["+ vacationRemain[prop]  +"]"+",在途["+freeRemain[prop]+"]";
										    divObject.append("<font class='remain_info'>"+newString +"</font>");
										}
										
										//fontObject.css('display','block')
										//var unit = oldString.substr(oldString.indexOf("]"),",") 
										//var newString = "剩余["+ vacationRemain[prop]  +"]"+",在途["+freeRemain[prop]+"]";
										//	fontObject.text(newString);
									}
									break;
								}
							}
						}
						if(!hasProp){
							if(fontObject.length != 0){
							    var newString = "";
								fontObject.text(newString);    
							}else{
							    var newString = "";
							    divObject.append("<font class='remain_info'>"+newString +"</font>");
							}
						}	
					}
			//	}
				
  		//	}
		}
		
	}
	//根据时间填写的 请假开始时间 和 请假结束时间 计算请假的时间长度
	/*,calculateLeaveLength:function(){
		var that = this ;
		$("#entries_endTime").change(function(){
			//that.calculateLeaveLengthSub();
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_beginTime").change(function(){
			//that.calculateLeaveLengthSub();
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_beginTime-time").change(function(){
			//that.calculateLeaveLengthSub();
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_endTime-time").change(function(){
			//that.calculateLeaveLengthSub();
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_beginTime-ap").change(function(){
			//that.calculateLeaveLengthSub();
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_endTime-ap").change(function(){
			//that.calculateLeaveLengthSub();
			that.getRestDayAndLegalHoliday();
		});
	}*/
	,calculateLeaveLengthSub:function(){
		var begintime = $("#entries_beginTime").val();
		var endtime = $("#entries_endTime").val();
		//if ( begintime!=""&&begintime!=null && endtime!=""&& endtime!=null ) {
		if($("#entries_beginTime-time").val()!=""&&$("#entries_beginTime-time").val()!=null 
				&& $("#entries_endTime-time").val()!=""&& $("#entries_endTime-time").val()!=null 
				&& $("#entries_beginTime-ap").val()!=""&& $("#entries_beginTime-ap").val()!=null 
				&& $("#entries_endTime-ap").val()!=""&& $("#entries_endTime-ap").val()!=null){ 
			var regEx = new RegExp("\\-","gi");
			begintime = begintime.replace(regEx,"/");
		 	endtime = 	endtime.replace(regEx,"/");
		 	var beginTimeOfDate = new Date(begintime); 
		 	var endTimeOfDate = new Date(endtime);
		 	var longTime = endTimeOfDate.getTime() - beginTimeOfDate.getTime();
		 	//以天为单位
		 	var day = parseFloat(longTime)/1000.0/60/60/24;
		 	day = day.toFixed(2);
		 	$("#entries_leaveLength").val(day);
		 	$("#entries_realLeaveLength").val($("#entries_leaveLength").val());
		}
	}
	// 通过 公休日 法定节假日 获得 除去非工作日的实际时长  （天）
	,getRealLeaveLengthOfDay:function(){
	    var that = this;
		var personId = $("#entries_person_el").val();
		//龙光需求 请假时间为上午下午 时  请假开始结束时间取法
		//var beginTime = $("#entries_beginTime-time").val()+" "+$("#entries_beginTime-ap").val();
		//var endTime = $("#entries_endTime-time").val()+" "+$("#entries_endTime-ap").val();
		//var beginTime = $("#entries_beginTime").val();
		//var endTime = $("#entries_endTime").val();
		
		var beginTime;
		var endTime;
		var beginDate;
		var endDate;
		var billId =  $("#id").val();
		var policyId = $("#entries_policy_el").val();//假期制度的ID 
		var holidayTypeId = $("#entries_policy_holidayType_el").val();//假期分类ID
		var leaveLength = parseFloat( $("#entries_leaveLength").val());//请假长度
		if(_isHalfDayOff){
		 beginDate = $("#entries_beginTime-time").val();
		 endDate = $("#entries_endTime-time").val();
		 if($("#entries_beginTime-ap").val()!=""&& $("#entries_beginTime-ap").val()!=null 
				&& $("#entries_endTime-ap").val()!=""&& $("#entries_endTime-ap").val()!=null){
		 	
		 	 if($("#entries_beginTime-ap").val().substring(3) == _defaultAmBeginTime){
		 	 	beginTime = "上午"
		 	 }else{
		 	 	beginTime = "下午"
		 	 };
		 	 if($("#entries_endTime-ap").val().substring(3) == _defaultAmEndTime){
		 	 	endTime  =  "上午"
		 	 }else{
		 	   endTime   = "下午"
		 	 };
		    //  endTime = $("#entries_endTime-ap").val();
					
		 }
		
		//if ( beginTime!=""&&beginTime!=null && endTime!=""&& endTime!=null ) {
		if($("#entries_beginTime-time").val()!=""&&$("#entries_beginTime-time").val()!=null 
				&& $("#entries_endTime-time").val()!=""&& $("#entries_endTime-time").val()!=null 
				&& $("#entries_beginTime-ap").val()!=""&& $("#entries_beginTime-ap").val()!=null 
				&& $("#entries_endTime-ap").val()!=""&& $("#entries_endTime-ap").val()!=null){ 
			
			//如果选择单位等于 次 默认为 1
			if(that._unitType == 4 || $("#entries_remark").val()=="次" ){
		 	$("#entries_leaveLength").val(1);
		 	$("#entries_realLeaveLength").val($("#entries_leaveLength").val());
		 	return;
		 	}
		 	
			var regEx = new RegExp("\\/","gi");
			beginTime = beginTime.replace(regEx,"-");
		 	endTime = 	endTime.replace(regEx,"-");
		 	that.remoteCall({
			type:"post",
			async: false,
			method:"getRealLeaveLengthOfDay",
			param:{personId:personId,beginDate:beginDate,endDate:endDate,beginTime:beginTime,endTime:endTime,billId:billId,holidayTypeId:holidayTypeId},
			success:function(res){
				info =  res;
			var day = parseFloat(info.leaveBillDays);
		 	day = day.toFixed(2);
		 	$("#entries_leaveLength").val(day);
		 	$("#entries_realLeaveLength").val($("#entries_leaveLength").val());
			}
		});
		
		}
		
		}else{
		if($("#entries_beginTime").val()!=""&&$("#entries_beginTime").val()!=null 
				&& $("#entries_endTime").val()!=""&& $("#entries_endTime").val()!=null){ 
			//beginTime = $('#entries_beginTime').val();
			//endTime = $('#entries_endTime').val();
			beginTime=$('#entries_beginTime').shrDateTimePicker('getValue');
			endTime=$('#entries_endTime').shrDateTimePicker('getValue');
			beginTime = beginTime.replace("\\/","-");
		 	endTime = 	endTime.replace("\\/","-");
		 	
		 	that.remoteCall({
			type:"post",
			async: false,
			method:"getRealLeaveLengthInfo",
			param:{personId:personId,beginTime:beginTime,endTime:endTime,billId:billId,holidayTypeId:holidayTypeId},
			success:function(res){
				info =  res;
				if(info.errorString){
					shr.showError({message: info.errorString});
				}else{
					var day = parseFloat(info.leaveBillDays);
				 	day = day.toFixed(2);
				 	$("#entries_leaveLength").val(day);
				 	$("#entries_realLeaveLength").val($("#entries_leaveLength").val());
				}
			}
		});
		}
		
		}
		
		
	}
	,getRestDayAndLegalHoliday:function(){
	    var that = this;
//alert(1);
		//获取请假的时间包含的公休日和法定节假日 的时间
		var personId = $("#entries_person_el").val();
		//龙光需求 请假时间为上午下午 时  请假开始结束时间取法
		var beginTime = $("#entries_beginTime-time").val()+" "+$("#entries_beginTime-ap_el").val();
		var endTime = $("#entries_endTime-time").val()+" "+$("#entries_endTime-ap_el").val();
		//var beginTime = $("#entries_beginTime").val();
		//var endTime = $("#entries_endTime").val();
		var billId =  $("#id").val();
		var policyId = $("#entries_policy_el").val();//假期制度的ID 
		var holidayTypeId = $("#entries_policy_holidayType_el").val();//假期分类ID
		var leaveLength = parseFloat( $("#entries_leaveLength").val());//请假长度
		//if ( beginTime!=""&&beginTime!=null && endTime!=""&& endTime!=null ) {
		if($("#entries_beginTime-time").val()!=""&&$("#entries_beginTime-time").val()!=null 
				&& $("#entries_endTime-time").val()!=""&& $("#entries_endTime-time").val()!=null 
				&& $("#entries_beginTime-ap").val()!=""&& $("#entries_beginTime-ap").val()!=null 
				&& $("#entries_endTime-ap").val()!=""&& $("#entries_endTime-ap").val()!=null){ 
			var regEx = new RegExp("\\/","gi");
			beginTime = beginTime.replace(regEx,"-");
		 	endTime = 	endTime.replace(regEx,"-");
		 	that.remoteCall({
			type:"post",
			async: false,
			method:"getRestDayAndLegalHoliday",
			param:{personId:personId,beginTime:beginTime,endTime:endTime,billId:billId,holidayTypeId:holidayTypeId},
			success:function(res){
				info =  res;
			var day = parseFloat(info.leaveBillLength)/1000.0/60/60/24;
		 	day = day.toFixed(2);
		 	$("#entries_leaveLength").val(day);
		 	$("#entries_realLeaveLength").val($("#entries_leaveLength").val());
			}
		});
		}
		
		
	}
	,getSplitString:function(val){
		
		var attr = val.split("-");
		var monthStr = attr[1];
		var dayttr = attr[2].split(" ");
		var dayStr = dayttr[0];
		var hour = dayttr[1].split(":")[0];
		
		if(parseInt(monthStr) < 10){
			monthStr = "0"+monthStr;
		}
		if(parseInt(dayStr) < 10){
			dayStr = "0"+dayStr;
		}
		if(parseInt(hour) < 10){
			hour = "0"+hour;
		}
		return attr[0]+"-"+monthStr+"-"+dayStr+" "+hour+":00:00";
	}
	/**
	 * 验证,校验方法 
	 * 保存前各种验证方法
	 * 保存请假单之前需要做比较多的控制
	 * 需要按照假期制度的参数来控制请假单的提交逻辑
	 */
	,verify:function(){
		var that = this;
		if(that.getOperateState() == 'VIEW')
		{
			var beginTime = that.getSplitString($("#entries_beginTime").text());
			var endTime = that.getSplitString($("#entries_endTime").text());
			var personId  = $("#entries_person").val();
			var leaveLength = $("#entries_leaveLength").text().replace(/[^0-9.]/ig,"");
			var holidayTypeId = $("#entries_policy_holidayType").val();
			var regEx = new RegExp("\\/","gi");
			var billId = $("#id").val();
			var operateState = that.getOperateState();
			
			var pageUipk = that.uipk;
			beginTime = beginTime.replace(regEx,"-");
		 	endTime = 	endTime.replace(regEx,"-");
			var info_res ;

			that.remoteCall({
				type:"post",
				async: false,
				method:"getLeaveBillInfoByPersonIdAndLeaveTime",
				param:{personId:personId,beginTime:beginTime,endTime:endTime,billId:billId,holidayTypeId:holidayTypeId},
				success:function(res){
					info_res =  res;
				}
			});
		    if (info_res.addFlag > 0) {
				  shr.showError({message: "在编号为["+info_res.billNo+"]的请假单中,存在时间重叠的记录：<br/>["+info_res.personName+",开始时间："+info_res.realBeginDate+" 结束时间："+info_res.realendDate+" ]"});
				  return false;
			}
		    
			info_res = that.validateLeavePriority(personId,beginTime,endTime,holidayTypeId);
			if(info_res!=null&&info_res.errorMsg!="")
			{
				shr.showError({message: info_res.errorMsg});
				return false;
			}

			that.remoteCall({
				type:"post",
				async: false,
				method:"validateLeaveBillCycle",
				param:{personId:personId,beginTime:beginTime,endTime:endTime,holidayTypeId:holidayTypeId,leaveLength:leaveLength,billId:billId,pageUipk:pageUipk,operateState:operateState},
				success:function(res){
					info_res =  res;
				}
			});
			
			if(info_res!=null&&info_res.errorString!="")
			{
				shr.showWarning({message: info_res.errorString });
				return false;
			}

			var info_res = that.validateHolidayPolicyControl(personId,holidayTypeId,beginTime,endTime,leaveLength);
			if(info_res!=null&&info_res.errorString!="")
			{
				shr.showWarning({message: info_res.errorString });
				return false;
			}
			return true;
		}
		
		var existHolidayType = $("#entries_policy").val();
		//判断是否有选中的假期类型
		if(existHolidayType==""||existHolidayType==null){
		shr.showInfo({message: "请选择请假类型，保存失败！"}); 
			return false;
		}
		//判断某人 再开始时间和结束时间之内有没有重复的请假单
		var personId = $("#entries_person_el").val();
		// $("#entries_person").val();
		var beginTime;
		var endTime;
		if(_isHalfDayOff){
		//龙光需求
		 beginTime = $("#entries_beginTime-time").val()+" "+$("#entries_beginTime-ap_el").val();
		 endTime = $("#entries_endTime-time").val()+" "+$("#entries_endTime-ap_el").val();
		}else{
		beginTime = $('#entries_beginTime').val();
		endTime = $('#entries_endTime').val();
		}
		
		//var regEx = new RegExp("\\-","gi");
		beginTime = beginTime.replace("\\-","/");
	 	endTime = 	endTime.replace("\\-","/");
		 
		var billId =  $("#id").val();

		var policyId = $("#entries_policy_el").val();//假期制度的ID 
		var holidayTypeId = $("#entries_policy_holidayType_el").val();//假期分类ID
		
		var leaveLength = parseFloat( $("#entries_leaveLength").val());//请假长度
	
	 	var beginTimeOfDate = new Date(beginTime); 
	 	var endTimeOfDate = new Date(endTime);
		var longTime = endTimeOfDate.getTime() - beginTimeOfDate.getTime();
		if (longTime <= 0) {
			shr.showWarning({message: "请假开始时间不能大于等于请假结束时间"}); 
			return false;
		}
		if (leaveLength <= 0) {
			shr.showWarning({message: "请假时间长度不能小于等于0"}); 
			return false;
		}
		if (longTime > 365 * 24 * 60 * 60 * 1000 ) {
			shr.showWarning({message: "请假时间差不能大于12个月"}); 
			return false;
		}
		//启动半天假
		if(_isHalfDayOff){
		// 时间 通过排班 工作日历 计算后的实际时间
		 var realLengthInfo ;
		 var beginDateTemp = $("#entries_beginTime-time").val();
		 var endDateTemp = $("#entries_endTime-time").val();
		 var beginTimeTemp = $("#entries_beginTime-ap_el").val();
		 var endTimeTemp = $("#entries_endTime-ap_el").val();
		  if($("#entries_beginTime-ap").val().substring(3) == _defaultAmBeginTime){
		 	 	beginTimeTemp = "上午"
		   }else{
		 	 	beginTimeTemp = "下午"
		   };
		   if($("#entries_endTime-ap").val().substring(3) == _defaultAmEndTime){
		 	 	endTimeTemp  =  "上午"
		   }else{
		 	   endTimeTemp   = "下午"
		   };
		 that.remoteCall({
			type:"post",
			async: false,
			method:"getRealLeaveLengthOfDay",
			param:{personId:personId,beginDate:beginDateTemp,endDate:endDateTemp,beginTime:beginTimeTemp,endTime:endTimeTemp,billId:billId,holidayTypeId:holidayTypeId},
			success:function(res){
				realLengthInfo =  res;
			}
		 });
		 if(realLengthInfo.leaveBillDays < leaveLength ){
			
			if(that._unitType == 1 || $("#entries_remark").val()=="天")
			{
				shr.showError({message: "请假时间不能超过"+realLengthInfo.leaveBillDays+"天"}); 
				return false;
			}else if(that._unitType == 0 || $("#entries_remark").val()=="小时"){
			
				shr.showError({message: "请假时间不能超过"+realLengthInfo.leaveBillDays+"小时"}); 
				return false;
			}
		}
		
		}else{
		// 时间 通过排班 工作日历 计算后的实际时间
			var realLengthInfo ;
	
			
			that.remoteCall({
				type:"post",
				async: false,
				method:"getRealLeaveLengthInfo",
				param:{personId:personId,beginTime:beginTime,endTime:endTime,holidayTypeId:holidayTypeId},
				success:function(res){
					info = res ;
					if(info.errorString){
						shr.showError({message: info.errorString});
					}else{
						realLengthInfo =  info;
					}
				}
			});
			if (realLengthInfo.leaveBillDays + realLengthInfo.segRest < leaveLength && (that._unitType == 1 || $("#entries_remark").val()=="天") ) {
				shr.showError({message: "请假时间不能超过"+(realLengthInfo.wholeLen).toFixed(2)+"天"}); 
				return false;
			}else if(realLengthInfo.leaveBillDays + realLengthInfo.segRest < leaveLength && (that._unitType == 0 || $("#entries_remark").val()=="小时") ){
				shr.showError({message: "请假时间不能超过"+(realLengthInfo.wholeLen).toFixed(2)+"小时"}); 
				return false;
			} 
			
		}
		
		
		
		//用当前人员id查询    验证请假时间是否在请假额度周期内
		//if (holidayTypeId == yearVacationId){
		//param:{personId:personId,beginTime:beginTime,endTime:endTime,holidayTypeId:holidayTypeId,leaveLength:leaveLength,billId:billId,validateAction:validateAction},
		    billId =  $("#id").val();
			var info_res = that.validateLeaveBillCycle(personId,beginTime,endTime,holidayTypeId,leaveLength,billId);
			if(info_res!=null&&info_res.errorString!=""){
				shr.showError({message: info_res.errorString });
			return false;
		}
		//	}
	 	
		var info_res = that.validateHolidayPolicyControl(personId,holidayTypeId,beginTime,endTime,leaveLength);
			if(info_res!=null&&info_res.errorString!=""){
				shr.showError({message: info_res.errorString });
				return false;
			}
		
		//用当前人员ID 查询假期额度返回
//			if (holidayTypeId == yearVacationId){
//			var info_res = that.getYearVacationRemainInfo(personId);
//			var remain_value = parseFloat(info_res.remainLimit);
//			if (remain_value <= 0) {
//				shr.showInfo({message: "该人员年假额度为"+remain_value+",不允许请假"}); 
//				return false;
//			}else if (leaveLength > remain_value) {
//				shr.showInfo({message: "请假时间长度不能大于该人员的年假剩余额度"});
//				return false;
//			}
//		}
		var info ;

		
		
		that.remoteCall({
			type:"post",
			async: false,
			method:"getLeaveBillInfoByPersonIdAndLeaveTime",
			param:{personId:personId,beginTime:beginTime,endTime:endTime,billId:billId,holidayTypeId:holidayTypeId},
			success:function(res){
				info =  res;
			}
		});
		if (that.getOperateState() == 'ADDNEW') {
		//校验 请假单编号
			/*	 框架统一会校验number字段重复
			var leaveBillNumber = $('#number').val();
			var that = this ;
				that.remoteCall({
					type:"post",
					async: false, 
					method:"validateLeaveBillNumber",
					param:{leaveBillNumber:leaveBillNumber},
					success:function(res){
						 infoLeaveBillNumber = res;
					}
				});
			//校验请假单编号是否重复	
			var infoLeaveBillNumber;
		    if(infoLeaveBillNumber.leaveBillExist=="true"){
		    shr.showError({message: "已经存在编码为"+infoLeaveBillNumber.leaveBillNumber+"的对象。"});
		    return false;
		    }
		    */
		}
	    
	    if (info.addFlag > 0) {
		  shr.showError({message: "在编号为["+info.billNo+"]的请假单中,存在时间重叠的记录：<br/>["+info.personName+",开始时间："+info.realBeginDate+" 结束时间："+info.realendDate+" ]"});
		  return false;
		}
//		else if (info.limitFlag == 1) {
//		  shr.showInfo({message: "请假的开始时间或者结束时间不在年假额度周期范围内,不能请假。"});
//	  		return false;
//		}
		//校验优先级
		info_res = that.validateLeavePriority(personId,beginTime,endTime,holidayTypeId);
		if(info_res!=null&&info_res.errorMsg!="")
		{
			shr.showError({message: info_res.errorMsg});
			return false;
		}else{
			return true;
		}
		
//		var regEx = new RegExp("\\-","gi"); //i不区分大小写 g匹配所有
//	 	var startTime = $("#entries_startTime").val();
//		var endTime = $("#entries_endTime").val();
//		startTime = startTime.replace(regEx,"/");
//		endTime = endTime.replace(regEx,"/");
//		var startTimeOfDate = new Date(startTime); 
//	 	var endTimeOfDate = new Date(endTime);
//	 	var longTime = endTimeOfDate.getTime() - startTimeOfDate.getTime();
//	 	
//	 	var realstartTime = $("#entries_realStartTime").val();
//		var realendTime = $("#entries_realEndTime").val();
//		var realstartTimeOfDate = new Date( realstartTime.replace(regEx,"/") ); 
//	 	var realendTimeOfDate = new Date( realendTime.replace(regEx,"/") );
//	 	var longTime_real = realendTimeOfDate.getTime() - realstartTimeOfDate.getTime();
//	 	var longTime = -1;
//	 	if (longTime <= 0) {
//	 		shr.showInfo({message: "加班开始时间必须小于加班结束时间。"});
//			return false;
//	 	}else if (longTime_real <= 0) {
//	 		shr.showInfo({message: "实际加班开始时间必须小于实际加班结束时间。"});
//			return false;
//	 	}else{
//			return true;
//		}
		
	}
	
	
	,validateHolidayPolicyControl:function(personId,holidayTypeId,beginTime,endTime,leaveLength){
		var that = this;
		var info ;
		that.remoteCall({
			type:"post",
			async: false,
			method:"validateHolidayPolicyControl",
			param:{personId:personId,holidayTypeId:holidayTypeId,beginTime:beginTime,endTime:endTime,leaveLength:leaveLength,billId:$("#id").val()},
			success:function(res){
				info =  res;
			}
		});
		return info;
	},
	/**
	 * 校验优先级
	 */
	validateLeavePriority: function(personId,beginTime,endTime,holidayTypeId){
	   var that = this;
	   var info ;
	   that.remoteCall({
				type:"post",
				async: false,
				method:"valiadLeavePriority",
				param:{personId:personId,beginTime:beginTime,endTime:endTime,holidayTypeId:holidayTypeId},
				success:function(res){
					info =  res;
				}
		});
		return info;
	}
	//验证请假周期
	,validateLeaveBillCycle:function(personId,beginTime,endTime,holidayTypeId,leaveLength,billId){
		var that = this;
		var beginTime;
		var endTime
		var operateState = that.getOperateState();
		var pageUipk = that.uipk;
		if(_isHalfDayOff){
		//龙光需求 请假时间为上午下午 时  请假开始结束时间取法
		 beginTime = $("#entries_beginTime-time").val()+" "+$("#entries_beginTime-ap_el").val();
		 endTime = $("#entries_endTime-time").val()+" "+$("#entries_endTime-ap_el").val();
		}else{
		beginTime = $('#entries_beginTime').val();
		endTime = $('#entries_endTime').val();
		}
		
		var regEx = new RegExp("\\/","gi");
		beginTime = beginTime.replace(regEx,"-");
	 	endTime = 	endTime.replace(regEx,"-");
		var info ;
		that.remoteCall({
			type:"post",
			async: false,
			method:"validateLeaveBillCycle",
			param:{personId:personId,beginTime:beginTime,endTime:endTime,holidayTypeId:holidayTypeId,leaveLength:leaveLength,billId:billId,pageUipk:pageUipk,operateState:operateState},
			//param:{personId:personId,beginTime:beginTime,endTime:endTime,holidayTypeId:holidayTypeId,leaveLength:leaveLength},
			success:function(res){
				info =  res;
			}
		});
		return info;
	}
	
	//通过人员ID 获取年假剩余额度信息
	,getYearVacationRemainInfo:function(personId){
		var that = this;
		var info ;
		that.remoteCall({
			type:"post",
			async: false,
			method:"getYearVacationRemain",
			param:{personId:personId},
			success:function(res){
				info =  res;
			}
		});
		return info;
	}
	
	//通过人员ID 获取年假  是否可以超额请假 和 超期额度下期扣减
	,getIsOverAndIsOverAutoSubByPersonId:function(personId){
		var that = this;
		var info ;
		that.remoteCall({
			type:"post",
			async: false,
			method:"getIsOverAndIsOverAutoSubByPersonId",
			param:{personId:personId},
			success:function(res){
				info =  res;
			}
		});
		return info;
	}
	
	//通过人员ID 获取年假  是否可以超额请假 和 超期额度下期扣减
	,getRealAndRemainLimit:function(personId,holidayPolicyId,billId){
		var that = this;
		var info ;
		that.remoteCall({
			type:"post",
			async: false,
			method:"getRealAndRemainLimit",
			param:{personId:personId,holidayPolicyId:holidayPolicyId,billId:billId},
			success:function(res){
				info =  res;
			}
		});
		return info;
	}
	
	,processHiddenFiled:function(){
		var that = this ;
		/*$("#entries_beginTime").change(function(){
			$("#entries_realBeginTime").val( $("#entries_beginTime").val() );
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_endTime").change(function(){
			$("#entries_realEndTime").val( $("#entries_endTime").val() ); 
			that.getRestDayAndLegalHoliday();
		});
		$("#entries_leaveLength").change(function(){
			$("#entries_realLeaveLength").val( $("#entries_leaveLength").val() ); 
		});*/
		//单位字段的处理再  setLeaveLengthType 
	}
	
	/**
	 * 龙光需求
	 * @return {Boolean}
	 */
	,calculateLeaveLength_Longon : function(){
		var that = this ;
		//增加时间控件的change事件 
		/**
		 * 0-天 1-小时 2- 分钟 3-次 hr-time Version
		 * 1-天 2-小时 3- 分钟 4-次 ats Version
		 */
		//$("#entries_endTime").change(function(){
			//alert( that._unitType);
			that._unitType = _unitType;
			var begintime = $("#entries_beginTime-time").val();
			var endtime =   $("#entries_endTime-time").val();
			
			var begintime_ap = $("#entries_beginTime-ap").val().substring(3);//开始时间的 上午和下午
			var endtime_ap = $("#entries_endTime-ap").val().substring(3);	//结束时间的上午和下午
			
			if(begintime_ap == "上午" && endtime_ap=="上午"){
			   begintime_ap = _defaultAmBeginTime;
			   endtime_ap   = _defaultAmEndTime;
			}else if (begintime_ap == "上午" && endtime_ap=="下午") {
			   begintime_ap = _defaultAmBeginTime;
			   endtime_ap   = _defaultPmEndTime;
			}else if (begintime_ap == "下午" && endtime_ap=="下午") {
			   begintime_ap = _defaultPmBeginTime;
			   endtime_ap   = _defaultPmEndTime;
			}else if (begintime_ap == "下午" && endtime_ap=="上午") {//并且这种情况 结束日期 必须大于开始日期
			   begintime_ap = _defaultPmBeginTime;
			   endtime_ap   = _defaultAmEndTime;
			}
			
			begintime = begintime + " " + begintime_ap;
			endtime = endtime + " " + endtime_ap;
			//alert(begintime);
			
			//绑定监听事件  请假开始结束时间改变则得到真实请假时长
	 	 	//that.leaveTimeChangeDealOfDay();
	 	 
			//if ( begintime!=""&&begintime!=null && endtime!=""&& endtime!=null ) {
			if ( $("#entries_beginTime-time").val()!=""&&$("#entries_beginTime-time").val()!=null 
				&& $("#entries_endTime-time").val()!=""&& $("#entries_endTime-time").val()!=null 
				&& $("#entries_beginTime-ap").val()!=""&& $("#entries_beginTime-ap").val()!=null 
				&& $("#entries_endTime-ap").val()!=""&& $("#entries_endTime-ap").val()!=null ) {
				var regEx = new RegExp("\\-","gi");
				begintime = begintime.replace(regEx,"/");
			 	endtime = 	endtime.replace(regEx,"/");
			 	var beginTimeOfDate = new Date(begintime); 
			 	var endTimeOfDate = new Date(endtime);
			 	
			 	var longTime = endTimeOfDate.getTime() - beginTimeOfDate.getTime();
			 		if (that._unitType == 2) { //单位为小时
			 			if (longTime < 0) {
					 	 	shr.showInfo({message: "请假开始日期不能大于请假结束日期"});
							return false;
					 	 }else{
				 			 t1 = parseFloat(longTime)/1000.0/60/60;
				 	 		 t1 = t1.toFixed(2);
				 	 		 $("#entries_leaveLength").val(t1);
					 	 }
			 		}else if (that._unitType == 1){//单位为天
			 		  	 //t1 = parseFloat(longTime)/1000.0/60/60/8;
					 	 //t1 = t1.toFixed(2);
			 			var begintime_day = $("#entries_beginTime-time").val();
						var endtime_day =   $("#entries_endTime-time").val();
						var begintime_ap_day = $("#entries_beginTime-ap").val().substring(3);//开始时间的 上午和下午
						var endtime_ap_day = $("#entries_endTime-ap").val().substring(3);	//结束时间的上午和下午
						
						var regEx2 = new RegExp("\\-","gi");
						begintime_day = begintime_day.replace(regEx,"/");
					 	endtime_day   = endtime_day.replace(regEx,"/");
					 	var beginTimeDayOfDate = new Date(begintime_day); 
					 	var endTimeDayOfDate = new Date(endtime_day);
						var longTime2 = endTimeDayOfDate.getTime() - beginTimeDayOfDate.getTime();
					 	 if (begintime_day == endtime_day) {
					 	 	if (begintime_ap_day == "上午" && endtime_ap_day=="上午"  ) {
					 	 		$("#entries_leaveLength").val("0.5");	
					 	 	}else if (begintime_ap_day == "上午" && endtime_ap_day=="下午") {
					 	 		$("#entries_leaveLength").val("1");
					 	 	}else if (begintime_ap_day == "下午" && endtime_ap_day=="下午") {
					 	 		$("#entries_leaveLength").val("0.5");
					 	 	}else if (begintime_ap_day == "下午" && endtime_ap_day=="上午") {
					 	 		$("#entries_leaveLength").val("0");
					 	 	}
					 	 }else if(longTime2 > 0){
					 	   //算上天
					 	   var day = parseFloat(longTime2)/1000.0/60/60/24;
					 	   if (begintime_ap_day == "上午" && endtime_ap_day=="上午"  ) {
					 	   		var resDay = parseFloat(day)+0.5;
					 	 		$("#entries_leaveLength").val(resDay);	
					 	 	}else if (begintime_ap_day == "上午" && endtime_ap_day=="下午") {
					 	 		var resDay = parseFloat(day)+1;
					 	 		$("#entries_leaveLength").val(resDay);
					 	 	}else if (begintime_ap_day == "下午" && endtime_ap_day=="下午") {
					 	 		var resDay = parseFloat(day)+0.5;
					 	 		$("#entries_leaveLength").val(resDay);
					 	 	}else if (begintime_ap_day == "下午" && endtime_ap_day=="上午") {
					 	 		var resDay = parseFloat(day)+0;
					 	 		//$("#entries_leaveLength").val(resDay);
					 	 	}
					 	 }else{
					 	 	shr.showInfo({message: "请假开始日期不能大于请假结束日期"});
							return false;
					 	 }
					 	
			 		}else if (that._unitType == 3){//单位为分钟
			 			 if (longTime < 0) {
					 	 	shr.showInfo({message: "请假开始日期不能大于请假结束日期"});
							return false;
					 	 }else{
				 		  	 t1 = parseFloat(longTime)/1000.0/60;
						 	 t1 = t1.toFixed(2);
							 $("#entries_leaveLength").val(t1);
					 	 }
			 		}else if (that._unitType == 4){//单位为次 默认为1次
						$("#entries_leaveLength").val(1);
			 		}
			}
		//});
	}
	
	//选择完开始时间之后 自动带出结束时间和计算天数
	,setBeginTimeAndEndTime:function(){
		var that = this ;
		/*$("#entries_beginTime").change(function(){
			$("#entries_endTime-time").val( $("#entries_beginTime-time").val() );
			that.calculateLeaveLength();
		});
		$("#entries_endTime").change(function(){
			that.calculateLeaveLength_Longon();
			that.getRestDayAndLegalHoliday();
		});
		*/
	}
	
	,showMoreBindClick:function(){
		var that = this;
		//alert($('#dialogViewMore').attr('id'));
		//$('#showMoreLeaveInfo').text('关闭请假记录');
		//if($('#dialogViewMore').attr('id')==null||$('#dialogViewMore').attr('id')=="undefined"||$('#dialogViewMore').attr('id')==""){
			
			//$('#showMoreLeaveInfo').unbind('click');
	    	//$('#showMoreLeaveInfo').click(function(e){
	    		yearGolbal = new Date().getFullYear();
	    		$(".longDemo").show();
	    		that.clearEventDataInfos();
		    	that.ajaxLoadAllLeaveBillDatas(yearGolbal);
		    	
		    	
		    	dialog_Html = "<div id='dialogViewMore' style = 'font-size: 12px; padding: 10px; width: 93%' title=''>" +
				//"<p id='ppppp'></p>" + 中民投
				//"<div class='longDemo demo'>" +  中民投
				"<div>" + 
				"	<h2 style='font: 14px Microsoft Yahei; margin:0 auto; margin-left:5px;'>" +
				"		<font style=' font-size:0px;'>test</font>" +
				"		<span style='float:left; display:block;'><a id='a_pre' style='cursor:pointer'>前一年</a></span>" + //onClick='pre()' 
				"		<span style='float:right;display:block;'><a id='a_next' style='cursor:pointer'>后一年</a></span>" + //onClick='next()'
				"	</h2>" +
				"	<div id='longTimeLine' style='margin:0 auto;'></div>" +
				"</div>" +
				"</div>";
				
			$('#message_head').append("<div id='showViewMore'></div>");
			$('#showViewMore').html(dialog_Html);
			
			//绑定点击事件--放在里边会出现绑定多次点击事件
	        $('#a_pre').click(function(e){
	        	that.clearEventDataInfos();
	        	that.loadPreLeaveBillDatas();
	        });
	        $('#a_next').click(function(e){
	        	that.clearEventDataInfos();
	        	that.loadNextLeaveBillDatas();
	        });
        
    		//	$('#showMoreLeaveInfo').text('关闭请假记录');
			//	that.showMoreBindClick();    			
	    	//});
	    /*	
		}else{
			$('#showMoreLeaveInfo').unbind('click');
	    	$('#showMoreLeaveInfo').click(function(e){
	    		$('#showViewMore').html('');
    			$('#showMoreLeaveInfo').text('查看更多请假记录>> ');
				that.showMoreBindClick();    			
	    	});
		}
	    */	
	},
	setNavigateLine: function(){
	    var fromFlag = localStorage.getItem("fromFlag");
		var punchCardFlag = sessionStorage.getItem("punchCardFlag");
		var empolyeeBoardFlag =	sessionStorage.getItem("empolyeeBoardFlag");
		var parentUipk = "";
		if(parent.window.shr==null){
     		parentUipk = shr.getCurrentViewPage().uipk;
     	}else{
     		parentUipk = parent.window.shr.getCurrentViewPage().uipk;
     	}
		if(fromFlag == "employeeBoard" || fromFlag == "punchCard" 	|| ("fromPunchCard" == punchCardFlag && 
	     	"com.kingdee.eas.hr.ats.app.WorkCalendarItem.listSelf" == parentUipk) ||
	     	("empolyeeBoardFlag" == empolyeeBoardFlag && "com.kingdee.eas.hr.ats.app.WorkCalendar.empATSDeskTop" == parentUipk)){//来自我的考勤的时候。将导航条删除掉。
		    $("#breadcrumb").parent().parent().remove();
		    localStorage.removeItem("fromFlag");
		    window.parent.changeDialogTitle("我要请假");
		}
		var paramMethod = shr.getUrlRequestParam("method");
     	//从我要请假菜单中点击进来的URL上没有method参数
     	if(paramMethod == null){
     	    $("#breadcrumb").find(".active").text("我要请假");
     	}
	}
	
	,beforeSubmit :function(){
		
		var _self = this,
		workArea = _self.getWorkarea(),
		$form = $('form', workArea);
		if(_isHalfDayOff)
		{
			$('#entries_beginTime').val($('#entries_beginTime-time').val()+" "+$('#entries_beginTime-ap_el').val());
			$('#entries_endTime').val($('#entries_endTime-time').val()+" "+$('#entries_endTime-ap_el').val());
		}
		if (($form.valid() && _self.verify())) {
			return true ;
		}
		// return false 也能保存，固让js报错 
		var len = workArea.length() ;
		return false ;
	}
	,getCurrentModel : function(){
		
		var that = this ;
		var model = shr.ats.AtsLeaveBillEdit.superClass.getCurrentModel.call(this);
		var beginTime = model.entries[0].beginTime ;
		var endTime = model.entries[0].endTime;
		if(!(/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/.test(beginTime)))
		{
		  model.entries[0].beginTime = beginTime+":00";
		}
		if(!(/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/.test(endTime)))
		{
		  model.entries[0].endTime = endTime+":00";
		}
		
		model.entries[0].realBeginTime =  model.entries[0].beginTime ;
		model.entries[0].realEndTime =  model.entries[0].endTime ;
		model.entries[0].realLeaveLength =  model.entries[0].leaveLength ;
		  
		return model ;
	}
});


/**
 * k 循环下标
 * size 类型个数
 * changeColor(0,1,"","3T54RtSQRIqAL6cffMh60P0tUpg=","年休假","天",0);
 */
changeColor = function(k,size,holidayPolicyId,attendTypeId,attendTypeName,unitTypeName,unitTypeValue){
	//选中改变边框的粗细
	//alert(unitTypeName + "   " + unitTypeValue);
	//alert(k+"  "+size+"  "+attendTypeId+"  "+attendTypeName+"  "+unitTypeName+"  "+unitTypeValue)
	if (unitTypeName=="" || unitTypeValue=="") {
		shr.showInfo({message: "假期类型单位为空,请先设置假期类型单位！"});
		return false;
	}
	_unitType = unitTypeValue;
	for(i=0;i<size;i++){
	  if(k==i){
	  	//$("#type_test").hide();
		$("#div"+i).css({ "background-color":"#E4E4E4" }); //#F2F2F2  98bf21  #F39814-橙色   D9EDF7-浅蓝色  #E4E4E4-浅灰
		$("#div"+i).css({ "border":"1px solid #428BCA" }); //#79BEF0
		//其他div颜色变白
		for(m=0;m<size;m++){
			if(m!=i){
				$("#div"+m).css({ "background-color":"#FFFFFF" });
				$("#div"+m).css({ "border":"1px solid #E4E4E4" }); //边框变灰
				//$("#div"+2).css({ "background-color":"#FFFFFF" });
				//$("#div"+3).css({ "background-color":"#FFFFFF" });
			}
		}
		//设置假期类型字段的值
		$("#entries_policy").val(attendTypeName);
		$("#entries_policy_el").val(holidayPolicyId);//attendTypeId
		//再设置隐藏域(假期分类)的值
		$("#entries_policy_holidayType").val(attendTypeName);
		$("#entries_policy_holidayType_el").val(attendTypeId);
		//alert(holidayPolicyId);
	  }
	  /*
	 if(k==1){
		 $("#div1").css({ "background-color":"#98bf21" });
		 for(m=0;m<4;m++){
			if(m!=1){
				$("#div"+m).css({ "background-color":"#FFFFFF" });
				//$("#div2").css({ "background-color":"#FFFFFF" });
				//$("#div3").css({ "background-color":"#FFFFFF" });
			}
		 }
	 }
	 */
	}
	var that = this ;
 	//填充假期类型的单位
	setLeaveLengthType(unitTypeValue,unitTypeName);
};

/**
 * 设置默认选中项
 * entries.policy 假期制度(假期类型)
 * attendColl[i].holidayType.id 假期类型ID
 * attendColl[i].id				假期制度ID
 */
changeColorDefault  = function(k,size,attendColl,holidayPolicyId,attendTypeId,attendTypeName,unitTypeName,unitTypeValue){
	_unitType = unitTypeValue;
	// 假期类型默认选中    原来的假期类型   - 年假  - 第一个假期类型 
	
	for(i=0; i<size; i++){
	  if($("#entries_policy").val()==attendColl[i].name){
	  	//灰色背景 蓝色边框
		$("#div"+i).css({ "background-color":"#E4E4E4" });
		$("#div"+i).css({ "border":"1px solid #428BCA" });
		//其他div颜色变白
		for(m=0;m<size;m++){
			if(m!=i){
				$("#div"+m).css({ "background-color":"#FFFFFF" }); //白色背景
				$("#div"+m).css({ "border":"1px solid #E4E4E4" }); //边框变灰
			}
		}
		//设置假期类型字段的值
		$("#entries_policy").val(attendTypeName);
		$("#entries_policy_el").val(holidayPolicyId);//holidayPolicyId  attendTypeId
		//再设置隐藏域(假期分类)的值
		$("#entries_policy_holidayType").val(attendTypeName);
		$("#entries_policy_holidayType_el").val(attendTypeId);
		setLeaveLengthType(unitTypeValue,unitTypeName);
		return;
		}
	  }
	  for(i=0; i<size; i++){ 
	  if(attendColl[i].holidayType.id == "3T54RtSQRIqAL6cffMh60P0tUpg="){
	  	var holidayPolicy_Id = attendColl[i].id;//年假对应的假期制度ID
	  	
	  	//灰色背景 蓝色边框
		$("#div"+i).css({ "background-color":"#E4E4E4" });
		$("#div"+i).css({ "border":"1px solid #428BCA" });
		//其他div颜色变白
		for(m=0;m<size;m++){
			if(m!=i){
				$("#div"+m).css({ "background-color":"#FFFFFF" }); //白色背景
				$("#div"+m).css({ "border":"1px solid #E4E4E4" }); //边框变灰
			}
		}
		//设置假期类型字段的值
		$("#entries_policy").val(attendTypeName);
		$("#entries_policy_el").val(holidayPolicyId);//holidayPolicyId  attendTypeId
		//再设置隐藏域(假期分类)的值
		$("#entries_policy_holidayType").val(attendTypeName);
		$("#entries_policy_holidayType_el").val(attendTypeId);
		setLeaveLengthType(unitTypeValue,unitTypeName);
		return;
		}
	  }
	  for(i=0; i<size; i++){ 
	  	//var holidayPolicy_Id = attendColl[0].id;
  		if(attendColl[i].holidayType.id == attendColl[0].holidayType.id){
	  	//灰色背景 蓝色边框
		$("#div"+i).css({ "background-color":"#E4E4E4" });
		$("#div"+i).css({ "border":"1px solid #428BCA" });
		//其他div颜色变白
		for(m=0;m<size;m++){
			if(m!=i){
				$("#div"+m).css({ "background-color":"#FFFFFF" }); //白色背景
				$("#div"+m).css({ "border":"1px solid #E4E4E4" }); //边框变灰
			}
		}
		//设置假期类型字段的值
		$("#entries_policy").val(attendTypeName);
		$("#entries_policy_el").val(holidayPolicyId);//holidayPolicyId  attendTypeId
		//再设置隐藏域(假期分类)的值
		$("#entries_policy_holidayType").val(attendTypeName);
		$("#entries_policy_holidayType_el").val(attendTypeId);
		setLeaveLengthType(unitTypeValue,unitTypeName);
		return;
	  }
	}
	
};
/**
 * 1-天   2-小时   3-分钟  4-次
 */
setLeaveLengthType = function(unitTypeValue,unitTypeName){
	var _type = unitTypeValue;
	//alert(_type);
	if (_type == 1) {
		//this.getFieldValue("labConentries.leaveLength").html("test");
		$("#entries_remark").val("天");
		$(".appendUnit").html("天");
		$("#entries_realUnit").val(_type);
		$("#entries_realUnit_el").val(_type);
		//$("#entries_beginTime-ap").shrSelect("disable");
		//$("#entries_endTime-ap").shrSelect("disable");
		//$("#entries_beginTime-ap").val("");//清空上午和下午的值
		//$("#entries_endTime-ap").val("");
	}else if (_type == 2) {
		$("#entries_remark").val("小时");
		$(".appendUnit").html("小时");
		$("#entries_realUnit").val(_type);
		$("#entries_realUnit_el").val(_type);
		//$('#entries_beginTime-ap').shrSelect("enable");
		//$('#entries_endTime-ap').shrSelect("enable");
	}else if (_type == 3) {
		$("#entries_remark").val("分钟");
		$(".appendUnit").html("分钟");
		$("#entries_realUnit").val(_type);
		$("#entries_realUnit_el").val(_type);
		//$('#entries_beginTime-ap').shrSelect("disable");
		//$('#entries_endTime-ap').shrSelect("disable");
		//$("#entries_beginTime-ap").val("");
		//$("#entries_endTime-ap").val("");
	}else if (_type == 4) {
		$("#entries_remark").val("次");
		$(".appendUnit").html("次");
		$("#entries_realUnit").val(_type);
		$("#entries_realUnit_el").val(_type);
		//$("#entries_beginTime-ap").val("");
		//$("#entries_endTime-ap").val(""); 
		//$('#entries_beginTime-ap').shrSelect("disable");
		//$('#entries_endTime-ap').shrSelect("disable");
	}
	
	
	
	
};

	











