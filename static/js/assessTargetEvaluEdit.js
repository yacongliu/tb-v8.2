//人力绩效与组织绩效考核JS
var nodeCountDatas_size = 0;
var lastcell = "";
var num = 0;
var clz = null;
var initFirset_flag = true;
//var query_flag = true;
var tableSize = 0;
var NODESEQ_value = 1;
var nodeCount = 1; //   节点总计
var sId = null;
var sName = null;
var objName = "";
var nodeProcesserId = "";
var nodeLevel = "";
var isNull = false;
var personNull = [];//评分为空的数组
var score = ""; //平均分
var scoredatas = []; //左侧列表评分数组
var orgscore="";//部门评分
var isOrg=false;
var objectId=""//评估对象id

function getCommentValueArr_one(commentValueArr, nodeCountDatas, nodeId) {
	switch (nodeId) {
		case 1:
			commentValueArr.push(nodeCountDatas.comment1);
			break;
		case 2:
			commentValueArr.push(nodeCountDatas.comment2);
			break;
		case 3:
			commentValueArr.push(nodeCountDatas.comment3);
			break;
		case 4:
			commentValueArr.push(nodeCountDatas.comment4);
			break;
		case 5:
			commentValueArr.push(nodeCountDatas.comment5);
			break;
	}
	return commentValueArr;

}

function getCommentValueArr(nodeCountDatas, nodeId) {
	var resultArr = [];
	switch (nodeId) {
		case 1:
			resultArr.push(nodeCountDatas.comment1);
			break;
		case 2:
			resultArr.push(nodeCountDatas.comment1);
			resultArr.push(nodeCountDatas.comment2);
			break;
		case 3:
			resultArr.push(nodeCountDatas.comment1);
			resultArr.push(nodeCountDatas.comment2);
			resultArr.push(nodeCountDatas.comment3);
			break;
		case 4:
			resultArr.push(nodeCountDatas.comment1);
			resultArr.push(nodeCountDatas.comment2);
			resultArr.push(nodeCountDatas.comment3);
			resultArr.push(nodeCountDatas.comment4);
			break;
		case 5:
			resultArr.push(nodeCountDatas.comment1);
			resultArr.push(nodeCountDatas.comment2);
			resultArr.push(nodeCountDatas.comment3);
			resultArr.push(nodeCountDatas.comment4);
			resultArr.push(nodeCountDatas.comment5);
			break;
	}
	return resultArr;

}
//var colNames = ["[ljj] ljj 60% 节点总分","[ljj] ljj 60% 评分等级","[ljj] ljj 40% 节点总分","[ljj] ljj 40% 评分等级"];
function getCommentNodeNameArr(colNames) {
	var tmpArr = [],
		resultArr = [],
		n = colNames.length,
		i, tmp;
	for (i = 0; i < n; i += 2) { //  colNames  格式为     "[ljj] ljj 60% 节点总分","[ljj] ljj 60% 评分等级"  ,因此步长为2  
		if (-1 == colNames[i].indexOf("%")) { //  格式为   [ljj] ljj 节点总分  ,   无权重时 采用  
			tmp = colNames[i].substr(0, colNames[i].length - 4);
			resultArr.push(tmp);
		} else {
			tmpArr = colNames[i].split("%");
			resultArr.push(tmpArr[0] + '%');
		}



	}
	return resultArr;
}


//去掉js数组中重复项 

function removeDuplElem(arr) {
	var newarr = [],
		n = arr.length,
		i, j;
	for (i = 0; i < n; i++) {
		for (j = i + 1; j < n; j++) {
			if (arr[i].value == arr[j].value) {
				j = false;
				break;
			}
		}
		if (j) newarr.push(arr[i]);
	}
	//return newarr.sort(function(arr,newarr){return arr-newarr}); 
	return newarr;
}



function calculateTotalScore(targetDatas, nodeId) {

	var totalScore = 0.00,
		targetData, targetData_score;
	var ruleModeValue = $("#ruleModeValue").val();

	for (var i = 0, length = targetDatas.length; i < length; i++) {
		targetData = targetDatas[i];
		//采用动态拼接字符串 score1、score2 等 时， targetData.scoreX 为未定义，  无奈采用直接targetData.score1
		switch (nodeId) {
			case "1":
				targetData_score = targetData.score1;
				break;
			case "2":
				targetData_score = targetData.score2;
				break;
			case "3":
				targetData_score = targetData.score3;
				break;
			case "4":
				targetData_score = targetData.score4;
				break;
			case "5":
				targetData_score = targetData.score5;
				break;

		}
		if (!targetData_score) {
			targetData_score = 0;
		}

		if (100 == ruleModeValue) { //权重&指标合计
			totalScore += (targetData.weight / 100) * targetData_score;
		} else if (101 == ruleModeValue) { //分值&指标合计
			totalScore += Number(targetData_score);

		}

	}

	//精确到小数点后2位
	return Number(totalScore.toFixed(2));
}



function doRenderDataGridForCount(n, value, cellEdit) {
	var rankFilter_value = "";
	rankFilter_value = value.rankFilter;
	options = {
		data: value.nodeCountDatas,
		//url:url,json   local
		datatype: "local",
		multiselect: false,
		rownumbers: false,
		_type_: "EditGrid",
		cellEdit: cellEdit,
		colNames: value.colNames,
		width: "inherit",
		autowidth: false,
		colModel: value.modelColList,
		height: 'auto',
		//	scroll : true,
		//	multiselect: true,			   	
		shrinkToFit: false,
		autoScroll: true,
		cellsubmit: "clientArray",
		mtype: "post",
		recordpos: 'left',
		recordtext: '({0}-{1})/{2}',
		gridview: true,
		pginput: false,
		afterEditCell: function (rowid, cellname, value, iRow, iCol) {

			var editor = null;
			var cellObject = $('#table_count').jqGrid('getCellObject', rowid, cellname);
			if (cellObject && cellObject.length > 0) {
				editor = cellObject[0].editor.getEditor();
			}
			editor.shrPromptBox('setFilter', rankFilter_value);

		},
		keyOptions: {
			addNewRowForLastCell: false,
			loop: true
		}


	};
	$("#table_count").jqGrid(options);


	setTimeout(function () {
		var datas = [];
		for (var i = 0; i < tableSize; i++) {
			//alert("tableSize"+tableSize);
			if ($("#isCompute" + i).val() == "1") { //  不参与计算的表体  直接continue
				continue;
			}
			var ds = $("#table" + i).wafGrid('getAllRowData');
			for (var j = 0; j < ds.length; j++) {
				//alert("tableSize"+tableSize);								
				datas.push(ds[j]);
			}

		}
		var cellname = value.modelColList[value.modelColList.length - 2].index;
		var nodeId = cellname.substr(5, 6);
		var totalScore = calculateTotalScore(datas, nodeId);

		var grid = $("#table_count");
		var id = grid.getDataIDs()[0];

		grid.setCell(id, cellname, totalScore);
		//grid.setCell(id, "score"+NODESEQ_value, totalScore);	
	}, 500);
}



function doRenderDataGrid(n, value, cellEdit) {
	options = {
		data: value.nodeProcesserData,
		//url:url,json   local
		datatype: "local",
		multiselect: false,
		rownumbers: false,
		_type_: "EditGrid",
		cellEdit: cellEdit,
		colNames: value.colNames,
		width: '100%',
		autowidth: false,
		colModel: value.modelColList,
		height: 'auto',
		// 	scroll : true,
		multiselect: true,
		shrinkToFit: false,
		autoScroll: true,
		cellsubmit: "clientArray",
		mtype: "post",
		recordpos: 'left',
		recordtext: '({0}-{1})/{2}',
		gridview: true,
		pginput: false,
		beforeEditCell: function (rowid, cellname, value, iRow, iCol) {
			if ("score" == cellname.substr(0, 5)) {
				var calFormula = $("#table" + n).jqGrid("getCell", rowid, "calFormula");
				if (calFormula) {
					$("#table" + n + "").jqGrid("setCell", rowid, iCol, '', 'not-editable-cell');
					$("#table" + n + "").jqGrid.restoreCell(iRow, iCol);
				}

			}


		},
		afterSaveCell: function (rowid, cellname, value, iRow, iCol) {
			if ("score" == cellname.substr(0, 5)) {
				if (value && isNaN(value)) {
					shr.showError({
						message: "输入值必须为数字！",
						hideAfter: 2
					});
					return;
				}
				var datas = [];
				for (var i = 0; i < tableSize; i++) {
					if ($("#isCompute" + i).val() == "1") { //  不参与计算的表体  直接continue
						continue;
					}

					//alert("tableSize"+tableSize);
					var ds = $("#table" + i).wafGrid('getAllRowData');
					//var col= $("#table"+i).jqGrid('getCol',cellname,false);//获取列名为name的列的值							
					//var id = $("#table"+i).getDataIDs()[0]; 
					//var score1 =  $("#table"+i).jqGrid("getCell", id, "score1");
					for (var j = 0; j < ds.length; j++) {
						datas.push(ds[j]);
					}
				}
				var nodeId = cellname.substr(5, 6);
				var totalScore = calculateTotalScore(datas, nodeId);
				var grid = $("#table_count");
				var id = grid.getDataIDs()[0];
				grid.setCell(id, cellname, totalScore);
			}
		},
		keyOptions: {
			addNewRowForLastCell: false,
			loop: true
		}
	};
	$("#table" + n + "").jqGrid(options);
}


//  因 目标表格为循环生成，  该方法用于获取相应表格进行操作
function getFormGridName(gridName) {

	var strs = new Array(); //定义一数组

	strs = gridName.split("_"); //字符分割      ，  因  动态生成的按钮的命名格式为  ： 如：  addbutton_0

	return "#table" + strs[1];
}


function initAssessTargetForm(id, html2, state_value, SOLUTIONPERIODID, EVALUOBJECTID, NODESEQ_value) {
	tableSize = 0;
	solutionpId=SOLUTIONPERIODID;
	that.remoteCall({
		method: "initTargetForm",
		param: {
			nodeState: $('#nodeState_el').val(),
			assignmentId: id,
			proInstId: $("#proInstId").val(),
			SOLUTIONPERIODID: SOLUTIONPERIODID,
			EVALUOBJECTID: EVALUOBJECTID,
			NODESEQ: NODESEQ_value
		},
		success: function (data) {
			//请求自定义单选框数据
				that.remoteCall({
						method: "getRadioData",
						success: function (radioData) {
			//计算公式出错提示
			if (data.opResult == 'no') {
				shr.showError({
					message: data.msg,
					hideAfter: 10
				});
				return;
			}
			var datas = eval(data);
			var cellEdit = true;
			var hidden_flag = "";
			if (state_value >= 4 || state_value == 2) { //提交后，设为不可编辑, 也不可提交
				cellEdit = false;
				hidden_flag = "none";
			} else {
				cellEdit = true;
				hidden_flag = "";
			}
			$.each(datas, function (n, value) {
				if ((value.nodeProcesserData != null) && (value.nodeProcesserData != '')) {
					//保存评分规则，用于节点总分计算
					$("#ruleModeValue").val(value.ruleModeValue);

					var hidden_flag = "";
					var isCompute_value = "0"; //  0 参与计算   1 不参与
					if (value.isCompute) { //   表单参与计算则不显示						
						hidden_flag = "none";
					} else {
						isCompute_value = "1";
						hidden_flag = "";
					}


					html2 += "<div class='assess-table'>";
					if ((value.nodeProcesserData != null) && (value.nodeProcesserData != '')) {
						html2 += "<div style=' display: inline-block;'><span class='shr-baseInfo-title'>" + value.Name + "(" + value.nodeProcesserData.length + "条)</span>&nbsp;&nbsp;&nbsp;&nbsp;<span  style=' display : " + hidden_flag + " ;   float:right; color:red' class='shr-baseInfo-title'>(此表单不参与总分计算)</span><input id='isCompute" + n + "' value='" + isCompute_value + "'  type='hidden'/>	</div>";
						//	"<div class='assess-weight'><span> 权重(%)/分值合计：  "+ value.firstLevelTargetTypeWeight_count +"</span></div>";
					} else {
						html2 += "<div style=' display: inline-block;'><span class='shr-baseInfo-title'>" + value.Name + "(0条)</span>&nbsp;&nbsp;&nbsp;&nbsp;<span   style=' display : " + hidden_flag + " ;   float:right; color:red' class='shr-baseInfo-title'>(此表单不参与总分计算)</span><input id='isCompute" + n + "' value='" + isCompute_value + "'  type='hidden'/></div>";

					}
					if ((value.PartTableDes != null) && (value.PartTableDes != '')) {
						html2 += "<div><textarea disabled='disabled' name='PartTableDes' cols='10'>" + value.PartTableDes + "</textarea></div>";

					}
					html2 += "<br>";
					html2 += "<div><table     id='table" + n + "'></table></div>"; //style='width:700px'
					html2 += "</div>"; // class='assess-table'

				} else if ((value.nodeCountDatas != null) && (value.nodeCountDatas != '')) {

					
					var commentNodeNameArr = getCommentNodeNameArr(value.colNames);
					nodeCount = commentNodeNameArr.length;
					var commentValueArr = [];
					var nodeSeq = null;
					/*
					for (var i = value.modelColList.length; i >= 0; i=i-2) {
						//nodeSeqArr.push(value.modelColList[0].index.substr(10,11));
						nodeSeq = value.modelColList[i-1].index.substr(10,11);
						commentValueArr = getCommentValueArr(value.nodeCountDatas[0],parseInt(nodeSeq));
					}*/
					for (var i = 0; i < value.modelColList.length; i = i + 2) {
						//nodeSeqArr.push(value.modelColList[0].index.substr(10,11));
						nodeSeq = value.modelColList[i].index.substr(5, 6);
						commentValueArr = getCommentValueArr_one(commentValueArr, value.nodeCountDatas[0], parseInt(nodeSeq));
					}

					/*
				  	if(nodeCount==1){   //  当只可见自身节点时走此分支，   获取当前节点序号  
				  		var node = value.modelColList[0].index.substr(5,6)
				  		commentValueArr = getCommentValueArr_one(value.nodeCountDatas[0],parseInt(node));
				  	}else if(nodeCount>0){
				  		commentValueArr = getCommentValueArr(value.nodeCountDatas[0],nodeCount);
				  	}
					*/
					//自定义单选框				
					if(radioData.radioData!=undefined &&isOrg==true){
							var radioList= new Array();
							radioList=radioData.radioData;
							for(var i=0;i<radioList.length;i++){
							html2+='<input name="exitType" type="radio" id="'+radioList[i].name+'" class="radio" data-pk="'+radioList[i].name+'" data-name="'+radioList[i].name+'" /> <label for="exitType'+radioList[i].name+'" class="radio_label">'+radioList[i].name+'</label>'+'</br>';	
							}
						}
					var commentValue = '';

					html2 += "<div class='node-comment'>";
					//html2 += "<div style='border:1px solid;  margin: 4px;   '>";
					//html2 += "<hr style=' border-top:1px dashed #CCCCCC;' />"; 
					html2 += "<td ><span  class='shr-baseInfo-title' >节点评论</span></td>";
					//
					html2 += "<table width='100%'   >"; // width='700' 
					

					for (var i = 0; i < nodeCount; i++) {
						if (commentValueArr[i] != null) {
							commentValue = commentValueArr[i];
						} else {
							commentValue = '';
						}

						html2 += "<tr>"; //cols='25'	   style="ime-mode:disabled"    
						if ((nodeCount == (i + 1)) && ($('#nodeState_el').val() != 1) && (value.nodeIsCanComment[i] > 0)) { // 当前节点 可编辑  , $('#nodeState_el').val()==1:已提交    均不可编辑	,  nodeIsCanComment: 0:无须评论;	1: 评论且必录   ;2 : 评论非必录		  								  				
							html2 += "<td><span  style='font-size: 16px;'>" + commentNodeNameArr[i] + "</span><textarea    id='comment" + i + "' rows='6'  >" + commentValue + "</textarea></td>";
						} else {
							html2 += "<td><span  style='font-size: 16px;'>" + commentNodeNameArr[i] + "</span><textarea    readonly='true'  id='comment" + i + "' rows='6'  >" + commentValue + "</textarea></td>";
						}
						html2 += "</tr>";
					}

					html2 += "</table >";
					html2 += "</div>"; //class='node-comment'
					//html2 += "<hr style='height: 1px; border:none; border-top:1px dashed #CCCCCC;' />";
					html2 += "<div class='node-count'>";
					html2 += "<td ><span  class='shr-baseInfo-title' > " + value.Name + "</span></td>";
					html2 += "<div><table   id='table_count'></table></div>"; //style='width:700px'	
					html2 += "</div>"; //class='node-count'
					
				} else {
					isCompute_value = "1"; //  未生成table  ，该table不参计算
					html2 += "<div><table     id='table" + n + "'><input id='isCompute" + n + "' value='" + isCompute_value + "'  type='hidden'/></table></div>"; //style='width:700px'
				}
					});

			// html2 += "</div>";  //  <div style='border:1px solid;  margin-top : 4px;'> 的  结束
			$("#assessTargetbaseInfo").append(html2);

			//生成 Grid
			$.each(datas, function (n, value) {
				if ((value.nodeProcesserData != null) && (value.nodeProcesserData != '')) {
					doRenderDataGrid(n, value, cellEdit);
					nodeCountDatas_size = value.nodeProcesserData.length;
					//统计生成的指标表格数
					tableSize = tableSize + 1;
					//  生成合计表格	 
				} else if ((value.nodeCountDatas != null) && (value.nodeCountDatas != '')) {
					doRenderDataGridForCount(n, value, cellEdit);
				} else {
					//统计生成的指标表格数
					tableSize = tableSize + 1; //  空表格 也计数
				}
			});
			//将单选框勾选
			that.remoteCall({
						method: "getRadioChecked",
						param: {
							objectId: objectId						
						},
					success: function (data) {
						if(data!=null&&data.radioid!=null){
							$("#"+data.radioid).attr("checked",true);
						}							
				}});
		}
	});
	}});
	//  设置当前节点的  NodeLevel
	if ($('#nodeState_el').val() == 0 || $('#nodeState_el').val() == "0") {
		nodeLevel = clz.getCurrentNodeLevel(id);
	}
	//设置自定义单选框选中
				//$("#exitType1").attr("checked",true);
				
	

}

function initEdit(Id) {
	setTimeout(function () {
		$.block.show({
			text: '正在执行，请稍候...'
		});
	}, shr.blockMinTime);

	that.remoteCall({
		method: "getAssessTarget",
		param: {
			Id: Id,
			nodeState: $('#nodeState_el').val()
		},
		success: function (data) {

			var datas = eval(data);
			$("#assessTargetbaseInfo").remove();
			//$("#baseInfo").append(" <div id='right-container' class='offset0 span8'><div class='access-content' ><div id='assessTargetbaseInfo'    ></div></div></div>");
			$("#baseInfo").append("<div id='assessTargetbaseInfo'></div>");
			var html2 = "";
			var hidden_commit = ""
			if ($('#nodeState_el').val() == 1) { //  已提交 状态下 不显示  保存 提交按钮
				hidden_commit = "none";
			}

			if (data == null || data.length == 0) {
				shr.showInfo({
					message: "请选择左侧数据！",
					hideAfter: 2
				});
				setTimeout(function () {
					$.block.hide();
				}, shr.blockMinTime);
				$("#hiddenId").val(""); //  无数据是  清空  hiddenId ，防止 保存报错
				return;
			}


			$.each(datas, function (n, value) {
				html2 += "<div class='row-fluid row-block ' id=''><div id='' class='span12 offset0 '><div id='base'>";
				if (value.SOLUTIONTYPE == "100") {
					html2 += "<div class='left-content span3'><div class='picture'><img id='personPhoto' title=" + value.OBJECTID + " src='/shr/personPhoto.do?personId=" + value.OBJECTID + " ' width='100' ,height='120' style='cursor: pointer;'  ></div></div>";
				} else {
					html2 += "<div class='left-content span3'><div class='picture'><img id='personPhoto'  src='/shr/personPhoto.do?personId=" + value.RESPONSERID + " ' width='100' ,height='120' style='cursor: pointer;'  ></div></div>";
				}
				html2 += "<div class='center-content span4'>";
				if ((value.ORGNAME != null) && (value.ORGNAME != '')) {
					html2 += "<h4 class='people-name'><span value='" + value.ID + "'  >" + value.ORGNAME + "(" + value.EVALUOBJECTNUMBER + ")</span></h4>";
					objName = value.ORGNAME;
				} else {
					html2 += "<h4 class='people-name'><span value='" + value.ID + "' >" + value.EVALUOBJECTNAME + "(" + value.EVALUOBJECTNUMBER + ")</span></h4>";
					objName = value.EVALUOBJECTNAME;
				}
				html2 += "<div class='people-info'>";
				if ((value.PRINCIPAL != null) || (value.RESPONPOSITION != null)) {
					html2 += "<div class='assess-project-title'>" + value.PRINCIPAL + "</div>";
					html2 += "<div class='assess-project-title'>" + value.RESPONPOSITION + "</div>";
				} else {
					if ((value.ORG != null) && (value.ORG != '')) {
						html2 += "<div class='assess-project-title' title=" + value.FULLORGNAME + "  >" + value.ORG + "</div>";
					}
					if ((value.POSITION != null) && (value.POSITION != '')) {
						html2 += "<div class='assess-project-title'>" + value.POSITION + "</div>";
					}
				}
				if (value.NODESCORE != null && value.NODESCORE != "") {
					html2 += "<div class='assess-project-title'><span>当前得分：</span><span class = 'black'>" + Number(value.NODESCORE).toFixed(2) + "</span></div>";
				}
				html2 += "</div>"; // class='people-info'                                                           
				html2 += "</div>"; //  class='center-content span5'

				html2 += "<div class='right-content span4'><div class='assess-info'>";
				html2 += "<div class='assess-project-title'><span>评估活动：</span><span class = 'black'>" + value.SOLUTIONPERIODNAME + "</span></div>" +
					"<div class='assess-start'><span>评估开始日期: </span><span class = 'black'>" + value.EVALUSTARTDATE + "</span></div>" +
					"<div class='assess-end'><span>评估结束日期: </span><span class = 'black'>" + value.EVALUENDDATE + "</span></div>";
				//"<div class='assess-score-title'><span>权重(%)/分值总合计：</span><span class = 'black'>" + Number(value.TARGETEVALUSTOREWEIGHT).toFixed(2) + "</span></div>";            
				if (value.GRADEMODE != null && value.GRADEMODE != "") {
					html2 += "<div class='assess-project-title'><span>评分分制：</span><span class = 'black'>" + value.GRADEMODE + "</span></div>";
					//html2 += "<div class='assess-project-title'><span>当前部门评分为：</span><span class = 'black'>" + data.a + "</span></div>";
				}
				if(value.SOLUTIONPERIODID!=null && value.SOLUTIONPERIODID!=""){
				that.remoteCall({
				method: "getUserOrgScore",
				param: {
					solutionId: value.SOLUTIONPERIODID
				},
				success: function (datas) {
				if(isOrg==false){
			    if(datas.adminOrgScore==null ||datas.adminOrgScore==''){
					html2 += "<div class='assess-project-title'><span>部门评分为：</span><span class = 'black'>" + '部门考核评分为空'+ "</span></div>";
				}else{
				orgscore=datas.adminOrgScore;
				html2 += "<div class='assess-project-title'><span>当前部门评分为：</span><span class = 'black'>" + datas.adminOrgScore + "</span></div>";
				}
				if (value.RANK != null && value.RANK != "") {					
					html2 += "<div class='assess-project-title'><span>当前评估等级：</span><span class = 'black'>" + value.RANK + "</span></div>";
				}
				}
				
				
				html2 += "</div></div>"; //   class='right-content span4'
				html2 += "<div class='resize-tag span1'><div class='arrow'><i class='icon-resize-full'></i> </div> </div>";
				html2 += "</div></div></div>"; //id='base'				
				// 保存参数
				html2 += "<input  id='solutionId' type='hidden'   value='" + value.SOLUTIONPERIODID + "'     />";
				html2 += "<input  id='objId' type='hidden'   value='" + value.OBJECTID + "'     />";
				//html2 += "<input  id='procInstID' type='hidden'   value='"+ value.procInstID +"'     />"  ;

				var state_value = 0;
				$("#proInstId").val(value.procInstID);

				if (value.NODESEQ != null) {
					NODESEQ_value = value.NODESEQ;
				}
				
					objectId=value.EVALUOBJECTID;
					initAssessTargetForm(value.ID, html2, state_value, value.SOLUTIONPERIODID, value.EVALUOBJECTID, NODESEQ_value);
					
				}});
				
				}
				
				// 设置菜单导航名称为评估对象					
				var breadcrumb = $('#breadcrumb').shrBreadcrumb();
				var pre = breadcrumb.shrBreadcrumb('pop');
				//保存上层面包屑
				if (breadcrumb.find('li').length === 0) {
					if (pre.find('li').length != 0) {
						breadcrumb.shrBreadcrumb('addItem', pre);
					}
				}
				breadcrumb.shrBreadcrumb('addItem', {
					name: objName + " " + value.SOLUTIONPERIODNAME
				});
			});
			setTimeout(function () {
				$.block.hide();
			}, shr.blockMinTime);

		}
	});

}

shr.defineClass("shr.perf.AssessTargetEvaluEdit", shr.framework.Edit, {
	that: null,
	preventScroll: false,
	resumeNav: null,

	initalizeDOM: function () {
		this.operateState = "EDIT";
		shr.perf.AssessTargetEvaluEdit.superClass.initalizeDOM.call(this);
		that = this;
		clz = this;

		var nodeState_value = shr.getUrlRequestParam("nodeState_value");
		var solutionPeriodname = shr.getUrlRequestParam("solutionPeriodname");
		var solutionPeriodId = shr.getUrlRequestParam("solutionPeriodId");
	
		var wfType = shr.getUrlRequestParam("wfType");
		if (wfType == "approve") { //由我的已办跳转进入， 设置为已提交
			nodeState_value = "1";
			//获取  评估结果id
			//nodeProcesserId = that.getNodeProcesserId(shr.getUrlRequestParam("assignmentID"));
		}

		if (nodeState_value != null || nodeState_value != "") {
			//默认查询未提交评估的数据			
			$('#nodeState_el').val(nodeState_value);
			if (nodeState_value == "1") {
				$('#nodeState').val("已提交");
			} else {
				$('#nodeState').val("未提交");
			}

		} else { //默认查询未提交评估的数据
			$('#nodeState').val("未提交");
			$('#nodeState_el').val(0);
		}
		// 屏蔽  由列表界面 进入时，默认填充所选的考核活动
		sId = solutionPeriodId;
		sName = decodeURI(solutionPeriodname);
		var solutionPeriodnameArr = [{
			'value': sId,
			'alias': sName
		}];
		var select_json = {
			id: "solutionPeriodname",
			readonly: "",
			value: "0",
			onChange: null,
			filter: ""
		};
		select_json.data = solutionPeriodnameArr;
		$('#solutionPeriodname').shrSelect(select_json);
		if (sId && sName) {
			$('#solutionPeriodname').val(sName);
			$('#solutionPeriodname_el').val(sId);
		} else {
			$('#solutionPeriodname').val("所有");
			$('#solutionPeriodname_el').val("");
		}

		if ($('#nodeState_el').val() == 1) {
			$("#saveTarget").hide();
			$("#commitTarget").hide();
			$("#allCommitTarget").hide();
			$("#backTarget").hide();
		} else {
			$("#saveTarget").show();
			$("#commitTarget").show();
			$("#allCommitTarget").show();
			$("#backTarget").show();
		}

		if (wfType == "approve") { //由我的已办跳转进入， 设置为已提交			
			//获取  评估结果id
			nodeProcesserId = that.getNodeProcesserId(shr.getUrlRequestParam("assignmentID"));
		} else {
			that.getAssessTargetForLeftList();
		}

		//添加onChange事件 , 只有状态下拉框生效， 暂屏蔽
		//that.initDataChangeAction();




	},

	buttonHide:function(id){		
		var _self=this;
		_self.remoteCall({
		method: 'getEvaluType',
		param: {
			id:id
			},
		success: function (data) {
			if(data.type=="org"){
			 $("#allCommitTarget").hide();
			 isOrg=true;
			}
			if(data.type=="person"){
			 $("#commitTarget").hide();
			}
		}
	});
	},


	/**
	 * 
	 */
	getAssessTargetForLeftList: function () {
		var _self=this;
		/*========扩展开始========*/
		var avgScore = 0; //平均分
		var sumScore = 0; //总分
		score = ""; //平均分（字符串）
		scoredatas = []; //左侧列表评分数组
		
		/*========扩展结束========*/
		setTimeout(function () {
			$.block.show({
				text: '正在执行，请稍候...'
			});
		}, shr.blockMinTime);
		this.remoteCall({
			method: "getAssessTarget",
			param: {
				nodeState: $('#nodeState_el').val(),
				solutionPeriodId: $('#solutionPeriodname_el').val()
			},
			success: function (data) {
				personNull = [];
				isNull = false;
				if (data == null || data.length == 0) {
					shr.showInfo({
						message: "本次查询无数据！",
						hideAfter: 2
					});
					$("#assessTargetList").remove();
					$("#assessTargetbaseInfo").remove();
					$("#hiddenId").val("");
					//置空考核活动查询条件
					//  控件bug   加载之前 先移除
					$("input[name='solutionPeriodname']").remove();
					$("#solutionPeriodname_div").append("<input type='text' class='block-father input-height'  id='solutionPeriodname' name='solutionPeriodname' />");

					var solutionPeriodnameArr = [{
						'value': '',
						'alias': '所有'
					}];
					var select_json = {
						id: "solutionPeriodname",
						readonly: "",
						value: "0",
						onChange: null,
						filter: ""
					};
					select_json.data = solutionPeriodnameArr;
					$('#solutionPeriodname').shrSelect(select_json);
					//  控件bug   加载之前 先移除
					if ($("div[class='ui-select-frame']").size() == 2) {
						$("div[class='ui-select-frame']").eq(0).remove();
					}

					//默认查询数据					
					$('#solutionPeriodname').val("所有");
					$('#solutionPeriodname_el').val("");

					setTimeout(function () {
						$.block.hide();
					}, shr.blockMinTime);

				} else {	
					
					_self.buttonHide(data[0].SOLUTIONPERIODID);//data[0].SOLUTIONPERIODID
					var datas = eval(data);
					//  加载前 移除原有数据，避免重复加载
					
					$("#assessTargetList").remove();
					$("#left").append("<div id='assessTargetList'  style='float: left; display: block; overflow: scroll;height: 600px;'   ></div>");
					var html2 = "";
					var solutionPeriodnameArr = [{
						'value': '',
						'alias': '所有'
					}];
					$.each(datas, function (n, value) {
						html2 = "<div class='score-box'>";
						html2 += "<div class='person-info'>";
						if (value.SOLUTIONTYPE == "100") {
							html2 += "<div class='person-pic'><img src='/shr/personSquarePhoto.do?personId=" + value.OBJECTID + " ' ></div>";
						} else {
							html2 += "<div class='person-pic'><img src='/shr/personSquarePhoto.do?personId=" + value.RESPONSERID + " ' ></div>";
						}
						html2 += "<div class='person-text'>";

						//if(value.SOLUTIONTYPE=="100"){
						html2 += "<p class='person-name'><span><a style='font-size: 16px; text-decoration:underline;' class='fff'  value='" + value.ID + "'   >" + value.EVALUOBJECTNAME + "(" + value.EVALUOBJECTNUMBER + ")<span id='" + value.ID + "' style=' display : none;'  class='tag'><i class='icon-ok-sign'></i></span></a></span></p>";
						//}else{
						//	html2 += "<p class='person-name'><span><a style='font-size: 16px; text-decoration:underline;' class='fff'  value='"+ value.ID +"'   >" + value.ORG + "("+value.EVALUOBJECTNUMBER+")</a></span></p>";
						//  }

						html2 += "<p class='job-info'>";
						if ((value.PRINCIPAL != null) || (value.RESPONPOSITION != null)) {
							//html2 += "<p class='access-proj'><span>负责人：" + value.PRINCIPAL + "</span></p>";
							//html2 += "<p class='access-proj'><span>负责岗位： " + value.RESPONPOSITION + "</span></p>";
							html2 += "<p><span class='access-proj'>" + value.PRINCIPAL + "</span></p>";
							html2 += "<p><span class='access-proj'>" + value.RESPONPOSITION + "</span></p>";
						} else {
							if ((value.ORG != null) && (value.ORG != '')) {
								//html2 += "<p class='access-proj'><span>部门：" + value.ORG + "</span></p>";
								html2 += "<p><span class='access-proj'>" + value.ORG + "</span></p>";
							}
							if ((value.POSITION != null) && (value.POSITION != '')) {
								html2 += "<p><span class='access-proj'>" + value.POSITION + "</span></p>";
								//html2 += "<p class='access-proj'><span>岗位：" + value.POSITION + "</span></p>";
							}
						}
						html2 += "</p>";
						html2 += "<p class='job-info'>";
						html2 += "<p   ><span class='access-proj' >" + value.SOLUTIONPERIODNAME + "</span></p>";
						//为 评估活动 查询条件 准备
						solutionPeriodnameArr.push({
							'value': value.SOLUTIONPERIODID,
							'alias': value.SOLUTIONPERIODNAME
						});

						//html2 +="<p class='access-score'> <span>权重(%)/分值总合计：</span><span>" + Number(value.TARGETEVALUSTOREWEIGHT).toFixed(2) + "</span></p>";
						//html2 +="<p class='access-score'> <span></span><span></span></p>";   class='access-proj'
						if (value.NODESCORE != null && value.NODESCORE != "") {
							html2 += "<p  >&nbsp;&nbsp;&nbsp;&nbsp;<span  >当前节点评分：" + Number(value.NODESCORE).toFixed(2) + "</span></p>";
							/*========扩展开始========*/
							//获取总分
							sumScore += Number(value.NODESCORE);
							//添加进数组
							scoredatas.push(Number(Number(value.NODESCORE).toFixed(2)));
							/*========扩展结束========*/
						} else {
							html2 += "<p  >&nbsp;&nbsp;&nbsp;&nbsp;<span  >当前节点评分：尚未保存评分</span></p>";
							isNull = true;
							personNull.push(value.EVALUOBJECTNAME);
						}
						html2 += "</p>";
						html2 += "</div> ";
						html2 += "</div>"; // class='person-info'
						html2 += "</div>"; // class='score-box'				
						$("#assessTargetList").append(html2);
					});
					// 为列表循环添加单击事件

					$(function () {
						$(".fff").each(function () {
							//a.push($(".fff").attr("value"));
							$(this).click(function () {
								//点击后显示选中图标
								$(".tag").hide();
								$(this).children().show();
								var m = $(this).attr("value");
								$("#hiddenId").val(m);
								initEdit(m);
							});
						});
					});
					// 填充考核活动查询条件
					//  控件bug   加载之前 先移除
					$("input[name='solutionPeriodname']").remove();
					$("#solutionPeriodname_div").append("<input type='text' class='block-father input-height'  id='solutionPeriodname' name='solutionPeriodname' />");
					var select_json = {
						id: "solutionPeriodname",
						readonly: "",
						value: "0",
						onChange: null,
						filter: ""
					};
					//去掉js数组中重复项 				
					solutionPeriodnameArr = removeDuplElem(solutionPeriodnameArr);
					select_json.data = solutionPeriodnameArr;
					$('#solutionPeriodname').shrSelect(select_json);
					//  控件bug   加载之前 先移除
					if ($("div[class='ui-select-frame']").size() == 2) {
						$("div[class='ui-select-frame']").eq(0).remove();
					}
					//默认查询数据
					if (sId && sName) {
						$('#solutionPeriodname').val(sName);
						$('#solutionPeriodname_el').val(sId);
					} else {
						$('#solutionPeriodname').val("所有");
						$('#solutionPeriodname_el').val("");
					}

					setTimeout(function () {
						$.block.hide();
					}, shr.blockMinTime);


					//默认初始化加载第一个到 右侧编辑框				
					if (datas.length > 0 && initFirset_flag) {
						// 查询时  不从billId  取值查询 ， 防止   已提交  、未提交  查询条件切换时     billId 不匹配  查询不到数据        query_flag = false						
						if (shr.getUrlRequestParam("assignmentID") != null && shr.getUrlRequestParam("assignmentID") != "") { //  有  assignmentID  说明由我的代办事项  跳转进入
							if (nodeProcesserId != "" && nodeProcesserId) {
								$("#hiddenId").val(nodeProcesserId);
							} else {
								$("#hiddenId").val(shr.getUrlRequestParam("assignmentID"));
							}

						} else if (shr.getUrlRequestParam("billId") != null && shr.getUrlRequestParam("billId") != "") {
							$("#hiddenId").val(shr.getUrlRequestParam("billId"));
						} else {
							$("#hiddenId").val(datas[0].ID);
						}
						//默认设置选中的图标
						$(".tag").hide();
						var tags = $(".tag");
						for (var i = 0; i < tags.length; i++) {
							if (tags[i].id == $("#hiddenId").val()) {
								tags[i].style.display = '';
								break;
							}
						}
						initEdit($("#hiddenId").val());
						initFirset_flag = true;
						//query_flag = true; 
					} else {
						//默认设置选中的图标   (为保存等操作后,依然选中操作对象)
						$(".tag").hide();
						var tags = $(".tag");
						for (var i = 0; i < tags.length; i++) {
							if (tags[i].id == $("#hiddenId").val()) {
								tags[i].style.display = '';
								break;
							}
						}

					}

					/*========扩展开始========*/
					//计算平均分
					avgScore = sumScore / datas.length
					score = avgScore.toFixed(2);
					/*========扩展结束========*/

				}
				//  为我的已办事项 跳转时  重设
				setTimeout(function () {
					if (window.frames.length != parent.frames.length)
						shr.setIframeHeight(window.id);
				}, shr.blockMinTime);


			}


		});
	
	},
	
	refulshAction: function (e) {
		this.reloadPage();
	},
	
	initDataChangeAction: function () {
		//shr.perf.AssessTargetEvaluEdit.superClass.initDataChangeAction.call(this);				
		$("#solutionPeriodname").shrSelect("option", {
			onChange: function () {
				clz.queryAction();
			}
		});

		$("#nodeState").shrSelect("option", {
			onChange: function () {
				clz.queryAction();
			}
		});
	},


	//点击查询按钮执行的方法
	queryAction: function () {
		//  为我的已办事项 跳转时  重设
		if (window.frames.length != parent.frames.length)
			shr.setIframeHeight(window.id);
		//query_flag = false; // 查询时  不从billId  取值查询 ， 防止   已提交  、未提交  查询条件切换时     billId 不匹配  查询不到数据       
		goSmall();
		if ($('#nodeState_el').val() == 1) {
			$("#saveTarget").hide();
			$("#commitTarget").hide();
			$("#allCommitTarget").hide();
			$("#backTarget").hide();
		} else {
			$("#saveTarget").show();
			$("#commitTarget").show();
			$("#allCommitTarget").show();
			$("#backTarget").show();
		}
		//查询前设置参数，供重新初始化  查询条件时设置当前值使用
		sId = $('#solutionPeriodname_el').val();
		sName = $('#solutionPeriodname').val();

		that.getAssessTargetForLeftList();
	},

	doCheck: function (e) {
		if ($("#hiddenId").val() == "" || $("#hiddenId").val() == null) {
			shr.showError({
				message: "无数据，不能操作！",
				hideAfter: 2
			});
			return true;
		}
		return false;
	},
	//获取单选框选中数据
	getRecuritmentTypeDate : function(){
		var recTypeId="";
		if(isOrg){
		recTypeId= $('input[name="exitType"]:checked').attr("data-pk");
		}
		
		return recTypeId;
	},
	saveTargetAction: function () {
		//通用检查				
		if (clz.doCheck()) {
			return;
		}
        /*========扩展开始========*/
     if(isOrg==false){
            var abs = that.checkScore();
            if (abs == false) {
                shr.showWarning({
                    message: "考核评分相差不能大于0.5,请修改评分后重新保存！",
                    hideAfter: 3
                })
                return;
            }
        }
		/*========扩展结束========*/
		var datas = [];
		for (var i = 0; i < tableSize; i++) {
			var ds = $("#table" + i).wafGrid('getAllRowData');
			for (var j = 0; j < ds.length; j++) {
				//alert("ds:"+ds[j]);
				datas.push(ds[j]);
			}
		}
		var count_datas = $("#table_count").wafGrid('getAllRowData');
		var commentDatas = [];
		var comment = "";
		for (var i = 0; i < nodeCount; i++) { //   节点总计
			comment = $("#comment" + i).val();

			if (comment.length > 2000) {
				shr.showError({
					message: "节点 " + (i + 1) + " 评论超过2000个字符！",
					hideAfter: 3
				});
				return;
			}

			commentDatas.push($("#comment" + i).val());
		}
		var radioData=this.getRecuritmentTypeDate();		
		that.remoteCall({
			method: "saveTargetForm",
			param: {
				assignmentId: $("#hiddenId").val(),
				datas: shr.toJSON(datas),
				count_datas: shr.toJSON(count_datas),
				commentDatas: shr.toJSON(commentDatas),
				objectId:objectId,
				isOrg:isOrg,
				radioData:radioData
			},
			success: function (response) {
				if (response.opResult == 'ok') {
					shr.showInfo({
						message: "保存成功！",
						hideAfter: 3
					});
					initFirset_flag = false;
					goSmall();
					clz.getAssessTargetForLeftList();
					initEdit($("#hiddenId").val());
				} else {

					shr.showError({
						message: response.msg,
						hideAfter: 10
					});
				}

			}
		});

	},
	getNodeProcesserId: function (assignmentId) {
		that.remoteCall({
			method: "getNodeProcesserId",
			param: {
				assignmentId: assignmentId
			},
			success: function (response) {
				nodeProcesserId = response.nodeProcesserId;
				clz.getAssessTargetForLeftList();
				return nodeProcesserId;
			}
		});
	},


	getCurrentNodeLevel: function (assignmentId) {
		that.remoteCall({
			method: "getCurrentNodeLevel",
			param: {
				assignmentId: assignmentId
			},
			success: function (response) {
				nodeLevel = response.nodeLevel;
				return response.nodeLevel;
			}
		});
	},


	commitTargetRealcommitTargetReal: function () {
		var radioData=this.getRecuritmentTypeDate();
		that.remoteCall({
			method: "submitTargetReal",
			param: {
				assignmentId: $("#hiddenId").val(),
				objectId:objectId,
				isOrg:isOrg,
				radioData:radioData
			},
			success: function (response) {
				if (response.opResult == 'ok') {
					shr.showInfo({
						message: "提交成功！",
						hideAfter: 3
					});
					clz.reloadPage({
						uipk: "com.kingdee.eas.hr.perf.app.TargetEvaluEditForm",
						nodeState_value: 0

					});
					// 提交后回退  导航菜单				
					var breadcrumb = $('#breadcrumb').shrBreadcrumb();
					breadcrumb.shrBreadcrumb('pop');

					/*	
						//initFirset_flag = false;
					query_flag = false;
					goSmall();
					setTimeout(function() {						
						clz.getAssessTargetForLeftList();
					}, 500);
					*/
				} else {
					shr.showError({
						message: response.msg,
						hideAfter: 10
					});
				}
			}
		});
	},
	commitTargetAction: function () {
		//通用检查				
		if (clz.doCheck()) {
			return;
		}
		var datas = [];
		for (var i = 0; i < tableSize; i++) {
			var ds = $("#table" + i).wafGrid('getAllRowData');
			for (var j = 0; j < ds.length; j++) {
				//alert("ds:"+ds[j]);
				datas.push(ds[j]);
			}
		}
		var count_datas = $("#table_count").wafGrid('getAllRowData');
		var commentDatas = [];
		for (var i = 0; i < nodeCount; i++) { //   节点总计
			commentDatas.push($("#comment" + i).val());
		}
		that.remoteCall({
			method: "submitTarget",

			param: {
				assignmentId: $("#hiddenId").val(),
				datas: shr.toJSON(datas),
				count_datas: shr.toJSON(count_datas),
				commentDatas: shr.toJSON(commentDatas)
			},
			success: function (response) {
				if (response.opResult == 'ok') {
					//重新加载右侧界面，保证触发计算总分
					initEdit($("#hiddenId").val());
					shr.showConfirm(response.evaluMsg + '您确认要提交生效吗？', function () {
						clz.commitTargetReal();
					});
				} else {
					shr.showError({
						message: response.msg,
						hideAfter: 10
					});
				}
			}
		});
	},
	//自定义批量提交
	allCommitTargetAction: function () {
		if (isNull) {
			var personName = "";
			for (var i = 0; i < personNull.length; i++) {
				personName += personNull[i] + "   ";
			}
			shr.showError({
				message: personName + "尚未保存评分不能提交",
				hideAfter: 3
			});
			return;
		}
		
		if(orgscore=="" || orgscore==undefined){
			shr.showError({
				message: "当前登录人部门评分不可为空,点击左侧数据查看",
				hideAfter: 6
			});
			return;
		}
		if(Number(score)>Number(orgscore)){
			shr.showError({
				message: "评估对象综合平均分不可大于部门评分！",
				hideAfter: 6
			});
			return;
		}
		shr.showConfirm('您确认要批量提交生效吗？', function () {
			clz.allSubmitAction();
		});
	},
	//调用标准产品提交功能
	allSubmitAction: function () {
		var that = this;
		var billids = [];
		$("a[class='fff']").each(function (j, item) {
			billids.push(item.firstElementChild.id);
		});
		for (var i = 0; i < billids.length; i++) {
			that.remoteCall({
				method: "submitTargetReal",
				param: {
					assignmentId: billids[i]
				},
				success: function (response) {
					if (response.opResult == 'ok') {
						shr.showInfo({
							message: "提交成功！",
							hideAfter: 3
						});
						clz.reloadPage({
							uipk: "com.kingdee.eas.hr.perf.app.TargetEvaluEditForm",
							nodeState_value: 0

						});
						// 提交后回退  导航菜单				
						var breadcrumb = $('#breadcrumb').shrBreadcrumb();
						breadcrumb.shrBreadcrumb('pop');

						/*	
							//initFirset_flag = false;
						query_flag = false;
						goSmall();
						setTimeout(function() {						
							clz.getAssessTargetForLeftList();
						}, 500);
						*/
					} else {
						shr.showError({
							message: response.msg,
							hideAfter: 10
						});
					}
				}
			});
		}
	},
	RealbackTarget: function (e) {
		setTimeout(function () {
			$.block.show({
				text: '正在执行，请稍候...'
			});
		}, shr.blockMinTime);

		that.remoteCall({
			method: "backTarget",
			param: {
				//evaluObjectId : $("#evaluObjectId").val(),
				assignmentId: $("#hiddenId").val(),
				reBackReason: $("#backReason").val(),
				nodeCount: nodeCount
			},
			success: function (response) {
				if (response.opResult == 'ok') {
					shr.showInfo({
						message: "打回成功！",
						hideAfter: 3
					});
					clz.reloadPage({
						uipk: "com.kingdee.eas.hr.perf.app.TargetEvaluEditForm",
						billId: "",
						nodeState_value: 0

					});
					// 提交后回退  导航菜单				
					var breadcrumb = $('#breadcrumb').shrBreadcrumb();
					breadcrumb.shrBreadcrumb('pop');
					/*
		        		$('#dialogViewMore').remove();
		        		query_flag = false;
		        		goSmall();
						setTimeout(function() {							
							clz.getAssessTargetForLeftList();
						}, 500);
						*/
				} else {
					shr.showError({
						message: response.msg,
						hideAfter: 10
					});
					//$(this).dialog( "close" );
					$('#dialogViewMore').remove();
				}

				setTimeout(function () {
					$.block.hide();
				}, shr.blockMinTime);

			}
		});
	},

	backTargetAction: function () { // cols='200'   rows='10' width='400'
		//通用检查				
		if (clz.doCheck()) {
			return;
		}
		if (nodeLevel == "1" || nodeLevel == 1) {
			shr.showError({
				message: "当前节点为一级节点，不可打回！",
				hideAfter: 3
			});
			return;
		}
		// if(nodeCount > 1   ){  //非首节点可以打回   --    是否一级节点放入后台处理
		var dialog_Html = "<div id='dialogViewMore' style='overflow: scroll;'  >" +
			"<textarea  style='width: 473px; min-height: 0px; max-height: none; height: 244px;'  id='backReason'    ></textarea></td>" +
			"</div>";
		$(document.body).append(dialog_Html);
		$('#dialogViewMore').dialog({
			title: '打回原因',
			width: 500,
			height: 400,
			modal: true,
			resizable: false,
			position: {
				my: 'center',
				//at: 'top+20%',
				at: 'center',
				of: window
			},
			buttons: {
				"确认": function () {
					clz.RealbackTarget();
				},
				"关闭": function () {
					$(this).dialog("close");
					$('#dialogViewMore').remove();
				}
			}
		});
		/*
	 	}else{
			shr.showError({
				message : "当前目标不可打回！",
				hideAfter : 3
					});
			return;	
		}	*/
	},
	/*========扩展开始========*/
	//校验评分是否相差0.5
	checkScore: function () {
		var that = this;
		/**
		 * 获取表格评分
		 */
		var datasOne = [];
		var thissc = 0; //表格的评分
		var thatsc = 0; //列表的评分
		for (var i = 0; i < tableSize; i++) {
			var ds = $("#table" + i).wafGrid('getAllRowData');
			for (var j = 0; j < ds.length; j++) {
				datasOne.push(ds[j]);
			}
		}

		for (var i = 0; i < datasOne.length; i++) {
			//console.info("当前表格评分" + datasOne[i].score1);
			thissc = Number(datasOne[i].score1);
			for (var j = 0; j < scoredatas.length; j++) {
				//console.info("当前列表评分" + scoredatas[j]);
				thatsc = scoredatas[j];
				if (Math.abs((thissc - thatsc)) > 0.5) {
					return false;
				}
			}

		}
		return true;
	}
	/*========扩展结束========*/



});